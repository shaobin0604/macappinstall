---
title: "Install uuu on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Universal Update Utility, mfgtools 3.0. NXP I.MX Chip image deploy tools"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install uuu on MacOS using homebrew

- App Name: uuu
- App description: Universal Update Utility, mfgtools 3.0. NXP I.MX Chip image deploy tools
- App Version: 1.4.165
- App Website: https://github.com/NXPmicro/mfgtools

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install uuu with the following command
   ```
   brew install uuu
   ```
4. uuu is ready to use now!