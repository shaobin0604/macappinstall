---
title: "Install pidof on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Display the PID number for a given process name"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pidof on MacOS using homebrew

- App Name: pidof
- App description: Display the PID number for a given process name
- App Version: 0.1.4
- App Website: http://www.nightproductions.net/cli.htm

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pidof with the following command
   ```
   brew install pidof
   ```
4. pidof is ready to use now!