---
title: "Install Clock Signal on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Latency-hating emulator of 8- and 16-bit platforms"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Clock Signal on MacOS using homebrew

- App Name: Clock Signal
- App description: Latency-hating emulator of 8- and 16-bit platforms
- App Version: 2021-12-19
- App Website: https://github.com/TomHarte/CLK

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Clock Signal with the following command
   ```
   brew install --cask clock-signal
   ```
4. Clock Signal is ready to use now!