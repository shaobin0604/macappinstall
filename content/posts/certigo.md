---
title: "Install certigo on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utility to examine and validate certificates in a variety of formats"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install certigo on MacOS using homebrew

- App Name: certigo
- App description: Utility to examine and validate certificates in a variety of formats
- App Version: 1.14.1
- App Website: https://github.com/square/certigo

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install certigo with the following command
   ```
   brew install certigo
   ```
4. certigo is ready to use now!