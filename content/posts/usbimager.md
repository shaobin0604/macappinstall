---
title: "Install USBImager on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Very minimal GUI app that can write/read to disk images and USB drives"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install USBImager on MacOS using homebrew

- App Name: USBImager
- App description: Very minimal GUI app that can write/read to disk images and USB drives
- App Version: 1.0.8
- App Website: https://bztsrc.gitlab.io/usbimager/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install USBImager with the following command
   ```
   brew install --cask usbimager
   ```
4. USBImager is ready to use now!