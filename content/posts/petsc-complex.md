---
title: "Install petsc-complex on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Portable, Extensible Toolkit for Scientific Computation (complex)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install petsc-complex on MacOS using homebrew

- App Name: petsc-complex
- App description: Portable, Extensible Toolkit for Scientific Computation (complex)
- App Version: 3.16.3
- App Website: https://www.mcs.anl.gov/petsc/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install petsc-complex with the following command
   ```
   brew install petsc-complex
   ```
4. petsc-complex is ready to use now!