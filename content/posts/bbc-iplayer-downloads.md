---
title: "Install BBC iPlayer Downloads on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Download programmes from the BBC iPlayer website"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install BBC iPlayer Downloads on MacOS using homebrew

- App Name: BBC iPlayer Downloads
- App description: Download programmes from the BBC iPlayer website
- App Version: 2.13.6
- App Website: https://www.bbc.co.uk/iplayer/install

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install BBC iPlayer Downloads with the following command
   ```
   brew install --cask bbc-iplayer-downloads
   ```
4. BBC iPlayer Downloads is ready to use now!