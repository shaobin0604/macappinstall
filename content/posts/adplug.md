---
title: "Install adplug on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Free, hardware independent AdLib sound player library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install adplug on MacOS using homebrew

- App Name: adplug
- App description: Free, hardware independent AdLib sound player library
- App Version: 2.3.3
- App Website: https://adplug.github.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install adplug with the following command
   ```
   brew install adplug
   ```
4. adplug is ready to use now!