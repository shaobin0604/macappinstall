---
title: "Install Leksah on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Haskell IDE"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Leksah on MacOS using homebrew

- App Name: Leksah
- App description: Haskell IDE
- App Version: 0.16.2.2-ghc-8.0.2
- App Website: http://leksah.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Leksah with the following command
   ```
   brew install --cask leksah
   ```
4. Leksah is ready to use now!