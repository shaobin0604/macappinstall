---
title: "Install Couchbase Server on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Distributed NoSQL cloud database"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Couchbase Server on MacOS using homebrew

- App Name: Couchbase Server
- App description: Distributed NoSQL cloud database
- App Version: 7.0.2
- App Website: https://www.couchbase.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Couchbase Server with the following command
   ```
   brew install --cask couchbase-server-community
   ```
4. Couchbase Server is ready to use now!