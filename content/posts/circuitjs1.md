---
title: "Install Falstad CircuitJS on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Electronic circuit simulator"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Falstad CircuitJS on MacOS using homebrew

- App Name: Falstad CircuitJS
- App description: Electronic circuit simulator
- App Version: 9.3.2
- App Website: https://www.falstad.com/circuit/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Falstad CircuitJS with the following command
   ```
   brew install --cask circuitjs1
   ```
4. Falstad CircuitJS is ready to use now!