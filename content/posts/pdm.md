---
title: "Install pdm on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Modern Python package manager with PEP 582 support"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pdm on MacOS using homebrew

- App Name: pdm
- App description: Modern Python package manager with PEP 582 support
- App Version: 1.13.0
- App Website: https://pdm.fming.dev

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pdm with the following command
   ```
   brew install pdm
   ```
4. pdm is ready to use now!