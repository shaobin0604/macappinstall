---
title: "Install grsync on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GUI for rsync"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install grsync on MacOS using homebrew

- App Name: grsync
- App description: GUI for rsync
- App Version: 1.2.8
- App Website: https://www.opbyte.it/grsync/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install grsync with the following command
   ```
   brew install grsync
   ```
4. grsync is ready to use now!