---
title: "Install Ex Falso on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Music tag editor"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Ex Falso on MacOS using homebrew

- App Name: Ex Falso
- App description: Music tag editor
- App Version: 4.4.0
- App Website: https://quodlibet.readthedocs.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Ex Falso with the following command
   ```
   brew install --cask exfalso
   ```
4. Ex Falso is ready to use now!