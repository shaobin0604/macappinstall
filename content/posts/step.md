---
title: "Install step on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Crypto and x509 Swiss-Army-Knife"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install step on MacOS using homebrew

- App Name: step
- App description: Crypto and x509 Swiss-Army-Knife
- App Version: 0.18.1
- App Website: https://smallstep.com

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install step with the following command
   ```
   brew install step
   ```
4. step is ready to use now!