---
title: "Install The Archive on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Note Taking: Nimble, Calm, Plain.txt"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install The Archive on MacOS using homebrew

- App Name: The Archive
- App description: Note Taking: Nimble, Calm, Plain.txt
- App Version: 1.7.5
- App Website: https://zettelkasten.de/the-archive/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install The Archive with the following command
   ```
   brew install --cask the-archive
   ```
4. The Archive is ready to use now!