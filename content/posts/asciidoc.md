---
title: "Install asciidoc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Formatter/translator for text files to numerous formats"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install asciidoc on MacOS using homebrew

- App Name: asciidoc
- App description: Formatter/translator for text files to numerous formats
- App Version: 10.1.2
- App Website: https://asciidoc-py.github.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install asciidoc with the following command
   ```
   brew install asciidoc
   ```
4. asciidoc is ready to use now!