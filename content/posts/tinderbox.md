---
title: "Install Tinderbox on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to take, visualize and analyze notes"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Tinderbox on MacOS using homebrew

- App Name: Tinderbox
- App description: Tool to take, visualize and analyze notes
- App Version: 9.1.0
- App Website: https://eastgate.com/Tinderbox/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Tinderbox with the following command
   ```
   brew install --cask tinderbox
   ```
4. Tinderbox is ready to use now!