---
title: "Install deja-gnu on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Framework for testing other programs"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install deja-gnu on MacOS using homebrew

- App Name: deja-gnu
- App description: Framework for testing other programs
- App Version: 1.6.3
- App Website: https://www.gnu.org/software/dejagnu/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install deja-gnu with the following command
   ```
   brew install deja-gnu
   ```
4. deja-gnu is ready to use now!