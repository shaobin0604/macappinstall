---
title: "Install git-review on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Submit git branches to gerrit for review"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install git-review on MacOS using homebrew

- App Name: git-review
- App description: Submit git branches to gerrit for review
- App Version: 2.2.0
- App Website: https://opendev.org/opendev/git-review

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install git-review with the following command
   ```
   brew install git-review
   ```
4. git-review is ready to use now!