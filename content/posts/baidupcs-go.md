---
title: "Install baidupcs-go on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Terminal utility for Baidu Network Disk"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install baidupcs-go on MacOS using homebrew

- App Name: baidupcs-go
- App description: Terminal utility for Baidu Network Disk
- App Version: 3.8.6
- App Website: https://github.com/qjfoidnh/BaiduPCS-Go

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install baidupcs-go with the following command
   ```
   brew install baidupcs-go
   ```
4. baidupcs-go is ready to use now!