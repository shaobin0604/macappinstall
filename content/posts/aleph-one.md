---
title: "Install Aleph One on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Open-source continuation of Bungie’s Marathon 2 game engine"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Aleph One on MacOS using homebrew

- App Name: Aleph One
- App description: Open-source continuation of Bungie’s Marathon 2 game engine
- App Version: 20220115
- App Website: https://alephone.lhowon.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Aleph One with the following command
   ```
   brew install --cask aleph-one
   ```
4. Aleph One is ready to use now!