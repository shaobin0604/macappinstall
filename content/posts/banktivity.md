---
title: "Install Banktivity on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "App to manage bank accounts in one place"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Banktivity on MacOS using homebrew

- App Name: Banktivity
- App description: App to manage bank accounts in one place
- App Version: 8.7.3
- App Website: https://www.iggsoftware.com/banktivity/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Banktivity with the following command
   ```
   brew install --cask banktivity
   ```
4. Banktivity is ready to use now!