---
title: "Install inxi on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Full featured CLI system information tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install inxi on MacOS using homebrew

- App Name: inxi
- App description: Full featured CLI system information tool
- App Version: 3.3.12-1
- App Website: https://smxi.org/docs/inxi.htm

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install inxi with the following command
   ```
   brew install inxi
   ```
4. inxi is ready to use now!