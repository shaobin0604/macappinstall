---
title: "Install Rocket.Chat on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Official desktop client for Rocket.Chat"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Rocket.Chat on MacOS using homebrew

- App Name: Rocket.Chat
- App description: Official desktop client for Rocket.Chat
- App Version: 3.7.7
- App Website: https://rocket.chat/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Rocket.Chat with the following command
   ```
   brew install --cask rocket-chat
   ```
4. Rocket.Chat is ready to use now!