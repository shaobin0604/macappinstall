---
title: "Install pokerstove on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Poker evaluation and enumeration software"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pokerstove on MacOS using homebrew

- App Name: pokerstove
- App description: Poker evaluation and enumeration software
- App Version: 1.0
- App Website: https://github.com/andrewprock/pokerstove

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pokerstove with the following command
   ```
   brew install pokerstove
   ```
4. pokerstove is ready to use now!