---
title: "Install libgosu on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "2D game development library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libgosu on MacOS using homebrew

- App Name: libgosu
- App description: 2D game development library
- App Version: 1.2.0
- App Website: https://libgosu.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libgosu with the following command
   ```
   brew install libgosu
   ```
4. libgosu is ready to use now!