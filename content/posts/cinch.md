---
title: "Install Cinch on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Window management tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Cinch on MacOS using homebrew

- App Name: Cinch
- App description: Window management tool
- App Version: 1.2.4,146
- App Website: https://www.irradiatedsoftware.com/cinch/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Cinch with the following command
   ```
   brew install --cask cinch
   ```
4. Cinch is ready to use now!