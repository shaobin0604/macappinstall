---
title: "Install FromScratch on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Autosaving Scratchpad. A simple but smart note-taking app"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install FromScratch on MacOS using homebrew

- App Name: FromScratch
- App description: Autosaving Scratchpad. A simple but smart note-taking app
- App Version: 1.4.3
- App Website: https://fromscratch.rocks/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install FromScratch with the following command
   ```
   brew install --cask fromscratch
   ```
4. FromScratch is ready to use now!