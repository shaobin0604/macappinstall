---
title: "Install remarshal on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Convert between TOML, YAML and JSON"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install remarshal on MacOS using homebrew

- App Name: remarshal
- App description: Convert between TOML, YAML and JSON
- App Version: 0.14.0
- App Website: https://github.com/dbohdan/remarshal

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install remarshal with the following command
   ```
   brew install remarshal
   ```
4. remarshal is ready to use now!