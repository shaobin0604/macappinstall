---
title: "Install Stay on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Windows manager"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Stay on MacOS using homebrew

- App Name: Stay
- App description: Windows manager
- App Version: 1.4,747
- App Website: https://cordlessdog.com/stay/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Stay with the following command
   ```
   brew install --cask stay
   ```
4. Stay is ready to use now!