---
title: "Install par on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Paragraph reflow for email"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install par on MacOS using homebrew

- App Name: par
- App description: Paragraph reflow for email
- App Version: 1.53.0
- App Website: http://www.nicemice.net/par/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install par with the following command
   ```
   brew install par
   ```
4. par is ready to use now!