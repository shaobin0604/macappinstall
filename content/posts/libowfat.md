---
title: "Install libowfat on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Reimplements libdjb"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libowfat on MacOS using homebrew

- App Name: libowfat
- App description: Reimplements libdjb
- App Version: 0.32
- App Website: https://www.fefe.de/libowfat/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libowfat with the following command
   ```
   brew install libowfat
   ```
4. libowfat is ready to use now!