---
title: "Install eot-utils on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tools to convert fonts from OTF/TTF to EOT format"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install eot-utils on MacOS using homebrew

- App Name: eot-utils
- App description: Tools to convert fonts from OTF/TTF to EOT format
- App Version: 1.1
- App Website: https://www.w3.org/Tools/eot-utils/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install eot-utils with the following command
   ```
   brew install eot-utils
   ```
4. eot-utils is ready to use now!