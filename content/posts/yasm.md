---
title: "Install yasm on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Modular BSD reimplementation of NASM"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install yasm on MacOS using homebrew

- App Name: yasm
- App description: Modular BSD reimplementation of NASM
- App Version: 1.3.0
- App Website: https://yasm.tortall.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install yasm with the following command
   ```
   brew install yasm
   ```
4. yasm is ready to use now!