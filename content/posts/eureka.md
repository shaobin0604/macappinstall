---
title: "Install eureka on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CLI tool to input and store your ideas without leaving the terminal"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install eureka on MacOS using homebrew

- App Name: eureka
- App description: CLI tool to input and store your ideas without leaving the terminal
- App Version: 1.8.1
- App Website: https://github.com/simeg/eureka

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install eureka with the following command
   ```
   brew install eureka
   ```
4. eureka is ready to use now!