---
title: "Install traefik on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Modern reverse proxy"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install traefik on MacOS using homebrew

- App Name: traefik
- App description: Modern reverse proxy
- App Version: 2.6.1
- App Website: https://traefik.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install traefik with the following command
   ```
   brew install traefik
   ```
4. traefik is ready to use now!