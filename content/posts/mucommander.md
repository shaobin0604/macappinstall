---
title: "Install muCommander on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "File manager with a dual-pane interface"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install muCommander on MacOS using homebrew

- App Name: muCommander
- App description: File manager with a dual-pane interface
- App Version: 0.9.7-1
- App Website: https://www.mucommander.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install muCommander with the following command
   ```
   brew install --cask mucommander
   ```
4. muCommander is ready to use now!