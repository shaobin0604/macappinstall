---
title: "Install gdl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GNOME Docking Library provides docking features for GTK+ 3"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gdl on MacOS using homebrew

- App Name: gdl
- App description: GNOME Docking Library provides docking features for GTK+ 3
- App Version: 3.40.0
- App Website: https://gitlab.gnome.org/GNOME/gdl

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gdl with the following command
   ```
   brew install gdl
   ```
4. gdl is ready to use now!