---
title: "Install FortiClient VPN on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Free VPN client for FortiClient"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install FortiClient VPN on MacOS using homebrew

- App Name: FortiClient VPN
- App description: Free VPN client for FortiClient
- App Version: 7.0.0.22
- App Website: https://forticlient.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install FortiClient VPN with the following command
   ```
   brew install --cask forticlient-vpn
   ```
4. FortiClient VPN is ready to use now!