---
title: "Install dvd+rw-tools on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "DVD+-RW/R tools"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dvd+rw-tools on MacOS using homebrew

- App Name: dvd+rw-tools
- App description: DVD+-RW/R tools
- App Version: 7.1
- App Website: http://fy.chalmers.se/~appro/linux/DVD+RW/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dvd+rw-tools with the following command
   ```
   brew install dvd+rw-tools
   ```
4. dvd+rw-tools is ready to use now!