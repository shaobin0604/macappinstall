---
title: "Install clutter-gtk on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GTK+ integration library for Clutter"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install clutter-gtk on MacOS using homebrew

- App Name: clutter-gtk
- App description: GTK+ integration library for Clutter
- App Version: 1.8.4
- App Website: https://wiki.gnome.org/Projects/Clutter

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install clutter-gtk with the following command
   ```
   brew install clutter-gtk
   ```
4. clutter-gtk is ready to use now!