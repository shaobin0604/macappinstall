---
title: "Install physfs on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library to provide abstract access to various archives"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install physfs on MacOS using homebrew

- App Name: physfs
- App description: Library to provide abstract access to various archives
- App Version: 3.0.2
- App Website: https://icculus.org/physfs/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install physfs with the following command
   ```
   brew install physfs
   ```
4. physfs is ready to use now!