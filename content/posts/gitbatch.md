---
title: "Install gitbatch on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Manage your git repositories in one place"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gitbatch on MacOS using homebrew

- App Name: gitbatch
- App description: Manage your git repositories in one place
- App Version: 0.6.1
- App Website: https://github.com/isacikgoz/gitbatch

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gitbatch with the following command
   ```
   brew install gitbatch
   ```
4. gitbatch is ready to use now!