---
title: "Install webify on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Wrapper for shell commands as web services"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install webify on MacOS using homebrew

- App Name: webify
- App description: Wrapper for shell commands as web services
- App Version: 1.5.0
- App Website: https://github.com/beefsack/webify

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install webify with the following command
   ```
   brew install webify
   ```
4. webify is ready to use now!