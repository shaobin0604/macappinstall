---
title: "Install Clone Hero on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Guitar Hero clone"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Clone Hero on MacOS using homebrew

- App Name: Clone Hero
- App description: Guitar Hero clone
- App Version: 0.23.2.2
- App Website: https://clonehero.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Clone Hero with the following command
   ```
   brew install --cask clone-hero
   ```
4. Clone Hero is ready to use now!