---
title: "Install xmount on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Convert between multiple input & output disk image types"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install xmount on MacOS using homebrew

- App Name: xmount
- App description: Convert between multiple input & output disk image types
- App Version: 0.7.6
- App Website: https://www.pinguin.lu/xmount/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install xmount with the following command
   ```
   brew install xmount
   ```
4. xmount is ready to use now!