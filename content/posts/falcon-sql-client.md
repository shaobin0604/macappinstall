---
title: "Install Falcon SQL Client on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Free, open-source SQL client"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Falcon SQL Client on MacOS using homebrew

- App Name: Falcon SQL Client
- App description: Free, open-source SQL client
- App Version: 4.1.0
- App Website: https://plot.ly/free-sql-client-download/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Falcon SQL Client with the following command
   ```
   brew install --cask falcon-sql-client
   ```
4. Falcon SQL Client is ready to use now!