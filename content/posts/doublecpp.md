---
title: "Install doublecpp on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Double dispatch in C++"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install doublecpp on MacOS using homebrew

- App Name: doublecpp
- App description: Double dispatch in C++
- App Version: 0.6.3
- App Website: https://doublecpp.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install doublecpp with the following command
   ```
   brew install doublecpp
   ```
4. doublecpp is ready to use now!