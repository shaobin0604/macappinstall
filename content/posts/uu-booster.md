---
title: "Install UU Booster on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Network accelerator"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install UU Booster on MacOS using homebrew

- App Name: UU Booster
- App description: Network accelerator
- App Version: 2.6.5,233
- App Website: https://uu.163.com/down/mac/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install UU Booster with the following command
   ```
   brew install --cask uu-booster
   ```
4. UU Booster is ready to use now!