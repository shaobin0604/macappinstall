---
title: "Install orocos-kdl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Orocos Kinematics and Dynamics C++ library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install orocos-kdl on MacOS using homebrew

- App Name: orocos-kdl
- App description: Orocos Kinematics and Dynamics C++ library
- App Version: 1.5.1
- App Website: https://orocos.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install orocos-kdl with the following command
   ```
   brew install orocos-kdl
   ```
4. orocos-kdl is ready to use now!