---
title: "Install Retrobatch on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Batch image processor"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Retrobatch on MacOS using homebrew

- App Name: Retrobatch
- App description: Batch image processor
- App Version: 1.4.4,963
- App Website: https://flyingmeat.com/retrobatch/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Retrobatch with the following command
   ```
   brew install --cask retrobatch
   ```
4. Retrobatch is ready to use now!