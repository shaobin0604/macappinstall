---
title: "Install zsync on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "File transfer program"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install zsync on MacOS using homebrew

- App Name: zsync
- App description: File transfer program
- App Version: 0.6.2
- App Website: http://zsync.moria.org.uk/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install zsync with the following command
   ```
   brew install zsync
   ```
4. zsync is ready to use now!