---
title: "Install flac123 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line program for playing FLAC audio files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install flac123 on MacOS using homebrew

- App Name: flac123
- App description: Command-line program for playing FLAC audio files
- App Version: 0.0.12
- App Website: https://flac-tools.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install flac123 with the following command
   ```
   brew install flac123
   ```
4. flac123 is ready to use now!