---
title: "Install Synergy on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Keyboard and mouse sharing tool - open-source core"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Synergy on MacOS using homebrew

- App Name: Synergy
- App description: Keyboard and mouse sharing tool - open-source core
- App Version: 1.14.2,c6918b74
- App Website: https://symless.com/synergy

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Synergy with the following command
   ```
   brew install --cask synergy
   ```
4. Synergy is ready to use now!