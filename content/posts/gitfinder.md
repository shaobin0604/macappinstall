---
title: "Install GitFinder on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Git client with Finder integration"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install GitFinder on MacOS using homebrew

- App Name: GitFinder
- App description: Git client with Finder integration
- App Version: 1.7.4,117
- App Website: https://gitfinder.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install GitFinder with the following command
   ```
   brew install --cask gitfinder
   ```
4. GitFinder is ready to use now!