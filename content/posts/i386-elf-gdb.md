---
title: "Install i386-elf-gdb on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GNU debugger for i386-elf cross development"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install i386-elf-gdb on MacOS using homebrew

- App Name: i386-elf-gdb
- App description: GNU debugger for i386-elf cross development
- App Version: 10.2
- App Website: https://www.gnu.org/software/gdb/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install i386-elf-gdb with the following command
   ```
   brew install i386-elf-gdb
   ```
4. i386-elf-gdb is ready to use now!