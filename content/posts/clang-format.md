---
title: "Install clang-format on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Formatting tools for C, C++, Obj-C, Java, JavaScript, TypeScript"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install clang-format on MacOS using homebrew

- App Name: clang-format
- App description: Formatting tools for C, C++, Obj-C, Java, JavaScript, TypeScript
- App Version: 13.0.1
- App Website: https://clang.llvm.org/docs/ClangFormat.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install clang-format with the following command
   ```
   brew install clang-format
   ```
4. clang-format is ready to use now!