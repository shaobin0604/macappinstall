---
title: "Install cconv on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Iconv based simplified-traditional Chinese conversion tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cconv on MacOS using homebrew

- App Name: cconv
- App description: Iconv based simplified-traditional Chinese conversion tool
- App Version: 0.6.3
- App Website: https://github.com/xiaoyjy/cconv

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cconv with the following command
   ```
   brew install cconv
   ```
4. cconv is ready to use now!