---
title: "Install libmrss on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C library for RSS files or streams"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libmrss on MacOS using homebrew

- App Name: libmrss
- App description: C library for RSS files or streams
- App Version: 0.19.2
- App Website: https://github.com/bakulf/libmrss

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libmrss with the following command
   ```
   brew install libmrss
   ```
4. libmrss is ready to use now!