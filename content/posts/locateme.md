---
title: "Install locateme on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Find your location using Apple's geolocation services"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install locateme on MacOS using homebrew

- App Name: locateme
- App description: Find your location using Apple's geolocation services
- App Version: 0.2.1
- App Website: https://iharder.sourceforge.io/current/macosx/locateme

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install locateme with the following command
   ```
   brew install locateme
   ```
4. locateme is ready to use now!