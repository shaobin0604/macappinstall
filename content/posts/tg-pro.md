---
title: "Install TG Pro on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Temperature monitoring, fan control and diagnostics"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install TG Pro on MacOS using homebrew

- App Name: TG Pro
- App description: Temperature monitoring, fan control and diagnostics
- App Version: 2.66,13197
- App Website: https://www.tunabellysoftware.com/tgpro/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install TG Pro with the following command
   ```
   brew install --cask tg-pro
   ```
4. TG Pro is ready to use now!