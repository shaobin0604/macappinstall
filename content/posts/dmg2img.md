---
title: "Install dmg2img on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utilities for converting macOS DMG images"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dmg2img on MacOS using homebrew

- App Name: dmg2img
- App description: Utilities for converting macOS DMG images
- App Version: 1.6.7
- App Website: http://vu1tur.eu.org/tools/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dmg2img with the following command
   ```
   brew install dmg2img
   ```
4. dmg2img is ready to use now!