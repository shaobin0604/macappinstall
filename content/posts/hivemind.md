---
title: "Install hivemind on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Process manager for Procfile-based applications"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install hivemind on MacOS using homebrew

- App Name: hivemind
- App description: Process manager for Procfile-based applications
- App Version: 1.1.0
- App Website: https://github.com/DarthSim/hivemind

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install hivemind with the following command
   ```
   brew install hivemind
   ```
4. hivemind is ready to use now!