---
title: "Install spoof-mac on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Spoof your MAC address in macOS"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install spoof-mac on MacOS using homebrew

- App Name: spoof-mac
- App description: Spoof your MAC address in macOS
- App Version: 2.1.1
- App Website: https://github.com/feross/SpoofMAC

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install spoof-mac with the following command
   ```
   brew install spoof-mac
   ```
4. spoof-mac is ready to use now!