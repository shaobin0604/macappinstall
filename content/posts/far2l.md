---
title: "Install far2l on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Unix fork of FAR Manager v2"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install far2l on MacOS using homebrew

- App Name: far2l
- App description: Unix fork of FAR Manager v2
- App Version: 2.4.0
- App Website: https://github.com/elfmz/far2l

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install far2l with the following command
   ```
   brew install --cask far2l
   ```
4. far2l is ready to use now!