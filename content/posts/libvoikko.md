---
title: "Install libvoikko on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Linguistic software and Finnish dictionary"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libvoikko on MacOS using homebrew

- App Name: libvoikko
- App description: Linguistic software and Finnish dictionary
- App Version: 4.3.1
- App Website: https://voikko.puimula.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libvoikko with the following command
   ```
   brew install libvoikko
   ```
4. libvoikko is ready to use now!