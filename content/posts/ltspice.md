---
title: "Install LTspice on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "SPICE simulation software, schematic capture and waveform viewer"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install LTspice on MacOS using homebrew

- App Name: LTspice
- App description: SPICE simulation software, schematic capture and waveform viewer
- App Version: 17.0.40
- App Website: https://www.analog.com/en/design-center/design-tools-and-calculators/ltspice-simulator.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install LTspice with the following command
   ```
   brew install --cask ltspice
   ```
4. LTspice is ready to use now!