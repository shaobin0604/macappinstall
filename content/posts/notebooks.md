---
title: "Install Notebooks on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Word processor"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Notebooks on MacOS using homebrew

- App Name: Notebooks
- App description: Word processor
- App Version: 2.4.3,218
- App Website: https://www.notebooksapp.com/mac/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Notebooks with the following command
   ```
   brew install --cask notebooks
   ```
4. Notebooks is ready to use now!