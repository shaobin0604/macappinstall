---
title: "Install luajit-openresty on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "OpenResty's Branch of LuaJIT 2"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install luajit-openresty on MacOS using homebrew

- App Name: luajit-openresty
- App description: OpenResty's Branch of LuaJIT 2
- App Version: 2.1-20220111
- App Website: https://github.com/openresty/luajit2

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install luajit-openresty with the following command
   ```
   brew install luajit-openresty
   ```
4. luajit-openresty is ready to use now!