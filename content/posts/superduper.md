---
title: "Install SuperDuper! on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Backup, recovery and cloning software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install SuperDuper! on MacOS using homebrew

- App Name: SuperDuper!
- App description: Backup, recovery and cloning software
- App Version: 3.5,122
- App Website: https://www.shirt-pocket.com/SuperDuper/SuperDuperDescription.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install SuperDuper! with the following command
   ```
   brew install --cask superduper
   ```
4. SuperDuper! is ready to use now!