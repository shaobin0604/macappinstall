---
title: "Install libkml on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library to parse, generate and operate on KML"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libkml on MacOS using homebrew

- App Name: libkml
- App description: Library to parse, generate and operate on KML
- App Version: 1.2.0
- App Website: https://code.google.com/archive/p/libkml/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libkml with the following command
   ```
   brew install libkml
   ```
4. libkml is ready to use now!