---
title: "Install clzip on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C language version of lzip"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install clzip on MacOS using homebrew

- App Name: clzip
- App description: C language version of lzip
- App Version: 1.13
- App Website: https://www.nongnu.org/lzip/clzip.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install clzip with the following command
   ```
   brew install clzip
   ```
4. clzip is ready to use now!