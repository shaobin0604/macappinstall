---
title: "Install insect on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "High precision scientific calculator with support for physical units"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install insect on MacOS using homebrew

- App Name: insect
- App description: High precision scientific calculator with support for physical units
- App Version: 5.6.0
- App Website: https://insect.sh/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install insect with the following command
   ```
   brew install insect
   ```
4. insect is ready to use now!