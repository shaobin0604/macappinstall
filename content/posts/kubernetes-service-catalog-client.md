---
title: "Install kubernetes-service-catalog-client on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Consume Services in k8s using the OSB API"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install kubernetes-service-catalog-client on MacOS using homebrew

- App Name: kubernetes-service-catalog-client
- App description: Consume Services in k8s using the OSB API
- App Version: 0.3.1
- App Website: https://svc-cat.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install kubernetes-service-catalog-client with the following command
   ```
   brew install kubernetes-service-catalog-client
   ```
4. kubernetes-service-catalog-client is ready to use now!