---
title: "Install re2c on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Generate C-based recognizers from regular expressions"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install re2c on MacOS using homebrew

- App Name: re2c
- App description: Generate C-based recognizers from regular expressions
- App Version: 3.0
- App Website: https://re2c.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install re2c with the following command
   ```
   brew install re2c
   ```
4. re2c is ready to use now!