---
title: "Install git-remote-gcrypt on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GPG-encrypted git remotes"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install git-remote-gcrypt on MacOS using homebrew

- App Name: git-remote-gcrypt
- App description: GPG-encrypted git remotes
- App Version: 1.4
- App Website: https://spwhitton.name/tech/code/git-remote-gcrypt/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install git-remote-gcrypt with the following command
   ```
   brew install git-remote-gcrypt
   ```
4. git-remote-gcrypt is ready to use now!