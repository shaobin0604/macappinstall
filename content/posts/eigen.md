---
title: "Install eigen on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C++ template library for linear algebra"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install eigen on MacOS using homebrew

- App Name: eigen
- App description: C++ template library for linear algebra
- App Version: 3.4.0
- App Website: https://eigen.tuxfamily.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install eigen with the following command
   ```
   brew install eigen
   ```
4. eigen is ready to use now!