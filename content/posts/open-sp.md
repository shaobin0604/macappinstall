---
title: "Install open-sp on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "SGML parser"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install open-sp on MacOS using homebrew

- App Name: open-sp
- App description: SGML parser
- App Version: 1.5.2
- App Website: https://openjade.sourceforge.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install open-sp with the following command
   ```
   brew install open-sp
   ```
4. open-sp is ready to use now!