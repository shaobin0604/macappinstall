---
title: "Install aliyun-cli on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Universal Command-Line Interface for Alibaba Cloud"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install aliyun-cli on MacOS using homebrew

- App Name: aliyun-cli
- App description: Universal Command-Line Interface for Alibaba Cloud
- App Version: 3.0.108
- App Website: https://github.com/aliyun/aliyun-cli

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install aliyun-cli with the following command
   ```
   brew install aliyun-cli
   ```
4. aliyun-cli is ready to use now!