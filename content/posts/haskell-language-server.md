---
title: "Install haskell-language-server on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Integration point for ghcide and haskell-ide-engine. One IDE to rule them all"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install haskell-language-server on MacOS using homebrew

- App Name: haskell-language-server
- App description: Integration point for ghcide and haskell-ide-engine. One IDE to rule them all
- App Version: 1.6.1.0
- App Website: https://github.com/haskell/haskell-language-server

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install haskell-language-server with the following command
   ```
   brew install haskell-language-server
   ```
4. haskell-language-server is ready to use now!