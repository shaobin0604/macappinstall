---
title: "Install libgsf on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "I/O abstraction library for dealing with structured file formats"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libgsf on MacOS using homebrew

- App Name: libgsf
- App description: I/O abstraction library for dealing with structured file formats
- App Version: 1.14.48
- App Website: https://gitlab.gnome.org/GNOME/libgsf

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libgsf with the following command
   ```
   brew install libgsf
   ```
4. libgsf is ready to use now!