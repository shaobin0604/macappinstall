---
title: "Install Cryptr on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GUI for Hashicorp's Vault"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Cryptr on MacOS using homebrew

- App Name: Cryptr
- App description: GUI for Hashicorp's Vault
- App Version: 0.5.0
- App Website: https://github.com/adobe/cryptr

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Cryptr with the following command
   ```
   brew install --cask cryptr
   ```
4. Cryptr is ready to use now!