---
title: "Install ec2-api-tools on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Client interface to the Amazon EC2 web service"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ec2-api-tools on MacOS using homebrew

- App Name: ec2-api-tools
- App description: Client interface to the Amazon EC2 web service
- App Version: 1.7.5.1
- App Website: https://aws.amazon.com/developertools/351

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ec2-api-tools with the following command
   ```
   brew install ec2-api-tools
   ```
4. ec2-api-tools is ready to use now!