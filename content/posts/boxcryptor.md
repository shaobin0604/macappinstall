---
title: "Install Boxcryptor on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to encrypt files and folders in various cloud storage services"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Boxcryptor on MacOS using homebrew

- App Name: Boxcryptor
- App description: Tool to encrypt files and folders in various cloud storage services
- App Version: 2.44.1602
- App Website: https://www.boxcryptor.com/en/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Boxcryptor with the following command
   ```
   brew install --cask boxcryptor
   ```
4. Boxcryptor is ready to use now!