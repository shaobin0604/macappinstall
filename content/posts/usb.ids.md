---
title: "Install usb.ids on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Repository of vendor, device, subsystem and device class IDs used in USB devices"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install usb.ids on MacOS using homebrew

- App Name: usb.ids
- App description: Repository of vendor, device, subsystem and device class IDs used in USB devices
- App Version: 2022.02.15
- App Website: http://www.linux-usb.org/usb-ids.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install usb.ids with the following command
   ```
   brew install usb.ids
   ```
4. usb.ids is ready to use now!