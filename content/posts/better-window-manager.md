---
title: "Install Better Window Manager on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Better Window Manager on MacOS using homebrew

- App Name: Better Window Manager
- App description: null
- App Version: 1.14,15
- App Website: https://www.gngrwzrd.com/better-window-manager/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Better Window Manager with the following command
   ```
   brew install --cask better-window-manager
   ```
4. Better Window Manager is ready to use now!