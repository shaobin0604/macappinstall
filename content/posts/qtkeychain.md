---
title: "Install qtkeychain on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Platform-independent Qt API for storing passwords securely"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install qtkeychain on MacOS using homebrew

- App Name: qtkeychain
- App description: Platform-independent Qt API for storing passwords securely
- App Version: 0.13.2
- App Website: https://github.com/frankosterfeld/qtkeychain

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install qtkeychain with the following command
   ```
   brew install qtkeychain
   ```
4. qtkeychain is ready to use now!