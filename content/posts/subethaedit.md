---
title: "Install SubEthaEdit on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Plain text and source editor"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install SubEthaEdit on MacOS using homebrew

- App Name: SubEthaEdit
- App description: Plain text and source editor
- App Version: 5.2.4,9813
- App Website: https://subethaedit.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install SubEthaEdit with the following command
   ```
   brew install --cask subethaedit
   ```
4. SubEthaEdit is ready to use now!