---
title: "Install CCtalk on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Real-time interactive education platform"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install CCtalk on MacOS using homebrew

- App Name: CCtalk
- App description: Real-time interactive education platform
- App Version: 7.9.2.6
- App Website: https://www.cctalk.com/download/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install CCtalk with the following command
   ```
   brew install --cask cctalk
   ```
4. CCtalk is ready to use now!