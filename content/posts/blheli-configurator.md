---
title: "Install BLHeli Configurator on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install BLHeli Configurator on MacOS using homebrew

- App Name: BLHeli Configurator
- App description: null
- App Version: 1.2.0
- App Website: https://github.com/blheli-configurator/blheli-configurator

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install BLHeli Configurator with the following command
   ```
   brew install --cask blheli-configurator
   ```
4. BLHeli Configurator is ready to use now!