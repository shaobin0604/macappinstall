---
title: "Install cmake-docs on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Documentation for CMake"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cmake-docs on MacOS using homebrew

- App Name: cmake-docs
- App description: Documentation for CMake
- App Version: 3.22.2
- App Website: https://www.cmake.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cmake-docs with the following command
   ```
   brew install cmake-docs
   ```
4. cmake-docs is ready to use now!