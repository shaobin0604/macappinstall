---
title: "Install Bob Wallet on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Handshake wallet GUI for name auction and DNS record management"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Bob Wallet on MacOS using homebrew

- App Name: Bob Wallet
- App description: Handshake wallet GUI for name auction and DNS record management
- App Version: 0.9.0
- App Website: https://bobwallet.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Bob Wallet with the following command
   ```
   brew install --cask kyokan-bob
   ```
4. Bob Wallet is ready to use now!