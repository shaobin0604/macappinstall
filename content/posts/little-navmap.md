---
title: "Install Little Navmap on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Flight planning and navigation and airport search and information system"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Little Navmap on MacOS using homebrew

- App Name: Little Navmap
- App description: Flight planning and navigation and airport search and information system
- App Version: 2.6.17
- App Website: https://albar965.github.io/littlenavmap.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Little Navmap with the following command
   ```
   brew install --cask little-navmap
   ```
4. Little Navmap is ready to use now!