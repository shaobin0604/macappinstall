---
title: "Install SwiftBar on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Menu bar customization tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install SwiftBar on MacOS using homebrew

- App Name: SwiftBar
- App description: Menu bar customization tool
- App Version: 1.4.2
- App Website: https://swiftbar.app/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install SwiftBar with the following command
   ```
   brew install --cask swiftbar
   ```
4. SwiftBar is ready to use now!