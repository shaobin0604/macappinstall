---
title: "Install ddgr on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "DuckDuckGo from the terminal"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ddgr on MacOS using homebrew

- App Name: ddgr
- App description: DuckDuckGo from the terminal
- App Version: 1.9
- App Website: https://github.com/jarun/ddgr

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ddgr with the following command
   ```
   brew install ddgr
   ```
4. ddgr is ready to use now!