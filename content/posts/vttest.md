---
title: "Install vttest on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Test compatibility of VT100-compatible terminals"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install vttest on MacOS using homebrew

- App Name: vttest
- App description: Test compatibility of VT100-compatible terminals
- App Version: 20220215
- App Website: https://invisible-island.net/vttest/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install vttest with the following command
   ```
   brew install vttest
   ```
4. vttest is ready to use now!