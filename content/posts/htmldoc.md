---
title: "Install htmldoc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Convert HTML to PDF or PostScript"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install htmldoc on MacOS using homebrew

- App Name: htmldoc
- App description: Convert HTML to PDF or PostScript
- App Version: 1.9.15
- App Website: https://www.msweet.org/htmldoc/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install htmldoc with the following command
   ```
   brew install htmldoc
   ```
4. htmldoc is ready to use now!