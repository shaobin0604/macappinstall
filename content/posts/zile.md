---
title: "Install zile on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Text editor development kit"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install zile on MacOS using homebrew

- App Name: zile
- App description: Text editor development kit
- App Version: 2.6.2
- App Website: https://www.gnu.org/software/zile/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install zile with the following command
   ```
   brew install zile
   ```
4. zile is ready to use now!