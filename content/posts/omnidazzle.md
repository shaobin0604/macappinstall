---
title: "Install OmniDazzle on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Set of plug-ins to highlight areas of your screen and your mouse pointer"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install OmniDazzle on MacOS using homebrew

- App Name: OmniDazzle
- App description: Set of plug-ins to highlight areas of your screen and your mouse pointer
- App Version: 1.2
- App Website: https://support.omnigroup.com/omnidazzle-troubleshooting/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install OmniDazzle with the following command
   ```
   brew install --cask omnidazzle
   ```
4. OmniDazzle is ready to use now!