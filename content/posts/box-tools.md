---
title: "Install Box Tools on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Create and edit any file directly from a web browser"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Box Tools on MacOS using homebrew

- App Name: Box Tools
- App description: Create and edit any file directly from a web browser
- App Version: latest
- App Website: https://www.box.com/resources/downloads

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Box Tools with the following command
   ```
   brew install --cask box-tools
   ```
4. Box Tools is ready to use now!