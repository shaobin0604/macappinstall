---
title: "Install DbGate on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Database manager for MySQL, PostgreSQL, SQL Server, MongoDB, SQLite and others"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install DbGate on MacOS using homebrew

- App Name: DbGate
- App description: Database manager for MySQL, PostgreSQL, SQL Server, MongoDB, SQLite and others
- App Version: 4.6.3
- App Website: https://dbgate.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install DbGate with the following command
   ```
   brew install --cask dbgate
   ```
4. DbGate is ready to use now!