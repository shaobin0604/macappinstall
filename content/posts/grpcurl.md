---
title: "Install grpcurl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Like cURL, but for gRPC"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install grpcurl on MacOS using homebrew

- App Name: grpcurl
- App description: Like cURL, but for gRPC
- App Version: 1.8.6
- App Website: https://github.com/fullstorydev/grpcurl

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install grpcurl with the following command
   ```
   brew install grpcurl
   ```
4. grpcurl is ready to use now!