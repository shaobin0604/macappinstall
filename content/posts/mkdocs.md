---
title: "Install mkdocs on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Project documentation with Markdown"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mkdocs on MacOS using homebrew

- App Name: mkdocs
- App description: Project documentation with Markdown
- App Version: 1.2.3
- App Website: https://www.mkdocs.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mkdocs with the following command
   ```
   brew install mkdocs
   ```
4. mkdocs is ready to use now!