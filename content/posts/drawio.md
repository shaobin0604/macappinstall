---
title: "Install draw.io Desktop on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Draw.io is free online diagram software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install draw.io Desktop on MacOS using homebrew

- App Name: draw.io Desktop
- App description: Draw.io is free online diagram software
- App Version: 16.5.1
- App Website: https://www.diagrams.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install draw.io Desktop with the following command
   ```
   brew install --cask drawio
   ```
4. draw.io Desktop is ready to use now!