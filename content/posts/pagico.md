---
title: "Install Pagico on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Manage all your tasks, files, and notes"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Pagico on MacOS using homebrew

- App Name: Pagico
- App description: Manage all your tasks, files, and notes
- App Version: 10,20220107
- App Website: https://www.pagico.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Pagico with the following command
   ```
   brew install --cask pagico
   ```
4. Pagico is ready to use now!