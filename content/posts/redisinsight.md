---
title: "Install RedisInsight on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GUI for streamlined Redis application development"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install RedisInsight on MacOS using homebrew

- App Name: RedisInsight
- App description: GUI for streamlined Redis application development
- App Version: 1.11.0
- App Website: https://www.redislabs.com/redisinsight/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install RedisInsight with the following command
   ```
   brew install --cask redisinsight
   ```
4. RedisInsight is ready to use now!