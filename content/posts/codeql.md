---
title: "Install CodeQL on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Semantic code analysis engine"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install CodeQL on MacOS using homebrew

- App Name: CodeQL
- App description: Semantic code analysis engine
- App Version: 2.8.0
- App Website: https://codeql.github.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install CodeQL with the following command
   ```
   brew install --cask codeql
   ```
4. CodeQL is ready to use now!