---
title: "Install gradle-profiler on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Profiling and benchmarking tool for Gradle builds"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gradle-profiler on MacOS using homebrew

- App Name: gradle-profiler
- App description: Profiling and benchmarking tool for Gradle builds
- App Version: 0.17.0
- App Website: https://github.com/gradle/gradle-profiler/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gradle-profiler with the following command
   ```
   brew install gradle-profiler
   ```
4. gradle-profiler is ready to use now!