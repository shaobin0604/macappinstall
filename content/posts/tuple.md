---
title: "Install Tuple on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Remote pair programming app"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Tuple on MacOS using homebrew

- App Name: Tuple
- App description: Remote pair programming app
- App Version: 0.93.0,2022-01-06,81f1eeba
- App Website: https://tuple.app/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Tuple with the following command
   ```
   brew install --cask tuple
   ```
4. Tuple is ready to use now!