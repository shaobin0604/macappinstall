---
title: "Install needle on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Compile-time safe Swift dependency injection framework with real code"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install needle on MacOS using homebrew

- App Name: needle
- App description: Compile-time safe Swift dependency injection framework with real code
- App Version: 0.17.2
- App Website: https://github.com/uber/needle

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install needle with the following command
   ```
   brew install needle
   ```
4. needle is ready to use now!