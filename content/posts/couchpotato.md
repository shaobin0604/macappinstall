---
title: "Install CouchPotato on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Automatic Movie Downloading via NZBs & Torrents"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install CouchPotato on MacOS using homebrew

- App Name: CouchPotato
- App description: Automatic Movie Downloading via NZBs & Torrents
- App Version: 3.0.1
- App Website: https://couchpota.to/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install CouchPotato with the following command
   ```
   brew install --cask couchpotato
   ```
4. CouchPotato is ready to use now!