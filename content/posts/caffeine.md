---
title: "Install Caffeine on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utility that prevents the system from going to sleep"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Caffeine on MacOS using homebrew

- App Name: Caffeine
- App description: Utility that prevents the system from going to sleep
- App Version: 1.1.3
- App Website: https://intelliscapesolutions.com/apps/caffeine

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Caffeine with the following command
   ```
   brew install --cask caffeine
   ```
4. Caffeine is ready to use now!