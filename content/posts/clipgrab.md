---
title: "Install ClipGrab on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Downloads videos and audio from websites"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ClipGrab on MacOS using homebrew

- App Name: ClipGrab
- App description: Downloads videos and audio from websites
- App Version: 3.9.7,1010
- App Website: https://clipgrab.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ClipGrab with the following command
   ```
   brew install --cask clipgrab
   ```
4. ClipGrab is ready to use now!