---
title: "Install QQ影音 on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Media player"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install QQ影音 on MacOS using homebrew

- App Name: QQ影音
- App description: Media player
- App Version: 1.1.1.1208
- App Website: https://player.qq.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install QQ影音 with the following command
   ```
   brew install --cask qqplayer
   ```
4. QQ影音 is ready to use now!