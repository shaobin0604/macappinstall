---
title: "Install PopClip on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Used to access context-specific actions when text is selected"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install PopClip on MacOS using homebrew

- App Name: PopClip
- App description: Used to access context-specific actions when text is selected
- App Version: 2021.11.1,3788
- App Website: https://pilotmoon.com/popclip/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install PopClip with the following command
   ```
   brew install --cask popclip
   ```
4. PopClip is ready to use now!