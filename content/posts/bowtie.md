---
title: "Install Bowtie on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Control your music with customizable shortcuts"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Bowtie on MacOS using homebrew

- App Name: Bowtie
- App description: Control your music with customizable shortcuts
- App Version: 1.5,1500
- App Website: http://bowtieapp.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Bowtie with the following command
   ```
   brew install --cask bowtie
   ```
4. Bowtie is ready to use now!