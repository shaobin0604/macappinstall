---
title: "Install libmpd on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Higher level access to MPD functions"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libmpd on MacOS using homebrew

- App Name: libmpd
- App description: Higher level access to MPD functions
- App Version: 11.8.17
- App Website: https://gmpc.fandom.com/wiki/Gnome_Music_Player_Client

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libmpd with the following command
   ```
   brew install libmpd
   ```
4. libmpd is ready to use now!