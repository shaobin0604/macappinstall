---
title: "Install screenfetch on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Generate ASCII art with terminal, shell, and OS info"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install screenfetch on MacOS using homebrew

- App Name: screenfetch
- App description: Generate ASCII art with terminal, shell, and OS info
- App Version: 3.9.1
- App Website: https://github.com/KittyKatt/screenFetch

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install screenfetch with the following command
   ```
   brew install screenfetch
   ```
4. screenfetch is ready to use now!