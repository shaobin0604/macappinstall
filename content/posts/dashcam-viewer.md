---
title: "Install Dashcam Viewer on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "View videos, GPS data, and G-force data recorded by dashcams and action cams"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Dashcam Viewer on MacOS using homebrew

- App Name: Dashcam Viewer
- App description: View videos, GPS data, and G-force data recorded by dashcams and action cams
- App Version: 3.8.1
- App Website: https://dashcamviewer.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Dashcam Viewer with the following command
   ```
   brew install --cask dashcam-viewer
   ```
4. Dashcam Viewer is ready to use now!