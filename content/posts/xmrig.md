---
title: "Install xmrig on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Monero (XMR) CPU miner"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install xmrig on MacOS using homebrew

- App Name: xmrig
- App description: Monero (XMR) CPU miner
- App Version: 6.15.3
- App Website: https://github.com/xmrig/xmrig

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install xmrig with the following command
   ```
   brew install xmrig
   ```
4. xmrig is ready to use now!