---
title: "Install Boostnote.Next on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Markdown note editor for developers"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Boostnote.Next on MacOS using homebrew

- App Name: Boostnote.Next
- App description: Markdown note editor for developers
- App Version: 0.23.1
- App Website: https://boostnote.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Boostnote.Next with the following command
   ```
   brew install --cask boost-note
   ```
4. Boostnote.Next is ready to use now!