---
title: "Install Berrycast on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Screen recorder"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Berrycast on MacOS using homebrew

- App Name: Berrycast
- App description: Screen recorder
- App Version: 0.35.9
- App Website: https://www.berrycast.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Berrycast with the following command
   ```
   brew install --cask berrycast
   ```
4. Berrycast is ready to use now!