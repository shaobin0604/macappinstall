---
title: "Install gotop on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Terminal based graphical activity monitor inspired by gtop and vtop"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gotop on MacOS using homebrew

- App Name: gotop
- App description: Terminal based graphical activity monitor inspired by gtop and vtop
- App Version: 4.1.3
- App Website: https://github.com/xxxserxxx/gotop

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gotop with the following command
   ```
   brew install gotop
   ```
4. gotop is ready to use now!