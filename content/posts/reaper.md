---
title: "Install REAPER on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Digital audio production application"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install REAPER on MacOS using homebrew

- App Name: REAPER
- App description: Digital audio production application
- App Version: 6.47
- App Website: https://www.reaper.fm/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install REAPER with the following command
   ```
   brew install --cask reaper
   ```
4. REAPER is ready to use now!