---
title: "Install bosh-cli on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cloud Foundry BOSH CLI v2"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bosh-cli on MacOS using homebrew

- App Name: bosh-cli
- App description: Cloud Foundry BOSH CLI v2
- App Version: 6.4.16
- App Website: https://bosh.io/docs/cli-v2/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bosh-cli with the following command
   ```
   brew install bosh-cli
   ```
4. bosh-cli is ready to use now!