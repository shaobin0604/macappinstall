---
title: "Install Sengi on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Mastodon and Pleroma desktop client"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Sengi on MacOS using homebrew

- App Name: Sengi
- App description: Mastodon and Pleroma desktop client
- App Version: 1.1.5
- App Website: https://github.com/NicolasConstant/sengi

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Sengi with the following command
   ```
   brew install --cask sengi
   ```
4. Sengi is ready to use now!