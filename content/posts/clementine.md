---
title: "Install Clementine on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Music player and library organizer"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Clementine on MacOS using homebrew

- App Name: Clementine
- App description: Music player and library organizer
- App Version: 1.3.1
- App Website: https://www.clementine-player.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Clementine with the following command
   ```
   brew install --cask clementine
   ```
4. Clementine is ready to use now!