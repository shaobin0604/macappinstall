---
title: "Install thefuck on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Programmatically correct mistyped console commands"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install thefuck on MacOS using homebrew

- App Name: thefuck
- App description: Programmatically correct mistyped console commands
- App Version: 3.32
- App Website: https://github.com/nvbn/thefuck

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install thefuck with the following command
   ```
   brew install thefuck
   ```
4. thefuck is ready to use now!