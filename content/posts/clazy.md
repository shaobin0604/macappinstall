---
title: "Install clazy on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Qt oriented static code analyzer"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install clazy on MacOS using homebrew

- App Name: clazy
- App description: Qt oriented static code analyzer
- App Version: 1.11
- App Website: https://www.kdab.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install clazy with the following command
   ```
   brew install clazy
   ```
4. clazy is ready to use now!