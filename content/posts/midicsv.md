---
title: "Install midicsv on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Convert MIDI audio files to human-readable CSV format"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install midicsv on MacOS using homebrew

- App Name: midicsv
- App description: Convert MIDI audio files to human-readable CSV format
- App Version: 1.1
- App Website: https://www.fourmilab.ch/webtools/midicsv/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install midicsv with the following command
   ```
   brew install midicsv
   ```
4. midicsv is ready to use now!