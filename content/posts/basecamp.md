---
title: "Install Basecamp on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "All-In-One Toolkit for Working Remotely"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Basecamp on MacOS using homebrew

- App Name: Basecamp
- App description: All-In-One Toolkit for Working Remotely
- App Version: 3,2.2.9
- App Website: https://basecamp.com/help/3,2.2.9/guides/apps/mac

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Basecamp with the following command
   ```
   brew install --cask basecamp
   ```
4. Basecamp is ready to use now!