---
title: "Install Cool Retro Term on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Terminal emulator mimicking the old cathode display"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Cool Retro Term on MacOS using homebrew

- App Name: Cool Retro Term
- App description: Terminal emulator mimicking the old cathode display
- App Version: 1.2.0
- App Website: https://github.com/Swordfish90/cool-retro-term

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Cool Retro Term with the following command
   ```
   brew install --cask cool-retro-term
   ```
4. Cool Retro Term is ready to use now!