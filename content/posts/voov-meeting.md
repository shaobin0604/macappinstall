---
title: "Install VooV Meeting on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cross-border video conferencing software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install VooV Meeting on MacOS using homebrew

- App Name: VooV Meeting
- App description: Cross-border video conferencing software
- App Version: 1.7.1.510,1410000198
- App Website: https://voovmeeting.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install VooV Meeting with the following command
   ```
   brew install --cask voov-meeting
   ```
4. VooV Meeting is ready to use now!