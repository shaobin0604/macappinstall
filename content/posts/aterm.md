---
title: "Install aterm on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Annotated Term for tree-like ADT exchange"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install aterm on MacOS using homebrew

- App Name: aterm
- App description: Annotated Term for tree-like ADT exchange
- App Version: 2.8
- App Website: https://web.archive.org/web/20180902175600/meta-environment.org/Meta-Environment/ATerms.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install aterm with the following command
   ```
   brew install aterm
   ```
4. aterm is ready to use now!