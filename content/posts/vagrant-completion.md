---
title: "Install vagrant-completion on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Bash completion for Vagrant"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install vagrant-completion on MacOS using homebrew

- App Name: vagrant-completion
- App description: Bash completion for Vagrant
- App Version: 2.2.19
- App Website: https://github.com/hashicorp/vagrant

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install vagrant-completion with the following command
   ```
   brew install vagrant-completion
   ```
4. vagrant-completion is ready to use now!