---
title: "Install bzr-colo on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Git style colocated branches for Bazaar"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bzr-colo on MacOS using homebrew

- App Name: bzr-colo
- App description: Git style colocated branches for Bazaar
- App Version: 0.4.0
- App Website: https://launchpad.net/bzr-colo

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bzr-colo with the following command
   ```
   brew install bzr-colo
   ```
4. bzr-colo is ready to use now!