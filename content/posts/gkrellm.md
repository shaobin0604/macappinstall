---
title: "Install gkrellm on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Extensible GTK system monitoring application"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gkrellm on MacOS using homebrew

- App Name: gkrellm
- App description: Extensible GTK system monitoring application
- App Version: 2.3.11
- App Website: https://billw2.github.io/gkrellm/gkrellm.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gkrellm with the following command
   ```
   brew install gkrellm
   ```
4. gkrellm is ready to use now!