---
title: "Install GraphicConverter on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "For browsing, enhancing and converting images"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install GraphicConverter on MacOS using homebrew

- App Name: GraphicConverter
- App description: For browsing, enhancing and converting images
- App Version: 11.6,5393
- App Website: https://www.lemkesoft.de/en/products/graphicconverter/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install GraphicConverter with the following command
   ```
   brew install --cask graphicconverter
   ```
4. GraphicConverter is ready to use now!