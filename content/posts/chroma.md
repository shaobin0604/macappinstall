---
title: "Install chroma on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "General purpose syntax highlighter in pure Go"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install chroma on MacOS using homebrew

- App Name: chroma
- App description: General purpose syntax highlighter in pure Go
- App Version: 0.10.0
- App Website: https://github.com/alecthomas/chroma

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install chroma with the following command
   ```
   brew install chroma
   ```
4. chroma is ready to use now!