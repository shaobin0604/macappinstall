---
title: "Install stella on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Atari 2600 VCS emulator"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install stella on MacOS using homebrew

- App Name: stella
- App description: Atari 2600 VCS emulator
- App Version: 6.6
- App Website: https://stella-emu.github.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install stella with the following command
   ```
   brew install stella
   ```
4. stella is ready to use now!