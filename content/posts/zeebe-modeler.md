---
title: "Install Zeebe Modeler on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Desktop Application for modeling Zeebe Workflows with BPMN"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Zeebe Modeler on MacOS using homebrew

- App Name: Zeebe Modeler
- App description: Desktop Application for modeling Zeebe Workflows with BPMN
- App Version: 0.11.0
- App Website: https://zeebe.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Zeebe Modeler with the following command
   ```
   brew install --cask zeebe-modeler
   ```
4. Zeebe Modeler is ready to use now!