---
title: "Install mpssh on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Mass parallel ssh"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mpssh on MacOS using homebrew

- App Name: mpssh
- App description: Mass parallel ssh
- App Version: 1.3.3
- App Website: https://github.com/ndenev/mpssh

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mpssh with the following command
   ```
   brew install mpssh
   ```
4. mpssh is ready to use now!