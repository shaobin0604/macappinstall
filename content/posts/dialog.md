---
title: "Install dialog on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Display user-friendly message boxes from shell scripts"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dialog on MacOS using homebrew

- App Name: dialog
- App description: Display user-friendly message boxes from shell scripts
- App Version: 1.3-20220117
- App Website: https://invisible-island.net/dialog/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dialog with the following command
   ```
   brew install dialog
   ```
4. dialog is ready to use now!