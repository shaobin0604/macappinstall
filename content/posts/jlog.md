---
title: "Install jlog on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Pure C message queue with subscribers and publishers for logs"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install jlog on MacOS using homebrew

- App Name: jlog
- App description: Pure C message queue with subscribers and publishers for logs
- App Version: 2.5.3
- App Website: https://labs.omniti.com/labs/jlog

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install jlog with the following command
   ```
   brew install jlog
   ```
4. jlog is ready to use now!