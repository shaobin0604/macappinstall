---
title: "Install gzip on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Popular GNU data compression program"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gzip on MacOS using homebrew

- App Name: gzip
- App description: Popular GNU data compression program
- App Version: 1.11
- App Website: https://www.gnu.org/software/gzip

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gzip with the following command
   ```
   brew install gzip
   ```
4. gzip is ready to use now!