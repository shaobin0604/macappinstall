---
title: "Install eye-d3 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Work with ID3 metadata in .mp3 files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install eye-d3 on MacOS using homebrew

- App Name: eye-d3
- App description: Work with ID3 metadata in .mp3 files
- App Version: 0.9.6
- App Website: https://eyed3.nicfit.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install eye-d3 with the following command
   ```
   brew install eye-d3
   ```
4. eye-d3 is ready to use now!