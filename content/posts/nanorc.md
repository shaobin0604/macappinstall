---
title: "Install nanorc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Improved Nano Syntax Highlighting Files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install nanorc on MacOS using homebrew

- App Name: nanorc
- App description: Improved Nano Syntax Highlighting Files
- App Version: 2020.10.10
- App Website: https://github.com/scopatz/nanorc

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install nanorc with the following command
   ```
   brew install nanorc
   ```
4. nanorc is ready to use now!