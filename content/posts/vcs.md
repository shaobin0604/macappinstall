---
title: "Install vcs on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Creates video contact sheets (previews) of videos"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install vcs on MacOS using homebrew

- App Name: vcs
- App description: Creates video contact sheets (previews) of videos
- App Version: 1.13.4
- App Website: https://p.outlyer.net/vcs/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install vcs with the following command
   ```
   brew install vcs
   ```
4. vcs is ready to use now!