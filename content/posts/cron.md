---
title: "Install Cron Calendar on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Calendar for professionals and teams"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Cron Calendar on MacOS using homebrew

- App Name: Cron Calendar
- App description: Calendar for professionals and teams
- App Version: 1.108.0
- App Website: https://cron.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Cron Calendar with the following command
   ```
   brew install --cask cron
   ```
4. Cron Calendar is ready to use now!