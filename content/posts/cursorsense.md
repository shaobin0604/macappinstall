---
title: "Install CursorSense on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Adjusts cursor acceleration and sensitivity"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install CursorSense on MacOS using homebrew

- App Name: CursorSense
- App description: Adjusts cursor acceleration and sensitivity
- App Version: 2.3
- App Website: https://plentycom.jp/en/cursorsense/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install CursorSense with the following command
   ```
   brew install --cask cursorsense
   ```
4. CursorSense is ready to use now!