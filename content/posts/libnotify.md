---
title: "Install libnotify on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library that sends desktop notifications to a notification daemon"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libnotify on MacOS using homebrew

- App Name: libnotify
- App description: Library that sends desktop notifications to a notification daemon
- App Version: 0.7.9
- App Website: https://gitlab.gnome.org/GNOME/libnotify

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libnotify with the following command
   ```
   brew install libnotify
   ```
4. libnotify is ready to use now!