---
title: "Install lm4tools on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tools for TI Stellaris Launchpad boards"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install lm4tools on MacOS using homebrew

- App Name: lm4tools
- App description: Tools for TI Stellaris Launchpad boards
- App Version: 0.1.3
- App Website: https://github.com/utzig/lm4tools

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install lm4tools with the following command
   ```
   brew install lm4tools
   ```
4. lm4tools is ready to use now!