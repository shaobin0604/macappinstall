---
title: "Install awscli on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Official Amazon AWS command-line interface"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install awscli on MacOS using homebrew

- App Name: awscli
- App description: Official Amazon AWS command-line interface
- App Version: 2.4.18
- App Website: https://aws.amazon.com/cli/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install awscli with the following command
   ```
   brew install awscli
   ```
4. awscli is ready to use now!