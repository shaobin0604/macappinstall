---
title: "Install NordPass on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Store credentials in a single place and log in on any device, even when offline"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install NordPass on MacOS using homebrew

- App Name: NordPass
- App description: Store credentials in a single place and log in on any device, even when offline
- App Version: 4.16.22
- App Website: https://nordpass.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install NordPass with the following command
   ```
   brew install --cask nordpass
   ```
4. NordPass is ready to use now!