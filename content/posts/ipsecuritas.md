---
title: "Install IPSecuritas on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "IPSec client"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install IPSecuritas on MacOS using homebrew

- App Name: IPSecuritas
- App description: IPSec client
- App Version: 5.0.1
- App Website: https://www.lobotomo.com/products/IPSecuritas/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install IPSecuritas with the following command
   ```
   brew install --cask ipsecuritas
   ```
4. IPSecuritas is ready to use now!