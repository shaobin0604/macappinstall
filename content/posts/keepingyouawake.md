---
title: "Install KeepingYouAwake on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to prevent the system from going into sleep mode"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install KeepingYouAwake on MacOS using homebrew

- App Name: KeepingYouAwake
- App description: Tool to prevent the system from going into sleep mode
- App Version: 1.6.1
- App Website: https://keepingyouawake.app/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install KeepingYouAwake with the following command
   ```
   brew install --cask keepingyouawake
   ```
4. KeepingYouAwake is ready to use now!