---
title: "Install uutils-coreutils on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cross-platform Rust rewrite of the GNU coreutils"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install uutils-coreutils on MacOS using homebrew

- App Name: uutils-coreutils
- App description: Cross-platform Rust rewrite of the GNU coreutils
- App Version: 0.0.12
- App Website: https://github.com/uutils/coreutils

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install uutils-coreutils with the following command
   ```
   brew install uutils-coreutils
   ```
4. uutils-coreutils is ready to use now!