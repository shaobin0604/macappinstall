---
title: "Install unicorn on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Lightweight multi-architecture CPU emulation framework"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install unicorn on MacOS using homebrew

- App Name: unicorn
- App description: Lightweight multi-architecture CPU emulation framework
- App Version: 1.0.3
- App Website: https://www.unicorn-engine.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install unicorn with the following command
   ```
   brew install unicorn
   ```
4. unicorn is ready to use now!