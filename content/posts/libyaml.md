---
title: "Install libyaml on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "YAML Parser"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libyaml on MacOS using homebrew

- App Name: libyaml
- App description: YAML Parser
- App Version: 0.2.5
- App Website: https://github.com/yaml/libyaml

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libyaml with the following command
   ```
   brew install libyaml
   ```
4. libyaml is ready to use now!