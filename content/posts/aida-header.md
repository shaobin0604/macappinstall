---
title: "Install aida-header on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Abstract Interfaces for Data Analysis define interfaces for physics analysis"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install aida-header on MacOS using homebrew

- App Name: aida-header
- App description: Abstract Interfaces for Data Analysis define interfaces for physics analysis
- App Version: 3.2.1
- App Website: https://aida.freehep.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install aida-header with the following command
   ```
   brew install aida-header
   ```
4. aida-header is ready to use now!