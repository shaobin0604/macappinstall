---
title: "Install Macgo Mac Blu-ray Player Pro on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Blu-ray player software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Macgo Mac Blu-ray Player Pro on MacOS using homebrew

- App Name: Macgo Mac Blu-ray Player Pro
- App description: Blu-ray player software
- App Version: 3.3.21,211028_0110
- App Website: https://www.macblurayplayer.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Macgo Mac Blu-ray Player Pro with the following command
   ```
   brew install --cask blu-ray-player-pro
   ```
4. Macgo Mac Blu-ray Player Pro is ready to use now!