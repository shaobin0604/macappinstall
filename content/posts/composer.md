---
title: "Install composer on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Dependency Manager for PHP"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install composer on MacOS using homebrew

- App Name: composer
- App description: Dependency Manager for PHP
- App Version: 2.2.6
- App Website: https://getcomposer.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install composer with the following command
   ```
   brew install composer
   ```
4. composer is ready to use now!