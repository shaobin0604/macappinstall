---
title: "Install jfrog-cli on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line interface for JFrog products"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install jfrog-cli on MacOS using homebrew

- App Name: jfrog-cli
- App description: Command-line interface for JFrog products
- App Version: 2.12.1
- App Website: https://www.jfrog.com/confluence/display/CLI/JFrog+CLI

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install jfrog-cli with the following command
   ```
   brew install jfrog-cli
   ```
4. jfrog-cli is ready to use now!