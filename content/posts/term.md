---
title: "Install term on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Open terminal in specified directory (and optionally run command)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install term on MacOS using homebrew

- App Name: term
- App description: Open terminal in specified directory (and optionally run command)
- App Version: 2.1
- App Website: https://github.com/liyanage/macosx-shell-scripts/blob/HEAD/term

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install term with the following command
   ```
   brew install term
   ```
4. term is ready to use now!