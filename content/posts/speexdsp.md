---
title: "Install speexdsp on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Speex audio processing library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install speexdsp on MacOS using homebrew

- App Name: speexdsp
- App description: Speex audio processing library
- App Version: 1.2.0
- App Website: https://github.com/xiph/speexdsp

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install speexdsp with the following command
   ```
   brew install speexdsp
   ```
4. speexdsp is ready to use now!