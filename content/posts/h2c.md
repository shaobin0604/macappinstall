---
title: "Install h2c on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Headers 2 curl"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install h2c on MacOS using homebrew

- App Name: h2c
- App description: Headers 2 curl
- App Version: 1.0
- App Website: https://curl.se/h2c/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install h2c with the following command
   ```
   brew install h2c
   ```
4. h2c is ready to use now!