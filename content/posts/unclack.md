---
title: "Install Unclack on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Mutes your keyboard while you type"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Unclack on MacOS using homebrew

- App Name: Unclack
- App description: Mutes your keyboard while you type
- App Version: 1.2.0
- App Website: https://unclack.app/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Unclack with the following command
   ```
   brew install --cask unclack
   ```
4. Unclack is ready to use now!