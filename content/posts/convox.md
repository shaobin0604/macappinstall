---
title: "Install convox on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line interface for the Convox PaaS"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install convox on MacOS using homebrew

- App Name: convox
- App description: Command-line interface for the Convox PaaS
- App Version: 3.3.3
- App Website: https://convox.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install convox with the following command
   ```
   brew install convox
   ```
4. convox is ready to use now!