---
title: "Install yj on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CLI to convert between YAML, TOML, JSON and HCL"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install yj on MacOS using homebrew

- App Name: yj
- App description: CLI to convert between YAML, TOML, JSON and HCL
- App Version: 5.0.0
- App Website: https://github.com/sclevine/yj

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install yj with the following command
   ```
   brew install yj
   ```
4. yj is ready to use now!