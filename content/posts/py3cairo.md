---
title: "Install py3cairo on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Python 3 bindings for the Cairo graphics library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install py3cairo on MacOS using homebrew

- App Name: py3cairo
- App description: Python 3 bindings for the Cairo graphics library
- App Version: 1.20.1
- App Website: https://cairographics.org/pycairo/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install py3cairo with the following command
   ```
   brew install py3cairo
   ```
4. py3cairo is ready to use now!