---
title: "Install connect on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Provides SOCKS and HTTPS proxy support to SSH"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install connect on MacOS using homebrew

- App Name: connect
- App description: Provides SOCKS and HTTPS proxy support to SSH
- App Version: 1.105
- App Website: https://github.com/gotoh/ssh-connect

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install connect with the following command
   ```
   brew install connect
   ```
4. connect is ready to use now!