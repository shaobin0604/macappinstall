---
title: "Install cassandra-reaper on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Management interface for Cassandra"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cassandra-reaper on MacOS using homebrew

- App Name: cassandra-reaper
- App description: Management interface for Cassandra
- App Version: 3.1.1
- App Website: https://cassandra-reaper.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cassandra-reaper with the following command
   ```
   brew install cassandra-reaper
   ```
4. cassandra-reaper is ready to use now!