---
title: "Install gtmess on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Console MSN messenger client"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gtmess on MacOS using homebrew

- App Name: gtmess
- App description: Console MSN messenger client
- App Version: 0.97
- App Website: https://gtmess.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gtmess with the following command
   ```
   brew install gtmess
   ```
4. gtmess is ready to use now!