---
title: "Install aspectj on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Aspect-oriented programming for Java"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install aspectj on MacOS using homebrew

- App Name: aspectj
- App description: Aspect-oriented programming for Java
- App Version: 1.9.7
- App Website: https://www.eclipse.org/aspectj/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install aspectj with the following command
   ```
   brew install aspectj
   ```
4. aspectj is ready to use now!