---
title: "Install QuickHue on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Menu bar utility for controlling the Philips Hue lighting system"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install QuickHue on MacOS using homebrew

- App Name: QuickHue
- App description: Menu bar utility for controlling the Philips Hue lighting system
- App Version: 1.3.3
- App Website: https://github.com/danparsons/QuickHue

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install QuickHue with the following command
   ```
   brew install --cask quickhue
   ```
4. QuickHue is ready to use now!