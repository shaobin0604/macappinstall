---
title: "Install ctop on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Top-like interface for container metrics"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ctop on MacOS using homebrew

- App Name: ctop
- App description: Top-like interface for container metrics
- App Version: 0.7.6
- App Website: https://bcicen.github.io/ctop/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ctop with the following command
   ```
   brew install ctop
   ```
4. ctop is ready to use now!