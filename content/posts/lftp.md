---
title: "Install lftp on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Sophisticated file transfer program"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install lftp on MacOS using homebrew

- App Name: lftp
- App description: Sophisticated file transfer program
- App Version: 4.9.2
- App Website: https://lftp.yar.ru/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install lftp with the following command
   ```
   brew install lftp
   ```
4. lftp is ready to use now!