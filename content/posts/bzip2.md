---
title: "Install bzip2 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Freely available high-quality data compressor"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bzip2 on MacOS using homebrew

- App Name: bzip2
- App description: Freely available high-quality data compressor
- App Version: 1.0.8
- App Website: https://sourceware.org/bzip2/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bzip2 with the following command
   ```
   brew install bzip2
   ```
4. bzip2 is ready to use now!