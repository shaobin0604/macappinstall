---
title: "Install Pretzel on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "DMCA-safe music for creators"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Pretzel on MacOS using homebrew

- App Name: Pretzel
- App description: DMCA-safe music for creators
- App Version: 2.9.7
- App Website: https://www.pretzel.rocks/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Pretzel with the following command
   ```
   brew install --cask pretzel
   ```
4. Pretzel is ready to use now!