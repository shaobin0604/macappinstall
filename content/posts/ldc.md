---
title: "Install ldc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Portable D programming language compiler"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ldc on MacOS using homebrew

- App Name: ldc
- App description: Portable D programming language compiler
- App Version: 1.28.1
- App Website: https://wiki.dlang.org/LDC

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ldc with the following command
   ```
   brew install ldc
   ```
4. ldc is ready to use now!