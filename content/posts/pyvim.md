---
title: "Install pyvim on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Pure Python Vim clone"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pyvim on MacOS using homebrew

- App Name: pyvim
- App description: Pure Python Vim clone
- App Version: 3.0.2
- App Website: https://github.com/jonathanslenders/pyvim

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pyvim with the following command
   ```
   brew install pyvim
   ```
4. pyvim is ready to use now!