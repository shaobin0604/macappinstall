---
title: "Install broot on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "New way to see and navigate directory trees"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install broot on MacOS using homebrew

- App Name: broot
- App description: New way to see and navigate directory trees
- App Version: 1.9.3
- App Website: https://dystroy.org/broot/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install broot with the following command
   ```
   brew install broot
   ```
4. broot is ready to use now!