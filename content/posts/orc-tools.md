---
title: "Install orc-tools on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "ORC java command-line tools and utilities"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install orc-tools on MacOS using homebrew

- App Name: orc-tools
- App description: ORC java command-line tools and utilities
- App Version: 1.7.3
- App Website: https://orc.apache.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install orc-tools with the following command
   ```
   brew install orc-tools
   ```
4. orc-tools is ready to use now!