---
title: "Install LuLu on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Open-source firewall to block unknown outgoing connections"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install LuLu on MacOS using homebrew

- App Name: LuLu
- App description: Open-source firewall to block unknown outgoing connections
- App Version: 2.4.1
- App Website: https://objective-see.com/products/lulu.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install LuLu with the following command
   ```
   brew install --cask lulu
   ```
4. LuLu is ready to use now!