---
title: "Install Thonny on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Python IDE for beginners"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Thonny on MacOS using homebrew

- App Name: Thonny
- App description: Python IDE for beginners
- App Version: 3.3.13
- App Website: https://thonny.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Thonny with the following command
   ```
   brew install --cask thonny
   ```
4. Thonny is ready to use now!