---
title: "Install fleet-cli on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Manage large fleets of Kubernetes clusters"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fleet-cli on MacOS using homebrew

- App Name: fleet-cli
- App description: Manage large fleets of Kubernetes clusters
- App Version: 0.3.8
- App Website: https://github.com/rancher/fleet

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fleet-cli with the following command
   ```
   brew install fleet-cli
   ```
4. fleet-cli is ready to use now!