---
title: "Install stk on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Sound Synthesis Toolkit"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install stk on MacOS using homebrew

- App Name: stk
- App description: Sound Synthesis Toolkit
- App Version: 4.6.2
- App Website: https://ccrma.stanford.edu/software/stk/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install stk with the following command
   ```
   brew install stk
   ```
4. stk is ready to use now!