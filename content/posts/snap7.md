---
title: "Install snap7 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Ethernet communication suite that works natively with Siemens S7 PLCs"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install snap7 on MacOS using homebrew

- App Name: snap7
- App description: Ethernet communication suite that works natively with Siemens S7 PLCs
- App Version: 1.4.2
- App Website: https://snap7.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install snap7 with the following command
   ```
   brew install snap7
   ```
4. snap7 is ready to use now!