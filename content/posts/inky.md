---
title: "Install Inky on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Editor for ink: inkle's narrative scripting language"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Inky on MacOS using homebrew

- App Name: Inky
- App description: Editor for ink: inkle's narrative scripting language
- App Version: 0.12.0
- App Website: https://www.inklestudios.com/ink/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Inky with the following command
   ```
   brew install --cask inky
   ```
4. Inky is ready to use now!