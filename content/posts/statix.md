---
title: "Install statix on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Lints and suggestions for the nix programming language"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install statix on MacOS using homebrew

- App Name: statix
- App description: Lints and suggestions for the nix programming language
- App Version: 0.5.3
- App Website: https://github.com/nerdypepper/statix

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install statix with the following command
   ```
   brew install statix
   ```
4. statix is ready to use now!