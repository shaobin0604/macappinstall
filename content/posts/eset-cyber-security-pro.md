---
title: "Install ESET Cyber Security Pro on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ESET Cyber Security Pro on MacOS using homebrew

- App Name: ESET Cyber Security Pro
- App description: null
- App Version: 6.10.460.1
- App Website: https://www.eset.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ESET Cyber Security Pro with the following command
   ```
   brew install --cask eset-cyber-security-pro
   ```
4. ESET Cyber Security Pro is ready to use now!