---
title: "Install mm-common on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Build utilities for C++ interfaces of GTK+ and GNOME packages"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mm-common on MacOS using homebrew

- App Name: mm-common
- App description: Build utilities for C++ interfaces of GTK+ and GNOME packages
- App Version: 1.0.4
- App Website: https://www.gtkmm.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mm-common with the following command
   ```
   brew install mm-common
   ```
4. mm-common is ready to use now!