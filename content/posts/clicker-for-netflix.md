---
title: "Install Clicker for Netflix on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Best standalone Netflix player"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Clicker for Netflix on MacOS using homebrew

- App Name: Clicker for Netflix
- App description: Best standalone Netflix player
- App Version: 2.13.0
- App Website: https://www.dbklabs.com/clicker-for-netflix/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Clicker for Netflix with the following command
   ```
   brew install --cask clicker-for-netflix
   ```
4. Clicker for Netflix is ready to use now!