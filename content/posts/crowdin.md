---
title: "Install crowdin on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line tool that allows to manage your resources with crowdin.com"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install crowdin on MacOS using homebrew

- App Name: crowdin
- App description: Command-line tool that allows to manage your resources with crowdin.com
- App Version: 3.7.7
- App Website: https://support.crowdin.com/cli-tool/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install crowdin with the following command
   ```
   brew install crowdin
   ```
4. crowdin is ready to use now!