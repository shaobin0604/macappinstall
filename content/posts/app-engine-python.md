---
title: "Install app-engine-python on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Google App Engine"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install app-engine-python on MacOS using homebrew

- App Name: app-engine-python
- App description: Google App Engine
- App Version: 1.9.86
- App Website: https://cloud.google.com/appengine/docs

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install app-engine-python with the following command
   ```
   brew install app-engine-python
   ```
4. app-engine-python is ready to use now!