---
title: "Install skktools on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "SKK dictionary maintenance tools"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install skktools on MacOS using homebrew

- App Name: skktools
- App description: SKK dictionary maintenance tools
- App Version: 1.3.4
- App Website: https://github.com/skk-dev/skktools

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install skktools with the following command
   ```
   brew install skktools
   ```
4. skktools is ready to use now!