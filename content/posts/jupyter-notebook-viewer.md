---
title: "Install Jupyter Notebook Viewer on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utility to render Jupyter notebooks"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Jupyter Notebook Viewer on MacOS using homebrew

- App Name: Jupyter Notebook Viewer
- App description: Utility to render Jupyter notebooks
- App Version: 0.1.4
- App Website: https://github.com/tuxu/nbviewer-app

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Jupyter Notebook Viewer with the following command
   ```
   brew install --cask jupyter-notebook-viewer
   ```
4. Jupyter Notebook Viewer is ready to use now!