---
title: "Install unoconv on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Convert between any document format supported by OpenOffice"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install unoconv on MacOS using homebrew

- App Name: unoconv
- App description: Convert between any document format supported by OpenOffice
- App Version: 0.9.0
- App Website: https://github.com/unoconv/unoconv

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install unoconv with the following command
   ```
   brew install unoconv
   ```
4. unoconv is ready to use now!