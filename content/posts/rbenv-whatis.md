---
title: "Install rbenv-whatis on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Resolves abbreviations and aliases to Ruby versions"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install rbenv-whatis on MacOS using homebrew

- App Name: rbenv-whatis
- App description: Resolves abbreviations and aliases to Ruby versions
- App Version: 1.0.0
- App Website: https://github.com/rkh/rbenv-whatis

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install rbenv-whatis with the following command
   ```
   brew install rbenv-whatis
   ```
4. rbenv-whatis is ready to use now!