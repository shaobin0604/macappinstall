---
title: "Install closure-stylesheets on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Extended CSS preprocessor, linter, and internationalizer"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install closure-stylesheets on MacOS using homebrew

- App Name: closure-stylesheets
- App description: Extended CSS preprocessor, linter, and internationalizer
- App Version: 1.5.0
- App Website: https://github.com/google/closure-stylesheets

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install closure-stylesheets with the following command
   ```
   brew install closure-stylesheets
   ```
4. closure-stylesheets is ready to use now!