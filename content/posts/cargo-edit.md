---
title: "Install cargo-edit on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utility for managing cargo dependencies from the command-line"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cargo-edit on MacOS using homebrew

- App Name: cargo-edit
- App description: Utility for managing cargo dependencies from the command-line
- App Version: 0.8.0
- App Website: https://killercup.github.io/cargo-edit/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cargo-edit with the following command
   ```
   brew install cargo-edit
   ```
4. cargo-edit is ready to use now!