---
title: "Install k2tf on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Kubernetes YAML to Terraform HCL converter"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install k2tf on MacOS using homebrew

- App Name: k2tf
- App description: Kubernetes YAML to Terraform HCL converter
- App Version: 0.6.3
- App Website: https://github.com/sl1pm4t/k2tf

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install k2tf with the following command
   ```
   brew install k2tf
   ```
4. k2tf is ready to use now!