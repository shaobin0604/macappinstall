---
title: "Install lmod on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Lua-based environment modules system to modify PATH variable"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install lmod on MacOS using homebrew

- App Name: lmod
- App description: Lua-based environment modules system to modify PATH variable
- App Version: 8.6.12
- App Website: https://lmod.readthedocs.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install lmod with the following command
   ```
   brew install lmod
   ```
4. lmod is ready to use now!