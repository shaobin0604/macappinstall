---
title: "Install eg on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Expert Guide. Norton Guide Reader For GNU/Linux"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install eg on MacOS using homebrew

- App Name: eg
- App description: Expert Guide. Norton Guide Reader For GNU/Linux
- App Version: 1.02
- App Website: https://github.com/davep/eg

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install eg with the following command
   ```
   brew install eg
   ```
4. eg is ready to use now!