---
title: "Install wolfmqtt on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Small, fast, portable MQTT client C implementation"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install wolfmqtt on MacOS using homebrew

- App Name: wolfmqtt
- App description: Small, fast, portable MQTT client C implementation
- App Version: 1.11.0
- App Website: https://github.com/wolfSSL/wolfMQTT

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install wolfmqtt with the following command
   ```
   brew install wolfmqtt
   ```
4. wolfmqtt is ready to use now!