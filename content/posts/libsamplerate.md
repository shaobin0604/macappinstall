---
title: "Install libsamplerate on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library for sample rate conversion of audio data"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libsamplerate on MacOS using homebrew

- App Name: libsamplerate
- App description: Library for sample rate conversion of audio data
- App Version: 0.1.9
- App Website: http://www.mega-nerd.com/SRC

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libsamplerate with the following command
   ```
   brew install libsamplerate
   ```
4. libsamplerate is ready to use now!