---
title: "Install tile38 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "In-memory geolocation data store, spatial index, and realtime geofence"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tile38 on MacOS using homebrew

- App Name: tile38
- App description: In-memory geolocation data store, spatial index, and realtime geofence
- App Version: 1.27.1
- App Website: https://tile38.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tile38 with the following command
   ```
   brew install tile38
   ```
4. tile38 is ready to use now!