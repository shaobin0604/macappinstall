---
title: "Install cadubi on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Creative ASCII drawing utility"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cadubi on MacOS using homebrew

- App Name: cadubi
- App description: Creative ASCII drawing utility
- App Version: 1.3.4
- App Website: https://github.com/statico/cadubi/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cadubi with the following command
   ```
   brew install cadubi
   ```
4. cadubi is ready to use now!