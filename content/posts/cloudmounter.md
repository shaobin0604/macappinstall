---
title: "Install Eltima CloudMounter on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Mounts cloud storages as local disks"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Eltima CloudMounter on MacOS using homebrew

- App Name: Eltima CloudMounter
- App description: Mounts cloud storages as local disks
- App Version: 3.11,698
- App Website: https://mac.eltima.com/mount-cloud-drive.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Eltima CloudMounter with the following command
   ```
   brew install --cask cloudmounter
   ```
4. Eltima CloudMounter is ready to use now!