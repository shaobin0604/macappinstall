---
title: "Install tev on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "HDR image comparison tool with an emphasis on OpenEXR images"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tev on MacOS using homebrew

- App Name: tev
- App description: HDR image comparison tool with an emphasis on OpenEXR images
- App Version: 1.23
- App Website: https://github.com/Tom94/tev

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tev with the following command
   ```
   brew install --cask tev
   ```
4. tev is ready to use now!