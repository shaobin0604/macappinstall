---
title: "Install kubeprod on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Installer for the Bitnami Kubernetes Production Runtime (BKPR)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install kubeprod on MacOS using homebrew

- App Name: kubeprod
- App description: Installer for the Bitnami Kubernetes Production Runtime (BKPR)
- App Version: 1.8.0
- App Website: https://kubeprod.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install kubeprod with the following command
   ```
   brew install kubeprod
   ```
4. kubeprod is ready to use now!