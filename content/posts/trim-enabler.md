---
title: "Install Trim Enabler on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Enable trim for SSD performance"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Trim Enabler on MacOS using homebrew

- App Name: Trim Enabler
- App description: Enable trim for SSD performance
- App Version: 4.3.6,26,1609590590
- App Website: https://cindori.org/trimenabler/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Trim Enabler with the following command
   ```
   brew install --cask trim-enabler
   ```
4. Trim Enabler is ready to use now!