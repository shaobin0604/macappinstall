---
title: "Install gpsd on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Global Positioning System (GPS) daemon"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gpsd on MacOS using homebrew

- App Name: gpsd
- App description: Global Positioning System (GPS) daemon
- App Version: 3.23.1
- App Website: https://gpsd.gitlab.io/gpsd/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gpsd with the following command
   ```
   brew install gpsd
   ```
4. gpsd is ready to use now!