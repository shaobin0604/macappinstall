---
title: "Install gdub on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Gradlew/gradle wrapper"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gdub on MacOS using homebrew

- App Name: gdub
- App description: Gradlew/gradle wrapper
- App Version: 0.2.0
- App Website: https://www.gdub.rocks/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gdub with the following command
   ```
   brew install gdub
   ```
4. gdub is ready to use now!