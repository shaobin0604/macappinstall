---
title: "Install json-table on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Transform nested JSON data into tabular data in the shell"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install json-table on MacOS using homebrew

- App Name: json-table
- App description: Transform nested JSON data into tabular data in the shell
- App Version: 4.3.3
- App Website: https://github.com/micha/json-table

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install json-table with the following command
   ```
   brew install json-table
   ```
4. json-table is ready to use now!