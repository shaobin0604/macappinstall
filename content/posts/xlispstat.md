---
title: "Install xlispstat on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Statistical data science environment based on Lisp"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install xlispstat on MacOS using homebrew

- App Name: xlispstat
- App description: Statistical data science environment based on Lisp
- App Version: 3.52.23
- App Website: https://homepage.stat.uiowa.edu/~luke/xls/xlsinfo/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install xlispstat with the following command
   ```
   brew install xlispstat
   ```
4. xlispstat is ready to use now!