---
title: "Install licensefinder on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Find licenses for your project's dependencies"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install licensefinder on MacOS using homebrew

- App Name: licensefinder
- App description: Find licenses for your project's dependencies
- App Version: 6.15.0
- App Website: https://github.com/pivotal/LicenseFinder

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install licensefinder with the following command
   ```
   brew install licensefinder
   ```
4. licensefinder is ready to use now!