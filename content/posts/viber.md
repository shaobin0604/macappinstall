---
title: "Install Viber on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Calling and messaging application focusing on security"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Viber on MacOS using homebrew

- App Name: Viber
- App description: Calling and messaging application focusing on security
- App Version: 16.9.0,1690
- App Website: https://www.viber.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Viber with the following command
   ```
   brew install --cask viber
   ```
4. Viber is ready to use now!