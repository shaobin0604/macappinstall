---
title: "Install Autodesk Sketchbook on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Draw, paint, & sketch application"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Autodesk Sketchbook on MacOS using homebrew

- App Name: Autodesk Sketchbook
- App description: Draw, paint, & sketch application
- App Version: 8.7.1,2019
- App Website: https://www.sketchbook.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Autodesk Sketchbook with the following command
   ```
   brew install --cask sketchbook
   ```
4. Autodesk Sketchbook is ready to use now!