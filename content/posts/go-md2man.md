---
title: "Install go-md2man on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Converts markdown into roff (man pages)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install go-md2man on MacOS using homebrew

- App Name: go-md2man
- App description: Converts markdown into roff (man pages)
- App Version: 2.0.1
- App Website: https://github.com/cpuguy83/go-md2man

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install go-md2man with the following command
   ```
   brew install go-md2man
   ```
4. go-md2man is ready to use now!