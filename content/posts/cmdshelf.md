---
title: "Install cmdshelf on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Better scripting life with cmdshelf"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cmdshelf on MacOS using homebrew

- App Name: cmdshelf
- App description: Better scripting life with cmdshelf
- App Version: 2.0.2
- App Website: https://github.com/toshi0383/cmdshelf

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cmdshelf with the following command
   ```
   brew install cmdshelf
   ```
4. cmdshelf is ready to use now!