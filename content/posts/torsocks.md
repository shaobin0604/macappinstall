---
title: "Install torsocks on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Use SOCKS-friendly applications with Tor"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install torsocks on MacOS using homebrew

- App Name: torsocks
- App description: Use SOCKS-friendly applications with Tor
- App Version: 2.3.0
- App Website: https://gitweb.torproject.org/torsocks.git/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install torsocks with the following command
   ```
   brew install torsocks
   ```
4. torsocks is ready to use now!