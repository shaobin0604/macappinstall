---
title: "Install rex on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line tool which executes commands on remote servers"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install rex on MacOS using homebrew

- App Name: rex
- App description: Command-line tool which executes commands on remote servers
- App Version: 1.11.0
- App Website: https://www.rexify.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install rex with the following command
   ```
   brew install rex
   ```
4. rex is ready to use now!