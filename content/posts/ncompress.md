---
title: "Install ncompress on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Fast, simple LZW file compressor"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ncompress on MacOS using homebrew

- App Name: ncompress
- App description: Fast, simple LZW file compressor
- App Version: 5.0
- App Website: https://github.com/vapier/ncompress

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ncompress with the following command
   ```
   brew install ncompress
   ```
4. ncompress is ready to use now!