---
title: "Install youtube-dl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Download YouTube videos from the command-line"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install youtube-dl on MacOS using homebrew

- App Name: youtube-dl
- App description: Download YouTube videos from the command-line
- App Version: 2021.12.17
- App Website: https://youtube-dl.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install youtube-dl with the following command
   ```
   brew install youtube-dl
   ```
4. youtube-dl is ready to use now!