---
title: "Install mmseqs2 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Software suite for very fast sequence search and clustering"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mmseqs2 on MacOS using homebrew

- App Name: mmseqs2
- App description: Software suite for very fast sequence search and clustering
- App Version: 13-45111
- App Website: https://mmseqs.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mmseqs2 with the following command
   ```
   brew install mmseqs2
   ```
4. mmseqs2 is ready to use now!