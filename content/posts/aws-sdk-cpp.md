---
title: "Install aws-sdk-cpp on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "AWS SDK for C++"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install aws-sdk-cpp on MacOS using homebrew

- App Name: aws-sdk-cpp
- App description: AWS SDK for C++
- App Version: 1.9.180
- App Website: https://github.com/aws/aws-sdk-cpp

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install aws-sdk-cpp with the following command
   ```
   brew install aws-sdk-cpp
   ```
4. aws-sdk-cpp is ready to use now!