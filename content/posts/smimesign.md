---
title: "Install smimesign on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "S/MIME signing utility for use with Git"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install smimesign on MacOS using homebrew

- App Name: smimesign
- App description: S/MIME signing utility for use with Git
- App Version: 0.2.0
- App Website: https://github.com/github/smimesign

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install smimesign with the following command
   ```
   brew install smimesign
   ```
4. smimesign is ready to use now!