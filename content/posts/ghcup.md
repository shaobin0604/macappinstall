---
title: "Install ghcup on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Installer for the general purpose language Haskell"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ghcup on MacOS using homebrew

- App Name: ghcup
- App description: Installer for the general purpose language Haskell
- App Version: 0.1.17.4
- App Website: https://www.haskell.org/ghcup/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ghcup with the following command
   ```
   brew install ghcup
   ```
4. ghcup is ready to use now!