---
title: "Install Rambox on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Free and Open Source messaging and emailing app"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Rambox on MacOS using homebrew

- App Name: Rambox
- App description: Free and Open Source messaging and emailing app
- App Version: 0.7.9
- App Website: https://rambox.pro/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Rambox with the following command
   ```
   brew install --cask rambox
   ```
4. Rambox is ready to use now!