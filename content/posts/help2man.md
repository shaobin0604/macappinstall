---
title: "Install help2man on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Automatically generate simple man pages"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install help2man on MacOS using homebrew

- App Name: help2man
- App description: Automatically generate simple man pages
- App Version: 1.49.1
- App Website: https://www.gnu.org/software/help2man/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install help2man with the following command
   ```
   brew install help2man
   ```
4. help2man is ready to use now!