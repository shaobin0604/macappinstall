---
title: "Install cassandra@2.1 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Distributed key-value store"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cassandra@2.1 on MacOS using homebrew

- App Name: cassandra@2.1
- App description: Distributed key-value store
- App Version: 2.1.21
- App Website: https://cassandra.apache.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cassandra@2.1 with the following command
   ```
   brew install cassandra@2.1
   ```
4. cassandra@2.1 is ready to use now!