---
title: "Install Mini Program Studio on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "IDE for the development of Alipay applets"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Mini Program Studio on MacOS using homebrew

- App Name: Mini Program Studio
- App description: IDE for the development of Alipay applets
- App Version: 2.5.6,6d3b92d2-548c-4af2-a9fb-036b2d176e88
- App Website: https://opendocs.alipay.com/mini/ide

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Mini Program Studio with the following command
   ```
   brew install --cask mini-program-studio
   ```
4. Mini Program Studio is ready to use now!