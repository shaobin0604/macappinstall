---
title: "Install mercury on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Logic/functional programming language"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mercury on MacOS using homebrew

- App Name: mercury
- App description: Logic/functional programming language
- App Version: 20.06.1
- App Website: https://mercurylang.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mercury with the following command
   ```
   brew install mercury
   ```
4. mercury is ready to use now!