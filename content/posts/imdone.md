---
title: "Install imdone on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Kanban board that works on plain text markdown files or code"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install imdone on MacOS using homebrew

- App Name: imdone
- App description: Kanban board that works on plain text markdown files or code
- App Version: 1.25.5
- App Website: https://imdone.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install imdone with the following command
   ```
   brew install --cask imdone
   ```
4. imdone is ready to use now!