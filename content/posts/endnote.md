---
title: "Install EndNote on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Reference manager"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install EndNote on MacOS using homebrew

- App Name: EndNote
- App description: Reference manager
- App Version: 20.2
- App Website: https://endnote.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install EndNote with the following command
   ```
   brew install --cask endnote
   ```
4. EndNote is ready to use now!