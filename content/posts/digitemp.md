---
title: "Install digitemp on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Read temperature sensors in a 1-Wire net"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install digitemp on MacOS using homebrew

- App Name: digitemp
- App description: Read temperature sensors in a 1-Wire net
- App Version: 3.7.2
- App Website: https://www.digitemp.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install digitemp with the following command
   ```
   brew install digitemp
   ```
4. digitemp is ready to use now!