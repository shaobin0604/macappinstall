---
title: "Install pdal on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Point data abstraction library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pdal on MacOS using homebrew

- App Name: pdal
- App description: Point data abstraction library
- App Version: 2.3.0
- App Website: https://www.pdal.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pdal with the following command
   ```
   brew install pdal
   ```
4. pdal is ready to use now!