---
title: "Install userspace-rcu on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library for userspace RCU (read-copy-update)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install userspace-rcu on MacOS using homebrew

- App Name: userspace-rcu
- App description: Library for userspace RCU (read-copy-update)
- App Version: 0.13.1
- App Website: https://liburcu.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install userspace-rcu with the following command
   ```
   brew install userspace-rcu
   ```
4. userspace-rcu is ready to use now!