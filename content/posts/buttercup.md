---
title: "Install Buttercup on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Javascript Secrets Vault - Multi-Platform Desktop Application"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Buttercup on MacOS using homebrew

- App Name: Buttercup
- App description: Javascript Secrets Vault - Multi-Platform Desktop Application
- App Version: 2.14.2
- App Website: https://buttercup.pw/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Buttercup with the following command
   ```
   brew install --cask buttercup
   ```
4. Buttercup is ready to use now!