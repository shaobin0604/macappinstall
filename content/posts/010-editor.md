---
title: "Install 010 Editor on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Text editor"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install 010 Editor on MacOS using homebrew

- App Name: 010 Editor
- App description: Text editor
- App Version: 12.0.1
- App Website: https://www.sweetscape.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install 010 Editor with the following command
   ```
   brew install --cask 010-editor
   ```
4. 010 Editor is ready to use now!