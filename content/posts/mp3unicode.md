---
title: "Install mp3unicode on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line utility to convert mp3 tags between different encodings"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mp3unicode on MacOS using homebrew

- App Name: mp3unicode
- App description: Command-line utility to convert mp3 tags between different encodings
- App Version: 1.2.1
- App Website: https://mp3unicode.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mp3unicode with the following command
   ```
   brew install mp3unicode
   ```
4. mp3unicode is ready to use now!