---
title: "Install dcadec on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "DTS Coherent Acoustics decoder with support for HD extensions"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dcadec on MacOS using homebrew

- App Name: dcadec
- App description: DTS Coherent Acoustics decoder with support for HD extensions
- App Version: 0.2.0
- App Website: https://github.com/foo86/dcadec

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dcadec with the following command
   ```
   brew install dcadec
   ```
4. dcadec is ready to use now!