---
title: "Install Santa on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Binary authorization system"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Santa on MacOS using homebrew

- App Name: Santa
- App description: Binary authorization system
- App Version: 2022.1
- App Website: https://github.com/google/santa

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Santa with the following command
   ```
   brew install --cask santa
   ```
4. Santa is ready to use now!