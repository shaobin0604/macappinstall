---
title: "Install dbxml on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Embeddable XML database with XQuery support and other advanced features"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dbxml on MacOS using homebrew

- App Name: dbxml
- App description: Embeddable XML database with XQuery support and other advanced features
- App Version: 6.1.4
- App Website: https://www.oracle.com/database/berkeley-db/xml.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dbxml with the following command
   ```
   brew install dbxml
   ```
4. dbxml is ready to use now!