---
title: "Install json-c on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "JSON parser for C"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install json-c on MacOS using homebrew

- App Name: json-c
- App description: JSON parser for C
- App Version: 0.15
- App Website: https://github.com/json-c/json-c/wiki

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install json-c with the following command
   ```
   brew install json-c
   ```
4. json-c is ready to use now!