---
title: "Install libva on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Hardware accelerated video processing library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libva on MacOS using homebrew

- App Name: libva
- App description: Hardware accelerated video processing library
- App Version: 2.13.0
- App Website: https://github.com/intel/libva

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libva with the following command
   ```
   brew install libva
   ```
4. libva is ready to use now!