---
title: "Install ssss on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Shamir's secret sharing scheme implementation"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ssss on MacOS using homebrew

- App Name: ssss
- App description: Shamir's secret sharing scheme implementation
- App Version: 0.5
- App Website: http://point-at-infinity.org/ssss/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ssss with the following command
   ```
   brew install ssss
   ```
4. ssss is ready to use now!