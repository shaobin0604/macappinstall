---
title: "Install iperf3 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Update of iperf: measures TCP, UDP, and SCTP bandwidth"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install iperf3 on MacOS using homebrew

- App Name: iperf3
- App description: Update of iperf: measures TCP, UDP, and SCTP bandwidth
- App Version: 3.11
- App Website: https://github.com/esnet/iperf

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install iperf3 with the following command
   ```
   brew install iperf3
   ```
4. iperf3 is ready to use now!