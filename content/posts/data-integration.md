---
title: "Install Pentaho Data Integration on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "End to end data integration and analytics platform"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Pentaho Data Integration on MacOS using homebrew

- App Name: Pentaho Data Integration
- App description: End to end data integration and analytics platform
- App Version: 9.2.0.0-290
- App Website: https://sourceforge.net/projects/pentaho/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Pentaho Data Integration with the following command
   ```
   brew install --cask data-integration
   ```
4. Pentaho Data Integration is ready to use now!