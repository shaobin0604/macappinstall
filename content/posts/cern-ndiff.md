---
title: "Install cern-ndiff on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Numerical diff tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cern-ndiff on MacOS using homebrew

- App Name: cern-ndiff
- App description: Numerical diff tool
- App Version: 5.08.00
- App Website: https://mad.web.cern.ch/mad/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cern-ndiff with the following command
   ```
   brew install cern-ndiff
   ```
4. cern-ndiff is ready to use now!