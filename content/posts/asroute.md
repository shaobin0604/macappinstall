---
title: "Install asroute on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CLI to interpret traceroute -a output to show AS names traversed"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install asroute on MacOS using homebrew

- App Name: asroute
- App description: CLI to interpret traceroute -a output to show AS names traversed
- App Version: 0.1.0
- App Website: https://github.com/stevenpack/asroute

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install asroute with the following command
   ```
   brew install asroute
   ```
4. asroute is ready to use now!