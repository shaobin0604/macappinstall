---
title: "Install dnsmap on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Passive DNS network mapper (a.k.a. subdomains bruteforcer)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dnsmap on MacOS using homebrew

- App Name: dnsmap
- App description: Passive DNS network mapper (a.k.a. subdomains bruteforcer)
- App Version: 0.30
- App Website: https://code.google.com/archive/p/dnsmap/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dnsmap with the following command
   ```
   brew install dnsmap
   ```
4. dnsmap is ready to use now!