---
title: "Install Avira Antivirus on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Antivirus and VPN software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Avira Antivirus on MacOS using homebrew

- App Name: Avira Antivirus
- App description: Antivirus and VPN software
- App Version: latest
- App Website: https://www.avira.com/en/free-antivirus-mac/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Avira Antivirus with the following command
   ```
   brew install --cask avira-antivirus
   ```
4. Avira Antivirus is ready to use now!