---
title: "Install libdmx on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "X.Org: X Window System DMX (Distributed Multihead X) extension library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libdmx on MacOS using homebrew

- App Name: libdmx
- App description: X.Org: X Window System DMX (Distributed Multihead X) extension library
- App Version: 1.1.4
- App Website: https://www.x.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libdmx with the following command
   ```
   brew install libdmx
   ```
4. libdmx is ready to use now!