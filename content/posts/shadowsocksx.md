---
title: "Install ShadowsocksX on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Removed according to regulations"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ShadowsocksX on MacOS using homebrew

- App Name: ShadowsocksX
- App description: Removed according to regulations
- App Version: 2.6.3
- App Website: https://github.com/shadowsocks/shadowsocks-iOS/wiki/Shadowsocks-for-OSX-Help

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ShadowsocksX with the following command
   ```
   brew install --cask shadowsocksx
   ```
4. ShadowsocksX is ready to use now!