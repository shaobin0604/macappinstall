---
title: "Install clp on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Linear programming solver"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install clp on MacOS using homebrew

- App Name: clp
- App description: Linear programming solver
- App Version: 1.17.7
- App Website: https://github.com/coin-or/Clp

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install clp with the following command
   ```
   brew install clp
   ```
4. clp is ready to use now!