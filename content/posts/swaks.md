---
title: "Install swaks on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "SMTP command-line test tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install swaks on MacOS using homebrew

- App Name: swaks
- App description: SMTP command-line test tool
- App Version: 20201014.0
- App Website: https://www.jetmore.org/john/code/swaks/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install swaks with the following command
   ```
   brew install swaks
   ```
4. swaks is ready to use now!