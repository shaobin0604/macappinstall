---
title: "Install forcecli on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line interface to Force.com"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install forcecli on MacOS using homebrew

- App Name: forcecli
- App description: Command-line interface to Force.com
- App Version: 0.33.0
- App Website: https://force-cli.herokuapp.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install forcecli with the following command
   ```
   brew install forcecli
   ```
4. forcecli is ready to use now!