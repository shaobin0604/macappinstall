---
title: "Install kDrive on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Client for the kDrive collaborative cloud storage service"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install kDrive on MacOS using homebrew

- App Name: kDrive
- App description: Client for the kDrive collaborative cloud storage service
- App Version: 3.2.7.20211210
- App Website: https://www.infomaniak.com/kdrive

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install kDrive with the following command
   ```
   brew install --cask kdrive
   ```
4. kDrive is ready to use now!