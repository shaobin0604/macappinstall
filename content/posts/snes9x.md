---
title: "Install Snes9x on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Snes9x on MacOS using homebrew

- App Name: Snes9x
- App description: null
- App Version: 1.60
- App Website: https://www.snes9x.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Snes9x with the following command
   ```
   brew install --cask snes9x
   ```
4. Snes9x is ready to use now!