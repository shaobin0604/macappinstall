---
title: "Install neovim-remote on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Control nvim processes using `nvr` command-line tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install neovim-remote on MacOS using homebrew

- App Name: neovim-remote
- App description: Control nvim processes using `nvr` command-line tool
- App Version: 2.4.0
- App Website: https://github.com/mhinz/neovim-remote

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install neovim-remote with the following command
   ```
   brew install neovim-remote
   ```
4. neovim-remote is ready to use now!