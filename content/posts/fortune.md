---
title: "Install fortune on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Infamous electronic fortune-cookie generator"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fortune on MacOS using homebrew

- App Name: fortune
- App description: Infamous electronic fortune-cookie generator
- App Version: 9708
- App Website: https://www.ibiblio.org/pub/linux/games/amusements/fortune/!INDEX.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fortune with the following command
   ```
   brew install fortune
   ```
4. fortune is ready to use now!