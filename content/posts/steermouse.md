---
title: "Install SteerMouse on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Customize mouse buttons, wheels and cursor speed"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install SteerMouse on MacOS using homebrew

- App Name: SteerMouse
- App description: Customize mouse buttons, wheels and cursor speed
- App Version: 5.6
- App Website: https://plentycom.jp/en/steermouse/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install SteerMouse with the following command
   ```
   brew install --cask steermouse
   ```
4. SteerMouse is ready to use now!