---
title: "Install blockhash on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Perceptual image hash calculation tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install blockhash on MacOS using homebrew

- App Name: blockhash
- App description: Perceptual image hash calculation tool
- App Version: 0.3.2
- App Website: https://github.com/commonsmachinery/blockhash

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install blockhash with the following command
   ```
   brew install blockhash
   ```
4. blockhash is ready to use now!