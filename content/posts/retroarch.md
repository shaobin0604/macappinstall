---
title: "Install RetroArch on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Emulator frontend (OpenGL graphics API version)"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install RetroArch on MacOS using homebrew

- App Name: RetroArch
- App description: Emulator frontend (OpenGL graphics API version)
- App Version: 1.10.0
- App Website: https://www.libretro.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install RetroArch with the following command
   ```
   brew install --cask retroarch
   ```
4. RetroArch is ready to use now!