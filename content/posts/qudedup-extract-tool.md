---
title: "Install Qnap QuDedup Extract Tool on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Restoring deduplicated .qdff files to their normal status"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Qnap QuDedup Extract Tool on MacOS using homebrew

- App Name: Qnap QuDedup Extract Tool
- App description: Restoring deduplicated .qdff files to their normal status
- App Version: 1.1.5.21201
- App Website: https://www.qnap.com/en/utilities#utliity_18

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Qnap QuDedup Extract Tool with the following command
   ```
   brew install --cask qudedup-extract-tool
   ```
4. Qnap QuDedup Extract Tool is ready to use now!