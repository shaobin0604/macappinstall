---
title: "Install sofia-sip on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "SIP User-Agent library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sofia-sip on MacOS using homebrew

- App Name: sofia-sip
- App description: SIP User-Agent library
- App Version: 1.13.7
- App Website: https://sofia-sip.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sofia-sip with the following command
   ```
   brew install sofia-sip
   ```
4. sofia-sip is ready to use now!