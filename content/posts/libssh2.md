---
title: "Install libssh2 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C library implementing the SSH2 protocol"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libssh2 on MacOS using homebrew

- App Name: libssh2
- App description: C library implementing the SSH2 protocol
- App Version: 1.10.0
- App Website: https://www.libssh2.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libssh2 with the following command
   ```
   brew install libssh2
   ```
4. libssh2 is ready to use now!