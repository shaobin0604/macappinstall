---
title: "Install exact-image on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Image processing library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install exact-image on MacOS using homebrew

- App Name: exact-image
- App description: Image processing library
- App Version: 1.0.2
- App Website: https://exactcode.com/opensource/exactimage/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install exact-image with the following command
   ```
   brew install exact-image
   ```
4. exact-image is ready to use now!