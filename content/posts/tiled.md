---
title: "Install Tiled on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Flexible level editor"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Tiled on MacOS using homebrew

- App Name: Tiled
- App description: Flexible level editor
- App Version: 1.7.2
- App Website: https://www.mapeditor.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Tiled with the following command
   ```
   brew install --cask tiled
   ```
4. Tiled is ready to use now!