---
title: "Install Taskade on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Task manager for teams"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Taskade on MacOS using homebrew

- App Name: Taskade
- App description: Task manager for teams
- App Version: 4.1.2
- App Website: https://www.taskade.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Taskade with the following command
   ```
   brew install --cask taskade
   ```
4. Taskade is ready to use now!