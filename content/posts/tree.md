---
title: "Install tree on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Display directories as trees (with optional color/HTML output)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tree on MacOS using homebrew

- App Name: tree
- App description: Display directories as trees (with optional color/HTML output)
- App Version: 2.0.2
- App Website: http://mama.indstate.edu/users/ice/tree/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tree with the following command
   ```
   brew install tree
   ```
4. tree is ready to use now!