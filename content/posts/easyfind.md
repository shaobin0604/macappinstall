---
title: "Install EasyFind on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Find files, folders, or contents in any file"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install EasyFind on MacOS using homebrew

- App Name: EasyFind
- App description: Find files, folders, or contents in any file
- App Version: 5.0.2
- App Website: https://www.devontechnologies.com/apps/freeware

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install EasyFind with the following command
   ```
   brew install --cask easyfind
   ```
4. EasyFind is ready to use now!