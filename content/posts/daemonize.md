---
title: "Install daemonize on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Run a command as a UNIX daemon"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install daemonize on MacOS using homebrew

- App Name: daemonize
- App description: Run a command as a UNIX daemon
- App Version: 1.7.8
- App Website: https://software.clapper.org/daemonize/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install daemonize with the following command
   ```
   brew install daemonize
   ```
4. daemonize is ready to use now!