---
title: "Install atlantis on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Terraform Pull Request Automation tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install atlantis on MacOS using homebrew

- App Name: atlantis
- App description: Terraform Pull Request Automation tool
- App Version: 0.18.2
- App Website: https://www.runatlantis.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install atlantis with the following command
   ```
   brew install atlantis
   ```
4. atlantis is ready to use now!