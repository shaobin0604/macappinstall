---
title: "Install b2sum on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "BLAKE2 b2sum reference binary"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install b2sum on MacOS using homebrew

- App Name: b2sum
- App description: BLAKE2 b2sum reference binary
- App Version: 20190724
- App Website: https://github.com/BLAKE2/BLAKE2

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install b2sum with the following command
   ```
   brew install b2sum
   ```
4. b2sum is ready to use now!