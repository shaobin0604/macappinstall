---
title: "Install Time To Leave on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Log work hours and get notified when it's time to leave the office"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Time To Leave on MacOS using homebrew

- App Name: Time To Leave
- App description: Log work hours and get notified when it's time to leave the office
- App Version: 2.0.1
- App Website: https://github.com/thamara/time-to-leave

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Time To Leave with the following command
   ```
   brew install --cask time-to-leave
   ```
4. Time To Leave is ready to use now!