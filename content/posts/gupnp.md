---
title: "Install gupnp on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Framework for creating UPnP devices and control points"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gupnp on MacOS using homebrew

- App Name: gupnp
- App description: Framework for creating UPnP devices and control points
- App Version: 1.4.3
- App Website: https://wiki.gnome.org/Projects/GUPnP

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gupnp with the following command
   ```
   brew install gupnp
   ```
4. gupnp is ready to use now!