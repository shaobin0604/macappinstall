---
title: "Install haproxy on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Reliable, high performance TCP/HTTP load balancer"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install haproxy on MacOS using homebrew

- App Name: haproxy
- App description: Reliable, high performance TCP/HTTP load balancer
- App Version: 2.5.2
- App Website: https://www.haproxy.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install haproxy with the following command
   ```
   brew install haproxy
   ```
4. haproxy is ready to use now!