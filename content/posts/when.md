---
title: "Install when on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tiny personal calendar"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install when on MacOS using homebrew

- App Name: when
- App description: Tiny personal calendar
- App Version: 1.1.42
- App Website: https://www.lightandmatter.com/when/when.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install when with the following command
   ```
   brew install when
   ```
4. when is ready to use now!