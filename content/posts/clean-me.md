---
title: "Install Clean-me on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "System cleaner for logs, caches and more"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Clean-me on MacOS using homebrew

- App Name: Clean-me
- App description: System cleaner for logs, caches and more
- App Version: 1.4.2
- App Website: https://github.com/Kevin-De-Koninck/Clean-Me

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Clean-me with the following command
   ```
   brew install --cask clean-me
   ```
4. Clean-me is ready to use now!