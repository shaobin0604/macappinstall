---
title: "Install ttyrec on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Terminal interaction recorder and player"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ttyrec on MacOS using homebrew

- App Name: ttyrec
- App description: Terminal interaction recorder and player
- App Version: 1.0.8
- App Website: http://0xcc.net/ttyrec/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ttyrec with the following command
   ```
   brew install ttyrec
   ```
4. ttyrec is ready to use now!