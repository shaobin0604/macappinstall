---
title: "Install temporal_tables on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Temporal Tables PostgreSQL Extension"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install temporal_tables on MacOS using homebrew

- App Name: temporal_tables
- App description: Temporal Tables PostgreSQL Extension
- App Version: 1.2.0
- App Website: https://pgxn.org/dist/temporal_tables/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install temporal_tables with the following command
   ```
   brew install temporal_tables
   ```
4. temporal_tables is ready to use now!