---
title: "Install pktanon on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Packet trace anonymization"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pktanon on MacOS using homebrew

- App Name: pktanon
- App description: Packet trace anonymization
- App Version: 1.4.0
- App Website: https://www.tm.uka.de/software/pktanon/index.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pktanon with the following command
   ```
   brew install pktanon
   ```
4. pktanon is ready to use now!