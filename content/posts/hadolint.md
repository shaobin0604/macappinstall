---
title: "Install hadolint on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Smarter Dockerfile linter to validate best practices"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install hadolint on MacOS using homebrew

- App Name: hadolint
- App description: Smarter Dockerfile linter to validate best practices
- App Version: 2.8.0
- App Website: https://github.com/hadolint/hadolint

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install hadolint with the following command
   ```
   brew install hadolint
   ```
4. hadolint is ready to use now!