---
title: "Install vte3 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Terminal emulator widget used by GNOME terminal"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install vte3 on MacOS using homebrew

- App Name: vte3
- App description: Terminal emulator widget used by GNOME terminal
- App Version: 0.64.2
- App Website: https://developer.gnome.org/vte/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install vte3 with the following command
   ```
   brew install vte3
   ```
4. vte3 is ready to use now!