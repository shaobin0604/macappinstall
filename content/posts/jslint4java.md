---
title: "Install jslint4java on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Java wrapper for JavaScript Lint (jsl)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install jslint4java on MacOS using homebrew

- App Name: jslint4java
- App description: Java wrapper for JavaScript Lint (jsl)
- App Version: 2.0.5
- App Website: https://code.google.com/archive/p/jslint4java/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install jslint4java with the following command
   ```
   brew install jslint4java
   ```
4. jslint4java is ready to use now!