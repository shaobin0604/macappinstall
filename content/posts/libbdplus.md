---
title: "Install libbdplus on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Implements the BD+ System Specifications"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libbdplus on MacOS using homebrew

- App Name: libbdplus
- App description: Implements the BD+ System Specifications
- App Version: 0.1.2
- App Website: https://www.videolan.org/developers/libbdplus.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libbdplus with the following command
   ```
   brew install libbdplus
   ```
4. libbdplus is ready to use now!