---
title: "Install PDFify on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Create searchable and smaller PDF"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install PDFify on MacOS using homebrew

- App Name: PDFify
- App description: Create searchable and smaller PDF
- App Version: 3.3.4-138
- App Website: https://pdfify.app/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install PDFify with the following command
   ```
   brew install --cask pdfify
   ```
4. PDFify is ready to use now!