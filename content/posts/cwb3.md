---
title: "Install cwb3 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tools for managing and querying large text corpora with linguistic annotations"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cwb3 on MacOS using homebrew

- App Name: cwb3
- App description: Tools for managing and querying large text corpora with linguistic annotations
- App Version: 3.4.33
- App Website: https://cwb.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cwb3 with the following command
   ```
   brew install cwb3
   ```
4. cwb3 is ready to use now!