---
title: "Install VESTA on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Visualization for electronic and structural analysis"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install VESTA on MacOS using homebrew

- App Name: VESTA
- App description: Visualization for electronic and structural analysis
- App Version: 3.5.7
- App Website: https://jp-minerals.org/vesta/en/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install VESTA with the following command
   ```
   brew install --cask vesta
   ```
4. VESTA is ready to use now!