---
title: "Install Rowmote Helper on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Control system with Rowmote Pro remote control"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Rowmote Helper on MacOS using homebrew

- App Name: Rowmote Helper
- App description: Control system with Rowmote Pro remote control
- App Version: 4.2.4
- App Website: https://regularrateandrhythm.com/apps/rowmote-pro/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Rowmote Helper with the following command
   ```
   brew install --cask rowmote-helper
   ```
4. Rowmote Helper is ready to use now!