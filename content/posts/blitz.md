---
title: "Install blitz on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Multi-dimensional array library for C++"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install blitz on MacOS using homebrew

- App Name: blitz
- App description: Multi-dimensional array library for C++
- App Version: 1.0.2
- App Website: https://github.com/blitzpp/blitz/wiki

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install blitz with the following command
   ```
   brew install blitz
   ```
4. blitz is ready to use now!