---
title: "Install Sproutcube Shortcat on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Sproutcube Shortcat on MacOS using homebrew

- App Name: Sproutcube Shortcat
- App description: null
- App Version: 0.7.11,91
- App Website: https://shortcatapp.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Sproutcube Shortcat with the following command
   ```
   brew install --cask shortcat
   ```
4. Sproutcube Shortcat is ready to use now!