---
title: "Install luaradio on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Lightweight, embeddable flow graph signal processing framework for SDR"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install luaradio on MacOS using homebrew

- App Name: luaradio
- App description: Lightweight, embeddable flow graph signal processing framework for SDR
- App Version: 0.10.0
- App Website: https://luaradio.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install luaradio with the following command
   ```
   brew install luaradio
   ```
4. luaradio is ready to use now!