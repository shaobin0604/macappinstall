---
title: "Install psalm on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "PHP Static Analysis Tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install psalm on MacOS using homebrew

- App Name: psalm
- App description: PHP Static Analysis Tool
- App Version: 4.20.0
- App Website: https://psalm.dev

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install psalm with the following command
   ```
   brew install psalm
   ```
4. psalm is ready to use now!