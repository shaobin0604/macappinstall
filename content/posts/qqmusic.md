---
title: "Install QQ音乐 on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Chinese music streaming application"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install QQ音乐 on MacOS using homebrew

- App Name: QQ音乐
- App description: Chinese music streaming application
- App Version: 8.0.0,71521
- App Website: https://y.qq.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install QQ音乐 with the following command
   ```
   brew install --cask qqmusic
   ```
4. QQ音乐 is ready to use now!