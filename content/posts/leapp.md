---
title: "Install Leapp on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cloud credentials manager"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Leapp on MacOS using homebrew

- App Name: Leapp
- App description: Cloud credentials manager
- App Version: 0.8.1
- App Website: https://www.leapp.cloud/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Leapp with the following command
   ```
   brew install --cask leapp
   ```
4. Leapp is ready to use now!