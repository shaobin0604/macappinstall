---
title: "Install zork on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Dungeon modified from FORTRAN to C"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install zork on MacOS using homebrew

- App Name: zork
- App description: Dungeon modified from FORTRAN to C
- App Version: 1.0.3
- App Website: https://github.com/devshane/zork

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install zork with the following command
   ```
   brew install zork
   ```
4. zork is ready to use now!