---
title: "Install vermin on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Concurrently detect the minimum Python versions needed to run code"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install vermin on MacOS using homebrew

- App Name: vermin
- App description: Concurrently detect the minimum Python versions needed to run code
- App Version: 1.3.3
- App Website: https://github.com/netromdk/vermin

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install vermin with the following command
   ```
   brew install vermin
   ```
4. vermin is ready to use now!