---
title: "Install media-info on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Unified display of technical and tag data for audio/video"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install media-info on MacOS using homebrew

- App Name: media-info
- App description: Unified display of technical and tag data for audio/video
- App Version: 21.09
- App Website: https://mediaarea.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install media-info with the following command
   ```
   brew install media-info
   ```
4. media-info is ready to use now!