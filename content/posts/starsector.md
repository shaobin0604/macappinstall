---
title: "Install Starsector on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Open-world single-player space combat and trading RPG"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Starsector on MacOS using homebrew

- App Name: Starsector
- App description: Open-world single-player space combat and trading RPG
- App Version: 0.95.1a-RC6
- App Website: https://fractalsoftworks.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Starsector with the following command
   ```
   brew install --cask starsector
   ```
4. Starsector is ready to use now!