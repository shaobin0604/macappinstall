---
title: "Install ProFind on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "File search app"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ProFind on MacOS using homebrew

- App Name: ProFind
- App description: File search app
- App Version: 1.9.3,1930
- App Website: https://www.zeroonetwenty.com/profind/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ProFind with the following command
   ```
   brew install --cask profind
   ```
4. ProFind is ready to use now!