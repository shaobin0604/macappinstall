---
title: "Install Letter Opener on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Display winmail.dat files directly in Mail.app"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Letter Opener on MacOS using homebrew

- App Name: Letter Opener
- App description: Display winmail.dat files directly in Mail.app
- App Version: 14.0.2
- App Website: https://winmail.help/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Letter Opener with the following command
   ```
   brew install --cask letter-opener
   ```
4. Letter Opener is ready to use now!