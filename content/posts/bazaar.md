---
title: "Install bazaar on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Friendly powerful distributed version control system"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bazaar on MacOS using homebrew

- App Name: bazaar
- App description: Friendly powerful distributed version control system
- App Version: 2.7.0
- App Website: https://bazaar.canonical.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bazaar with the following command
   ```
   brew install bazaar
   ```
4. bazaar is ready to use now!