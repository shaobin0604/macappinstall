---
title: "Install mkvdts2ac3 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Convert DTS audio to AC3 within a matroska file"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mkvdts2ac3 on MacOS using homebrew

- App Name: mkvdts2ac3
- App description: Convert DTS audio to AC3 within a matroska file
- App Version: 1.6.0
- App Website: https://github.com/JakeWharton/mkvdts2ac3

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mkvdts2ac3 with the following command
   ```
   brew install mkvdts2ac3
   ```
4. mkvdts2ac3 is ready to use now!