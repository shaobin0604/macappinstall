---
title: "Install LogDNA Agent on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Agent streams from log files to your LogDNA account"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install LogDNA Agent on MacOS using homebrew

- App Name: LogDNA Agent
- App description: Agent streams from log files to your LogDNA account
- App Version: 2.1.2
- App Website: https://logdna.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install LogDNA Agent with the following command
   ```
   brew install --cask logdna-agent
   ```
4. LogDNA Agent is ready to use now!