---
title: "Install AutoFirma on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Digital signature editor and validator"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install AutoFirma on MacOS using homebrew

- App Name: AutoFirma
- App description: Digital signature editor and validator
- App Version: 1.7.1
- App Website: https://firmaelectronica.gob.es/Home/Descargas.htm

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install AutoFirma with the following command
   ```
   brew install --cask autofirma
   ```
4. AutoFirma is ready to use now!