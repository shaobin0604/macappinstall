---
title: "Install fwup on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Configurable embedded Linux firmware update creator and runner"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fwup on MacOS using homebrew

- App Name: fwup
- App description: Configurable embedded Linux firmware update creator and runner
- App Version: 1.9.0
- App Website: https://github.com/fhunleth/fwup

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fwup with the following command
   ```
   brew install fwup
   ```
4. fwup is ready to use now!