---
title: "Install veclibfort on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GNU Fortran compatibility for Apple's vecLib"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install veclibfort on MacOS using homebrew

- App Name: veclibfort
- App description: GNU Fortran compatibility for Apple's vecLib
- App Version: 0.4.3
- App Website: https://github.com/mcg1969/vecLibFort

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install veclibfort with the following command
   ```
   brew install veclibfort
   ```
4. veclibfort is ready to use now!