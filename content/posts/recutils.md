---
title: "Install recutils on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tools to work with human-editable, plain text data files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install recutils on MacOS using homebrew

- App Name: recutils
- App description: Tools to work with human-editable, plain text data files
- App Version: 1.8
- App Website: https://www.gnu.org/software/recutils/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install recutils with the following command
   ```
   brew install recutils
   ```
4. recutils is ready to use now!