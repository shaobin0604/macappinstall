---
title: "Install ttf2eot on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Convert TTF files to EOT"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ttf2eot on MacOS using homebrew

- App Name: ttf2eot
- App description: Convert TTF files to EOT
- App Version: 0.0.3
- App Website: https://github.com/wget/ttf2eot

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ttf2eot with the following command
   ```
   brew install ttf2eot
   ```
4. ttf2eot is ready to use now!