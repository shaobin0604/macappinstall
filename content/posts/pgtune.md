---
title: "Install pgtune on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tuning wizard for postgresql.conf"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pgtune on MacOS using homebrew

- App Name: pgtune
- App description: Tuning wizard for postgresql.conf
- App Version: 0.9.3
- App Website: https://web.archive.org/web/20190717075914/pgfoundry.org/projects/pgtune

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pgtune with the following command
   ```
   brew install pgtune
   ```
4. pgtune is ready to use now!