---
title: "Install Private Internet Access on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "VPN client"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Private Internet Access on MacOS using homebrew

- App Name: Private Internet Access
- App description: VPN client
- App Version: 3.2-06857
- App Website: https://www.privateinternetaccess.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Private Internet Access with the following command
   ```
   brew install --cask private-internet-access
   ```
4. Private Internet Access is ready to use now!