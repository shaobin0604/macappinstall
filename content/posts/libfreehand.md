---
title: "Install libfreehand on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Interpret and import Aldus/Macromedia/Adobe FreeHand documents"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libfreehand on MacOS using homebrew

- App Name: libfreehand
- App description: Interpret and import Aldus/Macromedia/Adobe FreeHand documents
- App Version: 0.1.2
- App Website: https://wiki.documentfoundation.org/DLP/Libraries/libfreehand

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libfreehand with the following command
   ```
   brew install libfreehand
   ```
4. libfreehand is ready to use now!