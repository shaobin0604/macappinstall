---
title: "Install aText on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to replace abbreviations while typing"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install aText on MacOS using homebrew

- App Name: aText
- App description: Tool to replace abbreviations while typing
- App Version: 2.40.5,122
- App Website: https://www.trankynam.com/atext/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install aText with the following command
   ```
   brew install --cask atext
   ```
4. aText is ready to use now!