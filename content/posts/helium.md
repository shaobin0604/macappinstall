---
title: "Install Helium on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Helium on MacOS using homebrew

- App Name: Helium
- App description: null
- App Version: 1.0
- App Website: https://github.com/koush/support-wiki/wiki/Helium-Desktop-Installer-and-Android-App

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Helium with the following command
   ```
   brew install --cask helium
   ```
4. Helium is ready to use now!