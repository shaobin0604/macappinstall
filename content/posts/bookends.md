---
title: "Install Bookends on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Reference management and bibliography software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Bookends on MacOS using homebrew

- App Name: Bookends
- App description: Reference management and bibliography software
- App Version: 14.0.4
- App Website: https://www.sonnysoftware.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Bookends with the following command
   ```
   brew install --cask bookends
   ```
4. Bookends is ready to use now!