---
title: "Install FabFilter Micro on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Filter plug-in"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install FabFilter Micro on MacOS using homebrew

- App Name: FabFilter Micro
- App description: Filter plug-in
- App Version: 1.23
- App Website: https://www.fabfilter.com/products/micro-mini-filter-plug-in

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install FabFilter Micro with the following command
   ```
   brew install --cask fabfilter-micro
   ```
4. FabFilter Micro is ready to use now!