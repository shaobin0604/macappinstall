---
title: "Install NitroShare on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Network file transfer application"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install NitroShare on MacOS using homebrew

- App Name: NitroShare
- App description: Network file transfer application
- App Version: 0.3.4
- App Website: https://launchpad.net/nitroshare

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install NitroShare with the following command
   ```
   brew install --cask nitroshare
   ```
4. NitroShare is ready to use now!