---
title: "Install libfishsound on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Decode and encode audio data using the Xiph.org codecs"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libfishsound on MacOS using homebrew

- App Name: libfishsound
- App description: Decode and encode audio data using the Xiph.org codecs
- App Version: 1.0.0
- App Website: https://xiph.org/fishsound/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libfishsound with the following command
   ```
   brew install libfishsound
   ```
4. libfishsound is ready to use now!