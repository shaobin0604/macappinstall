---
title: "Install proxytunnel on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Create TCP tunnels through HTTPS proxies"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install proxytunnel on MacOS using homebrew

- App Name: proxytunnel
- App description: Create TCP tunnels through HTTPS proxies
- App Version: 1.10.20210604
- App Website: https://github.com/proxytunnel/proxytunnel

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install proxytunnel with the following command
   ```
   brew install proxytunnel
   ```
4. proxytunnel is ready to use now!