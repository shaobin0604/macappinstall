---
title: "Install bc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Arbitrary precision numeric processing language"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bc on MacOS using homebrew

- App Name: bc
- App description: Arbitrary precision numeric processing language
- App Version: 1.07.1
- App Website: https://www.gnu.org/software/bc/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bc with the following command
   ```
   brew install bc
   ```
4. bc is ready to use now!