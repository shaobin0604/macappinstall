---
title: "Install Firecamp on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Multi-protocol API development platform"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Firecamp on MacOS using homebrew

- App Name: Firecamp
- App description: Multi-protocol API development platform
- App Version: 2.5.2
- App Website: https://firecamp.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Firecamp with the following command
   ```
   brew install --cask firecamp
   ```
4. Firecamp is ready to use now!