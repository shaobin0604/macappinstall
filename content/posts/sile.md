---
title: "Install sile on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Modern typesetting system inspired by TeX"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sile on MacOS using homebrew

- App Name: sile
- App description: Modern typesetting system inspired by TeX
- App Version: 0.12.2
- App Website: https://sile-typesetter.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sile with the following command
   ```
   brew install sile
   ```
4. sile is ready to use now!