---
title: "Install autogen on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Automated text file generator"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install autogen on MacOS using homebrew

- App Name: autogen
- App description: Automated text file generator
- App Version: 5.18.16
- App Website: https://autogen.sourceforge.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install autogen with the following command
   ```
   brew install autogen
   ```
4. autogen is ready to use now!