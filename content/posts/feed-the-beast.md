---
title: "Install Feed the Beast on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Minecraft mod downloader and manager"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Feed the Beast on MacOS using homebrew

- App Name: Feed the Beast
- App description: Minecraft mod downloader and manager
- App Version: 202202141200,45844bd46c
- App Website: https://www.feed-the-beast.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Feed the Beast with the following command
   ```
   brew install --cask feed-the-beast
   ```
4. Feed the Beast is ready to use now!