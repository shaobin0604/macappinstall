---
title: "Install git-flow on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Extensions to follow Vincent Driessen's branching model"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install git-flow on MacOS using homebrew

- App Name: git-flow
- App description: Extensions to follow Vincent Driessen's branching model
- App Version: 0.4.1
- App Website: https://github.com/nvie/gitflow

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install git-flow with the following command
   ```
   brew install git-flow
   ```
4. git-flow is ready to use now!