---
title: "Install BankID on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Electronic personal identification system"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install BankID on MacOS using homebrew

- App Name: BankID
- App description: Electronic personal identification system
- App Version: 7.12.0
- App Website: https://install.bankid.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install BankID with the following command
   ```
   brew install --cask bankid
   ```
4. BankID is ready to use now!