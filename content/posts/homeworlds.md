---
title: "Install homeworlds on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C++ framework for the game of Binary Homeworlds"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install homeworlds on MacOS using homebrew

- App Name: homeworlds
- App description: C++ framework for the game of Binary Homeworlds
- App Version: 20141022
- App Website: https://github.com/Quuxplusone/Homeworlds/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install homeworlds with the following command
   ```
   brew install homeworlds
   ```
4. homeworlds is ready to use now!