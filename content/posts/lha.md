---
title: "Install lha on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utility for creating and opening lzh archives"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install lha on MacOS using homebrew

- App Name: lha
- App description: Utility for creating and opening lzh archives
- App Version: 1.14i-ac20050924p1
- App Website: https://lha.osdn.jp/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install lha with the following command
   ```
   brew install lha
   ```
4. lha is ready to use now!