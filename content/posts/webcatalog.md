---
title: "Install WebCatalog on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to run web apps like desktop apps"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install WebCatalog on MacOS using homebrew

- App Name: WebCatalog
- App description: Tool to run web apps like desktop apps
- App Version: 41.0.0
- App Website: https://webcatalog.app/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install WebCatalog with the following command
   ```
   brew install --cask webcatalog
   ```
4. WebCatalog is ready to use now!