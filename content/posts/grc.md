---
title: "Install grc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Colorize logfiles and command output"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install grc on MacOS using homebrew

- App Name: grc
- App description: Colorize logfiles and command output
- App Version: 1.13
- App Website: http://kassiopeia.juls.savba.sk/~garabik/software/grc.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install grc with the following command
   ```
   brew install grc
   ```
4. grc is ready to use now!