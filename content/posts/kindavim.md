---
title: "Install kindaVim on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Use Vim in input fields and non input fields"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install kindaVim on MacOS using homebrew

- App Name: kindaVim
- App description: Use Vim in input fields and non input fields
- App Version: 4.0.0
- App Website: https://kindavim.app/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install kindaVim with the following command
   ```
   brew install --cask kindavim
   ```
4. kindaVim is ready to use now!