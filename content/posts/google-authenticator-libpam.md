---
title: "Install google-authenticator-libpam on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "PAM module for two-factor authentication"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install google-authenticator-libpam on MacOS using homebrew

- App Name: google-authenticator-libpam
- App description: PAM module for two-factor authentication
- App Version: 1.09
- App Website: https://github.com/google/google-authenticator-libpam

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install google-authenticator-libpam with the following command
   ```
   brew install google-authenticator-libpam
   ```
4. google-authenticator-libpam is ready to use now!