---
title: "Install eccodes on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Decode and encode messages in the GRIB 1/2 and BUFR 3/4 formats"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install eccodes on MacOS using homebrew

- App Name: eccodes
- App description: Decode and encode messages in the GRIB 1/2 and BUFR 3/4 formats
- App Version: 2.24.2
- App Website: https://confluence.ecmwf.int/display/ECC

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install eccodes with the following command
   ```
   brew install eccodes
   ```
4. eccodes is ready to use now!