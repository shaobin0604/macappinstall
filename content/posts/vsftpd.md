---
title: "Install vsftpd on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Secure FTP server for UNIX"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install vsftpd on MacOS using homebrew

- App Name: vsftpd
- App description: Secure FTP server for UNIX
- App Version: 3.0.5
- App Website: https://security.appspot.com/vsftpd.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install vsftpd with the following command
   ```
   brew install vsftpd
   ```
4. vsftpd is ready to use now!