---
title: "Install Mountain on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Display notifications when mounting/unmounting volumes"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Mountain on MacOS using homebrew

- App Name: Mountain
- App description: Display notifications when mounting/unmounting volumes
- App Version: 1.6.6,54
- App Website: https://appgineers.de/mountain/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Mountain with the following command
   ```
   brew install --cask mountain
   ```
4. Mountain is ready to use now!