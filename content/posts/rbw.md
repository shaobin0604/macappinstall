---
title: "Install rbw on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Unoffical Bitwarden CLI client"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install rbw on MacOS using homebrew

- App Name: rbw
- App description: Unoffical Bitwarden CLI client
- App Version: 1.4.3
- App Website: https://github.com/doy/rbw

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install rbw with the following command
   ```
   brew install rbw
   ```
4. rbw is ready to use now!