---
title: "Install Zoom.us on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Video communication and virtual meeting platform"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Zoom.us on MacOS using homebrew

- App Name: Zoom.us
- App description: Video communication and virtual meeting platform
- App Version: 5.9.3.4239
- App Website: https://www.zoom.us/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Zoom.us with the following command
   ```
   brew install --cask zoom
   ```
4. Zoom.us is ready to use now!