---
title: "Install abnfgen on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Quickly generate random documents that match an ABFN grammar"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install abnfgen on MacOS using homebrew

- App Name: abnfgen
- App description: Quickly generate random documents that match an ABFN grammar
- App Version: 0.20
- App Website: http://www.quut.com/abnfgen/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install abnfgen with the following command
   ```
   brew install abnfgen
   ```
4. abnfgen is ready to use now!