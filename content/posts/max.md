---
title: "Install Max on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Creats high-quality audio files in various formats, from CDs or files"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Max on MacOS using homebrew

- App Name: Max
- App description: Creats high-quality audio files in various formats, from CDs or files
- App Version: 0.9.2b4
- App Website: https://sbooth.org/Max/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Max with the following command
   ```
   brew install --cask max
   ```
4. Max is ready to use now!