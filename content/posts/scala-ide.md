---
title: "Install Scala IDE on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Scala IDE on MacOS using homebrew

- App Name: Scala IDE
- App description: null
- App Version: 4.7.0,2.12,20170929
- App Website: http://scala-ide.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Scala IDE with the following command
   ```
   brew install --cask scala-ide
   ```
4. Scala IDE is ready to use now!