---
title: "Install imageworsener on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utility and library for image scaling and processing"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install imageworsener on MacOS using homebrew

- App Name: imageworsener
- App description: Utility and library for image scaling and processing
- App Version: 1.3.4
- App Website: https://entropymine.com/imageworsener/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install imageworsener with the following command
   ```
   brew install imageworsener
   ```
4. imageworsener is ready to use now!