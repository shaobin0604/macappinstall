---
title: "Install Nault on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Wallet for the Nano cryptocurrency with support for hardware wallets"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Nault on MacOS using homebrew

- App Name: Nault
- App description: Wallet for the Nano cryptocurrency with support for hardware wallets
- App Version: 1.18.0
- App Website: https://github.com/Nault/Nault

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Nault with the following command
   ```
   brew install --cask nault
   ```
4. Nault is ready to use now!