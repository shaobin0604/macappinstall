---
title: "Install root on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Object oriented framework for large scale data analysis"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install root on MacOS using homebrew

- App Name: root
- App description: Object oriented framework for large scale data analysis
- App Version: 6.24.06
- App Website: https://root.cern.ch/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install root with the following command
   ```
   brew install root
   ```
4. root is ready to use now!