---
title: "Install rocksdb on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Embeddable, persistent key-value store for fast storage"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install rocksdb on MacOS using homebrew

- App Name: rocksdb
- App description: Embeddable, persistent key-value store for fast storage
- App Version: 6.28.2
- App Website: https://rocksdb.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install rocksdb with the following command
   ```
   brew install rocksdb
   ```
4. rocksdb is ready to use now!