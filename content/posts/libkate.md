---
title: "Install libkate on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Overlay codec for multiplexed audio/video in Ogg"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libkate on MacOS using homebrew

- App Name: libkate
- App description: Overlay codec for multiplexed audio/video in Ogg
- App Version: 0.4.1
- App Website: https://code.google.com/archive/p/libkate/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libkate with the following command
   ```
   brew install libkate
   ```
4. libkate is ready to use now!