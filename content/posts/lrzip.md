---
title: "Install lrzip on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Compression program with a very high compression ratio"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install lrzip on MacOS using homebrew

- App Name: lrzip
- App description: Compression program with a very high compression ratio
- App Version: 0.641
- App Website: http://lrzip.kolivas.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install lrzip with the following command
   ```
   brew install lrzip
   ```
4. lrzip is ready to use now!