---
title: "Install mscgen on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Parses Message Sequence Chart descriptions and produces images"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mscgen on MacOS using homebrew

- App Name: mscgen
- App description: Parses Message Sequence Chart descriptions and produces images
- App Version: 0.20
- App Website: https://www.mcternan.me.uk/mscgen/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mscgen with the following command
   ```
   brew install mscgen
   ```
4. mscgen is ready to use now!