---
title: "Install sratoolkit on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Data tools for INSDC Sequence Read Archive"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sratoolkit on MacOS using homebrew

- App Name: sratoolkit
- App description: Data tools for INSDC Sequence Read Archive
- App Version: 2.11.3
- App Website: https://github.com/ncbi/sra-tools

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sratoolkit with the following command
   ```
   brew install sratoolkit
   ```
4. sratoolkit is ready to use now!