---
title: "Install freerdp on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "X11 implementation of the Remote Desktop Protocol (RDP)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install freerdp on MacOS using homebrew

- App Name: freerdp
- App description: X11 implementation of the Remote Desktop Protocol (RDP)
- App Version: 2.4.0
- App Website: https://www.freerdp.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install freerdp with the following command
   ```
   brew install freerdp
   ```
4. freerdp is ready to use now!