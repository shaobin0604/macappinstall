---
title: "Install goocanvas on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Canvas widget for GTK+ using the Cairo 2D library for drawing"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install goocanvas on MacOS using homebrew

- App Name: goocanvas
- App description: Canvas widget for GTK+ using the Cairo 2D library for drawing
- App Version: 2.0.4
- App Website: https://wiki.gnome.org/Projects/GooCanvas

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install goocanvas with the following command
   ```
   brew install goocanvas
   ```
4. goocanvas is ready to use now!