---
title: "Install gspell on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Flexible API to implement spellchecking in GTK+ applications"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gspell on MacOS using homebrew

- App Name: gspell
- App description: Flexible API to implement spellchecking in GTK+ applications
- App Version: 1.8.4
- App Website: https://wiki.gnome.org/Projects/gspell

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gspell with the following command
   ```
   brew install gspell
   ```
4. gspell is ready to use now!