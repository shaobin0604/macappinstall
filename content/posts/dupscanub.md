---
title: "Install DupScan on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Searches for duplicated files in specified folders"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install DupScan on MacOS using homebrew

- App Name: DupScan
- App description: Searches for duplicated files in specified folders
- App Version: 2.4.1
- App Website: http://www5.wind.ne.jp/miko/mac_soft/dup_scan/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install DupScan with the following command
   ```
   brew install --cask dupscanub
   ```
4. DupScan is ready to use now!