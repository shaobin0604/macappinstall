---
title: "Install trunk on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Build, bundle & ship your Rust WASM application to the web"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install trunk on MacOS using homebrew

- App Name: trunk
- App description: Build, bundle & ship your Rust WASM application to the web
- App Version: 0.14.0
- App Website: https://github.com/thedodd/trunk

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install trunk with the following command
   ```
   brew install trunk
   ```
4. trunk is ready to use now!