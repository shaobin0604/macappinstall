---
title: "Install woff2 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utilities to create and convert Web Open Font File (WOFF) files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install woff2 on MacOS using homebrew

- App Name: woff2
- App description: Utilities to create and convert Web Open Font File (WOFF) files
- App Version: 1.0.2
- App Website: https://github.com/google/woff2

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install woff2 with the following command
   ```
   brew install woff2
   ```
4. woff2 is ready to use now!