---
title: "Install natalie on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Storyboard Code Generator (for Swift)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install natalie on MacOS using homebrew

- App Name: natalie
- App description: Storyboard Code Generator (for Swift)
- App Version: 0.7.0
- App Website: https://github.com/krzyzanowskim/Natalie

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install natalie with the following command
   ```
   brew install natalie
   ```
4. natalie is ready to use now!