---
title: "Install cruft on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utility that creates projects from templates and maintains the cruft afterwards"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cruft on MacOS using homebrew

- App Name: cruft
- App description: Utility that creates projects from templates and maintains the cruft afterwards
- App Version: 2.10.1
- App Website: https://cruft.github.io/cruft/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cruft with the following command
   ```
   brew install cruft
   ```
4. cruft is ready to use now!