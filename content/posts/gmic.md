---
title: "Install gmic on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Full-Featured Open-Source Framework for Image Processing"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gmic on MacOS using homebrew

- App Name: gmic
- App description: Full-Featured Open-Source Framework for Image Processing
- App Version: 3.0.1
- App Website: https://gmic.eu/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gmic with the following command
   ```
   brew install gmic
   ```
4. gmic is ready to use now!