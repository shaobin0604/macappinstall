---
title: "Install frege on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Non-strict, functional programming language in the spirit of Haskell"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install frege on MacOS using homebrew

- App Name: frege
- App description: Non-strict, functional programming language in the spirit of Haskell
- App Version: 3.24.405
- App Website: https://github.com/Frege/frege/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install frege with the following command
   ```
   brew install frege
   ```
4. frege is ready to use now!