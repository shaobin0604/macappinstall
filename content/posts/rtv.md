---
title: "Install rtv on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line Reddit client"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install rtv on MacOS using homebrew

- App Name: rtv
- App description: Command-line Reddit client
- App Version: 1.27.0
- App Website: https://github.com/michael-lazar/rtv

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install rtv with the following command
   ```
   brew install rtv
   ```
4. rtv is ready to use now!