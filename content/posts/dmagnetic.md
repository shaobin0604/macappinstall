---
title: "Install dmagnetic on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Magnetic Scrolls Interpreter"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dmagnetic on MacOS using homebrew

- App Name: dmagnetic
- App description: Magnetic Scrolls Interpreter
- App Version: 0.32
- App Website: https://www.dettus.net/dMagnetic/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dmagnetic with the following command
   ```
   brew install dmagnetic
   ```
4. dmagnetic is ready to use now!