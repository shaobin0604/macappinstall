---
title: "Install libzt on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Encrypted P2P networking library for applications (GPLv3)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libzt on MacOS using homebrew

- App Name: libzt
- App description: Encrypted P2P networking library for applications (GPLv3)
- App Version: 1.3.3
- App Website: https://www.zerotier.com

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libzt with the following command
   ```
   brew install libzt
   ```
4. libzt is ready to use now!