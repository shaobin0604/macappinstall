---
title: "Install libxc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library of exchange and correlation functionals for codes"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libxc on MacOS using homebrew

- App Name: libxc
- App description: Library of exchange and correlation functionals for codes
- App Version: 5.2.2
- App Website: https://tddft.org/programs/libxc/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libxc with the following command
   ```
   brew install libxc
   ```
4. libxc is ready to use now!