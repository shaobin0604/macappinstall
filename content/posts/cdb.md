---
title: "Install cdb on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Create and read constant databases"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cdb on MacOS using homebrew

- App Name: cdb
- App description: Create and read constant databases
- App Version: 0.75
- App Website: https://cr.yp.to/cdb.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cdb with the following command
   ```
   brew install cdb
   ```
4. cdb is ready to use now!