---
title: "Install microplane on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CLI tool to make git changes across many repos"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install microplane on MacOS using homebrew

- App Name: microplane
- App description: CLI tool to make git changes across many repos
- App Version: 0.0.34
- App Website: https://github.com/Clever/microplane

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install microplane with the following command
   ```
   brew install microplane
   ```
4. microplane is ready to use now!