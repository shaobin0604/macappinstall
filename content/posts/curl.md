---
title: "Install curl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Get a file from an HTTP, HTTPS or FTP server"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install curl on MacOS using homebrew

- App Name: curl
- App description: Get a file from an HTTP, HTTPS or FTP server
- App Version: 7.81.0
- App Website: https://curl.se

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install curl with the following command
   ```
   brew install curl
   ```
4. curl is ready to use now!