---
title: "Install Canon My Image Garden on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Photo editing and printing tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Canon My Image Garden on MacOS using homebrew

- App Name: Canon My Image Garden
- App description: Photo editing and printing tool
- App Version: 3.6.6,04
- App Website: https://support-asia.canon-asia.com/?personal

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Canon My Image Garden with the following command
   ```
   brew install --cask my-image-garden
   ```
4. Canon My Image Garden is ready to use now!