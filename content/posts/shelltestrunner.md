---
title: "Install shelltestrunner on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Portable command-line tool for testing command-line programs"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install shelltestrunner on MacOS using homebrew

- App Name: shelltestrunner
- App description: Portable command-line tool for testing command-line programs
- App Version: 1.9
- App Website: https://github.com/simonmichael/shelltestrunner

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install shelltestrunner with the following command
   ```
   brew install shelltestrunner
   ```
4. shelltestrunner is ready to use now!