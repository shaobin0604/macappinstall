---
title: "Install lsusb on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "List USB devices, just like the Linux lsusb command"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install lsusb on MacOS using homebrew

- App Name: lsusb
- App description: List USB devices, just like the Linux lsusb command
- App Version: 1.0
- App Website: https://github.com/jlhonora/lsusb

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install lsusb with the following command
   ```
   brew install lsusb
   ```
4. lsusb is ready to use now!