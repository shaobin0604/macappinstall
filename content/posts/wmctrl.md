---
title: "Install wmctrl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "UNIX/Linux command-line tool to interact with an EWMH/NetWM"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install wmctrl on MacOS using homebrew

- App Name: wmctrl
- App description: UNIX/Linux command-line tool to interact with an EWMH/NetWM
- App Version: 1.07
- App Website: https://sites.google.com/site/tstyblo/wmctrl

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install wmctrl with the following command
   ```
   brew install wmctrl
   ```
4. wmctrl is ready to use now!