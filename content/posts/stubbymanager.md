---
title: "Install StubbyManager on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install StubbyManager on MacOS using homebrew

- App Name: StubbyManager
- App description: null
- App Version: 0.2.6,6782984
- App Website: https://dnsprivacy.org/wiki/display/DP/Stubby+GUI+for+macOS

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install StubbyManager with the following command
   ```
   brew install --cask stubbymanager
   ```
4. StubbyManager is ready to use now!