---
title: "Install swfmill on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Processor of xml2swf and swf2xml"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install swfmill on MacOS using homebrew

- App Name: swfmill
- App description: Processor of xml2swf and swf2xml
- App Version: 0.3.6
- App Website: https://www.swfmill.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install swfmill with the following command
   ```
   brew install swfmill
   ```
4. swfmill is ready to use now!