---
title: "Install Quail on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Unofficial but officially accepted esa app"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Quail on MacOS using homebrew

- App Name: Quail
- App description: Unofficial but officially accepted esa app
- App Version: 2.4.0
- App Website: https://github.com/1000ch/quail

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Quail with the following command
   ```
   brew install --cask quail
   ```
4. Quail is ready to use now!