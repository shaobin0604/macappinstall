---
title: "Install polyml on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Standard ML implementation"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install polyml on MacOS using homebrew

- App Name: polyml
- App description: Standard ML implementation
- App Version: 5.9
- App Website: https://www.polyml.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install polyml with the following command
   ```
   brew install polyml
   ```
4. polyml is ready to use now!