---
title: "Install RunJS on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "JavaScript playground that auto-evaluates as code is typed"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install RunJS on MacOS using homebrew

- App Name: RunJS
- App description: JavaScript playground that auto-evaluates as code is typed
- App Version: 2.3.0
- App Website: https://runjs.app/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install RunJS with the following command
   ```
   brew install --cask runjs
   ```
4. RunJS is ready to use now!