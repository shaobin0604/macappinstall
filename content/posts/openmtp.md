---
title: "Install OpenMTP on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Android file transfer"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install OpenMTP on MacOS using homebrew

- App Name: OpenMTP
- App description: Android file transfer
- App Version: 3.1.15
- App Website: https://openmtp.ganeshrvel.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install OpenMTP with the following command
   ```
   brew install --cask openmtp
   ```
4. OpenMTP is ready to use now!