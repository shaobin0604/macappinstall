---
title: "Install sixtyforce on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "N64 emulator"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sixtyforce on MacOS using homebrew

- App Name: sixtyforce
- App description: N64 emulator
- App Version: 2.0.2,87
- App Website: https://sixtyforce.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sixtyforce with the following command
   ```
   brew install --cask sixtyforce
   ```
4. sixtyforce is ready to use now!