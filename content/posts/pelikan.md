---
title: "Install pelikan on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Production-ready cache services"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pelikan on MacOS using homebrew

- App Name: pelikan
- App description: Production-ready cache services
- App Version: 0.1.2
- App Website: https://twitter.github.io/pelikan

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pelikan with the following command
   ```
   brew install pelikan
   ```
4. pelikan is ready to use now!