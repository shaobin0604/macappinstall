---
title: "Install smcFanControl on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Sets a minimum speed for built-in fans"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install smcFanControl on MacOS using homebrew

- App Name: smcFanControl
- App description: Sets a minimum speed for built-in fans
- App Version: 2.6
- App Website: https://github.com/hholtmann/smcFanControl

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install smcFanControl with the following command
   ```
   brew install --cask smcfancontrol
   ```
4. smcFanControl is ready to use now!