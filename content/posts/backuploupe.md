---
title: "Install BackupLoupe on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Alternative GUI for Time Machine"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install BackupLoupe on MacOS using homebrew

- App Name: BackupLoupe
- App description: Alternative GUI for Time Machine
- App Version: 3.5.4,2322
- App Website: https://www.soma-zone.com/BackupLoupe/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install BackupLoupe with the following command
   ```
   brew install --cask backuploupe
   ```
4. BackupLoupe is ready to use now!