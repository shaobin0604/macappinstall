---
title: "Install telnetd on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "TELNET server"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install telnetd on MacOS using homebrew

- App Name: telnetd
- App description: TELNET server
- App Version: 63
- App Website: https://opensource.apple.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install telnetd with the following command
   ```
   brew install telnetd
   ```
4. telnetd is ready to use now!