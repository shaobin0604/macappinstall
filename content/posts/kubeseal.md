---
title: "Install kubeseal on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Kubernetes controller and tool for one-way encrypted Secrets"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install kubeseal on MacOS using homebrew

- App Name: kubeseal
- App description: Kubernetes controller and tool for one-way encrypted Secrets
- App Version: 0.17.3
- App Website: https://github.com/bitnami-labs/sealed-secrets

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install kubeseal with the following command
   ```
   brew install kubeseal
   ```
4. kubeseal is ready to use now!