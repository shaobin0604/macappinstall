---
title: "Install chezmoi on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Manage your dotfiles across multiple diverse machines, securely"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install chezmoi on MacOS using homebrew

- App Name: chezmoi
- App description: Manage your dotfiles across multiple diverse machines, securely
- App Version: 2.12.0
- App Website: https://chezmoi.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install chezmoi with the following command
   ```
   brew install chezmoi
   ```
4. chezmoi is ready to use now!