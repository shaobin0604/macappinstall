---
title: "Install libghthash on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Generic hash table for C++"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libghthash on MacOS using homebrew

- App Name: libghthash
- App description: Generic hash table for C++
- App Version: 0.6.2
- App Website: https://web.archive.org/web/20170824230514/www.bth.se/people/ska/sim_home/libghthash.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libghthash with the following command
   ```
   brew install libghthash
   ```
4. libghthash is ready to use now!