---
title: "Install gptfdisk on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Text-mode partitioning tools"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gptfdisk on MacOS using homebrew

- App Name: gptfdisk
- App description: Text-mode partitioning tools
- App Version: 1.0.8
- App Website: https://www.rodsbooks.com/gdisk/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gptfdisk with the following command
   ```
   brew install gptfdisk
   ```
4. gptfdisk is ready to use now!