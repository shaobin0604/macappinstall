---
title: "Install Galleon on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Software wallet for the Tezos blockchain"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Galleon on MacOS using homebrew

- App Name: Galleon
- App description: Software wallet for the Tezos blockchain
- App Version: 1.2.8b
- App Website: https://cryptonomic.tech/galleon.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Galleon with the following command
   ```
   brew install --cask cryptonomic-galleon
   ```
4. Galleon is ready to use now!