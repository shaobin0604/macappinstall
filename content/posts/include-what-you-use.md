---
title: "Install include-what-you-use on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to analyze #includes in C and C++ source files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install include-what-you-use on MacOS using homebrew

- App Name: include-what-you-use
- App description: Tool to analyze #includes in C and C++ source files
- App Version: 0.17
- App Website: https://include-what-you-use.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install include-what-you-use with the following command
   ```
   brew install include-what-you-use
   ```
4. include-what-you-use is ready to use now!