---
title: "Install Dylib Hijack Scanner on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Scans for dylib hijacking"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Dylib Hijack Scanner on MacOS using homebrew

- App Name: Dylib Hijack Scanner
- App description: Scans for dylib hijacking
- App Version: 1.4.1
- App Website: https://objective-see.com/products/dhs.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Dylib Hijack Scanner with the following command
   ```
   brew install --cask dhs
   ```
4. Dylib Hijack Scanner is ready to use now!