---
title: "Install eprover on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Theorem prover for full first-order logic with equality"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install eprover on MacOS using homebrew

- App Name: eprover
- App description: Theorem prover for full first-order logic with equality
- App Version: 2.6
- App Website: https://eprover.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install eprover with the following command
   ```
   brew install eprover
   ```
4. eprover is ready to use now!