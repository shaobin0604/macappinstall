---
title: "Install libphonenumber on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C++ Phone Number library by Google"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libphonenumber on MacOS using homebrew

- App Name: libphonenumber
- App description: C++ Phone Number library by Google
- App Version: 8.12.42
- App Website: https://github.com/google/libphonenumber

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libphonenumber with the following command
   ```
   brew install libphonenumber
   ```
4. libphonenumber is ready to use now!