---
title: "Install ledit on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Line editor for interactive commands"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ledit on MacOS using homebrew

- App Name: ledit
- App description: Line editor for interactive commands
- App Version: 2.05
- App Website: https://pauillac.inria.fr/~ddr/ledit/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ledit with the following command
   ```
   brew install ledit
   ```
4. ledit is ready to use now!