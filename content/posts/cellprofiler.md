---
title: "Install CellProfiler on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Open-source application for biological image analysis"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install CellProfiler on MacOS using homebrew

- App Name: CellProfiler
- App description: Open-source application for biological image analysis
- App Version: 4.2.1
- App Website: https://cellprofiler.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install CellProfiler with the following command
   ```
   brew install --cask cellprofiler
   ```
4. CellProfiler is ready to use now!