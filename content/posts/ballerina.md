---
title: "Install ballerina on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Programming Language for Network Distributed Applications"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ballerina on MacOS using homebrew

- App Name: ballerina
- App description: Programming Language for Network Distributed Applications
- App Version: 2201.0.0
- App Website: https://ballerina.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ballerina with the following command
   ```
   brew install ballerina
   ```
4. ballerina is ready to use now!