---
title: "Install clisp on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GNU CLISP, a Common Lisp implementation"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install clisp on MacOS using homebrew

- App Name: clisp
- App description: GNU CLISP, a Common Lisp implementation
- App Version: 2.49.92
- App Website: https://clisp.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install clisp with the following command
   ```
   brew install clisp
   ```
4. clisp is ready to use now!