---
title: "Install geoipupdate on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Automatic updates of GeoIP2 and GeoIP Legacy databases"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install geoipupdate on MacOS using homebrew

- App Name: geoipupdate
- App description: Automatic updates of GeoIP2 and GeoIP Legacy databases
- App Version: 4.9.0
- App Website: https://github.com/maxmind/geoipupdate

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install geoipupdate with the following command
   ```
   brew install geoipupdate
   ```
4. geoipupdate is ready to use now!