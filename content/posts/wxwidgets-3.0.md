---
title: "Install wxwidgets@3.0 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cross-platform C++ GUI toolkit - Stable Release"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install wxwidgets@3.0 on MacOS using homebrew

- App Name: wxwidgets@3.0
- App description: Cross-platform C++ GUI toolkit - Stable Release
- App Version: 3.0.5.1
- App Website: https://www.wxwidgets.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install wxwidgets@3.0 with the following command
   ```
   brew install wxwidgets@3.0
   ```
4. wxwidgets@3.0 is ready to use now!