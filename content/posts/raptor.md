---
title: "Install raptor on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "RDF parser toolkit"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install raptor on MacOS using homebrew

- App Name: raptor
- App description: RDF parser toolkit
- App Version: 2.0.15
- App Website: https://librdf.org/raptor/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install raptor with the following command
   ```
   brew install raptor
   ```
4. raptor is ready to use now!