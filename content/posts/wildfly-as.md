---
title: "Install wildfly-as on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Managed application runtime for building applications"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install wildfly-as on MacOS using homebrew

- App Name: wildfly-as
- App description: Managed application runtime for building applications
- App Version: 26.0.1
- App Website: https://www.wildfly.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install wildfly-as with the following command
   ```
   brew install wildfly-as
   ```
4. wildfly-as is ready to use now!