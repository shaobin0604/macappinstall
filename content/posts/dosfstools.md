---
title: "Install dosfstools on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tools to create, check and label file systems of the FAT family"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dosfstools on MacOS using homebrew

- App Name: dosfstools
- App description: Tools to create, check and label file systems of the FAT family
- App Version: 4.2
- App Website: https://github.com/dosfstools

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dosfstools with the following command
   ```
   brew install dosfstools
   ```
4. dosfstools is ready to use now!