---
title: "Install task-spooler on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Batch system to run tasks one after another"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install task-spooler on MacOS using homebrew

- App Name: task-spooler
- App description: Batch system to run tasks one after another
- App Version: 1.0.2
- App Website: https://vicerveza.homeunix.net/~viric/soft/ts/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install task-spooler with the following command
   ```
   brew install task-spooler
   ```
4. task-spooler is ready to use now!