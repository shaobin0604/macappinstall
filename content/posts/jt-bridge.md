---
title: "Install JT-Bridge on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Acts as a bridge between WSJT-X and ham radio logging application"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install JT-Bridge on MacOS using homebrew

- App Name: JT-Bridge
- App description: Acts as a bridge between WSJT-X and ham radio logging application
- App Version: 3.4.3
- App Website: https://jt-bridge.eller.nu/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install JT-Bridge with the following command
   ```
   brew install --cask jt-bridge
   ```
4. JT-Bridge is ready to use now!