---
title: "Install aespipe on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "AES encryption or decryption for pipes"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install aespipe on MacOS using homebrew

- App Name: aespipe
- App description: AES encryption or decryption for pipes
- App Version: 2.4f
- App Website: https://loop-aes.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install aespipe with the following command
   ```
   brew install aespipe
   ```
4. aespipe is ready to use now!