---
title: "Install Noun Project on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Icon manager"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Noun Project on MacOS using homebrew

- App Name: Noun Project
- App description: Icon manager
- App Version: 2.2.5,641
- App Website: https://thenounproject.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Noun Project with the following command
   ```
   brew install --cask noun-project
   ```
4. Noun Project is ready to use now!