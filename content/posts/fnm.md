---
title: "Install fnm on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Fast and simple Node.js version manager"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fnm on MacOS using homebrew

- App Name: fnm
- App description: Fast and simple Node.js version manager
- App Version: 1.31.0
- App Website: https://github.com/Schniz/fnm

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fnm with the following command
   ```
   brew install fnm
   ```
4. fnm is ready to use now!