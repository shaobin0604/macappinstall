---
title: "Install gnupg-pkcs11-scd on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Enable the use of PKCS#11 tokens with GnuPG"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gnupg-pkcs11-scd on MacOS using homebrew

- App Name: gnupg-pkcs11-scd
- App description: Enable the use of PKCS#11 tokens with GnuPG
- App Version: 0.10.0
- App Website: https://gnupg-pkcs11.sourceforge.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gnupg-pkcs11-scd with the following command
   ```
   brew install gnupg-pkcs11-scd
   ```
4. gnupg-pkcs11-scd is ready to use now!