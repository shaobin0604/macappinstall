---
title: "Install doxygen on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Generate documentation for several programming languages"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install doxygen on MacOS using homebrew

- App Name: doxygen
- App description: Generate documentation for several programming languages
- App Version: 1.9.3
- App Website: https://www.doxygen.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install doxygen with the following command
   ```
   brew install doxygen
   ```
4. doxygen is ready to use now!