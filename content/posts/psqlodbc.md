---
title: "Install psqlodbc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Official PostgreSQL ODBC driver"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install psqlodbc on MacOS using homebrew

- App Name: psqlodbc
- App description: Official PostgreSQL ODBC driver
- App Version: 13.02.0000
- App Website: https://odbc.postgresql.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install psqlodbc with the following command
   ```
   brew install psqlodbc
   ```
4. psqlodbc is ready to use now!