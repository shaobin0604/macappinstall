---
title: "Install EagleFiler on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Organize files, archive e-mails, save Web pages and notes, search everything"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install EagleFiler on MacOS using homebrew

- App Name: EagleFiler
- App description: Organize files, archive e-mails, save Web pages and notes, search everything
- App Version: 1.9.6
- App Website: https://c-command.com/eaglefiler/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install EagleFiler with the following command
   ```
   brew install --cask eaglefiler
   ```
4. EagleFiler is ready to use now!