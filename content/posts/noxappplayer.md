---
title: "Install NoxAppPlayer on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Android emulator to play mobile games"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install NoxAppPlayer on MacOS using homebrew

- App Name: NoxAppPlayer
- App description: Android emulator to play mobile games
- App Version: 3.8.5.7,20211216,23d583db237f4401b01bd0f549acfda1
- App Website: https://www.bignox.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install NoxAppPlayer with the following command
   ```
   brew install --cask noxappplayer
   ```
4. NoxAppPlayer is ready to use now!