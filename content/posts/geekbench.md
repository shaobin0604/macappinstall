---
title: "Install Geekbench on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to measure the computer system's performance"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Geekbench on MacOS using homebrew

- App Name: Geekbench
- App description: Tool to measure the computer system's performance
- App Version: 5.4.4,503875
- App Website: https://www.geekbench.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Geekbench with the following command
   ```
   brew install --cask geekbench
   ```
4. Geekbench is ready to use now!