---
title: "Install spdylay on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Experimental implementation of SPDY protocol versions 2, 3, and 3.1"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install spdylay on MacOS using homebrew

- App Name: spdylay
- App description: Experimental implementation of SPDY protocol versions 2, 3, and 3.1
- App Version: 1.4.0
- App Website: https://github.com/tatsuhiro-t/spdylay

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install spdylay with the following command
   ```
   brew install spdylay
   ```
4. spdylay is ready to use now!