---
title: "Install fzf on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line fuzzy finder written in Go"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fzf on MacOS using homebrew

- App Name: fzf
- App description: Command-line fuzzy finder written in Go
- App Version: 0.29.0
- App Website: https://github.com/junegunn/fzf

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fzf with the following command
   ```
   brew install fzf
   ```
4. fzf is ready to use now!