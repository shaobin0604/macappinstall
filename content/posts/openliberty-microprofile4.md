---
title: "Install openliberty-microprofile4 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Lightweight open framework for Java (Micro Profile 4)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install openliberty-microprofile4 on MacOS using homebrew

- App Name: openliberty-microprofile4
- App description: Lightweight open framework for Java (Micro Profile 4)
- App Version: 22.0.0.1
- App Website: https://openliberty.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install openliberty-microprofile4 with the following command
   ```
   brew install openliberty-microprofile4
   ```
4. openliberty-microprofile4 is ready to use now!