---
title: "Install mruby-cli on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Build native command-line applications for Linux, MacOS, and Windows"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mruby-cli on MacOS using homebrew

- App Name: mruby-cli
- App description: Build native command-line applications for Linux, MacOS, and Windows
- App Version: 0.0.4
- App Website: https://github.com/hone/mruby-cli

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mruby-cli with the following command
   ```
   brew install mruby-cli
   ```
4. mruby-cli is ready to use now!