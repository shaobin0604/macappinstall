---
title: "Install ldpl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Compiled programming language inspired by COBOL"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ldpl on MacOS using homebrew

- App Name: ldpl
- App description: Compiled programming language inspired by COBOL
- App Version: 4.4
- App Website: https://www.ldpl-lang.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ldpl with the following command
   ```
   brew install ldpl
   ```
4. ldpl is ready to use now!