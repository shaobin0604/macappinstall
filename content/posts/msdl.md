---
title: "Install msdl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Downloader for various streaming protocols"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install msdl on MacOS using homebrew

- App Name: msdl
- App description: Downloader for various streaming protocols
- App Version: 1.2.7-r2
- App Website: https://msdl.sourceforge.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install msdl with the following command
   ```
   brew install msdl
   ```
4. msdl is ready to use now!