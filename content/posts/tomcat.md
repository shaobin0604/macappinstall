---
title: "Install tomcat on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Implementation of Java Servlet and JavaServer Pages"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tomcat on MacOS using homebrew

- App Name: tomcat
- App description: Implementation of Java Servlet and JavaServer Pages
- App Version: 10.0.16
- App Website: https://tomcat.apache.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tomcat with the following command
   ```
   brew install tomcat
   ```
4. tomcat is ready to use now!