---
title: "Install Chatology on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Chat manager and message search software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Chatology on MacOS using homebrew

- App Name: Chatology
- App description: Chat manager and message search software
- App Version: 1.2.5
- App Website: https://flexibits.com/chatology

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Chatology with the following command
   ```
   brew install --cask chatology
   ```
4. Chatology is ready to use now!