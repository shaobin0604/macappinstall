---
title: "Install magic-wormhole on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Securely transfers data between computers"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install magic-wormhole on MacOS using homebrew

- App Name: magic-wormhole
- App description: Securely transfers data between computers
- App Version: 0.12.0
- App Website: https://github.com/warner/magic-wormhole

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install magic-wormhole with the following command
   ```
   brew install magic-wormhole
   ```
4. magic-wormhole is ready to use now!