---
title: "Install Biscuit on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Browser to organize apps"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Biscuit on MacOS using homebrew

- App Name: Biscuit
- App description: Browser to organize apps
- App Version: 1.2.24
- App Website: https://eatbiscuit.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Biscuit with the following command
   ```
   brew install --cask biscuit
   ```
4. Biscuit is ready to use now!