---
title: "Install flvstreamer on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Stream audio and video from flash & RTMP Servers"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install flvstreamer on MacOS using homebrew

- App Name: flvstreamer
- App description: Stream audio and video from flash & RTMP Servers
- App Version: 2.1c1
- App Website: https://www.nongnu.org/flvstreamer/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install flvstreamer with the following command
   ```
   brew install flvstreamer
   ```
4. flvstreamer is ready to use now!