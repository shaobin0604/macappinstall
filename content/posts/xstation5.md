---
title: "Install xStation5 on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Desktop trading platform"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install xStation5 on MacOS using homebrew

- App Name: xStation5
- App description: Desktop trading platform
- App Version: 2.35.2-Build.3
- App Website: https://www.xtb.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install xStation5 with the following command
   ```
   brew install --cask xstation5
   ```
4. xStation5 is ready to use now!