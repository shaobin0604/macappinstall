---
title: "Install Git-it on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Desktop app for learning Git and GitHub"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Git-it on MacOS using homebrew

- App Name: Git-it
- App description: Desktop app for learning Git and GitHub
- App Version: 4.4.0
- App Website: https://github.com/jlord/git-it-electron

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Git-it with the following command
   ```
   brew install --cask git-it
   ```
4. Git-it is ready to use now!