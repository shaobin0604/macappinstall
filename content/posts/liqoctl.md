---
title: "Install liqoctl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Is a CLI tool to install and manage Liqo-enabled clusters"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install liqoctl on MacOS using homebrew

- App Name: liqoctl
- App description: Is a CLI tool to install and manage Liqo-enabled clusters
- App Version: 0.3.2
- App Website: https://liqo.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install liqoctl with the following command
   ```
   brew install liqoctl
   ```
4. liqoctl is ready to use now!