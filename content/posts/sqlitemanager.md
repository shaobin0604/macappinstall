---
title: "Install SQLiteManager on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Database management system for sqlite databases"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install SQLiteManager on MacOS using homebrew

- App Name: SQLiteManager
- App description: Database management system for sqlite databases
- App Version: 4.8.4
- App Website: https://www.sqlabs.com/sqlitemanager.php

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install SQLiteManager with the following command
   ```
   brew install --cask sqlitemanager
   ```
4. SQLiteManager is ready to use now!