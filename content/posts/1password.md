---
title: "Install 1Password on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Password manager that keeps all passwords secure behind one password"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install 1Password on MacOS using homebrew

- App Name: 1Password
- App description: Password manager that keeps all passwords secure behind one password
- App Version: 7.9.2
- App Website: https://1password.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install 1Password with the following command
   ```
   brew install --cask 1password
   ```
4. 1Password is ready to use now!