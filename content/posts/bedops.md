---
title: "Install bedops on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Set and statistical operations on genomic data of arbitrary scale"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bedops on MacOS using homebrew

- App Name: bedops
- App description: Set and statistical operations on genomic data of arbitrary scale
- App Version: 2.4.40
- App Website: https://github.com/bedops/bedops

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bedops with the following command
   ```
   brew install bedops
   ```
4. bedops is ready to use now!