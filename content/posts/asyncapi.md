---
title: "Install asyncapi on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "All in one CLI for all AsyncAPI tools"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install asyncapi on MacOS using homebrew

- App Name: asyncapi
- App description: All in one CLI for all AsyncAPI tools
- App Version: 0.13.3
- App Website: https://github.com/asyncapi/cli

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install asyncapi with the following command
   ```
   brew install asyncapi
   ```
4. asyncapi is ready to use now!