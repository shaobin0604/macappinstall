---
title: "Install Mockuuups Studio on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Allows designers and marketers to drag and drop visuals into scenes"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Mockuuups Studio on MacOS using homebrew

- App Name: Mockuuups Studio
- App description: Allows designers and marketers to drag and drop visuals into scenes
- App Version: 3.5.0
- App Website: https://mockuuups.studio/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Mockuuups Studio with the following command
   ```
   brew install --cask mockuuups-studio
   ```
4. Mockuuups Studio is ready to use now!