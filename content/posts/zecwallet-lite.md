---
title: "Install Zecwallet Lite on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Zcash Light Wallet"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Zecwallet Lite on MacOS using homebrew

- App Name: Zecwallet Lite
- App description: Zcash Light Wallet
- App Version: 1.7.8
- App Website: https://www.zecwallet.co/#download

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Zecwallet Lite with the following command
   ```
   brew install --cask zecwallet-lite
   ```
4. Zecwallet Lite is ready to use now!