---
title: "Install OpenLoco on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Open source re-implementation of Chris Sawyer's Locomotion"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install OpenLoco on MacOS using homebrew

- App Name: OpenLoco
- App description: Open source re-implementation of Chris Sawyer's Locomotion
- App Version: 22.02
- App Website: https://github.com/OpenLoco/OpenLoco

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install OpenLoco with the following command
   ```
   brew install --cask openloco
   ```
4. OpenLoco is ready to use now!