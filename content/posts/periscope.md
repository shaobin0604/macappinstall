---
title: "Install periscope on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Organize and de-duplicate your files without losing data"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install periscope on MacOS using homebrew

- App Name: periscope
- App description: Organize and de-duplicate your files without losing data
- App Version: 0.3.2
- App Website: https://github.com/anishathalye/periscope

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install periscope with the following command
   ```
   brew install periscope
   ```
4. periscope is ready to use now!