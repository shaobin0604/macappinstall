---
title: "Install Th-MakerX on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Th-MakerX on MacOS using homebrew

- App Name: Th-MakerX
- App description: null
- App Version: 4.0.0
- App Website: http://www5.wind.ne.jp/miko/mac_soft/th-maker_x/index-en.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Th-MakerX with the following command
   ```
   brew install --cask th-makerx
   ```
4. Th-MakerX is ready to use now!