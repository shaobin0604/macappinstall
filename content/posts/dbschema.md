---
title: "Install DbSchema on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Design, document and deploy databases"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install DbSchema on MacOS using homebrew

- App Name: DbSchema
- App description: Design, document and deploy databases
- App Version: 8.5.1
- App Website: https://www.dbschema.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install DbSchema with the following command
   ```
   brew install --cask dbschema
   ```
4. DbSchema is ready to use now!