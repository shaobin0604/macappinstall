---
title: "Install ec2-ami-tools on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Amazon EC2 AMI Tools (helps bundle Amazon Machine Images)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ec2-ami-tools on MacOS using homebrew

- App Name: ec2-ami-tools
- App description: Amazon EC2 AMI Tools (helps bundle Amazon Machine Images)
- App Version: 1.5.7
- App Website: https://aws.amazon.com/developertools/368

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ec2-ami-tools with the following command
   ```
   brew install ec2-ami-tools
   ```
4. ec2-ami-tools is ready to use now!