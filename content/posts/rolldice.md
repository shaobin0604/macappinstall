---
title: "Install rolldice on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Rolls an amount of virtual dice"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install rolldice on MacOS using homebrew

- App Name: rolldice
- App description: Rolls an amount of virtual dice
- App Version: 1.16
- App Website: https://github.com/sstrickl/rolldice

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install rolldice with the following command
   ```
   brew install rolldice
   ```
4. rolldice is ready to use now!