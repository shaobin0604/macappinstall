---
title: "Install Quit All on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Quickly quit one, some, or all apps"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Quit All on MacOS using homebrew

- App Name: Quit All
- App description: Quickly quit one, some, or all apps
- App Version: 1.1.1
- App Website: https://amicoapps.com/app/quit-all/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Quit All with the following command
   ```
   brew install --cask quit-all
   ```
4. Quit All is ready to use now!