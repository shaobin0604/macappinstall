---
title: "Install ioke on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Dynamic language targeted at virtual machines"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ioke on MacOS using homebrew

- App Name: ioke
- App description: Dynamic language targeted at virtual machines
- App Version: 0.4.0
- App Website: https://ioke.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ioke with the following command
   ```
   brew install ioke
   ```
4. ioke is ready to use now!