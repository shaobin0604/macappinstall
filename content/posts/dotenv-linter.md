---
title: "Install dotenv-linter on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Lightning-fast linter for .env files written in Rust"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dotenv-linter on MacOS using homebrew

- App Name: dotenv-linter
- App description: Lightning-fast linter for .env files written in Rust
- App Version: 3.2.0
- App Website: https://dotenv-linter.github.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dotenv-linter with the following command
   ```
   brew install dotenv-linter
   ```
4. dotenv-linter is ready to use now!