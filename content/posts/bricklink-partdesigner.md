---
title: "Install PartDesigner on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Design your own LEGO parts"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install PartDesigner on MacOS using homebrew

- App Name: PartDesigner
- App description: Design your own LEGO parts
- App Version: 1.0.6_5
- App Website: https://bricklink.com/v3/studio/partdesigner.page

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install PartDesigner with the following command
   ```
   brew install --cask bricklink-partdesigner
   ```
4. PartDesigner is ready to use now!