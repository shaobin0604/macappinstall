---
title: "Install Chia Blockchain on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GUI Python implementation for the Chia blockchain"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Chia Blockchain on MacOS using homebrew

- App Name: Chia Blockchain
- App description: GUI Python implementation for the Chia blockchain
- App Version: 1.2.11
- App Website: https://www.chia.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Chia Blockchain with the following command
   ```
   brew install --cask chia
   ```
4. Chia Blockchain is ready to use now!