---
title: "Install Golly on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Explore Conway's Game of Life and other types of cellular automata"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Golly on MacOS using homebrew

- App Name: Golly
- App description: Explore Conway's Game of Life and other types of cellular automata
- App Version: 4.1
- App Website: https://golly.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Golly with the following command
   ```
   brew install --cask golly
   ```
4. Golly is ready to use now!