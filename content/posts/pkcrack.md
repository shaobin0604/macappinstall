---
title: "Install pkcrack on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Implementation of an algorithm for breaking the PkZip cipher"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pkcrack on MacOS using homebrew

- App Name: pkcrack
- App description: Implementation of an algorithm for breaking the PkZip cipher
- App Version: 1.2.2
- App Website: https://www.unix-ag.uni-kl.de/~conrad/krypto/pkcrack.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pkcrack with the following command
   ```
   brew install pkcrack
   ```
4. pkcrack is ready to use now!