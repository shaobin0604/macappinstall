---
title: "Install protoc-gen-gogo on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Protocol Buffers for Go with Gadgets"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install protoc-gen-gogo on MacOS using homebrew

- App Name: protoc-gen-gogo
- App description: Protocol Buffers for Go with Gadgets
- App Version: 1.3.2
- App Website: https://github.com/gogo/protobuf

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install protoc-gen-gogo with the following command
   ```
   brew install protoc-gen-gogo
   ```
4. protoc-gen-gogo is ready to use now!