---
title: "Install OmniDiskSweeper on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Finds large, unwanted files and deletes them"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install OmniDiskSweeper on MacOS using homebrew

- App Name: OmniDiskSweeper
- App description: Finds large, unwanted files and deletes them
- App Version: 1.13
- App Website: https://www.omnigroup.com/more/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install OmniDiskSweeper with the following command
   ```
   brew install --cask omnidisksweeper
   ```
4. OmniDiskSweeper is ready to use now!