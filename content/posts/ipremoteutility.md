---
title: "Install Flanders IP Remote Utility on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Management of Flanders Scientific hardware"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Flanders IP Remote Utility on MacOS using homebrew

- App Name: Flanders IP Remote Utility
- App description: Management of Flanders Scientific hardware
- App Version: 1.8.9
- App Website: https://www.flandersscientific.com/ip-remote/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Flanders IP Remote Utility with the following command
   ```
   brew install --cask ipremoteutility
   ```
4. Flanders IP Remote Utility is ready to use now!