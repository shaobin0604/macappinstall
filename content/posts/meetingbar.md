---
title: "Install MeetingBar on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Shows the next meeting in the menu bar"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install MeetingBar on MacOS using homebrew

- App Name: MeetingBar
- App description: Shows the next meeting in the menu bar
- App Version: 3.10.1
- App Website: https://github.com/leits/MeetingBar

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install MeetingBar with the following command
   ```
   brew install --cask meetingbar
   ```
4. MeetingBar is ready to use now!