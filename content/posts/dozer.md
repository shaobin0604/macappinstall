---
title: "Install Dozer on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to hide status bar icons"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Dozer on MacOS using homebrew

- App Name: Dozer
- App description: Tool to hide status bar icons
- App Version: 4.0.0
- App Website: https://github.com/Mortennn/Dozer

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Dozer with the following command
   ```
   brew install --cask dozer
   ```
4. Dozer is ready to use now!