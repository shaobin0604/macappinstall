---
title: "Install ospray on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Ray-tracing-based rendering engine for high-fidelity visualization"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ospray on MacOS using homebrew

- App Name: ospray
- App description: Ray-tracing-based rendering engine for high-fidelity visualization
- App Version: 2.9.0
- App Website: https://www.ospray.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ospray with the following command
   ```
   brew install ospray
   ```
4. ospray is ready to use now!