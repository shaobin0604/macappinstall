---
title: "Install sql-translator on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Manipulate structured data definitions (SQL and more)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sql-translator on MacOS using homebrew

- App Name: sql-translator
- App description: Manipulate structured data definitions (SQL and more)
- App Version: 1.62
- App Website: https://github.com/dbsrgits/sql-translator/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sql-translator with the following command
   ```
   brew install sql-translator
   ```
4. sql-translator is ready to use now!