---
title: "Install squashfs on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Compressed read-only file system for Linux"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install squashfs on MacOS using homebrew

- App Name: squashfs
- App description: Compressed read-only file system for Linux
- App Version: 4.5
- App Website: https://github.com/plougher/squashfs-tools

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install squashfs with the following command
   ```
   brew install squashfs
   ```
4. squashfs is ready to use now!