---
title: "Install Chiaki on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "PlayStation remote play client"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Chiaki on MacOS using homebrew

- App Name: Chiaki
- App description: PlayStation remote play client
- App Version: 2.1.1
- App Website: https://git.sr.ht/~thestr4ng3r/chiaki

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Chiaki with the following command
   ```
   brew install --cask chiaki
   ```
4. Chiaki is ready to use now!