---
title: "Install SpiderOak One Backup on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cloud backup and storage"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install SpiderOak One Backup on MacOS using homebrew

- App Name: SpiderOak One Backup
- App description: Cloud backup and storage
- App Version: 7.5.0
- App Website: https://spideroak.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install SpiderOak One Backup with the following command
   ```
   brew install --cask spideroakone
   ```
4. SpiderOak One Backup is ready to use now!