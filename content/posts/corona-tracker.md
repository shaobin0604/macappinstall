---
title: "Install Corona Tracker on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Coronavirus tracker app with maps and charts"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Corona Tracker on MacOS using homebrew

- App Name: Corona Tracker
- App description: Coronavirus tracker app with maps and charts
- App Version: 1.7.2
- App Website: https://coronatracker.samabox.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Corona Tracker with the following command
   ```
   brew install --cask corona-tracker
   ```
4. Corona Tracker is ready to use now!