---
title: "Install dynamips on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cisco 7200/3600/3725/3745/2600/1700 Router Emulator"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dynamips on MacOS using homebrew

- App Name: dynamips
- App description: Cisco 7200/3600/3725/3745/2600/1700 Router Emulator
- App Version: 0.2.21
- App Website: https://github.com/GNS3/dynamips

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dynamips with the following command
   ```
   brew install dynamips
   ```
4. dynamips is ready to use now!