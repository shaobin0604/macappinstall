---
title: "Install LuxMark on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "OpenCL benchmark"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install LuxMark on MacOS using homebrew

- App Name: LuxMark
- App description: OpenCL benchmark
- App Version: 3.1
- App Website: http://www.luxmark.info/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install LuxMark with the following command
   ```
   brew install --cask luxmark
   ```
4. LuxMark is ready to use now!