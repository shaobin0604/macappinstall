---
title: "Install Klokki on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Automatic time-tracking solution or freelancers and makers"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Klokki on MacOS using homebrew

- App Name: Klokki
- App description: Automatic time-tracking solution or freelancers and makers
- App Version: 1.3.3,62
- App Website: https://klokki.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Klokki with the following command
   ```
   brew install --cask klokki
   ```
4. Klokki is ready to use now!