---
title: "Install MacX DVD Ripper Pro on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "DVD ripping application"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install MacX DVD Ripper Pro on MacOS using homebrew

- App Name: MacX DVD Ripper Pro
- App description: DVD ripping application
- App Version: 6.5.5,20210105
- App Website: https://www.macxdvd.com/mac-dvd-ripper-pro/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install MacX DVD Ripper Pro with the following command
   ```
   brew install --cask macx-dvd-ripper-pro
   ```
4. MacX DVD Ripper Pro is ready to use now!