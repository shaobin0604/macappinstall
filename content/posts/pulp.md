---
title: "Install pulp on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Build tool for PureScript projects"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pulp on MacOS using homebrew

- App Name: pulp
- App description: Build tool for PureScript projects
- App Version: 15.0.0
- App Website: https://github.com/bodil/pulp

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pulp with the following command
   ```
   brew install pulp
   ```
4. pulp is ready to use now!