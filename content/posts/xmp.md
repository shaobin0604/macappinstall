---
title: "Install xmp on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line player for module music formats (MOD, S3M, IT, etc)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install xmp on MacOS using homebrew

- App Name: xmp
- App description: Command-line player for module music formats (MOD, S3M, IT, etc)
- App Version: 4.1.0
- App Website: https://xmp.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install xmp with the following command
   ```
   brew install xmp
   ```
4. xmp is ready to use now!