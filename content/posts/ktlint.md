---
title: "Install ktlint on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Anti-bikeshedding Kotlin linter with built-in formatter"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ktlint on MacOS using homebrew

- App Name: ktlint
- App description: Anti-bikeshedding Kotlin linter with built-in formatter
- App Version: 0.44.0
- App Website: https://ktlint.github.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ktlint with the following command
   ```
   brew install ktlint
   ```
4. ktlint is ready to use now!