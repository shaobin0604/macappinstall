---
title: "Install httpx on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Fast and multi-purpose HTTP toolkit"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install httpx on MacOS using homebrew

- App Name: httpx
- App description: Fast and multi-purpose HTTP toolkit
- App Version: 1.1.5
- App Website: https://github.com/projectdiscovery/httpx

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install httpx with the following command
   ```
   brew install httpx
   ```
4. httpx is ready to use now!