---
title: "Install tcping on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "TCP connect to the given IP/port combo"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tcping on MacOS using homebrew

- App Name: tcping
- App description: TCP connect to the given IP/port combo
- App Version: 1.3.6
- App Website: https://github.com/mkirchner/tcping

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tcping with the following command
   ```
   brew install tcping
   ```
4. tcping is ready to use now!