---
title: "Install gerbv on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Gerber (RS-274X) viewer"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gerbv on MacOS using homebrew

- App Name: gerbv
- App description: Gerber (RS-274X) viewer
- App Version: 2.7.0
- App Website: http://gerbv.gpleda.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gerbv with the following command
   ```
   brew install gerbv
   ```
4. gerbv is ready to use now!