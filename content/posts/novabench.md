---
title: "Install Novabench on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Benchmark tool to quickly test and compare the computer's performance"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Novabench on MacOS using homebrew

- App Name: Novabench
- App description: Benchmark tool to quickly test and compare the computer's performance
- App Version: 4.0.2,11
- App Website: https://novabench.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Novabench with the following command
   ```
   brew install --cask novabench
   ```
4. Novabench is ready to use now!