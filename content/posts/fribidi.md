---
title: "Install fribidi on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Implementation of the Unicode BiDi algorithm"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fribidi on MacOS using homebrew

- App Name: fribidi
- App description: Implementation of the Unicode BiDi algorithm
- App Version: 1.0.11
- App Website: https://github.com/fribidi/fribidi

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fribidi with the following command
   ```
   brew install fribidi
   ```
4. fribidi is ready to use now!