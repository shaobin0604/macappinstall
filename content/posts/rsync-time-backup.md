---
title: "Install rsync-time-backup on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Time Machine-style backup for the terminal using rsync"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install rsync-time-backup on MacOS using homebrew

- App Name: rsync-time-backup
- App description: Time Machine-style backup for the terminal using rsync
- App Version: 1.1.5
- App Website: https://github.com/laurent22/rsync-time-backup

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install rsync-time-backup with the following command
   ```
   brew install rsync-time-backup
   ```
4. rsync-time-backup is ready to use now!