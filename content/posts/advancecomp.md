---
title: "Install advancecomp on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Recompression utilities for .PNG, .MNG, .ZIP, and .GZ files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install advancecomp on MacOS using homebrew

- App Name: advancecomp
- App description: Recompression utilities for .PNG, .MNG, .ZIP, and .GZ files
- App Version: 2.1
- App Website: https://www.advancemame.it/comp-readme.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install advancecomp with the following command
   ```
   brew install advancecomp
   ```
4. advancecomp is ready to use now!