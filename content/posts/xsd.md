---
title: "Install xsd on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "XML Data Binding for C++"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install xsd on MacOS using homebrew

- App Name: xsd
- App description: XML Data Binding for C++
- App Version: 4.0.0
- App Website: https://www.codesynthesis.com/products/xsd/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install xsd with the following command
   ```
   brew install xsd
   ```
4. xsd is ready to use now!