---
title: "Install aamath on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Renders mathematical expressions as ASCII art"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install aamath on MacOS using homebrew

- App Name: aamath
- App description: Renders mathematical expressions as ASCII art
- App Version: 0.3
- App Website: http://fuse.superglue.se/aamath/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install aamath with the following command
   ```
   brew install aamath
   ```
4. aamath is ready to use now!