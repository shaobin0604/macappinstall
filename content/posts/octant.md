---
title: "Install octant on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Kubernetes introspection tool for developers"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install octant on MacOS using homebrew

- App Name: octant
- App description: Kubernetes introspection tool for developers
- App Version: 0.25.0
- App Website: https://octant.dev

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install octant with the following command
   ```
   brew install octant
   ```
4. octant is ready to use now!