---
title: "Install Yakyak on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Desktop chat client for Google Hangouts"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Yakyak on MacOS using homebrew

- App Name: Yakyak
- App description: Desktop chat client for Google Hangouts
- App Version: 1.5.11.5
- App Website: https://github.com/yakyak/yakyak

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Yakyak with the following command
   ```
   brew install --cask yakyak
   ```
4. Yakyak is ready to use now!