---
title: "Install universal-ctags on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Maintained ctags implementation"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install universal-ctags on MacOS using homebrew

- App Name: universal-ctags
- App description: Maintained ctags implementation
- App Version: p5.9.20220213.0
- App Website: https://github.com/universal-ctags/ctags

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install universal-ctags with the following command
   ```
   brew install universal-ctags
   ```
4. universal-ctags is ready to use now!