---
title: "Install Disk Drill on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Data recovery software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Disk Drill on MacOS using homebrew

- App Name: Disk Drill
- App description: Data recovery software
- App Version: 4.6.370
- App Website: https://www.cleverfiles.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Disk Drill with the following command
   ```
   brew install --cask disk-drill
   ```
4. Disk Drill is ready to use now!