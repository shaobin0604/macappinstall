---
title: "Install Tencent Lemon Cleaner on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cleanup and system status tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Tencent Lemon Cleaner on MacOS using homebrew

- App Name: Tencent Lemon Cleaner
- App description: Cleanup and system status tool
- App Version: 5.0.4_2
- App Website: https://lemon.qq.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Tencent Lemon Cleaner with the following command
   ```
   brew install --cask tencent-lemon
   ```
4. Tencent Lemon Cleaner is ready to use now!