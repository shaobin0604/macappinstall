---
title: "Install Devolo dLAN Cockpit on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Configuration and network monitoring software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Devolo dLAN Cockpit on MacOS using homebrew

- App Name: Devolo dLAN Cockpit
- App description: Configuration and network monitoring software
- App Version: 5.1.6.2
- App Website: https://www.devolo.global/devolo-cockpit

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Devolo dLAN Cockpit with the following command
   ```
   brew install --cask devolo-cockpit
   ```
4. Devolo dLAN Cockpit is ready to use now!