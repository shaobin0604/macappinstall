---
title: "Install kickstart on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Scaffolding tool to get new projects up and running quickly"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install kickstart on MacOS using homebrew

- App Name: kickstart
- App description: Scaffolding tool to get new projects up and running quickly
- App Version: 0.3.0
- App Website: https://github.com/Keats/kickstart

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install kickstart with the following command
   ```
   brew install kickstart
   ```
4. kickstart is ready to use now!