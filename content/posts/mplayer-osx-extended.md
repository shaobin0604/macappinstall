---
title: "Install MPlayer OSX Extended on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Video player thats uses MPlayer as backend"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install MPlayer OSX Extended on MacOS using homebrew

- App Name: MPlayer OSX Extended
- App description: Video player thats uses MPlayer as backend
- App Version: 16
- App Website: https://mplayerosx.ch/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install MPlayer OSX Extended with the following command
   ```
   brew install --cask mplayer-osx-extended
   ```
4. MPlayer OSX Extended is ready to use now!