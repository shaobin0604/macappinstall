---
title: "Install iSlide on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "PPT-based plug-in tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install iSlide on MacOS using homebrew

- App Name: iSlide
- App description: PPT-based plug-in tool
- App Version: 1.2.0
- App Website: https://www.islide.cc/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install iSlide with the following command
   ```
   brew install --cask islide
   ```
4. iSlide is ready to use now!