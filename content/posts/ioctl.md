---
title: "Install ioctl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line interface for interacting with the IoTeX blockchain"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ioctl on MacOS using homebrew

- App Name: ioctl
- App description: Command-line interface for interacting with the IoTeX blockchain
- App Version: 1.6.3
- App Website: https://docs.iotex.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ioctl with the following command
   ```
   brew install ioctl
   ```
4. ioctl is ready to use now!