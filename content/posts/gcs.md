---
title: "Install gcs on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Character sheet editor for the GURPS Fourth Edition roleplaying game"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gcs on MacOS using homebrew

- App Name: gcs
- App description: Character sheet editor for the GURPS Fourth Edition roleplaying game
- App Version: 4.37.1
- App Website: https://gurpscharactersheet.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gcs with the following command
   ```
   brew install --cask gcs
   ```
4. gcs is ready to use now!