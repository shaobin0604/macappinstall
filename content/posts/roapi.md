---
title: "Install roapi on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Full-fledged APIs for static datasets without writing a single line of code"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install roapi on MacOS using homebrew

- App Name: roapi
- App description: Full-fledged APIs for static datasets without writing a single line of code
- App Version: 0.5.3
- App Website: https://roapi.github.io/docs

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install roapi with the following command
   ```
   brew install roapi
   ```
4. roapi is ready to use now!