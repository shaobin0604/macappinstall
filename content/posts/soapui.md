---
title: "Install SmartBear SoapUI on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "API testing tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install SmartBear SoapUI on MacOS using homebrew

- App Name: SmartBear SoapUI
- App description: API testing tool
- App Version: 5.7.0
- App Website: https://www.soapui.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install SmartBear SoapUI with the following command
   ```
   brew install --cask soapui
   ```
4. SmartBear SoapUI is ready to use now!