---
title: "Install vdirsyncer on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Synchronize calendars and contacts"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install vdirsyncer on MacOS using homebrew

- App Name: vdirsyncer
- App description: Synchronize calendars and contacts
- App Version: 0.18.0
- App Website: https://github.com/pimutils/vdirsyncer

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install vdirsyncer with the following command
   ```
   brew install vdirsyncer
   ```
4. vdirsyncer is ready to use now!