---
title: "Install OpenConnect-GUI on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GitLab mirror - Graphical OpenConnect client (beta phase)"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install OpenConnect-GUI on MacOS using homebrew

- App Name: OpenConnect-GUI
- App description: GitLab mirror - Graphical OpenConnect client (beta phase)
- App Version: 1.5.3
- App Website: https://openconnect.github.io/openconnect-gui/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install OpenConnect-GUI with the following command
   ```
   brew install --cask openconnect-gui
   ```
4. OpenConnect-GUI is ready to use now!