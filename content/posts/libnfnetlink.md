---
title: "Install libnfnetlink on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Low-level library for netfilter related communication"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libnfnetlink on MacOS using homebrew

- App Name: libnfnetlink
- App description: Low-level library for netfilter related communication
- App Version: 1.0.1
- App Website: https://www.netfilter.org/projects/libnfnetlink

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libnfnetlink with the following command
   ```
   brew install libnfnetlink
   ```
4. libnfnetlink is ready to use now!