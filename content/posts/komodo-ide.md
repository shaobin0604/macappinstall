---
title: "Install Komodo IDE on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "One IDE for all your languages"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Komodo IDE on MacOS using homebrew

- App Name: Komodo IDE
- App description: One IDE for all your languages
- App Version: 12.0.1,91869
- App Website: https://www.activestate.com/komodo-ide/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Komodo IDE with the following command
   ```
   brew install --cask komodo-ide
   ```
4. Komodo IDE is ready to use now!