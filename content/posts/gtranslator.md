---
title: "Install gtranslator on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GNOME gettext PO file editor"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gtranslator on MacOS using homebrew

- App Name: gtranslator
- App description: GNOME gettext PO file editor
- App Version: 40.0
- App Website: https://wiki.gnome.org/Design/Apps/Translator

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gtranslator with the following command
   ```
   brew install gtranslator
   ```
4. gtranslator is ready to use now!