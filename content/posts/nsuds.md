---
title: "Install nsuds on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Ncurses Sudoku system"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install nsuds on MacOS using homebrew

- App Name: nsuds
- App description: Ncurses Sudoku system
- App Version: 0.7B
- App Website: https://nsuds.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install nsuds with the following command
   ```
   brew install nsuds
   ```
4. nsuds is ready to use now!