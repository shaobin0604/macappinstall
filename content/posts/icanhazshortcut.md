---
title: "Install iCanHazShortcut on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Shortcut manager"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install iCanHazShortcut on MacOS using homebrew

- App Name: iCanHazShortcut
- App description: Shortcut manager
- App Version: 1.3.0
- App Website: https://github.com/deseven/icanhazshortcut/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install iCanHazShortcut with the following command
   ```
   brew install --cask icanhazshortcut
   ```
4. iCanHazShortcut is ready to use now!