---
title: "Install Pd on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Visual programming language for multimedia"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Pd on MacOS using homebrew

- App Name: Pd
- App description: Visual programming language for multimedia
- App Version: 0.52-1
- App Website: http://msp.ucsd.edu/software.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Pd with the following command
   ```
   brew install --cask pd
   ```
4. Pd is ready to use now!