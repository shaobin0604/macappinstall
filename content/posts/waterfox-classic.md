---
title: "Install Waterfox Classic on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Web browser"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Waterfox Classic on MacOS using homebrew

- App Name: Waterfox Classic
- App description: Web browser
- App Version: 2021.10
- App Website: https://www.waterfox.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Waterfox Classic with the following command
   ```
   brew install --cask waterfox-classic
   ```
4. Waterfox Classic is ready to use now!