---
title: "Install simh on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Portable, multi-system simulator"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install simh on MacOS using homebrew

- App Name: simh
- App description: Portable, multi-system simulator
- App Version: 3.11.1
- App Website: http://simh.trailing-edge.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install simh with the following command
   ```
   brew install simh
   ```
4. simh is ready to use now!