---
title: "Install autopano-sift-c on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Find control points in overlapping image pairs"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install autopano-sift-c on MacOS using homebrew

- App Name: autopano-sift-c
- App description: Find control points in overlapping image pairs
- App Version: 2.5.1
- App Website: https://wiki.panotools.org/Autopano-sift-C

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install autopano-sift-c with the following command
   ```
   brew install autopano-sift-c
   ```
4. autopano-sift-c is ready to use now!