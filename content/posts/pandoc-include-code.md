---
title: "Install pandoc-include-code on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Pandoc filter for including code from source files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pandoc-include-code on MacOS using homebrew

- App Name: pandoc-include-code
- App description: Pandoc filter for including code from source files
- App Version: 1.5.0.0
- App Website: https://github.com/owickstrom/pandoc-include-code

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pandoc-include-code with the following command
   ```
   brew install pandoc-include-code
   ```
4. pandoc-include-code is ready to use now!