---
title: "Install jc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Serializes the output of command-line tools to structured JSON output"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install jc on MacOS using homebrew

- App Name: jc
- App description: Serializes the output of command-line tools to structured JSON output
- App Version: 1.18.3
- App Website: https://github.com/kellyjonbrazil/jc

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install jc with the following command
   ```
   brew install jc
   ```
4. jc is ready to use now!