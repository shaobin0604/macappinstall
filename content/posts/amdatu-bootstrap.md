---
title: "Install amdatu-bootstrap on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Bootstrapping OSGi development"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install amdatu-bootstrap on MacOS using homebrew

- App Name: amdatu-bootstrap
- App description: Bootstrapping OSGi development
- App Version: 9
- App Website: https://bitbucket.org/amdatuadm/amdatu-bootstrap/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install amdatu-bootstrap with the following command
   ```
   brew install amdatu-bootstrap
   ```
4. amdatu-bootstrap is ready to use now!