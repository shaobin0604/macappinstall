---
title: "Install Session Manager Plugin for the AWS CLI on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Plugin for AWS CLI to start and end sessions that connect to managed instances"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Session Manager Plugin for the AWS CLI on MacOS using homebrew

- App Name: Session Manager Plugin for the AWS CLI
- App description: Plugin for AWS CLI to start and end sessions that connect to managed instances
- App Version: 1.2.295.0
- App Website: https://docs.aws.amazon.com/systems-manager/latest/userguide/session-manager-working-with-install-plugin.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Session Manager Plugin for the AWS CLI with the following command
   ```
   brew install --cask session-manager-plugin
   ```
4. Session Manager Plugin for the AWS CLI is ready to use now!