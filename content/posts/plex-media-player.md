---
title: "Install Plex Media Player on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Home media player"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Plex Media Player on MacOS using homebrew

- App Name: Plex Media Player
- App description: Home media player
- App Version: 2.58.0.1076-38e019da
- App Website: https://www.plex.tv/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Plex Media Player with the following command
   ```
   brew install --cask plex-media-player
   ```
4. Plex Media Player is ready to use now!