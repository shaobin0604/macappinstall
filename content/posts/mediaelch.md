---
title: "Install MediaElch on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Media Manager for Kodi"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install MediaElch on MacOS using homebrew

- App Name: MediaElch
- App description: Media Manager for Kodi
- App Version: 2.8.14,2022-02-06,84e18bb2
- App Website: https://www.kvibes.de/en/mediaelch/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install MediaElch with the following command
   ```
   brew install --cask mediaelch
   ```
4. MediaElch is ready to use now!