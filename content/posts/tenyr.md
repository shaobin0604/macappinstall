---
title: "Install tenyr on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "32-bit computing environment (including simulated CPU)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tenyr on MacOS using homebrew

- App Name: tenyr
- App description: 32-bit computing environment (including simulated CPU)
- App Version: 0.9.9
- App Website: https://tenyr.info/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tenyr with the following command
   ```
   brew install tenyr
   ```
4. tenyr is ready to use now!