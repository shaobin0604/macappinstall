---
title: "Install iLok License Manager on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Software for iLok devices"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install iLok License Manager on MacOS using homebrew

- App Name: iLok License Manager
- App description: Software for iLok devices
- App Version: 5.4.1,3455
- App Website: https://ilok.com/#!license-manager

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install iLok License Manager with the following command
   ```
   brew install --cask ilok-license-manager
   ```
4. iLok License Manager is ready to use now!