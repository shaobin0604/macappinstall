---
title: "Install zellij on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Pluggable terminal workspace, with terminal multiplexer as the base feature"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install zellij on MacOS using homebrew

- App Name: zellij
- App description: Pluggable terminal workspace, with terminal multiplexer as the base feature
- App Version: 0.24.0
- App Website: https://zellij.dev

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install zellij with the following command
   ```
   brew install zellij
   ```
4. zellij is ready to use now!