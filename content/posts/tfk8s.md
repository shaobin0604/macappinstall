---
title: "Install tfk8s on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Kubernetes YAML manifests to Terraform HCL converter"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tfk8s on MacOS using homebrew

- App Name: tfk8s
- App description: Kubernetes YAML manifests to Terraform HCL converter
- App Version: 0.1.7
- App Website: https://github.com/jrhouston/tfk8s

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tfk8s with the following command
   ```
   brew install tfk8s
   ```
4. tfk8s is ready to use now!