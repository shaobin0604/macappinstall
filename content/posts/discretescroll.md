---
title: "Install DiscreteScroll on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utility to fix a common scroll wheel problem"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install DiscreteScroll on MacOS using homebrew

- App Name: DiscreteScroll
- App description: Utility to fix a common scroll wheel problem
- App Version: 0.1.1
- App Website: https://github.com/emreyolcu/discrete-scroll

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install DiscreteScroll with the following command
   ```
   brew install --cask discretescroll
   ```
4. DiscreteScroll is ready to use now!