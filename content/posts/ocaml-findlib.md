---
title: "Install ocaml-findlib on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "OCaml library manager"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ocaml-findlib on MacOS using homebrew

- App Name: ocaml-findlib
- App description: OCaml library manager
- App Version: 1.9.1
- App Website: http://projects.camlcity.org/projects/findlib.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ocaml-findlib with the following command
   ```
   brew install ocaml-findlib
   ```
4. ocaml-findlib is ready to use now!