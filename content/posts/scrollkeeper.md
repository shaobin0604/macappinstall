---
title: "Install scrollkeeper on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Transitional package for scrollkeeper"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install scrollkeeper on MacOS using homebrew

- App Name: scrollkeeper
- App description: Transitional package for scrollkeeper
- App Version: 0.3.14
- App Website: https://scrollkeeper.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install scrollkeeper with the following command
   ```
   brew install scrollkeeper
   ```
4. scrollkeeper is ready to use now!