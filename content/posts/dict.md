---
title: "Install dict on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Dictionary Server Protocol (RFC2229) client"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dict on MacOS using homebrew

- App Name: dict
- App description: Dictionary Server Protocol (RFC2229) client
- App Version: 1.13.1
- App Website: http://www.dict.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dict with the following command
   ```
   brew install dict
   ```
4. dict is ready to use now!