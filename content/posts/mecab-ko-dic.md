---
title: "Install mecab-ko-dic on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "See mecab"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mecab-ko-dic on MacOS using homebrew

- App Name: mecab-ko-dic
- App description: See mecab
- App Version: 2.1.1-20180720
- App Website: https://bitbucket.org/eunjeon/mecab-ko-dic

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mecab-ko-dic with the following command
   ```
   brew install mecab-ko-dic
   ```
4. mecab-ko-dic is ready to use now!