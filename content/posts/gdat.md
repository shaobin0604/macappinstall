---
title: "Install Genealogical DNA Analysis Tool on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "App that utilizes autosomal DNA to aid in the research of family trees"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Genealogical DNA Analysis Tool on MacOS using homebrew

- App Name: Genealogical DNA Analysis Tool
- App description: App that utilizes autosomal DNA to aid in the research of family trees
- App Version: 2022r01
- App Website: https://sites.google.com/view/genealogical-dna-analysis-tool

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Genealogical DNA Analysis Tool with the following command
   ```
   brew install --cask gdat
   ```
4. Genealogical DNA Analysis Tool is ready to use now!