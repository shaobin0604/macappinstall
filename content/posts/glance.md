---
title: "Install Glance on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utility to provide quick look previews for files that aren't natively supported"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Glance on MacOS using homebrew

- App Name: Glance
- App description: Utility to provide quick look previews for files that aren't natively supported
- App Version: 1.2.0
- App Website: https://github.com/samuelmeuli/glance

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Glance with the following command
   ```
   brew install --cask glance
   ```
4. Glance is ready to use now!