---
title: "Install dynet on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Dynamic Neural Network Toolkit"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dynet on MacOS using homebrew

- App Name: dynet
- App description: Dynamic Neural Network Toolkit
- App Version: 2.1.2
- App Website: https://github.com/clab/dynet

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dynet with the following command
   ```
   brew install dynet
   ```
4. dynet is ready to use now!