---
title: "Install Sonarr Menu on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utility that adds a menu to the Status Bar for managing Sonarr"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Sonarr Menu on MacOS using homebrew

- App Name: Sonarr Menu
- App description: Utility that adds a menu to the Status Bar for managing Sonarr
- App Version: 3.3
- App Website: https://github.com/jefbarn/Sonarr-Menu/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Sonarr Menu with the following command
   ```
   brew install --cask sonarr-menu
   ```
4. Sonarr Menu is ready to use now!