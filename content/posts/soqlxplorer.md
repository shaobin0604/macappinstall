---
title: "Install SoqlXplorer on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Desktop client for Salesforce.com platform"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install SoqlXplorer on MacOS using homebrew

- App Name: SoqlXplorer
- App description: Desktop client for Salesforce.com platform
- App Version: 4.3
- App Website: https://www.pocketsoap.com/osx/soqlx/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install SoqlXplorer with the following command
   ```
   brew install --cask soqlxplorer
   ```
4. SoqlXplorer is ready to use now!