---
title: "Install Cantata on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Qt5 Graphical MPD Client"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Cantata on MacOS using homebrew

- App Name: Cantata
- App description: Qt5 Graphical MPD Client
- App Version: 2.3.2
- App Website: https://github.com/cdrummond/cantata

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Cantata with the following command
   ```
   brew install --cask cantata
   ```
4. Cantata is ready to use now!