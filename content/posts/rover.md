---
title: "Install rover on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CLI for managing and maintaining data graphs with Apollo Studio"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install rover on MacOS using homebrew

- App Name: rover
- App description: CLI for managing and maintaining data graphs with Apollo Studio
- App Version: 0.4.3
- App Website: https://www.apollographql.com/docs/rover/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install rover with the following command
   ```
   brew install rover
   ```
4. rover is ready to use now!