---
title: "Install SnapMotion on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Extract images from videos"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install SnapMotion on MacOS using homebrew

- App Name: SnapMotion
- App description: Extract images from videos
- App Version: 5.0.5,5895ef0e-ace3-4454-b817-dad5caba4e29
- App Website: https://neededapps.com/snapmotion/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install SnapMotion with the following command
   ```
   brew install --cask snapmotion
   ```
4. SnapMotion is ready to use now!