---
title: "Install pngcheck on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Print info and check PNG, JNG, and MNG files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pngcheck on MacOS using homebrew

- App Name: pngcheck
- App description: Print info and check PNG, JNG, and MNG files
- App Version: 3.0.3
- App Website: http://www.libpng.org/pub/png/apps/pngcheck.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pngcheck with the following command
   ```
   brew install pngcheck
   ```
4. pngcheck is ready to use now!