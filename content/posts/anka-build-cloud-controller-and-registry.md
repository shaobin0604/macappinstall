---
title: "Install Anka Build Cloud Controller & Registry on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Virtual machine management GUI/API and registry"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Anka Build Cloud Controller & Registry on MacOS using homebrew

- App Name: Anka Build Cloud Controller & Registry
- App description: Virtual machine management GUI/API and registry
- App Version: 1.22.0-5dc750f1
- App Website: https://veertu.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Anka Build Cloud Controller & Registry with the following command
   ```
   brew install --cask anka-build-cloud-controller-and-registry
   ```
4. Anka Build Cloud Controller & Registry is ready to use now!