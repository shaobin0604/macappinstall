---
title: "Install Oryoki on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Experimental web browser with a thin interface"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Oryoki on MacOS using homebrew

- App Name: Oryoki
- App description: Experimental web browser with a thin interface
- App Version: 0.2.2
- App Website: http://oryoki.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Oryoki with the following command
   ```
   brew install --cask oryoki
   ```
4. Oryoki is ready to use now!