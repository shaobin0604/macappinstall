---
title: "Install OpenDNS Updater on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Dynamic IP updater client"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install OpenDNS Updater on MacOS using homebrew

- App Name: OpenDNS Updater
- App description: Dynamic IP updater client
- App Version: 3.1
- App Website: https://support.opendns.com/hc/en-us/articles/227987867

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install OpenDNS Updater with the following command
   ```
   brew install --cask opendnsupdater
   ```
4. OpenDNS Updater is ready to use now!