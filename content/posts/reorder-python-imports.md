---
title: "Install reorder-python-imports on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Rewrites source to reorder python imports"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install reorder-python-imports on MacOS using homebrew

- App Name: reorder-python-imports
- App description: Rewrites source to reorder python imports
- App Version: 2.7.1
- App Website: https://github.com/asottile/reorder_python_imports

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install reorder-python-imports with the following command
   ```
   brew install reorder-python-imports
   ```
4. reorder-python-imports is ready to use now!