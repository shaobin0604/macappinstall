---
title: "Install nikto on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Web server scanner"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install nikto on MacOS using homebrew

- App Name: nikto
- App description: Web server scanner
- App Version: 2.1.6
- App Website: https://cirt.net/nikto2

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install nikto with the following command
   ```
   brew install nikto
   ```
4. nikto is ready to use now!