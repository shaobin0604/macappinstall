---
title: "Install Runway on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "UML (Unified Modeling Language) design app"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Runway on MacOS using homebrew

- App Name: Runway
- App description: UML (Unified Modeling Language) design app
- App Version: 2.0,2002
- App Website: https://celestialteapot.com/runway/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Runway with the following command
   ```
   brew install --cask celestialteapot-runway
   ```
4. Runway is ready to use now!