---
title: "Install pylint on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "It's not just a linter that annoys you!"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pylint on MacOS using homebrew

- App Name: pylint
- App description: It's not just a linter that annoys you!
- App Version: 2.12.2
- App Website: https://github.com/PyCQA/pylint

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pylint with the following command
   ```
   brew install pylint
   ```
4. pylint is ready to use now!