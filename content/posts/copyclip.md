---
title: "Install CopyClip on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Clipboard manager"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install CopyClip on MacOS using homebrew

- App Name: CopyClip
- App description: Clipboard manager
- App Version: 2.9.98.9
- App Website: https://fiplab.com/apps/copyclip-for-mac

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install CopyClip with the following command
   ```
   brew install --cask copyclip
   ```
4. CopyClip is ready to use now!