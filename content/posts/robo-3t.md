---
title: "Install Robo 3T (formerly Robomongo) on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "MongoDB management tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Robo 3T (formerly Robomongo) on MacOS using homebrew

- App Name: Robo 3T (formerly Robomongo)
- App description: MongoDB management tool
- App Version: 1.4.4,e6ac9ec
- App Website: https://robomongo.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Robo 3T (formerly Robomongo) with the following command
   ```
   brew install --cask robo-3t
   ```
4. Robo 3T (formerly Robomongo) is ready to use now!