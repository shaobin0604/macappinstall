---
title: "Install mmhmm on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Virtual video presentation software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mmhmm on MacOS using homebrew

- App Name: mmhmm
- App description: Virtual video presentation software
- App Version: 2.2.5,1644628000
- App Website: https://www.mmhmm.app/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mmhmm with the following command
   ```
   brew install --cask mmhmm
   ```
4. mmhmm is ready to use now!