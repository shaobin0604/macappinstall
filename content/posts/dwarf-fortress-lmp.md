---
title: "Install Dwarf Fortress LMP (Lazy Mac Pack) on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Use and switch graphics packs with Dwarf Fortress without corrupting your game"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Dwarf Fortress LMP (Lazy Mac Pack) on MacOS using homebrew

- App Name: Dwarf Fortress LMP (Lazy Mac Pack)
- App description: Use and switch graphics packs with Dwarf Fortress without corrupting your game
- App Version: 0.47.05+dfhack-r1
- App Website: https://dffd.bay12games.com/file.php?id=12202

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Dwarf Fortress LMP (Lazy Mac Pack) with the following command
   ```
   brew install --cask dwarf-fortress-lmp
   ```
4. Dwarf Fortress LMP (Lazy Mac Pack) is ready to use now!