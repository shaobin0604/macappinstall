---
title: "Install hy on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Dialect of Lisp that's embedded in Python"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install hy on MacOS using homebrew

- App Name: hy
- App description: Dialect of Lisp that's embedded in Python
- App Version: 0.20.0
- App Website: https://github.com/hylang/hy

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install hy with the following command
   ```
   brew install hy
   ```
4. hy is ready to use now!