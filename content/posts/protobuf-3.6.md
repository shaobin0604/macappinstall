---
title: "Install protobuf@3.6 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Protocol buffers (Google's data interchange format)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install protobuf@3.6 on MacOS using homebrew

- App Name: protobuf@3.6
- App description: Protocol buffers (Google's data interchange format)
- App Version: 3.6.1.3
- App Website: https://github.com/protocolbuffers/protobuf/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install protobuf@3.6 with the following command
   ```
   brew install protobuf@3.6
   ```
4. protobuf@3.6 is ready to use now!