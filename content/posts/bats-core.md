---
title: "Install bats-core on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Bash Automated Testing System"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bats-core on MacOS using homebrew

- App Name: bats-core
- App description: Bash Automated Testing System
- App Version: 1.5.0
- App Website: https://github.com/bats-core/bats-core

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bats-core with the following command
   ```
   brew install bats-core
   ```
4. bats-core is ready to use now!