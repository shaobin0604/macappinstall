---
title: "Install chapel on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Programming language for productive parallel computing at scale"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install chapel on MacOS using homebrew

- App Name: chapel
- App description: Programming language for productive parallel computing at scale
- App Version: 1.25.1
- App Website: https://chapel-lang.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install chapel with the following command
   ```
   brew install chapel
   ```
4. chapel is ready to use now!