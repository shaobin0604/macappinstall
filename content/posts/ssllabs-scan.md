---
title: "Install ssllabs-scan on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "This tool is a command-line client for the SSL Labs APIs"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ssllabs-scan on MacOS using homebrew

- App Name: ssllabs-scan
- App description: This tool is a command-line client for the SSL Labs APIs
- App Version: 1.5.0
- App Website: https://github.com/ssllabs/ssllabs-scan/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ssllabs-scan with the following command
   ```
   brew install ssllabs-scan
   ```
4. ssllabs-scan is ready to use now!