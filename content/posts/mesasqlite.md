---
title: "Install MesaSQLite on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Create, design and alter an SQLite3 database"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install MesaSQLite on MacOS using homebrew

- App Name: MesaSQLite
- App description: Create, design and alter an SQLite3 database
- App Version: 4.3.5
- App Website: http://www.desertsandsoftware.com/wordpress/?page_id=17

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install MesaSQLite with the following command
   ```
   brew install --cask mesasqlite
   ```
4. MesaSQLite is ready to use now!