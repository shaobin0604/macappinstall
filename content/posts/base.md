---
title: "Install Menial Base on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "App to create, design, edit and browse SQLite 3 database files"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Menial Base on MacOS using homebrew

- App Name: Menial Base
- App description: App to create, design, edit and browse SQLite 3 database files
- App Version: 2.5.2,20502
- App Website: https://menial.co.uk/base/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Menial Base with the following command
   ```
   brew install --cask base
   ```
4. Menial Base is ready to use now!