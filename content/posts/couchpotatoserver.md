---
title: "Install couchpotatoserver on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Download movies automatically"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install couchpotatoserver on MacOS using homebrew

- App Name: couchpotatoserver
- App description: Download movies automatically
- App Version: 3.0.1
- App Website: https://couchpota.to

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install couchpotatoserver with the following command
   ```
   brew install couchpotatoserver
   ```
4. couchpotatoserver is ready to use now!