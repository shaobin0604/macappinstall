---
title: "Install Trilium Notes on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Personal knowledge base"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Trilium Notes on MacOS using homebrew

- App Name: Trilium Notes
- App description: Personal knowledge base
- App Version: 0.50.2
- App Website: https://github.com/zadam/trilium

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Trilium Notes with the following command
   ```
   brew install --cask trilium-notes
   ```
4. Trilium Notes is ready to use now!