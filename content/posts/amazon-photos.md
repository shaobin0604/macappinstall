---
title: "Install Amazon Drive on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Photo storage and sharing service"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Amazon Drive on MacOS using homebrew

- App Name: Amazon Drive
- App description: Photo storage and sharing service
- App Version: latest
- App Website: https://www.amazon.com/Amazon-Photos/b?node=13234696011

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Amazon Drive with the following command
   ```
   brew install --cask amazon-photos
   ```
4. Amazon Drive is ready to use now!