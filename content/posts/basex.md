---
title: "Install basex on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Light-weight XML database and XPath/XQuery processor"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install basex on MacOS using homebrew

- App Name: basex
- App description: Light-weight XML database and XPath/XQuery processor
- App Version: 9.6.4
- App Website: https://basex.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install basex with the following command
   ```
   brew install basex
   ```
4. basex is ready to use now!