---
title: "Install Sonic Lineup on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Rapid visualisation of multiple audio files for comparison"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Sonic Lineup on MacOS using homebrew

- App Name: Sonic Lineup
- App description: Rapid visualisation of multiple audio files for comparison
- App Version: 1.1,2768
- App Website: https://sonicvisualiser.org/sonic-lineup/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Sonic Lineup with the following command
   ```
   brew install --cask sonic-lineup
   ```
4. Sonic Lineup is ready to use now!