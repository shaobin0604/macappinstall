---
title: "Install SelfControl on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Block your own access to distracting websites"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install SelfControl on MacOS using homebrew

- App Name: SelfControl
- App description: Block your own access to distracting websites
- App Version: 4.0.2
- App Website: https://selfcontrolapp.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install SelfControl with the following command
   ```
   brew install --cask selfcontrol
   ```
4. SelfControl is ready to use now!