---
title: "Install SwiftDefaultApps on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Replacement for RCDefaultApps, written in Swift"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install SwiftDefaultApps on MacOS using homebrew

- App Name: SwiftDefaultApps
- App description: Replacement for RCDefaultApps, written in Swift
- App Version: 2.0.1
- App Website: https://github.com/Lord-Kamina/SwiftDefaultApps

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install SwiftDefaultApps with the following command
   ```
   brew install --cask swiftdefaultappsprefpane
   ```
4. SwiftDefaultApps is ready to use now!