---
title: "Install mpfi on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Multiple precision interval arithmetic library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mpfi on MacOS using homebrew

- App Name: mpfi
- App description: Multiple precision interval arithmetic library
- App Version: 1.5.3
- App Website: https://perso.ens-lyon.fr/nathalie.revol/software.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mpfi with the following command
   ```
   brew install mpfi
   ```
4. mpfi is ready to use now!