---
title: "Install pfetch on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Pretty system information tool written in POSIX sh"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pfetch on MacOS using homebrew

- App Name: pfetch
- App description: Pretty system information tool written in POSIX sh
- App Version: 0.6.0
- App Website: https://github.com/dylanaraps/pfetch/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pfetch with the following command
   ```
   brew install pfetch
   ```
4. pfetch is ready to use now!