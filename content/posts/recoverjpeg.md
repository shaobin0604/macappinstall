---
title: "Install recoverjpeg on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to recover JPEG images from a file system image"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install recoverjpeg on MacOS using homebrew

- App Name: recoverjpeg
- App description: Tool to recover JPEG images from a file system image
- App Version: 2.6.3
- App Website: https://rfc1149.net/devel/recoverjpeg.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install recoverjpeg with the following command
   ```
   brew install recoverjpeg
   ```
4. recoverjpeg is ready to use now!