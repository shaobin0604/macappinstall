---
title: "Install osslsigncode on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "OpenSSL based Authenticode signing for PE/MSI/Java CAB files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install osslsigncode on MacOS using homebrew

- App Name: osslsigncode
- App description: OpenSSL based Authenticode signing for PE/MSI/Java CAB files
- App Version: 2.2
- App Website: https://github.com/mtrojnar/osslsigncode

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install osslsigncode with the following command
   ```
   brew install osslsigncode
   ```
4. osslsigncode is ready to use now!