---
title: "Install naturaldocs on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Extensible, multi-language documentation generator"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install naturaldocs on MacOS using homebrew

- App Name: naturaldocs
- App description: Extensible, multi-language documentation generator
- App Version: 2.1.1
- App Website: https://www.naturaldocs.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install naturaldocs with the following command
   ```
   brew install naturaldocs
   ```
4. naturaldocs is ready to use now!