---
title: "Install zita-convolver on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Fast, partitioned convolution engine library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install zita-convolver on MacOS using homebrew

- App Name: zita-convolver
- App description: Fast, partitioned convolution engine library
- App Version: 4.0.3
- App Website: https://kokkinizita.linuxaudio.org/linuxaudio/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install zita-convolver with the following command
   ```
   brew install zita-convolver
   ```
4. zita-convolver is ready to use now!