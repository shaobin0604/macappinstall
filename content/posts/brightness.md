---
title: "Install brightness on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Change macOS display brightness from the command-line"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install brightness on MacOS using homebrew

- App Name: brightness
- App description: Change macOS display brightness from the command-line
- App Version: 1.2
- App Website: https://github.com/nriley/brightness

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install brightness with the following command
   ```
   brew install brightness
   ```
4. brightness is ready to use now!