---
title: "Install JetBrains PhpStorm on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "PHP IDE by JetBrains"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install JetBrains PhpStorm on MacOS using homebrew

- App Name: JetBrains PhpStorm
- App description: PHP IDE by JetBrains
- App Version: 2021.3.2,213.6777.58
- App Website: https://www.jetbrains.com/phpstorm/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install JetBrains PhpStorm with the following command
   ```
   brew install --cask phpstorm
   ```
4. JetBrains PhpStorm is ready to use now!