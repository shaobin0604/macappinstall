---
title: "Install MP4tools on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Create and edit MP4 videos"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install MP4tools on MacOS using homebrew

- App Name: MP4tools
- App description: Create and edit MP4 videos
- App Version: 3.7.2
- App Website: https://www.emmgunn.com/mp4tools-home/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install MP4tools with the following command
   ```
   brew install --cask mp4tools
   ```
4. MP4tools is ready to use now!