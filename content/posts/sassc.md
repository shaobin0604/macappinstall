---
title: "Install sassc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Wrapper around libsass that helps to create command-line apps"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sassc on MacOS using homebrew

- App Name: sassc
- App description: Wrapper around libsass that helps to create command-line apps
- App Version: 3.6.2
- App Website: https://github.com/sass/sassc

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sassc with the following command
   ```
   brew install sassc
   ```
4. sassc is ready to use now!