---
title: "Install ndiff on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Virtual package provided by nmap"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ndiff on MacOS using homebrew

- App Name: ndiff
- App description: Virtual package provided by nmap
- App Version: 2.00
- App Website: https://www.math.utah.edu/~beebe/software/ndiff/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ndiff with the following command
   ```
   brew install ndiff
   ```
4. ndiff is ready to use now!