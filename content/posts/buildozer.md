---
title: "Install buildozer on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Rewrite bazel BUILD files using standard commands"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install buildozer on MacOS using homebrew

- App Name: buildozer
- App description: Rewrite bazel BUILD files using standard commands
- App Version: 5.0.1
- App Website: https://github.com/bazelbuild/buildtools

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install buildozer with the following command
   ```
   brew install buildozer
   ```
4. buildozer is ready to use now!