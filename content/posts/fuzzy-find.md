---
title: "Install fuzzy-find on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Fuzzy filename finder matching across directories as well as files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fuzzy-find on MacOS using homebrew

- App Name: fuzzy-find
- App description: Fuzzy filename finder matching across directories as well as files
- App Version: 0.6.0
- App Website: https://github.com/silentbicycle/ff

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fuzzy-find with the following command
   ```
   brew install fuzzy-find
   ```
4. fuzzy-find is ready to use now!