---
title: "Install goofys on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Filey-System interface to Amazon S3"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install goofys on MacOS using homebrew

- App Name: goofys
- App description: Filey-System interface to Amazon S3
- App Version: 0.24.0
- App Website: https://github.com/kahing/goofys

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install goofys with the following command
   ```
   brew install goofys
   ```
4. goofys is ready to use now!