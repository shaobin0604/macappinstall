---
title: "Install Etcher on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to flash OS images to SD cards & USB drives"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Etcher on MacOS using homebrew

- App Name: Etcher
- App description: Tool to flash OS images to SD cards & USB drives
- App Version: 1.7.3
- App Website: https://balena.io/etcher

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Etcher with the following command
   ```
   brew install --cask balenaetcher
   ```
4. Etcher is ready to use now!