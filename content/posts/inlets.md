---
title: "Install inlets on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Expose your local endpoints to the Internet"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install inlets on MacOS using homebrew

- App Name: inlets
- App description: Expose your local endpoints to the Internet
- App Version: 3.0.2
- App Website: https://github.com/inlets/inlets

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install inlets with the following command
   ```
   brew install inlets
   ```
4. inlets is ready to use now!