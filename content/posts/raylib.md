---
title: "Install raylib on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Simple and easy-to-use library to learn videogames programming"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install raylib on MacOS using homebrew

- App Name: raylib
- App description: Simple and easy-to-use library to learn videogames programming
- App Version: 4.0.0
- App Website: https://www.raylib.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install raylib with the following command
   ```
   brew install raylib
   ```
4. raylib is ready to use now!