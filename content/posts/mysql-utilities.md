---
title: "Install MySQL Utilities on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install MySQL Utilities on MacOS using homebrew

- App Name: MySQL Utilities
- App description: null
- App Version: 1.6.5
- App Website: https://dev.mysql.com/downloads/utilities/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install MySQL Utilities with the following command
   ```
   brew install --cask mysql-utilities
   ```
4. MySQL Utilities is ready to use now!