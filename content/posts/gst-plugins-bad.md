---
title: "Install gst-plugins-bad on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GStreamer plugins less supported, not fully tested"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gst-plugins-bad on MacOS using homebrew

- App Name: gst-plugins-bad
- App description: GStreamer plugins less supported, not fully tested
- App Version: 1.18.5
- App Website: https://gstreamer.freedesktop.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gst-plugins-bad with the following command
   ```
   brew install gst-plugins-bad
   ```
4. gst-plugins-bad is ready to use now!