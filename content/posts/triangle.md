---
title: "Install triangle on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Convert images to computer generated art using Delaunay triangulation"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install triangle on MacOS using homebrew

- App Name: triangle
- App description: Convert images to computer generated art using Delaunay triangulation
- App Version: 1.2.3
- App Website: https://github.com/esimov/triangle

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install triangle with the following command
   ```
   brew install triangle
   ```
4. triangle is ready to use now!