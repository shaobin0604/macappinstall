---
title: "Install libosip on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Implementation of the eXosip2 stack"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libosip on MacOS using homebrew

- App Name: libosip
- App description: Implementation of the eXosip2 stack
- App Version: 5.3.0
- App Website: https://www.gnu.org/software/osip/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libosip with the following command
   ```
   brew install libosip
   ```
4. libosip is ready to use now!