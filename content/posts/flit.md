---
title: "Install flit on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Simplified packaging of Python modules"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install flit on MacOS using homebrew

- App Name: flit
- App description: Simplified packaging of Python modules
- App Version: 3.6.0
- App Website: https://github.com/takluyver/flit

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install flit with the following command
   ```
   brew install flit
   ```
4. flit is ready to use now!