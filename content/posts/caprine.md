---
title: "Install Caprine on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Elegant Facebook Messenger desktop app"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Caprine on MacOS using homebrew

- App Name: Caprine
- App description: Elegant Facebook Messenger desktop app
- App Version: 2.55.2
- App Website: https://github.com/sindresorhus/caprine

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Caprine with the following command
   ```
   brew install --cask caprine
   ```
4. Caprine is ready to use now!