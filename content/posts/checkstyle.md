---
title: "Install checkstyle on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Check Java source against a coding standard"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install checkstyle on MacOS using homebrew

- App Name: checkstyle
- App description: Check Java source against a coding standard
- App Version: 9.3
- App Website: https://checkstyle.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install checkstyle with the following command
   ```
   brew install checkstyle
   ```
4. checkstyle is ready to use now!