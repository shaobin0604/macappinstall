---
title: "Install Postgres on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "App wrapper for Postgres"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Postgres on MacOS using homebrew

- App Name: Postgres
- App description: App wrapper for Postgres
- App Version: 2.5.6
- App Website: https://postgresapp.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Postgres with the following command
   ```
   brew install --cask postgres-unofficial
   ```
4. Postgres is ready to use now!