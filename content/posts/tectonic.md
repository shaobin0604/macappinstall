---
title: "Install tectonic on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Modernized, complete, self-contained TeX/LaTeX engine"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tectonic on MacOS using homebrew

- App Name: tectonic
- App description: Modernized, complete, self-contained TeX/LaTeX engine
- App Version: 0.8.0
- App Website: https://tectonic-typesetting.github.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tectonic with the following command
   ```
   brew install tectonic
   ```
4. tectonic is ready to use now!