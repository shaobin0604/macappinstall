---
title: "Install OpenToonz on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Open-source full-featured 2D animation creation software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install OpenToonz on MacOS using homebrew

- App Name: OpenToonz
- App description: Open-source full-featured 2D animation creation software
- App Version: 1.5.0
- App Website: https://opentoonz.github.io/e/index.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install OpenToonz with the following command
   ```
   brew install --cask opentoonz
   ```
4. OpenToonz is ready to use now!