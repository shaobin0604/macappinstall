---
title: "Install polkadot{.js} on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Portal into the Polkadot and Substrate networks"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install polkadot{.js} on MacOS using homebrew

- App Name: polkadot{.js}
- App description: Portal into the Polkadot and Substrate networks
- App Version: 0.100.1
- App Website: https://polkadot.js.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install polkadot{.js} with the following command
   ```
   brew install --cask polkadot-js
   ```
4. polkadot{.js} is ready to use now!