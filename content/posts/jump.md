---
title: "Install jump on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Helps you navigate your file system faster by learning your habits"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install jump on MacOS using homebrew

- App Name: jump
- App description: Helps you navigate your file system faster by learning your habits
- App Version: 0.40.0
- App Website: https://github.com/gsamokovarov/jump

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install jump with the following command
   ```
   brew install jump
   ```
4. jump is ready to use now!