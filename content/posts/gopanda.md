---
title: "Install GoPanda on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Pandanet client"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install GoPanda on MacOS using homebrew

- App Name: GoPanda
- App description: Pandanet client
- App Version: 2.7.8
- App Website: https://pandanet-igs.com/communities/gopanda2

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install GoPanda with the following command
   ```
   brew install --cask gopanda
   ```
4. GoPanda is ready to use now!