---
title: "Install anjuta on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GNOME Integrated Development Environment"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install anjuta on MacOS using homebrew

- App Name: anjuta
- App description: GNOME Integrated Development Environment
- App Version: 3.34.0
- App Website: http://anjuta.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install anjuta with the following command
   ```
   brew install anjuta
   ```
4. anjuta is ready to use now!