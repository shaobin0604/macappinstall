---
title: "Install Beeper on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Universal chat app powered by Matrix, unifying 15 different chat networks"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Beeper on MacOS using homebrew

- App Name: Beeper
- App description: Universal chat app powered by Matrix, unifying 15 different chat networks
- App Version: 3.0.1
- App Website: https://www.beeper.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Beeper with the following command
   ```
   brew install --cask beeper
   ```
4. Beeper is ready to use now!