---
title: "Install Alfred on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Application launcher and productivity software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Alfred on MacOS using homebrew

- App Name: Alfred
- App description: Application launcher and productivity software
- App Version: 4.6.3,1285
- App Website: https://www.alfredapp.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Alfred with the following command
   ```
   brew install --cask alfred
   ```
4. Alfred is ready to use now!