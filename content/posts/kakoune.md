---
title: "Install kakoune on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Selection-based modal text editor"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install kakoune on MacOS using homebrew

- App Name: kakoune
- App description: Selection-based modal text editor
- App Version: 2021.11.08
- App Website: https://github.com/mawww/kakoune

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install kakoune with the following command
   ```
   brew install kakoune
   ```
4. kakoune is ready to use now!