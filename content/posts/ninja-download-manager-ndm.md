---
title: "Install Ninja Download Manager on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "File download organizer and accelerator"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Ninja Download Manager on MacOS using homebrew

- App Name: Ninja Download Manager
- App description: File download organizer and accelerator
- App Version: 46
- App Website: https://ninjadownloadmanager.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Ninja Download Manager with the following command
   ```
   brew install --cask ninja-download-manager-ndm
   ```
4. Ninja Download Manager is ready to use now!