---
title: "Install lemon on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "LALR(1) parser generator like yacc or bison"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install lemon on MacOS using homebrew

- App Name: lemon
- App description: LALR(1) parser generator like yacc or bison
- App Version: 3.36.0
- App Website: https://www.hwaci.com/sw/lemon/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install lemon with the following command
   ```
   brew install lemon
   ```
4. lemon is ready to use now!