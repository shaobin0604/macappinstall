---
title: "Install rats on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Rough auditing tool for security"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install rats on MacOS using homebrew

- App Name: rats
- App description: Rough auditing tool for security
- App Version: 2.4
- App Website: https://security.web.cern.ch/security/recommendations/en/codetools/rats.shtml

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install rats with the following command
   ```
   brew install rats
   ```
4. rats is ready to use now!