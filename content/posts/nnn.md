---
title: "Install nnn on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tiny, lightning fast, feature-packed file manager"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install nnn on MacOS using homebrew

- App Name: nnn
- App description: Tiny, lightning fast, feature-packed file manager
- App Version: 4.4
- App Website: https://github.com/jarun/nnn

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install nnn with the following command
   ```
   brew install nnn
   ```
4. nnn is ready to use now!