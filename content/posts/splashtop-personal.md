---
title: "Install Splashtop Personal on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Connect to and control computers from desktop and mobile devices"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Splashtop Personal on MacOS using homebrew

- App Name: Splashtop Personal
- App description: Connect to and control computers from desktop and mobile devices
- App Version: 3.4.6.1
- App Website: https://www.splashtop.com/personal

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Splashtop Personal with the following command
   ```
   brew install --cask splashtop-personal
   ```
4. Splashtop Personal is ready to use now!