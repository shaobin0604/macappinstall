---
title: "Install Blink1Control on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utility to control blink(1) USB RGB LED devices"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Blink1Control on MacOS using homebrew

- App Name: Blink1Control
- App description: Utility to control blink(1) USB RGB LED devices
- App Version: 2.2.5
- App Website: https://blink1.thingm.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Blink1Control with the following command
   ```
   brew install --cask blink1control
   ```
4. Blink1Control is ready to use now!