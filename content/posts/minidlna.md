---
title: "Install minidlna on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Media server software, compliant with DLNA/UPnP-AV clients"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install minidlna on MacOS using homebrew

- App Name: minidlna
- App description: Media server software, compliant with DLNA/UPnP-AV clients
- App Version: 1.3.0
- App Website: https://sourceforge.net/projects/minidlna/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install minidlna with the following command
   ```
   brew install minidlna
   ```
4. minidlna is ready to use now!