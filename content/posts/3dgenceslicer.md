---
title: "Install 3DGence Slicer on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Prepare files for 3D printing based on CAD models for 3DGence printers"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install 3DGence Slicer on MacOS using homebrew

- App Name: 3DGence Slicer
- App description: Prepare files for 3D printing based on CAD models for 3DGence printers
- App Version: 3.1.1,4.0
- App Website: https://3dgence.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install 3DGence Slicer with the following command
   ```
   brew install --cask 3dgenceslicer
   ```
4. 3DGence Slicer is ready to use now!