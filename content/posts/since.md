---
title: "Install since on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Stateful tail: show changes to files since last check"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install since on MacOS using homebrew

- App Name: since
- App description: Stateful tail: show changes to files since last check
- App Version: 1.1
- App Website: http://welz.org.za/projects/since

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install since with the following command
   ```
   brew install since
   ```
4. since is ready to use now!