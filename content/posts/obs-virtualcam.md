---
title: "Install OBS Virtual Camera on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install OBS Virtual Camera on MacOS using homebrew

- App Name: OBS Virtual Camera
- App description: null
- App Version: 1.3.1,5bf3231
- App Website: https://github.com/johnboiles/obs-mac-virtualcam

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install OBS Virtual Camera with the following command
   ```
   brew install --cask obs-virtualcam
   ```
4. OBS Virtual Camera is ready to use now!