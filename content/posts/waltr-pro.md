---
title: "Install WALTR PRO on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Media conversion and direct transfer tool for Apple devices"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install WALTR PRO on MacOS using homebrew

- App Name: WALTR PRO
- App description: Media conversion and direct transfer tool for Apple devices
- App Version: 4.0.114
- App Website: https://softorino.com/waltr/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install WALTR PRO with the following command
   ```
   brew install --cask waltr-pro
   ```
4. WALTR PRO is ready to use now!