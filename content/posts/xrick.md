---
title: "Install xrick on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Clone of Rick Dangerous"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install xrick on MacOS using homebrew

- App Name: xrick
- App description: Clone of Rick Dangerous
- App Version: 021212
- App Website: https://www.bigorno.net/xrick/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install xrick with the following command
   ```
   brew install xrick
   ```
4. xrick is ready to use now!