---
title: "Install grt on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Gesture Recognition Toolkit for real-time machine learning"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install grt on MacOS using homebrew

- App Name: grt
- App description: Gesture Recognition Toolkit for real-time machine learning
- App Version: 0.2.4
- App Website: https://nickgillian.com/grt/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install grt with the following command
   ```
   brew install grt
   ```
4. grt is ready to use now!