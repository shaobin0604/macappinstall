---
title: "Install jetty-runner on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Use Jetty without an installed distribution"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install jetty-runner on MacOS using homebrew

- App Name: jetty-runner
- App description: Use Jetty without an installed distribution
- App Version: 9.4.45.v20220203
- App Website: https://www.eclipse.org/jetty/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install jetty-runner with the following command
   ```
   brew install jetty-runner
   ```
4. jetty-runner is ready to use now!