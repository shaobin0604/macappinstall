---
title: "Install toml11 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "TOML for Modern C++"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install toml11 on MacOS using homebrew

- App Name: toml11
- App description: TOML for Modern C++
- App Version: 3.7.0
- App Website: https://github.com/ToruNiina/toml11

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install toml11 with the following command
   ```
   brew install toml11
   ```
4. toml11 is ready to use now!