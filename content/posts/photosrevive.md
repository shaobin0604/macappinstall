---
title: "Install PhotosRevive on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Colorize old black and white photos automatically"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install PhotosRevive on MacOS using homebrew

- App Name: PhotosRevive
- App description: Colorize old black and white photos automatically
- App Version: 2.0.4,971f3656-21cb-4e67-8b8e-9614e9dfab12
- App Website: https://neededapps.com/photosrevive/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install PhotosRevive with the following command
   ```
   brew install --cask photosrevive
   ```
4. PhotosRevive is ready to use now!