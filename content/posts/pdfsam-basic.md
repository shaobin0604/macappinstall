---
title: "Install PDFsam Basic on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Extracts pages, splits, merges, mixes and rotates PDF files"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install PDFsam Basic on MacOS using homebrew

- App Name: PDFsam Basic
- App description: Extracts pages, splits, merges, mixes and rotates PDF files
- App Version: 4.2.12
- App Website: https://pdfsam.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install PDFsam Basic with the following command
   ```
   brew install --cask pdfsam-basic
   ```
4. PDFsam Basic is ready to use now!