---
title: "Install GitHub Classroom Assistant on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to clone student repositories in bulk"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install GitHub Classroom Assistant on MacOS using homebrew

- App Name: GitHub Classroom Assistant
- App description: Tool to clone student repositories in bulk
- App Version: 2.0.4
- App Website: https://classroom.github.com/assistant

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install GitHub Classroom Assistant with the following command
   ```
   brew install --cask classroom-assistant
   ```
4. GitHub Classroom Assistant is ready to use now!