---
title: "Install blis on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "BLAS-like Library Instantiation Software Framework"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install blis on MacOS using homebrew

- App Name: blis
- App description: BLAS-like Library Instantiation Software Framework
- App Version: 0.8.1
- App Website: https://github.com/flame/blis

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install blis with the following command
   ```
   brew install blis
   ```
4. blis is ready to use now!