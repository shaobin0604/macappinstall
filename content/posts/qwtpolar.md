---
title: "Install qwtpolar on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library for displaying values on a polar coordinate system"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install qwtpolar on MacOS using homebrew

- App Name: qwtpolar
- App description: Library for displaying values on a polar coordinate system
- App Version: 1.1.1
- App Website: https://qwtpolar.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install qwtpolar with the following command
   ```
   brew install qwtpolar
   ```
4. qwtpolar is ready to use now!