---
title: "Install KeyCombiner on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Instant shortcut lookup"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install KeyCombiner on MacOS using homebrew

- App Name: KeyCombiner
- App description: Instant shortcut lookup
- App Version: 0.7.2
- App Website: https://keycombiner.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install KeyCombiner with the following command
   ```
   brew install --cask keycombiner
   ```
4. KeyCombiner is ready to use now!