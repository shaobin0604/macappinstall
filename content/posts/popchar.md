---
title: "Install PopChar X on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utility to display all characters of a font"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install PopChar X on MacOS using homebrew

- App Name: PopChar X
- App description: Utility to display all characters of a font
- App Version: 9.3
- App Website: https://www.ergonis.com/products/popcharx/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install PopChar X with the following command
   ```
   brew install --cask popchar
   ```
4. PopChar X is ready to use now!