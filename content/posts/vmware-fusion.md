---
title: "Install VMware Fusion on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Create, manage, and run virtual machines"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install VMware Fusion on MacOS using homebrew

- App Name: VMware Fusion
- App description: Create, manage, and run virtual machines
- App Version: 12.2.1,18811640
- App Website: https://www.vmware.com/products/fusion.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install VMware Fusion with the following command
   ```
   brew install --cask vmware-fusion
   ```
4. VMware Fusion is ready to use now!