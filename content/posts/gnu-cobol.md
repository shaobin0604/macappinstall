---
title: "Install gnu-cobol on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Implements much of the COBOL 85 and COBOL 2002 standards"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gnu-cobol on MacOS using homebrew

- App Name: gnu-cobol
- App description: Implements much of the COBOL 85 and COBOL 2002 standards
- App Version: 3.1.2
- App Website: https://sourceforge.net/projects/gnucobol/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gnu-cobol with the following command
   ```
   brew install gnu-cobol
   ```
4. gnu-cobol is ready to use now!