---
title: "Install ipv6calc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Small utility for manipulating IPv6 addresses"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ipv6calc on MacOS using homebrew

- App Name: ipv6calc
- App description: Small utility for manipulating IPv6 addresses
- App Version: 4.0.1
- App Website: https://www.deepspace6.net/projects/ipv6calc.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ipv6calc with the following command
   ```
   brew install ipv6calc
   ```
4. ipv6calc is ready to use now!