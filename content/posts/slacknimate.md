---
title: "Install slacknimate on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Text animation for Slack messages"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install slacknimate on MacOS using homebrew

- App Name: slacknimate
- App description: Text animation for Slack messages
- App Version: 1.1.0
- App Website: https://github.com/mroth/slacknimate

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install slacknimate with the following command
   ```
   brew install slacknimate
   ```
4. slacknimate is ready to use now!