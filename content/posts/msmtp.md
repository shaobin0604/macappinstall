---
title: "Install msmtp on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "SMTP client that can be used as an SMTP plugin for Mutt"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install msmtp on MacOS using homebrew

- App Name: msmtp
- App description: SMTP client that can be used as an SMTP plugin for Mutt
- App Version: 1.8.19
- App Website: https://marlam.de/msmtp/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install msmtp with the following command
   ```
   brew install msmtp
   ```
4. msmtp is ready to use now!