---
title: "Install Spectacle Editor on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Drag and drop Spectacle editor"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Spectacle Editor on MacOS using homebrew

- App Name: Spectacle Editor
- App description: Drag and drop Spectacle editor
- App Version: 0.1.6
- App Website: https://github.com/plotly/spectacle-editor

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Spectacle Editor with the following command
   ```
   brew install --cask spectacle-editor
   ```
4. Spectacle Editor is ready to use now!