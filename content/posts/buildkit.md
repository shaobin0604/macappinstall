---
title: "Install buildkit on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Сoncurrent, cache-efficient, and Dockerfile-agnostic builder toolkit"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install buildkit on MacOS using homebrew

- App Name: buildkit
- App description: Сoncurrent, cache-efficient, and Dockerfile-agnostic builder toolkit
- App Version: 0.9.3
- App Website: https://github.com/moby/buildkit

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install buildkit with the following command
   ```
   brew install buildkit
   ```
4. buildkit is ready to use now!