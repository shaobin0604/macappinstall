---
title: "Install osm-pbf on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tools related to PBF (an alternative to XML format)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install osm-pbf on MacOS using homebrew

- App Name: osm-pbf
- App description: Tools related to PBF (an alternative to XML format)
- App Version: 1.5.0
- App Website: https://wiki.openstreetmap.org/wiki/PBF_Format

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install osm-pbf with the following command
   ```
   brew install osm-pbf
   ```
4. osm-pbf is ready to use now!