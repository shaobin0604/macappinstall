---
title: "Install singular on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Computer algebra system for polynomial computations"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install singular on MacOS using homebrew

- App Name: singular
- App description: Computer algebra system for polynomial computations
- App Version: 4.2.1
- App Website: https://www.singular.uni-kl.de/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install singular with the following command
   ```
   brew install singular
   ```
4. singular is ready to use now!