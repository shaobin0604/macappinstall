---
title: "Install LyricsX on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Lyrics for iTunes, Spotify, Vox and Audirvana Plus"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install LyricsX on MacOS using homebrew

- App Name: LyricsX
- App description: Lyrics for iTunes, Spotify, Vox and Audirvana Plus
- App Version: 1.6.1,2341
- App Website: https://github.com/ddddxxx/LyricsX

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install LyricsX with the following command
   ```
   brew install --cask lyricsx
   ```
4. LyricsX is ready to use now!