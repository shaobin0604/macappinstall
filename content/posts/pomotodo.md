---
title: "Install Pomododo on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Time management app for creators"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Pomododo on MacOS using homebrew

- App Name: Pomododo
- App description: Time management app for creators
- App Version: 3.4.2,1508736942
- App Website: https://pomotodo.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Pomododo with the following command
   ```
   brew install --cask pomotodo
   ```
4. Pomododo is ready to use now!