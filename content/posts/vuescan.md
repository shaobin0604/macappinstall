---
title: "Install VueScan on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "App that provides drivers for older model scanners that are no longer supported"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install VueScan on MacOS using homebrew

- App Name: VueScan
- App description: App that provides drivers for older model scanners that are no longer supported
- App Version: 9.7.78
- App Website: https://www.hamrick.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install VueScan with the following command
   ```
   brew install --cask vuescan
   ```
4. VueScan is ready to use now!