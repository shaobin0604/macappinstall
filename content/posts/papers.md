---
title: "Install ReadCube Papers on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Reference management software for researchers"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ReadCube Papers on MacOS using homebrew

- App Name: ReadCube Papers
- App description: Reference management software for researchers
- App Version: latest
- App Website: https://www.readcube.com/home

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ReadCube Papers with the following command
   ```
   brew install --cask papers
   ```
4. ReadCube Papers is ready to use now!