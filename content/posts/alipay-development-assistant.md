---
title: "Install Alipay Development Assistant on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Alipay's open platform development assistant"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Alipay Development Assistant on MacOS using homebrew

- App Name: Alipay Development Assistant
- App description: Alipay's open platform development assistant
- App Version: 1.0.7
- App Website: https://opendocs.alipay.com/open/291/introduce

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Alipay Development Assistant with the following command
   ```
   brew install --cask alipay-development-assistant
   ```
4. Alipay Development Assistant is ready to use now!