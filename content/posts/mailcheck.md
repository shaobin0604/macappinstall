---
title: "Install mailcheck on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Check multiple mailboxes/maildirs for mail"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mailcheck on MacOS using homebrew

- App Name: mailcheck
- App description: Check multiple mailboxes/maildirs for mail
- App Version: 1.91.2
- App Website: https://mailcheck.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mailcheck with the following command
   ```
   brew install mailcheck
   ```
4. mailcheck is ready to use now!