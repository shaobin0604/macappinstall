---
title: "Install spawn-fcgi on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Spawn fast-CGI processes"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install spawn-fcgi on MacOS using homebrew

- App Name: spawn-fcgi
- App description: Spawn fast-CGI processes
- App Version: 1.6.4
- App Website: https://redmine.lighttpd.net/projects/spawn-fcgi

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install spawn-fcgi with the following command
   ```
   brew install spawn-fcgi
   ```
4. spawn-fcgi is ready to use now!