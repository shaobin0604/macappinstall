---
title: "Install typescript on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Language for application scale JavaScript development"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install typescript on MacOS using homebrew

- App Name: typescript
- App description: Language for application scale JavaScript development
- App Version: 4.5.5
- App Website: https://www.typescriptlang.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install typescript with the following command
   ```
   brew install typescript
   ```
4. typescript is ready to use now!