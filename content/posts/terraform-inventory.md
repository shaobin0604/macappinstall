---
title: "Install terraform-inventory on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Terraform State → Ansible Dynamic Inventory"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install terraform-inventory on MacOS using homebrew

- App Name: terraform-inventory
- App description: Terraform State → Ansible Dynamic Inventory
- App Version: 0.10
- App Website: https://github.com/adammck/terraform-inventory

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install terraform-inventory with the following command
   ```
   brew install terraform-inventory
   ```
4. terraform-inventory is ready to use now!