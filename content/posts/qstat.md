---
title: "Install qstat on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Query Quake servers from the command-line"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install qstat on MacOS using homebrew

- App Name: qstat
- App description: Query Quake servers from the command-line
- App Version: 2.17
- App Website: https://github.com/multiplay/qstat

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install qstat with the following command
   ```
   brew install qstat
   ```
4. qstat is ready to use now!