---
title: "Install The Unarchiver on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Unpacks archive files"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install The Unarchiver on MacOS using homebrew

- App Name: The Unarchiver
- App description: Unpacks archive files
- App Version: 4.3.0,132,1618399262
- App Website: https://theunarchiver.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install The Unarchiver with the following command
   ```
   brew install --cask the-unarchiver
   ```
4. The Unarchiver is ready to use now!