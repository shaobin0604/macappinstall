---
title: "Install crun on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Fast and lightweight fully featured OCI runtime and C library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install crun on MacOS using homebrew

- App Name: crun
- App description: Fast and lightweight fully featured OCI runtime and C library
- App Version: 1.4.2
- App Website: https://github.com/containers/crun

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install crun with the following command
   ```
   brew install crun
   ```
4. crun is ready to use now!