---
title: "Install iThoughtsX on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Mind mapping tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install iThoughtsX on MacOS using homebrew

- App Name: iThoughtsX
- App description: Mind mapping tool
- App Version: 5.29.0
- App Website: https://www.toketaware.com/ithoughts-osx

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install iThoughtsX with the following command
   ```
   brew install --cask ithoughtsx
   ```
4. iThoughtsX is ready to use now!