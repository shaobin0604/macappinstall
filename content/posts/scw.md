---
title: "Install scw on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line Interface for Scaleway"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install scw on MacOS using homebrew

- App Name: scw
- App description: Command-line Interface for Scaleway
- App Version: 2.4.0
- App Website: https://github.com/scaleway/scaleway-cli

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install scw with the following command
   ```
   brew install scw
   ```
4. scw is ready to use now!