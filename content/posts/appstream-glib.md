---
title: "Install appstream-glib on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Helper library for reading and writing AppStream metadata"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install appstream-glib on MacOS using homebrew

- App Name: appstream-glib
- App description: Helper library for reading and writing AppStream metadata
- App Version: 0.7.18
- App Website: https://github.com/hughsie/appstream-glib

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install appstream-glib with the following command
   ```
   brew install appstream-glib
   ```
4. appstream-glib is ready to use now!