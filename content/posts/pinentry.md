---
title: "Install pinentry on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Passphrase entry dialog utilizing the Assuan protocol"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pinentry on MacOS using homebrew

- App Name: pinentry
- App description: Passphrase entry dialog utilizing the Assuan protocol
- App Version: 1.2.0
- App Website: https://www.gnupg.org/related_software/pinentry/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pinentry with the following command
   ```
   brew install pinentry
   ```
4. pinentry is ready to use now!