---
title: "Install a2ps on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Any-to-PostScript filter"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install a2ps on MacOS using homebrew

- App Name: a2ps
- App description: Any-to-PostScript filter
- App Version: 4.14
- App Website: https://www.gnu.org/software/a2ps/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install a2ps with the following command
   ```
   brew install a2ps
   ```
4. a2ps is ready to use now!