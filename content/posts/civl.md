---
title: "Install civl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Concurrency Intermediate Verification Language"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install civl on MacOS using homebrew

- App Name: civl
- App description: Concurrency Intermediate Verification Language
- App Version: 1.20-5259
- App Website: https://vsl.cis.udel.edu/civl/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install civl with the following command
   ```
   brew install civl
   ```
4. civl is ready to use now!