---
title: "Install qdae on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Quick and Dirty Apricot Emulator"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install qdae on MacOS using homebrew

- App Name: qdae
- App description: Quick and Dirty Apricot Emulator
- App Version: 0.0.10
- App Website: https://www.seasip.info/Unix/QDAE/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install qdae with the following command
   ```
   brew install qdae
   ```
4. qdae is ready to use now!