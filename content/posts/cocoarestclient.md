---
title: "Install CocoaRestClient on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "App for testing HTTP/REST endpoints"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install CocoaRestClient on MacOS using homebrew

- App Name: CocoaRestClient
- App description: App for testing HTTP/REST endpoints
- App Version: 1.4.7
- App Website: https://mmattozzi.github.io/cocoa-rest-client/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install CocoaRestClient with the following command
   ```
   brew install --cask cocoarestclient
   ```
4. CocoaRestClient is ready to use now!