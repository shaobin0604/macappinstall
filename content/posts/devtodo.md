---
title: "Install devtodo on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line task lists"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install devtodo on MacOS using homebrew

- App Name: devtodo
- App description: Command-line task lists
- App Version: 0.1.20
- App Website: https://swapoff.org/devtodo.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install devtodo with the following command
   ```
   brew install devtodo
   ```
4. devtodo is ready to use now!