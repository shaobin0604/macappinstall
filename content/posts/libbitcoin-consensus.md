---
title: "Install libbitcoin-consensus on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Bitcoin Consensus Library (optional)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libbitcoin-consensus on MacOS using homebrew

- App Name: libbitcoin-consensus
- App description: Bitcoin Consensus Library (optional)
- App Version: 3.6.0
- App Website: https://github.com/libbitcoin/libbitcoin-consensus

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libbitcoin-consensus with the following command
   ```
   brew install libbitcoin-consensus
   ```
4. libbitcoin-consensus is ready to use now!