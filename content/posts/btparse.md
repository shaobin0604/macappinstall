---
title: "Install btparse on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "BibTeX utility libraries"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install btparse on MacOS using homebrew

- App Name: btparse
- App description: BibTeX utility libraries
- App Version: 0.35
- App Website: https://metacpan.org/pod/distribution/Text-BibTeX/btparse/doc/btparse.pod

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install btparse with the following command
   ```
   brew install btparse
   ```
4. btparse is ready to use now!