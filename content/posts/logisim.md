---
title: "Install Logisim on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool for designing and simulating digital logic circuits"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Logisim on MacOS using homebrew

- App Name: Logisim
- App description: Tool for designing and simulating digital logic circuits
- App Version: 2.7.1
- App Website: http://www.cburch.com/logisim/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Logisim with the following command
   ```
   brew install --cask logisim
   ```
4. Logisim is ready to use now!