---
title: "Install darglint on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Python docstring argument linter"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install darglint on MacOS using homebrew

- App Name: darglint
- App description: Python docstring argument linter
- App Version: 1.8.1
- App Website: https://github.com/terrencepreilly/darglint

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install darglint with the following command
   ```
   brew install darglint
   ```
4. darglint is ready to use now!