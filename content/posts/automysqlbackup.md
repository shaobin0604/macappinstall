---
title: "Install automysqlbackup on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Automate MySQL backups"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install automysqlbackup on MacOS using homebrew

- App Name: automysqlbackup
- App description: Automate MySQL backups
- App Version: 3.0-rc6
- App Website: https://sourceforge.net/projects/automysqlbackup/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install automysqlbackup with the following command
   ```
   brew install automysqlbackup
   ```
4. automysqlbackup is ready to use now!