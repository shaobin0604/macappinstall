---
title: "Install stanford-corenlp on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Java suite of core NLP tools"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install stanford-corenlp on MacOS using homebrew

- App Name: stanford-corenlp
- App description: Java suite of core NLP tools
- App Version: 4.4.0
- App Website: https://stanfordnlp.github.io/CoreNLP/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install stanford-corenlp with the following command
   ```
   brew install stanford-corenlp
   ```
4. stanford-corenlp is ready to use now!