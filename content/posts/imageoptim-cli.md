---
title: "Install imageoptim-cli on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CLI for ImageOptim, ImageAlpha and JPEGmini"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install imageoptim-cli on MacOS using homebrew

- App Name: imageoptim-cli
- App description: CLI for ImageOptim, ImageAlpha and JPEGmini
- App Version: 3.0.7
- App Website: https://jamiemason.github.io/ImageOptim-CLI/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install imageoptim-cli with the following command
   ```
   brew install imageoptim-cli
   ```
4. imageoptim-cli is ready to use now!