---
title: "Install aurora on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Beanstalkd queue server console"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install aurora on MacOS using homebrew

- App Name: aurora
- App description: Beanstalkd queue server console
- App Version: 2.2
- App Website: https://xuri.me/aurora

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install aurora with the following command
   ```
   brew install aurora
   ```
4. aurora is ready to use now!