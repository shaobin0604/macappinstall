---
title: "Install pinentry-mac on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Pinentry for GPG on Mac"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pinentry-mac on MacOS using homebrew

- App Name: pinentry-mac
- App description: Pinentry for GPG on Mac
- App Version: 1.1.1.1
- App Website: https://github.com/GPGTools/pinentry

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pinentry-mac with the following command
   ```
   brew install pinentry-mac
   ```
4. pinentry-mac is ready to use now!