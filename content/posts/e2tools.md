---
title: "Install e2tools on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utilities to read, write, and manipulate files in ext2/3/4 filesystems"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install e2tools on MacOS using homebrew

- App Name: e2tools
- App description: Utilities to read, write, and manipulate files in ext2/3/4 filesystems
- App Version: 0.1.0
- App Website: https://e2tools.github.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install e2tools with the following command
   ```
   brew install e2tools
   ```
4. e2tools is ready to use now!