---
title: "Install cgal on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Computational Geometry Algorithms Library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cgal on MacOS using homebrew

- App Name: cgal
- App description: Computational Geometry Algorithms Library
- App Version: 5.4
- App Website: https://www.cgal.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cgal with the following command
   ```
   brew install cgal
   ```
4. cgal is ready to use now!