---
title: "Install curseofwar on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Fast-paced action strategy game"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install curseofwar on MacOS using homebrew

- App Name: curseofwar
- App description: Fast-paced action strategy game
- App Version: 1.3.0
- App Website: https://a-nikolaev.github.io/curseofwar/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install curseofwar with the following command
   ```
   brew install curseofwar
   ```
4. curseofwar is ready to use now!