---
title: "Install gst-python on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Python overrides for gobject-introspection-based pygst bindings"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gst-python on MacOS using homebrew

- App Name: gst-python
- App description: Python overrides for gobject-introspection-based pygst bindings
- App Version: 1.18.5
- App Website: https://gstreamer.freedesktop.org/modules/gst-python.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gst-python with the following command
   ```
   brew install gst-python
   ```
4. gst-python is ready to use now!