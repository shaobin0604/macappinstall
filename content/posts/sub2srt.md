---
title: "Install sub2srt on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Convert subtitles from .sub to subviewer .srt format"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sub2srt on MacOS using homebrew

- App Name: sub2srt
- App description: Convert subtitles from .sub to subviewer .srt format
- App Version: 0.5.5
- App Website: https://github.com/robelix/sub2srt

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sub2srt with the following command
   ```
   brew install sub2srt
   ```
4. sub2srt is ready to use now!