---
title: "Install IRCCloud Desktop on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "IRC client"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install IRCCloud Desktop on MacOS using homebrew

- App Name: IRCCloud Desktop
- App description: IRC client
- App Version: 0.16.0
- App Website: https://github.com/irccloud/irccloud-desktop

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install IRCCloud Desktop with the following command
   ```
   brew install --cask irccloud
   ```
4. IRCCloud Desktop is ready to use now!