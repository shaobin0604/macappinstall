---
title: "Install qd on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C++/Fortran-90 double-double and quad-double package"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install qd on MacOS using homebrew

- App Name: qd
- App description: C++/Fortran-90 double-double and quad-double package
- App Version: 2.3.23
- App Website: https://www.davidhbailey.com/dhbsoftware/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install qd with the following command
   ```
   brew install qd
   ```
4. qd is ready to use now!