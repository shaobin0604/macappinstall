---
title: "Install jenkins-job-builder on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Configure Jenkins jobs with YAML files stored in Git"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install jenkins-job-builder on MacOS using homebrew

- App Name: jenkins-job-builder
- App description: Configure Jenkins jobs with YAML files stored in Git
- App Version: 3.12.0
- App Website: https://docs.openstack.org/infra/jenkins-job-builder/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install jenkins-job-builder with the following command
   ```
   brew install jenkins-job-builder
   ```
4. jenkins-job-builder is ready to use now!