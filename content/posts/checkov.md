---
title: "Install checkov on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Prevent cloud misconfigurations during build-time for IaC tools"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install checkov on MacOS using homebrew

- App Name: checkov
- App description: Prevent cloud misconfigurations during build-time for IaC tools
- App Version: 2.0.855
- App Website: https://www.checkov.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install checkov with the following command
   ```
   brew install checkov
   ```
4. checkov is ready to use now!