---
title: "Install python-launcher on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Launch your Python interpreter the lazy/smart way"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install python-launcher on MacOS using homebrew

- App Name: python-launcher
- App description: Launch your Python interpreter the lazy/smart way
- App Version: 1.0.0
- App Website: https://github.com/brettcannon/python-launcher

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install python-launcher with the following command
   ```
   brew install python-launcher
   ```
4. python-launcher is ready to use now!