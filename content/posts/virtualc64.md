---
title: "Install VirtualC64 on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cycle-accurate C64 emulator"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install VirtualC64 on MacOS using homebrew

- App Name: VirtualC64
- App description: Cycle-accurate C64 emulator
- App Version: 4.4
- App Website: https://dirkwhoffmann.github.io/virtualc64

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install VirtualC64 with the following command
   ```
   brew install --cask virtualc64
   ```
4. VirtualC64 is ready to use now!