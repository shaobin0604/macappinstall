---
title: "Install Minecraft Education Edition on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Educational version of Minecraft"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Minecraft Education Edition on MacOS using homebrew

- App Name: Minecraft Education Edition
- App description: Educational version of Minecraft
- App Version: 1.14.50.0
- App Website: https://education.minecraft.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Minecraft Education Edition with the following command
   ```
   brew install --cask minecraftpe
   ```
4. Minecraft Education Edition is ready to use now!