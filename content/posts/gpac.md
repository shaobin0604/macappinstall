---
title: "Install gpac on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Multimedia framework for research and academic purposes"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gpac on MacOS using homebrew

- App Name: gpac
- App description: Multimedia framework for research and academic purposes
- App Version: 1.0.1
- App Website: https://gpac.wp.mines-telecom.fr/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gpac with the following command
   ```
   brew install gpac
   ```
4. gpac is ready to use now!