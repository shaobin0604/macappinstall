---
title: "Install Spatterlight on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Play most kinds of interactive fiction game files"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Spatterlight on MacOS using homebrew

- App Name: Spatterlight
- App description: Play most kinds of interactive fiction game files
- App Version: 0.9.1
- App Website: https://ccxvii.net/spatterlight/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Spatterlight with the following command
   ```
   brew install --cask spatterlight
   ```
4. Spatterlight is ready to use now!