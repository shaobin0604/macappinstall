---
title: "Install libuv on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Multi-platform support library with a focus on asynchronous I/O"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libuv on MacOS using homebrew

- App Name: libuv
- App description: Multi-platform support library with a focus on asynchronous I/O
- App Version: 1.43.0
- App Website: https://libuv.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libuv with the following command
   ```
   brew install libuv
   ```
4. libuv is ready to use now!