---
title: "Install Cube 2: Sauerbraten on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Multiplayer & singleplayer first person shooter"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Cube 2: Sauerbraten on MacOS using homebrew

- App Name: Cube 2: Sauerbraten
- App description: Multiplayer & singleplayer first person shooter
- App Version: 2020.11.29,2020.12.21
- App Website: http://sauerbraten.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Cube 2: Sauerbraten with the following command
   ```
   brew install --cask sauerbraten
   ```
4. Cube 2: Sauerbraten is ready to use now!