---
title: "Install oh-my-posh on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Prompt theme engine for any shell"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install oh-my-posh on MacOS using homebrew

- App Name: oh-my-posh
- App description: Prompt theme engine for any shell
- App Version: 7.18.0
- App Website: https://ohmyposh.dev

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install oh-my-posh with the following command
   ```
   brew install oh-my-posh
   ```
4. oh-my-posh is ready to use now!