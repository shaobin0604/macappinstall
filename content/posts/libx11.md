---
title: "Install libx11 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "X.Org: Core X11 protocol client library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libx11 on MacOS using homebrew

- App Name: libx11
- App description: X.Org: Core X11 protocol client library
- App Version: 1.7.3.1
- App Website: https://www.x.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libx11 with the following command
   ```
   brew install libx11
   ```
4. libx11 is ready to use now!