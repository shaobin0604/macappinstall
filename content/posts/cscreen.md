---
title: "Install cscreen on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line display management utility"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cscreen on MacOS using homebrew

- App Name: cscreen
- App description: Command-line display management utility
- App Version: 2012.09
- App Website: https://www.pyehouse.com/cscreen/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cscreen with the following command
   ```
   brew install --cask cscreen
   ```
4. cscreen is ready to use now!