---
title: "Install goose on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Go Language's command-line interface for database migrations"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install goose on MacOS using homebrew

- App Name: goose
- App description: Go Language's command-line interface for database migrations
- App Version: 3.5.3
- App Website: https://github.com/pressly/goose

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install goose with the following command
   ```
   brew install goose
   ```
4. goose is ready to use now!