---
title: "Install cassowary on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Modern cross-platform HTTP load-testing tool written in Go"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cassowary on MacOS using homebrew

- App Name: cassowary
- App description: Modern cross-platform HTTP load-testing tool written in Go
- App Version: 0.14.0
- App Website: https://github.com/rogerwelin/cassowary

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cassowary with the following command
   ```
   brew install cassowary
   ```
4. cassowary is ready to use now!