---
title: "Install chrome-cli on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Control Google Chrome from the command-line"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install chrome-cli on MacOS using homebrew

- App Name: chrome-cli
- App description: Control Google Chrome from the command-line
- App Version: 1.7.1
- App Website: https://github.com/prasmussen/chrome-cli

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install chrome-cli with the following command
   ```
   brew install chrome-cli
   ```
4. chrome-cli is ready to use now!