---
title: "Install libcap on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "User-space interfaces to POSIX 1003.1e capabilities"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libcap on MacOS using homebrew

- App Name: libcap
- App description: User-space interfaces to POSIX 1003.1e capabilities
- App Version: 2.63
- App Website: https://sites.google.com/site/fullycapable/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libcap with the following command
   ```
   brew install libcap
   ```
4. libcap is ready to use now!