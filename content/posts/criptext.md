---
title: "Install Criptext on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Email service that's built around privacy"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Criptext on MacOS using homebrew

- App Name: Criptext
- App description: Email service that's built around privacy
- App Version: 0.31.0,2.0.82
- App Website: https://criptext.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Criptext with the following command
   ```
   brew install --cask criptext
   ```
4. Criptext is ready to use now!