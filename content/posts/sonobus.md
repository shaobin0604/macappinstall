---
title: "Install SonoBus on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "High-quality network audio streaming"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install SonoBus on MacOS using homebrew

- App Name: SonoBus
- App description: High-quality network audio streaming
- App Version: 1.4.9
- App Website: https://sonobus.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install SonoBus with the following command
   ```
   brew install --cask sonobus
   ```
4. SonoBus is ready to use now!