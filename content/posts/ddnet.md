---
title: "Install DDNet on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cooperative online platform game based on Teeworlds"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install DDNet on MacOS using homebrew

- App Name: DDNet
- App description: Cooperative online platform game based on Teeworlds
- App Version: 15.9.1
- App Website: https://ddnet.tw/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install DDNet with the following command
   ```
   brew install --cask ddnet
   ```
4. DDNet is ready to use now!