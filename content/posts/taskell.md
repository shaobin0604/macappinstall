---
title: "Install taskell on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line Kanban board/task manager with support for Trello"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install taskell on MacOS using homebrew

- App Name: taskell
- App description: Command-line Kanban board/task manager with support for Trello
- App Version: 1.11.4
- App Website: https://taskell.app

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install taskell with the following command
   ```
   brew install taskell
   ```
4. taskell is ready to use now!