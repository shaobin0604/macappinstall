---
title: "Install scamper on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Advanced traceroute and network measurement utility"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install scamper on MacOS using homebrew

- App Name: scamper
- App description: Advanced traceroute and network measurement utility
- App Version: 20211212
- App Website: https://www.caida.org/catalog/software/scamper/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install scamper with the following command
   ```
   brew install scamper
   ```
4. scamper is ready to use now!