---
title: "Install goreman on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Foreman clone written in Go"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install goreman on MacOS using homebrew

- App Name: goreman
- App description: Foreman clone written in Go
- App Version: 0.3.9
- App Website: https://github.com/mattn/goreman

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install goreman with the following command
   ```
   brew install goreman
   ```
4. goreman is ready to use now!