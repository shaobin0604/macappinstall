---
title: "Install SwitchHosts on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "App to switch hosts"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install SwitchHosts on MacOS using homebrew

- App Name: SwitchHosts
- App description: App to switch hosts
- App Version: 4.1.1.6077
- App Website: https://oldj.github.io/SwitchHosts/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install SwitchHosts with the following command
   ```
   brew install --cask switchhosts
   ```
4. SwitchHosts is ready to use now!