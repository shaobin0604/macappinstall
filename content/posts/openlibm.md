---
title: "Install openlibm on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "High quality, portable, open source libm implementation"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install openlibm on MacOS using homebrew

- App Name: openlibm
- App description: High quality, portable, open source libm implementation
- App Version: 0.8.1
- App Website: https://openlibm.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install openlibm with the following command
   ```
   brew install openlibm
   ```
4. openlibm is ready to use now!