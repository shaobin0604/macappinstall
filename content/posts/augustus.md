---
title: "Install augustus on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Predict genes in eukaryotic genomic sequences"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install augustus on MacOS using homebrew

- App Name: augustus
- App description: Predict genes in eukaryotic genomic sequences
- App Version: 3.3.3
- App Website: https://bioinf.uni-greifswald.de/augustus/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install augustus with the following command
   ```
   brew install augustus
   ```
4. augustus is ready to use now!