---
title: "Install libscrypt on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library for scrypt"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libscrypt on MacOS using homebrew

- App Name: libscrypt
- App description: Library for scrypt
- App Version: 1.21
- App Website: https://github.com/technion/libscrypt

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libscrypt with the following command
   ```
   brew install libscrypt
   ```
4. libscrypt is ready to use now!