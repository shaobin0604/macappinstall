---
title: "Install iGlance on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "System monitor for the status bar"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install iGlance on MacOS using homebrew

- App Name: iGlance
- App description: System monitor for the status bar
- App Version: 2.1.0
- App Website: https://github.com/iglance/iGlance

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install iGlance with the following command
   ```
   brew install --cask iglance
   ```
4. iGlance is ready to use now!