---
title: "Install mysql++ on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C++ wrapper for MySQL's C API"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mysql++ on MacOS using homebrew

- App Name: mysql++
- App description: C++ wrapper for MySQL's C API
- App Version: 3.3.0
- App Website: https://tangentsoft.com/mysqlpp/home

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mysql++ with the following command
   ```
   brew install mysql++
   ```
4. mysql++ is ready to use now!