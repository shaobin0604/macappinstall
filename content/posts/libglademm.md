---
title: "Install libglademm on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C++ wrapper around libglade"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libglademm on MacOS using homebrew

- App Name: libglademm
- App description: C++ wrapper around libglade
- App Version: 2.6.7
- App Website: https://gnome.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libglademm with the following command
   ```
   brew install libglademm
   ```
4. libglademm is ready to use now!