---
title: "Install EDFbrowser on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "EDF+ and BDF+ viewer and toolbox"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install EDFbrowser on MacOS using homebrew

- App Name: EDFbrowser
- App description: EDF+ and BDF+ viewer and toolbox
- App Version: 1.84,81b147ef06488445bd7ef8fe4b036648
- App Website: https://www.teuniz.net/edfbrowser

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install EDFbrowser with the following command
   ```
   brew install --cask edfbrowser
   ```
4. EDFbrowser is ready to use now!