---
title: "Install V2rayU on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Collection of tools to build a dedicated basic communication network"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install V2rayU on MacOS using homebrew

- App Name: V2rayU
- App description: Collection of tools to build a dedicated basic communication network
- App Version: 3.2.0
- App Website: https://github.com/yanue/V2rayU

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install V2rayU with the following command
   ```
   brew install --cask v2rayu
   ```
4. V2rayU is ready to use now!