---
title: "Install libffcall on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GNU Foreign Function Interface library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libffcall on MacOS using homebrew

- App Name: libffcall
- App description: GNU Foreign Function Interface library
- App Version: 2.4
- App Website: https://www.gnu.org/software/libffcall/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libffcall with the following command
   ```
   brew install libffcall
   ```
4. libffcall is ready to use now!