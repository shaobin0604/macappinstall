---
title: "Install FabFilter Twin on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Synthesizer plug-in"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install FabFilter Twin on MacOS using homebrew

- App Name: FabFilter Twin
- App description: Synthesizer plug-in
- App Version: 2.33
- App Website: https://www.fabfilter.com/products/twin-2-powerful-synthesizer-plug-in

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install FabFilter Twin with the following command
   ```
   brew install --cask fabfilter-twin
   ```
4. FabFilter Twin is ready to use now!