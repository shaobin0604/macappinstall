---
title: "Install SilentKnight on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Automatically checks computer's security"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install SilentKnight on MacOS using homebrew

- App Name: SilentKnight
- App description: Automatically checks computer's security
- App Version: 1.18,2022.01
- App Website: https://eclecticlight.co/lockrattler-systhist/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install SilentKnight with the following command
   ```
   brew install --cask silentknight
   ```
4. SilentKnight is ready to use now!