---
title: "Install swift-protobuf on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Plugin and runtime library for using protobuf with Swift"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install swift-protobuf on MacOS using homebrew

- App Name: swift-protobuf
- App description: Plugin and runtime library for using protobuf with Swift
- App Version: 1.18.0
- App Website: https://github.com/apple/swift-protobuf

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install swift-protobuf with the following command
   ```
   brew install swift-protobuf
   ```
4. swift-protobuf is ready to use now!