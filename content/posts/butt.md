---
title: "Install Broadcast Using This Tool on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Shoutcast and Icecast streaming client"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Broadcast Using This Tool on MacOS using homebrew

- App Name: Broadcast Using This Tool
- App description: Shoutcast and Icecast streaming client
- App Version: 0.1.33
- App Website: https://danielnoethen.de/butt/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Broadcast Using This Tool with the following command
   ```
   brew install --cask butt
   ```
4. Broadcast Using This Tool is ready to use now!