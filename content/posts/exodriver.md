---
title: "Install exodriver on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Thin interface to LabJack devices"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install exodriver on MacOS using homebrew

- App Name: exodriver
- App description: Thin interface to LabJack devices
- App Version: 2.6.0
- App Website: https://labjack.com/support/linux-and-mac-os-x-drivers

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install exodriver with the following command
   ```
   brew install exodriver
   ```
4. exodriver is ready to use now!