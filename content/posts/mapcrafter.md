---
title: "Install mapcrafter on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Minecraft map renderer"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mapcrafter on MacOS using homebrew

- App Name: mapcrafter
- App description: Minecraft map renderer
- App Version: 2.4
- App Website: https://mapcrafter.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mapcrafter with the following command
   ```
   brew install mapcrafter
   ```
4. mapcrafter is ready to use now!