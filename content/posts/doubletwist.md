---
title: "Install doubleTwist on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install doubleTwist on MacOS using homebrew

- App Name: doubleTwist
- App description: null
- App Version: 3.2.0,11870
- App Website: https://www.doubletwist.com/desktop

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install doubleTwist with the following command
   ```
   brew install --cask doubletwist
   ```
4. doubleTwist is ready to use now!