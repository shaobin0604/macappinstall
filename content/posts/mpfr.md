---
title: "Install mpfr on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C library for multiple-precision floating-point computations"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mpfr on MacOS using homebrew

- App Name: mpfr
- App description: C library for multiple-precision floating-point computations
- App Version: 4.1.0
- App Website: https://www.mpfr.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mpfr with the following command
   ```
   brew install mpfr
   ```
4. mpfr is ready to use now!