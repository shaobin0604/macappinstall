---
title: "Install cocot on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Code converter on tty"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cocot on MacOS using homebrew

- App Name: cocot
- App description: Code converter on tty
- App Version: 1.2-20171118
- App Website: https://vmi.jp/software/cygwin/cocot.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cocot with the following command
   ```
   brew install cocot
   ```
4. cocot is ready to use now!