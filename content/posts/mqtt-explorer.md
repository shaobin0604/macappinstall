---
title: "Install MQTT Explorer on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install MQTT Explorer on MacOS using homebrew

- App Name: MQTT Explorer
- App description: null
- App Version: 0.3.5
- App Website: https://mqtt-explorer.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install MQTT Explorer with the following command
   ```
   brew install --cask mqtt-explorer
   ```
4. MQTT Explorer is ready to use now!