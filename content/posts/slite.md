---
title: "Install Slite on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Team communication and collaboration software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Slite on MacOS using homebrew

- App Name: Slite
- App description: Team communication and collaboration software
- App Version: 64
- App Website: https://slite.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Slite with the following command
   ```
   brew install --cask slite
   ```
4. Slite is ready to use now!