---
title: "Install ethereum on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Official Go implementation of the Ethereum protocol"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ethereum on MacOS using homebrew

- App Name: ethereum
- App description: Official Go implementation of the Ethereum protocol
- App Version: 1.10.16
- App Website: https://geth.ethereum.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ethereum with the following command
   ```
   brew install ethereum
   ```
4. ethereum is ready to use now!