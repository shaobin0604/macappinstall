---
title: "Install Bit Fiddle on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Converts decimal, hexadecimal, binary numbers and ASCII characters"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Bit Fiddle on MacOS using homebrew

- App Name: Bit Fiddle
- App description: Converts decimal, hexadecimal, binary numbers and ASCII characters
- App Version: 1.4.2
- App Website: https://manderc.com/apps/bitfiddle/index_en.php

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Bit Fiddle with the following command
   ```
   brew install --cask bit-fiddle
   ```
4. Bit Fiddle is ready to use now!