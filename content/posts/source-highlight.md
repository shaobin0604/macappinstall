---
title: "Install source-highlight on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Source-code syntax highlighter"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install source-highlight on MacOS using homebrew

- App Name: source-highlight
- App description: Source-code syntax highlighter
- App Version: 3.1.9
- App Website: https://www.gnu.org/software/src-highlite/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install source-highlight with the following command
   ```
   brew install source-highlight
   ```
4. source-highlight is ready to use now!