---
title: "Install ChatMate for Facebook on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Facebook Messenger client"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ChatMate for Facebook on MacOS using homebrew

- App Name: ChatMate for Facebook
- App description: Facebook Messenger client
- App Version: 4.3.1,482,1537946763
- App Website: https://chatmate.io/mac/facebook/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ChatMate for Facebook with the following command
   ```
   brew install --cask chatmate-for-facebook
   ```
4. ChatMate for Facebook is ready to use now!