---
title: "Install libodfgen on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "ODF export library for projects using librevenge"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libodfgen on MacOS using homebrew

- App Name: libodfgen
- App description: ODF export library for projects using librevenge
- App Version: 0.1.8
- App Website: https://sourceforge.net/p/libwpd/wiki/libodfgen/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libodfgen with the following command
   ```
   brew install libodfgen
   ```
4. libodfgen is ready to use now!