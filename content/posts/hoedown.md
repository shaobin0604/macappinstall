---
title: "Install hoedown on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Secure Markdown processing (a revived fork of Sundown)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install hoedown on MacOS using homebrew

- App Name: hoedown
- App description: Secure Markdown processing (a revived fork of Sundown)
- App Version: 3.0.7
- App Website: https://github.com/hoedown/hoedown

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install hoedown with the following command
   ```
   brew install hoedown
   ```
4. hoedown is ready to use now!