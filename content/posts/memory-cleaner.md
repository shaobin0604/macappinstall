---
title: "Install Memory Cleaner on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Free up RAM manually and automatically"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Memory Cleaner on MacOS using homebrew

- App Name: Memory Cleaner
- App description: Free up RAM manually and automatically
- App Version: 4.4.1,118
- App Website: https://nektony.com/memory-cleaner

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Memory Cleaner with the following command
   ```
   brew install --cask memory-cleaner
   ```
4. Memory Cleaner is ready to use now!