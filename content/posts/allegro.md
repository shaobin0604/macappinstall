---
title: "Install allegro on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C/C++ multimedia library for cross-platform game development"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install allegro on MacOS using homebrew

- App Name: allegro
- App description: C/C++ multimedia library for cross-platform game development
- App Version: 5.2.7.0
- App Website: https://liballeg.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install allegro with the following command
   ```
   brew install allegro
   ```
4. allegro is ready to use now!