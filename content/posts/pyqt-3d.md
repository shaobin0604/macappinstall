---
title: "Install pyqt-3d on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Python bindings for The Qt Company’s Qt 3D framework"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pyqt-3d on MacOS using homebrew

- App Name: pyqt-3d
- App description: Python bindings for The Qt Company’s Qt 3D framework
- App Version: 6.1.0
- App Website: https://www.riverbankcomputing.com/software/pyqt3d/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pyqt-3d with the following command
   ```
   brew install pyqt-3d
   ```
4. pyqt-3d is ready to use now!