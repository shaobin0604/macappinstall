---
title: "Install Sauce Connect on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Proxy server to securely connect to the Sauce Labs automated testing platform"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Sauce Connect on MacOS using homebrew

- App Name: Sauce Connect
- App description: Proxy server to securely connect to the Sauce Labs automated testing platform
- App Version: 4.7.1
- App Website: https://docs.saucelabs.com/secure-connections/sauce-connect/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Sauce Connect with the following command
   ```
   brew install --cask sauce-connect
   ```
4. Sauce Connect is ready to use now!