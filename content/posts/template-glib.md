---
title: "Install template-glib on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GNOME templating library for GLib"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install template-glib on MacOS using homebrew

- App Name: template-glib
- App description: GNOME templating library for GLib
- App Version: 3.34.0
- App Website: https://gitlab.gnome.org/GNOME/template-glib

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install template-glib with the following command
   ```
   brew install template-glib
   ```
4. template-glib is ready to use now!