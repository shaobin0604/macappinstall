---
title: "Install geph4 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Modular Internet censorship circumvention system to deal with national filtering"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install geph4 on MacOS using homebrew

- App Name: geph4
- App description: Modular Internet censorship circumvention system to deal with national filtering
- App Version: 4.4.20
- App Website: https://geph.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install geph4 with the following command
   ```
   brew install geph4
   ```
4. geph4 is ready to use now!