---
title: "Install pinboard-notes-backup on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Efficiently back up the notes you've saved to Pinboard"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pinboard-notes-backup on MacOS using homebrew

- App Name: pinboard-notes-backup
- App description: Efficiently back up the notes you've saved to Pinboard
- App Version: 1.0.5.3
- App Website: https://github.com/bdesham/pinboard-notes-backup

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pinboard-notes-backup with the following command
   ```
   brew install pinboard-notes-backup
   ```
4. pinboard-notes-backup is ready to use now!