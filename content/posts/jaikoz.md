---
title: "Install Jaikoz on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Audio tag editor"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Jaikoz on MacOS using homebrew

- App Name: Jaikoz
- App description: Audio tag editor
- App Version: 11.0.6,1.0
- App Website: https://www.jthink.net/jaikoz/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Jaikoz with the following command
   ```
   brew install --cask jaikoz
   ```
4. Jaikoz is ready to use now!