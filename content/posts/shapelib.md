---
title: "Install shapelib on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library for reading and writing ArcView Shapefiles"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install shapelib on MacOS using homebrew

- App Name: shapelib
- App description: Library for reading and writing ArcView Shapefiles
- App Version: 1.5.0
- App Website: http://shapelib.maptools.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install shapelib with the following command
   ```
   brew install shapelib
   ```
4. shapelib is ready to use now!