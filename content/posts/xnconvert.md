---
title: "Install XnSoft XnConvert on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Image-converter and resizer tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install XnSoft XnConvert on MacOS using homebrew

- App Name: XnSoft XnConvert
- App description: Image-converter and resizer tool
- App Version: 1.92
- App Website: https://www.xnview.com/en/xnconvert/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install XnSoft XnConvert with the following command
   ```
   brew install --cask xnconvert
   ```
4. XnSoft XnConvert is ready to use now!