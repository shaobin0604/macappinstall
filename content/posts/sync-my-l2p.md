---
title: "Install Sync-my-L2P on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Synchronizes your documents from the L2P and Moodle of RWTH Aachen"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Sync-my-L2P on MacOS using homebrew

- App Name: Sync-my-L2P
- App description: Synchronizes your documents from the L2P and Moodle of RWTH Aachen
- App Version: 2.4.5
- App Website: https://www.syncmyl2p.de/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Sync-my-L2P with the following command
   ```
   brew install --cask sync-my-l2p
   ```
4. Sync-my-L2P is ready to use now!