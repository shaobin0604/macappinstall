---
title: "Install SEGGER Embedded Studio for ARM on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "IDE for embedded systems"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install SEGGER Embedded Studio for ARM on MacOS using homebrew

- App Name: SEGGER Embedded Studio for ARM
- App description: IDE for embedded systems
- App Version: 5.68
- App Website: https://www.segger.com/products/development-tools/embedded-studio

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install SEGGER Embedded Studio for ARM with the following command
   ```
   brew install --cask segger-embedded-studio-for-arm
   ```
4. SEGGER Embedded Studio for ARM is ready to use now!