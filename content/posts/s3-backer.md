---
title: "Install s3-backer on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "FUSE-based single file backing store via Amazon S3"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install s3-backer on MacOS using homebrew

- App Name: s3-backer
- App description: FUSE-based single file backing store via Amazon S3
- App Version: 1.5.6
- App Website: https://github.com/archiecobbs/s3backer

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install s3-backer with the following command
   ```
   brew install s3-backer
   ```
4. s3-backer is ready to use now!