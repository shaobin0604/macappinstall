---
title: "Install makensis on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "System to create Windows installers"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install makensis on MacOS using homebrew

- App Name: makensis
- App description: System to create Windows installers
- App Version: 3.08
- App Website: https://nsis.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install makensis with the following command
   ```
   brew install makensis
   ```
4. makensis is ready to use now!