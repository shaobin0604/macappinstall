---
title: "Install geomview on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Interactive 3D viewing program"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install geomview on MacOS using homebrew

- App Name: geomview
- App description: Interactive 3D viewing program
- App Version: 1.9.5
- App Website: http://www.geomview.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install geomview with the following command
   ```
   brew install geomview
   ```
4. geomview is ready to use now!