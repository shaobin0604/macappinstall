---
title: "Install dateutils on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tools to manipulate dates with a focus on financial data"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dateutils on MacOS using homebrew

- App Name: dateutils
- App description: Tools to manipulate dates with a focus on financial data
- App Version: 0.4.9
- App Website: https://www.fresse.org/dateutils/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dateutils with the following command
   ```
   brew install dateutils
   ```
4. dateutils is ready to use now!