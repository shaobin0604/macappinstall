---
title: "Install Screencast on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Simple screen video capture application"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Screencast on MacOS using homebrew

- App Name: Screencast
- App description: Simple screen video capture application
- App Version: 0.0.6
- App Website: https://github.com/soh335/Screencast

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Screencast with the following command
   ```
   brew install --cask screencast
   ```
4. Screencast is ready to use now!