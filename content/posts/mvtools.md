---
title: "Install mvtools on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Filters for motion estimation and compensation"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mvtools on MacOS using homebrew

- App Name: mvtools
- App description: Filters for motion estimation and compensation
- App Version: 23
- App Website: https://github.com/dubhater/vapoursynth-mvtools

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mvtools with the following command
   ```
   brew install mvtools
   ```
4. mvtools is ready to use now!