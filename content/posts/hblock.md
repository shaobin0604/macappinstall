---
title: "Install hblock on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Adblocker that creates a hosts file from multiple sources"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install hblock on MacOS using homebrew

- App Name: hblock
- App description: Adblocker that creates a hosts file from multiple sources
- App Version: 3.3.1
- App Website: https://hblock.molinero.dev/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install hblock with the following command
   ```
   brew install hblock
   ```
4. hblock is ready to use now!