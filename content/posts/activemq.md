---
title: "Install activemq on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Apache ActiveMQ: powerful open source messaging server"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install activemq on MacOS using homebrew

- App Name: activemq
- App description: Apache ActiveMQ: powerful open source messaging server
- App Version: 5.16.3
- App Website: https://activemq.apache.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install activemq with the following command
   ```
   brew install activemq
   ```
4. activemq is ready to use now!