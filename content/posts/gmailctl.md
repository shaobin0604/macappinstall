---
title: "Install gmailctl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Declarative configuration for Gmail filters"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gmailctl on MacOS using homebrew

- App Name: gmailctl
- App description: Declarative configuration for Gmail filters
- App Version: 0.10.0
- App Website: https://github.com/mbrt/gmailctl

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gmailctl with the following command
   ```
   brew install gmailctl
   ```
4. gmailctl is ready to use now!