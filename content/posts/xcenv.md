---
title: "Install xcenv on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Xcode version manager"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install xcenv on MacOS using homebrew

- App Name: xcenv
- App description: Xcode version manager
- App Version: 1.2.0
- App Website: https://github.com/xcenv/xcenv

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install xcenv with the following command
   ```
   brew install xcenv
   ```
4. xcenv is ready to use now!