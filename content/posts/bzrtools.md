---
title: "Install bzrtools on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Bazaar plugin that supplies useful additional utilities"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bzrtools on MacOS using homebrew

- App Name: bzrtools
- App description: Bazaar plugin that supplies useful additional utilities
- App Version: 2.6.0
- App Website: http://wiki.bazaar.canonical.com/BzrTools

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bzrtools with the following command
   ```
   brew install bzrtools
   ```
4. bzrtools is ready to use now!