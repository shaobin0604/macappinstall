---
title: "Install Avocode on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Collaborate on design files"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Avocode on MacOS using homebrew

- App Name: Avocode
- App description: Collaborate on design files
- App Version: 4.15.5
- App Website: https://avocode.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Avocode with the following command
   ```
   brew install --cask avocode
   ```
4. Avocode is ready to use now!