---
title: "Install luit on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Filter run between arbitrary application and UTF-8 terminal emulator"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install luit on MacOS using homebrew

- App Name: luit
- App description: Filter run between arbitrary application and UTF-8 terminal emulator
- App Version: 20220111
- App Website: https://invisible-island.net/luit/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install luit with the following command
   ```
   brew install luit
   ```
4. luit is ready to use now!