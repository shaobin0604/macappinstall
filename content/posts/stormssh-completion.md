---
title: "Install stormssh-completion on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Completion for storm-ssh"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install stormssh-completion on MacOS using homebrew

- App Name: stormssh-completion
- App description: Completion for storm-ssh
- App Version: 0.1.1
- App Website: https://github.com/vigo/stormssh-completion

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install stormssh-completion with the following command
   ```
   brew install stormssh-completion
   ```
4. stormssh-completion is ready to use now!