---
title: "Install jellyfish on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Fast, memory-efficient counting of DNA k-mers"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install jellyfish on MacOS using homebrew

- App Name: jellyfish
- App description: Fast, memory-efficient counting of DNA k-mers
- App Version: 2.3.0
- App Website: http://www.genome.umd.edu/jellyfish.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install jellyfish with the following command
   ```
   brew install jellyfish
   ```
4. jellyfish is ready to use now!