---
title: "Install source-to-image on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool for building source and injecting into docker images"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install source-to-image on MacOS using homebrew

- App Name: source-to-image
- App description: Tool for building source and injecting into docker images
- App Version: 1.3.1
- App Website: https://github.com/openshift/source-to-image

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install source-to-image with the following command
   ```
   brew install source-to-image
   ```
4. source-to-image is ready to use now!