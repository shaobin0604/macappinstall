---
title: "Install yacas on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "General purpose computer algebra system"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install yacas on MacOS using homebrew

- App Name: yacas
- App description: General purpose computer algebra system
- App Version: 1.9.1
- App Website: https://www.yacas.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install yacas with the following command
   ```
   brew install yacas
   ```
4. yacas is ready to use now!