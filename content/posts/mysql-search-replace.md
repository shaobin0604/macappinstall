---
title: "Install mysql-search-replace on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Database search and replace script in PHP"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mysql-search-replace on MacOS using homebrew

- App Name: mysql-search-replace
- App description: Database search and replace script in PHP
- App Version: 4.1.2
- App Website: https://interconnectit.com/products/search-and-replace-for-wordpress-databases/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mysql-search-replace with the following command
   ```
   brew install mysql-search-replace
   ```
4. mysql-search-replace is ready to use now!