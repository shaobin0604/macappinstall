---
title: "Install libxaw3d on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "X.Org: 3D Athena widget set based on the Xt library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libxaw3d on MacOS using homebrew

- App Name: libxaw3d
- App description: X.Org: 3D Athena widget set based on the Xt library
- App Version: 1.6.3
- App Website: https://www.x.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libxaw3d with the following command
   ```
   brew install libxaw3d
   ```
4. libxaw3d is ready to use now!