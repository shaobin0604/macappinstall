---
title: "Install Samsung DeX on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Extend some Samsung devices into a desktop-like experience"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Samsung DeX on MacOS using homebrew

- App Name: Samsung DeX
- App description: Extend some Samsung devices into a desktop-like experience
- App Version: 20211210154322368
- App Website: https://www.samsung.com/us/explore/dex/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Samsung DeX with the following command
   ```
   brew install --cask samsung-dex
   ```
4. Samsung DeX is ready to use now!