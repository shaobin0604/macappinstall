---
title: "Install DueFocus on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Time tracking and productivity software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install DueFocus on MacOS using homebrew

- App Name: DueFocus
- App description: Time tracking and productivity software
- App Version: 2.5.0
- App Website: https://duefocus.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install DueFocus with the following command
   ```
   brew install --cask duefocus
   ```
4. DueFocus is ready to use now!