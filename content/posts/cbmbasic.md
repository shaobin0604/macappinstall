---
title: "Install cbmbasic on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Commodore BASIC V2 as a scripting language"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cbmbasic on MacOS using homebrew

- App Name: cbmbasic
- App description: Commodore BASIC V2 as a scripting language
- App Version: 1.0
- App Website: https://github.com/mist64/cbmbasic

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cbmbasic with the following command
   ```
   brew install cbmbasic
   ```
4. cbmbasic is ready to use now!