---
title: "Install algol68g on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Algol 68 compiler-interpreter"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install algol68g on MacOS using homebrew

- App Name: algol68g
- App description: Algol 68 compiler-interpreter
- App Version: 2.8.5
- App Website: https://jmvdveer.home.xs4all.nl/algol.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install algol68g with the following command
   ```
   brew install algol68g
   ```
4. algol68g is ready to use now!