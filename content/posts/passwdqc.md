---
title: "Install passwdqc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Password/passphrase strength checking and enforcement toolset"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install passwdqc on MacOS using homebrew

- App Name: passwdqc
- App description: Password/passphrase strength checking and enforcement toolset
- App Version: 2.0.2
- App Website: https://www.openwall.com/passwdqc/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install passwdqc with the following command
   ```
   brew install passwdqc
   ```
4. passwdqc is ready to use now!