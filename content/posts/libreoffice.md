---
title: "Install LibreOffice on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Office suite"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install LibreOffice on MacOS using homebrew

- App Name: LibreOffice
- App description: Office suite
- App Version: 7.3.0
- App Website: https://www.libreoffice.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install LibreOffice with the following command
   ```
   brew install --cask libreoffice
   ```
4. LibreOffice is ready to use now!