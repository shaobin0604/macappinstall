---
title: "Install libgsm on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Lossy speech compression library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libgsm on MacOS using homebrew

- App Name: libgsm
- App description: Lossy speech compression library
- App Version: 1.0.19
- App Website: http://www.quut.com/gsm/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libgsm with the following command
   ```
   brew install libgsm
   ```
4. libgsm is ready to use now!