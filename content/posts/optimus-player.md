---
title: "Install Optimus Player on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Media player"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Optimus Player on MacOS using homebrew

- App Name: Optimus Player
- App description: Media player
- App Version: 1.4,12
- App Website: https://www.optimusplayer.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Optimus Player with the following command
   ```
   brew install --cask optimus-player
   ```
4. Optimus Player is ready to use now!