---
title: "Install dcd on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Auto-complete program for the D programming language"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dcd on MacOS using homebrew

- App Name: dcd
- App description: Auto-complete program for the D programming language
- App Version: 0.13.6
- App Website: https://github.com/dlang-community/DCD

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dcd with the following command
   ```
   brew install dcd
   ```
4. dcd is ready to use now!