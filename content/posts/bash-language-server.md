---
title: "Install bash-language-server on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Language Server for Bash"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bash-language-server on MacOS using homebrew

- App Name: bash-language-server
- App description: Language Server for Bash
- App Version: 2.0.0
- App Website: https://github.com/bash-lsp/bash-language-server

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bash-language-server with the following command
   ```
   brew install bash-language-server
   ```
4. bash-language-server is ready to use now!