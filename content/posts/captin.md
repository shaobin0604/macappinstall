---
title: "Install Captin on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to show caps lock status"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Captin on MacOS using homebrew

- App Name: Captin
- App description: Tool to show caps lock status
- App Version: 1.1.3,143,1619187317
- App Website: http://captin.strikingly.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Captin with the following command
   ```
   brew install --cask captin
   ```
4. Captin is ready to use now!