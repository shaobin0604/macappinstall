---
title: "Install UVtools on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "MSLA/DLP, file analysis, calibration, repair, conversion and manipulation"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install UVtools on MacOS using homebrew

- App Name: UVtools
- App description: MSLA/DLP, file analysis, calibration, repair, conversion and manipulation
- App Version: 2.28.1
- App Website: https://github.com/sn4k3/UVtools

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install UVtools with the following command
   ```
   brew install --cask uvtools
   ```
4. UVtools is ready to use now!