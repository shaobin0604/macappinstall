---
title: "Install gotify on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line interface for pushing messages to gotify/server"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gotify on MacOS using homebrew

- App Name: gotify
- App description: Command-line interface for pushing messages to gotify/server
- App Version: 2.2.1
- App Website: https://github.com/gotify/cli

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gotify with the following command
   ```
   brew install gotify
   ```
4. gotify is ready to use now!