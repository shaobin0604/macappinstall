---
title: "Install oXygen XML Editor on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tools for XML editing, including Oxygen XML Developer and Author"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install oXygen XML Editor on MacOS using homebrew

- App Name: oXygen XML Editor
- App description: Tools for XML editing, including Oxygen XML Developer and Author
- App Version: 24.0,2021121518
- App Website: https://www.oxygenxml.com/xml_editor.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install oXygen XML Editor with the following command
   ```
   brew install --cask oxygen-xml-editor
   ```
4. oXygen XML Editor is ready to use now!