---
title: "Install cminpack on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Solves nonlinear equations and nonlinear least squares problems"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cminpack on MacOS using homebrew

- App Name: cminpack
- App description: Solves nonlinear equations and nonlinear least squares problems
- App Version: 1.3.8
- App Website: http://devernay.free.fr/hacks/cminpack/cminpack.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cminpack with the following command
   ```
   brew install cminpack
   ```
4. cminpack is ready to use now!