---
title: "Install mftrace on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Trace TeX bitmap font to PFA, PFB, or TTF font"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mftrace on MacOS using homebrew

- App Name: mftrace
- App description: Trace TeX bitmap font to PFA, PFB, or TTF font
- App Version: 1.2.20
- App Website: https://lilypond.org/mftrace/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mftrace with the following command
   ```
   brew install mftrace
   ```
4. mftrace is ready to use now!