---
title: "Install Apparency on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Inspect application bundles"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Apparency on MacOS using homebrew

- App Name: Apparency
- App description: Inspect application bundles
- App Version: 1.4,175
- App Website: https://www.mothersruin.com/software/Apparency/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Apparency with the following command
   ```
   brew install --cask apparency
   ```
4. Apparency is ready to use now!