---
title: "Install nacl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Network communication, encryption, decryption, signatures library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install nacl on MacOS using homebrew

- App Name: nacl
- App description: Network communication, encryption, decryption, signatures library
- App Version: 20110221
- App Website: https://nacl.cr.yp.to/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install nacl with the following command
   ```
   brew install nacl
   ```
4. nacl is ready to use now!