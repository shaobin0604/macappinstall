---
title: "Install chgems on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Chroot for Ruby gems"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install chgems on MacOS using homebrew

- App Name: chgems
- App description: Chroot for Ruby gems
- App Version: 0.3.2
- App Website: https://github.com/postmodern/chgems#readme

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install chgems with the following command
   ```
   brew install chgems
   ```
4. chgems is ready to use now!