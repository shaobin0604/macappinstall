---
title: "Install Session on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Onion routing based messenger"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Session on MacOS using homebrew

- App Name: Session
- App description: Onion routing based messenger
- App Version: 1.7.6
- App Website: https://getsession.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Session with the following command
   ```
   brew install --cask session
   ```
4. Session is ready to use now!