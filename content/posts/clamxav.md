---
title: "Install ClamXAV on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Anti-virus and malware scanner"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ClamXAV on MacOS using homebrew

- App Name: ClamXAV
- App description: Anti-virus and malware scanner
- App Version: 3.3.1,9025
- App Website: https://www.clamxav.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ClamXAV with the following command
   ```
   brew install --cask clamxav
   ```
4. ClamXAV is ready to use now!