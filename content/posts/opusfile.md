---
title: "Install opusfile on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "API for decoding and seeking in .opus files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install opusfile on MacOS using homebrew

- App Name: opusfile
- App description: API for decoding and seeking in .opus files
- App Version: 0.12
- App Website: https://www.opus-codec.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install opusfile with the following command
   ```
   brew install opusfile
   ```
4. opusfile is ready to use now!