---
title: "Install sshs on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Graphical command-line client for SSH"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sshs on MacOS using homebrew

- App Name: sshs
- App description: Graphical command-line client for SSH
- App Version: 2.0.0
- App Website: https://github.com/quantumsheep/sshs

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sshs with the following command
   ```
   brew install sshs
   ```
4. sshs is ready to use now!