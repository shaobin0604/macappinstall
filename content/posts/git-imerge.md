---
title: "Install git-imerge on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Incremental merge for git"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install git-imerge on MacOS using homebrew

- App Name: git-imerge
- App description: Incremental merge for git
- App Version: 1.2.0
- App Website: https://github.com/mhagger/git-imerge

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install git-imerge with the following command
   ```
   brew install git-imerge
   ```
4. git-imerge is ready to use now!