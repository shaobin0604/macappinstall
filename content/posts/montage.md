---
title: "Install montage on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Toolkit for assembling FITS images into custom mosaics"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install montage on MacOS using homebrew

- App Name: montage
- App description: Toolkit for assembling FITS images into custom mosaics
- App Version: 4.0
- App Website: http://montage.ipac.caltech.edu

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install montage with the following command
   ```
   brew install montage
   ```
4. montage is ready to use now!