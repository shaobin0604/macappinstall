---
title: "Install MPLab X IDE on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "IDE for Microchip's microcontrollers and digital signal controllers"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install MPLab X IDE on MacOS using homebrew

- App Name: MPLab X IDE
- App description: IDE for Microchip's microcontrollers and digital signal controllers
- App Version: 5.50
- App Website: https://www.microchip.com/mplab/mplab-x-ide

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install MPLab X IDE with the following command
   ```
   brew install --cask mplabx-ide
   ```
4. MPLab X IDE is ready to use now!