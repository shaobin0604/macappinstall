---
title: "Install graphql-cli on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line tool for common GraphQL development workflows"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install graphql-cli on MacOS using homebrew

- App Name: graphql-cli
- App description: Command-line tool for common GraphQL development workflows
- App Version: 4.1.0
- App Website: https://github.com/Urigo/graphql-cli

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install graphql-cli with the following command
   ```
   brew install graphql-cli
   ```
4. graphql-cli is ready to use now!