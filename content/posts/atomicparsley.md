---
title: "Install atomicparsley on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "MPEG-4 command-line tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install atomicparsley on MacOS using homebrew

- App Name: atomicparsley
- App description: MPEG-4 command-line tool
- App Version: 20210715.151551.e7ad03a
- App Website: https://github.com/wez/atomicparsley

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install atomicparsley with the following command
   ```
   brew install atomicparsley
   ```
4. atomicparsley is ready to use now!