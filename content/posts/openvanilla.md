---
title: "Install OpenVanilla on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Provides common input methods"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install OpenVanilla on MacOS using homebrew

- App Name: OpenVanilla
- App description: Provides common input methods
- App Version: 1.6.2,3300
- App Website: https://openvanilla.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install OpenVanilla with the following command
   ```
   brew install --cask openvanilla
   ```
4. OpenVanilla is ready to use now!