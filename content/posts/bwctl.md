---
title: "Install bwctl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line tool and daemon for network measuring tools"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bwctl on MacOS using homebrew

- App Name: bwctl
- App description: Command-line tool and daemon for network measuring tools
- App Version: 1.5.4
- App Website: https://software.internet2.edu/bwctl/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bwctl with the following command
   ```
   brew install bwctl
   ```
4. bwctl is ready to use now!