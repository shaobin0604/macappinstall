---
title: "Install LibreOffice Language Pack on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Collection of alternate languages for LibreOffice"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install LibreOffice Language Pack on MacOS using homebrew

- App Name: LibreOffice Language Pack
- App description: Collection of alternate languages for LibreOffice
- App Version: 7.3.0
- App Website: https://www.libreoffice.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install LibreOffice Language Pack with the following command
   ```
   brew install --cask libreoffice-language-pack
   ```
4. LibreOffice Language Pack is ready to use now!