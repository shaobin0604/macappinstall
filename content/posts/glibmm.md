---
title: "Install glibmm on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C++ interface to glib"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install glibmm on MacOS using homebrew

- App Name: glibmm
- App description: C++ interface to glib
- App Version: 2.70.0
- App Website: https://www.gtkmm.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install glibmm with the following command
   ```
   brew install glibmm
   ```
4. glibmm is ready to use now!