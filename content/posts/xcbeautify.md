---
title: "Install xcbeautify on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Little beautifier tool for xcodebuild"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install xcbeautify on MacOS using homebrew

- App Name: xcbeautify
- App description: Little beautifier tool for xcodebuild
- App Version: 0.11.0
- App Website: https://github.com/thii/xcbeautify

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install xcbeautify with the following command
   ```
   brew install xcbeautify
   ```
4. xcbeautify is ready to use now!