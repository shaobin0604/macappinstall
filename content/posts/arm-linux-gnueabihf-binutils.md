---
title: "Install arm-linux-gnueabihf-binutils on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "FSF/GNU binutils for cross-compiling to arm-linux"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install arm-linux-gnueabihf-binutils on MacOS using homebrew

- App Name: arm-linux-gnueabihf-binutils
- App description: FSF/GNU binutils for cross-compiling to arm-linux
- App Version: 2.38
- App Website: https://www.gnu.org/software/binutils/binutils.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install arm-linux-gnueabihf-binutils with the following command
   ```
   brew install arm-linux-gnueabihf-binutils
   ```
4. arm-linux-gnueabihf-binutils is ready to use now!