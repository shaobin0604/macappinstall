---
title: "Install range2cidr on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Converts IP ranges to CIDRs"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install range2cidr on MacOS using homebrew

- App Name: range2cidr
- App description: Converts IP ranges to CIDRs
- App Version: 1.2.0
- App Website: https://ipinfo.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install range2cidr with the following command
   ```
   brew install range2cidr
   ```
4. range2cidr is ready to use now!