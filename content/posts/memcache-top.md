---
title: "Install memcache-top on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Grab real-time stats from memcache"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install memcache-top on MacOS using homebrew

- App Name: memcache-top
- App description: Grab real-time stats from memcache
- App Version: 0.6
- App Website: https://code.google.com/archive/p/memcache-top/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install memcache-top with the following command
   ```
   brew install memcache-top
   ```
4. memcache-top is ready to use now!