---
title: "Install Mic Drop on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Quickly mute your microphone with a global shortcut or menu bar control"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Mic Drop on MacOS using homebrew

- App Name: Mic Drop
- App description: Quickly mute your microphone with a global shortcut or menu bar control
- App Version: 1.3.6,1306
- App Website: https://getmicdrop.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Mic Drop with the following command
   ```
   brew install --cask mic-drop
   ```
4. Mic Drop is ready to use now!