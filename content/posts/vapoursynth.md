---
title: "Install vapoursynth on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Video processing framework with simplicity in mind"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install vapoursynth on MacOS using homebrew

- App Name: vapoursynth
- App description: Video processing framework with simplicity in mind
- App Version: 57
- App Website: https://www.vapoursynth.com

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install vapoursynth with the following command
   ```
   brew install vapoursynth
   ```
4. vapoursynth is ready to use now!