---
title: "Install kube-linter on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Static analysis tool for Kubernetes YAML files and Helm charts"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install kube-linter on MacOS using homebrew

- App Name: kube-linter
- App description: Static analysis tool for Kubernetes YAML files and Helm charts
- App Version: 0.2.5
- App Website: https://github.com/stackrox/kube-linter

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install kube-linter with the following command
   ```
   brew install kube-linter
   ```
4. kube-linter is ready to use now!