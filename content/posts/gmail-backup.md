---
title: "Install gmail-backup on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Backup and restore the content of your Gmail account"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gmail-backup on MacOS using homebrew

- App Name: gmail-backup
- App description: Backup and restore the content of your Gmail account
- App Version: 20110307
- App Website: https://code.google.com/archive/p/gmail-backup-com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gmail-backup with the following command
   ```
   brew install gmail-backup
   ```
4. gmail-backup is ready to use now!