---
title: "Install ultracopier on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Replacement for files copy dialogs"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ultracopier on MacOS using homebrew

- App Name: ultracopier
- App description: Replacement for files copy dialogs
- App Version: 2.2.4.14
- App Website: http://ultracopier.first-world.info/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ultracopier with the following command
   ```
   brew install --cask ultracopier
   ```
4. ultracopier is ready to use now!