---
title: "Install sysstat on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Performance monitoring tools for Linux"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sysstat on MacOS using homebrew

- App Name: sysstat
- App description: Performance monitoring tools for Linux
- App Version: 12.5.5
- App Website: https://github.com/sysstat/sysstat

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sysstat with the following command
   ```
   brew install sysstat
   ```
4. sysstat is ready to use now!