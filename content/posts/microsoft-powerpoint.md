---
title: "Install Microsoft PowerPoint on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Presentation software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Microsoft PowerPoint on MacOS using homebrew

- App Name: Microsoft PowerPoint
- App description: Presentation software
- App Version: 16.57.22011101
- App Website: https://products.office.com/en-US/powerpoint

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Microsoft PowerPoint with the following command
   ```
   brew install --cask microsoft-powerpoint
   ```
4. Microsoft PowerPoint is ready to use now!