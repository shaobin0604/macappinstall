---
title: "Install libbitcoin-explorer on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Bitcoin command-line tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libbitcoin-explorer on MacOS using homebrew

- App Name: libbitcoin-explorer
- App description: Bitcoin command-line tool
- App Version: 3.6.0
- App Website: https://github.com/libbitcoin/libbitcoin-explorer

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libbitcoin-explorer with the following command
   ```
   brew install libbitcoin-explorer
   ```
4. libbitcoin-explorer is ready to use now!