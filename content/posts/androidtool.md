---
title: "Install AndroidTool on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "App for recording the screen and installing apps in iOS and Android"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install AndroidTool on MacOS using homebrew

- App Name: AndroidTool
- App description: App for recording the screen and installing apps in iOS and Android
- App Version: 1.66
- App Website: https://github.com/mortenjust/androidtool-mac

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install AndroidTool with the following command
   ```
   brew install --cask androidtool
   ```
4. AndroidTool is ready to use now!