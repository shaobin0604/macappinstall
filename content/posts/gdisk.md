---
title: "Install GPT fdisk on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Disk partitioning tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install GPT fdisk on MacOS using homebrew

- App Name: GPT fdisk
- App description: Disk partitioning tool
- App Version: 1.0.8
- App Website: https://sourceforge.net/projects/gptfdisk/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install GPT fdisk with the following command
   ```
   brew install --cask gdisk
   ```
4. GPT fdisk is ready to use now!