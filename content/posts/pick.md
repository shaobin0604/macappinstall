---
title: "Install pick on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utility to choose one option from a set of choices"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pick on MacOS using homebrew

- App Name: pick
- App description: Utility to choose one option from a set of choices
- App Version: 4.0.0
- App Website: https://github.com/mptre/pick

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pick with the following command
   ```
   brew install pick
   ```
4. pick is ready to use now!