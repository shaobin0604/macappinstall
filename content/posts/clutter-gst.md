---
title: "Install clutter-gst on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "ClutterMedia interface using GStreamer for video and audio"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install clutter-gst on MacOS using homebrew

- App Name: clutter-gst
- App description: ClutterMedia interface using GStreamer for video and audio
- App Version: 3.0.27
- App Website: https://gitlab.gnome.org/GNOME/clutter-gst

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install clutter-gst with the following command
   ```
   brew install clutter-gst
   ```
4. clutter-gst is ready to use now!