---
title: "Install Get iPlayer Automator on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Download and watch BBC and ITV shows"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Get iPlayer Automator on MacOS using homebrew

- App Name: Get iPlayer Automator
- App description: Download and watch BBC and ITV shows
- App Version: 1.21.14,20220207001
- App Website: https://github.com/Ascoware/get-iplayer-automator

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Get iPlayer Automator with the following command
   ```
   brew install --cask get-iplayer-automator
   ```
4. Get iPlayer Automator is ready to use now!