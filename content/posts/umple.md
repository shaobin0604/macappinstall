---
title: "Install umple on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Modeling tool/programming language that enables Model-Oriented Programming"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install umple on MacOS using homebrew

- App Name: umple
- App description: Modeling tool/programming language that enables Model-Oriented Programming
- App Version: 1.31.1.5860.78bb27cc6
- App Website: https://www.umple.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install umple with the following command
   ```
   brew install umple
   ```
4. umple is ready to use now!