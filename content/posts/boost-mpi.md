---
title: "Install boost-mpi on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C++ library for C++/MPI interoperability"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install boost-mpi on MacOS using homebrew

- App Name: boost-mpi
- App description: C++ library for C++/MPI interoperability
- App Version: 1.76.0
- App Website: https://www.boost.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install boost-mpi with the following command
   ```
   brew install boost-mpi
   ```
4. boost-mpi is ready to use now!