---
title: "Install betty on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "English-like interface for the command-line"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install betty on MacOS using homebrew

- App Name: betty
- App description: English-like interface for the command-line
- App Version: 0.1.7
- App Website: https://github.com/pickhardt/betty

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install betty with the following command
   ```
   brew install betty
   ```
4. betty is ready to use now!