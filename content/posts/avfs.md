---
title: "Install avfs on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Virtual file system that facilitates looking inside archives"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install avfs on MacOS using homebrew

- App Name: avfs
- App description: Virtual file system that facilitates looking inside archives
- App Version: 1.1.3
- App Website: https://avf.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install avfs with the following command
   ```
   brew install avfs
   ```
4. avfs is ready to use now!