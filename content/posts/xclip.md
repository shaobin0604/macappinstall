---
title: "Install xclip on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Access X11 clipboards from the command-line"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install xclip on MacOS using homebrew

- App Name: xclip
- App description: Access X11 clipboards from the command-line
- App Version: 0.13
- App Website: https://github.com/astrand/xclip

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install xclip with the following command
   ```
   brew install xclip
   ```
4. xclip is ready to use now!