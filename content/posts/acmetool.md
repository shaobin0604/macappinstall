---
title: "Install acmetool on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Automatic certificate acquisition tool for ACME (Let's Encrypt)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install acmetool on MacOS using homebrew

- App Name: acmetool
- App description: Automatic certificate acquisition tool for ACME (Let's Encrypt)
- App Version: 0.0.67
- App Website: https://github.com/hlandau/acmetool

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install acmetool with the following command
   ```
   brew install acmetool
   ```
4. acmetool is ready to use now!