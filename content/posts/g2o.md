---
title: "Install g2o on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "General framework for graph optimization"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install g2o on MacOS using homebrew

- App Name: g2o
- App description: General framework for graph optimization
- App Version: 20201223
- App Website: https://openslam-org.github.io/g2o.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install g2o with the following command
   ```
   brew install g2o
   ```
4. g2o is ready to use now!