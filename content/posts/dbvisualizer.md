---
title: "Install DbVisualizer on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Database management and analysis tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install DbVisualizer on MacOS using homebrew

- App Name: DbVisualizer
- App description: Database management and analysis tool
- App Version: 12.1.8
- App Website: https://www.dbvis.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install DbVisualizer with the following command
   ```
   brew install --cask dbvisualizer
   ```
4. DbVisualizer is ready to use now!