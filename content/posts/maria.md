---
title: "Install Maria on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "App/widget for aria2 download tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Maria on MacOS using homebrew

- App Name: Maria
- App description: App/widget for aria2 download tool
- App Version: 1.2.6,1701213
- App Website: https://github.com/shincurry/Maria

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Maria with the following command
   ```
   brew install --cask maria
   ```
4. Maria is ready to use now!