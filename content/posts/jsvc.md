---
title: "Install jsvc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Wrapper to launch Java applications as daemons"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install jsvc on MacOS using homebrew

- App Name: jsvc
- App description: Wrapper to launch Java applications as daemons
- App Version: 1.2.4
- App Website: https://commons.apache.org/daemon/jsvc.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install jsvc with the following command
   ```
   brew install jsvc
   ```
4. jsvc is ready to use now!