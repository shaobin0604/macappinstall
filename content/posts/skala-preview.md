---
title: "Install Skala Preview on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Skala Preview on MacOS using homebrew

- App Name: Skala Preview
- App description: null
- App Version: 2.0
- App Website: https://bjango.com/mac/skalapreview/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Skala Preview with the following command
   ```
   brew install --cask skala-preview
   ```
4. Skala Preview is ready to use now!