---
title: "Install TechSmith Capture on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Screen capture software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install TechSmith Capture on MacOS using homebrew

- App Name: TechSmith Capture
- App description: Screen capture software
- App Version: 1.3.31,198
- App Website: https://www.techsmith.com/jing-tool.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install TechSmith Capture with the following command
   ```
   brew install --cask techsmith-capture
   ```
4. TechSmith Capture is ready to use now!