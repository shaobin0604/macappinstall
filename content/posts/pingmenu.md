---
title: "Install PingMenu on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utility that shows the current network latency in the menu bar"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install PingMenu on MacOS using homebrew

- App Name: PingMenu
- App description: Utility that shows the current network latency in the menu bar
- App Version: 1.3,2
- App Website: https://github.com/kalleboo/PingMenu

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install PingMenu with the following command
   ```
   brew install --cask pingmenu
   ```
4. PingMenu is ready to use now!