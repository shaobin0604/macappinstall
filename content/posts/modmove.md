---
title: "Install ModMove on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utility to move/resize windows using modifiers and the mouse"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ModMove on MacOS using homebrew

- App Name: ModMove
- App description: Utility to move/resize windows using modifiers and the mouse
- App Version: 1.1.1
- App Website: https://github.com/keith/modmove

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ModMove with the following command
   ```
   brew install --cask modmove
   ```
4. ModMove is ready to use now!