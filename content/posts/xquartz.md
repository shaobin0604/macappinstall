---
title: "Install XQuartz on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Open-source version of the X.Org X Window System"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install XQuartz on MacOS using homebrew

- App Name: XQuartz
- App description: Open-source version of the X.Org X Window System
- App Version: 2.8.1
- App Website: https://www.xquartz.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install XQuartz with the following command
   ```
   brew install --cask xquartz
   ```
4. XQuartz is ready to use now!