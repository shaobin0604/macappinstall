---
title: "Install ndm on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Desktop manager for the Node.js Package Manager (NPM)"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ndm on MacOS using homebrew

- App Name: ndm
- App description: Desktop manager for the Node.js Package Manager (NPM)
- App Version: 1.2.0
- App Website: https://720kb.github.io/ndm/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ndm with the following command
   ```
   brew install --cask ndm
   ```
4. ndm is ready to use now!