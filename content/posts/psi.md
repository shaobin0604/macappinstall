---
title: "Install Psi on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Instant messaging application designed for the XMPP network"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Psi on MacOS using homebrew

- App Name: Psi
- App description: Instant messaging application designed for the XMPP network
- App Version: 1.4
- App Website: https://psi-im.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Psi with the following command
   ```
   brew install --cask psi
   ```
4. Psi is ready to use now!