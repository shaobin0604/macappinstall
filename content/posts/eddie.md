---
title: "Install Air VPN on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "OpenVPN UI"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Air VPN on MacOS using homebrew

- App Name: Air VPN
- App description: OpenVPN UI
- App Version: 2.20.0
- App Website: https://airvpn.org/macos/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Air VPN with the following command
   ```
   brew install --cask eddie
   ```
4. Air VPN is ready to use now!