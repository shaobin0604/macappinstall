---
title: "Install KiCad on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Electronics design automation suite"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install KiCad on MacOS using homebrew

- App Name: KiCad
- App description: Electronics design automation suite
- App Version: 6.0.2-0
- App Website: https://kicad.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install KiCad with the following command
   ```
   brew install --cask kicad
   ```
4. KiCad is ready to use now!