---
title: "Install irrlicht on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Realtime 3D engine"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install irrlicht on MacOS using homebrew

- App Name: irrlicht
- App description: Realtime 3D engine
- App Version: 1.8.5
- App Website: https://irrlicht.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install irrlicht with the following command
   ```
   brew install irrlicht
   ```
4. irrlicht is ready to use now!