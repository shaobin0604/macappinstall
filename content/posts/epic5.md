---
title: "Install epic5 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Enhanced, programmable IRC client"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install epic5 on MacOS using homebrew

- App Name: epic5
- App description: Enhanced, programmable IRC client
- App Version: 2.1.6
- App Website: http://www.epicsol.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install epic5 with the following command
   ```
   brew install epic5
   ```
4. epic5 is ready to use now!