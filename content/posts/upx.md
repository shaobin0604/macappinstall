---
title: "Install upx on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Compress/expand executable files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install upx on MacOS using homebrew

- App Name: upx
- App description: Compress/expand executable files
- App Version: 3.96
- App Website: https://upx.github.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install upx with the following command
   ```
   brew install upx
   ```
4. upx is ready to use now!