---
title: "Install ansilove on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "ANSI/ASCII art to PNG converter"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ansilove on MacOS using homebrew

- App Name: ansilove
- App description: ANSI/ASCII art to PNG converter
- App Version: 4.1.5
- App Website: https://www.ansilove.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ansilove with the following command
   ```
   brew install ansilove
   ```
4. ansilove is ready to use now!