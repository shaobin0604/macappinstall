---
title: "Install cakebrewjs on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Homebrew GUI app written in electron"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cakebrewjs on MacOS using homebrew

- App Name: cakebrewjs
- App description: Homebrew GUI app written in electron
- App Version: 1.0.1
- App Website: https://sourceforge.net/projects/cakebrewjs

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cakebrewjs with the following command
   ```
   brew install --cask cakebrewjs
   ```
4. cakebrewjs is ready to use now!