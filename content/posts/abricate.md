---
title: "Install abricate on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Find antimicrobial resistance and virulence genes in contigs"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install abricate on MacOS using homebrew

- App Name: abricate
- App description: Find antimicrobial resistance and virulence genes in contigs
- App Version: 1.0.1
- App Website: https://github.com/tseemann/abricate

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install abricate with the following command
   ```
   brew install abricate
   ```
4. abricate is ready to use now!