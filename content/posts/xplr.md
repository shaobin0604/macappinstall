---
title: "Install xplr on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Hackable, minimal, fast TUI file explorer"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install xplr on MacOS using homebrew

- App Name: xplr
- App description: Hackable, minimal, fast TUI file explorer
- App Version: 0.17.2
- App Website: https://github.com/sayanarijit/xplr

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install xplr with the following command
   ```
   brew install xplr
   ```
4. xplr is ready to use now!