---
title: "Install dust on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "More intuitive version of du in rust"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dust on MacOS using homebrew

- App Name: dust
- App description: More intuitive version of du in rust
- App Version: 0.7.5
- App Website: https://github.com/bootandy/dust

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dust with the following command
   ```
   brew install dust
   ```
4. dust is ready to use now!