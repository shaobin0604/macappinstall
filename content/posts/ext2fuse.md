---
title: "Install ext2fuse on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Compact implementation of ext2 file system using FUSE"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ext2fuse on MacOS using homebrew

- App Name: ext2fuse
- App description: Compact implementation of ext2 file system using FUSE
- App Version: 0.8.1
- App Website: https://sourceforge.net/projects/ext2fuse

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ext2fuse with the following command
   ```
   brew install ext2fuse
   ```
4. ext2fuse is ready to use now!