---
title: "Install bwa on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Burrow-Wheeler Aligner for pairwise alignment of DNA"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bwa on MacOS using homebrew

- App Name: bwa
- App description: Burrow-Wheeler Aligner for pairwise alignment of DNA
- App Version: 0.7.17
- App Website: https://github.com/lh3/bwa

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bwa with the following command
   ```
   brew install bwa
   ```
4. bwa is ready to use now!