---
title: "Install SunloginControl on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Target component of remote desktop control and monitoring tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install SunloginControl on MacOS using homebrew

- App Name: SunloginControl
- App description: Target component of remote desktop control and monitoring tool
- App Version: 5.3.1.40594
- App Website: https://sunlogin.oray.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install SunloginControl with the following command
   ```
   brew install --cask sunlogincontrol
   ```
4. SunloginControl is ready to use now!