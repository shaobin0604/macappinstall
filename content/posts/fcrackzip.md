---
title: "Install fcrackzip on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Zip password cracker"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fcrackzip on MacOS using homebrew

- App Name: fcrackzip
- App description: Zip password cracker
- App Version: 1.0
- App Website: http://oldhome.schmorp.de/marc/fcrackzip.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fcrackzip with the following command
   ```
   brew install fcrackzip
   ```
4. fcrackzip is ready to use now!