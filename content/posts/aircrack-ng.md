---
title: "Install aircrack-ng on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Next-generation aircrack with lots of new features"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install aircrack-ng on MacOS using homebrew

- App Name: aircrack-ng
- App description: Next-generation aircrack with lots of new features
- App Version: 1.6
- App Website: https://aircrack-ng.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install aircrack-ng with the following command
   ```
   brew install aircrack-ng
   ```
4. aircrack-ng is ready to use now!