---
title: "Install dante on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "SOCKS server and client, implementing RFC 1928 and related standards"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dante on MacOS using homebrew

- App Name: dante
- App description: SOCKS server and client, implementing RFC 1928 and related standards
- App Version: 1.4.3
- App Website: https://www.inet.no/dante/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dante with the following command
   ```
   brew install dante
   ```
4. dante is ready to use now!