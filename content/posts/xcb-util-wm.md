---
title: "Install xcb-util-wm on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Client and window-manager helpers for EWMH and ICCCM"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install xcb-util-wm on MacOS using homebrew

- App Name: xcb-util-wm
- App description: Client and window-manager helpers for EWMH and ICCCM
- App Version: 0.4.1
- App Website: https://xcb.freedesktop.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install xcb-util-wm with the following command
   ```
   brew install xcb-util-wm
   ```
4. xcb-util-wm is ready to use now!