---
title: "Install Polar Bookshelf on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Integrated reading environment"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Polar Bookshelf on MacOS using homebrew

- App Name: Polar Bookshelf
- App description: Integrated reading environment
- App Version: 2.0.103
- App Website: https://getpolarized.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Polar Bookshelf with the following command
   ```
   brew install --cask polar-bookshelf
   ```
4. Polar Bookshelf is ready to use now!