---
title: "Install Timestamp on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Improved clock for the menu bar"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Timestamp on MacOS using homebrew

- App Name: Timestamp
- App description: Improved clock for the menu bar
- App Version: 1.0.1
- App Website: https://mzdr.github.io/timestamp/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Timestamp with the following command
   ```
   brew install --cask timestamp
   ```
4. Timestamp is ready to use now!