---
title: "Install sf-pwgen on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Generate passwords using SecurityFoundation framework"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sf-pwgen on MacOS using homebrew

- App Name: sf-pwgen
- App description: Generate passwords using SecurityFoundation framework
- App Version: 1.5
- App Website: https://github.com/anders/pwgen/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sf-pwgen with the following command
   ```
   brew install sf-pwgen
   ```
4. sf-pwgen is ready to use now!