---
title: "Install Kid3 on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Audio tagger focusing on efficiency"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Kid3 on MacOS using homebrew

- App Name: Kid3
- App description: Audio tagger focusing on efficiency
- App Version: 3.9.1
- App Website: https://kid3.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Kid3 with the following command
   ```
   brew install --cask kid3
   ```
4. Kid3 is ready to use now!