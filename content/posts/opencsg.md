---
title: "Install opencsg on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Constructive solid geometry rendering library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install opencsg on MacOS using homebrew

- App Name: opencsg
- App description: Constructive solid geometry rendering library
- App Version: 1.4.2
- App Website: http://www.opencsg.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install opencsg with the following command
   ```
   brew install opencsg
   ```
4. opencsg is ready to use now!