---
title: "Install Jumpcut on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Clipboard manager"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Jumpcut on MacOS using homebrew

- App Name: Jumpcut
- App description: Clipboard manager
- App Version: 0.75
- App Website: https://snark.github.io/jumpcut/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Jumpcut with the following command
   ```
   brew install --cask jumpcut
   ```
4. Jumpcut is ready to use now!