---
title: "Install golang-migrate on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Database migrations CLI tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install golang-migrate on MacOS using homebrew

- App Name: golang-migrate
- App description: Database migrations CLI tool
- App Version: 4.15.1
- App Website: https://github.com/golang-migrate/migrate

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install golang-migrate with the following command
   ```
   brew install golang-migrate
   ```
4. golang-migrate is ready to use now!