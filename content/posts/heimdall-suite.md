---
title: "Install Heimdall Suite on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Flash firmware onto Samsung mobile devices"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Heimdall Suite on MacOS using homebrew

- App Name: Heimdall Suite
- App description: Flash firmware onto Samsung mobile devices
- App Version: 1.4.0
- App Website: https://glassechidna.com.au/heimdall/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Heimdall Suite with the following command
   ```
   brew install --cask heimdall-suite
   ```
4. Heimdall Suite is ready to use now!