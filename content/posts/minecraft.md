---
title: "Install Minecraft on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Sandbox construction video game"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Minecraft on MacOS using homebrew

- App Name: Minecraft
- App description: Sandbox construction video game
- App Version: 1097,1
- App Website: https://minecraft.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Minecraft with the following command
   ```
   brew install --cask minecraft
   ```
4. Minecraft is ready to use now!