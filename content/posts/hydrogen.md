---
title: "Install Hydrogen on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Drum machine and sequencer"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Hydrogen on MacOS using homebrew

- App Name: Hydrogen
- App description: Drum machine and sequencer
- App Version: 1.1.1
- App Website: http://www.hydrogen-music.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Hydrogen with the following command
   ```
   brew install --cask hydrogen
   ```
4. Hydrogen is ready to use now!