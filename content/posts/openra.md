---
title: "Install OpenRA on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Real-time strategy game engine for Westwood games"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install OpenRA on MacOS using homebrew

- App Name: OpenRA
- App description: Real-time strategy game engine for Westwood games
- App Version: 20210321
- App Website: https://www.openra.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install OpenRA with the following command
   ```
   brew install --cask openra
   ```
4. OpenRA is ready to use now!