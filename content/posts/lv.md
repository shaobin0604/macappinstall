---
title: "Install lv on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Powerful multi-lingual file viewer/grep"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install lv on MacOS using homebrew

- App Name: lv
- App description: Powerful multi-lingual file viewer/grep
- App Version: 4.51
- App Website: https://web.archive.org/web/20160310122517/www.ff.iij4u.or.jp/~nrt/lv/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install lv with the following command
   ```
   brew install lv
   ```
4. lv is ready to use now!