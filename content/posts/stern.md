---
title: "Install stern on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tail multiple Kubernetes pods & their containers"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install stern on MacOS using homebrew

- App Name: stern
- App description: Tail multiple Kubernetes pods & their containers
- App Version: 1.21.0
- App Website: https://github.com/stern/stern

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install stern with the following command
   ```
   brew install stern
   ```
4. stern is ready to use now!