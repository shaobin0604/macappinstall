---
title: "Install xlslib on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C++/C library to construct Excel .xls files in code"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install xlslib on MacOS using homebrew

- App Name: xlslib
- App description: C++/C library to construct Excel .xls files in code
- App Version: 2.5.0
- App Website: https://sourceforge.net/projects/xlslib

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install xlslib with the following command
   ```
   brew install xlslib
   ```
4. xlslib is ready to use now!