---
title: "Install numdiff on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Putative files comparison tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install numdiff on MacOS using homebrew

- App Name: numdiff
- App description: Putative files comparison tool
- App Version: 5.9.0
- App Website: https://www.nongnu.org/numdiff

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install numdiff with the following command
   ```
   brew install numdiff
   ```
4. numdiff is ready to use now!