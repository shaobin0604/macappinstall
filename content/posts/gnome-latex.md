---
title: "Install gnome-latex on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "LaTeX editor for the GNOME desktop"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gnome-latex on MacOS using homebrew

- App Name: gnome-latex
- App description: LaTeX editor for the GNOME desktop
- App Version: 3.38.0
- App Website: https://wiki.gnome.org/Apps/GNOME-LaTeX

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gnome-latex with the following command
   ```
   brew install gnome-latex
   ```
4. gnome-latex is ready to use now!