---
title: "Install tdlib on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cross-platform library for building Telegram clients"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tdlib on MacOS using homebrew

- App Name: tdlib
- App description: Cross-platform library for building Telegram clients
- App Version: 1.8.0
- App Website: https://core.telegram.org/tdlib

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tdlib with the following command
   ```
   brew install tdlib
   ```
4. tdlib is ready to use now!