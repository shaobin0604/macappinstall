---
title: "Install solarus on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Action-RPG game engine"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install solarus on MacOS using homebrew

- App Name: solarus
- App description: Action-RPG game engine
- App Version: 1.6.5
- App Website: https://www.solarus-games.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install solarus with the following command
   ```
   brew install solarus
   ```
4. solarus is ready to use now!