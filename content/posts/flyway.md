---
title: "Install flyway on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Database version control to control migrations"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install flyway on MacOS using homebrew

- App Name: flyway
- App description: Database version control to control migrations
- App Version: 8.5.0
- App Website: https://flywaydb.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install flyway with the following command
   ```
   brew install flyway
   ```
4. flyway is ready to use now!