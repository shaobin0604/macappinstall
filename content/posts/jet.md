---
title: "Install Codeship Jet on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CI/CD as a service"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Codeship Jet on MacOS using homebrew

- App Name: Codeship Jet
- App description: CI/CD as a service
- App Version: 2.11.0
- App Website: https://docs.cloudbees.com/docs/cloudbees-codeship/latest/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Codeship Jet with the following command
   ```
   brew install --cask jet
   ```
4. Codeship Jet is ready to use now!