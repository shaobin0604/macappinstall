---
title: "Install archi-steam-farm on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Application for idling Steam cards from multiple accounts simultaneously"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install archi-steam-farm on MacOS using homebrew

- App Name: archi-steam-farm
- App description: Application for idling Steam cards from multiple accounts simultaneously
- App Version: 5.2.2.5
- App Website: https://github.com/JustArchiNET/ArchiSteamFarm

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install archi-steam-farm with the following command
   ```
   brew install archi-steam-farm
   ```
4. archi-steam-farm is ready to use now!