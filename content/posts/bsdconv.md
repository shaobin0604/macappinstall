---
title: "Install bsdconv on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Charset/encoding converter library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bsdconv on MacOS using homebrew

- App Name: bsdconv
- App description: Charset/encoding converter library
- App Version: 11.6
- App Website: https://github.com/buganini/bsdconv

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bsdconv with the following command
   ```
   brew install bsdconv
   ```
4. bsdconv is ready to use now!