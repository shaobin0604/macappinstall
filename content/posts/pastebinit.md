---
title: "Install pastebinit on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Send things to pastebin from the command-line"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pastebinit on MacOS using homebrew

- App Name: pastebinit
- App description: Send things to pastebin from the command-line
- App Version: 1.5
- App Website: https://launchpad.net/pastebinit

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pastebinit with the following command
   ```
   brew install pastebinit
   ```
4. pastebinit is ready to use now!