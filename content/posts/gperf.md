---
title: "Install gperf on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Perfect hash function generator"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gperf on MacOS using homebrew

- App Name: gperf
- App description: Perfect hash function generator
- App Version: 3.1
- App Website: https://www.gnu.org/software/gperf

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gperf with the following command
   ```
   brew install gperf
   ```
4. gperf is ready to use now!