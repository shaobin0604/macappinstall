---
title: "Install Abstract on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Collaborative design tool with support for Sketch files"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Abstract on MacOS using homebrew

- App Name: Abstract
- App description: Collaborative design tool with support for Sketch files
- App Version: 98.1.3
- App Website: https://www.goabstract.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Abstract with the following command
   ```
   brew install --cask abstract
   ```
4. Abstract is ready to use now!