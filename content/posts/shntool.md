---
title: "Install shntool on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Multi-purpose tool for manipulating and analyzing WAV files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install shntool on MacOS using homebrew

- App Name: shntool
- App description: Multi-purpose tool for manipulating and analyzing WAV files
- App Version: 3.0.10
- App Website: http://shnutils.freeshell.org/shntool/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install shntool with the following command
   ```
   brew install shntool
   ```
4. shntool is ready to use now!