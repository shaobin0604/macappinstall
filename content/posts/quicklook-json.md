---
title: "Install quick look JSON on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install quick look JSON on MacOS using homebrew

- App Name: quick look JSON
- App description: null
- App Version: 2,1.0
- App Website: http://www.sagtau.com/quicklookjson.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install quick look JSON with the following command
   ```
   brew install --cask quicklook-json
   ```
4. quick look JSON is ready to use now!