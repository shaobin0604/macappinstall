---
title: "Install Light Table on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "IDE"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Light Table on MacOS using homebrew

- App Name: Light Table
- App description: IDE
- App Version: 0.8.1
- App Website: http://lighttable.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Light Table with the following command
   ```
   brew install --cask lighttable
   ```
4. Light Table is ready to use now!