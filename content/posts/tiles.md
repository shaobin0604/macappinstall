---
title: "Install Sempliva Tiles on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Window manager"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Sempliva Tiles on MacOS using homebrew

- App Name: Sempliva Tiles
- App description: Window manager
- App Version: 1.2.3,9890ca0f
- App Website: https://www.sempliva.com/tiles/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Sempliva Tiles with the following command
   ```
   brew install --cask tiles
   ```
4. Sempliva Tiles is ready to use now!