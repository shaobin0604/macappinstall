---
title: "Install redstore on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Lightweight RDF triplestore powered by Redland"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install redstore on MacOS using homebrew

- App Name: redstore
- App description: Lightweight RDF triplestore powered by Redland
- App Version: 0.5.4
- App Website: https://www.aelius.com/njh/redstore/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install redstore with the following command
   ```
   brew install redstore
   ```
4. redstore is ready to use now!