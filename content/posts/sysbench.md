---
title: "Install sysbench on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "System performance benchmark tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sysbench on MacOS using homebrew

- App Name: sysbench
- App description: System performance benchmark tool
- App Version: 1.0.20
- App Website: https://github.com/akopytov/sysbench

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sysbench with the following command
   ```
   brew install sysbench
   ```
4. sysbench is ready to use now!