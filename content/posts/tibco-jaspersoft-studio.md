---
title: "Install TIBCO Jaspersoft Studio on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Eclipse-based report development tool for JasperReports"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install TIBCO Jaspersoft Studio on MacOS using homebrew

- App Name: TIBCO Jaspersoft Studio
- App description: Eclipse-based report development tool for JasperReports
- App Version: 6.18.1
- App Website: https://community.jaspersoft.com/project/jaspersoft-studio

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install TIBCO Jaspersoft Studio with the following command
   ```
   brew install --cask tibco-jaspersoft-studio
   ```
4. TIBCO Jaspersoft Studio is ready to use now!