---
title: "Install libnxml on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C library for parsing, writing, and creating XML files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libnxml on MacOS using homebrew

- App Name: libnxml
- App description: C library for parsing, writing, and creating XML files
- App Version: 0.18.3
- App Website: https://github.com/bakulf/libnxml

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libnxml with the following command
   ```
   brew install libnxml
   ```
4. libnxml is ready to use now!