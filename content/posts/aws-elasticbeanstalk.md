---
title: "Install aws-elasticbeanstalk on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Client for Amazon Elastic Beanstalk web service"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install aws-elasticbeanstalk on MacOS using homebrew

- App Name: aws-elasticbeanstalk
- App description: Client for Amazon Elastic Beanstalk web service
- App Version: 3.20.3
- App Website: https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/eb-cli3.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install aws-elasticbeanstalk with the following command
   ```
   brew install aws-elasticbeanstalk
   ```
4. aws-elasticbeanstalk is ready to use now!