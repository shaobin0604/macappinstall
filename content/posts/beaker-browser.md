---
title: "Install Beaker Browser on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Experimental peer-to-peer web browser"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Beaker Browser on MacOS using homebrew

- App Name: Beaker Browser
- App description: Experimental peer-to-peer web browser
- App Version: 1.1.0
- App Website: https://beakerbrowser.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Beaker Browser with the following command
   ```
   brew install --cask beaker-browser
   ```
4. Beaker Browser is ready to use now!