---
title: "Install OpenLP on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Worship presentation software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install OpenLP on MacOS using homebrew

- App Name: OpenLP
- App description: Worship presentation software
- App Version: 2.9.4
- App Website: https://openlp.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install OpenLP with the following command
   ```
   brew install --cask openlp
   ```
4. OpenLP is ready to use now!