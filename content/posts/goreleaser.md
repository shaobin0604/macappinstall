---
title: "Install goreleaser on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Deliver Go binaries as fast and easily as possible"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install goreleaser on MacOS using homebrew

- App Name: goreleaser
- App description: Deliver Go binaries as fast and easily as possible
- App Version: 1.5.0
- App Website: https://goreleaser.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install goreleaser with the following command
   ```
   brew install goreleaser
   ```
4. goreleaser is ready to use now!