---
title: "Install gtk-gnutella on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Share files in a peer-to-peer (P2P) network"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gtk-gnutella on MacOS using homebrew

- App Name: gtk-gnutella
- App description: Share files in a peer-to-peer (P2P) network
- App Version: 1.2.1
- App Website: https://gtk-gnutella.sourceforge.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gtk-gnutella with the following command
   ```
   brew install gtk-gnutella
   ```
4. gtk-gnutella is ready to use now!