---
title: "Install gawk on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GNU awk utility"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gawk on MacOS using homebrew

- App Name: gawk
- App description: GNU awk utility
- App Version: 5.1.1
- App Website: https://www.gnu.org/software/gawk/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gawk with the following command
   ```
   brew install gawk
   ```
4. gawk is ready to use now!