---
title: "Install docker-swarm on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Turn a pool of Docker hosts into a single, virtual host"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install docker-swarm on MacOS using homebrew

- App Name: docker-swarm
- App description: Turn a pool of Docker hosts into a single, virtual host
- App Version: 1.2.9
- App Website: https://github.com/docker/classicswarm

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install docker-swarm with the following command
   ```
   brew install docker-swarm
   ```
4. docker-swarm is ready to use now!