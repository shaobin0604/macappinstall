---
title: "Install libproxy on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library that provides automatic proxy configuration management"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libproxy on MacOS using homebrew

- App Name: libproxy
- App description: Library that provides automatic proxy configuration management
- App Version: 0.4.17
- App Website: https://libproxy.github.io/libproxy/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libproxy with the following command
   ```
   brew install libproxy
   ```
4. libproxy is ready to use now!