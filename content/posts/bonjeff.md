---
title: "Install Bonjeff on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Shows a live display of the Bonjour services published on your network"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Bonjeff on MacOS using homebrew

- App Name: Bonjeff
- App description: Shows a live display of the Bonjour services published on your network
- App Version: 2.0.0
- App Website: https://github.com/lapcat/Bonjeff

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Bonjeff with the following command
   ```
   brew install --cask bonjeff
   ```
4. Bonjeff is ready to use now!