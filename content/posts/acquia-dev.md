---
title: "Install Acquia Dev Desktop on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Install, test, and build Drupal sites locally"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Acquia Dev Desktop on MacOS using homebrew

- App Name: Acquia Dev Desktop
- App description: Install, test, and build Drupal sites locally
- App Version: 2.2021.01.14
- App Website: https://www.acquia.com/drupal/acquia-dev-desktop

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Acquia Dev Desktop with the following command
   ```
   brew install --cask acquia-dev
   ```
4. Acquia Dev Desktop is ready to use now!