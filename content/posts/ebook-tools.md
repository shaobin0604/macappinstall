---
title: "Install ebook-tools on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Access and convert several ebook formats"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ebook-tools on MacOS using homebrew

- App Name: ebook-tools
- App description: Access and convert several ebook formats
- App Version: 0.2.2
- App Website: https://sourceforge.net/projects/ebook-tools/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ebook-tools with the following command
   ```
   brew install ebook-tools
   ```
4. ebook-tools is ready to use now!