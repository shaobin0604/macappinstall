---
title: "Install spires on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Frontend for inspire-hep and arxiv"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install spires on MacOS using homebrew

- App Name: spires
- App description: Frontend for inspire-hep and arxiv
- App Version: 2.0.7
- App Website: https://member.ipmu.jp/yuji.tachikawa/spires/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install spires with the following command
   ```
   brew install --cask spires
   ```
4. spires is ready to use now!