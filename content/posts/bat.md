---
title: "Install bat on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Clone of cat(1) with syntax highlighting and Git integration"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bat on MacOS using homebrew

- App Name: bat
- App description: Clone of cat(1) with syntax highlighting and Git integration
- App Version: 0.19.0
- App Website: https://github.com/sharkdp/bat

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bat with the following command
   ```
   brew install bat
   ```
4. bat is ready to use now!