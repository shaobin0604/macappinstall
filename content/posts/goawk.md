---
title: "Install goawk on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "POSIX-compliant AWK interpreter written in Go"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install goawk on MacOS using homebrew

- App Name: goawk
- App description: POSIX-compliant AWK interpreter written in Go
- App Version: 1.15.0
- App Website: https://benhoyt.com/writings/goawk/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install goawk with the following command
   ```
   brew install goawk
   ```
4. goawk is ready to use now!