---
title: "Install DCommander on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Two-pane file manager"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install DCommander on MacOS using homebrew

- App Name: DCommander
- App description: Two-pane file manager
- App Version: 3.9.1.3
- App Website: https://devstorm-apps.com/dc/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install DCommander with the following command
   ```
   brew install --cask dcommander
   ```
4. DCommander is ready to use now!