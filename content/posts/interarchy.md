---
title: "Install Interarchy on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "File fransfer tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Interarchy on MacOS using homebrew

- App Name: Interarchy
- App description: File fransfer tool
- App Version: 10.0.7
- App Website: https://www.kangacode.com/interarchy/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Interarchy with the following command
   ```
   brew install --cask interarchy
   ```
4. Interarchy is ready to use now!