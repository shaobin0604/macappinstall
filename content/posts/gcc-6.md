---
title: "Install gcc@6 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GNU compiler collection"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gcc@6 on MacOS using homebrew

- App Name: gcc@6
- App description: GNU compiler collection
- App Version: 6.5.0
- App Website: https://gcc.gnu.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gcc@6 with the following command
   ```
   brew install gcc@6
   ```
4. gcc@6 is ready to use now!