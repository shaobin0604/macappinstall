---
title: "Install NICE DCV Viewer on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Client for NICE DCV remote display protocol"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install NICE DCV Viewer on MacOS using homebrew

- App Name: NICE DCV Viewer
- App description: Client for NICE DCV remote display protocol
- App Version: 2021.3.3829
- App Website: https://www.nice-dcv.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install NICE DCV Viewer with the following command
   ```
   brew install --cask dcv-viewer
   ```
4. NICE DCV Viewer is ready to use now!