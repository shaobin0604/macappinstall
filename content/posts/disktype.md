---
title: "Install disktype on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Detect content format of a disk or disk image"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install disktype on MacOS using homebrew

- App Name: disktype
- App description: Detect content format of a disk or disk image
- App Version: 9
- App Website: https://disktype.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install disktype with the following command
   ```
   brew install disktype
   ```
4. disktype is ready to use now!