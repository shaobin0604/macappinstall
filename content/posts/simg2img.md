---
title: "Install simg2img on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to convert Android sparse images to raw images and back"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install simg2img on MacOS using homebrew

- App Name: simg2img
- App description: Tool to convert Android sparse images to raw images and back
- App Version: 1.1.4
- App Website: https://github.com/anestisb/android-simg2img

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install simg2img with the following command
   ```
   brew install simg2img
   ```
4. simg2img is ready to use now!