---
title: "Install libmetalink on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C library to parse Metalink XML files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libmetalink on MacOS using homebrew

- App Name: libmetalink
- App description: C library to parse Metalink XML files
- App Version: 0.1.3
- App Website: https://launchpad.net/libmetalink/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libmetalink with the following command
   ```
   brew install libmetalink
   ```
4. libmetalink is ready to use now!