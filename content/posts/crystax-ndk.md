---
title: "Install Crystax NDK on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Drop-in replacement for Google's Android NDK"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Crystax NDK on MacOS using homebrew

- App Name: Crystax NDK
- App description: Drop-in replacement for Google's Android NDK
- App Version: 10.3.2
- App Website: https://www.crystax.net/android/ndk

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Crystax NDK with the following command
   ```
   brew install --cask crystax-ndk
   ```
4. Crystax NDK is ready to use now!