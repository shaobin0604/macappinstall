---
title: "Install Yippy on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Open source clipboard manager"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Yippy on MacOS using homebrew

- App Name: Yippy
- App description: Open source clipboard manager
- App Version: 2.8.1
- App Website: https://yippy.mattdavo.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Yippy with the following command
   ```
   brew install --cask yippy
   ```
4. Yippy is ready to use now!