---
title: "Install git-series on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Track changes to a patch series over time"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install git-series on MacOS using homebrew

- App Name: git-series
- App description: Track changes to a patch series over time
- App Version: 0.9.1
- App Website: https://github.com/git-series/git-series

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install git-series with the following command
   ```
   brew install git-series
   ```
4. git-series is ready to use now!