---
title: "Install ploticus on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Scriptable plotting and graphing utility"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ploticus on MacOS using homebrew

- App Name: ploticus
- App description: Scriptable plotting and graphing utility
- App Version: 2.42
- App Website: https://ploticus.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ploticus with the following command
   ```
   brew install ploticus
   ```
4. ploticus is ready to use now!