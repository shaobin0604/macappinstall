---
title: "Install pktriot on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Host server applications and static websites"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pktriot on MacOS using homebrew

- App Name: pktriot
- App description: Host server applications and static websites
- App Version: 0.13.1
- App Website: https://packetriot.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pktriot with the following command
   ```
   brew install --cask pktriot
   ```
4. pktriot is ready to use now!