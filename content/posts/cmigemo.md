---
title: "Install cmigemo on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Migemo is a tool that supports Japanese incremental search with Romaji"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cmigemo on MacOS using homebrew

- App Name: cmigemo
- App description: Migemo is a tool that supports Japanese incremental search with Romaji
- App Version: 20110227
- App Website: https://www.kaoriya.net/software/cmigemo

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cmigemo with the following command
   ```
   brew install cmigemo
   ```
4. cmigemo is ready to use now!