---
title: "Install jhead on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Extract Digicam setting info from EXIF JPEG headers"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install jhead on MacOS using homebrew

- App Name: jhead
- App description: Extract Digicam setting info from EXIF JPEG headers
- App Version: 3.06.0.1
- App Website: https://github.com/Matthias-Wandel/jhead

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install jhead with the following command
   ```
   brew install jhead
   ```
4. jhead is ready to use now!