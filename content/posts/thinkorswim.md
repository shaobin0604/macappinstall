---
title: "Install thinkDesktop on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install thinkDesktop on MacOS using homebrew

- App Name: thinkDesktop
- App description: null
- App Version: latest
- App Website: https://mediaserver.thinkorswim.com/installer/install.html#macosx

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install thinkDesktop with the following command
   ```
   brew install --cask thinkorswim
   ```
4. thinkDesktop is ready to use now!