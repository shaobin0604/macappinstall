---
title: "Install simple-scan on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GNOME document scanning application"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install simple-scan on MacOS using homebrew

- App Name: simple-scan
- App description: GNOME document scanning application
- App Version: 40.7
- App Website: https://gitlab.gnome.org/GNOME/simple-scan

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install simple-scan with the following command
   ```
   brew install simple-scan
   ```
4. simple-scan is ready to use now!