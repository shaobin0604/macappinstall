---
title: "Install cpansearch on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CPAN module search written in C"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cpansearch on MacOS using homebrew

- App Name: cpansearch
- App description: CPAN module search written in C
- App Version: 0.2
- App Website: https://github.com/c9s/cpansearch

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cpansearch with the following command
   ```
   brew install cpansearch
   ```
4. cpansearch is ready to use now!