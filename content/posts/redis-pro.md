---
title: "Install redis-pro on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Redis desktop"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install redis-pro on MacOS using homebrew

- App Name: redis-pro
- App description: Redis desktop
- App Version: 1.4.3
- App Website: https://github.com/cmushroom/redis-pro

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install redis-pro with the following command
   ```
   brew install --cask redis-pro
   ```
4. redis-pro is ready to use now!