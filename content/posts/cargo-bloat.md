---
title: "Install cargo-bloat on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Find out what takes most of the space in your executable"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cargo-bloat on MacOS using homebrew

- App Name: cargo-bloat
- App description: Find out what takes most of the space in your executable
- App Version: 0.11.0
- App Website: https://github.com/RazrFalcon/cargo-bloat

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cargo-bloat with the following command
   ```
   brew install cargo-bloat
   ```
4. cargo-bloat is ready to use now!