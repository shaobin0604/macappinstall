---
title: "Install fontforge on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line outline and bitmap font editor/converter"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fontforge on MacOS using homebrew

- App Name: fontforge
- App description: Command-line outline and bitmap font editor/converter
- App Version: 20201107
- App Website: https://fontforge.github.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fontforge with the following command
   ```
   brew install fontforge
   ```
4. fontforge is ready to use now!