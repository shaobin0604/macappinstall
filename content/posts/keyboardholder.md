---
title: "Install KeyboardHolder on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Switch input method per application"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install KeyboardHolder on MacOS using homebrew

- App Name: KeyboardHolder
- App description: Switch input method per application
- App Version: 1.5.2
- App Website: https://github.com/leaves615/KeyboardHolder

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install KeyboardHolder with the following command
   ```
   brew install --cask keyboardholder
   ```
4. KeyboardHolder is ready to use now!