---
title: "Install mlkit on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Compiler for the Standard ML programming language"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mlkit on MacOS using homebrew

- App Name: mlkit
- App description: Compiler for the Standard ML programming language
- App Version: 4.6.1
- App Website: https://melsman.github.io/mlkit

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mlkit with the following command
   ```
   brew install mlkit
   ```
4. mlkit is ready to use now!