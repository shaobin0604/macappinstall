---
title: "Install Scroll on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Configure scrolling on Trackpad and Magic Mouse"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Scroll on MacOS using homebrew

- App Name: Scroll
- App description: Configure scrolling on Trackpad and Magic Mouse
- App Version: 2.3,23
- App Website: https://ryanhanson.dev/scroll

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Scroll with the following command
   ```
   brew install --cask scroll
   ```
4. Scroll is ready to use now!