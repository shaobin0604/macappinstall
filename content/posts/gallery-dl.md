---
title: "Install gallery-dl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line downloader for image-hosting site galleries and collections"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gallery-dl on MacOS using homebrew

- App Name: gallery-dl
- App description: Command-line downloader for image-hosting site galleries and collections
- App Version: 1.20.5
- App Website: https://github.com/mikf/gallery-dl

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gallery-dl with the following command
   ```
   brew install gallery-dl
   ```
4. gallery-dl is ready to use now!