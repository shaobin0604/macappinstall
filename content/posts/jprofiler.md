---
title: "Install JProfiler on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Java profiler"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install JProfiler on MacOS using homebrew

- App Name: JProfiler
- App description: Java profiler
- App Version: 13.0
- App Website: https://www.ej-technologies.com/products/jprofiler/overview.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install JProfiler with the following command
   ```
   brew install --cask jprofiler
   ```
4. JProfiler is ready to use now!