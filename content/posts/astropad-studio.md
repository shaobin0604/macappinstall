---
title: "Install Astropad Studio on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Turn your iPad into a professional drawing tablet"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Astropad Studio on MacOS using homebrew

- App Name: Astropad Studio
- App description: Turn your iPad into a professional drawing tablet
- App Version: 3.7.0,3219
- App Website: https://astropad.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Astropad Studio with the following command
   ```
   brew install --cask astropad-studio
   ```
4. Astropad Studio is ready to use now!