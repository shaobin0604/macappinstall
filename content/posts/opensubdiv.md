---
title: "Install opensubdiv on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Open-source subdivision surface library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install opensubdiv on MacOS using homebrew

- App Name: opensubdiv
- App description: Open-source subdivision surface library
- App Version: 3.4.4
- App Website: https://graphics.pixar.com/opensubdiv/docs/intro.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install opensubdiv with the following command
   ```
   brew install opensubdiv
   ```
4. opensubdiv is ready to use now!