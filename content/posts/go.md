---
title: "Install go on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Open source programming language to build simple/reliable/efficient software"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install go on MacOS using homebrew

- App Name: go
- App description: Open source programming language to build simple/reliable/efficient software
- App Version: 1.17.6
- App Website: https://go.dev/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install go with the following command
   ```
   brew install go
   ```
4. go is ready to use now!