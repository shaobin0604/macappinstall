---
title: "Install libtecla on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line editing facilities similar to the tcsh shell"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libtecla on MacOS using homebrew

- App Name: libtecla
- App description: Command-line editing facilities similar to the tcsh shell
- App Version: 1.6.3
- App Website: https://sites.astro.caltech.edu/~mcs/tecla/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libtecla with the following command
   ```
   brew install libtecla
   ```
4. libtecla is ready to use now!