---
title: "Install nqp on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Lightweight Perl 6-like environment for virtual machines"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install nqp on MacOS using homebrew

- App Name: nqp
- App description: Lightweight Perl 6-like environment for virtual machines
- App Version: 2021.12
- App Website: https://github.com/Raku/nqp

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install nqp with the following command
   ```
   brew install nqp
   ```
4. nqp is ready to use now!