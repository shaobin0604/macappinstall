---
title: "Install putty on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Implementation of Telnet and SSH"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install putty on MacOS using homebrew

- App Name: putty
- App description: Implementation of Telnet and SSH
- App Version: 0.76
- App Website: https://www.chiark.greenend.org.uk/~sgtatham/putty/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install putty with the following command
   ```
   brew install putty
   ```
4. putty is ready to use now!