---
title: "Install Mindmanager on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Mind Mapping Tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Mindmanager on MacOS using homebrew

- App Name: Mindmanager
- App description: Mind Mapping Tool
- App Version: 13.2.204
- App Website: https://www.mindjet.com/mindmanager/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Mindmanager with the following command
   ```
   brew install --cask mindjet-mindmanager
   ```
4. Mindmanager is ready to use now!