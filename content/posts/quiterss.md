---
title: "Install QuiteRSS on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Free news feeds reader"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install QuiteRSS on MacOS using homebrew

- App Name: QuiteRSS
- App description: Free news feeds reader
- App Version: 0.19.4
- App Website: https://quiterss.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install QuiteRSS with the following command
   ```
   brew install --cask quiterss
   ```
4. QuiteRSS is ready to use now!