---
title: "Install proj@7 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cartographic Projections Library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install proj@7 on MacOS using homebrew

- App Name: proj@7
- App description: Cartographic Projections Library
- App Version: 7.2.1
- App Website: https://proj.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install proj@7 with the following command
   ```
   brew install proj@7
   ```
4. proj@7 is ready to use now!