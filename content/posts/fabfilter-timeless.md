---
title: "Install FabFilter Timeless on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tape delay plug-in"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install FabFilter Timeless on MacOS using homebrew

- App Name: FabFilter Timeless
- App description: Tape delay plug-in
- App Version: 3.02
- App Website: https://www.fabfilter.com/products/timeless-2-stereo-tape-delay-plug-in

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install FabFilter Timeless with the following command
   ```
   brew install --cask fabfilter-timeless
   ```
4. FabFilter Timeless is ready to use now!