---
title: "Install Synology Chat on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Messaging service that runs on Synology NAS"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Synology Chat on MacOS using homebrew

- App Name: Synology Chat
- App description: Messaging service that runs on Synology NAS
- App Version: 1.2.0-0146
- App Website: https://www.synology.com/en-us/dsm/feature/chat

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Synology Chat with the following command
   ```
   brew install --cask synology-chat
   ```
4. Synology Chat is ready to use now!