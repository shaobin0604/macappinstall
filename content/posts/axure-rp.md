---
title: "Install Axure RP on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Planning and prototyping tool for developers"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Axure RP on MacOS using homebrew

- App Name: Axure RP
- App description: Planning and prototyping tool for developers
- App Version: 10.0.0.3857
- App Website: https://www.axure.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Axure RP with the following command
   ```
   brew install --cask axure-rp
   ```
4. Axure RP is ready to use now!