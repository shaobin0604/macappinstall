---
title: "Install LaTexDraw on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Drawing editor for creating LaTeX PSTricks code"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install LaTexDraw on MacOS using homebrew

- App Name: LaTexDraw
- App description: Drawing editor for creating LaTeX PSTricks code
- App Version: 4.0.3
- App Website: https://latexdraw.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install LaTexDraw with the following command
   ```
   brew install --cask latexdraw
   ```
4. LaTexDraw is ready to use now!