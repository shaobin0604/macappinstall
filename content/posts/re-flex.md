---
title: "Install re-flex on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Regex-centric, fast and flexible scanner generator for C++"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install re-flex on MacOS using homebrew

- App Name: re-flex
- App description: Regex-centric, fast and flexible scanner generator for C++
- App Version: 3.1.0
- App Website: https://www.genivia.com/doc/reflex/html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install re-flex with the following command
   ```
   brew install re-flex
   ```
4. re-flex is ready to use now!