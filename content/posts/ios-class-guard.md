---
title: "Install ios-class-guard on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Objective-C obfuscator for Mach-O executables"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ios-class-guard on MacOS using homebrew

- App Name: ios-class-guard
- App description: Objective-C obfuscator for Mach-O executables
- App Version: 0.8
- App Website: https://github.com/Polidea/ios-class-guard/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ios-class-guard with the following command
   ```
   brew install ios-class-guard
   ```
4. ios-class-guard is ready to use now!