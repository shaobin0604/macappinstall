---
title: "Install suil on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Lightweight C library for loading and wrapping LV2 plugin UIs"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install suil on MacOS using homebrew

- App Name: suil
- App description: Lightweight C library for loading and wrapping LV2 plugin UIs
- App Version: 0.10.10
- App Website: https://drobilla.net/software/suil.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install suil with the following command
   ```
   brew install suil
   ```
4. suil is ready to use now!