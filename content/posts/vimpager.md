---
title: "Install vimpager on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Use ViM as PAGER"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install vimpager on MacOS using homebrew

- App Name: vimpager
- App description: Use ViM as PAGER
- App Version: 2.06
- App Website: https://github.com/rkitover/vimpager

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install vimpager with the following command
   ```
   brew install vimpager
   ```
4. vimpager is ready to use now!