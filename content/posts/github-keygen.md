---
title: "Install github-keygen on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Bootstrap GitHub SSH configuration"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install github-keygen on MacOS using homebrew

- App Name: github-keygen
- App description: Bootstrap GitHub SSH configuration
- App Version: 1.305
- App Website: https://github.com/dolmen/github-keygen

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install github-keygen with the following command
   ```
   brew install github-keygen
   ```
4. github-keygen is ready to use now!