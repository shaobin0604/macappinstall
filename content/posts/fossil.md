---
title: "Install fossil on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Distributed software configuration management"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fossil on MacOS using homebrew

- App Name: fossil
- App description: Distributed software configuration management
- App Version: 2.17
- App Website: https://www.fossil-scm.org/home/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fossil with the following command
   ```
   brew install fossil
   ```
4. fossil is ready to use now!