---
title: "Install transfer on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Standalone TFTP, FTP, and SFTP server"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install transfer on MacOS using homebrew

- App Name: transfer
- App description: Standalone TFTP, FTP, and SFTP server
- App Version: 2.0.2,13
- App Website: https://www.intuitibits.com/products/transfer/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install transfer with the following command
   ```
   brew install --cask transfer
   ```
4. transfer is ready to use now!