---
title: "Install WeakAuras Companion on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Update your auras from Wago.io and creates regular backups of them"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install WeakAuras Companion on MacOS using homebrew

- App Name: WeakAuras Companion
- App description: Update your auras from Wago.io and creates regular backups of them
- App Version: 3.3.4
- App Website: https://weakauras.wtf/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install WeakAuras Companion with the following command
   ```
   brew install --cask weakauras-companion
   ```
4. WeakAuras Companion is ready to use now!