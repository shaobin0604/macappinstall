---
title: "Install spigot on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line streaming exact real calculator"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install spigot on MacOS using homebrew

- App Name: spigot
- App description: Command-line streaming exact real calculator
- App Version: 20210527
- App Website: https://www.chiark.greenend.org.uk/~sgtatham/spigot/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install spigot with the following command
   ```
   brew install spigot
   ```
4. spigot is ready to use now!