---
title: "Install cmuclmtk on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Language model tools (from CMU Sphinx)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cmuclmtk on MacOS using homebrew

- App Name: cmuclmtk
- App description: Language model tools (from CMU Sphinx)
- App Version: 0.7
- App Website: https://cmusphinx.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cmuclmtk with the following command
   ```
   brew install cmuclmtk
   ```
4. cmuclmtk is ready to use now!