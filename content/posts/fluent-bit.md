---
title: "Install fluent-bit on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Fast and Lightweight Logs and Metrics processor"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fluent-bit on MacOS using homebrew

- App Name: fluent-bit
- App description: Fast and Lightweight Logs and Metrics processor
- App Version: 1.8.12
- App Website: https://github.com/fluent/fluent-bit

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fluent-bit with the following command
   ```
   brew install fluent-bit
   ```
4. fluent-bit is ready to use now!