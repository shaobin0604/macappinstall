---
title: "Install lnav on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Curses-based tool for viewing and analyzing log files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install lnav on MacOS using homebrew

- App Name: lnav
- App description: Curses-based tool for viewing and analyzing log files
- App Version: 0.10.1
- App Website: https://lnav.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install lnav with the following command
   ```
   brew install lnav
   ```
4. lnav is ready to use now!