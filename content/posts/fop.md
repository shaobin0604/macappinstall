---
title: "Install fop on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "XSL-FO print formatter for making PDF or PS documents"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fop on MacOS using homebrew

- App Name: fop
- App description: XSL-FO print formatter for making PDF or PS documents
- App Version: 2.7
- App Website: https://xmlgraphics.apache.org/fop/index.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fop with the following command
   ```
   brew install fop
   ```
4. fop is ready to use now!