---
title: "Install TripMode on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Control your data usage on slow or expensive networks"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install TripMode on MacOS using homebrew

- App Name: TripMode
- App description: Control your data usage on slow or expensive networks
- App Version: 3.1.2,1267
- App Website: https://www.tripmode.ch/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install TripMode with the following command
   ```
   brew install --cask tripmode
   ```
4. TripMode is ready to use now!