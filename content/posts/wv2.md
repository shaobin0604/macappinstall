---
title: "Install wv2 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Programs for accessing Microsoft Word documents"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install wv2 on MacOS using homebrew

- App Name: wv2
- App description: Programs for accessing Microsoft Word documents
- App Version: 0.4.2
- App Website: https://wvware.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install wv2 with the following command
   ```
   brew install wv2
   ```
4. wv2 is ready to use now!