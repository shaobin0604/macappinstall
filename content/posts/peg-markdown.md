---
title: "Install peg-markdown on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Markdown implementation based on a PEG grammar"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install peg-markdown on MacOS using homebrew

- App Name: peg-markdown
- App description: Markdown implementation based on a PEG grammar
- App Version: 0.4.14
- App Website: https://github.com/jgm/peg-markdown

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install peg-markdown with the following command
   ```
   brew install peg-markdown
   ```
4. peg-markdown is ready to use now!