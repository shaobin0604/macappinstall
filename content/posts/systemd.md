---
title: "Install systemd on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "System and service manager"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install systemd on MacOS using homebrew

- App Name: systemd
- App description: System and service manager
- App Version: 250
- App Website: https://wiki.freedesktop.org/www/Software/systemd/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install systemd with the following command
   ```
   brew install systemd
   ```
4. systemd is ready to use now!