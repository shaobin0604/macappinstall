---
title: "Install numcpp on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C++ implementation of the Python Numpy library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install numcpp on MacOS using homebrew

- App Name: numcpp
- App description: C++ implementation of the Python Numpy library
- App Version: 2.7.0
- App Website: https://dpilger26.github.io/NumCpp

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install numcpp with the following command
   ```
   brew install numcpp
   ```
4. numcpp is ready to use now!