---
title: "Install MDS on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Deploy Intel and Apple Silicon Macs in Seconds"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install MDS on MacOS using homebrew

- App Name: MDS
- App description: Deploy Intel and Apple Silicon Macs in Seconds
- App Version: 4.0,40106
- App Website: https://twocanoes.com/products/mac/mac-deploy-stick/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install MDS with the following command
   ```
   brew install --cask mds
   ```
4. MDS is ready to use now!