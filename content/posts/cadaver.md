---
title: "Install cadaver on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line client for DAV"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cadaver on MacOS using homebrew

- App Name: cadaver
- App description: Command-line client for DAV
- App Version: 0.23.3
- App Website: https://directory.fsf.org/wiki/Cadaver

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cadaver with the following command
   ```
   brew install cadaver
   ```
4. cadaver is ready to use now!