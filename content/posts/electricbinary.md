---
title: "Install Electric VLSI Design System on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Electrical CAD system for the design of integrated circuits"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Electric VLSI Design System on MacOS using homebrew

- App Name: Electric VLSI Design System
- App description: Electrical CAD system for the design of integrated circuits
- App Version: 9.07
- App Website: https://www.gnu.org/software/electric/electric.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Electric VLSI Design System with the following command
   ```
   brew install --cask electricbinary
   ```
4. Electric VLSI Design System is ready to use now!