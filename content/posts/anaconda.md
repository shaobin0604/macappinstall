---
title: "Install Continuum Analytics Anaconda on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Distribution of the Python and R programming languages for scientific computing"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Continuum Analytics Anaconda on MacOS using homebrew

- App Name: Continuum Analytics Anaconda
- App description: Distribution of the Python and R programming languages for scientific computing
- App Version: 2021.11
- App Website: https://www.anaconda.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Continuum Analytics Anaconda with the following command
   ```
   brew install --cask anaconda
   ```
4. Continuum Analytics Anaconda is ready to use now!