---
title: "Install isa-l on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Intelligent Storage Acceleration Library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install isa-l on MacOS using homebrew

- App Name: isa-l
- App description: Intelligent Storage Acceleration Library
- App Version: 2.30.0
- App Website: https://github.com/intel/isa-l

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install isa-l with the following command
   ```
   brew install isa-l
   ```
4. isa-l is ready to use now!