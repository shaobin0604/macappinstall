---
title: "Install c7n on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Rules engine for cloud security, cost optimization, and governance"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install c7n on MacOS using homebrew

- App Name: c7n
- App description: Rules engine for cloud security, cost optimization, and governance
- App Version: 0.9.14.0
- App Website: https://github.com/cloud-custodian/cloud-custodian

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install c7n with the following command
   ```
   brew install c7n
   ```
4. c7n is ready to use now!