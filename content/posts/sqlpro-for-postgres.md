---
title: "Install SQLPro for Postgres on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Lightweight PostgresSQL database client"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install SQLPro for Postgres on MacOS using homebrew

- App Name: SQLPro for Postgres
- App description: Lightweight PostgresSQL database client
- App Version: 2021.53
- App Website: https://www.macpostgresclient.com/SQLProPostgres

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install SQLPro for Postgres with the following command
   ```
   brew install --cask sqlpro-for-postgres
   ```
4. SQLPro for Postgres is ready to use now!