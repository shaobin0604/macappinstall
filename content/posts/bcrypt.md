---
title: "Install bcrypt on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cross platform file encryption utility using blowfish"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bcrypt on MacOS using homebrew

- App Name: bcrypt
- App description: Cross platform file encryption utility using blowfish
- App Version: 1.1
- App Website: https://bcrypt.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bcrypt with the following command
   ```
   brew install bcrypt
   ```
4. bcrypt is ready to use now!