---
title: "Install 4K Video Downloader on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Free video downloader"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install 4K Video Downloader on MacOS using homebrew

- App Name: 4K Video Downloader
- App description: Free video downloader
- App Version: 4.19.4
- App Website: https://www.4kdownload.com/products/product-videodownloader

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install 4K Video Downloader with the following command
   ```
   brew install --cask 4k-video-downloader
   ```
4. 4K Video Downloader is ready to use now!