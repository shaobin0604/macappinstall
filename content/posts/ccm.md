---
title: "Install ccm on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Create and destroy an Apache Cassandra cluster on localhost"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ccm on MacOS using homebrew

- App Name: ccm
- App description: Create and destroy an Apache Cassandra cluster on localhost
- App Version: 3.1.5
- App Website: https://github.com/pcmanus/ccm

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ccm with the following command
   ```
   brew install ccm
   ```
4. ccm is ready to use now!