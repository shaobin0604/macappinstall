---
title: "Install task on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Feature-rich console based todo list manager"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install task on MacOS using homebrew

- App Name: task
- App description: Feature-rich console based todo list manager
- App Version: 2.6.1
- App Website: https://taskwarrior.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install task with the following command
   ```
   brew install task
   ```
4. task is ready to use now!