---
title: "Install homeshick on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Git dotfiles synchronizer written in bash"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install homeshick on MacOS using homebrew

- App Name: homeshick
- App description: Git dotfiles synchronizer written in bash
- App Version: 2.0.0
- App Website: https://github.com/andsens/homeshick

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install homeshick with the following command
   ```
   brew install homeshick
   ```
4. homeshick is ready to use now!