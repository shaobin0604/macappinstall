---
title: "Install globjects on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C++ library strictly wrapping OpenGL objects"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install globjects on MacOS using homebrew

- App Name: globjects
- App description: C++ library strictly wrapping OpenGL objects
- App Version: 1.1.0
- App Website: https://github.com/cginternals/globjects

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install globjects with the following command
   ```
   brew install globjects
   ```
4. globjects is ready to use now!