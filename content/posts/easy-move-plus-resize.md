---
title: "Install Easy Move+Resize on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utility to support moving and resizing using a modifier key and mouse drag"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Easy Move+Resize on MacOS using homebrew

- App Name: Easy Move+Resize
- App description: Utility to support moving and resizing using a modifier key and mouse drag
- App Version: 1.4.2
- App Website: https://github.com/dmarcotte/easy-move-resize

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Easy Move+Resize with the following command
   ```
   brew install --cask easy-move-plus-resize
   ```
4. Easy Move+Resize is ready to use now!