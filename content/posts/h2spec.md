---
title: "Install h2spec on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Conformance testing tool for HTTP/2 implementation"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install h2spec on MacOS using homebrew

- App Name: h2spec
- App description: Conformance testing tool for HTTP/2 implementation
- App Version: 2.6.0
- App Website: https://github.com/summerwind/h2spec

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install h2spec with the following command
   ```
   brew install h2spec
   ```
4. h2spec is ready to use now!