---
title: "Install boost-bcp on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utility for extracting subsets of the Boost library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install boost-bcp on MacOS using homebrew

- App Name: boost-bcp
- App description: Utility for extracting subsets of the Boost library
- App Version: 1.78.0
- App Website: https://www.boost.org/doc/tools/bcp/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install boost-bcp with the following command
   ```
   brew install boost-bcp
   ```
4. boost-bcp is ready to use now!