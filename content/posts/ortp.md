---
title: "Install ortp on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Real-time transport protocol (RTP, RFC3550) library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ortp on MacOS using homebrew

- App Name: ortp
- App description: Real-time transport protocol (RTP, RFC3550) library
- App Version: 5.1.2
- App Website: https://www.linphone.org/technical-corner/ortp

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ortp with the following command
   ```
   brew install ortp
   ```
4. ortp is ready to use now!