---
title: "Install pmd on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Source code analyzer for Java, JavaScript, and more"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pmd on MacOS using homebrew

- App Name: pmd
- App description: Source code analyzer for Java, JavaScript, and more
- App Version: 6.42.0
- App Website: https://pmd.github.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pmd with the following command
   ```
   brew install pmd
   ```
4. pmd is ready to use now!