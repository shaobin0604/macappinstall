---
title: "Install Aptible Toolbelt on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line tool for Aptible Deploy, an audit-ready App Deployment Platform"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Aptible Toolbelt on MacOS using homebrew

- App Name: Aptible Toolbelt
- App description: Command-line tool for Aptible Deploy, an audit-ready App Deployment Platform
- App Version: 0.19.1,20210802230457,251
- App Website: https://www.aptible.com/documentation/deploy/cli.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Aptible Toolbelt with the following command
   ```
   brew install --cask aptible
   ```
4. Aptible Toolbelt is ready to use now!