---
title: "Install elektra on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Framework to access config settings in a global key database"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install elektra on MacOS using homebrew

- App Name: elektra
- App description: Framework to access config settings in a global key database
- App Version: 0.9.8
- App Website: https://libelektra.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install elektra with the following command
   ```
   brew install elektra
   ```
4. elektra is ready to use now!