---
title: "Install MacZip on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utility to open, create and modify archive files"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install MacZip on MacOS using homebrew

- App Name: MacZip
- App description: Utility to open, create and modify archive files
- App Version: 2.2
- App Website: https://ezip.awehunt.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install MacZip with the following command
   ```
   brew install --cask maczip
   ```
4. MacZip is ready to use now!