---
title: "Install osc-cli on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Official Outscale CLI providing connectors to Outscale API"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install osc-cli on MacOS using homebrew

- App Name: osc-cli
- App description: Official Outscale CLI providing connectors to Outscale API
- App Version: 1.7.1
- App Website: https://github.com/outscale/osc-cli

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install osc-cli with the following command
   ```
   brew install osc-cli
   ```
4. osc-cli is ready to use now!