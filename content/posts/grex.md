---
title: "Install grex on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line tool for generating regular expressions"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install grex on MacOS using homebrew

- App Name: grex
- App description: Command-line tool for generating regular expressions
- App Version: 1.3.0
- App Website: https://github.com/pemistahl/grex

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install grex with the following command
   ```
   brew install grex
   ```
4. grex is ready to use now!