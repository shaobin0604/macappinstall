---
title: "Install feh on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "X11 image viewer"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install feh on MacOS using homebrew

- App Name: feh
- App description: X11 image viewer
- App Version: 3.8
- App Website: https://feh.finalrewind.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install feh with the following command
   ```
   brew install feh
   ```
4. feh is ready to use now!