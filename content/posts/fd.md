---
title: "Install fd on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Simple, fast and user-friendly alternative to find"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fd on MacOS using homebrew

- App Name: fd
- App description: Simple, fast and user-friendly alternative to find
- App Version: 8.3.2
- App Website: https://github.com/sharkdp/fd

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fd with the following command
   ```
   brew install fd
   ```
4. fd is ready to use now!