---
title: "Install xray on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Platform for building proxies to bypass network restrictions"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install xray on MacOS using homebrew

- App Name: xray
- App description: Platform for building proxies to bypass network restrictions
- App Version: 1.5.2
- App Website: https://xtls.github.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install xray with the following command
   ```
   brew install xray
   ```
4. xray is ready to use now!