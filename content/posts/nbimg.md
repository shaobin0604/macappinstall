---
title: "Install nbimg on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Smartphone boot splash screen converter for Android and winCE"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install nbimg on MacOS using homebrew

- App Name: nbimg
- App description: Smartphone boot splash screen converter for Android and winCE
- App Version: 1.2.1
- App Website: https://github.com/poliva/nbimg

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install nbimg with the following command
   ```
   brew install nbimg
   ```
4. nbimg is ready to use now!