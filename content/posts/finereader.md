---
title: "Install ABBYY FineReader Pro on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to parse information out of documents"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ABBYY FineReader Pro on MacOS using homebrew

- App Name: ABBYY FineReader Pro
- App description: Tool to parse information out of documents
- App Version: 12.1.14,1052435
- App Website: https://www.abbyy.com/finereader-pro-mac-downloads/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ABBYY FineReader Pro with the following command
   ```
   brew install --cask finereader
   ```
4. ABBYY FineReader Pro is ready to use now!