---
title: "Install Command Pad on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Start and stop command-line tools and monitor the output"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Command Pad on MacOS using homebrew

- App Name: Command Pad
- App description: Start and stop command-line tools and monitor the output
- App Version: 0.1.2
- App Website: https://github.com/supnate/command-pad

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Command Pad with the following command
   ```
   brew install --cask command-pad
   ```
4. Command Pad is ready to use now!