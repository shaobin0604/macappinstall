---
title: "Install expat on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "XML 1.0 parser"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install expat on MacOS using homebrew

- App Name: expat
- App description: XML 1.0 parser
- App Version: 2.4.4
- App Website: https://libexpat.github.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install expat with the following command
   ```
   brew install expat
   ```
4. expat is ready to use now!