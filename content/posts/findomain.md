---
title: "Install findomain on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cross-platform subdomain enumerator"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install findomain on MacOS using homebrew

- App Name: findomain
- App description: Cross-platform subdomain enumerator
- App Version: 6.1.0
- App Website: https://github.com/Findomain/Findomain

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install findomain with the following command
   ```
   brew install findomain
   ```
4. findomain is ready to use now!