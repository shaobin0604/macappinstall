---
title: "Install teku on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Java Implementation of the Ethereum 2.0 Beacon Chain"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install teku on MacOS using homebrew

- App Name: teku
- App description: Java Implementation of the Ethereum 2.0 Beacon Chain
- App Version: 22.1.0
- App Website: https://docs.teku.consensys.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install teku with the following command
   ```
   brew install teku
   ```
4. teku is ready to use now!