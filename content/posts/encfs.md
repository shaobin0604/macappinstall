---
title: "Install encfs on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Encrypted pass-through FUSE file system"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install encfs on MacOS using homebrew

- App Name: encfs
- App description: Encrypted pass-through FUSE file system
- App Version: 1.9.5
- App Website: https://vgough.github.io/encfs/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install encfs with the following command
   ```
   brew install encfs
   ```
4. encfs is ready to use now!