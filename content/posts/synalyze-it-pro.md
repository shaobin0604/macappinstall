---
title: "Install Synalyze It! Pro on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Hex editing and binary file analysis app"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Synalyze It! Pro on MacOS using homebrew

- App Name: Synalyze It! Pro
- App description: Hex editing and binary file analysis app
- App Version: 1.23.4
- App Website: https://www.synalysis.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Synalyze It! Pro with the following command
   ```
   brew install --cask synalyze-it-pro
   ```
4. Synalyze It! Pro is ready to use now!