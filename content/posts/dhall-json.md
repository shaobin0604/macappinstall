---
title: "Install dhall-json on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Dhall to JSON compiler and a Dhall to YAML compiler"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dhall-json on MacOS using homebrew

- App Name: dhall-json
- App description: Dhall to JSON compiler and a Dhall to YAML compiler
- App Version: 1.7.9
- App Website: https://github.com/dhall-lang/dhall-haskell/tree/master/dhall-json

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dhall-json with the following command
   ```
   brew install dhall-json
   ```
4. dhall-json is ready to use now!