---
title: "Install Gloomhaven Helper on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Officially licensed companion application for playing the Gloomhaven board game"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Gloomhaven Helper on MacOS using homebrew

- App Name: Gloomhaven Helper
- App description: Officially licensed companion application for playing the Gloomhaven board game
- App Version: 8.4.12
- App Website: https://esotericsoftware.com/gloomhaven-helper

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Gloomhaven Helper with the following command
   ```
   brew install --cask gloomhaven-helper
   ```
4. Gloomhaven Helper is ready to use now!