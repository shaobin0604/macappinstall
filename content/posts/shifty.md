---
title: "Install Shifty on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Menu bar app that provides more control over Night Shift"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Shifty on MacOS using homebrew

- App Name: Shifty
- App description: Menu bar app that provides more control over Night Shift
- App Version: 1.2
- App Website: https://shifty.natethompson.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Shifty with the following command
   ```
   brew install --cask shifty
   ```
4. Shifty is ready to use now!