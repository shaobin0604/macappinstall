---
title: "Install fakeroot on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Provide a fake root environment"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fakeroot on MacOS using homebrew

- App Name: fakeroot
- App description: Provide a fake root environment
- App Version: 1.24
- App Website: https://tracker.debian.org/pkg/fakeroot

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fakeroot with the following command
   ```
   brew install fakeroot
   ```
4. fakeroot is ready to use now!