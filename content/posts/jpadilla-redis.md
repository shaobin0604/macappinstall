---
title: "Install Redis on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "App wrapper for Redis"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Redis on MacOS using homebrew

- App Name: Redis
- App description: App wrapper for Redis
- App Version: 4.0.2-build.1
- App Website: https://jpadilla.github.io/redisapp/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Redis with the following command
   ```
   brew install --cask jpadilla-redis
   ```
4. Redis is ready to use now!