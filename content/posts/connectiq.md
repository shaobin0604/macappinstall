---
title: "Install Garmin Connect IQ SDK on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Build wearable experiences for Garmin devices and sensors with ConnectIQ SDK"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Garmin Connect IQ SDK on MacOS using homebrew

- App Name: Garmin Connect IQ SDK
- App description: Build wearable experiences for Garmin devices and sensors with ConnectIQ SDK
- App Version: 4.0.9,2022-01-24,2154651d3
- App Website: https://developer.garmin.com/connect-iq/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Garmin Connect IQ SDK with the following command
   ```
   brew install --cask connectiq
   ```
4. Garmin Connect IQ SDK is ready to use now!