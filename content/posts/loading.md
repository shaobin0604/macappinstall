---
title: "Install Loading on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Network activity monitor"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Loading on MacOS using homebrew

- App Name: Loading
- App description: Network activity monitor
- App Version: 1.2.6,632
- App Website: https://bonzaiapps.com/loading/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Loading with the following command
   ```
   brew install --cask loading
   ```
4. Loading is ready to use now!