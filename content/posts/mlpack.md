---
title: "Install mlpack on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Scalable C++ machine learning library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mlpack on MacOS using homebrew

- App Name: mlpack
- App description: Scalable C++ machine learning library
- App Version: 3.4.2
- App Website: https://www.mlpack.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mlpack with the following command
   ```
   brew install mlpack
   ```
4. mlpack is ready to use now!