---
title: "Install chars on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line tool to display information about unicode characters"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install chars on MacOS using homebrew

- App Name: chars
- App description: Command-line tool to display information about unicode characters
- App Version: 0.6.0
- App Website: https://github.com/antifuchs/chars/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install chars with the following command
   ```
   brew install chars
   ```
4. chars is ready to use now!