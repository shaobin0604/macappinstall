---
title: "Install amtk on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Actions, Menus and Toolbars Kit for GNOME"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install amtk on MacOS using homebrew

- App Name: amtk
- App description: Actions, Menus and Toolbars Kit for GNOME
- App Version: 5.2.0
- App Website: https://wiki.gnome.org/Projects/Amtk

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install amtk with the following command
   ```
   brew install amtk
   ```
4. amtk is ready to use now!