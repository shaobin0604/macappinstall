---
title: "Install sqlite on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line interface for SQLite"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sqlite on MacOS using homebrew

- App Name: sqlite
- App description: Command-line interface for SQLite
- App Version: 3.37.2
- App Website: https://sqlite.org/index.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sqlite with the following command
   ```
   brew install sqlite
   ```
4. sqlite is ready to use now!