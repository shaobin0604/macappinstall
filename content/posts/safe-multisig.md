---
title: "Install Gnosis Safe Multisig on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Ethereum multisig wallet"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Gnosis Safe Multisig on MacOS using homebrew

- App Name: Gnosis Safe Multisig
- App description: Ethereum multisig wallet
- App Version: 3.8.4
- App Website: https://gnosis-safe.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Gnosis Safe Multisig with the following command
   ```
   brew install --cask safe-multisig
   ```
4. Gnosis Safe Multisig is ready to use now!