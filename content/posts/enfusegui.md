---
title: "Install EnfuseGUI on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "HDR image creator"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install EnfuseGUI on MacOS using homebrew

- App Name: EnfuseGUI
- App description: HDR image creator
- App Version: 3.2
- App Website: https://swipeware.com/applications/enfusegui/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install EnfuseGUI with the following command
   ```
   brew install --cask enfusegui
   ```
4. EnfuseGUI is ready to use now!