---
title: "Install lci on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Interpreter for the lambda calculus"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install lci on MacOS using homebrew

- App Name: lci
- App description: Interpreter for the lambda calculus
- App Version: 1.0
- App Website: https://www.chatzi.org/lci/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install lci with the following command
   ```
   brew install lci
   ```
4. lci is ready to use now!