---
title: "Install NoSQL Workbench on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Client-side GUI application for modern database development and operations"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install NoSQL Workbench on MacOS using homebrew

- App Name: NoSQL Workbench
- App description: Client-side GUI application for modern database development and operations
- App Version: 3.2.1
- App Website: https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/workbench.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install NoSQL Workbench with the following command
   ```
   brew install --cask nosql-workbench
   ```
4. NoSQL Workbench is ready to use now!