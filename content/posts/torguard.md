---
title: "Install TorGuard on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "VPN client"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install TorGuard on MacOS using homebrew

- App Name: TorGuard
- App description: VPN client
- App Version: 4.8.6
- App Website: https://torguard.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install TorGuard with the following command
   ```
   brew install --cask torguard
   ```
4. TorGuard is ready to use now!