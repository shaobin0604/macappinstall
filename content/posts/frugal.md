---
title: "Install frugal on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cross language code generator for creating scalable microservices"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install frugal on MacOS using homebrew

- App Name: frugal
- App description: Cross language code generator for creating scalable microservices
- App Version: 3.14.14
- App Website: https://github.com/Workiva/frugal

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install frugal with the following command
   ```
   brew install frugal
   ```
4. frugal is ready to use now!