---
title: "Install rp on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to find ROP sequences in PE/Elf/Mach-O x86/x64 binaries"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install rp on MacOS using homebrew

- App Name: rp
- App description: Tool to find ROP sequences in PE/Elf/Mach-O x86/x64 binaries
- App Version: 1.0
- App Website: https://0vercl0k.github.io/rp/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install rp with the following command
   ```
   brew install rp
   ```
4. rp is ready to use now!