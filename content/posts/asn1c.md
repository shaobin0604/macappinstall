---
title: "Install asn1c on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Compile ASN.1 specifications into C source code"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install asn1c on MacOS using homebrew

- App Name: asn1c
- App description: Compile ASN.1 specifications into C source code
- App Version: 0.9.28
- App Website: http://www.lionet.info/asn1c/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install asn1c with the following command
   ```
   brew install asn1c
   ```
4. asn1c is ready to use now!