---
title: "Install Photo Supreme Single User on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Photo Supreme Single User on MacOS using homebrew

- App Name: Photo Supreme Single User
- App description: null
- App Version: 4
- App Website: https://www.idimager.com/home

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Photo Supreme Single User with the following command
   ```
   brew install --cask photo-supreme-single-user
   ```
4. Photo Supreme Single User is ready to use now!