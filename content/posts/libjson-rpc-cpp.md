---
title: "Install libjson-rpc-cpp on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C++ framework for json-rpc"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libjson-rpc-cpp on MacOS using homebrew

- App Name: libjson-rpc-cpp
- App description: C++ framework for json-rpc
- App Version: 1.4.1
- App Website: https://github.com/cinemast/libjson-rpc-cpp

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libjson-rpc-cpp with the following command
   ```
   brew install libjson-rpc-cpp
   ```
4. libjson-rpc-cpp is ready to use now!