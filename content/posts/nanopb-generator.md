---
title: "Install nanopb-generator on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C library for encoding and decoding Protocol Buffer messages"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install nanopb-generator on MacOS using homebrew

- App Name: nanopb-generator
- App description: C library for encoding and decoding Protocol Buffer messages
- App Version: 0.4.5
- App Website: https://jpa.kapsi.fi/nanopb/docs/index.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install nanopb-generator with the following command
   ```
   brew install nanopb-generator
   ```
4. nanopb-generator is ready to use now!