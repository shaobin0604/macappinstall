---
title: "Install Drop to GIF on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Zero-click animated Gifs"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Drop to GIF on MacOS using homebrew

- App Name: Drop to GIF
- App description: Zero-click animated Gifs
- App Version: 1.28
- App Website: https://mortenjust.github.io/droptogif/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Drop to GIF with the following command
   ```
   brew install --cask drop-to-gif
   ```
4. Drop to GIF is ready to use now!