---
title: "Install rnv on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Implementation of Relax NG Compact Syntax validator"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install rnv on MacOS using homebrew

- App Name: rnv
- App description: Implementation of Relax NG Compact Syntax validator
- App Version: 1.7.11
- App Website: https://sourceforge.net/projects/rnv/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install rnv with the following command
   ```
   brew install rnv
   ```
4. rnv is ready to use now!