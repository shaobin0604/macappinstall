---
title: "Install truffle on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Development environment, testing framework and asset pipeline for Ethereum"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install truffle on MacOS using homebrew

- App Name: truffle
- App description: Development environment, testing framework and asset pipeline for Ethereum
- App Version: 5.4.12
- App Website: https://trufflesuite.com

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install truffle with the following command
   ```
   brew install truffle
   ```
4. truffle is ready to use now!