---
title: "Install rush on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GNU's Restricted User SHell"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install rush on MacOS using homebrew

- App Name: rush
- App description: GNU's Restricted User SHell
- App Version: 2.2
- App Website: https://www.gnu.org/software/rush/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install rush with the following command
   ```
   brew install rush
   ```
4. rush is ready to use now!