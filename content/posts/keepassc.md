---
title: "Install keepassc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Curses-based password manager for KeePass v.1.x and KeePassX"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install keepassc on MacOS using homebrew

- App Name: keepassc
- App description: Curses-based password manager for KeePass v.1.x and KeePassX
- App Version: 1.8.2
- App Website: https://github.com/raymontag/keepassc

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install keepassc with the following command
   ```
   brew install keepassc
   ```
4. keepassc is ready to use now!