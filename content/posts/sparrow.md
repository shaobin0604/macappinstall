---
title: "Install Sparrow Bitcoin Wallet on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Bitcoin wallet application"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Sparrow Bitcoin Wallet on MacOS using homebrew

- App Name: Sparrow Bitcoin Wallet
- App description: Bitcoin wallet application
- App Version: 1.5.5
- App Website: https://sparrowwallet.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Sparrow Bitcoin Wallet with the following command
   ```
   brew install --cask sparrow
   ```
4. Sparrow Bitcoin Wallet is ready to use now!