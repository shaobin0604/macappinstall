---
title: "Install beansdb on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Yet another distributed key-value storage system"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install beansdb on MacOS using homebrew

- App Name: beansdb
- App description: Yet another distributed key-value storage system
- App Version: 0.7.1.4
- App Website: https://github.com/douban/beansdb

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install beansdb with the following command
   ```
   brew install beansdb
   ```
4. beansdb is ready to use now!