---
title: "Install tin on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Threaded, NNTP-, and spool-based UseNet newsreader"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tin on MacOS using homebrew

- App Name: tin
- App description: Threaded, NNTP-, and spool-based UseNet newsreader
- App Version: 2.6.1
- App Website: http://www.tin.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tin with the following command
   ```
   brew install tin
   ```
4. tin is ready to use now!