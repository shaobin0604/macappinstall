---
title: "Install libsodium on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "NaCl networking and cryptography library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libsodium on MacOS using homebrew

- App Name: libsodium
- App description: NaCl networking and cryptography library
- App Version: 1.0.18
- App Website: https://libsodium.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libsodium with the following command
   ```
   brew install libsodium
   ```
4. libsodium is ready to use now!