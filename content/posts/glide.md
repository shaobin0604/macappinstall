---
title: "Install glide on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Simplified Go project management, dependency management, and vendoring"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install glide on MacOS using homebrew

- App Name: glide
- App description: Simplified Go project management, dependency management, and vendoring
- App Version: 0.13.3
- App Website: https://github.com/Masterminds/glide

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install glide with the following command
   ```
   brew install glide
   ```
4. glide is ready to use now!