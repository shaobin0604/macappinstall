---
title: "Install LaunchControl on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Create, manage and debug system- and user services"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install LaunchControl on MacOS using homebrew

- App Name: LaunchControl
- App description: Create, manage and debug system- and user services
- App Version: 1.52.4,1777
- App Website: https://www.soma-zone.com/LaunchControl/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install LaunchControl with the following command
   ```
   brew install --cask launchcontrol
   ```
4. LaunchControl is ready to use now!