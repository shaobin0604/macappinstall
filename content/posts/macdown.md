---
title: "Install MacDown on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Open-source Markdown editor"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install MacDown on MacOS using homebrew

- App Name: MacDown
- App description: Open-source Markdown editor
- App Version: 0.7.2
- App Website: https://macdown.uranusjr.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install MacDown with the following command
   ```
   brew install --cask macdown
   ```
4. MacDown is ready to use now!