---
title: "Install gumbo-parser on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C99 library for parsing HTML5"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gumbo-parser on MacOS using homebrew

- App Name: gumbo-parser
- App description: C99 library for parsing HTML5
- App Version: 0.10.1
- App Website: https://github.com/google/gumbo-parser

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gumbo-parser with the following command
   ```
   brew install gumbo-parser
   ```
4. gumbo-parser is ready to use now!