---
title: "Install austin on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Python frame stack sampler for CPython"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install austin on MacOS using homebrew

- App Name: austin
- App description: Python frame stack sampler for CPython
- App Version: 3.3.0
- App Website: https://github.com/P403n1x87/austin

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install austin with the following command
   ```
   brew install austin
   ```
4. austin is ready to use now!