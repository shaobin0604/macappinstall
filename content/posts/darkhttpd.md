---
title: "Install darkhttpd on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Small static webserver without CGI"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install darkhttpd on MacOS using homebrew

- App Name: darkhttpd
- App description: Small static webserver without CGI
- App Version: 1.13
- App Website: https://unix4lyfe.org/darkhttpd/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install darkhttpd with the following command
   ```
   brew install darkhttpd
   ```
4. darkhttpd is ready to use now!