---
title: "Install Audiobook Builder on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Turn audio CDs and files into audiobooks"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Audiobook Builder on MacOS using homebrew

- App Name: Audiobook Builder
- App description: Turn audio CDs and files into audiobooks
- App Version: 2.2.2
- App Website: https://www.splasm.com/audiobookbuilder/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Audiobook Builder with the following command
   ```
   brew install --cask audiobook-builder
   ```
4. Audiobook Builder is ready to use now!