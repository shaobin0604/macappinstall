---
title: "Install SparkleShare on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to sync with any Git repository instantly"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install SparkleShare on MacOS using homebrew

- App Name: SparkleShare
- App description: Tool to sync with any Git repository instantly
- App Version: 3.28
- App Website: https://sparkleshare.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install SparkleShare with the following command
   ```
   brew install --cask sparkleshare
   ```
4. SparkleShare is ready to use now!