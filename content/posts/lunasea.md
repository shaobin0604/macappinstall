---
title: "Install LunaSea on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Open source self-hosted remote control for media software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install LunaSea on MacOS using homebrew

- App Name: LunaSea
- App description: Open source self-hosted remote control for media software
- App Version: 6.0.0,60000006
- App Website: https://github.com/CometTools/LunaSea/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install LunaSea with the following command
   ```
   brew install --cask lunasea
   ```
4. LunaSea is ready to use now!