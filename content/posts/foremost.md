---
title: "Install foremost on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Console program to recover files based on their headers and footers"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install foremost on MacOS using homebrew

- App Name: foremost
- App description: Console program to recover files based on their headers and footers
- App Version: 1.5.7
- App Website: https://foremost.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install foremost with the following command
   ```
   brew install foremost
   ```
4. foremost is ready to use now!