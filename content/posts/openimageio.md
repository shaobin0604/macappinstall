---
title: "Install openimageio on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library for reading, processing and writing images"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install openimageio on MacOS using homebrew

- App Name: openimageio
- App description: Library for reading, processing and writing images
- App Version: 2.3.11.0
- App Website: https://openimageio.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install openimageio with the following command
   ```
   brew install openimageio
   ```
4. openimageio is ready to use now!