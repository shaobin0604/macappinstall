---
title: "Install ragel on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "State machine compiler"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ragel on MacOS using homebrew

- App Name: ragel
- App description: State machine compiler
- App Version: 6.10
- App Website: https://www.colm.net/open-source/ragel/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ragel with the following command
   ```
   brew install ragel
   ```
4. ragel is ready to use now!