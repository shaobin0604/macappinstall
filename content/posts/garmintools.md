---
title: "Install garmintools on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Interface to the Garmin Forerunner GPS units"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install garmintools on MacOS using homebrew

- App Name: garmintools
- App description: Interface to the Garmin Forerunner GPS units
- App Version: 0.10
- App Website: https://code.google.com/archive/p/garmintools/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install garmintools with the following command
   ```
   brew install garmintools
   ```
4. garmintools is ready to use now!