---
title: "Install 1Password CLI on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line helper for the 1Password password manager"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install 1Password CLI on MacOS using homebrew

- App Name: 1Password CLI
- App description: Command-line helper for the 1Password password manager
- App Version: 1.12.4
- App Website: https://support.1password.com/command-line/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install 1Password CLI with the following command
   ```
   brew install --cask 1password-cli
   ```
4. 1Password CLI is ready to use now!