---
title: "Install tokei on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Program that allows you to count code, quickly"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tokei on MacOS using homebrew

- App Name: tokei
- App description: Program that allows you to count code, quickly
- App Version: 12.1.2
- App Website: https://github.com/XAMPPRocky/tokei

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tokei with the following command
   ```
   brew install tokei
   ```
4. tokei is ready to use now!