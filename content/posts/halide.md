---
title: "Install halide on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Language for fast, portable data-parallel computation"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install halide on MacOS using homebrew

- App Name: halide
- App description: Language for fast, portable data-parallel computation
- App Version: 13.0.4
- App Website: https://halide-lang.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install halide with the following command
   ```
   brew install halide
   ```
4. halide is ready to use now!