---
title: "Install AppGrid on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Window manager with Vim–like hotkeys"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install AppGrid on MacOS using homebrew

- App Name: AppGrid
- App description: Window manager with Vim–like hotkeys
- App Version: 1.0.4
- App Website: https://github.com/mjolnirapp/AppGrid/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install AppGrid with the following command
   ```
   brew install --cask appgrid
   ```
4. AppGrid is ready to use now!