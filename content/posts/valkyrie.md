---
title: "Install Valkyrie on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Game Master for Fantasy Flight board games"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Valkyrie on MacOS using homebrew

- App Name: Valkyrie
- App description: Game Master for Fantasy Flight board games
- App Version: 2.5.6
- App Website: https://npbruce.github.io/valkyrie/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Valkyrie with the following command
   ```
   brew install --cask valkyrie
   ```
4. Valkyrie is ready to use now!