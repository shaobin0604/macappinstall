---
title: "Install Transcribe! on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Transcribes recorded music"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Transcribe! on MacOS using homebrew

- App Name: Transcribe!
- App description: Transcribes recorded music
- App Version: 9.10
- App Website: https://www.seventhstring.com/xscribe/overview.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Transcribe! with the following command
   ```
   brew install --cask transcribe
   ```
4. Transcribe! is ready to use now!