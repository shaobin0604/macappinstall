---
title: "Install VMware Horizon Client on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Virtual machine client"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install VMware Horizon Client on MacOS using homebrew

- App Name: VMware Horizon Client
- App description: Virtual machine client
- App Version: 2111-8.4.0-18968281,CART22FH2
- App Website: https://www.vmware.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install VMware Horizon Client with the following command
   ```
   brew install --cask vmware-horizon-client
   ```
4. VMware Horizon Client is ready to use now!