---
title: "Install Nozbe on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Task manager"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Nozbe on MacOS using homebrew

- App Name: Nozbe
- App description: Task manager
- App Version: 3.21,3210.7
- App Website: https://nozbe.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Nozbe with the following command
   ```
   brew install --cask nozbe-personal
   ```
4. Nozbe is ready to use now!