---
title: "Install Bitwarden on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Desktop password and login vault"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Bitwarden on MacOS using homebrew

- App Name: Bitwarden
- App description: Desktop password and login vault
- App Version: 1.31.3
- App Website: https://bitwarden.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Bitwarden with the following command
   ```
   brew install --cask bitwarden
   ```
4. Bitwarden is ready to use now!