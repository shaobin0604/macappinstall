---
title: "Install newt on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library for color text mode, widget based user interfaces"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install newt on MacOS using homebrew

- App Name: newt
- App description: Library for color text mode, widget based user interfaces
- App Version: 0.52.21
- App Website: https://pagure.io/newt

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install newt with the following command
   ```
   brew install newt
   ```
4. newt is ready to use now!