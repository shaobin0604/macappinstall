---
title: "Install md4c on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C Markdown parser. Fast. SAX-like interface"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install md4c on MacOS using homebrew

- App Name: md4c
- App description: C Markdown parser. Fast. SAX-like interface
- App Version: 0.4.8
- App Website: https://github.com/mity/md4c

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install md4c with the following command
   ```
   brew install md4c
   ```
4. md4c is ready to use now!