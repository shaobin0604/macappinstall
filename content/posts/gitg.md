---
title: "Install gitg on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GNOME GUI client to view git repositories"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gitg on MacOS using homebrew

- App Name: gitg
- App description: GNOME GUI client to view git repositories
- App Version: 41
- App Website: https://wiki.gnome.org/Apps/Gitg

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gitg with the following command
   ```
   brew install gitg
   ```
4. gitg is ready to use now!