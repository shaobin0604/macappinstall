---
title: "Install A Better Finder Attributes on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "File and photo tweaking tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install A Better Finder Attributes on MacOS using homebrew

- App Name: A Better Finder Attributes
- App description: File and photo tweaking tool
- App Version: 7.17
- App Website: https://www.publicspace.net/ABetterFinderAttributes/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install A Better Finder Attributes with the following command
   ```
   brew install --cask a-better-finder-attributes
   ```
4. A Better Finder Attributes is ready to use now!