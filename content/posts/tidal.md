---
title: "Install TIDAL on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Music streaming service with high fidelity sound and hi-def video quality"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install TIDAL on MacOS using homebrew

- App Name: TIDAL
- App description: Music streaming service with high fidelity sound and hi-def video quality
- App Version: 2.29.7
- App Website: https://tidal.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install TIDAL with the following command
   ```
   brew install --cask tidal
   ```
4. TIDAL is ready to use now!