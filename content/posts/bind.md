---
title: "Install bind on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Implementation of the DNS protocols"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bind on MacOS using homebrew

- App Name: bind
- App description: Implementation of the DNS protocols
- App Version: 9.18.0
- App Website: https://www.isc.org/bind/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bind with the following command
   ```
   brew install bind
   ```
4. bind is ready to use now!