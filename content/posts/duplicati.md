---
title: "Install Duplicati on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Store securely encrypted backups in the cloud"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Duplicati on MacOS using homebrew

- App Name: Duplicati
- App description: Store securely encrypted backups in the cloud
- App Version: 2.0.6.3_beta_2021-06-17
- App Website: https://www.duplicati.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Duplicati with the following command
   ```
   brew install --cask duplicati
   ```
4. Duplicati is ready to use now!