---
title: "Install skafos on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CLI for the Metis Machine A.I. and machine learning deployment platform"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install skafos on MacOS using homebrew

- App Name: skafos
- App description: CLI for the Metis Machine A.I. and machine learning deployment platform
- App Version: 1.7.7
- App Website: https://skafos.ai/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install skafos with the following command
   ```
   brew install skafos
   ```
4. skafos is ready to use now!