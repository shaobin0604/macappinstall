---
title: "Install sdf on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Syntax Definition Formalism: high-level description of grammars"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sdf on MacOS using homebrew

- App Name: sdf
- App description: Syntax Definition Formalism: high-level description of grammars
- App Version: 2.6.3
- App Website: https://strategoxt.org/Sdf/WebHome

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sdf with the following command
   ```
   brew install sdf
   ```
4. sdf is ready to use now!