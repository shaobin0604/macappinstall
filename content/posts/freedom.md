---
title: "Install Freedom on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "App and website blocker"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Freedom on MacOS using homebrew

- App Name: Freedom
- App description: App and website blocker
- App Version: 2.7,1954.1
- App Website: https://freedom.to/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Freedom with the following command
   ```
   brew install --cask freedom
   ```
4. Freedom is ready to use now!