---
title: "Install reaver on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Implements brute force attack to recover WPA/WPA2 passkeys"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install reaver on MacOS using homebrew

- App Name: reaver
- App description: Implements brute force attack to recover WPA/WPA2 passkeys
- App Version: 1.4
- App Website: https://code.google.com/archive/p/reaver-wps/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install reaver with the following command
   ```
   brew install reaver
   ```
4. reaver is ready to use now!