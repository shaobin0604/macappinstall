---
title: "Install libforensic1394 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Live memory forensics over IEEE 1394 (\"FireWire\") interface"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libforensic1394 on MacOS using homebrew

- App Name: libforensic1394
- App description: Live memory forensics over IEEE 1394 ("FireWire") interface
- App Version: 0.2
- App Website: https://freddie.witherden.org/tools/libforensic1394/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libforensic1394 with the following command
   ```
   brew install libforensic1394
   ```
4. libforensic1394 is ready to use now!