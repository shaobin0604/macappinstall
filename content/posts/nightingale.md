---
title: "Install Nightingale on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Working tree for the community fork of Songbird"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Nightingale on MacOS using homebrew

- App Name: Nightingale
- App description: Working tree for the community fork of Songbird
- App Version: 1.12.1,2454
- App Website: https://getnightingale.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Nightingale with the following command
   ```
   brew install --cask nightingale
   ```
4. Nightingale is ready to use now!