---
title: "Install whois on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Lookup tool for domain names and other internet resources"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install whois on MacOS using homebrew

- App Name: whois
- App description: Lookup tool for domain names and other internet resources
- App Version: 5.5.11
- App Website: https://packages.debian.org/sid/whois

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install whois with the following command
   ```
   brew install whois
   ```
4. whois is ready to use now!