---
title: "Install mspdebug on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Debugger for use with MSP430 MCUs"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mspdebug on MacOS using homebrew

- App Name: mspdebug
- App description: Debugger for use with MSP430 MCUs
- App Version: 0.25
- App Website: https://dlbeer.co.nz/mspdebug/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mspdebug with the following command
   ```
   brew install mspdebug
   ```
4. mspdebug is ready to use now!