---
title: "Install knock on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Port-knock server"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install knock on MacOS using homebrew

- App Name: knock
- App description: Port-knock server
- App Version: 0.8
- App Website: https://zeroflux.org/projects/knock

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install knock with the following command
   ```
   brew install knock
   ```
4. knock is ready to use now!