---
title: "Install nailgun on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line client, protocol and server for Java programs"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install nailgun on MacOS using homebrew

- App Name: nailgun
- App description: Command-line client, protocol and server for Java programs
- App Version: 1.0.1
- App Website: http://www.martiansoftware.com/nailgun/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install nailgun with the following command
   ```
   brew install nailgun
   ```
4. nailgun is ready to use now!