---
title: "Install MindMaster on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install MindMaster on MacOS using homebrew

- App Name: MindMaster
- App description: null
- App Version: 8.5.1,851
- App Website: https://www.edrawsoft.com/mindmaster/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install MindMaster with the following command
   ```
   brew install --cask mindmaster
   ```
4. MindMaster is ready to use now!