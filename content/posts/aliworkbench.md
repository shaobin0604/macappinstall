---
title: "Install AliWorkBench on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Merchant workbench"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install AliWorkBench on MacOS using homebrew

- App Name: AliWorkBench
- App description: Merchant workbench
- App Version: 9.04.02,LqEYADnbwALXMQPyQRIT
- App Website: https://cts.alibaba.com/product/qianniu/download-pc

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install AliWorkBench with the following command
   ```
   brew install --cask aliworkbench
   ```
4. AliWorkBench is ready to use now!