---
title: "Install Sipgate Softphone on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Make telephone calls on the computer"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Sipgate Softphone on MacOS using homebrew

- App Name: Sipgate Softphone
- App description: Make telephone calls on the computer
- App Version: 1.16.4
- App Website: https://www.sipgateteam.de/softphone

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Sipgate Softphone with the following command
   ```
   brew install --cask sipgate-softphone
   ```
4. Sipgate Softphone is ready to use now!