---
title: "Install dmidiplayer on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Multiplatform MIDI File Player"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dmidiplayer on MacOS using homebrew

- App Name: dmidiplayer
- App description: Multiplatform MIDI File Player
- App Version: 1.5.2
- App Website: https://dmidiplayer.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dmidiplayer with the following command
   ```
   brew install --cask dmidiplayer
   ```
4. dmidiplayer is ready to use now!