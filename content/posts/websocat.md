---
title: "Install websocat on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line client for WebSockets"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install websocat on MacOS using homebrew

- App Name: websocat
- App description: Command-line client for WebSockets
- App Version: 1.9.0
- App Website: https://github.com/vi/websocat

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install websocat with the following command
   ```
   brew install websocat
   ```
4. websocat is ready to use now!