---
title: "Install git-remote-hg on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Transparent bidirectional bridge between Git and Mercurial"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install git-remote-hg on MacOS using homebrew

- App Name: git-remote-hg
- App description: Transparent bidirectional bridge between Git and Mercurial
- App Version: 0.4
- App Website: https://github.com/felipec/git-remote-hg

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install git-remote-hg with the following command
   ```
   brew install git-remote-hg
   ```
4. git-remote-hg is ready to use now!