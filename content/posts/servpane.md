---
title: "Install ServPane on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Launchd menu bar app"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ServPane on MacOS using homebrew

- App Name: ServPane
- App description: Launchd menu bar app
- App Version: 0.1.1
- App Website: https://github.com/aderyabin/ServPane

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ServPane with the following command
   ```
   brew install --cask servpane
   ```
4. ServPane is ready to use now!