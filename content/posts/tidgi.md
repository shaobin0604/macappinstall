---
title: "Install TidGi on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Personal knowledge-base app"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install TidGi on MacOS using homebrew

- App Name: TidGi
- App description: Personal knowledge-base app
- App Version: 0.7.3
- App Website: https://github.com/tiddly-gittly/TidGi-Desktop

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install TidGi with the following command
   ```
   brew install --cask tidgi
   ```
4. TidGi is ready to use now!