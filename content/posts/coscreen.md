---
title: "Install CoScreen on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Collaboration tool with multi-user screen sharing"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install CoScreen on MacOS using homebrew

- App Name: CoScreen
- App description: Collaboration tool with multi-user screen sharing
- App Version: 3.9.43
- App Website: https://www.coscreen.co/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install CoScreen with the following command
   ```
   brew install --cask coscreen
   ```
4. CoScreen is ready to use now!