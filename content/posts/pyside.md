---
title: "Install pyside on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Official Python bindings for Qt"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pyside on MacOS using homebrew

- App Name: pyside
- App description: Official Python bindings for Qt
- App Version: 6.2.2
- App Website: https://wiki.qt.io/Qt_for_Python

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pyside with the following command
   ```
   brew install pyside
   ```
4. pyside is ready to use now!