---
title: "Install internetarchive on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Python wrapper for the various Internet Archive APIs"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install internetarchive on MacOS using homebrew

- App Name: internetarchive
- App description: Python wrapper for the various Internet Archive APIs
- App Version: 2.3.0
- App Website: https://github.com/jjjake/internetarchive

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install internetarchive with the following command
   ```
   brew install internetarchive
   ```
4. internetarchive is ready to use now!