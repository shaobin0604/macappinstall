---
title: "Install libfido2 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Provides library functionality for FIDO U2F & FIDO 2.0, including USB"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libfido2 on MacOS using homebrew

- App Name: libfido2
- App description: Provides library functionality for FIDO U2F & FIDO 2.0, including USB
- App Version: 1.10.0
- App Website: https://developers.yubico.com/libfido2/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libfido2 with the following command
   ```
   brew install libfido2
   ```
4. libfido2 is ready to use now!