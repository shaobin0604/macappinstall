---
title: "Install viewvc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Browser interface for CVS and Subversion repositories"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install viewvc on MacOS using homebrew

- App Name: viewvc
- App description: Browser interface for CVS and Subversion repositories
- App Version: 1.2.1
- App Website: http://www.viewvc.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install viewvc with the following command
   ```
   brew install viewvc
   ```
4. viewvc is ready to use now!