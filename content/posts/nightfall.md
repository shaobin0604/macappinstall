---
title: "Install Nightfall on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Menu bar utility for toggling dark mode"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Nightfall on MacOS using homebrew

- App Name: Nightfall
- App description: Menu bar utility for toggling dark mode
- App Version: 3.0.0
- App Website: https://github.com/r-thomson/Nightfall/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Nightfall with the following command
   ```
   brew install --cask nightfall
   ```
4. Nightfall is ready to use now!