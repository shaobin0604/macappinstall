---
title: "Install ircd-hybrid on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "High-performance secure IRC server"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ircd-hybrid on MacOS using homebrew

- App Name: ircd-hybrid
- App description: High-performance secure IRC server
- App Version: 8.2.39
- App Website: https://www.ircd-hybrid.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ircd-hybrid with the following command
   ```
   brew install ircd-hybrid
   ```
4. ircd-hybrid is ready to use now!