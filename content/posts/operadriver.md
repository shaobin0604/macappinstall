---
title: "Install OperaChromiumDriver on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Driver for Chromium-based Opera releases"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install OperaChromiumDriver on MacOS using homebrew

- App Name: OperaChromiumDriver
- App description: Driver for Chromium-based Opera releases
- App Version: 96.0.4664.45
- App Website: https://github.com/operasoftware/operachromiumdriver

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install OperaChromiumDriver with the following command
   ```
   brew install --cask operadriver
   ```
4. OperaChromiumDriver is ready to use now!