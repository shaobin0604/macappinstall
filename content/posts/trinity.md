---
title: "Install IOTA Trinity Wallet on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cryptocurrency wallet"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install IOTA Trinity Wallet on MacOS using homebrew

- App Name: IOTA Trinity Wallet
- App description: Cryptocurrency wallet
- App Version: 1.6.2
- App Website: https://trinity.iota.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install IOTA Trinity Wallet with the following command
   ```
   brew install --cask trinity
   ```
4. IOTA Trinity Wallet is ready to use now!