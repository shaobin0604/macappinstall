---
title: "Install ford on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Automatic documentation generator for modern Fortran programs"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ford on MacOS using homebrew

- App Name: ford
- App description: Automatic documentation generator for modern Fortran programs
- App Version: 6.1.8
- App Website: https://github.com/Fortran-FOSS-Programmers/ford

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ford with the following command
   ```
   brew install ford
   ```
4. ford is ready to use now!