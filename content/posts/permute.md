---
title: "Install Permute on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Converts and edits video, audio or image files"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Permute on MacOS using homebrew

- App Name: Permute
- App description: Converts and edits video, audio or image files
- App Version: 3.8.5,2572
- App Website: https://software.charliemonroe.net/permute/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Permute with the following command
   ```
   brew install --cask permute
   ```
4. Permute is ready to use now!