---
title: "Install moco on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Stub server with Maven, Gradle, Scala, and shell integration"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install moco on MacOS using homebrew

- App Name: moco
- App description: Stub server with Maven, Gradle, Scala, and shell integration
- App Version: 1.3.0
- App Website: https://github.com/dreamhead/moco

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install moco with the following command
   ```
   brew install moco
   ```
4. moco is ready to use now!