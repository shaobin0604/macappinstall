---
title: "Install rlwrap on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Readline wrapper: adds readline support to tools that lack it"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install rlwrap on MacOS using homebrew

- App Name: rlwrap
- App description: Readline wrapper: adds readline support to tools that lack it
- App Version: 0.45.2
- App Website: https://github.com/hanslub42/rlwrap

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install rlwrap with the following command
   ```
   brew install rlwrap
   ```
4. rlwrap is ready to use now!