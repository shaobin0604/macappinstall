---
title: "Install jp on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Dead simple terminal plots from JSON data"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install jp on MacOS using homebrew

- App Name: jp
- App description: Dead simple terminal plots from JSON data
- App Version: 1.1.12
- App Website: https://github.com/sgreben/jp

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install jp with the following command
   ```
   brew install jp
   ```
4. jp is ready to use now!