---
title: "Install AnyDesk on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Allows connection to a computer remotely"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install AnyDesk on MacOS using homebrew

- App Name: AnyDesk
- App description: Allows connection to a computer remotely
- App Version: 6.4.0
- App Website: https://anydesk.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install AnyDesk with the following command
   ```
   brew install --cask anydesk
   ```
4. AnyDesk is ready to use now!