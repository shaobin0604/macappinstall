---
title: "Install pdfpc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Presenter console with multi-monitor support for PDF files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pdfpc on MacOS using homebrew

- App Name: pdfpc
- App description: Presenter console with multi-monitor support for PDF files
- App Version: 4.4.1
- App Website: https://pdfpc.github.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pdfpc with the following command
   ```
   brew install pdfpc
   ```
4. pdfpc is ready to use now!