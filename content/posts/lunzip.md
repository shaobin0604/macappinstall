---
title: "Install lunzip on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Decompressor for lzip files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install lunzip on MacOS using homebrew

- App Name: lunzip
- App description: Decompressor for lzip files
- App Version: 1.13
- App Website: https://www.nongnu.org/lzip/lunzip.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install lunzip with the following command
   ```
   brew install lunzip
   ```
4. lunzip is ready to use now!