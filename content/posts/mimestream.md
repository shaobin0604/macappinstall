---
title: "Install Mimestream on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Native app email client for Gmail"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Mimestream on MacOS using homebrew

- App Name: Mimestream
- App description: Native app email client for Gmail
- App Version: 0.32.4
- App Website: https://mimestream.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Mimestream with the following command
   ```
   brew install --cask mimestream
   ```
4. Mimestream is ready to use now!