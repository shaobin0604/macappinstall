---
title: "Install scc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Fast and accurate code counter with complexity and COCOMO estimates"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install scc on MacOS using homebrew

- App Name: scc
- App description: Fast and accurate code counter with complexity and COCOMO estimates
- App Version: 3.0.0
- App Website: https://github.com/boyter/scc/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install scc with the following command
   ```
   brew install scc
   ```
4. scc is ready to use now!