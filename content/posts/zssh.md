---
title: "Install zssh on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Interactive file transfers over SSH"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install zssh on MacOS using homebrew

- App Name: zssh
- App description: Interactive file transfers over SSH
- App Version: 1.5c
- App Website: https://zssh.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install zssh with the following command
   ```
   brew install zssh
   ```
4. zssh is ready to use now!