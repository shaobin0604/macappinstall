---
title: "Install qthreads on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Lightweight locality-aware user-level threading runtime"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install qthreads on MacOS using homebrew

- App Name: qthreads
- App description: Lightweight locality-aware user-level threading runtime
- App Version: 1.17
- App Website: https://github.com/Qthreads/qthreads

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install qthreads with the following command
   ```
   brew install qthreads
   ```
4. qthreads is ready to use now!