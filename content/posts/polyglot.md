---
title: "Install polyglot on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Protocol adapter to run UCI engines under XBoard"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install polyglot on MacOS using homebrew

- App Name: polyglot
- App description: Protocol adapter to run UCI engines under XBoard
- App Version: 2.0.4
- App Website: https://www.chessprogramming.org/PolyGlot

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install polyglot with the following command
   ```
   brew install polyglot
   ```
4. polyglot is ready to use now!