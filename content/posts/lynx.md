---
title: "Install lynx on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Text-based web browser"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install lynx on MacOS using homebrew

- App Name: lynx
- App description: Text-based web browser
- App Version: 2.8.9rel.1
- App Website: https://invisible-island.net/lynx/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install lynx with the following command
   ```
   brew install lynx
   ```
4. lynx is ready to use now!