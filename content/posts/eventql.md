---
title: "Install eventql on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Database for large-scale event analytics"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install eventql on MacOS using homebrew

- App Name: eventql
- App description: Database for large-scale event analytics
- App Version: 0.4.1
- App Website: https://eventql.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install eventql with the following command
   ```
   brew install eventql
   ```
4. eventql is ready to use now!