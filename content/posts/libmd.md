---
title: "Install libmd on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "BSD Message Digest library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libmd on MacOS using homebrew

- App Name: libmd
- App description: BSD Message Digest library
- App Version: 1.0.4
- App Website: https://www.hadrons.org/software/libmd/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libmd with the following command
   ```
   brew install libmd
   ```
4. libmd is ready to use now!