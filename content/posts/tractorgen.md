---
title: "Install tractorgen on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Generates ASCII tractor art"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tractorgen on MacOS using homebrew

- App Name: tractorgen
- App description: Generates ASCII tractor art
- App Version: 0.31.7
- App Website: http://www.vergenet.net/~conrad/software/tractorgen/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tractorgen with the following command
   ```
   brew install tractorgen
   ```
4. tractorgen is ready to use now!