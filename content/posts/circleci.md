---
title: "Install circleci on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Enables you to reproduce the CircleCI environment locally"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install circleci on MacOS using homebrew

- App Name: circleci
- App description: Enables you to reproduce the CircleCI environment locally
- App Version: 0.1.16737
- App Website: https://circleci.com/docs/2.0/local-cli/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install circleci with the following command
   ```
   brew install circleci
   ```
4. circleci is ready to use now!