---
title: "Install VOX on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Music player for high resoluion (Hi-Res) music through the external sources"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install VOX on MacOS using homebrew

- App Name: VOX
- App description: Music player for high resoluion (Hi-Res) music through the external sources
- App Version: 3.4.4,3440.1
- App Website: https://vox.rocks/mac-music-player

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install VOX with the following command
   ```
   brew install --cask vox
   ```
4. VOX is ready to use now!