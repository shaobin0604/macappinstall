---
title: "Install opencbm on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Provides access to various floppy drive formats"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install opencbm on MacOS using homebrew

- App Name: opencbm
- App description: Provides access to various floppy drive formats
- App Version: 0.4.99.104
- App Website: https://spiro.trikaliotis.net/opencbm

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install opencbm with the following command
   ```
   brew install opencbm
   ```
4. opencbm is ready to use now!