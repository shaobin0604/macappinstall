---
title: "Install FreeMacSoft AppCleaner on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Application uninstaller"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install FreeMacSoft AppCleaner on MacOS using homebrew

- App Name: FreeMacSoft AppCleaner
- App description: Application uninstaller
- App Version: 3.6.3,4222
- App Website: https://freemacsoft.net/appcleaner/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install FreeMacSoft AppCleaner with the following command
   ```
   brew install --cask appcleaner
   ```
4. FreeMacSoft AppCleaner is ready to use now!