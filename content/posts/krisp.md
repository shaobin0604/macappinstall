---
title: "Install Krisp on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Sound clear in online meetings"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Krisp on MacOS using homebrew

- App Name: Krisp
- App description: Sound clear in online meetings
- App Version: 1.32.16
- App Website: https://krisp.ai/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Krisp with the following command
   ```
   brew install --cask krisp
   ```
4. Krisp is ready to use now!