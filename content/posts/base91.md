---
title: "Install base91 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utility to encode and decode base91 files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install base91 on MacOS using homebrew

- App Name: base91
- App description: Utility to encode and decode base91 files
- App Version: 0.6.0
- App Website: https://base91.sourceforge.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install base91 with the following command
   ```
   brew install base91
   ```
4. base91 is ready to use now!