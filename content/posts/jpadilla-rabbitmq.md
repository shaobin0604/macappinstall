---
title: "Install RabbitMQ on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "App wrapper for RabbitMQ"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install RabbitMQ on MacOS using homebrew

- App Name: RabbitMQ
- App description: App wrapper for RabbitMQ
- App Version: 3.6.1-build.1
- App Website: https://jpadilla.github.io/rabbitmqapp/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install RabbitMQ with the following command
   ```
   brew install --cask jpadilla-rabbitmq
   ```
4. RabbitMQ is ready to use now!