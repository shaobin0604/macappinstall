---
title: "Install python@3.8 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Interpreted, interactive, object-oriented programming language"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install python@3.8 on MacOS using homebrew

- App Name: python@3.8
- App description: Interpreted, interactive, object-oriented programming language
- App Version: 3.8.12
- App Website: https://www.python.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install python@3.8 with the following command
   ```
   brew install python@3.8
   ```
4. python@3.8 is ready to use now!