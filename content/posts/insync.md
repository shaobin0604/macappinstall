---
title: "Install Insync on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Manage your Google Drive and OneDrive files"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Insync on MacOS using homebrew

- App Name: Insync
- App description: Manage your Google Drive and OneDrive files
- App Version: 3.7.0.50216
- App Website: https://www.insynchq.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Insync with the following command
   ```
   brew install --cask insync
   ```
4. Insync is ready to use now!