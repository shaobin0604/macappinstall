---
title: "Install libxmp on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C library for playback of module music (MOD, S3M, IT, etc)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libxmp on MacOS using homebrew

- App Name: libxmp
- App description: C library for playback of module music (MOD, S3M, IT, etc)
- App Version: 4.5.0
- App Website: https://xmp.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libxmp with the following command
   ```
   brew install libxmp
   ```
4. libxmp is ready to use now!