---
title: "Install par2 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Parchive: Parity Archive Volume Set for data recovery"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install par2 on MacOS using homebrew

- App Name: par2
- App description: Parchive: Parity Archive Volume Set for data recovery
- App Version: 0.8.1
- App Website: https://github.com/Parchive/par2cmdline

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install par2 with the following command
   ```
   brew install par2
   ```
4. par2 is ready to use now!