---
title: "Install BeerSmith on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Beer brewing software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install BeerSmith on MacOS using homebrew

- App Name: BeerSmith
- App description: Beer brewing software
- App Version: 3.2.7
- App Website: https://beersmith.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install BeerSmith with the following command
   ```
   brew install --cask beersmith
   ```
4. BeerSmith is ready to use now!