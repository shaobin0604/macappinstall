---
title: "Install gtksourceviewmm on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C++ bindings for gtksourceview"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gtksourceviewmm on MacOS using homebrew

- App Name: gtksourceviewmm
- App description: C++ bindings for gtksourceview
- App Version: 2.10.3
- App Website: https://gitlab.gnome.org/GNOME/gtksourceviewmm

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gtksourceviewmm with the following command
   ```
   brew install gtksourceviewmm
   ```
4. gtksourceviewmm is ready to use now!