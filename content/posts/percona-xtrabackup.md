---
title: "Install percona-xtrabackup on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Open source hot backup tool for InnoDB and XtraDB databases"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install percona-xtrabackup on MacOS using homebrew

- App Name: percona-xtrabackup
- App description: Open source hot backup tool for InnoDB and XtraDB databases
- App Version: 8.0.25-17
- App Website: https://www.percona.com/software/mysql-database/percona-xtrabackup

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install percona-xtrabackup with the following command
   ```
   brew install percona-xtrabackup
   ```
4. percona-xtrabackup is ready to use now!