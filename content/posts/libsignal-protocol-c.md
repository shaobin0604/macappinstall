---
title: "Install libsignal-protocol-c on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Signal Protocol C Library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libsignal-protocol-c on MacOS using homebrew

- App Name: libsignal-protocol-c
- App description: Signal Protocol C Library
- App Version: 2.3.3
- App Website: https://github.com/signalapp/libsignal-protocol-c

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libsignal-protocol-c with the following command
   ```
   brew install libsignal-protocol-c
   ```
4. libsignal-protocol-c is ready to use now!