---
title: "Install ExactScan on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Doceument scanner"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ExactScan on MacOS using homebrew

- App Name: ExactScan
- App description: Doceument scanner
- App Version: 22.1
- App Website: https://exactscan.com/index.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ExactScan with the following command
   ```
   brew install --cask exactscan
   ```
4. ExactScan is ready to use now!