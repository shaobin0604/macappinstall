---
title: "Install di on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Advanced df-like disk information utility"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install di on MacOS using homebrew

- App Name: di
- App description: Advanced df-like disk information utility
- App Version: 4.51
- App Website: https://gentoo.com/di/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install di with the following command
   ```
   brew install di
   ```
4. di is ready to use now!