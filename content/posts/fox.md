---
title: "Install fox on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Toolkit for developing Graphical User Interfaces easily"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fox on MacOS using homebrew

- App Name: fox
- App description: Toolkit for developing Graphical User Interfaces easily
- App Version: 1.6.56
- App Website: http://fox-toolkit.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fox with the following command
   ```
   brew install fox
   ```
4. fox is ready to use now!