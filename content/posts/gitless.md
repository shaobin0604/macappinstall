---
title: "Install gitless on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Simplified version control system on top of git"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gitless on MacOS using homebrew

- App Name: gitless
- App description: Simplified version control system on top of git
- App Version: 0.8.8
- App Website: https://gitless.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gitless with the following command
   ```
   brew install gitless
   ```
4. gitless is ready to use now!