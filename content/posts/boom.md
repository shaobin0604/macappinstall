---
title: "Install Boom on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Transforms audio input"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Boom on MacOS using homebrew

- App Name: Boom
- App description: Transforms audio input
- App Version: 1.7.1,101.7.1039
- App Website: https://www.globaldelight.com/boom2/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Boom with the following command
   ```
   brew install --cask boom
   ```
4. Boom is ready to use now!