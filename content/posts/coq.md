---
title: "Install coq on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Proof assistant for higher-order logic"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install coq on MacOS using homebrew

- App Name: coq
- App description: Proof assistant for higher-order logic
- App Version: 8.15.0
- App Website: https://coq.inria.fr/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install coq with the following command
   ```
   brew install coq
   ```
4. coq is ready to use now!