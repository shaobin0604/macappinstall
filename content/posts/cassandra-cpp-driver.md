---
title: "Install cassandra-cpp-driver on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "DataStax C/C++ Driver for Apache Cassandra"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cassandra-cpp-driver on MacOS using homebrew

- App Name: cassandra-cpp-driver
- App description: DataStax C/C++ Driver for Apache Cassandra
- App Version: 2.16.2
- App Website: https://docs.datastax.com/en/developer/cpp-driver/latest

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cassandra-cpp-driver with the following command
   ```
   brew install cassandra-cpp-driver
   ```
4. cassandra-cpp-driver is ready to use now!