---
title: "Install gtksourceviewmm3 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C++ bindings for gtksourceview3"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gtksourceviewmm3 on MacOS using homebrew

- App Name: gtksourceviewmm3
- App description: C++ bindings for gtksourceview3
- App Version: 3.18.0
- App Website: https://gitlab.gnome.org/GNOME/gtksourceviewmm

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gtksourceviewmm3 with the following command
   ```
   brew install gtksourceviewmm3
   ```
4. gtksourceviewmm3 is ready to use now!