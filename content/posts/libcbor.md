---
title: "Install libcbor on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CBOR protocol implementation for C and others"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libcbor on MacOS using homebrew

- App Name: libcbor
- App description: CBOR protocol implementation for C and others
- App Version: 0.9.0
- App Website: https://github.com/PJK/libcbor

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libcbor with the following command
   ```
   brew install libcbor
   ```
4. libcbor is ready to use now!