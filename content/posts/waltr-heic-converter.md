---
title: "Install WALTR HEIC Converter on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Drag-and-drop HEIC to JPEG image converter"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install WALTR HEIC Converter on MacOS using homebrew

- App Name: WALTR HEIC Converter
- App description: Drag-and-drop HEIC to JPEG image converter
- App Version: 1.0.2,1537972843
- App Website: https://softorino.com/heic-converter/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install WALTR HEIC Converter with the following command
   ```
   brew install --cask waltr-heic-converter
   ```
4. WALTR HEIC Converter is ready to use now!