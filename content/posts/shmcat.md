---
title: "Install shmcat on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool that dumps shared memory segments (System V and POSIX)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install shmcat on MacOS using homebrew

- App Name: shmcat
- App description: Tool that dumps shared memory segments (System V and POSIX)
- App Version: 1.9
- App Website: https://shmcat.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install shmcat with the following command
   ```
   brew install shmcat
   ```
4. shmcat is ready to use now!