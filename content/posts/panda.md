---
title: "Install Panda on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utility to switch from light to dark mode"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Panda on MacOS using homebrew

- App Name: Panda
- App description: Utility to switch from light to dark mode
- App Version: 1.4.2
- App Website: https://github.com/pablosproject/Panda-Mac-app

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Panda with the following command
   ```
   brew install --cask panda
   ```
4. Panda is ready to use now!