---
title: "Install bookloupe on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "List common formatting errors in a Project Gutenberg candidate file"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bookloupe on MacOS using homebrew

- App Name: bookloupe
- App description: List common formatting errors in a Project Gutenberg candidate file
- App Version: 2.0
- App Website: http://www.juiblex.co.uk/pgdp/bookloupe/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bookloupe with the following command
   ```
   brew install bookloupe
   ```
4. bookloupe is ready to use now!