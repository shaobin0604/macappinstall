---
title: "Install pg_top on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Monitor PostgreSQL processes"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pg_top on MacOS using homebrew

- App Name: pg_top
- App description: Monitor PostgreSQL processes
- App Version: 3.7.0
- App Website: https://pg_top.gitlab.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pg_top with the following command
   ```
   brew install pg_top
   ```
4. pg_top is ready to use now!