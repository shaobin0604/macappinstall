---
title: "Install webalizer on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Web server log file analysis"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install webalizer on MacOS using homebrew

- App Name: webalizer
- App description: Web server log file analysis
- App Version: 2.23-08
- App Website: https://web.archive.org/web/20200622121953/www.webalizer.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install webalizer with the following command
   ```
   brew install webalizer
   ```
4. webalizer is ready to use now!