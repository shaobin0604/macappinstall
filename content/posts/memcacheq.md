---
title: "Install memcacheq on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Queue service for memcache"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install memcacheq on MacOS using homebrew

- App Name: memcacheq
- App description: Queue service for memcache
- App Version: 0.2.0
- App Website: https://code.google.com/archive/p/memcacheq/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install memcacheq with the following command
   ```
   brew install memcacheq
   ```
4. memcacheq is ready to use now!