---
title: "Install Fing Desktop Embedded CLI on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Network scanner"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Fing Desktop Embedded CLI on MacOS using homebrew

- App Name: Fing Desktop Embedded CLI
- App description: Network scanner
- App Version: 5.5.2
- App Website: https://www.fing.com/products/development-toolkit/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Fing Desktop Embedded CLI with the following command
   ```
   brew install --cask fing-cli
   ```
4. Fing Desktop Embedded CLI is ready to use now!