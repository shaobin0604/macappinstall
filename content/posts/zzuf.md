---
title: "Install zzuf on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Transparent application input fuzzer"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install zzuf on MacOS using homebrew

- App Name: zzuf
- App description: Transparent application input fuzzer
- App Version: 0.15
- App Website: http://caca.zoy.org/wiki/zzuf

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install zzuf with the following command
   ```
   brew install zzuf
   ```
4. zzuf is ready to use now!