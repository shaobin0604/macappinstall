---
title: "Install jgrasp on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "IDE with visualizations for improving software comprehensibility"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install jgrasp on MacOS using homebrew

- App Name: jgrasp
- App description: IDE with visualizations for improving software comprehensibility
- App Version: 2.0.6_09
- App Website: https://jgrasp.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install jgrasp with the following command
   ```
   brew install --cask jgrasp
   ```
4. jgrasp is ready to use now!