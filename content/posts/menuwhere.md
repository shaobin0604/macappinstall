---
title: "Install Menuwhere on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Access the menu from anywhere"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Menuwhere on MacOS using homebrew

- App Name: Menuwhere
- App description: Access the menu from anywhere
- App Version: 2.2,30
- App Website: https://manytricks.com/menuwhere/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Menuwhere with the following command
   ```
   brew install --cask menuwhere
   ```
4. Menuwhere is ready to use now!