---
title: "Install Marvel on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Prototyping, testing and handoff tools"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Marvel on MacOS using homebrew

- App Name: Marvel
- App description: Prototyping, testing and handoff tools
- App Version: 10.9.7,5072
- App Website: https://marvelapp.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Marvel with the following command
   ```
   brew install --cask marvel
   ```
4. Marvel is ready to use now!