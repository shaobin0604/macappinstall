---
title: "Install xcb-util-image on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "XCB port of Xlib's XImage and XShmImage"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install xcb-util-image on MacOS using homebrew

- App Name: xcb-util-image
- App description: XCB port of Xlib's XImage and XShmImage
- App Version: 0.4.0
- App Website: https://xcb.freedesktop.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install xcb-util-image with the following command
   ```
   brew install xcb-util-image
   ```
4. xcb-util-image is ready to use now!