---
title: "Install PiP on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Always on top window preview"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install PiP on MacOS using homebrew

- App Name: PiP
- App description: Always on top window preview
- App Version: 1.40
- App Website: https://github.com/amitv87/PiP

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install PiP with the following command
   ```
   brew install --cask amitv87-pip
   ```
4. PiP is ready to use now!