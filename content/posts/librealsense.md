---
title: "Install librealsense on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Intel RealSense D400 series and SR300 capture"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install librealsense on MacOS using homebrew

- App Name: librealsense
- App description: Intel RealSense D400 series and SR300 capture
- App Version: 2.50.0
- App Website: https://github.com/IntelRealSense/librealsense

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install librealsense with the following command
   ```
   brew install librealsense
   ```
4. librealsense is ready to use now!