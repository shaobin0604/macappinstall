---
title: "Install serverless on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Build applications with serverless architectures"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install serverless on MacOS using homebrew

- App Name: serverless
- App description: Build applications with serverless architectures
- App Version: 3.2.1
- App Website: https://www.serverless.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install serverless with the following command
   ```
   brew install serverless
   ```
4. serverless is ready to use now!