---
title: "Install quilt on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Work with series of patches"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install quilt on MacOS using homebrew

- App Name: quilt
- App description: Work with series of patches
- App Version: 0.67
- App Website: https://savannah.nongnu.org/projects/quilt

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install quilt with the following command
   ```
   brew install quilt
   ```
4. quilt is ready to use now!