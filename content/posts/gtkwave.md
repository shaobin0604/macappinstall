---
title: "Install GTKWave on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GTK+ based wave viewer"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install GTKWave on MacOS using homebrew

- App Name: GTKWave
- App description: GTK+ based wave viewer
- App Version: 3.3.107
- App Website: https://gtkwave.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install GTKWave with the following command
   ```
   brew install --cask gtkwave
   ```
4. GTKWave is ready to use now!