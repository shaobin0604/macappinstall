---
title: "Install apache-arrow-glib on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GLib bindings for Apache Arrow"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install apache-arrow-glib on MacOS using homebrew

- App Name: apache-arrow-glib
- App description: GLib bindings for Apache Arrow
- App Version: 7.0.0
- App Website: https://arrow.apache.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install apache-arrow-glib with the following command
   ```
   brew install apache-arrow-glib
   ```
4. apache-arrow-glib is ready to use now!