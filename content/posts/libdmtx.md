---
title: "Install libdmtx on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Data Matrix library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libdmtx on MacOS using homebrew

- App Name: libdmtx
- App description: Data Matrix library
- App Version: 0.7.5
- App Website: https://libdmtx.sourceforge.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libdmtx with the following command
   ```
   brew install libdmtx
   ```
4. libdmtx is ready to use now!