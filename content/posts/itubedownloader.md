---
title: "Install iTubeDownloader on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Download YouTube videos, channels, or playlists"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install iTubeDownloader on MacOS using homebrew

- App Name: iTubeDownloader
- App description: Download YouTube videos, channels, or playlists
- App Version: 6.6.0,66000
- App Website: https://alphasoftware.co/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install iTubeDownloader with the following command
   ```
   brew install --cask itubedownloader
   ```
4. iTubeDownloader is ready to use now!