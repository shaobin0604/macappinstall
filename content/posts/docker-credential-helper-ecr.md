---
title: "Install docker-credential-helper-ecr on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Docker Credential Helper for Amazon ECR"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install docker-credential-helper-ecr on MacOS using homebrew

- App Name: docker-credential-helper-ecr
- App description: Docker Credential Helper for Amazon ECR
- App Version: 0.6.0
- App Website: https://github.com/awslabs/amazon-ecr-credential-helper

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install docker-credential-helper-ecr with the following command
   ```
   brew install docker-credential-helper-ecr
   ```
4. docker-credential-helper-ecr is ready to use now!