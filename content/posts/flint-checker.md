---
title: "Install flint-checker on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Check your project for common sources of contributor friction"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install flint-checker on MacOS using homebrew

- App Name: flint-checker
- App description: Check your project for common sources of contributor friction
- App Version: 0.1.0
- App Website: https://github.com/pengwynn/flint

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install flint-checker with the following command
   ```
   brew install flint-checker
   ```
4. flint-checker is ready to use now!