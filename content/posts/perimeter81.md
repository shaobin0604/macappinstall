---
title: "Install Perimeter 81 on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Zero trust network as a service client"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Perimeter 81 on MacOS using homebrew

- App Name: Perimeter 81
- App description: Zero trust network as a service client
- App Version: 7.0.0
- App Website: https://perimeter81.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Perimeter 81 with the following command
   ```
   brew install --cask perimeter81
   ```
4. Perimeter 81 is ready to use now!