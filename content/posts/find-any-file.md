---
title: "Install Find Any File on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "File finder"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Find Any File on MacOS using homebrew

- App Name: Find Any File
- App description: File finder
- App Version: 2.3.2
- App Website: https://apps.tempel.org/FindAnyFile/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Find Any File with the following command
   ```
   brew install --cask find-any-file
   ```
4. Find Any File is ready to use now!