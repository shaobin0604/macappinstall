---
title: "Install Eclipse SDK on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "SDK for the Eclipse IDE"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Eclipse SDK on MacOS using homebrew

- App Name: Eclipse SDK
- App description: SDK for the Eclipse IDE
- App Version: 4.22,202111241800
- App Website: https://eclipse.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Eclipse SDK with the following command
   ```
   brew install --cask eclipse-platform
   ```
4. Eclipse SDK is ready to use now!