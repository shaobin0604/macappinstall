---
title: "Install SaveHollywood Screensaver on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Screen saver for custom video files"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install SaveHollywood Screensaver on MacOS using homebrew

- App Name: SaveHollywood Screensaver
- App description: Screen saver for custom video files
- App Version: 2.5
- App Website: http://s.sudre.free.fr/Software/SaveHollywood/about.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install SaveHollywood Screensaver with the following command
   ```
   brew install --cask save-hollywood
   ```
4. SaveHollywood Screensaver is ready to use now!