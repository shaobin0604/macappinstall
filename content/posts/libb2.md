---
title: "Install libb2 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Secure hashing function"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libb2 on MacOS using homebrew

- App Name: libb2
- App description: Secure hashing function
- App Version: 0.98.1
- App Website: https://blake2.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libb2 with the following command
   ```
   brew install libb2
   ```
4. libb2 is ready to use now!