---
title: "Install pachi on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Software for the Board Game of Go/Weiqi/Baduk"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pachi on MacOS using homebrew

- App Name: pachi
- App description: Software for the Board Game of Go/Weiqi/Baduk
- App Version: 12.60
- App Website: https://pachi.or.cz/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pachi with the following command
   ```
   brew install pachi
   ```
4. pachi is ready to use now!