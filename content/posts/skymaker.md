---
title: "Install skymaker on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Generates fake astronomical images"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install skymaker on MacOS using homebrew

- App Name: skymaker
- App description: Generates fake astronomical images
- App Version: 3.10.5
- App Website: https://www.astromatic.net/software/skymaker

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install skymaker with the following command
   ```
   brew install skymaker
   ```
4. skymaker is ready to use now!