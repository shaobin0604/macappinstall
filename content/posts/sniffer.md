---
title: "Install sniffer on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Modern alternative network traffic sniffer"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sniffer on MacOS using homebrew

- App Name: sniffer
- App description: Modern alternative network traffic sniffer
- App Version: 0.6.0
- App Website: https://github.com/chenjiandongx/sniffer

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sniffer with the following command
   ```
   brew install sniffer
   ```
4. sniffer is ready to use now!