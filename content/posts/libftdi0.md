---
title: "Install libftdi0 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library to talk to FTDI chips"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libftdi0 on MacOS using homebrew

- App Name: libftdi0
- App description: Library to talk to FTDI chips
- App Version: 0.20
- App Website: https://www.intra2net.com/en/developer/libftdi

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libftdi0 with the following command
   ```
   brew install libftdi0
   ```
4. libftdi0 is ready to use now!