---
title: "Install Plex Media Server on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Home media server"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Plex Media Server on MacOS using homebrew

- App Name: Plex Media Server
- App description: Home media server
- App Version: 1.25.5.5492,12f6b8c83
- App Website: https://www.plex.tv/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Plex Media Server with the following command
   ```
   brew install --cask plex-media-server
   ```
4. Plex Media Server is ready to use now!