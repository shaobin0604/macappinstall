---
title: "Install kotlin-language-server on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Intelligent Kotlin support for any editor/IDE using the Language Server Protocol"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install kotlin-language-server on MacOS using homebrew

- App Name: kotlin-language-server
- App description: Intelligent Kotlin support for any editor/IDE using the Language Server Protocol
- App Version: 1.2.0
- App Website: https://github.com/fwcd/kotlin-language-server

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install kotlin-language-server with the following command
   ```
   brew install kotlin-language-server
   ```
4. kotlin-language-server is ready to use now!