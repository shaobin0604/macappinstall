---
title: "Install wiggle on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Program for applying patches with conflicting changes"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install wiggle on MacOS using homebrew

- App Name: wiggle
- App description: Program for applying patches with conflicting changes
- App Version: 1.3
- App Website: https://github.com/neilbrown/wiggle

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install wiggle with the following command
   ```
   brew install wiggle
   ```
4. wiggle is ready to use now!