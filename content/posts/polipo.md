---
title: "Install polipo on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Web caching proxy"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install polipo on MacOS using homebrew

- App Name: polipo
- App description: Web caching proxy
- App Version: 1.1.1
- App Website: https://www.irif.univ-paris-diderot.fr/~jch/software/polipo/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install polipo with the following command
   ```
   brew install polipo
   ```
4. polipo is ready to use now!