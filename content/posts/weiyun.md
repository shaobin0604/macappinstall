---
title: "Install Weiyun on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Document backup and online management"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Weiyun on MacOS using homebrew

- App Name: Weiyun
- App description: Document backup and online management
- App Version: 5.2.1184
- App Website: https://www.weiyun.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Weiyun with the following command
   ```
   brew install --cask weiyun
   ```
4. Weiyun is ready to use now!