---
title: "Install terminal-notifier on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Send macOS User Notifications from the command-line"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install terminal-notifier on MacOS using homebrew

- App Name: terminal-notifier
- App description: Send macOS User Notifications from the command-line
- App Version: 2.0.0
- App Website: https://github.com/julienXX/terminal-notifier

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install terminal-notifier with the following command
   ```
   brew install terminal-notifier
   ```
4. terminal-notifier is ready to use now!