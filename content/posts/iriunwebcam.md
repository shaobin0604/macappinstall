---
title: "Install Iriun on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Use your phone's camera as a wireless webcam"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Iriun on MacOS using homebrew

- App Name: Iriun
- App description: Use your phone's camera as a wireless webcam
- App Version: 2.7.2
- App Website: https://iriun.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Iriun with the following command
   ```
   brew install --cask iriunwebcam
   ```
4. Iriun is ready to use now!