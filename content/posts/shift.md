---
title: "Install Shift on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Workstation to streamline your accounts, apps, and workflows"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Shift on MacOS using homebrew

- App Name: Shift
- App description: Workstation to streamline your accounts, apps, and workflows
- App Version: 7.2.10
- App Website: https://tryshift.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Shift with the following command
   ```
   brew install --cask shift
   ```
4. Shift is ready to use now!