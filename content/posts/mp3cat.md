---
title: "Install mp3cat on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Reads and writes mp3 files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mp3cat on MacOS using homebrew

- App Name: mp3cat
- App description: Reads and writes mp3 files
- App Version: 0.5
- App Website: https://tomclegg.ca/mp3cat

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mp3cat with the following command
   ```
   brew install mp3cat
   ```
4. mp3cat is ready to use now!