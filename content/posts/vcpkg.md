---
title: "Install vcpkg on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C++ Library Manager"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install vcpkg on MacOS using homebrew

- App Name: vcpkg
- App description: C++ Library Manager
- App Version: 2022.02.01
- App Website: https://github.com/microsoft/vcpkg

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install vcpkg with the following command
   ```
   brew install vcpkg
   ```
4. vcpkg is ready to use now!