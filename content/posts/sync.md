---
title: "Install Sync on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Store, share and access files from anywhere"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Sync on MacOS using homebrew

- App Name: Sync
- App description: Store, share and access files from anywhere
- App Version: 2.0.19
- App Website: https://www.sync.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Sync with the following command
   ```
   brew install --cask sync
   ```
4. Sync is ready to use now!