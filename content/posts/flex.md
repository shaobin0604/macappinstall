---
title: "Install flex on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Fast Lexical Analyzer, generates Scanners (tokenizers)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install flex on MacOS using homebrew

- App Name: flex
- App description: Fast Lexical Analyzer, generates Scanners (tokenizers)
- App Version: 2.6.4
- App Website: https://github.com/westes/flex

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install flex with the following command
   ```
   brew install flex
   ```
4. flex is ready to use now!