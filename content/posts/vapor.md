---
title: "Install vapor on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line tool for Vapor (Server-side Swift web framework)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install vapor on MacOS using homebrew

- App Name: vapor
- App description: Command-line tool for Vapor (Server-side Swift web framework)
- App Version: 18.3.3
- App Website: https://vapor.codes

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install vapor with the following command
   ```
   brew install vapor
   ```
4. vapor is ready to use now!