---
title: "Install VirtualGL on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "3D without boundaries"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install VirtualGL on MacOS using homebrew

- App Name: VirtualGL
- App description: 3D without boundaries
- App Version: 3.0
- App Website: https://www.virtualgl.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install VirtualGL with the following command
   ```
   brew install --cask virtualgl
   ```
4. VirtualGL is ready to use now!