---
title: "Install pwsafe on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Generate passwords and manage encrypted password databases"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pwsafe on MacOS using homebrew

- App Name: pwsafe
- App description: Generate passwords and manage encrypted password databases
- App Version: 0.2.0
- App Website: https://github.com/nsd20463/pwsafe

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pwsafe with the following command
   ```
   brew install pwsafe
   ```
4. pwsafe is ready to use now!