---
title: "Install Xtreme Download Manager on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to increase download speed"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Xtreme Download Manager on MacOS using homebrew

- App Name: Xtreme Download Manager
- App description: Tool to increase download speed
- App Version: 7.2.7
- App Website: https://xtremedownloadmanager.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Xtreme Download Manager with the following command
   ```
   brew install --cask xdm
   ```
4. Xtreme Download Manager is ready to use now!