---
title: "Install Toggl Track on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Time tracker"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Toggl Track on MacOS using homebrew

- App Name: Toggl Track
- App description: Time tracker
- App Version: 7.5.441
- App Website: https://www.toggl.com/track/toggl-desktop/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Toggl Track with the following command
   ```
   brew install --cask toggl-track
   ```
4. Toggl Track is ready to use now!