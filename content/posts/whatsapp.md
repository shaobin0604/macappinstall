---
title: "Install WhatsApp on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Desktop client for WhatsApp"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install WhatsApp on MacOS using homebrew

- App Name: WhatsApp
- App description: Desktop client for WhatsApp
- App Version: 2.2202.12
- App Website: https://www.whatsapp.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install WhatsApp with the following command
   ```
   brew install --cask whatsapp
   ```
4. WhatsApp is ready to use now!