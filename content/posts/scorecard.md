---
title: "Install scorecard on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Security health metrics for Open Source"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install scorecard on MacOS using homebrew

- App Name: scorecard
- App description: Security health metrics for Open Source
- App Version: 4.0.1
- App Website: https://github.com/ossf/scorecard

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install scorecard with the following command
   ```
   brew install scorecard
   ```
4. scorecard is ready to use now!