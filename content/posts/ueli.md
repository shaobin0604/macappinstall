---
title: "Install Ueli on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Keystroke launcher"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Ueli on MacOS using homebrew

- App Name: Ueli
- App description: Keystroke launcher
- App Version: 8.19.0
- App Website: https://ueli.app/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Ueli with the following command
   ```
   brew install --cask ueli
   ```
4. Ueli is ready to use now!