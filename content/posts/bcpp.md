---
title: "Install bcpp on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C(++) beautifier"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bcpp on MacOS using homebrew

- App Name: bcpp
- App description: C(++) beautifier
- App Version: 20210108
- App Website: https://invisible-island.net/bcpp/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bcpp with the following command
   ```
   brew install bcpp
   ```
4. bcpp is ready to use now!