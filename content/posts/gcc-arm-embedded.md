---
title: "Install GCC ARM Embedded on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Pre-built GNU bare-metal toolchain for 32-bit Arm processors"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install GCC ARM Embedded on MacOS using homebrew

- App Name: GCC ARM Embedded
- App description: Pre-built GNU bare-metal toolchain for 32-bit Arm processors
- App Version: 10.3-2021.10
- App Website: https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install GCC ARM Embedded with the following command
   ```
   brew install --cask gcc-arm-embedded
   ```
4. GCC ARM Embedded is ready to use now!