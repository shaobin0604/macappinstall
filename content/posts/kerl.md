---
title: "Install kerl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Easy building and installing of Erlang/OTP instances"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install kerl on MacOS using homebrew

- App Name: kerl
- App description: Easy building and installing of Erlang/OTP instances
- App Version: 2.2.4
- App Website: https://github.com/kerl/kerl

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install kerl with the following command
   ```
   brew install kerl
   ```
4. kerl is ready to use now!