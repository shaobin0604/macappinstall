---
title: "Install LinkLiar on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Link-Layer MAC spoofing GUI for macOS"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install LinkLiar on MacOS using homebrew

- App Name: LinkLiar
- App description: Link-Layer MAC spoofing GUI for macOS
- App Version: 3.0.3
- App Website: https://github.com/halo/LinkLiar

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install LinkLiar with the following command
   ```
   brew install --cask linkliar
   ```
4. LinkLiar is ready to use now!