---
title: "Install Vivaldi on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Web browser focusing on customization and control"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Vivaldi on MacOS using homebrew

- App Name: Vivaldi
- App description: Web browser focusing on customization and control
- App Version: 5.1.2567.49
- App Website: https://vivaldi.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Vivaldi with the following command
   ```
   brew install --cask vivaldi
   ```
4. Vivaldi is ready to use now!