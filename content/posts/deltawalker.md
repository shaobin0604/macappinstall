---
title: "Install DeltaWalker on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to compare and synchronize files and folders"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install DeltaWalker on MacOS using homebrew

- App Name: DeltaWalker
- App description: Tool to compare and synchronize files and folders
- App Version: 2.6.3
- App Website: http://www.deltawalker.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install DeltaWalker with the following command
   ```
   brew install --cask deltawalker
   ```
4. DeltaWalker is ready to use now!