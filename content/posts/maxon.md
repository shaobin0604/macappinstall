---
title: "Install Maxon App on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Install, use, and try Maxon products"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Maxon App on MacOS using homebrew

- App Name: Maxon App
- App description: Install, use, and try Maxon products
- App Version: 2.1.0,5
- App Website: https://www.maxon.net/en/downloads/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Maxon App with the following command
   ```
   brew install --cask maxon
   ```
4. Maxon App is ready to use now!