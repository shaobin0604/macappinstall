---
title: "Install advancemenu on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Frontend for AdvanceMAME/MESS"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install advancemenu on MacOS using homebrew

- App Name: advancemenu
- App description: Frontend for AdvanceMAME/MESS
- App Version: 3.9
- App Website: https://www.advancemame.it/menu-readme.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install advancemenu with the following command
   ```
   brew install advancemenu
   ```
4. advancemenu is ready to use now!