---
title: "Install DMM Player for Chrome on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Play videos from DMM in Chrome"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install DMM Player for Chrome on MacOS using homebrew

- App Name: DMM Player for Chrome
- App description: Play videos from DMM in Chrome
- App Version: 1.5.0.10
- App Website: https://www.dmm.com/digital/info_for_chrome_user_html/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install DMM Player for Chrome with the following command
   ```
   brew install --cask dmm-player-for-chrome
   ```
4. DMM Player for Chrome is ready to use now!