---
title: "Install Kaleidoscope on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Spot and merge differences in text and image files or folders"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Kaleidoscope on MacOS using homebrew

- App Name: Kaleidoscope
- App description: Spot and merge differences in text and image files or folders
- App Version: 3.2.2,2042
- App Website: https://www.kaleidoscope.app/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Kaleidoscope with the following command
   ```
   brew install --cask kaleidoscope
   ```
4. Kaleidoscope is ready to use now!