---
title: "Install Azul Zulu Java Standard Edition Development Kit on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "OpenJDK distribution from Azul"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Azul Zulu Java Standard Edition Development Kit on MacOS using homebrew

- App Name: Azul Zulu Java Standard Edition Development Kit
- App description: OpenJDK distribution from Azul
- App Version: 17.0.2,17.32.13-ca
- App Website: https://www.azul.com/downloads/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Azul Zulu Java Standard Edition Development Kit with the following command
   ```
   brew install --cask zulu
   ```
4. Azul Zulu Java Standard Edition Development Kit is ready to use now!