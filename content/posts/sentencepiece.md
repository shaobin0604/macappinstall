---
title: "Install sentencepiece on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Unsupervised text tokenizer and detokenizer"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sentencepiece on MacOS using homebrew

- App Name: sentencepiece
- App description: Unsupervised text tokenizer and detokenizer
- App Version: 0.1.96
- App Website: https://github.com/google/sentencepiece

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sentencepiece with the following command
   ```
   brew install sentencepiece
   ```
4. sentencepiece is ready to use now!