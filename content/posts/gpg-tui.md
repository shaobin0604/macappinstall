---
title: "Install gpg-tui on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Manage your GnuPG keys with ease! 🔐"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gpg-tui on MacOS using homebrew

- App Name: gpg-tui
- App description: Manage your GnuPG keys with ease! 🔐
- App Version: 0.8.2
- App Website: https://github.com/orhun/gpg-tui

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gpg-tui with the following command
   ```
   brew install gpg-tui
   ```
4. gpg-tui is ready to use now!