---
title: "Install x11vnc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "VNC server for real X displays"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install x11vnc on MacOS using homebrew

- App Name: x11vnc
- App description: VNC server for real X displays
- App Version: 0.9.16
- App Website: https://github.com/LibVNC/x11vnc

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install x11vnc with the following command
   ```
   brew install x11vnc
   ```
4. x11vnc is ready to use now!