---
title: "Install archey4 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Simple system information tool written in Python"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install archey4 on MacOS using homebrew

- App Name: archey4
- App description: Simple system information tool written in Python
- App Version: 4.13.3
- App Website: https://github.com/HorlogeSkynet/archey4

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install archey4 with the following command
   ```
   brew install archey4
   ```
4. archey4 is ready to use now!