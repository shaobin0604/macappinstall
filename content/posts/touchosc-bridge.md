---
title: "Install TouchOSC Bridge on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Modular touch control surface bridge for OSC & MIDI"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install TouchOSC Bridge on MacOS using homebrew

- App Name: TouchOSC Bridge
- App description: Modular touch control surface bridge for OSC & MIDI
- App Version: 1.7.2.147
- App Website: https://hexler.net/touchosc

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install TouchOSC Bridge with the following command
   ```
   brew install --cask touchosc-bridge
   ```
4. TouchOSC Bridge is ready to use now!