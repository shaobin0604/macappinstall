---
title: "Install corral on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Dependency manager for the Pony language"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install corral on MacOS using homebrew

- App Name: corral
- App description: Dependency manager for the Pony language
- App Version: 0.5.5
- App Website: https://github.com/ponylang/corral

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install corral with the following command
   ```
   brew install corral
   ```
4. corral is ready to use now!