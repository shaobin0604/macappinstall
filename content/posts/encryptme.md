---
title: "Install EncryptMe on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "VPN and encryption software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install EncryptMe on MacOS using homebrew

- App Name: EncryptMe
- App description: VPN and encryption software
- App Version: 4.3.0,52218
- App Website: https://encrypt.me/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install EncryptMe with the following command
   ```
   brew install --cask encryptme
   ```
4. EncryptMe is ready to use now!