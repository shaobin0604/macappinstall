---
title: "Install file-formula on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utility to determine file types"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install file-formula on MacOS using homebrew

- App Name: file-formula
- App description: Utility to determine file types
- App Version: 5.41
- App Website: https://darwinsys.com/file/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install file-formula with the following command
   ```
   brew install file-formula
   ```
4. file-formula is ready to use now!