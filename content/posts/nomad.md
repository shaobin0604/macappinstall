---
title: "Install nomad on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Distributed, Highly Available, Datacenter-Aware Scheduler"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install nomad on MacOS using homebrew

- App Name: nomad
- App description: Distributed, Highly Available, Datacenter-Aware Scheduler
- App Version: 1.2.6
- App Website: https://www.nomadproject.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install nomad with the following command
   ```
   brew install nomad
   ```
4. nomad is ready to use now!