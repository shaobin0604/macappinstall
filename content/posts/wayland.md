---
title: "Install wayland on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Protocol for a compositor to talk to its clients"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install wayland on MacOS using homebrew

- App Name: wayland
- App description: Protocol for a compositor to talk to its clients
- App Version: 1.20.0
- App Website: https://wayland.freedesktop.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install wayland with the following command
   ```
   brew install wayland
   ```
4. wayland is ready to use now!