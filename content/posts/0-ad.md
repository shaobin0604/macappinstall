---
title: "Install 0 A.D. on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Real-time strategy game"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install 0 A.D. on MacOS using homebrew

- App Name: 0 A.D.
- App description: Real-time strategy game
- App Version: 0.0.25b-alpha
- App Website: https://play0ad.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install 0 A.D. with the following command
   ```
   brew install --cask 0-ad
   ```
4. 0 A.D. is ready to use now!