---
title: "Install dc3dd on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Patched GNU dd that is intended for forensic acquisition of data"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dc3dd on MacOS using homebrew

- App Name: dc3dd
- App description: Patched GNU dd that is intended for forensic acquisition of data
- App Version: 7.2.646
- App Website: https://sourceforge.net/projects/dc3dd/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dc3dd with the following command
   ```
   brew install dc3dd
   ```
4. dc3dd is ready to use now!