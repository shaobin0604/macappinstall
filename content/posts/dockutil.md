---
title: "Install dockutil on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool for managing dock items"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dockutil on MacOS using homebrew

- App Name: dockutil
- App description: Tool for managing dock items
- App Version: 2.0.5
- App Website: https://github.com/kcrawford/dockutil

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dockutil with the following command
   ```
   brew install dockutil
   ```
4. dockutil is ready to use now!