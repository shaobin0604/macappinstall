---
title: "Install Rotato on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Mockup generator & animator 3D"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Rotato on MacOS using homebrew

- App Name: Rotato
- App description: Mockup generator & animator 3D
- App Version: 112,1604697138
- App Website: https://rotato.app/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Rotato with the following command
   ```
   brew install --cask rotato
   ```
4. Rotato is ready to use now!