---
title: "Install React Native Debugger on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Standalone app for debugging React Native apps"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install React Native Debugger on MacOS using homebrew

- App Name: React Native Debugger
- App description: Standalone app for debugging React Native apps
- App Version: 0.12.1
- App Website: https://github.com/jhen0409/react-native-debugger

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install React Native Debugger with the following command
   ```
   brew install --cask react-native-debugger
   ```
4. React Native Debugger is ready to use now!