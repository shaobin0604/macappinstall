---
title: "Install argus-clients on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Audit Record Generation and Utilization System clients"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install argus-clients on MacOS using homebrew

- App Name: argus-clients
- App description: Audit Record Generation and Utilization System clients
- App Version: 3.0.8.2
- App Website: https://openargus.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install argus-clients with the following command
   ```
   brew install argus-clients
   ```
4. argus-clients is ready to use now!