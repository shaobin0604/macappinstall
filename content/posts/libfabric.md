---
title: "Install libfabric on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "OpenFabrics libfabric"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libfabric on MacOS using homebrew

- App Name: libfabric
- App description: OpenFabrics libfabric
- App Version: 1.14.0
- App Website: https://ofiwg.github.io/libfabric/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libfabric with the following command
   ```
   brew install libfabric
   ```
4. libfabric is ready to use now!