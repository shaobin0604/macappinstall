---
title: "Install git-svn-abandon on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "History-preserving svn-to-git migration"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install git-svn-abandon on MacOS using homebrew

- App Name: git-svn-abandon
- App description: History-preserving svn-to-git migration
- App Version: 0.0.1
- App Website: https://github.com/nothingmuch/git-svn-abandon

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install git-svn-abandon with the following command
   ```
   brew install git-svn-abandon
   ```
4. git-svn-abandon is ready to use now!