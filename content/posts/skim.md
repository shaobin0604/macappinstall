---
title: "Install Skim on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "PDF reader and note-taking application"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Skim on MacOS using homebrew

- App Name: Skim
- App description: PDF reader and note-taking application
- App Version: 1.6.8,138
- App Website: https://skim-app.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Skim with the following command
   ```
   brew install --cask skim
   ```
4. Skim is ready to use now!