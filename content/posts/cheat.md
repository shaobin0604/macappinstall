---
title: "Install cheat on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Create and view interactive cheat sheets for *nix commands"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cheat on MacOS using homebrew

- App Name: cheat
- App description: Create and view interactive cheat sheets for *nix commands
- App Version: 4.2.3
- App Website: https://github.com/cheat/cheat

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cheat with the following command
   ```
   brew install cheat
   ```
4. cheat is ready to use now!