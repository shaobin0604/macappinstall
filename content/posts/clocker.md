---
title: "Install Clocker on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Menu bar timezone tracker"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Clocker on MacOS using homebrew

- App Name: Clocker
- App description: Menu bar timezone tracker
- App Version: 22.02.01
- App Website: https://abhishekbanthia.com/clocker

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Clocker with the following command
   ```
   brew install --cask clocker
   ```
4. Clocker is ready to use now!