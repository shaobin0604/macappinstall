---
title: "Install Ejector on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Adds an Eject icon to the menu bar"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Ejector on MacOS using homebrew

- App Name: Ejector
- App description: Adds an Eject icon to the menu bar
- App Version: 0.8.1
- App Website: https://www.jeb.com.fr/en/ejector.shtml

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Ejector with the following command
   ```
   brew install --cask ejector
   ```
4. Ejector is ready to use now!