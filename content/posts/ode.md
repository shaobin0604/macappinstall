---
title: "Install ode on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Simulating articulated rigid body dynamics"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ode on MacOS using homebrew

- App Name: ode
- App description: Simulating articulated rigid body dynamics
- App Version: 0.16.2
- App Website: https://www.ode.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ode with the following command
   ```
   brew install ode
   ```
4. ode is ready to use now!