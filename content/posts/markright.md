---
title: "Install MarkRight on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Electron-powered markdown editor with live preview"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install MarkRight on MacOS using homebrew

- App Name: MarkRight
- App description: Electron-powered markdown editor with live preview
- App Version: 0.1.11
- App Website: https://github.com/dvcrn/markright

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install MarkRight with the following command
   ```
   brew install --cask markright
   ```
4. MarkRight is ready to use now!