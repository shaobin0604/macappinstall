---
title: "Install aws-cdk on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "AWS Cloud Development Kit - framework for defining AWS infra as code"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install aws-cdk on MacOS using homebrew

- App Name: aws-cdk
- App description: AWS Cloud Development Kit - framework for defining AWS infra as code
- App Version: 2.12.0
- App Website: https://github.com/aws/aws-cdk

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install aws-cdk with the following command
   ```
   brew install aws-cdk
   ```
4. aws-cdk is ready to use now!