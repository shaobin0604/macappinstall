---
title: "Install threemux on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Terminal multiplexer inspired by i3"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install threemux on MacOS using homebrew

- App Name: threemux
- App description: Terminal multiplexer inspired by i3
- App Version: 1.1.0
- App Website: https://github.com/aaronjanse/3mux

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install threemux with the following command
   ```
   brew install threemux
   ```
4. threemux is ready to use now!