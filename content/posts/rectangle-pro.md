---
title: "Install Rectangle Pro on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Window snapping tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Rectangle Pro on MacOS using homebrew

- App Name: Rectangle Pro
- App description: Window snapping tool
- App Version: 2.2.1,104
- App Website: https://rectangleapp.com/pro

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Rectangle Pro with the following command
   ```
   brew install --cask rectangle-pro
   ```
4. Rectangle Pro is ready to use now!