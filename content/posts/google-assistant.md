---
title: "Install Google Assistant Unofficial Desktop Client on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cross-platform unofficial Google Assistant Client for Desktop"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Google Assistant Unofficial Desktop Client on MacOS using homebrew

- App Name: Google Assistant Unofficial Desktop Client
- App description: Cross-platform unofficial Google Assistant Client for Desktop
- App Version: 1.0.0
- App Website: https://github.com/Melvin-Abraham/Google-Assistant-Unofficial-Desktop-Client

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Google Assistant Unofficial Desktop Client with the following command
   ```
   brew install --cask google-assistant
   ```
4. Google Assistant Unofficial Desktop Client is ready to use now!