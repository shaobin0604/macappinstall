---
title: "Install ChronoAgent on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Remote file sharing for ChronoSync"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ChronoAgent on MacOS using homebrew

- App Name: ChronoAgent
- App description: Remote file sharing for ChronoSync
- App Version: 1.9.9
- App Website: https://www.econtechnologies.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ChronoAgent with the following command
   ```
   brew install --cask chronoagent
   ```
4. ChronoAgent is ready to use now!