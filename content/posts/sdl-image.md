---
title: "Install sdl_image on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Image file loading library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sdl_image on MacOS using homebrew

- App Name: sdl_image
- App description: Image file loading library
- App Version: 1.2.12
- App Website: https://www.libsdl.org/projects/SDL_image/release-1.2.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sdl_image with the following command
   ```
   brew install sdl_image
   ```
4. sdl_image is ready to use now!