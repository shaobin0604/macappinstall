---
title: "Install Yandex.Disk on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cloud storage"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Yandex.Disk on MacOS using homebrew

- App Name: Yandex.Disk
- App description: Cloud storage
- App Version: 3.2.15,2827
- App Website: https://disk.yandex.ru/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Yandex.Disk with the following command
   ```
   brew install --cask yandex-disk
   ```
4. Yandex.Disk is ready to use now!