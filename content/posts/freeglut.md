---
title: "Install freeglut on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Open-source alternative to the OpenGL Utility Toolkit (GLUT) library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install freeglut on MacOS using homebrew

- App Name: freeglut
- App description: Open-source alternative to the OpenGL Utility Toolkit (GLUT) library
- App Version: 3.2.2
- App Website: https://freeglut.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install freeglut with the following command
   ```
   brew install freeglut
   ```
4. freeglut is ready to use now!