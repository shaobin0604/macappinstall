---
title: "Install xtitle on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Set window title and icon for your X terminal"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install xtitle on MacOS using homebrew

- App Name: xtitle
- App description: Set window title and icon for your X terminal
- App Version: 1.0.4
- App Website: https://kinzler.com/me/xtitle/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install xtitle with the following command
   ```
   brew install xtitle
   ```
4. xtitle is ready to use now!