---
title: "Install Linphone on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Software for communication systems developers"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Linphone on MacOS using homebrew

- App Name: Linphone
- App description: Software for communication systems developers
- App Version: 4.3.2
- App Website: https://www.linphone.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Linphone with the following command
   ```
   brew install --cask linphone
   ```
4. Linphone is ready to use now!