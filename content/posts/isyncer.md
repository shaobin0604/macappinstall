---
title: "Install iSyncer on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Apple Music playlist exporting tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install iSyncer on MacOS using homebrew

- App Name: iSyncer
- App description: Apple Music playlist exporting tool
- App Version: 3.8.0
- App Website: https://www.isyncer.de/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install iSyncer with the following command
   ```
   brew install --cask isyncer
   ```
4. iSyncer is ready to use now!