---
title: "Install isort on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Sort Python imports automatically"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install isort on MacOS using homebrew

- App Name: isort
- App description: Sort Python imports automatically
- App Version: 5.10.1
- App Website: https://pycqa.github.io/isort/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install isort with the following command
   ```
   brew install isort
   ```
4. isort is ready to use now!