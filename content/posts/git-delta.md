---
title: "Install git-delta on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Syntax-highlighting pager for git and diff output"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install git-delta on MacOS using homebrew

- App Name: git-delta
- App description: Syntax-highlighting pager for git and diff output
- App Version: 0.12.0
- App Website: https://github.com/dandavison/delta

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install git-delta with the following command
   ```
   brew install git-delta
   ```
4. git-delta is ready to use now!