---
title: "Install TabTopus on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Web browser tabs URL exporter"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install TabTopus on MacOS using homebrew

- App Name: TabTopus
- App description: Web browser tabs URL exporter
- App Version: 4.2.5,4250
- App Website: https://www.mariogt.com/tabtopus.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install TabTopus with the following command
   ```
   brew install --cask tabtopus
   ```
4. TabTopus is ready to use now!