---
title: "Install NetNewsWire on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Free and open-source RSS reader"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install NetNewsWire on MacOS using homebrew

- App Name: NetNewsWire
- App description: Free and open-source RSS reader
- App Version: 6.0.3
- App Website: https://netnewswire.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install NetNewsWire with the following command
   ```
   brew install --cask netnewswire
   ```
4. NetNewsWire is ready to use now!