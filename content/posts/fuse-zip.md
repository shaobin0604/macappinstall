---
title: "Install fuse-zip on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "FUSE file system to create & manipulate ZIP archives"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fuse-zip on MacOS using homebrew

- App Name: fuse-zip
- App description: FUSE file system to create & manipulate ZIP archives
- App Version: 0.7.1
- App Website: https://bitbucket.org/agalanin/fuse-zip

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fuse-zip with the following command
   ```
   brew install fuse-zip
   ```
4. fuse-zip is ready to use now!