---
title: "Install globe on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Prints ASCII graphic of currently-lit side of the Earth"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install globe on MacOS using homebrew

- App Name: globe
- App description: Prints ASCII graphic of currently-lit side of the Earth
- App Version: 0.0.20140814
- App Website: https://www.acme.com/software/globe/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install globe with the following command
   ```
   brew install globe
   ```
4. globe is ready to use now!