---
title: "Install yydecode on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Decode yEnc archives"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install yydecode on MacOS using homebrew

- App Name: yydecode
- App description: Decode yEnc archives
- App Version: 0.2.10
- App Website: https://yydecode.sourceforge.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install yydecode with the following command
   ```
   brew install yydecode
   ```
4. yydecode is ready to use now!