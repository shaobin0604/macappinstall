---
title: "Install rswift on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Get strong typed, autocompleted resources like images, fonts and segues"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install rswift on MacOS using homebrew

- App Name: rswift
- App description: Get strong typed, autocompleted resources like images, fonts and segues
- App Version: 5.4.0
- App Website: https://github.com/mac-cain13/R.swift

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install rswift with the following command
   ```
   brew install rswift
   ```
4. rswift is ready to use now!