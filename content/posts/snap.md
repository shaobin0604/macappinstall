---
title: "Install snap on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to work with .snap files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install snap on MacOS using homebrew

- App Name: snap
- App description: Tool to work with .snap files
- App Version: 2.46
- App Website: https://snapcraft.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install snap with the following command
   ```
   brew install snap
   ```
4. snap is ready to use now!