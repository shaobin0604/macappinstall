---
title: "Install solana on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Web-Scale Blockchain for decentralized apps and marketplaces"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install solana on MacOS using homebrew

- App Name: solana
- App description: Web-Scale Blockchain for decentralized apps and marketplaces
- App Version: 1.9.7
- App Website: https://solana.com

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install solana with the following command
   ```
   brew install solana
   ```
4. solana is ready to use now!