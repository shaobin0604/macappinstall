---
title: "Install fre:ac on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Audio converter and CD ripper"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fre:ac on MacOS using homebrew

- App Name: fre:ac
- App description: Audio converter and CD ripper
- App Version: 1.1.6
- App Website: https://www.freac.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fre:ac with the following command
   ```
   brew install --cask freac
   ```
4. fre:ac is ready to use now!