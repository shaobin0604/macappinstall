---
title: "Install jenv on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Manage your Java environment"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install jenv on MacOS using homebrew

- App Name: jenv
- App description: Manage your Java environment
- App Version: 0.5.4
- App Website: https://www.jenv.be/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install jenv with the following command
   ```
   brew install jenv
   ```
4. jenv is ready to use now!