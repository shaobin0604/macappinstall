---
title: "Install cunit on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Lightweight unit testing framework for C"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cunit on MacOS using homebrew

- App Name: cunit
- App description: Lightweight unit testing framework for C
- App Version: 2.1-3
- App Website: https://cunit.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cunit with the following command
   ```
   brew install cunit
   ```
4. cunit is ready to use now!