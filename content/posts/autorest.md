---
title: "Install autorest on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Swagger (OpenAPI) Specification code generator"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install autorest on MacOS using homebrew

- App Name: autorest
- App description: Swagger (OpenAPI) Specification code generator
- App Version: 3.5.1
- App Website: https://github.com/Azure/autorest

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install autorest with the following command
   ```
   brew install autorest
   ```
4. autorest is ready to use now!