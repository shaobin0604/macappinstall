---
title: "Install ipv6toolkit on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Security assessment and troubleshooting tool for IPv6"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ipv6toolkit on MacOS using homebrew

- App Name: ipv6toolkit
- App description: Security assessment and troubleshooting tool for IPv6
- App Version: 2.0
- App Website: https://www.si6networks.com/research/tools/ipv6toolkit/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ipv6toolkit with the following command
   ```
   brew install ipv6toolkit
   ```
4. ipv6toolkit is ready to use now!