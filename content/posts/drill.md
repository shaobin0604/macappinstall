---
title: "Install drill on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "HTTP load testing application written in Rust"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install drill on MacOS using homebrew

- App Name: drill
- App description: HTTP load testing application written in Rust
- App Version: 0.7.2
- App Website: https://github.com/fcsonline/drill

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install drill with the following command
   ```
   brew install drill
   ```
4. drill is ready to use now!