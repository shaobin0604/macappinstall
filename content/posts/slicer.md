---
title: "Install 3D Slicer on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Medical image processing and visualization system"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install 3D Slicer on MacOS using homebrew

- App Name: 3D Slicer
- App description: Medical image processing and visualization system
- App Version: 4.11.20210226,60add6fdae4540bf6a89bf73
- App Website: https://www.slicer.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install 3D Slicer with the following command
   ```
   brew install --cask slicer
   ```
4. 3D Slicer is ready to use now!