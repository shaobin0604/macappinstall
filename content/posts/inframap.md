---
title: "Install inframap on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Read your tfstate or HCL to generate a graph"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install inframap on MacOS using homebrew

- App Name: inframap
- App description: Read your tfstate or HCL to generate a graph
- App Version: 0.6.7
- App Website: https://github.com/cycloidio/inframap

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install inframap with the following command
   ```
   brew install inframap
   ```
4. inframap is ready to use now!