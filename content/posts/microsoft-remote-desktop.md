---
title: "Install Microsoft Remote Desktop on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Remote desktop client"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Microsoft Remote Desktop on MacOS using homebrew

- App Name: Microsoft Remote Desktop
- App description: Remote desktop client
- App Version: 10.7.6
- App Website: https://docs.microsoft.com/en-us/windows-server/remote/remote-desktop-services/clients/remote-desktop-mac

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Microsoft Remote Desktop with the following command
   ```
   brew install --cask microsoft-remote-desktop
   ```
4. Microsoft Remote Desktop is ready to use now!