---
title: "Install Ungoogled Chromium on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Google Chromium, sans integration with Google"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Ungoogled Chromium on MacOS using homebrew

- App Name: Ungoogled Chromium
- App description: Google Chromium, sans integration with Google
- App Version: 98.0.4758.102-1.1,1644985332
- App Website: https://ungoogled-software.github.io/ungoogled-chromium-binaries/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Ungoogled Chromium with the following command
   ```
   brew install --cask eloston-chromium
   ```
4. Ungoogled Chromium is ready to use now!