---
title: "Install ccze on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Robust and modular log colorizer"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ccze on MacOS using homebrew

- App Name: ccze
- App description: Robust and modular log colorizer
- App Version: 0.2.1
- App Website: https://packages.debian.org/wheezy/ccze

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ccze with the following command
   ```
   brew install ccze
   ```
4. ccze is ready to use now!