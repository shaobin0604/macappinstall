---
title: "Install smu on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Simple markup with markdown-like syntax"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install smu on MacOS using homebrew

- App Name: smu
- App description: Simple markup with markdown-like syntax
- App Version: 1.5
- App Website: https://github.com/Gottox/smu

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install smu with the following command
   ```
   brew install smu
   ```
4. smu is ready to use now!