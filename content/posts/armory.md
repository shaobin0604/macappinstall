---
title: "Install Armory on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Python-Based Bitcoin Software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Armory on MacOS using homebrew

- App Name: Armory
- App description: Python-Based Bitcoin Software
- App Version: 0.96.5
- App Website: https://btcarmory.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Armory with the following command
   ```
   brew install --cask armory
   ```
4. Armory is ready to use now!