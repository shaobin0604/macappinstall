---
title: "Install libgnomecanvasmm on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C++ wrapper for libgnomecanvas"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libgnomecanvasmm on MacOS using homebrew

- App Name: libgnomecanvasmm
- App description: C++ wrapper for libgnomecanvas
- App Version: 2.26.0
- App Website: https://launchpad.net/libgnomecanvasmm

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libgnomecanvasmm with the following command
   ```
   brew install libgnomecanvasmm
   ```
4. libgnomecanvasmm is ready to use now!