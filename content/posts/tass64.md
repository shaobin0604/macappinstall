---
title: "Install tass64 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Multi pass optimizing macro assembler for the 65xx series of processors"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tass64 on MacOS using homebrew

- App Name: tass64
- App description: Multi pass optimizing macro assembler for the 65xx series of processors
- App Version: 1.56.2625
- App Website: https://tass64.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tass64 with the following command
   ```
   brew install tass64
   ```
4. tass64 is ready to use now!