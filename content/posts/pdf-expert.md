---
title: "Install PDF Expert on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "PDF reader, editor and annotator"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install PDF Expert on MacOS using homebrew

- App Name: PDF Expert
- App description: PDF reader, editor and annotator
- App Version: 2.5.19,752
- App Website: https://pdfexpert.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install PDF Expert with the following command
   ```
   brew install --cask pdf-expert
   ```
4. PDF Expert is ready to use now!