---
title: "Install cjson on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Ultralightweight JSON parser in ANSI C"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cjson on MacOS using homebrew

- App Name: cjson
- App description: Ultralightweight JSON parser in ANSI C
- App Version: 1.7.15
- App Website: https://github.com/DaveGamble/cJSON

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cjson with the following command
   ```
   brew install cjson
   ```
4. cjson is ready to use now!