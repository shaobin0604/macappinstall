---
title: "Install hexgui on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GUI for playing Hex over Hex Text Protocol"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install hexgui on MacOS using homebrew

- App Name: hexgui
- App description: GUI for playing Hex over Hex Text Protocol
- App Version: 0.9.3
- App Website: https://sourceforge.net/p/benzene/hexgui/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install hexgui with the following command
   ```
   brew install hexgui
   ```
4. hexgui is ready to use now!