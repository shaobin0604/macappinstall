---
title: "Install Persepolis on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GUI for aria2"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Persepolis on MacOS using homebrew

- App Name: Persepolis
- App description: GUI for aria2
- App Version: 3.2.0
- App Website: https://persepolisdm.github.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Persepolis with the following command
   ```
   brew install --cask persepolis-download-manager
   ```
4. Persepolis is ready to use now!