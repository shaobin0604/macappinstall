---
title: "Install Binance on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cryptocurrency exchange"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Binance on MacOS using homebrew

- App Name: Binance
- App description: Cryptocurrency exchange
- App Version: 1.31.0
- App Website: http://binance.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Binance with the following command
   ```
   brew install --cask binance
   ```
4. Binance is ready to use now!