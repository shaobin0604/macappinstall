---
title: "Install ompl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Open Motion Planning Library consists of many motion planning algorithms"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ompl on MacOS using homebrew

- App Name: ompl
- App description: Open Motion Planning Library consists of many motion planning algorithms
- App Version: 1.5.2
- App Website: https://ompl.kavrakilab.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ompl with the following command
   ```
   brew install ompl
   ```
4. ompl is ready to use now!