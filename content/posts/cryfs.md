---
title: "Install cryfs on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Encrypts your files so you can safely store them in Dropbox, iCloud, etc."
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cryfs on MacOS using homebrew

- App Name: cryfs
- App description: Encrypts your files so you can safely store them in Dropbox, iCloud, etc.
- App Version: 0.10.2
- App Website: https://www.cryfs.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cryfs with the following command
   ```
   brew install cryfs
   ```
4. cryfs is ready to use now!