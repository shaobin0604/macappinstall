---
title: "Install Offset Explorer on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GUI for managing and using Apache Kafka clusters"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Offset Explorer on MacOS using homebrew

- App Name: Offset Explorer
- App description: GUI for managing and using Apache Kafka clusters
- App Version: 2.2
- App Website: https://www.kafkatool.com/index.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Offset Explorer with the following command
   ```
   brew install --cask offset-explorer
   ```
4. Offset Explorer is ready to use now!