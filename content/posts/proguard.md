---
title: "Install proguard on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Java class file shrinker, optimizer, and obfuscator"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install proguard on MacOS using homebrew

- App Name: proguard
- App description: Java class file shrinker, optimizer, and obfuscator
- App Version: 7.2.0
- App Website: https://www.guardsquare.com/en/products/proguard

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install proguard with the following command
   ```
   brew install proguard
   ```
4. proguard is ready to use now!