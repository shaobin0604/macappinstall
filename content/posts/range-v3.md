---
title: "Install range-v3 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Experimental range library for C++14/17/20"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install range-v3 on MacOS using homebrew

- App Name: range-v3
- App description: Experimental range library for C++14/17/20
- App Version: 0.11.0
- App Website: https://ericniebler.github.io/range-v3/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install range-v3 with the following command
   ```
   brew install range-v3
   ```
4. range-v3 is ready to use now!