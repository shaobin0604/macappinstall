---
title: "Install ThemeEngine on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "App to edit compiled .car files"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ThemeEngine on MacOS using homebrew

- App Name: ThemeEngine
- App description: App to edit compiled .car files
- App Version: 1.0.0,111
- App Website: https://github.com/alexzielenski/ThemeEngine/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ThemeEngine with the following command
   ```
   brew install --cask themeengine
   ```
4. ThemeEngine is ready to use now!