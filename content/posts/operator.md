---
title: "Install Operator on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Prelude Operator is a desktop adversary emulation platform"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Operator on MacOS using homebrew

- App Name: Operator
- App description: Prelude Operator is a desktop adversary emulation platform
- App Version: 1.4.2
- App Website: https://www.prelude.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Operator with the following command
   ```
   brew install --cask operator
   ```
4. Operator is ready to use now!