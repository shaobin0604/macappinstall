---
title: "Install truncate on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Truncates a file to a given size"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install truncate on MacOS using homebrew

- App Name: truncate
- App description: Truncates a file to a given size
- App Version: 0.9
- App Website: https://www.vanheusden.com/truncate/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install truncate with the following command
   ```
   brew install truncate
   ```
4. truncate is ready to use now!