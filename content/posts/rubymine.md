---
title: "Install RubyMine on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Ruby on Rails IDE"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install RubyMine on MacOS using homebrew

- App Name: RubyMine
- App description: Ruby on Rails IDE
- App Version: 2021.3.2,213.6777.43
- App Website: https://www.jetbrains.com/ruby/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install RubyMine with the following command
   ```
   brew install --cask rubymine
   ```
4. RubyMine is ready to use now!