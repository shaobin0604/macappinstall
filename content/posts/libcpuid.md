---
title: "Install libcpuid on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Small C library for x86 CPU detection and feature extraction"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libcpuid on MacOS using homebrew

- App Name: libcpuid
- App description: Small C library for x86 CPU detection and feature extraction
- App Version: 0.5.1
- App Website: https://github.com/anrieff/libcpuid

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libcpuid with the following command
   ```
   brew install libcpuid
   ```
4. libcpuid is ready to use now!