---
title: "Install Chatterino on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Chat client for https://twitch.tv"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Chatterino on MacOS using homebrew

- App Name: Chatterino
- App description: Chat client for https://twitch.tv
- App Version: 2.3.4
- App Website: https://chatterino.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Chatterino with the following command
   ```
   brew install --cask chatterino
   ```
4. Chatterino is ready to use now!