---
title: "Install UNetbootin on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to install Linux/BSD distributions to a partition or USB drive"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install UNetbootin on MacOS using homebrew

- App Name: UNetbootin
- App description: Tool to install Linux/BSD distributions to a partition or USB drive
- App Version: 702
- App Website: https://unetbootin.github.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install UNetbootin with the following command
   ```
   brew install --cask unetbootin
   ```
4. UNetbootin is ready to use now!