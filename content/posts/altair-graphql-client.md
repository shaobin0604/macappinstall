---
title: "Install Altair GraphQL Client on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GraphQL client"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Altair GraphQL Client on MacOS using homebrew

- App Name: Altair GraphQL Client
- App description: GraphQL client
- App Version: 4.4.0
- App Website: https://altair.sirmuel.design/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Altair GraphQL Client with the following command
   ```
   brew install --cask altair-graphql-client
   ```
4. Altair GraphQL Client is ready to use now!