---
title: "Install Alva on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Create living prototypes with code components"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Alva on MacOS using homebrew

- App Name: Alva
- App description: Create living prototypes with code components
- App Version: 0.9.1
- App Website: https://meetalva.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Alva with the following command
   ```
   brew install --cask alva
   ```
4. Alva is ready to use now!