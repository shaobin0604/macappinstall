---
title: "Install exiv2 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "EXIF and IPTC metadata manipulation library and tools"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install exiv2 on MacOS using homebrew

- App Name: exiv2
- App description: EXIF and IPTC metadata manipulation library and tools
- App Version: 0.27.5
- App Website: https://exiv2.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install exiv2 with the following command
   ```
   brew install exiv2
   ```
4. exiv2 is ready to use now!