---
title: "Install Timer on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Stopwatch, alarm clock, and clock utility"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Timer on MacOS using homebrew

- App Name: Timer
- App description: Stopwatch, alarm clock, and clock utility
- App Version: 9.0.3
- App Website: https://www.apimac.com/mac/timer/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Timer with the following command
   ```
   brew install --cask timer
   ```
4. Timer is ready to use now!