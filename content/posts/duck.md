---
title: "Install duck on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line interface for Cyberduck (a multi-protocol file transfer tool)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install duck on MacOS using homebrew

- App Name: duck
- App description: Command-line interface for Cyberduck (a multi-protocol file transfer tool)
- App Version: 8.2.3.36880
- App Website: https://duck.sh/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install duck with the following command
   ```
   brew install duck
   ```
4. duck is ready to use now!