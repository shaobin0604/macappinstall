---
title: "Install Raven Reader on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "News reader with flexible settings"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Raven Reader on MacOS using homebrew

- App Name: Raven Reader
- App description: News reader with flexible settings
- App Version: 1.0.70
- App Website: https://ravenreader.app/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Raven Reader with the following command
   ```
   brew install --cask raven-reader
   ```
4. Raven Reader is ready to use now!