---
title: "Install Xournal++ on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Handwriting notetaking software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Xournal++ on MacOS using homebrew

- App Name: Xournal++
- App description: Handwriting notetaking software
- App Version: 1.1.0
- App Website: https://github.com/xournalpp/xournalpp

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Xournal++ with the following command
   ```
   brew install --cask xournal-plus-plus
   ```
4. Xournal++ is ready to use now!