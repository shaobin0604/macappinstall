---
title: "Install pstree on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Show ps output as a tree"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pstree on MacOS using homebrew

- App Name: pstree
- App description: Show ps output as a tree
- App Version: 2.39
- App Website: http://www.thp.uni-duisburg.de/pstree/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pstree with the following command
   ```
   brew install pstree
   ```
4. pstree is ready to use now!