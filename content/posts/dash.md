---
title: "Install dash on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "POSIX-compliant descendant of NetBSD's ash (the Almquist SHell)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dash on MacOS using homebrew

- App Name: dash
- App description: POSIX-compliant descendant of NetBSD's ash (the Almquist SHell)
- App Version: 0.5.11.5
- App Website: http://gondor.apana.org.au/~herbert/dash/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dash with the following command
   ```
   brew install dash
   ```
4. dash is ready to use now!