---
title: "Install adios2 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Next generation of ADIOS developed in the Exascale Computing Program"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install adios2 on MacOS using homebrew

- App Name: adios2
- App description: Next generation of ADIOS developed in the Exascale Computing Program
- App Version: 2.7.1
- App Website: https://adios2.readthedocs.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install adios2 with the following command
   ```
   brew install adios2
   ```
4. adios2 is ready to use now!