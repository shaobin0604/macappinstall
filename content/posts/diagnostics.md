---
title: "Install Diagnostics on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Diagnostic (crash) reports viewer"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Diagnostics on MacOS using homebrew

- App Name: Diagnostics
- App description: Diagnostic (crash) reports viewer
- App Version: 1.4.1
- App Website: https://github.com/macmade/Diagnostics

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Diagnostics with the following command
   ```
   brew install --cask diagnostics
   ```
4. Diagnostics is ready to use now!