---
title: "Install libquantum on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C library for the simulation of quantum mechanics"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libquantum on MacOS using homebrew

- App Name: libquantum
- App description: C library for the simulation of quantum mechanics
- App Version: 1.0.0
- App Website: http://www.libquantum.de/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libquantum with the following command
   ```
   brew install libquantum
   ```
4. libquantum is ready to use now!