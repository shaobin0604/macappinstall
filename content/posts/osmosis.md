---
title: "Install osmosis on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line OpenStreetMap data processor"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install osmosis on MacOS using homebrew

- App Name: osmosis
- App description: Command-line OpenStreetMap data processor
- App Version: 0.48.3
- App Website: https://wiki.openstreetmap.org/wiki/Osmosis

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install osmosis with the following command
   ```
   brew install osmosis
   ```
4. osmosis is ready to use now!