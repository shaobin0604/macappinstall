---
title: "Install uhd on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Hardware driver for all USRP devices"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install uhd on MacOS using homebrew

- App Name: uhd
- App description: Hardware driver for all USRP devices
- App Version: 4.1.0.5
- App Website: https://files.ettus.com/manual/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install uhd with the following command
   ```
   brew install uhd
   ```
4. uhd is ready to use now!