---
title: "Install Muzzle on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Silence embarrassing notifications while screensharing"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Muzzle on MacOS using homebrew

- App Name: Muzzle
- App description: Silence embarrassing notifications while screensharing
- App Version: 1.9,426
- App Website: https://muzzleapp.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Muzzle with the following command
   ```
   brew install --cask muzzle
   ```
4. Muzzle is ready to use now!