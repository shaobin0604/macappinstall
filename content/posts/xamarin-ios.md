---
title: "Install Xamarin.iOS on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Gives .NET developers complete access to iOS, watchOS, and tvOS SDK's"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Xamarin.iOS on MacOS using homebrew

- App Name: Xamarin.iOS
- App description: Gives .NET developers complete access to iOS, watchOS, and tvOS SDK's
- App Version: 15.4.0.0
- App Website: https://www.xamarin.com/platform

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Xamarin.iOS with the following command
   ```
   brew install --cask xamarin-ios
   ```
4. Xamarin.iOS is ready to use now!