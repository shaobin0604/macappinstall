---
title: "Install confuse on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Configuration file parser library written in C"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install confuse on MacOS using homebrew

- App Name: confuse
- App description: Configuration file parser library written in C
- App Version: 3.3
- App Website: https://github.com/libconfuse/libconfuse

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install confuse with the following command
   ```
   brew install confuse
   ```
4. confuse is ready to use now!