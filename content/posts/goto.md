---
title: "Install goto on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Bash tool for navigation to aliased directories with auto-completion"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install goto on MacOS using homebrew

- App Name: goto
- App description: Bash tool for navigation to aliased directories with auto-completion
- App Version: 2.0.0
- App Website: https://github.com/iridakos/goto

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install goto with the following command
   ```
   brew install goto
   ```
4. goto is ready to use now!