---
title: "Install Colorpicker on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Get and save color codes"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Colorpicker on MacOS using homebrew

- App Name: Colorpicker
- App description: Get and save color codes
- App Version: 2.0.5
- App Website: https://colorpicker.fr/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Colorpicker with the following command
   ```
   brew install --cask toinane-colorpicker
   ```
4. Colorpicker is ready to use now!