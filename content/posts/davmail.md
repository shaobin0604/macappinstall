---
title: "Install davmail on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "POP/IMAP/SMTP/Caldav/Carddav/LDAP exchange gateway"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install davmail on MacOS using homebrew

- App Name: davmail
- App description: POP/IMAP/SMTP/Caldav/Carddav/LDAP exchange gateway
- App Version: 6.0.1
- App Website: https://davmail.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install davmail with the following command
   ```
   brew install davmail
   ```
4. davmail is ready to use now!