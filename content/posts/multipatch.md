---
title: "Install MultiPatch on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "File patching utility"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install MultiPatch on MacOS using homebrew

- App Name: MultiPatch
- App description: File patching utility
- App Version: 2.0
- App Website: https://projects.sappharad.com/multipatch/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install MultiPatch with the following command
   ```
   brew install --cask multipatch
   ```
4. MultiPatch is ready to use now!