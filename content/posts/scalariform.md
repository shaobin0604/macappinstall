---
title: "Install scalariform on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Scala source code formatter"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install scalariform on MacOS using homebrew

- App Name: scalariform
- App description: Scala source code formatter
- App Version: 0.2.10
- App Website: https://github.com/scala-ide/scalariform

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install scalariform with the following command
   ```
   brew install scalariform
   ```
4. scalariform is ready to use now!