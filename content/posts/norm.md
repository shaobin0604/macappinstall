---
title: "Install norm on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "NACK-Oriented Reliable Multicast"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install norm on MacOS using homebrew

- App Name: norm
- App description: NACK-Oriented Reliable Multicast
- App Version: 1.5.8
- App Website: https://www.nrl.navy.mil/itd/ncs/products/norm

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install norm with the following command
   ```
   brew install norm
   ```
4. norm is ready to use now!