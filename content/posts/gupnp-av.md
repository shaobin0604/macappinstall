---
title: "Install gupnp-av on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library to help implement UPnP A/V profiles"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gupnp-av on MacOS using homebrew

- App Name: gupnp-av
- App description: Library to help implement UPnP A/V profiles
- App Version: 0.14.0
- App Website: https://wiki.gnome.org/GUPnP/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gupnp-av with the following command
   ```
   brew install gupnp-av
   ```
4. gupnp-av is ready to use now!