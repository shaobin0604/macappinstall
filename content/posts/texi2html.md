---
title: "Install texi2html on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Convert TeXinfo files to HTML"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install texi2html on MacOS using homebrew

- App Name: texi2html
- App description: Convert TeXinfo files to HTML
- App Version: 5.0
- App Website: https://www.nongnu.org/texi2html/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install texi2html with the following command
   ```
   brew install texi2html
   ```
4. texi2html is ready to use now!