---
title: "Install Electron API Demos on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Explore the Electron APIs"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Electron API Demos on MacOS using homebrew

- App Name: Electron API Demos
- App description: Explore the Electron APIs
- App Version: 2.0.2
- App Website: https://github.com/electron/electron-api-demos

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Electron API Demos with the following command
   ```
   brew install --cask electron-api-demos
   ```
4. Electron API Demos is ready to use now!