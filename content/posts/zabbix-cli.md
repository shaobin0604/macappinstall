---
title: "Install zabbix-cli on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CLI tool for interacting with Zabbix monitoring system"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install zabbix-cli on MacOS using homebrew

- App Name: zabbix-cli
- App description: CLI tool for interacting with Zabbix monitoring system
- App Version: 2.2.1
- App Website: https://github.com/unioslo/zabbix-cli/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install zabbix-cli with the following command
   ```
   brew install zabbix-cli
   ```
4. zabbix-cli is ready to use now!