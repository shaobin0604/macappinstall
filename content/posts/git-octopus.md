---
title: "Install git-octopus on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Continuous merge workflow"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install git-octopus on MacOS using homebrew

- App Name: git-octopus
- App description: Continuous merge workflow
- App Version: 1.4
- App Website: https://github.com/lesfurets/git-octopus

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install git-octopus with the following command
   ```
   brew install git-octopus
   ```
4. git-octopus is ready to use now!