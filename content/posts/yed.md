---
title: "Install yWorks yEd on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Create diagrams manually, or import external data for analysis"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install yWorks yEd on MacOS using homebrew

- App Name: yWorks yEd
- App description: Create diagrams manually, or import external data for analysis
- App Version: 3.21.1
- App Website: https://www.yworks.com/products/yed

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install yWorks yEd with the following command
   ```
   brew install --cask yed
   ```
4. yWorks yEd is ready to use now!