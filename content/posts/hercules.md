---
title: "Install hercules on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "System/370, ESA/390 and z/Architecture Emulator"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install hercules on MacOS using homebrew

- App Name: hercules
- App description: System/370, ESA/390 and z/Architecture Emulator
- App Version: 3.13
- App Website: http://www.hercules-390.eu/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install hercules with the following command
   ```
   brew install hercules
   ```
4. hercules is ready to use now!