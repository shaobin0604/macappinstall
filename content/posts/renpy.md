---
title: "Install Ren'Py on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Visual novel engine in Python"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Ren'Py on MacOS using homebrew

- App Name: Ren'Py
- App description: Visual novel engine in Python
- App Version: 7.4.11
- App Website: https://www.renpy.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Ren'Py with the following command
   ```
   brew install --cask renpy
   ```
4. Ren'Py is ready to use now!