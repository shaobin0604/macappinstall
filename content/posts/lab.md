---
title: "Install lab on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Git wrapper for GitLab"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install lab on MacOS using homebrew

- App Name: lab
- App description: Git wrapper for GitLab
- App Version: 0.23.0
- App Website: https://zaquestion.github.io/lab

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install lab with the following command
   ```
   brew install lab
   ```
4. lab is ready to use now!