---
title: "Install fpc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Free Pascal: multi-architecture Pascal compiler"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fpc on MacOS using homebrew

- App Name: fpc
- App description: Free Pascal: multi-architecture Pascal compiler
- App Version: 3.2.2
- App Website: https://www.freepascal.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fpc with the following command
   ```
   brew install fpc
   ```
4. fpc is ready to use now!