---
title: "Install sk on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Fuzzy Finder in rust!"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sk on MacOS using homebrew

- App Name: sk
- App description: Fuzzy Finder in rust!
- App Version: 0.9.4
- App Website: https://github.com/lotabout/skim

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sk with the following command
   ```
   brew install sk
   ```
4. sk is ready to use now!