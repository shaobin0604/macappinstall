---
title: "Install libtiff on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "TIFF library and utilities"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libtiff on MacOS using homebrew

- App Name: libtiff
- App description: TIFF library and utilities
- App Version: 4.3.0
- App Website: https://libtiff.gitlab.io/libtiff/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libtiff with the following command
   ```
   brew install libtiff
   ```
4. libtiff is ready to use now!