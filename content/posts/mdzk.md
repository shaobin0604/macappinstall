---
title: "Install mdzk on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Plain text Zettelkasten based on mdBook"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mdzk on MacOS using homebrew

- App Name: mdzk
- App description: Plain text Zettelkasten based on mdBook
- App Version: 0.5.0
- App Website: https://mdzk.app/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mdzk with the following command
   ```
   brew install mdzk
   ```
4. mdzk is ready to use now!