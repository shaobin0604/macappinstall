---
title: "Install pcalc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Calculator for those working with multiple bases, sizes, and close to the bits"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pcalc on MacOS using homebrew

- App Name: pcalc
- App description: Calculator for those working with multiple bases, sizes, and close to the bits
- App Version: 2.2
- App Website: https://github.com/alt-romes/programmer-calculator

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pcalc with the following command
   ```
   brew install pcalc
   ```
4. pcalc is ready to use now!