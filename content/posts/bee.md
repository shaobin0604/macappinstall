---
title: "Install bee on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool for managing database changes"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bee on MacOS using homebrew

- App Name: bee
- App description: Tool for managing database changes
- App Version: 1.96
- App Website: https://github.com/bluesoft/bee

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bee with the following command
   ```
   brew install bee
   ```
4. bee is ready to use now!