---
title: "Install csvprintf on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line utility for parsing CSV files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install csvprintf on MacOS using homebrew

- App Name: csvprintf
- App description: Command-line utility for parsing CSV files
- App Version: 1.3.1
- App Website: https://github.com/archiecobbs/csvprintf

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install csvprintf with the following command
   ```
   brew install csvprintf
   ```
4. csvprintf is ready to use now!