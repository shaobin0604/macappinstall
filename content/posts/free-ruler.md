---
title: "Install Free Ruler on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Horizontal and vertical rulers"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Free Ruler on MacOS using homebrew

- App Name: Free Ruler
- App description: Horizontal and vertical rulers
- App Version: 2.0.5
- App Website: http://www.pascal.com/software/freeruler/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Free Ruler with the following command
   ```
   brew install --cask free-ruler
   ```
4. Free Ruler is ready to use now!