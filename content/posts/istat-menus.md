---
title: "Install iStats Menus on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "System monitoring app"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install iStats Menus on MacOS using homebrew

- App Name: iStats Menus
- App description: System monitoring app
- App Version: 6.61
- App Website: https://bjango.com/mac/istatmenus/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install iStats Menus with the following command
   ```
   brew install --cask istat-menus
   ```
4. iStats Menus is ready to use now!