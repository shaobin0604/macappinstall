---
title: "Install primer3 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Program for designing PCR primers"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install primer3 on MacOS using homebrew

- App Name: primer3
- App description: Program for designing PCR primers
- App Version: 2.4.0
- App Website: https://primer3.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install primer3 with the following command
   ```
   brew install primer3
   ```
4. primer3 is ready to use now!