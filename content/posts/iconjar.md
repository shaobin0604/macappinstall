---
title: "Install IconJar on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Icon organizer"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install IconJar on MacOS using homebrew

- App Name: IconJar
- App description: Icon organizer
- App Version: 2.10.0,50258
- App Website: https://geticonjar.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install IconJar with the following command
   ```
   brew install --cask iconjar
   ```
4. IconJar is ready to use now!