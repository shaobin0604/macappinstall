---
title: "Install HyperBackupExplorer on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Backup data from a Synology NAS"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install HyperBackupExplorer on MacOS using homebrew

- App Name: HyperBackupExplorer
- App description: Backup data from a Synology NAS
- App Version: 3.0.0-0149
- App Website: https://www.synology.com/en-us/dsm/feature/hyper_backup

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install HyperBackupExplorer with the following command
   ```
   brew install --cask hyperbackupexplorer
   ```
4. HyperBackupExplorer is ready to use now!