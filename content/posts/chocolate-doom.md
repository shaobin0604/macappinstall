---
title: "Install chocolate-doom on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Accurate source port of Doom"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install chocolate-doom on MacOS using homebrew

- App Name: chocolate-doom
- App description: Accurate source port of Doom
- App Version: 3.0.1
- App Website: https://www.chocolate-doom.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install chocolate-doom with the following command
   ```
   brew install chocolate-doom
   ```
4. chocolate-doom is ready to use now!