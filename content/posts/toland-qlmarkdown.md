---
title: "Install QLMarkdown on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "QuickLook generator for Markdown files"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install QLMarkdown on MacOS using homebrew

- App Name: QLMarkdown
- App description: QuickLook generator for Markdown files
- App Version: 1.3.6
- App Website: https://github.com/toland/qlmarkdown

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install QLMarkdown with the following command
   ```
   brew install --cask toland-qlmarkdown
   ```
4. QLMarkdown is ready to use now!