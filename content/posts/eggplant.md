---
title: "Install eggPlant Functional on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install eggPlant Functional on MacOS using homebrew

- App Name: eggPlant Functional
- App description: null
- App Version: 21.1.0,2101270001
- App Website: https://www.eggplantsoftware.com/eggplant-functional-downloads

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install eggPlant Functional with the following command
   ```
   brew install --cask eggplant
   ```
4. eggPlant Functional is ready to use now!