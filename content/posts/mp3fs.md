---
title: "Install mp3fs on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Read-only FUSE file system: transcodes audio formats to MP3"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mp3fs on MacOS using homebrew

- App Name: mp3fs
- App description: Read-only FUSE file system: transcodes audio formats to MP3
- App Version: 1.1.1
- App Website: https://khenriks.github.io/mp3fs/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mp3fs with the following command
   ```
   brew install mp3fs
   ```
4. mp3fs is ready to use now!