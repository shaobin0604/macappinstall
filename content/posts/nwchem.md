---
title: "Install nwchem on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "High-performance computational chemistry tools"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install nwchem on MacOS using homebrew

- App Name: nwchem
- App description: High-performance computational chemistry tools
- App Version: 7.0.2
- App Website: https://nwchemgit.github.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install nwchem with the following command
   ```
   brew install nwchem
   ```
4. nwchem is ready to use now!