---
title: "Install zsh-navigation-tools on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Zsh curses-based tools, e.g. multi-word history searcher"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install zsh-navigation-tools on MacOS using homebrew

- App Name: zsh-navigation-tools
- App description: Zsh curses-based tools, e.g. multi-word history searcher
- App Version: 2.2.7
- App Website: https://github.com/psprint/zsh-navigation-tools

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install zsh-navigation-tools with the following command
   ```
   brew install zsh-navigation-tools
   ```
4. zsh-navigation-tools is ready to use now!