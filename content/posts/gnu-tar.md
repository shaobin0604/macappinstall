---
title: "Install gnu-tar on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GNU version of the tar archiving utility"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gnu-tar on MacOS using homebrew

- App Name: gnu-tar
- App description: GNU version of the tar archiving utility
- App Version: 1.34
- App Website: https://www.gnu.org/software/tar/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gnu-tar with the following command
   ```
   brew install gnu-tar
   ```
4. gnu-tar is ready to use now!