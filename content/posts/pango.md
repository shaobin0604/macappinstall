---
title: "Install pango on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Framework for layout and rendering of i18n text"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pango on MacOS using homebrew

- App Name: pango
- App description: Framework for layout and rendering of i18n text
- App Version: 1.50.4
- App Website: https://pango.gnome.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pango with the following command
   ```
   brew install pango
   ```
4. pango is ready to use now!