---
title: "Install rbtools on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CLI and API for working with code and document reviews on Review Board"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install rbtools on MacOS using homebrew

- App Name: rbtools
- App description: CLI and API for working with code and document reviews on Review Board
- App Version: 2.0.1
- App Website: https://www.reviewboard.org/downloads/rbtools/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install rbtools with the following command
   ```
   brew install rbtools
   ```
4. rbtools is ready to use now!