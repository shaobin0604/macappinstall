---
title: "Install Jettison on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Automatically ejects external drives"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Jettison on MacOS using homebrew

- App Name: Jettison
- App description: Automatically ejects external drives
- App Version: 1.8.4,3396
- App Website: https://stclairsoft.com/Jettison/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Jettison with the following command
   ```
   brew install --cask jettison
   ```
4. Jettison is ready to use now!