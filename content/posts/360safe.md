---
title: "Install 360 Total Security on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Protection and antivirus security"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install 360 Total Security on MacOS using homebrew

- App Name: 360 Total Security
- App description: Protection and antivirus security
- App Version: 1.2.6
- App Website: https://www.360totalsecurity.com/features/360-total-security-mac/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install 360 Total Security with the following command
   ```
   brew install --cask 360safe
   ```
4. 360 Total Security is ready to use now!