---
title: "Install myman on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Text-mode videogame inspired by Namco's Pac-Man"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install myman on MacOS using homebrew

- App Name: myman
- App description: Text-mode videogame inspired by Namco's Pac-Man
- App Version: 2009-10-30
- App Website: https://myman.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install myman with the following command
   ```
   brew install myman
   ```
4. myman is ready to use now!