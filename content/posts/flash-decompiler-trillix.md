---
title: "Install Flash Decompiler Trillix on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Flash Decompiler Trillix on MacOS using homebrew

- App Name: Flash Decompiler Trillix
- App description: null
- App Version: 5.3.1301
- App Website: https://www.flash-decompiler.com/mac.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Flash Decompiler Trillix with the following command
   ```
   brew install --cask flash-decompiler-trillix
   ```
4. Flash Decompiler Trillix is ready to use now!