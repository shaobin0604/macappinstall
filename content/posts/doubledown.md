---
title: "Install doubledown on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Sync local changes to a remote directory"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install doubledown on MacOS using homebrew

- App Name: doubledown
- App description: Sync local changes to a remote directory
- App Version: 0.0.2
- App Website: https://github.com/devstructure/doubledown

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install doubledown with the following command
   ```
   brew install doubledown
   ```
4. doubledown is ready to use now!