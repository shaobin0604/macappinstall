---
title: "Install SQLPro for MySQL on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "MySQL & MariaDB database client"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install SQLPro for MySQL on MacOS using homebrew

- App Name: SQLPro for MySQL
- App description: MySQL & MariaDB database client
- App Version: 2021.53
- App Website: https://www.mysqlui.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install SQLPro for MySQL with the following command
   ```
   brew install --cask sqlpro-for-mysql
   ```
4. SQLPro for MySQL is ready to use now!