---
title: "Install tre on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Lightweight, POSIX-compliant regular expression (regex) library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tre on MacOS using homebrew

- App Name: tre
- App description: Lightweight, POSIX-compliant regular expression (regex) library
- App Version: 0.8.0
- App Website: https://laurikari.net/tre/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tre with the following command
   ```
   brew install tre
   ```
4. tre is ready to use now!