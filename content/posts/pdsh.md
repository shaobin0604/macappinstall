---
title: "Install pdsh on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Efficient rsh-like utility, for using hosts in parallel"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pdsh on MacOS using homebrew

- App Name: pdsh
- App description: Efficient rsh-like utility, for using hosts in parallel
- App Version: 2.34
- App Website: https://github.com/chaos/pdsh

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pdsh with the following command
   ```
   brew install pdsh
   ```
4. pdsh is ready to use now!