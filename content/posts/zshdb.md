---
title: "Install zshdb on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Debugger for zsh"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install zshdb on MacOS using homebrew

- App Name: zshdb
- App description: Debugger for zsh
- App Version: 1.1.2
- App Website: https://github.com/rocky/zshdb

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install zshdb with the following command
   ```
   brew install zshdb
   ```
4. zshdb is ready to use now!