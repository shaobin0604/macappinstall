---
title: "Install node-sass on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "JavaScript implementation of a Sass compiler"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install node-sass on MacOS using homebrew

- App Name: node-sass
- App description: JavaScript implementation of a Sass compiler
- App Version: 1.49.8
- App Website: https://github.com/sass/dart-sass

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install node-sass with the following command
   ```
   brew install node-sass
   ```
4. node-sass is ready to use now!