---
title: "Install FreeSurfer on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Open source software suite for processing and analyzing brain MRI images"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install FreeSurfer on MacOS using homebrew

- App Name: FreeSurfer
- App description: Open source software suite for processing and analyzing brain MRI images
- App Version: latest
- App Website: https://surfer.nmr.mgh.harvard.edu/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install FreeSurfer with the following command
   ```
   brew install --cask freesurfer
   ```
4. FreeSurfer is ready to use now!