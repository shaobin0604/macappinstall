---
title: "Install TheBrain on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Mind mapping and personal knowledge base software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install TheBrain on MacOS using homebrew

- App Name: TheBrain
- App description: Mind mapping and personal knowledge base software
- App Version: 12.0.80.0
- App Website: https://www.thebrain.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install TheBrain with the following command
   ```
   brew install --cask thebrain
   ```
4. TheBrain is ready to use now!