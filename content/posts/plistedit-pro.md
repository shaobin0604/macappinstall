---
title: "Install PlistEdit Pro on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Property list and JSON editor"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install PlistEdit Pro on MacOS using homebrew

- App Name: PlistEdit Pro
- App description: Property list and JSON editor
- App Version: 1.9.2,908
- App Website: https://www.fatcatsoftware.com/plisteditpro/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install PlistEdit Pro with the following command
   ```
   brew install --cask plistedit-pro
   ```
4. PlistEdit Pro is ready to use now!