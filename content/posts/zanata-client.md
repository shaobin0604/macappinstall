---
title: "Install zanata-client on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Zanata translation system command-line client"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install zanata-client on MacOS using homebrew

- App Name: zanata-client
- App description: Zanata translation system command-line client
- App Version: 4.6.2
- App Website: http://zanata.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install zanata-client with the following command
   ```
   brew install zanata-client
   ```
4. zanata-client is ready to use now!