---
title: "Install texapp on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "App.net client based on TTYtter"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install texapp on MacOS using homebrew

- App Name: texapp
- App description: App.net client based on TTYtter
- App Version: 0.6.11
- App Website: https://www.floodgap.com/software/texapp/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install texapp with the following command
   ```
   brew install texapp
   ```
4. texapp is ready to use now!