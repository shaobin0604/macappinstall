---
title: "Install riemann-client on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C client library for the Riemann monitoring system"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install riemann-client on MacOS using homebrew

- App Name: riemann-client
- App description: C client library for the Riemann monitoring system
- App Version: 1.10.5
- App Website: https://github.com/algernon/riemann-c-client

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install riemann-client with the following command
   ```
   brew install riemann-client
   ```
4. riemann-client is ready to use now!