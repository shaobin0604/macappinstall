---
title: "Install pssh on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Parallel versions of OpenSSH and related tools"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pssh on MacOS using homebrew

- App Name: pssh
- App description: Parallel versions of OpenSSH and related tools
- App Version: 2.3.1
- App Website: https://code.google.com/archive/p/parallel-ssh/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pssh with the following command
   ```
   brew install pssh
   ```
4. pssh is ready to use now!