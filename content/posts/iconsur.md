---
title: "Install iconsur on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "macOS Big Sur Adaptive Icon Generator"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install iconsur on MacOS using homebrew

- App Name: iconsur
- App description: macOS Big Sur Adaptive Icon Generator
- App Version: 1.6.2
- App Website: https://github.com/rikumi/iconsur

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install iconsur with the following command
   ```
   brew install iconsur
   ```
4. iconsur is ready to use now!