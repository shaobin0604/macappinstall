---
title: "Install soapyrtlsdr on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "SoapySDR RTL-SDR Support Module"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install soapyrtlsdr on MacOS using homebrew

- App Name: soapyrtlsdr
- App description: SoapySDR RTL-SDR Support Module
- App Version: 0.3.2
- App Website: https://github.com/pothosware/SoapyRTLSDR/wiki

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install soapyrtlsdr with the following command
   ```
   brew install soapyrtlsdr
   ```
4. soapyrtlsdr is ready to use now!