---
title: "Install libsmf on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C library for handling SMF ('*.mid') files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libsmf on MacOS using homebrew

- App Name: libsmf
- App description: C library for handling SMF ('*.mid') files
- App Version: 1.3
- App Website: https://sourceforge.net/projects/libsmf/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libsmf with the following command
   ```
   brew install libsmf
   ```
4. libsmf is ready to use now!