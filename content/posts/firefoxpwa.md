---
title: "Install firefoxpwa on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to install, manage and use Progressive Web Apps in Mozilla Firefox"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install firefoxpwa on MacOS using homebrew

- App Name: firefoxpwa
- App description: Tool to install, manage and use Progressive Web Apps in Mozilla Firefox
- App Version: 1.4.0
- App Website: https://github.com/filips123/PWAsForFirefox

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install firefoxpwa with the following command
   ```
   brew install firefoxpwa
   ```
4. firefoxpwa is ready to use now!