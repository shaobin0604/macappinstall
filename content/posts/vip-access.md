---
title: "Install Symantec VIP Access on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Two-step authentication software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Symantec VIP Access on MacOS using homebrew

- App Name: Symantec VIP Access
- App description: Two-step authentication software
- App Version: 1.0.7
- App Website: https://vip.symantec.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Symantec VIP Access with the following command
   ```
   brew install --cask vip-access
   ```
4. Symantec VIP Access is ready to use now!