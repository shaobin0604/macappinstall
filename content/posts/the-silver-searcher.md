---
title: "Install the_silver_searcher on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Code-search similar to ack"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install the_silver_searcher on MacOS using homebrew

- App Name: the_silver_searcher
- App description: Code-search similar to ack
- App Version: 2.2.0
- App Website: https://github.com/ggreer/the_silver_searcher

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install the_silver_searcher with the following command
   ```
   brew install the_silver_searcher
   ```
4. the_silver_searcher is ready to use now!