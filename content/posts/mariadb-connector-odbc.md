---
title: "Install mariadb-connector-odbc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Database driver using the industry standard ODBC API"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mariadb-connector-odbc on MacOS using homebrew

- App Name: mariadb-connector-odbc
- App description: Database driver using the industry standard ODBC API
- App Version: 3.1.15
- App Website: https://mariadb.org/download/?tab=connector&prod=connector-odbc

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mariadb-connector-odbc with the following command
   ```
   brew install mariadb-connector-odbc
   ```
4. mariadb-connector-odbc is ready to use now!