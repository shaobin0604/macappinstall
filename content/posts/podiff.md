---
title: "Install podiff on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Compare textual information in two PO files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install podiff on MacOS using homebrew

- App Name: podiff
- App description: Compare textual information in two PO files
- App Version: 1.3
- App Website: https://puszcza.gnu.org.ua/software/podiff/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install podiff with the following command
   ```
   brew install podiff
   ```
4. podiff is ready to use now!