---
title: "Install MilkyTracker on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Music tracker compatible with FT2"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install MilkyTracker on MacOS using homebrew

- App Name: MilkyTracker
- App description: Music tracker compatible with FT2
- App Version: 1.03.00
- App Website: https://milkytracker.titandemo.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install MilkyTracker with the following command
   ```
   brew install --cask milkytracker
   ```
4. MilkyTracker is ready to use now!