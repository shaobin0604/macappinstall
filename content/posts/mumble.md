---
title: "Install Mumble on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Open-source, low-latency, high quality voice chat software for gaming"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Mumble on MacOS using homebrew

- App Name: Mumble
- App description: Open-source, low-latency, high quality voice chat software for gaming
- App Version: 1.4.230
- App Website: https://wiki.mumble.info/wiki/Main_Page

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Mumble with the following command
   ```
   brew install --cask mumble
   ```
4. Mumble is ready to use now!