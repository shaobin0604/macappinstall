---
title: "Install redex on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Bytecode optimizer for Android apps"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install redex on MacOS using homebrew

- App Name: redex
- App description: Bytecode optimizer for Android apps
- App Version: 2017.10.31
- App Website: https://fbredex.com

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install redex with the following command
   ```
   brew install redex
   ```
4. redex is ready to use now!