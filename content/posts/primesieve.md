---
title: "Install primesieve on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Fast C/C++ prime number generator"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install primesieve on MacOS using homebrew

- App Name: primesieve
- App description: Fast C/C++ prime number generator
- App Version: 7.8
- App Website: https://github.com/kimwalisch/primesieve

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install primesieve with the following command
   ```
   brew install primesieve
   ```
4. primesieve is ready to use now!