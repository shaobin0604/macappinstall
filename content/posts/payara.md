---
title: "Install payara on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Java EE application server forked from GlassFish"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install payara on MacOS using homebrew

- App Name: payara
- App description: Java EE application server forked from GlassFish
- App Version: 5.2021.10
- App Website: https://www.payara.fish

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install payara with the following command
   ```
   brew install payara
   ```
4. payara is ready to use now!