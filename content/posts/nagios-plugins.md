---
title: "Install nagios-plugins on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Plugins for the nagios network monitoring system"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install nagios-plugins on MacOS using homebrew

- App Name: nagios-plugins
- App description: Plugins for the nagios network monitoring system
- App Version: 2.3.3
- App Website: https://www.nagios-plugins.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install nagios-plugins with the following command
   ```
   brew install nagios-plugins
   ```
4. nagios-plugins is ready to use now!