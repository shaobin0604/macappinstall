---
title: "Install SwitchResX on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Controls screen display settings"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install SwitchResX on MacOS using homebrew

- App Name: SwitchResX
- App description: Controls screen display settings
- App Version: 4.11.3
- App Website: https://www.madrau.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install SwitchResX with the following command
   ```
   brew install --cask switchresx
   ```
4. SwitchResX is ready to use now!