---
title: "Install djl-serving on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "This module contains an universal model serving implementation"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install djl-serving on MacOS using homebrew

- App Name: djl-serving
- App description: This module contains an universal model serving implementation
- App Version: 0.15.0
- App Website: https://github.com/deepjavalibrary/djl-serving

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install djl-serving with the following command
   ```
   brew install djl-serving
   ```
4. djl-serving is ready to use now!