---
title: "Install wxpython on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Python bindings for wxWidgets"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install wxpython on MacOS using homebrew

- App Name: wxpython
- App description: Python bindings for wxWidgets
- App Version: 4.1.1
- App Website: https://www.wxpython.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install wxpython with the following command
   ```
   brew install wxpython
   ```
4. wxpython is ready to use now!