---
title: "Install Slippi on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Fork of the Dolphin GameCube and Wii emulator with netplay support via Slippi"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Slippi on MacOS using homebrew

- App Name: Slippi
- App description: Fork of the Dolphin GameCube and Wii emulator with netplay support via Slippi
- App Version: 2.3.3
- App Website: https://slippi.gg/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Slippi with the following command
   ```
   brew install --cask slippi-dolphin
   ```
4. Slippi is ready to use now!