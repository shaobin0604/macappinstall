---
title: "Install rhino on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "JavaScript engine"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install rhino on MacOS using homebrew

- App Name: rhino
- App description: JavaScript engine
- App Version: 1.7.14
- App Website: https://mozilla.github.io/rhino/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install rhino with the following command
   ```
   brew install rhino
   ```
4. rhino is ready to use now!