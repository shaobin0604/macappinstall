---
title: "Install imap-backup on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Backup GMail (or other IMAP) accounts to disk"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install imap-backup on MacOS using homebrew

- App Name: imap-backup
- App description: Backup GMail (or other IMAP) accounts to disk
- App Version: 5.1.0
- App Website: https://github.com/joeyates/imap-backup

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install imap-backup with the following command
   ```
   brew install imap-backup
   ```
4. imap-backup is ready to use now!