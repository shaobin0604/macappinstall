---
title: "Install git-crypt on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Enable transparent encryption/decryption of files in a git repo"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install git-crypt on MacOS using homebrew

- App Name: git-crypt
- App description: Enable transparent encryption/decryption of files in a git repo
- App Version: 0.6.0
- App Website: https://www.agwa.name/projects/git-crypt/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install git-crypt with the following command
   ```
   brew install git-crypt
   ```
4. git-crypt is ready to use now!