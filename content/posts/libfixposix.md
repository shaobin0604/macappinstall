---
title: "Install libfixposix on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Thin wrapper over POSIX syscalls"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libfixposix on MacOS using homebrew

- App Name: libfixposix
- App description: Thin wrapper over POSIX syscalls
- App Version: 0.4.3
- App Website: https://github.com/sionescu/libfixposix

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libfixposix with the following command
   ```
   brew install libfixposix
   ```
4. libfixposix is ready to use now!