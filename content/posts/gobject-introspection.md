---
title: "Install gobject-introspection on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Generate introspection data for GObject libraries"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gobject-introspection on MacOS using homebrew

- App Name: gobject-introspection
- App description: Generate introspection data for GObject libraries
- App Version: 1.70.0
- App Website: https://gi.readthedocs.io/en/latest/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gobject-introspection with the following command
   ```
   brew install gobject-introspection
   ```
4. gobject-introspection is ready to use now!