---
title: "Install JetBrains MPS on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Create your own domain-specific language"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install JetBrains MPS on MacOS using homebrew

- App Name: JetBrains MPS
- App description: Create your own domain-specific language
- App Version: 2021.3,213.6777.846
- App Website: https://www.jetbrains.com/mps/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install JetBrains MPS with the following command
   ```
   brew install --cask mps
   ```
4. JetBrains MPS is ready to use now!