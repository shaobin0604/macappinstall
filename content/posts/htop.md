---
title: "Install htop on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Improved top (interactive process viewer)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install htop on MacOS using homebrew

- App Name: htop
- App description: Improved top (interactive process viewer)
- App Version: 3.1.2
- App Website: https://htop.dev/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install htop with the following command
   ```
   brew install htop
   ```
4. htop is ready to use now!