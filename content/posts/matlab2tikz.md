---
title: "Install matlab2tikz on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Convert MATLAB(R) figures into TikZ/Pgfplots figures"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install matlab2tikz on MacOS using homebrew

- App Name: matlab2tikz
- App description: Convert MATLAB(R) figures into TikZ/Pgfplots figures
- App Version: 1.1.0
- App Website: https://github.com/matlab2tikz/matlab2tikz

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install matlab2tikz with the following command
   ```
   brew install matlab2tikz
   ```
4. matlab2tikz is ready to use now!