---
title: "Install yosys on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Framework for Verilog RTL synthesis"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install yosys on MacOS using homebrew

- App Name: yosys
- App description: Framework for Verilog RTL synthesis
- App Version: 0.14
- App Website: https://yosyshq.net/yosys/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install yosys with the following command
   ```
   brew install yosys
   ```
4. yosys is ready to use now!