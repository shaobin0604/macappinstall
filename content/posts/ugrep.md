---
title: "Install ugrep on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Ultra fast grep with query UI, fuzzy search, archive search, and more"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ugrep on MacOS using homebrew

- App Name: ugrep
- App description: Ultra fast grep with query UI, fuzzy search, archive search, and more
- App Version: 3.7.2
- App Website: https://github.com/Genivia/ugrep

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ugrep with the following command
   ```
   brew install ugrep
   ```
4. ugrep is ready to use now!