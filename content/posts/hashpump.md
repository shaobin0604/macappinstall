---
title: "Install hashpump on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to exploit hash length extension attack"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install hashpump on MacOS using homebrew

- App Name: hashpump
- App description: Tool to exploit hash length extension attack
- App Version: 1.2.0
- App Website: https://github.com/bwall/HashPump

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install hashpump with the following command
   ```
   brew install hashpump
   ```
4. hashpump is ready to use now!