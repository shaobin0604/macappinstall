---
title: "Install SwiftyBeaver on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install SwiftyBeaver on MacOS using homebrew

- App Name: SwiftyBeaver
- App description: null
- App Version: 1.1.0,1100
- App Website: https://swiftybeaver.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install SwiftyBeaver with the following command
   ```
   brew install --cask swiftybeaver
   ```
4. SwiftyBeaver is ready to use now!