---
title: "Install USB Overdrive on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "USB and Bluetooth device driver"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install USB Overdrive on MacOS using homebrew

- App Name: USB Overdrive
- App description: USB and Bluetooth device driver
- App Version: 5.1
- App Website: https://www.usboverdrive.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install USB Overdrive with the following command
   ```
   brew install --cask usb-overdrive
   ```
4. USB Overdrive is ready to use now!