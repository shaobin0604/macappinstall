---
title: "Install hidapi on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library for communicating with USB and Bluetooth HID devices"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install hidapi on MacOS using homebrew

- App Name: hidapi
- App description: Library for communicating with USB and Bluetooth HID devices
- App Version: 0.11.2
- App Website: https://github.com/libusb/hidapi

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install hidapi with the following command
   ```
   brew install hidapi
   ```
4. hidapi is ready to use now!