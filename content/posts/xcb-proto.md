---
title: "Install xcb-proto on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "X.Org: XML-XCB protocol descriptions for libxcb code generation"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install xcb-proto on MacOS using homebrew

- App Name: xcb-proto
- App description: X.Org: XML-XCB protocol descriptions for libxcb code generation
- App Version: 1.14.1
- App Website: https://www.x.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install xcb-proto with the following command
   ```
   brew install xcb-proto
   ```
4. xcb-proto is ready to use now!