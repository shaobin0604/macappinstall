---
title: "Install shared-mime-info on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Database of common MIME types"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install shared-mime-info on MacOS using homebrew

- App Name: shared-mime-info
- App description: Database of common MIME types
- App Version: 2.1
- App Website: https://wiki.freedesktop.org/www/Software/shared-mime-info

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install shared-mime-info with the following command
   ```
   brew install shared-mime-info
   ```
4. shared-mime-info is ready to use now!