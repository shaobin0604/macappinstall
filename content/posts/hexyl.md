---
title: "Install hexyl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line hex viewer"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install hexyl on MacOS using homebrew

- App Name: hexyl
- App description: Command-line hex viewer
- App Version: 0.9.0
- App Website: https://github.com/sharkdp/hexyl

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install hexyl with the following command
   ```
   brew install hexyl
   ```
4. hexyl is ready to use now!