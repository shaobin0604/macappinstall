---
title: "Install GZDoom on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Adds an OpenGL renderer to the ZDoom source port"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install GZDoom on MacOS using homebrew

- App Name: GZDoom
- App description: Adds an OpenGL renderer to the ZDoom source port
- App Version: 4.7.1
- App Website: https://zdoom.org/index

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install GZDoom with the following command
   ```
   brew install --cask gzdoom
   ```
4. GZDoom is ready to use now!