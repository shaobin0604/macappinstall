---
title: "Install Tribler on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Privacy enhanced BitTorrent client with P2P content discovery"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Tribler on MacOS using homebrew

- App Name: Tribler
- App description: Privacy enhanced BitTorrent client with P2P content discovery
- App Version: 7.11.0
- App Website: https://www.tribler.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Tribler with the following command
   ```
   brew install --cask tribler
   ```
4. Tribler is ready to use now!