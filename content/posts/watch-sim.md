---
title: "Install watch-sim on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line WatchKit application launcher"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install watch-sim on MacOS using homebrew

- App Name: watch-sim
- App description: Command-line WatchKit application launcher
- App Version: 1.0.0
- App Website: https://github.com/alloy/watch-sim

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install watch-sim with the following command
   ```
   brew install watch-sim
   ```
4. watch-sim is ready to use now!