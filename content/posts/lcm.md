---
title: "Install lcm on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Libraries and tools for message passing and data marshalling"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install lcm on MacOS using homebrew

- App Name: lcm
- App description: Libraries and tools for message passing and data marshalling
- App Version: 1.4.0
- App Website: https://lcm-proj.github.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install lcm with the following command
   ```
   brew install lcm
   ```
4. lcm is ready to use now!