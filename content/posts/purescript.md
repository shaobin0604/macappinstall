---
title: "Install purescript on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Strongly typed programming language that compiles to JavaScript"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install purescript on MacOS using homebrew

- App Name: purescript
- App description: Strongly typed programming language that compiles to JavaScript
- App Version: 0.14.5
- App Website: https://www.purescript.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install purescript with the following command
   ```
   brew install purescript
   ```
4. purescript is ready to use now!