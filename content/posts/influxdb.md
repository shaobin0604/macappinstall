---
title: "Install influxdb on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Time series, events, and metrics database"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install influxdb on MacOS using homebrew

- App Name: influxdb
- App description: Time series, events, and metrics database
- App Version: 2.1.1
- App Website: https://influxdata.com/time-series-platform/influxdb/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install influxdb with the following command
   ```
   brew install influxdb
   ```
4. influxdb is ready to use now!