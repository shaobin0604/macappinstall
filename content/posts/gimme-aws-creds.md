---
title: "Install gimme-aws-creds on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CLI to retrieve AWS credentials from Okta"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gimme-aws-creds on MacOS using homebrew

- App Name: gimme-aws-creds
- App description: CLI to retrieve AWS credentials from Okta
- App Version: 2.4.3
- App Website: https://github.com/Nike-Inc/gimme-aws-creds

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gimme-aws-creds with the following command
   ```
   brew install gimme-aws-creds
   ```
4. gimme-aws-creds is ready to use now!