---
title: "Install pcal on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Generate Postscript calendars without X"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pcal on MacOS using homebrew

- App Name: pcal
- App description: Generate Postscript calendars without X
- App Version: 4.11.0
- App Website: https://pcal.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pcal with the following command
   ```
   brew install pcal
   ```
4. pcal is ready to use now!