---
title: "Install dirac on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "General-purpose video codec aimed at a range of resolutions"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dirac on MacOS using homebrew

- App Name: dirac
- App description: General-purpose video codec aimed at a range of resolutions
- App Version: 1.0.2
- App Website: https://sourceforge.net/projects/dirac/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dirac with the following command
   ```
   brew install dirac
   ```
4. dirac is ready to use now!