---
title: "Install nsd on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Name server daemon"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install nsd on MacOS using homebrew

- App Name: nsd
- App description: Name server daemon
- App Version: 4.3.9
- App Website: https://www.nlnetlabs.nl/projects/nsd/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install nsd with the following command
   ```
   brew install nsd
   ```
4. nsd is ready to use now!