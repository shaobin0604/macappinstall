---
title: "Install Origami Studio on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Design, animate, and prototype design tool from Facebook"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Origami Studio on MacOS using homebrew

- App Name: Origami Studio
- App description: Design, animate, and prototype design tool from Facebook
- App Version: 104.0.0.27.237,339595936
- App Website: https://origami.design/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Origami Studio with the following command
   ```
   brew install --cask origami-studio
   ```
4. Origami Studio is ready to use now!