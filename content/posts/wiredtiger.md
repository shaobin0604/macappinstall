---
title: "Install wiredtiger on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "High performance NoSQL extensible platform for data management"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install wiredtiger on MacOS using homebrew

- App Name: wiredtiger
- App description: High performance NoSQL extensible platform for data management
- App Version: 10.0.0
- App Website: https://source.wiredtiger.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install wiredtiger with the following command
   ```
   brew install wiredtiger
   ```
4. wiredtiger is ready to use now!