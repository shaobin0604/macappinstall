---
title: "Install tmate on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Instant terminal sharing"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tmate on MacOS using homebrew

- App Name: tmate
- App description: Instant terminal sharing
- App Version: 2.4.0
- App Website: https://tmate.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tmate with the following command
   ```
   brew install tmate
   ```
4. tmate is ready to use now!