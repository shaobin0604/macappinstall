---
title: "Install asciidoctorj on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Java wrapper and bindings for Asciidoctor"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install asciidoctorj on MacOS using homebrew

- App Name: asciidoctorj
- App description: Java wrapper and bindings for Asciidoctor
- App Version: 2.5.3
- App Website: https://github.com/asciidoctor/asciidoctorj

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install asciidoctorj with the following command
   ```
   brew install asciidoctorj
   ```
4. asciidoctorj is ready to use now!