---
title: "Install pixman on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Low-level library for pixel manipulation"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pixman on MacOS using homebrew

- App Name: pixman
- App description: Low-level library for pixel manipulation
- App Version: 0.40.0
- App Website: https://cairographics.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pixman with the following command
   ```
   brew install pixman
   ```
4. pixman is ready to use now!