---
title: "Install libgfshare on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library for sharing secrets"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libgfshare on MacOS using homebrew

- App Name: libgfshare
- App description: Library for sharing secrets
- App Version: 2.0.0
- App Website: https://www.digital-scurf.org/software/libgfshare

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libgfshare with the following command
   ```
   brew install libgfshare
   ```
4. libgfshare is ready to use now!