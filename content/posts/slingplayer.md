---
title: "Install Slingplayer Desktop on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Desktop application for Sling products"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Slingplayer Desktop on MacOS using homebrew

- App Name: Slingplayer Desktop
- App description: Desktop application for Sling products
- App Version: 5.0.92
- App Website: https://www.slingbox.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Slingplayer Desktop with the following command
   ```
   brew install --cask slingplayer
   ```
4. Slingplayer Desktop is ready to use now!