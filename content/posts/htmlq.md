---
title: "Install htmlq on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Uses CSS selectors to extract bits content from HTML files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install htmlq on MacOS using homebrew

- App Name: htmlq
- App description: Uses CSS selectors to extract bits content from HTML files
- App Version: 0.4.0
- App Website: https://github.com/mgdm/htmlq

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install htmlq with the following command
   ```
   brew install htmlq
   ```
4. htmlq is ready to use now!