---
title: "Install libevhtp on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Create extremely-fast and secure embedded HTTP servers with ease"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libevhtp on MacOS using homebrew

- App Name: libevhtp
- App description: Create extremely-fast and secure embedded HTTP servers with ease
- App Version: 1.2.18
- App Website: https://github.com/criticalstack/libevhtp/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libevhtp with the following command
   ```
   brew install libevhtp
   ```
4. libevhtp is ready to use now!