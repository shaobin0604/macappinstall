---
title: "Install cpprestsdk on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C++ libraries for cloud-based client-server communication"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cpprestsdk on MacOS using homebrew

- App Name: cpprestsdk
- App description: C++ libraries for cloud-based client-server communication
- App Version: 2.10.18
- App Website: https://github.com/Microsoft/cpprestsdk

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cpprestsdk with the following command
   ```
   brew install cpprestsdk
   ```
4. cpprestsdk is ready to use now!