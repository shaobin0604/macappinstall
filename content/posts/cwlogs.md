---
title: "Install cwlogs on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CLI tool for reading logs from Cloudwatch Logs"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cwlogs on MacOS using homebrew

- App Name: cwlogs
- App description: CLI tool for reading logs from Cloudwatch Logs
- App Version: 1.2.0
- App Website: https://github.com/segmentio/cwlogs

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cwlogs with the following command
   ```
   brew install cwlogs
   ```
4. cwlogs is ready to use now!