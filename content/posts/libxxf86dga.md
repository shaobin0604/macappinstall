---
title: "Install libxxf86dga on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "X.Org: XFree86-DGA X extension"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libxxf86dga on MacOS using homebrew

- App Name: libxxf86dga
- App description: X.Org: XFree86-DGA X extension
- App Version: 1.1.5
- App Website: https://www.x.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libxxf86dga with the following command
   ```
   brew install libxxf86dga
   ```
4. libxxf86dga is ready to use now!