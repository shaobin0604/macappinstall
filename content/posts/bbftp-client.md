---
title: "Install bbftp-client on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Secure file transfer software, optimized for large files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bbftp-client on MacOS using homebrew

- App Name: bbftp-client
- App description: Secure file transfer software, optimized for large files
- App Version: 3.2.1
- App Website: http://software.in2p3.fr/bbftp/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bbftp-client with the following command
   ```
   brew install bbftp-client
   ```
4. bbftp-client is ready to use now!