---
title: "Install Vagrant VMware Utility on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Gives Vagrant VMware plugin access to various VMware functionalities"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Vagrant VMware Utility on MacOS using homebrew

- App Name: Vagrant VMware Utility
- App description: Gives Vagrant VMware plugin access to various VMware functionalities
- App Version: 1.0.21
- App Website: https://www.vagrantup.com/vmware/downloads.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Vagrant VMware Utility with the following command
   ```
   brew install --cask vagrant-vmware-utility
   ```
4. Vagrant VMware Utility is ready to use now!