---
title: "Install libxfixes on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "X.Org: Header files for the XFIXES extension"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libxfixes on MacOS using homebrew

- App Name: libxfixes
- App description: X.Org: Header files for the XFIXES extension
- App Version: 6.0.0
- App Website: https://www.x.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libxfixes with the following command
   ```
   brew install libxfixes
   ```
4. libxfixes is ready to use now!