---
title: "Install envoy@1.18 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cloud-native high-performance edge/middle/service proxy"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install envoy@1.18 on MacOS using homebrew

- App Name: envoy@1.18
- App description: Cloud-native high-performance edge/middle/service proxy
- App Version: 1.18.4
- App Website: https://www.envoyproxy.io/index.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install envoy@1.18 with the following command
   ```
   brew install envoy@1.18
   ```
4. envoy@1.18 is ready to use now!