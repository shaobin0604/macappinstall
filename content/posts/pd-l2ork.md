---
title: "Install Pd-l2ork on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Programming environment for computer music and multimedia applications"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Pd-l2ork on MacOS using homebrew

- App Name: Pd-l2ork
- App description: Programming environment for computer music and multimedia applications
- App Version: 2.17.0,20210417,ca592f6f
- App Website: https://agraef.github.io/purr-data/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Pd-l2ork with the following command
   ```
   brew install --cask pd-l2ork
   ```
4. Pd-l2ork is ready to use now!