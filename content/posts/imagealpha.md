---
title: "Install ImageAlpha on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utility to reduce the size of 24-bit PNG files"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ImageAlpha on MacOS using homebrew

- App Name: ImageAlpha
- App description: Utility to reduce the size of 24-bit PNG files
- App Version: 1.5.1
- App Website: https://pngmini.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ImageAlpha with the following command
   ```
   brew install --cask imagealpha
   ```
4. ImageAlpha is ready to use now!