---
title: "Install finatra on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Scala web framework inspired by Sinatra"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install finatra on MacOS using homebrew

- App Name: finatra
- App description: Scala web framework inspired by Sinatra
- App Version: 1.5.3
- App Website: http://finatra.info/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install finatra with the following command
   ```
   brew install finatra
   ```
4. finatra is ready to use now!