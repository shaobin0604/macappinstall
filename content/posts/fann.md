---
title: "Install fann on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Fast artificial neural network library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fann on MacOS using homebrew

- App Name: fann
- App description: Fast artificial neural network library
- App Version: 2.2.0
- App Website: https://sourceforge.net/projects/fann

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fann with the following command
   ```
   brew install fann
   ```
4. fann is ready to use now!