---
title: "Install p7zip on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "7-Zip (high compression file archiver) implementation"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install p7zip on MacOS using homebrew

- App Name: p7zip
- App description: 7-Zip (high compression file archiver) implementation
- App Version: 17.04
- App Website: https://github.com/jinfeihan57/p7zip

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install p7zip with the following command
   ```
   brew install p7zip
   ```
4. p7zip is ready to use now!