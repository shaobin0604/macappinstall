---
title: "Install libxinerama on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "X.Org: API for Xinerama extension to X11 Protocol"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libxinerama on MacOS using homebrew

- App Name: libxinerama
- App description: X.Org: API for Xinerama extension to X11 Protocol
- App Version: 1.1.4
- App Website: https://www.x.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libxinerama with the following command
   ```
   brew install libxinerama
   ```
4. libxinerama is ready to use now!