---
title: "Install cryptominisat on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Advanced SAT solver"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cryptominisat on MacOS using homebrew

- App Name: cryptominisat
- App description: Advanced SAT solver
- App Version: 5.8.0
- App Website: https://www.msoos.org/cryptominisat5/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cryptominisat with the following command
   ```
   brew install cryptominisat
   ```
4. cryptominisat is ready to use now!