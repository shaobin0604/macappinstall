---
title: "Install snappy on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Compression/decompression library aiming for high speed"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install snappy on MacOS using homebrew

- App Name: snappy
- App description: Compression/decompression library aiming for high speed
- App Version: 1.1.9
- App Website: https://google.github.io/snappy/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install snappy with the following command
   ```
   brew install snappy
   ```
4. snappy is ready to use now!