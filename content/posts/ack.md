---
title: "Install ack on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Search tool like grep, but optimized for programmers"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ack on MacOS using homebrew

- App Name: ack
- App description: Search tool like grep, but optimized for programmers
- App Version: 3.5.0
- App Website: https://beyondgrep.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ack with the following command
   ```
   brew install ack
   ```
4. ack is ready to use now!