---
title: "Install DrawBot on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Write Python scripts to generate two-dimensional graphics"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install DrawBot on MacOS using homebrew

- App Name: DrawBot
- App description: Write Python scripts to generate two-dimensional graphics
- App Version: 3.128
- App Website: https://www.drawbot.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install DrawBot with the following command
   ```
   brew install --cask drawbot
   ```
4. DrawBot is ready to use now!