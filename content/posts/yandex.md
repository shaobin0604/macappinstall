---
title: "Install Yandex.Browser on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Web browser"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Yandex.Browser on MacOS using homebrew

- App Name: Yandex.Browser
- App description: Web browser
- App Version: 22.1.0
- App Website: https://browser.yandex.ru/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Yandex.Browser with the following command
   ```
   brew install --cask yandex
   ```
4. Yandex.Browser is ready to use now!