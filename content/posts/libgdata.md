---
title: "Install libgdata on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GLib-based library for accessing online service APIs"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libgdata on MacOS using homebrew

- App Name: libgdata
- App description: GLib-based library for accessing online service APIs
- App Version: 0.18.1
- App Website: https://wiki.gnome.org/Projects/libgdata

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libgdata with the following command
   ```
   brew install libgdata
   ```
4. libgdata is ready to use now!