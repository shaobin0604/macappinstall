---
title: "Install Cinderella on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Interactive Geometry Software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Cinderella on MacOS using homebrew

- App Name: Cinderella
- App description: Interactive Geometry Software
- App Version: 3.0b.2039
- App Website: https://cinderella.de/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Cinderella with the following command
   ```
   brew install --cask cinderella
   ```
4. Cinderella is ready to use now!