---
title: "Install jd on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "JSON diff and patch"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install jd on MacOS using homebrew

- App Name: jd
- App description: JSON diff and patch
- App Version: 1.5.1
- App Website: https://github.com/josephburnett/jd

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install jd with the following command
   ```
   brew install jd
   ```
4. jd is ready to use now!