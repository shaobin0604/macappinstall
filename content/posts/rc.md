---
title: "Install rc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Implementation of the AT&T Plan 9 shell"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install rc on MacOS using homebrew

- App Name: rc
- App description: Implementation of the AT&T Plan 9 shell
- App Version: 1.7.4
- App Website: http://doc.cat-v.org/plan_9/4th_edition/papers/rc

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install rc with the following command
   ```
   brew install rc
   ```
4. rc is ready to use now!