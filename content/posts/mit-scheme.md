---
title: "Install mit-scheme on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "MIT/GNU Scheme development tools and runtime library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mit-scheme on MacOS using homebrew

- App Name: mit-scheme
- App description: MIT/GNU Scheme development tools and runtime library
- App Version: 11.2
- App Website: https://www.gnu.org/software/mit-scheme/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mit-scheme with the following command
   ```
   brew install mit-scheme
   ```
4. mit-scheme is ready to use now!