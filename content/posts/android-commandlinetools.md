---
title: "Install Android SDK Command-line Tools on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line tools for building and debugging Android apps"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Android SDK Command-line Tools on MacOS using homebrew

- App Name: Android SDK Command-line Tools
- App description: Command-line tools for building and debugging Android apps
- App Version: 8092744
- App Website: https://developer.android.com/studio

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Android SDK Command-line Tools with the following command
   ```
   brew install --cask android-commandlinetools
   ```
4. Android SDK Command-line Tools is ready to use now!