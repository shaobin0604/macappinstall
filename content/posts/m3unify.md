---
title: "Install M3Unify on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install M3Unify on MacOS using homebrew

- App Name: M3Unify
- App description: null
- App Version: 2.2.0,2200027
- App Website: https://dougscripts.com/apps/m3unifyapp.php

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install M3Unify with the following command
   ```
   brew install --cask m3unify
   ```
4. M3Unify is ready to use now!