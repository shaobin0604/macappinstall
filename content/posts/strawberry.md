---
title: "Install Strawberry on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Music player and music collection organizer"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Strawberry on MacOS using homebrew

- App Name: Strawberry
- App description: Music player and music collection organizer
- App Version: 1.0.1
- App Website: https://www.strawberrymusicplayer.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Strawberry with the following command
   ```
   brew install --cask strawberry
   ```
4. Strawberry is ready to use now!