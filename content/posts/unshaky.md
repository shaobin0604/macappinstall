---
title: "Install Unshaky on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Software fix for double key presses on Apple's butterfly keyboard"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Unshaky on MacOS using homebrew

- App Name: Unshaky
- App description: Software fix for double key presses on Apple's butterfly keyboard
- App Version: 0.7.0
- App Website: https://github.com/aahung/Unshaky

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Unshaky with the following command
   ```
   brew install --cask unshaky
   ```
4. Unshaky is ready to use now!