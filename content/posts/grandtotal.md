---
title: "Install GrandTotal on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Create invoices and estimates"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install GrandTotal on MacOS using homebrew

- App Name: GrandTotal
- App description: Create invoices and estimates
- App Version: 7.2.5
- App Website: https://www.mediaatelier.com/GrandTotal7/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install GrandTotal with the following command
   ```
   brew install --cask grandtotal
   ```
4. GrandTotal is ready to use now!