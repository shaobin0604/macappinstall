---
title: "Install PlasicSCM - a Cloud Edition on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Install PlasticSCM locally and join a Cloud Edition subscription"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install PlasicSCM - a Cloud Edition on MacOS using homebrew

- App Name: PlasicSCM - a Cloud Edition
- App description: Install PlasticSCM locally and join a Cloud Edition subscription
- App Version: 10.0.16.6375
- App Website: https://www.plasticscm.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install PlasicSCM - a Cloud Edition with the following command
   ```
   brew install --cask plasticscm-cloud-edition
   ```
4. PlasicSCM - a Cloud Edition is ready to use now!