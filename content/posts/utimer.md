---
title: "Install utimer on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Multifunction timer tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install utimer on MacOS using homebrew

- App Name: utimer
- App description: Multifunction timer tool
- App Version: 0.4
- App Website: https://launchpad.net/utimer

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install utimer with the following command
   ```
   brew install utimer
   ```
4. utimer is ready to use now!