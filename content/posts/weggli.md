---
title: "Install weggli on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Fast and robust semantic search tool for C and C++ codebases"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install weggli on MacOS using homebrew

- App Name: weggli
- App description: Fast and robust semantic search tool for C and C++ codebases
- App Version: 0.2.3
- App Website: https://github.com/googleprojectzero/weggli

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install weggli with the following command
   ```
   brew install weggli
   ```
4. weggli is ready to use now!