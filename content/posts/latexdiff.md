---
title: "Install latexdiff on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Compare and mark up LaTeX file differences"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install latexdiff on MacOS using homebrew

- App Name: latexdiff
- App description: Compare and mark up LaTeX file differences
- App Version: 1.3.2
- App Website: https://www.ctan.org/pkg/latexdiff

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install latexdiff with the following command
   ```
   brew install latexdiff
   ```
4. latexdiff is ready to use now!