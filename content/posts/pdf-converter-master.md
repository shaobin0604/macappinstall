---
title: "Install PDF Converter Master on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install PDF Converter Master on MacOS using homebrew

- App Name: PDF Converter Master
- App description: null
- App Version: 6.2.0
- App Website: https://www.lightenpdf.com/pdf-converter-mac.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install PDF Converter Master with the following command
   ```
   brew install --cask pdf-converter-master
   ```
4. PDF Converter Master is ready to use now!