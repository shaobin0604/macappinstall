---
title: "Install MxSrvs on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "ANMP (Apache, Nginx, MySQL, PHP) ingintegrated development environment"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install MxSrvs on MacOS using homebrew

- App Name: MxSrvs
- App description: ANMP (Apache, Nginx, MySQL, PHP) ingintegrated development environment
- App Version: 1.2.1
- App Website: http://www.xsrvs.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install MxSrvs with the following command
   ```
   brew install --cask mxsrvs
   ```
4. MxSrvs is ready to use now!