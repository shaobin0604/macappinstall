---
title: "Install nq on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Unix command-line queue utility"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install nq on MacOS using homebrew

- App Name: nq
- App description: Unix command-line queue utility
- App Version: 0.4
- App Website: https://github.com/chneukirchen/nq

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install nq with the following command
   ```
   brew install nq
   ```
4. nq is ready to use now!