---
title: "Install dovecot on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "IMAP/POP3 server"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dovecot on MacOS using homebrew

- App Name: dovecot
- App description: IMAP/POP3 server
- App Version: 2.3.17
- App Website: https://dovecot.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dovecot with the following command
   ```
   brew install dovecot
   ```
4. dovecot is ready to use now!