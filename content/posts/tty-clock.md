---
title: "Install tty-clock on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Digital clock in ncurses"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tty-clock on MacOS using homebrew

- App Name: tty-clock
- App description: Digital clock in ncurses
- App Version: 2.3
- App Website: https://github.com/xorg62/tty-clock

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tty-clock with the following command
   ```
   brew install tty-clock
   ```
4. tty-clock is ready to use now!