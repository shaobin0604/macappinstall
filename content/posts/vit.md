---
title: "Install vit on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Full-screen terminal interface for Taskwarrior"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install vit on MacOS using homebrew

- App Name: vit
- App description: Full-screen terminal interface for Taskwarrior
- App Version: 2.1.0
- App Website: https://taskwarrior.org/news/news.20140406.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install vit with the following command
   ```
   brew install vit
   ```
4. vit is ready to use now!