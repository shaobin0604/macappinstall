---
title: "Install ngircd on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Lightweight Internet Relay Chat server"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ngircd on MacOS using homebrew

- App Name: ngircd
- App description: Lightweight Internet Relay Chat server
- App Version: 26.1
- App Website: https://ngircd.barton.de/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ngircd with the following command
   ```
   brew install ngircd
   ```
4. ngircd is ready to use now!