---
title: "Install Soduto on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Communicate and share information between devices"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Soduto on MacOS using homebrew

- App Name: Soduto
- App description: Communicate and share information between devices
- App Version: 1.0.1
- App Website: https://soduto.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Soduto with the following command
   ```
   brew install --cask soduto
   ```
4. Soduto is ready to use now!