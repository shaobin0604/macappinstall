---
title: "Install bastet on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Bastard Tetris"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bastet on MacOS using homebrew

- App Name: bastet
- App description: Bastard Tetris
- App Version: 0.43.2
- App Website: https://fph.altervista.org/prog/bastet.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bastet with the following command
   ```
   brew install bastet
   ```
4. bastet is ready to use now!