---
title: "Install IBM Aspera Connect on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Facilitate uploads and downloads with an Aspera transfer server"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install IBM Aspera Connect on MacOS using homebrew

- App Name: IBM Aspera Connect
- App description: Facilitate uploads and downloads with an Aspera transfer server
- App Version: 4.1.1.73
- App Website: https://www.ibm.com/aspera/connect/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install IBM Aspera Connect with the following command
   ```
   brew install --cask ibm-aspera-connect
   ```
4. IBM Aspera Connect is ready to use now!