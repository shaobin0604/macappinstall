---
title: "Install Wonder Unit Storyboarder on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Visualize a story as fast you can draw stick figures"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Wonder Unit Storyboarder on MacOS using homebrew

- App Name: Wonder Unit Storyboarder
- App description: Visualize a story as fast you can draw stick figures
- App Version: 3.0.0
- App Website: https://wonderunit.com/storyboarder/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Wonder Unit Storyboarder with the following command
   ```
   brew install --cask storyboarder
   ```
4. Wonder Unit Storyboarder is ready to use now!