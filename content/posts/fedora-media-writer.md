---
title: "Install Fedora Media Writer on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to write Fedora images to portable media files"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Fedora Media Writer on MacOS using homebrew

- App Name: Fedora Media Writer
- App description: Tool to write Fedora images to portable media files
- App Version: 4.2.2
- App Website: https://docs.fedoraproject.org/en-US/quick-docs/creating-and-using-a-live-installation-image/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Fedora Media Writer with the following command
   ```
   brew install --cask fedora-media-writer
   ```
4. Fedora Media Writer is ready to use now!