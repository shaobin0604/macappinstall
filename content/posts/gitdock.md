---
title: "Install GitDock on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Displays all your GitLab activities in one place"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install GitDock on MacOS using homebrew

- App Name: GitDock
- App description: Displays all your GitLab activities in one place
- App Version: 0.1.21
- App Website: https://gitlab.com/mvanremmerden/gitdock

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install GitDock with the following command
   ```
   brew install --cask gitdock
   ```
4. GitDock is ready to use now!