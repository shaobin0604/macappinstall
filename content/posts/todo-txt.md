---
title: "Install todo-txt on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Minimal, todo.txt-focused editor"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install todo-txt on MacOS using homebrew

- App Name: todo-txt
- App description: Minimal, todo.txt-focused editor
- App Version: 2.12.0
- App Website: http://todotxt.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install todo-txt with the following command
   ```
   brew install todo-txt
   ```
4. todo-txt is ready to use now!