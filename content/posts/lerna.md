---
title: "Install lerna on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool for managing JavaScript projects with multiple packages"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install lerna on MacOS using homebrew

- App Name: lerna
- App description: Tool for managing JavaScript projects with multiple packages
- App Version: 4.0.0
- App Website: https://lerna.js.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install lerna with the following command
   ```
   brew install lerna
   ```
4. lerna is ready to use now!