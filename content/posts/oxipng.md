---
title: "Install oxipng on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Multithreaded PNG optimizer written in Rust"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install oxipng on MacOS using homebrew

- App Name: oxipng
- App description: Multithreaded PNG optimizer written in Rust
- App Version: 5.0.1
- App Website: https://github.com/shssoichiro/oxipng

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install oxipng with the following command
   ```
   brew install oxipng
   ```
4. oxipng is ready to use now!