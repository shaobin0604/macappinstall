---
title: "Install WJoy on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Nintendo wiimote driver"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install WJoy on MacOS using homebrew

- App Name: WJoy
- App description: Nintendo wiimote driver
- App Version: 0.7.1
- App Website: https://github.com/alxn1/wjoy

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install WJoy with the following command
   ```
   brew install --cask wjoy
   ```
4. WJoy is ready to use now!