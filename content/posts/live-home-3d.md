---
title: "Install Live Home 3D on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Home & floorplan designer & renderer"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Live Home 3D on MacOS using homebrew

- App Name: Live Home 3D
- App description: Home & floorplan designer & renderer
- App Version: 4.3
- App Website: https://www.livehome3d.com/mac/live-home-3d

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Live Home 3D with the following command
   ```
   brew install --cask live-home-3d
   ```
4. Live Home 3D is ready to use now!