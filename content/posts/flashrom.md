---
title: "Install flashrom on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Identify, read, write, verify, and erase flash chips"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install flashrom on MacOS using homebrew

- App Name: flashrom
- App description: Identify, read, write, verify, and erase flash chips
- App Version: 1.2
- App Website: https://flashrom.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install flashrom with the following command
   ```
   brew install flashrom
   ```
4. flashrom is ready to use now!