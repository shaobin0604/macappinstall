---
title: "Install cloog on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Generate code for scanning Z-polyhedra"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cloog on MacOS using homebrew

- App Name: cloog
- App description: Generate code for scanning Z-polyhedra
- App Version: 0.18.4
- App Website: http://www.bastoul.net/cloog/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cloog with the following command
   ```
   brew install cloog
   ```
4. cloog is ready to use now!