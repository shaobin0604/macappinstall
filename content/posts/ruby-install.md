---
title: "Install ruby-install on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Install Ruby, JRuby, Rubinius, TruffleRuby, or mruby"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ruby-install on MacOS using homebrew

- App Name: ruby-install
- App description: Install Ruby, JRuby, Rubinius, TruffleRuby, or mruby
- App Version: 0.8.3
- App Website: https://github.com/postmodern/ruby-install#readme

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ruby-install with the following command
   ```
   brew install ruby-install
   ```
4. ruby-install is ready to use now!