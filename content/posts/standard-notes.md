---
title: "Install Standard Notes on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Free, open-source, and completely encrypted notes app"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Standard Notes on MacOS using homebrew

- App Name: Standard Notes
- App description: Free, open-source, and completely encrypted notes app
- App Version: 3.10.0
- App Website: https://standardnotes.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Standard Notes with the following command
   ```
   brew install --cask standard-notes
   ```
4. Standard Notes is ready to use now!