---
title: "Install Android File Transfer on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Android File Transfer for Linux"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Android File Transfer on MacOS using homebrew

- App Name: Android File Transfer
- App description: Android File Transfer for Linux
- App Version: 4.2
- App Website: https://whoozle.github.io/android-file-transfer-linux/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Android File Transfer with the following command
   ```
   brew install --cask whoozle-android-file-transfer
   ```
4. Android File Transfer is ready to use now!