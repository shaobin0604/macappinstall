---
title: "Install Kernelpanic GhostTile on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Hide your running applications from Dock"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Kernelpanic GhostTile on MacOS using homebrew

- App Name: Kernelpanic GhostTile
- App description: Hide your running applications from Dock
- App Version: 15,15,1510040474
- App Website: https://ghosttile.kernelpanic.im/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Kernelpanic GhostTile with the following command
   ```
   brew install --cask ghosttile
   ```
4. Kernelpanic GhostTile is ready to use now!