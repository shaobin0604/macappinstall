---
title: "Install fx on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line JSON processing tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fx on MacOS using homebrew

- App Name: fx
- App description: Command-line JSON processing tool
- App Version: 20.0.2
- App Website: https://github.com/antonmedv/fx

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fx with the following command
   ```
   brew install fx
   ```
4. fx is ready to use now!