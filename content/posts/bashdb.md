---
title: "Install bashdb on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Bash shell debugger"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bashdb on MacOS using homebrew

- App Name: bashdb
- App description: Bash shell debugger
- App Version: 5.0-1.1.2
- App Website: https://bashdb.sourceforge.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bashdb with the following command
   ```
   brew install bashdb
   ```
4. bashdb is ready to use now!