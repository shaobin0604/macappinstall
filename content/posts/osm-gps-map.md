---
title: "Install osm-gps-map on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GTK+ library to embed OpenStreetMap maps"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install osm-gps-map on MacOS using homebrew

- App Name: osm-gps-map
- App description: GTK+ library to embed OpenStreetMap maps
- App Version: 1.2.0
- App Website: https://github.com/nzjrs/osm-gps-map

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install osm-gps-map with the following command
   ```
   brew install osm-gps-map
   ```
4. osm-gps-map is ready to use now!