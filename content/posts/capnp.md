---
title: "Install capnp on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Data interchange format and capability-based RPC system"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install capnp on MacOS using homebrew

- App Name: capnp
- App description: Data interchange format and capability-based RPC system
- App Version: 0.9.1
- App Website: https://capnproto.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install capnp with the following command
   ```
   brew install capnp
   ```
4. capnp is ready to use now!