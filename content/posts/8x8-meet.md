---
title: "Install 8x8 Meet on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Video conferencing platform"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install 8x8 Meet on MacOS using homebrew

- App Name: 8x8 Meet
- App description: Video conferencing platform
- App Version: 0.3.8.1
- App Website: https://8x8.vc/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install 8x8 Meet with the following command
   ```
   brew install --cask 8x8-meet
   ```
4. 8x8 Meet is ready to use now!