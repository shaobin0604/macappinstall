---
title: "Install Valentina Studio on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Visual editors for data"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Valentina Studio on MacOS using homebrew

- App Name: Valentina Studio
- App description: Visual editors for data
- App Version: 11.5.2
- App Website: https://valentina-db.com/en/valentina-studio-overview

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Valentina Studio with the following command
   ```
   brew install --cask valentina-studio
   ```
4. Valentina Studio is ready to use now!