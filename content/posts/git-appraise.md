---
title: "Install git-appraise on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Distributed code review system for Git repos"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install git-appraise on MacOS using homebrew

- App Name: git-appraise
- App description: Distributed code review system for Git repos
- App Version: 0.6
- App Website: https://github.com/google/git-appraise

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install git-appraise with the following command
   ```
   brew install git-appraise
   ```
4. git-appraise is ready to use now!