---
title: "Install rdesktop on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "UNIX client for connecting to Windows Remote Desktop Services"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install rdesktop on MacOS using homebrew

- App Name: rdesktop
- App description: UNIX client for connecting to Windows Remote Desktop Services
- App Version: 1.9.0
- App Website: https://github.com/rdesktop/rdesktop

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install rdesktop with the following command
   ```
   brew install rdesktop
   ```
4. rdesktop is ready to use now!