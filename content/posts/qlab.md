---
title: "Install QLab on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Sound, video and lighting control"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install QLab on MacOS using homebrew

- App Name: QLab
- App description: Sound, video and lighting control
- App Version: 4.6.11,4611
- App Website: https://qlab.app/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install QLab with the following command
   ```
   brew install --cask qlab
   ```
4. QLab is ready to use now!