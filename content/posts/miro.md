---
title: "Install Miro on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Online collaborative whiteboard platform"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Miro on MacOS using homebrew

- App Name: Miro
- App description: Online collaborative whiteboard platform
- App Version: 0.7.2
- App Website: https://miro.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Miro with the following command
   ```
   brew install --cask miro
   ```
4. Miro is ready to use now!