---
title: "Install Bob on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Translation application for text, pictures, and manual input"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Bob on MacOS using homebrew

- App Name: Bob
- App description: Translation application for text, pictures, and manual input
- App Version: 0.7.0
- App Website: https://github.com/ripperhe/Bob

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Bob with the following command
   ```
   brew install --cask bob
   ```
4. Bob is ready to use now!