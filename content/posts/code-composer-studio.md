---
title: "Install Code Composer Studio (CCS) on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Integrated development environment"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Code Composer Studio (CCS) on MacOS using homebrew

- App Name: Code Composer Studio (CCS)
- App description: Integrated development environment
- App Version: 11.1.0.00011
- App Website: https://www.ti.com/tool/CCSTUDIO

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Code Composer Studio (CCS) with the following command
   ```
   brew install --cask code-composer-studio
   ```
4. Code Composer Studio (CCS) is ready to use now!