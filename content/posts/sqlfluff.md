---
title: "Install sqlfluff on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "SQL linter and auto-formatter for Humans"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sqlfluff on MacOS using homebrew

- App Name: sqlfluff
- App description: SQL linter and auto-formatter for Humans
- App Version: 0.10.1
- App Website: https://docs.sqlfluff.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sqlfluff with the following command
   ```
   brew install sqlfluff
   ```
4. sqlfluff is ready to use now!