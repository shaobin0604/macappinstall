---
title: "Install coArchi plugin for Archi on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Model Collaboration plugin for Archi"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install coArchi plugin for Archi on MacOS using homebrew

- App Name: coArchi plugin for Archi
- App description: Model Collaboration plugin for Archi
- App Version: 0.8.1.202112061132
- App Website: https://www.archimatetool.com/plugins/#coArchi

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install coArchi plugin for Archi with the following command
   ```
   brew install --cask coarchi
   ```
4. coArchi plugin for Archi is ready to use now!