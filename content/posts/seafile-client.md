---
title: "Install Seafile Client on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "File syncing client"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Seafile Client on MacOS using homebrew

- App Name: Seafile Client
- App description: File syncing client
- App Version: 8.0.4
- App Website: https://www.seafile.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Seafile Client with the following command
   ```
   brew install --cask seafile-client
   ```
4. Seafile Client is ready to use now!