---
title: "Install ettercap on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Multipurpose sniffer/interceptor/logger for switched LAN"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ettercap on MacOS using homebrew

- App Name: ettercap
- App description: Multipurpose sniffer/interceptor/logger for switched LAN
- App Version: 0.8.3.1
- App Website: https://ettercap.github.io/ettercap/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ettercap with the following command
   ```
   brew install ettercap
   ```
4. ettercap is ready to use now!