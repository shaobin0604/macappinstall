---
title: "Install tracker on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library and daemon that is an efficient search engine and triplestore"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tracker on MacOS using homebrew

- App Name: tracker
- App description: Library and daemon that is an efficient search engine and triplestore
- App Version: 3.1.2
- App Website: https://gnome.pages.gitlab.gnome.org/tracker/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tracker with the following command
   ```
   brew install tracker
   ```
4. tracker is ready to use now!