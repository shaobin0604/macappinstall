---
title: "Install Quip on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool for teams to create living documents"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Quip on MacOS using homebrew

- App Name: Quip
- App description: Tool for teams to create living documents
- App Version: 7.50.0
- App Website: https://quip.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Quip with the following command
   ```
   brew install --cask quip
   ```
4. Quip is ready to use now!