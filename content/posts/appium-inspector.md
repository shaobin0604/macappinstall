---
title: "Install Appium Inspector GUI on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GUI inspector for mobile apps"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Appium Inspector GUI on MacOS using homebrew

- App Name: Appium Inspector GUI
- App description: GUI inspector for mobile apps
- App Version: 2021.12.2
- App Website: https://github.com/appium/appium-inspector/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Appium Inspector GUI with the following command
   ```
   brew install --cask appium-inspector
   ```
4. Appium Inspector GUI is ready to use now!