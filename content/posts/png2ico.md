---
title: "Install png2ico on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "PNG to icon converter"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install png2ico on MacOS using homebrew

- App Name: png2ico
- App description: PNG to icon converter
- App Version: 2002-12-08
- App Website: https://www.winterdrache.de/freeware/png2ico/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install png2ico with the following command
   ```
   brew install png2ico
   ```
4. png2ico is ready to use now!