---
title: "Install Gingko on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Word processor that shows structure and content"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Gingko on MacOS using homebrew

- App Name: Gingko
- App description: Word processor that shows structure and content
- App Version: 2.4.15
- App Website: https://gingko.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Gingko with the following command
   ```
   brew install --cask gingko
   ```
4. Gingko is ready to use now!