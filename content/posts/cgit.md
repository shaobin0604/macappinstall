---
title: "Install cgit on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Hyperfast web frontend for Git repositories written in C"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cgit on MacOS using homebrew

- App Name: cgit
- App description: Hyperfast web frontend for Git repositories written in C
- App Version: 1.2.3
- App Website: https://git.zx2c4.com/cgit/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cgit with the following command
   ```
   brew install cgit
   ```
4. cgit is ready to use now!