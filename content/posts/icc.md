---
title: "Install International Chess Club on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Chess club client"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install International Chess Club on MacOS using homebrew

- App Name: International Chess Club
- App description: Chess club client
- App Version: 1.0,7648
- App Website: https://www.chessclub.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install International Chess Club with the following command
   ```
   brew install --cask icc
   ```
4. International Chess Club is ready to use now!