---
title: "Install atkmm on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Official C++ interface for the ATK accessibility toolkit library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install atkmm on MacOS using homebrew

- App Name: atkmm
- App description: Official C++ interface for the ATK accessibility toolkit library
- App Version: 2.36.1
- App Website: https://www.gtkmm.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install atkmm with the following command
   ```
   brew install atkmm
   ```
4. atkmm is ready to use now!