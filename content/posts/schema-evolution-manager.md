---
title: "Install schema-evolution-manager on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Manage postgresql database schema migrations"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install schema-evolution-manager on MacOS using homebrew

- App Name: schema-evolution-manager
- App description: Manage postgresql database schema migrations
- App Version: 0.9.45
- App Website: https://github.com/mbryzek/schema-evolution-manager

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install schema-evolution-manager with the following command
   ```
   brew install schema-evolution-manager
   ```
4. schema-evolution-manager is ready to use now!