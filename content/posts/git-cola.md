---
title: "Install git-cola on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Highly caffeinated git GUI"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install git-cola on MacOS using homebrew

- App Name: git-cola
- App description: Highly caffeinated git GUI
- App Version: 3.12.0
- App Website: https://git-cola.github.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install git-cola with the following command
   ```
   brew install git-cola
   ```
4. git-cola is ready to use now!