---
title: "Install texlive on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Free software distribution for the TeX typesetting system"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install texlive on MacOS using homebrew

- App Name: texlive
- App description: Free software distribution for the TeX typesetting system
- App Version: 58837
- App Website: https://www.tug.org/texlive/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install texlive with the following command
   ```
   brew install texlive
   ```
4. texlive is ready to use now!