---
title: "Install coreutils on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GNU File, Shell, and Text utilities"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install coreutils on MacOS using homebrew

- App Name: coreutils
- App description: GNU File, Shell, and Text utilities
- App Version: 9.0
- App Website: https://www.gnu.org/software/coreutils

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install coreutils with the following command
   ```
   brew install coreutils
   ```
4. coreutils is ready to use now!