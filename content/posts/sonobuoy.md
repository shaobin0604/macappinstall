---
title: "Install sonobuoy on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Kubernetes component that generates reports on cluster conformance"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sonobuoy on MacOS using homebrew

- App Name: sonobuoy
- App description: Kubernetes component that generates reports on cluster conformance
- App Version: 0.56.1
- App Website: https://github.com/vmware-tanzu/sonobuoy

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sonobuoy with the following command
   ```
   brew install sonobuoy
   ```
4. sonobuoy is ready to use now!