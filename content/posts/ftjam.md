---
title: "Install ftjam on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Build tool that can be used as a replacement for Make"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ftjam on MacOS using homebrew

- App Name: ftjam
- App description: Build tool that can be used as a replacement for Make
- App Version: 2.5.2
- App Website: https://www.freetype.org/jam/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ftjam with the following command
   ```
   brew install ftjam
   ```
4. ftjam is ready to use now!