---
title: "Install Accordance Bible Software on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Bible study software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Accordance Bible Software on MacOS using homebrew

- App Name: Accordance Bible Software
- App description: Bible study software
- App Version: 13.3.2
- App Website: https://www.accordancebible.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Accordance Bible Software with the following command
   ```
   brew install --cask accordance
   ```
4. Accordance Bible Software is ready to use now!