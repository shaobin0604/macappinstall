---
title: "Install chibi-scheme on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Small footprint Scheme for use as a C Extension Language"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install chibi-scheme on MacOS using homebrew

- App Name: chibi-scheme
- App description: Small footprint Scheme for use as a C Extension Language
- App Version: 0.10
- App Website: https://github.com/ashinn/chibi-scheme

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install chibi-scheme with the following command
   ```
   brew install chibi-scheme
   ```
4. chibi-scheme is ready to use now!