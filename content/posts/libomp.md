---
title: "Install libomp on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "LLVM's OpenMP runtime library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libomp on MacOS using homebrew

- App Name: libomp
- App description: LLVM's OpenMP runtime library
- App Version: 13.0.1
- App Website: https://openmp.llvm.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libomp with the following command
   ```
   brew install libomp
   ```
4. libomp is ready to use now!