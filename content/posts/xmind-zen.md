---
title: "Install XMind on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Mindmap and brainstorming app"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install XMind on MacOS using homebrew

- App Name: XMind
- App description: Mindmap and brainstorming app
- App Version: 11.1.2,202111071943
- App Website: https://www.xmind.net/desktop/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install XMind with the following command
   ```
   brew install --cask xmind-zen
   ```
4. XMind is ready to use now!