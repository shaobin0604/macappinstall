---
title: "Install simple-tiles on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Image generation library for spatial data"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install simple-tiles on MacOS using homebrew

- App Name: simple-tiles
- App description: Image generation library for spatial data
- App Version: 0.6.1
- App Website: https://github.com/propublica/simple-tiles

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install simple-tiles with the following command
   ```
   brew install simple-tiles
   ```
4. simple-tiles is ready to use now!