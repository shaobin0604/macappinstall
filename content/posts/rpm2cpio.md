---
title: "Install rpm2cpio on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to convert RPM package to CPIO archive"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install rpm2cpio on MacOS using homebrew

- App Name: rpm2cpio
- App description: Tool to convert RPM package to CPIO archive
- App Version: 1.4
- App Website: https://svnweb.freebsd.org/ports/head/archivers/rpm2cpio/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install rpm2cpio with the following command
   ```
   brew install rpm2cpio
   ```
4. rpm2cpio is ready to use now!