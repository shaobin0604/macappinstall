---
title: "Install JGR's OpenTTD Patchpack on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Collection of patches applied to OpenTTD"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install JGR's OpenTTD Patchpack on MacOS using homebrew

- App Name: JGR's OpenTTD Patchpack
- App description: Collection of patches applied to OpenTTD
- App Version: 0.46.1
- App Website: https://github.com/JGRennison/OpenTTD-patches/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install JGR's OpenTTD Patchpack with the following command
   ```
   brew install --cask jgrennison-openttd
   ```
4. JGR's OpenTTD Patchpack is ready to use now!