---
title: "Install launch_socket_server on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Bind to privileged ports without running a server as root"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install launch_socket_server on MacOS using homebrew

- App Name: launch_socket_server
- App description: Bind to privileged ports without running a server as root
- App Version: 2.0.0
- App Website: https://github.com/mistydemeo/launch_socket_server

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install launch_socket_server with the following command
   ```
   brew install launch_socket_server
   ```
4. launch_socket_server is ready to use now!