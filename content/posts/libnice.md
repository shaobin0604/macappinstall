---
title: "Install libnice on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GLib ICE implementation"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libnice on MacOS using homebrew

- App Name: libnice
- App description: GLib ICE implementation
- App Version: 0.1.18
- App Website: https://wiki.freedesktop.org/nice/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libnice with the following command
   ```
   brew install libnice
   ```
4. libnice is ready to use now!