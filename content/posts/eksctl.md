---
title: "Install eksctl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Simple command-line tool for creating clusters on Amazon EKS"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install eksctl on MacOS using homebrew

- App Name: eksctl
- App description: Simple command-line tool for creating clusters on Amazon EKS
- App Version: 0.83.0
- App Website: https://eksctl.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install eksctl with the following command
   ```
   brew install eksctl
   ```
4. eksctl is ready to use now!