---
title: "Install inotify-tools on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C library and command-line programs providing a simple interface to inotify"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install inotify-tools on MacOS using homebrew

- App Name: inotify-tools
- App description: C library and command-line programs providing a simple interface to inotify
- App Version: 3.22.1.0
- App Website: https://github.com/inotify-tools/inotify-tools

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install inotify-tools with the following command
   ```
   brew install inotify-tools
   ```
4. inotify-tools is ready to use now!