---
title: "Install libebml on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Sort of a sbinary version of XML"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libebml on MacOS using homebrew

- App Name: libebml
- App description: Sort of a sbinary version of XML
- App Version: 1.4.2
- App Website: https://www.matroska.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libebml with the following command
   ```
   brew install libebml
   ```
4. libebml is ready to use now!