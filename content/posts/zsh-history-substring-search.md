---
title: "Install zsh-history-substring-search on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Zsh port of Fish shell's history search"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install zsh-history-substring-search on MacOS using homebrew

- App Name: zsh-history-substring-search
- App description: Zsh port of Fish shell's history search
- App Version: 1.0.2
- App Website: https://github.com/zsh-users/zsh-history-substring-search

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install zsh-history-substring-search with the following command
   ```
   brew install zsh-history-substring-search
   ```
4. zsh-history-substring-search is ready to use now!