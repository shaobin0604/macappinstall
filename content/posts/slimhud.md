---
title: "Install SlimHUD on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Replacement for the volume, brightness and keyboard backlight HUDs"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install SlimHUD on MacOS using homebrew

- App Name: SlimHUD
- App description: Replacement for the volume, brightness and keyboard backlight HUDs
- App Version: 1.3.7
- App Website: https://github.com/AlexPerathoner/SlimHUD/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install SlimHUD with the following command
   ```
   brew install --cask slimhud
   ```
4. SlimHUD is ready to use now!