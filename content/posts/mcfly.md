---
title: "Install mcfly on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Fly through your shell history"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mcfly on MacOS using homebrew

- App Name: mcfly
- App description: Fly through your shell history
- App Version: 0.5.13
- App Website: https://github.com/cantino/mcfly

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mcfly with the following command
   ```
   brew install mcfly
   ```
4. mcfly is ready to use now!