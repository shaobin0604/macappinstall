---
title: "Install cflow on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Generate call graphs from C code"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cflow on MacOS using homebrew

- App Name: cflow
- App description: Generate call graphs from C code
- App Version: 1.7
- App Website: https://www.gnu.org/software/cflow/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cflow with the following command
   ```
   brew install cflow
   ```
4. cflow is ready to use now!