---
title: "Install multimarkdown on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Turn marked-up plain text into well-formatted documents"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install multimarkdown on MacOS using homebrew

- App Name: multimarkdown
- App description: Turn marked-up plain text into well-formatted documents
- App Version: 6.6.0
- App Website: https://fletcher.github.io/MultiMarkdown-6/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install multimarkdown with the following command
   ```
   brew install multimarkdown
   ```
4. multimarkdown is ready to use now!