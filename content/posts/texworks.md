---
title: "Install TeXworks on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Main codebase"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install TeXworks on MacOS using homebrew

- App Name: TeXworks
- App description: Main codebase
- App Version: 0.6.6,202103111124,24442ac
- App Website: https://www.tug.org/texworks/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install TeXworks with the following command
   ```
   brew install --cask texworks
   ```
4. TeXworks is ready to use now!