---
title: "Install Glyphs on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Font editor"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Glyphs on MacOS using homebrew

- App Name: Glyphs
- App description: Font editor
- App Version: 3.0.5,3112
- App Website: https://glyphsapp.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Glyphs with the following command
   ```
   brew install --cask glyphs
   ```
4. Glyphs is ready to use now!