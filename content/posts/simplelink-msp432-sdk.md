---
title: "Install SimpleLink MSP432P4 High-precision ADC MCU Software Development Kit on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Software development kit for MSP432P4 micro-controllers"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install SimpleLink MSP432P4 High-precision ADC MCU Software Development Kit on MacOS using homebrew

- App Name: SimpleLink MSP432P4 High-precision ADC MCU Software Development Kit
- App description: Software development kit for MSP432P4 micro-controllers
- App Version: 3.40.01.02
- App Website: https://www.ti.com/tool/SIMPLELINK-MSP432-SDK

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install SimpleLink MSP432P4 High-precision ADC MCU Software Development Kit with the following command
   ```
   brew install --cask simplelink-msp432-sdk
   ```
4. SimpleLink MSP432P4 High-precision ADC MCU Software Development Kit is ready to use now!