---
title: "Install orientdb on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Graph database"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install orientdb on MacOS using homebrew

- App Name: orientdb
- App description: Graph database
- App Version: 3.2.4
- App Website: https://orientdb.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install orientdb with the following command
   ```
   brew install orientdb
   ```
4. orientdb is ready to use now!