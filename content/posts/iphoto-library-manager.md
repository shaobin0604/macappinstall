---
title: "Install iPhoto Library Manager on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "App for organizing photos among multiple iPhoto libraries"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install iPhoto Library Manager on MacOS using homebrew

- App Name: iPhoto Library Manager
- App description: App for organizing photos among multiple iPhoto libraries
- App Version: 4.2.7,954
- App Website: https://www.fatcatsoftware.com/iplm/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install iPhoto Library Manager with the following command
   ```
   brew install --cask iphoto-library-manager
   ```
4. iPhoto Library Manager is ready to use now!