---
title: "Install gps4cam on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Geotag your photos"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gps4cam on MacOS using homebrew

- App Name: gps4cam
- App description: Geotag your photos
- App Version: 7.2
- App Website: http://gps4cam.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gps4cam with the following command
   ```
   brew install --cask gps4cam
   ```
4. gps4cam is ready to use now!