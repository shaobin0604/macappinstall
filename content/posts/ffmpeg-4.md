---
title: "Install ffmpeg@4 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Play, record, convert, and stream audio and video"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ffmpeg@4 on MacOS using homebrew

- App Name: ffmpeg@4
- App description: Play, record, convert, and stream audio and video
- App Version: 4.4.1
- App Website: https://ffmpeg.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ffmpeg@4 with the following command
   ```
   brew install ffmpeg@4
   ```
4. ffmpeg@4 is ready to use now!