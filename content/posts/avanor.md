---
title: "Install avanor on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Quick-growing roguelike game with easy ADOM-like UI"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install avanor on MacOS using homebrew

- App Name: avanor
- App description: Quick-growing roguelike game with easy ADOM-like UI
- App Version: 0.5.8
- App Website: https://avanor.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install avanor with the following command
   ```
   brew install avanor
   ```
4. avanor is ready to use now!