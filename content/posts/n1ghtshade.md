---
title: "Install n1ghtshade on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Permits the downgrade/jailbreak of 32-bit iOS devices"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install n1ghtshade on MacOS using homebrew

- App Name: n1ghtshade
- App description: Permits the downgrade/jailbreak of 32-bit iOS devices
- App Version: RC3
- App Website: https://github.com/synackuk/n1ghtshade

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install n1ghtshade with the following command
   ```
   brew install --cask n1ghtshade
   ```
4. n1ghtshade is ready to use now!