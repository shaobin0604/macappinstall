---
title: "Install Twine on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool for telling interactive, nonlinear stories"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Twine on MacOS using homebrew

- App Name: Twine
- App description: Tool for telling interactive, nonlinear stories
- App Version: 2.3.16
- App Website: https://twinery.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Twine with the following command
   ```
   brew install --cask twine
   ```
4. Twine is ready to use now!