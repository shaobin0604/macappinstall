---
title: "Install Doxie on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Companion app for scanner hardware"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Doxie on MacOS using homebrew

- App Name: Doxie
- App description: Companion app for scanner hardware
- App Version: 2.14
- App Website: https://www.getdoxie.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Doxie with the following command
   ```
   brew install --cask doxie
   ```
4. Doxie is ready to use now!