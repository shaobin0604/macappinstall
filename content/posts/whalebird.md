---
title: "Install Whalebird on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Mastodon, Pleroma and Misskey client"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Whalebird on MacOS using homebrew

- App Name: Whalebird
- App description: Mastodon, Pleroma and Misskey client
- App Version: 4.5.1
- App Website: https://whalebird.social/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Whalebird with the following command
   ```
   brew install --cask whalebird
   ```
4. Whalebird is ready to use now!