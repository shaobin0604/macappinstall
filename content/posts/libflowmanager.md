---
title: "Install libflowmanager on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Flow-based measurement tasks with packet-based inputs"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libflowmanager on MacOS using homebrew

- App Name: libflowmanager
- App description: Flow-based measurement tasks with packet-based inputs
- App Version: 3.0.0
- App Website: https://research.wand.net.nz/software/libflowmanager.php

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libflowmanager with the following command
   ```
   brew install libflowmanager
   ```
4. libflowmanager is ready to use now!