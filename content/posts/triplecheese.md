---
title: "Install Triple Cheese on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Luscious and cheesy synthesizer"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Triple Cheese on MacOS using homebrew

- App Name: Triple Cheese
- App description: Luscious and cheesy synthesizer
- App Version: 1.3,12092
- App Website: https://u-he.com/products/triplecheese/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Triple Cheese with the following command
   ```
   brew install --cask triplecheese
   ```
4. Triple Cheese is ready to use now!