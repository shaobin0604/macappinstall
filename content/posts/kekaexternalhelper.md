---
title: "Install Keka External Helper on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Helper application for the Keka file archiver"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Keka External Helper on MacOS using homebrew

- App Name: Keka External Helper
- App description: Helper application for the Keka file archiver
- App Version: 1.1.2,1.2.50
- App Website: https://github.com/aonez/Keka/wiki/Default-application

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Keka External Helper with the following command
   ```
   brew install --cask kekaexternalhelper
   ```
4. Keka External Helper is ready to use now!