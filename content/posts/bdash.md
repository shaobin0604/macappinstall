---
title: "Install Bdash on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Simple SQL Client for lightweight data analysis"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Bdash on MacOS using homebrew

- App Name: Bdash
- App description: Simple SQL Client for lightweight data analysis
- App Version: 1.14.1
- App Website: https://github.com/bdash-app/bdash

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Bdash with the following command
   ```
   brew install --cask bdash
   ```
4. Bdash is ready to use now!