---
title: "Install libpq on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Postgres C API library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libpq on MacOS using homebrew

- App Name: libpq
- App description: Postgres C API library
- App Version: 14.2
- App Website: https://www.postgresql.org/docs/14/libpq.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libpq with the following command
   ```
   brew install libpq
   ```
4. libpq is ready to use now!