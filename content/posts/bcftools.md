---
title: "Install bcftools on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tools for BCF/VCF files and variant calling from samtools"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bcftools on MacOS using homebrew

- App Name: bcftools
- App description: Tools for BCF/VCF files and variant calling from samtools
- App Version: 1.14
- App Website: https://www.htslib.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bcftools with the following command
   ```
   brew install bcftools
   ```
4. bcftools is ready to use now!