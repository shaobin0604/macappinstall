---
title: "Install gpsbabel on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Converts/uploads GPS waypoints, tracks, and routes"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gpsbabel on MacOS using homebrew

- App Name: gpsbabel
- App description: Converts/uploads GPS waypoints, tracks, and routes
- App Version: 1.8.0
- App Website: https://www.gpsbabel.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gpsbabel with the following command
   ```
   brew install gpsbabel
   ```
4. gpsbabel is ready to use now!