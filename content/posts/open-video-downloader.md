---
title: "Install Open Video Downloader on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cross-platform GUI for youtube-dl made in Electron and node.js"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Open Video Downloader on MacOS using homebrew

- App Name: Open Video Downloader
- App description: Cross-platform GUI for youtube-dl made in Electron and node.js
- App Version: 2.4.0
- App Website: https://github.com/jely2002/youtube-dl-gui

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Open Video Downloader with the following command
   ```
   brew install --cask open-video-downloader
   ```
4. Open Video Downloader is ready to use now!