---
title: "Install Tumult Hype on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "App to create animated and interactive web content"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Tumult Hype on MacOS using homebrew

- App Name: Tumult Hype
- App description: App to create animated and interactive web content
- App Version: 4.1.7,736
- App Website: https://tumult.com/hype/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Tumult Hype with the following command
   ```
   brew install --cask hype
   ```
4. Tumult Hype is ready to use now!