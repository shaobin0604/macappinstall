---
title: "Install Linear on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "App to manage software development and track bugs"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Linear on MacOS using homebrew

- App Name: Linear
- App description: App to manage software development and track bugs
- App Version: 1.5.3
- App Website: https://linear.app/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Linear with the following command
   ```
   brew install --cask linear-linear
   ```
4. Linear is ready to use now!