---
title: "Install EVKey on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Vietnamese keyboard"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install EVKey on MacOS using homebrew

- App Name: EVKey
- App description: Vietnamese keyboard
- App Version: 3.3.6,1
- App Website: https://evkeyvn.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install EVKey with the following command
   ```
   brew install --cask evkey
   ```
4. EVKey is ready to use now!