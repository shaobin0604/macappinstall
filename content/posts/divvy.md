---
title: "Install Divvy on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Application window manager focusing on simplicity"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Divvy on MacOS using homebrew

- App Name: Divvy
- App description: Application window manager focusing on simplicity
- App Version: 1.5.1,581
- App Website: https://mizage.com/divvy/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Divvy with the following command
   ```
   brew install --cask divvy
   ```
4. Divvy is ready to use now!