---
title: "Install Datovka on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Access and store data messages in a local database"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Datovka on MacOS using homebrew

- App Name: Datovka
- App description: Access and store data messages in a local database
- App Version: 4.19.0
- App Website: https://www.datovka.cz/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Datovka with the following command
   ```
   brew install --cask datovka
   ```
4. Datovka is ready to use now!