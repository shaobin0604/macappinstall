---
title: "Install gleam on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "✨ A statically typed language for the Erlang VM"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gleam on MacOS using homebrew

- App Name: gleam
- App description: ✨ A statically typed language for the Erlang VM
- App Version: 0.19.0
- App Website: https://gleam.run

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gleam with the following command
   ```
   brew install gleam
   ```
4. gleam is ready to use now!