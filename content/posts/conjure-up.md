---
title: "Install conjure-up on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Big software deployments so easy it's almost magical"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install conjure-up on MacOS using homebrew

- App Name: conjure-up
- App description: Big software deployments so easy it's almost magical
- App Version: 2.6.14
- App Website: https://conjure-up.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install conjure-up with the following command
   ```
   brew install conjure-up
   ```
4. conjure-up is ready to use now!