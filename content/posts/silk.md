---
title: "Install silk on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Collection of traffic analysis tools"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install silk on MacOS using homebrew

- App Name: silk
- App description: Collection of traffic analysis tools
- App Version: 3.19.1
- App Website: https://tools.netsa.cert.org/silk/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install silk with the following command
   ```
   brew install silk
   ```
4. silk is ready to use now!