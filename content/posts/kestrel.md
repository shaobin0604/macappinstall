---
title: "Install kestrel on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Distributed message queue"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install kestrel on MacOS using homebrew

- App Name: kestrel
- App description: Distributed message queue
- App Version: 2.4.1
- App Website: https://twitter-archive.github.io/kestrel/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install kestrel with the following command
   ```
   brew install kestrel
   ```
4. kestrel is ready to use now!