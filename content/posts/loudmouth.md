---
title: "Install loudmouth on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Lightweight C library for the Jabber protocol"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install loudmouth on MacOS using homebrew

- App Name: loudmouth
- App description: Lightweight C library for the Jabber protocol
- App Version: 1.5.4
- App Website: https://mcabber.com

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install loudmouth with the following command
   ```
   brew install loudmouth
   ```
4. loudmouth is ready to use now!