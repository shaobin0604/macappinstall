---
title: "Install binkd on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "TCP/IP FTN Mailer"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install binkd on MacOS using homebrew

- App Name: binkd
- App description: TCP/IP FTN Mailer
- App Version: 1.0.4
- App Website: https://2f.ru/binkd/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install binkd with the following command
   ```
   brew install binkd
   ```
4. binkd is ready to use now!