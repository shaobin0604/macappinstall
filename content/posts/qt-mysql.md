---
title: "Install qt-mysql on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Qt SQL Database Driver"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install qt-mysql on MacOS using homebrew

- App Name: qt-mysql
- App description: Qt SQL Database Driver
- App Version: 6.2.2
- App Website: https://www.qt.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install qt-mysql with the following command
   ```
   brew install qt-mysql
   ```
4. qt-mysql is ready to use now!