---
title: "Install zookeeper on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Centralized server for distributed coordination of services"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install zookeeper on MacOS using homebrew

- App Name: zookeeper
- App description: Centralized server for distributed coordination of services
- App Version: 3.7.0
- App Website: https://zookeeper.apache.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install zookeeper with the following command
   ```
   brew install zookeeper
   ```
4. zookeeper is ready to use now!