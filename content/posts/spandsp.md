---
title: "Install spandsp on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "DSP functions library for telephony"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install spandsp on MacOS using homebrew

- App Name: spandsp
- App description: DSP functions library for telephony
- App Version: 0.0.6
- App Website: https://www.soft-switch.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install spandsp with the following command
   ```
   brew install spandsp
   ```
4. spandsp is ready to use now!