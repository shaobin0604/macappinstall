---
title: "Install grace on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "WYSIWYG 2D plotting tool for X11"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install grace on MacOS using homebrew

- App Name: grace
- App description: WYSIWYG 2D plotting tool for X11
- App Version: 5.1.25
- App Website: https://plasma-gate.weizmann.ac.il/Grace/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install grace with the following command
   ```
   brew install grace
   ```
4. grace is ready to use now!