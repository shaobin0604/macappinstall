---
title: "Install Microsoft Auto Update on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Provides updates to various Microsoft products"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Microsoft Auto Update on MacOS using homebrew

- App Name: Microsoft Auto Update
- App description: Provides updates to various Microsoft products
- App Version: 4.43.22011101
- App Website: https://docs.microsoft.com/officeupdates/release-history-microsoft-autoupdate

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Microsoft Auto Update with the following command
   ```
   brew install --cask microsoft-auto-update
   ```
4. Microsoft Auto Update is ready to use now!