---
title: "Install chipmunk on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "2D rigid body physics library written in C"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install chipmunk on MacOS using homebrew

- App Name: chipmunk
- App description: 2D rigid body physics library written in C
- App Version: 7.0.3
- App Website: https://chipmunk-physics.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install chipmunk with the following command
   ```
   brew install chipmunk
   ```
4. chipmunk is ready to use now!