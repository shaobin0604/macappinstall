---
title: "Install MenuBar Stats on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "System monitor with temperature & fans plugins"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install MenuBar Stats on MacOS using homebrew

- App Name: MenuBar Stats
- App description: System monitor with temperature & fans plugins
- App Version: 3.8.5,20211201
- App Website: https://seense.com/menubarstats/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install MenuBar Stats with the following command
   ```
   brew install --cask menubar-stats
   ```
4. MenuBar Stats is ready to use now!