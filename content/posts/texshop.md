---
title: "Install TeXShop on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "TeX previewer"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install TeXShop on MacOS using homebrew

- App Name: TeXShop
- App description: TeX previewer
- App Version: 4.69
- App Website: https://pages.uoregon.edu/koch/texshop/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install TeXShop with the following command
   ```
   brew install --cask texshop
   ```
4. TeXShop is ready to use now!