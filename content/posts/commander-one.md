---
title: "Install Commander One on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Two-panel file manager"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Commander One on MacOS using homebrew

- App Name: Commander One
- App description: Two-panel file manager
- App Version: 3.3,3508
- App Website: https://mac.eltima.com/file-manager.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Commander One with the following command
   ```
   brew install --cask commander-one
   ```
4. Commander One is ready to use now!