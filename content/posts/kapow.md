---
title: "Install Kapow on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Punch clock program"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Kapow on MacOS using homebrew

- App Name: Kapow
- App description: Punch clock program
- App Version: 1.5.10
- App Website: https://gottcode.org/kapow/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Kapow with the following command
   ```
   brew install --cask kapow
   ```
4. Kapow is ready to use now!