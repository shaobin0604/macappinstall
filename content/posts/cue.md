---
title: "Install cue on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Validate and define text-based and dynamic configuration"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cue on MacOS using homebrew

- App Name: cue
- App description: Validate and define text-based and dynamic configuration
- App Version: 0.4.2
- App Website: https://cuelang.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cue with the following command
   ```
   brew install cue
   ```
4. cue is ready to use now!