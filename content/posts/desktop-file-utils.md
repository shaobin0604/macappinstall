---
title: "Install desktop-file-utils on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line utilities for working with desktop entries"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install desktop-file-utils on MacOS using homebrew

- App Name: desktop-file-utils
- App description: Command-line utilities for working with desktop entries
- App Version: 0.26
- App Website: https://wiki.freedesktop.org/www/Software/desktop-file-utils/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install desktop-file-utils with the following command
   ```
   brew install desktop-file-utils
   ```
4. desktop-file-utils is ready to use now!