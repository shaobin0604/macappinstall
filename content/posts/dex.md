---
title: "Install dex on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Dextrous text editor"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dex on MacOS using homebrew

- App Name: dex
- App description: Dextrous text editor
- App Version: 1.0
- App Website: https://github.com/tihirvon/dex

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dex with the following command
   ```
   brew install dex
   ```
4. dex is ready to use now!