---
title: "Install chordii on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Text file to music sheet converter"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install chordii on MacOS using homebrew

- App Name: chordii
- App description: Text file to music sheet converter
- App Version: 4.5.3b
- App Website: https://www.vromans.org/johan/projects/Chordii/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install chordii with the following command
   ```
   brew install chordii
   ```
4. chordii is ready to use now!