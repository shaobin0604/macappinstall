---
title: "Install git-branchless on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "High-velocity, monorepo-scale workflow for Git"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install git-branchless on MacOS using homebrew

- App Name: git-branchless
- App description: High-velocity, monorepo-scale workflow for Git
- App Version: 0.3.9
- App Website: https://github.com/arxanas/git-branchless

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install git-branchless with the following command
   ```
   brew install git-branchless
   ```
4. git-branchless is ready to use now!