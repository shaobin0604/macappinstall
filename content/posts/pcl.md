---
title: "Install pcl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library for 2D/3D image and point cloud processing"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pcl on MacOS using homebrew

- App Name: pcl
- App description: Library for 2D/3D image and point cloud processing
- App Version: 1.12.1
- App Website: https://pointclouds.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pcl with the following command
   ```
   brew install pcl
   ```
4. pcl is ready to use now!