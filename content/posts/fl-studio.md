---
title: "Install FL Studio on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Digital audio production application"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install FL Studio on MacOS using homebrew

- App Name: FL Studio
- App description: Digital audio production application
- App Version: 20.9.0.2256
- App Website: https://www.image-line.com/flstudio/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install FL Studio with the following command
   ```
   brew install --cask fl-studio
   ```
4. FL Studio is ready to use now!