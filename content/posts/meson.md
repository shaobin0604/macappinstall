---
title: "Install meson on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Fast and user friendly build system"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install meson on MacOS using homebrew

- App Name: meson
- App description: Fast and user friendly build system
- App Version: 0.61.2
- App Website: https://mesonbuild.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install meson with the following command
   ```
   brew install meson
   ```
4. meson is ready to use now!