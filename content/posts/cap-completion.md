---
title: "Install cap-completion on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Bash completion for Capistrano"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cap-completion on MacOS using homebrew

- App Name: cap-completion
- App description: Bash completion for Capistrano
- App Version: 1.0.0
- App Website: https://github.com/bashaus/capistrano-autocomplete

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cap-completion with the following command
   ```
   brew install cap-completion
   ```
4. cap-completion is ready to use now!