---
title: "Install ivy on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Agile dependency manager"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ivy on MacOS using homebrew

- App Name: ivy
- App description: Agile dependency manager
- App Version: 2.5.0
- App Website: https://ant.apache.org/ivy/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ivy with the following command
   ```
   brew install ivy
   ```
4. ivy is ready to use now!