---
title: "Install miniforge on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Minimal installer for conda specific to conda-forge"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install miniforge on MacOS using homebrew

- App Name: miniforge
- App description: Minimal installer for conda specific to conda-forge
- App Version: 4.11.0-2
- App Website: https://github.com/conda-forge/miniforge

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install miniforge with the following command
   ```
   brew install --cask miniforge
   ```
4. miniforge is ready to use now!