---
title: "Install WezTerm on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GPU-accelerated cross-platform terminal emulator and multiplexer"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install WezTerm on MacOS using homebrew

- App Name: WezTerm
- App description: GPU-accelerated cross-platform terminal emulator and multiplexer
- App Version: 20220101-133340,7edc5b5a
- App Website: https://wezfurlong.org/wezterm/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install WezTerm with the following command
   ```
   brew install --cask wezterm
   ```
4. WezTerm is ready to use now!