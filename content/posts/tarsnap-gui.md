---
title: "Install tarsnap-gui on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cross-platform GUI for the Tarsnap command-line client"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tarsnap-gui on MacOS using homebrew

- App Name: tarsnap-gui
- App description: Cross-platform GUI for the Tarsnap command-line client
- App Version: 1.0.2
- App Website: https://github.com/Tarsnap/tarsnap-gui/wiki

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tarsnap-gui with the following command
   ```
   brew install tarsnap-gui
   ```
4. tarsnap-gui is ready to use now!