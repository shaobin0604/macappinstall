---
title: "Install libmpc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C library for the arithmetic of high precision complex numbers"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libmpc on MacOS using homebrew

- App Name: libmpc
- App description: C library for the arithmetic of high precision complex numbers
- App Version: 1.2.1
- App Website: http://www.multiprecision.org/mpc/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libmpc with the following command
   ```
   brew install libmpc
   ```
4. libmpc is ready to use now!