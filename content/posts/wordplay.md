---
title: "Install wordplay on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Anagram generator"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install wordplay on MacOS using homebrew

- App Name: wordplay
- App description: Anagram generator
- App Version: 7.22
- App Website: http://hsvmovies.com/static_subpages/personal_orig/wordplay/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install wordplay with the following command
   ```
   brew install wordplay
   ```
4. wordplay is ready to use now!