---
title: "Install libswiften on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C++ library for implementing XMPP applications"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libswiften on MacOS using homebrew

- App Name: libswiften
- App description: C++ library for implementing XMPP applications
- App Version: 4.0
- App Website: https://swift.im/swiften.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libswiften with the following command
   ```
   brew install libswiften
   ```
4. libswiften is ready to use now!