---
title: "Install sword on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cross-platform tools to write Bible software"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sword on MacOS using homebrew

- App Name: sword
- App description: Cross-platform tools to write Bible software
- App Version: 1.9.0
- App Website: https://www.crosswire.org/sword/index.jsp

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sword with the following command
   ```
   brew install sword
   ```
4. sword is ready to use now!