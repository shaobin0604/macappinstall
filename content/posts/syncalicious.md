---
title: "Install Syncalicious on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Backup and synchronize preferences across multiple machines"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Syncalicious on MacOS using homebrew

- App Name: Syncalicious
- App description: Backup and synchronize preferences across multiple machines
- App Version: 0.0.6
- App Website: https://github.com/zenangst/Syncalicious

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Syncalicious with the following command
   ```
   brew install --cask syncalicious
   ```
4. Syncalicious is ready to use now!