---
title: "Install darcs on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Distributed version control system that tracks changes, via Haskell"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install darcs on MacOS using homebrew

- App Name: darcs
- App description: Distributed version control system that tracks changes, via Haskell
- App Version: 2.16.4
- App Website: http://darcs.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install darcs with the following command
   ```
   brew install darcs
   ```
4. darcs is ready to use now!