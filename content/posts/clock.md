---
title: "Install Clock on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Big clock"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Clock on MacOS using homebrew

- App Name: Clock
- App description: Big clock
- App Version: 1.1
- App Website: https://github.com/zachwaugh/Clock.app

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Clock with the following command
   ```
   brew install --cask clock
   ```
4. Clock is ready to use now!