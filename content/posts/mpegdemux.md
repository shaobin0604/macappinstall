---
title: "Install mpegdemux on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "MPEG1/2 system stream demultiplexer"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mpegdemux on MacOS using homebrew

- App Name: mpegdemux
- App description: MPEG1/2 system stream demultiplexer
- App Version: 0.1.4
- App Website: http://www.hampa.ch/mpegdemux/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mpegdemux with the following command
   ```
   brew install mpegdemux
   ```
4. mpegdemux is ready to use now!