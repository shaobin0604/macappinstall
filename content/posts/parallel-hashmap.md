---
title: "Install parallel-hashmap on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Family of header-only, fast, memory-friendly C++ hashmap and btree containers"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install parallel-hashmap on MacOS using homebrew

- App Name: parallel-hashmap
- App description: Family of header-only, fast, memory-friendly C++ hashmap and btree containers
- App Version: 1.33
- App Website: https://greg7mdp.github.io/parallel-hashmap/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install parallel-hashmap with the following command
   ```
   brew install parallel-hashmap
   ```
4. parallel-hashmap is ready to use now!