---
title: "Install plenv on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Perl binary manager"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install plenv on MacOS using homebrew

- App Name: plenv
- App description: Perl binary manager
- App Version: 2.3.1
- App Website: https://github.com/tokuhirom/plenv

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install plenv with the following command
   ```
   brew install plenv
   ```
4. plenv is ready to use now!