---
title: "Install janet on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Dynamic language and bytecode vm"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install janet on MacOS using homebrew

- App Name: janet
- App description: Dynamic language and bytecode vm
- App Version: 1.20.0
- App Website: https://janet-lang.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install janet with the following command
   ```
   brew install janet
   ```
4. janet is ready to use now!