---
title: "Install known_hosts on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line manager for known hosts"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install known_hosts on MacOS using homebrew

- App Name: known_hosts
- App description: Command-line manager for known hosts
- App Version: 1.0.0
- App Website: https://github.com/markmcconachie/known_hosts

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install known_hosts with the following command
   ```
   brew install known_hosts
   ```
4. known_hosts is ready to use now!