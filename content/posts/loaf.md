---
title: "Install Loaf on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Animated icon library"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Loaf on MacOS using homebrew

- App Name: Loaf
- App description: Animated icon library
- App Version: 1.1.18
- App Website: https://getloaf.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Loaf with the following command
   ```
   brew install --cask loaf
   ```
4. Loaf is ready to use now!