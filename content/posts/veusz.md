---
title: "Install Veusz on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Scientific plotting application"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Veusz on MacOS using homebrew

- App Name: Veusz
- App description: Scientific plotting application
- App Version: 3.4
- App Website: https://veusz.github.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Veusz with the following command
   ```
   brew install --cask veusz
   ```
4. Veusz is ready to use now!