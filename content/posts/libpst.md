---
title: "Install libpst on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utilities for the PST file format"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libpst on MacOS using homebrew

- App Name: libpst
- App description: Utilities for the PST file format
- App Version: 0.6.76
- App Website: https://www.five-ten-sg.com/libpst/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libpst with the following command
   ```
   brew install libpst
   ```
4. libpst is ready to use now!