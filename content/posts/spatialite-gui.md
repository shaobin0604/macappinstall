---
title: "Install spatialite-gui on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GUI tool supporting SpatiaLite"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install spatialite-gui on MacOS using homebrew

- App Name: spatialite-gui
- App description: GUI tool supporting SpatiaLite
- App Version: 1.7.1
- App Website: https://www.gaia-gis.it/fossil/spatialite_gui/index

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install spatialite-gui with the following command
   ```
   brew install spatialite-gui
   ```
4. spatialite-gui is ready to use now!