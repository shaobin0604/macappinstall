---
title: "Install Godot Engine on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Game development engine"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Godot Engine on MacOS using homebrew

- App Name: Godot Engine
- App description: Game development engine
- App Version: 3.4.2
- App Website: https://godotengine.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Godot Engine with the following command
   ```
   brew install --cask godot
   ```
4. Godot Engine is ready to use now!