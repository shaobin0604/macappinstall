---
title: "Install packmol on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Packing optimization for molecular dynamics simulations"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install packmol on MacOS using homebrew

- App Name: packmol
- App description: Packing optimization for molecular dynamics simulations
- App Version: 20.010
- App Website: https://www.ime.unicamp.br/~martinez/packmol/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install packmol with the following command
   ```
   brew install packmol
   ```
4. packmol is ready to use now!