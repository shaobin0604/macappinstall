---
title: "Install radamsa on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Test case generator for robustness testing (a.k.a. a \"fuzzer\")"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install radamsa on MacOS using homebrew

- App Name: radamsa
- App description: Test case generator for robustness testing (a.k.a. a "fuzzer")
- App Version: 0.6
- App Website: https://gitlab.com/akihe/radamsa

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install radamsa with the following command
   ```
   brew install radamsa
   ```
4. radamsa is ready to use now!