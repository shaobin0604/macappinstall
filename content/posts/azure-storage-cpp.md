---
title: "Install azure-storage-cpp on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Microsoft Azure Storage Client Library for C++"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install azure-storage-cpp on MacOS using homebrew

- App Name: azure-storage-cpp
- App description: Microsoft Azure Storage Client Library for C++
- App Version: 7.5.0
- App Website: https://azure.github.io/azure-storage-cpp

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install azure-storage-cpp with the following command
   ```
   brew install azure-storage-cpp
   ```
4. azure-storage-cpp is ready to use now!