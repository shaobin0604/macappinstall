---
title: "Install xmlformat on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Format XML documents"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install xmlformat on MacOS using homebrew

- App Name: xmlformat
- App description: Format XML documents
- App Version: 1.04
- App Website: http://www.kitebird.com/software/xmlformat/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install xmlformat with the following command
   ```
   brew install xmlformat
   ```
4. xmlformat is ready to use now!