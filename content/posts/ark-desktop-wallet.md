---
title: "Install Ark Desktop Wallet on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Multi Platform ARK Desktop Wallet"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Ark Desktop Wallet on MacOS using homebrew

- App Name: Ark Desktop Wallet
- App description: Multi Platform ARK Desktop Wallet
- App Version: 2.9.5
- App Website: https://ark.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Ark Desktop Wallet with the following command
   ```
   brew install --cask ark-desktop-wallet
   ```
4. Ark Desktop Wallet is ready to use now!