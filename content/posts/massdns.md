---
title: "Install massdns on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "High-performance DNS stub resolver"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install massdns on MacOS using homebrew

- App Name: massdns
- App description: High-performance DNS stub resolver
- App Version: 1.0.0
- App Website: https://github.com/blechschmidt/massdns

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install massdns with the following command
   ```
   brew install massdns
   ```
4. massdns is ready to use now!