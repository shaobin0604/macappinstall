---
title: "Install vert on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line version testing"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install vert on MacOS using homebrew

- App Name: vert
- App description: Command-line version testing
- App Version: 0.1.0
- App Website: https://github.com/Masterminds/vert

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install vert with the following command
   ```
   brew install vert
   ```
4. vert is ready to use now!