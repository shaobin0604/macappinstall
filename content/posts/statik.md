---
title: "Install statik on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Python-based, generic static web site generator aimed at developers"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install statik on MacOS using homebrew

- App Name: statik
- App description: Python-based, generic static web site generator aimed at developers
- App Version: 0.23.0
- App Website: https://getstatik.com

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install statik with the following command
   ```
   brew install statik
   ```
4. statik is ready to use now!