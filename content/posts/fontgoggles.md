---
title: "Install FontGoggles on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Font viewer for various font formats"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install FontGoggles on MacOS using homebrew

- App Name: FontGoggles
- App description: Font viewer for various font formats
- App Version: 1.4.4
- App Website: https://fontgoggles.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install FontGoggles with the following command
   ```
   brew install --cask fontgoggles
   ```
4. FontGoggles is ready to use now!