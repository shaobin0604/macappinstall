---
title: "Install z.lua on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "New cd command that helps you navigate faster by learning your habits"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install z.lua on MacOS using homebrew

- App Name: z.lua
- App description: New cd command that helps you navigate faster by learning your habits
- App Version: 1.8.14
- App Website: https://github.com/skywind3000/z.lua

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install z.lua with the following command
   ```
   brew install z.lua
   ```
4. z.lua is ready to use now!