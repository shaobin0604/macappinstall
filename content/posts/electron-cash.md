---
title: "Install Electron Cash on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Thin client for Bitcoin Cash"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Electron Cash on MacOS using homebrew

- App Name: Electron Cash
- App description: Thin client for Bitcoin Cash
- App Version: 4.2.6
- App Website: https://electroncash.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Electron Cash with the following command
   ```
   brew install --cask electron-cash
   ```
4. Electron Cash is ready to use now!