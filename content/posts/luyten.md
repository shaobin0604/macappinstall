---
title: "Install Luyten on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Open-source Java decompiler GUI for Procyon"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Luyten on MacOS using homebrew

- App Name: Luyten
- App description: Open-source Java decompiler GUI for Procyon
- App Version: 0.5.4
- App Website: https://deathmarine.github.io/Luyten/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Luyten with the following command
   ```
   brew install --cask luyten
   ```
4. Luyten is ready to use now!