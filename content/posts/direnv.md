---
title: "Install direnv on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Load/unload environment variables based on $PWD"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install direnv on MacOS using homebrew

- App Name: direnv
- App description: Load/unload environment variables based on $PWD
- App Version: 2.30.3
- App Website: https://direnv.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install direnv with the following command
   ```
   brew install direnv
   ```
4. direnv is ready to use now!