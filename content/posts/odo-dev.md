---
title: "Install odo-dev on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Developer-focused CLI for Kubernetes and OpenShift"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install odo-dev on MacOS using homebrew

- App Name: odo-dev
- App description: Developer-focused CLI for Kubernetes and OpenShift
- App Version: 2.5.0
- App Website: https://odo.dev

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install odo-dev with the following command
   ```
   brew install odo-dev
   ```
4. odo-dev is ready to use now!