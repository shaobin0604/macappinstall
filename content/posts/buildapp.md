---
title: "Install buildapp on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Creates executables with SBCL"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install buildapp on MacOS using homebrew

- App Name: buildapp
- App description: Creates executables with SBCL
- App Version: 1.5.6
- App Website: https://www.xach.com/lisp/buildapp/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install buildapp with the following command
   ```
   brew install buildapp
   ```
4. buildapp is ready to use now!