---
title: "Install UncrustifyX on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Uncrustify utility and documentation browser"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install UncrustifyX on MacOS using homebrew

- App Name: UncrustifyX
- App description: Uncrustify utility and documentation browser
- App Version: 0.4.3
- App Website: https://github.com/ryanmaxwell/UncrustifyX

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install UncrustifyX with the following command
   ```
   brew install --cask uncrustifyx
   ```
4. UncrustifyX is ready to use now!