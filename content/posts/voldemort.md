---
title: "Install voldemort on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Distributed key-value storage system"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install voldemort on MacOS using homebrew

- App Name: voldemort
- App description: Distributed key-value storage system
- App Version: 1.10.26
- App Website: https://www.project-voldemort.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install voldemort with the following command
   ```
   brew install voldemort
   ```
4. voldemort is ready to use now!