---
title: "Install Displaperture on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Rounds your display corners"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Displaperture on MacOS using homebrew

- App Name: Displaperture
- App description: Rounds your display corners
- App Version: 2.2,1055
- App Website: https://manytricks.com/displaperture/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Displaperture with the following command
   ```
   brew install --cask displaperture
   ```
4. Displaperture is ready to use now!