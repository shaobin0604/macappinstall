---
title: "Install fmt on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Open-source formatting library for C++"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fmt on MacOS using homebrew

- App Name: fmt
- App description: Open-source formatting library for C++
- App Version: 8.1.1
- App Website: https://fmt.dev/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fmt with the following command
   ```
   brew install fmt
   ```
4. fmt is ready to use now!