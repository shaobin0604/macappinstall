---
title: "Install h264bitstream on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library for reading and writing H264 video streams"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install h264bitstream on MacOS using homebrew

- App Name: h264bitstream
- App description: Library for reading and writing H264 video streams
- App Version: 0.2.0
- App Website: https://h264bitstream.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install h264bitstream with the following command
   ```
   brew install h264bitstream
   ```
4. h264bitstream is ready to use now!