---
title: "Install plantuml on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Draw UML diagrams"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install plantuml on MacOS using homebrew

- App Name: plantuml
- App description: Draw UML diagrams
- App Version: 1.2022.1
- App Website: https://plantuml.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install plantuml with the following command
   ```
   brew install plantuml
   ```
4. plantuml is ready to use now!