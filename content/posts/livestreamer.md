---
title: "Install livestreamer on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Pipes video from streaming services into a player such as VLC"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install livestreamer on MacOS using homebrew

- App Name: livestreamer
- App description: Pipes video from streaming services into a player such as VLC
- App Version: 1.12.2
- App Website: https://livestreamer.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install livestreamer with the following command
   ```
   brew install livestreamer
   ```
4. livestreamer is ready to use now!