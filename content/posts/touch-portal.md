---
title: "Install Touch Portal on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Macro remote control"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Touch Portal on MacOS using homebrew

- App Name: Touch Portal
- App description: Macro remote control
- App Version: 2.3.010
- App Website: https://www.touch-portal.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Touch Portal with the following command
   ```
   brew install --cask touch-portal
   ```
4. Touch Portal is ready to use now!