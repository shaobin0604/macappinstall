---
title: "Install pgpool-ii on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "PostgreSQL connection pool server"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pgpool-ii on MacOS using homebrew

- App Name: pgpool-ii
- App description: PostgreSQL connection pool server
- App Version: 4.3.0
- App Website: https://www.pgpool.net/mediawiki/index.php/Main_Page

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pgpool-ii with the following command
   ```
   brew install pgpool-ii
   ```
4. pgpool-ii is ready to use now!