---
title: "Install geocode-glib on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GNOME library for gecoding and reverse geocoding"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install geocode-glib on MacOS using homebrew

- App Name: geocode-glib
- App description: GNOME library for gecoding and reverse geocoding
- App Version: 3.26.2
- App Website: https://developer.gnome.org/geocode-glib

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install geocode-glib with the following command
   ```
   brew install geocode-glib
   ```
4. geocode-glib is ready to use now!