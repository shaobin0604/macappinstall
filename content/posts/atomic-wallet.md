---
title: "Install Atomic Wallet on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Manage Bitcoin, Ethereum, XRP, Litecoin, XLM and over 300 other coins and tokens"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Atomic Wallet on MacOS using homebrew

- App Name: Atomic Wallet
- App description: Manage Bitcoin, Ethereum, XRP, Litecoin, XLM and over 300 other coins and tokens
- App Version: 2.37.0
- App Website: https://atomicwallet.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Atomic Wallet with the following command
   ```
   brew install --cask atomic-wallet
   ```
4. Atomic Wallet is ready to use now!