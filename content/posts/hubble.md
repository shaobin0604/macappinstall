---
title: "Install hubble on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Network, Service & Security Observability for Kubernetes using eBPF"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install hubble on MacOS using homebrew

- App Name: hubble
- App description: Network, Service & Security Observability for Kubernetes using eBPF
- App Version: 0.9.0
- App Website: https://github.com/cilium/hubble

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install hubble with the following command
   ```
   brew install hubble
   ```
4. hubble is ready to use now!