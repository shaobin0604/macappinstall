---
title: "Install ActivityWatch on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Time tracker"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ActivityWatch on MacOS using homebrew

- App Name: ActivityWatch
- App description: Time tracker
- App Version: 0.11.0
- App Website: https://activitywatch.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ActivityWatch with the following command
   ```
   brew install --cask activitywatch
   ```
4. ActivityWatch is ready to use now!