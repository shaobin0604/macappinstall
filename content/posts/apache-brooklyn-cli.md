---
title: "Install apache-brooklyn-cli on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Apache Brooklyn command-line interface"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install apache-brooklyn-cli on MacOS using homebrew

- App Name: apache-brooklyn-cli
- App description: Apache Brooklyn command-line interface
- App Version: 1.0.0
- App Website: https://brooklyn.apache.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install apache-brooklyn-cli with the following command
   ```
   brew install apache-brooklyn-cli
   ```
4. apache-brooklyn-cli is ready to use now!