---
title: "Install vf on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Enhanced version of `cd` command"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install vf on MacOS using homebrew

- App Name: vf
- App description: Enhanced version of `cd` command
- App Version: 0.0.1
- App Website: https://github.com/glejeune/vf

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install vf with the following command
   ```
   brew install vf
   ```
4. vf is ready to use now!