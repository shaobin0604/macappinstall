---
title: "Install Secure Pipes on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Manage SSH tunnels"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Secure Pipes on MacOS using homebrew

- App Name: Secure Pipes
- App description: Manage SSH tunnels
- App Version: 0.99.11,c67223c50be3604
- App Website: https://www.opoet.com/pyro/index.php/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Secure Pipes with the following command
   ```
   brew install --cask secure-pipes
   ```
4. Secure Pipes is ready to use now!