---
title: "Install powerline-go on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Beautiful and useful low-latency prompt for your shell"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install powerline-go on MacOS using homebrew

- App Name: powerline-go
- App description: Beautiful and useful low-latency prompt for your shell
- App Version: 1.21.0
- App Website: https://github.com/justjanne/powerline-go

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install powerline-go with the following command
   ```
   brew install powerline-go
   ```
4. powerline-go is ready to use now!