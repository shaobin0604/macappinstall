---
title: "Install HP ePrint on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Mobile printing solution"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install HP ePrint on MacOS using homebrew

- App Name: HP ePrint
- App description: Mobile printing solution
- App Version: 2.5.0
- App Website: https://h20331.www2.hp.com/hpsub/us/en/eprint/overview.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install HP ePrint with the following command
   ```
   brew install --cask hp-eprint
   ```
4. HP ePrint is ready to use now!