---
title: "Install OpenBCI on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Connect to OpenBCI hardware, visualize and stream physiological data"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install OpenBCI on MacOS using homebrew

- App Name: OpenBCI
- App description: Connect to OpenBCI hardware, visualize and stream physiological data
- App Version: 5.0.9,2021-11-06_00-16-07
- App Website: https://openbci.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install OpenBCI with the following command
   ```
   brew install --cask openbci
   ```
4. OpenBCI is ready to use now!