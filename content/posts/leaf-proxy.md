---
title: "Install leaf-proxy on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Lightweight and fast proxy utility"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install leaf-proxy on MacOS using homebrew

- App Name: leaf-proxy
- App description: Lightweight and fast proxy utility
- App Version: 0.4.1
- App Website: https://github.com/eycorsican/leaf

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install leaf-proxy with the following command
   ```
   brew install leaf-proxy
   ```
4. leaf-proxy is ready to use now!