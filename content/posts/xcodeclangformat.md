---
title: "Install XcodeClangFormat on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Format code in Xcode with clang-format"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install XcodeClangFormat on MacOS using homebrew

- App Name: XcodeClangFormat
- App description: Format code in Xcode with clang-format
- App Version: 1.2.1
- App Website: https://github.com/mapbox/XcodeClangFormat

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install XcodeClangFormat with the following command
   ```
   brew install --cask xcodeclangformat
   ```
4. XcodeClangFormat is ready to use now!