---
title: "Install tinc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Virtual Private Network (VPN) tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tinc on MacOS using homebrew

- App Name: tinc
- App description: Virtual Private Network (VPN) tool
- App Version: 1.0.36
- App Website: https://www.tinc-vpn.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tinc with the following command
   ```
   brew install tinc
   ```
4. tinc is ready to use now!