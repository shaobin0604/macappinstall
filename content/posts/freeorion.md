---
title: "Install FreeOrion on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Turn-based space empire and galactic conquest game"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install FreeOrion on MacOS using homebrew

- App Name: FreeOrion
- App description: Turn-based space empire and galactic conquest game
- App Version: 0.4.10.2,2021-08-01,f663dad
- App Website: https://freeorion.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install FreeOrion with the following command
   ```
   brew install --cask freeorion
   ```
4. FreeOrion is ready to use now!