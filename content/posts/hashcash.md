---
title: "Install hashcash on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Proof-of-work algorithm to counter denial-of-service (DoS) attacks"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install hashcash on MacOS using homebrew

- App Name: hashcash
- App description: Proof-of-work algorithm to counter denial-of-service (DoS) attacks
- App Version: 1.22
- App Website: http://hashcash.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install hashcash with the following command
   ```
   brew install hashcash
   ```
4. hashcash is ready to use now!