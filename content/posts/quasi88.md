---
title: "Install quasi88 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "PC-8801 emulator"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install quasi88 on MacOS using homebrew

- App Name: quasi88
- App description: PC-8801 emulator
- App Version: 0.6.4
- App Website: https://www.eonet.ne.jp/~showtime/quasi88/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install quasi88 with the following command
   ```
   brew install quasi88
   ```
4. quasi88 is ready to use now!