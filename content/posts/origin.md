---
title: "Install Origin on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Play PC games and connect with your friends"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Origin on MacOS using homebrew

- App Name: Origin
- App description: Play PC games and connect with your friends
- App Version: 10.5.108.49699
- App Website: https://www.origin.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Origin with the following command
   ```
   brew install --cask origin
   ```
4. Origin is ready to use now!