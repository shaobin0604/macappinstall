---
title: "Install mydumper on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "How MySQL DBA & support engineer would imagine 'mysqldump' ;-)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mydumper on MacOS using homebrew

- App Name: mydumper
- App description: How MySQL DBA & support engineer would imagine 'mysqldump' ;-)
- App Version: 0.11.5-2
- App Website: https://launchpad.net/mydumper

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mydumper with the following command
   ```
   brew install mydumper
   ```
4. mydumper is ready to use now!