---
title: "Install Specter on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Desktop GUI for Bitcoin Core optimised to work with hardware wallets"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Specter on MacOS using homebrew

- App Name: Specter
- App description: Desktop GUI for Bitcoin Core optimised to work with hardware wallets
- App Version: 1.7.2
- App Website: https://specter.solutions/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Specter with the following command
   ```
   brew install --cask specter
   ```
4. Specter is ready to use now!