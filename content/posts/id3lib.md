---
title: "Install id3lib on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "ID3 tag manipulation"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install id3lib on MacOS using homebrew

- App Name: id3lib
- App description: ID3 tag manipulation
- App Version: 3.8.3
- App Website: https://id3lib.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install id3lib with the following command
   ```
   brew install id3lib
   ```
4. id3lib is ready to use now!