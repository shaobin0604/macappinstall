---
title: "Install bchunk on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Convert CD images from .bin/.cue to .iso/.cdr"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bchunk on MacOS using homebrew

- App Name: bchunk
- App description: Convert CD images from .bin/.cue to .iso/.cdr
- App Version: 1.2.2
- App Website: http://he.fi/bchunk/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bchunk with the following command
   ```
   brew install bchunk
   ```
4. bchunk is ready to use now!