---
title: "Install A Slower Speed of Light on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "First-person game"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install A Slower Speed of Light on MacOS using homebrew

- App Name: A Slower Speed of Light
- App description: First-person game
- App Version: 2020
- App Website: http://gamelab.mit.edu/games/a-slower-speed-of-light/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install A Slower Speed of Light with the following command
   ```
   brew install --cask a-slower-speed-of-light
   ```
4. A Slower Speed of Light is ready to use now!