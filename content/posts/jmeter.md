---
title: "Install jmeter on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Load testing and performance measurement application"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install jmeter on MacOS using homebrew

- App Name: jmeter
- App description: Load testing and performance measurement application
- App Version: 5.4.3
- App Website: https://jmeter.apache.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install jmeter with the following command
   ```
   brew install jmeter
   ```
4. jmeter is ready to use now!