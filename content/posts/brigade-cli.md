---
title: "Install brigade-cli on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Brigade command-line interface"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install brigade-cli on MacOS using homebrew

- App Name: brigade-cli
- App description: Brigade command-line interface
- App Version: 2.3.0
- App Website: https://brigade.sh

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install brigade-cli with the following command
   ```
   brew install brigade-cli
   ```
4. brigade-cli is ready to use now!