---
title: "Install PlistEDPlus on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Plist file editor"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install PlistEDPlus on MacOS using homebrew

- App Name: PlistEDPlus
- App description: Plist file editor
- App Version: 1.2.34
- App Website: https://github.com/ic005k/PlistEDPlus

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install PlistEDPlus with the following command
   ```
   brew install --cask plistedplus
   ```
4. PlistEDPlus is ready to use now!