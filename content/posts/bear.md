---
title: "Install bear on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Generate compilation database for clang tooling"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bear on MacOS using homebrew

- App Name: bear
- App description: Generate compilation database for clang tooling
- App Version: 3.0.18
- App Website: https://github.com/rizsotto/Bear

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bear with the following command
   ```
   brew install bear
   ```
4. bear is ready to use now!