---
title: "Install bbe on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Sed-like editor for binary files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bbe on MacOS using homebrew

- App Name: bbe
- App description: Sed-like editor for binary files
- App Version: 0.2.2
- App Website: https://sourceforge.net/projects/bbe-/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bbe with the following command
   ```
   brew install bbe
   ```
4. bbe is ready to use now!