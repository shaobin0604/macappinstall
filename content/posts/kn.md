---
title: "Install kn on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line interface for managing Knative Serving and Eventing resources"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install kn on MacOS using homebrew

- App Name: kn
- App description: Command-line interface for managing Knative Serving and Eventing resources
- App Version: 1.2.0
- App Website: https://github.com/knative/client

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install kn with the following command
   ```
   brew install kn
   ```
4. kn is ready to use now!