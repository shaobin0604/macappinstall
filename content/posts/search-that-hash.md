---
title: "Install search-that-hash on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Searches Hash APIs to crack your hash quickly"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install search-that-hash on MacOS using homebrew

- App Name: search-that-hash
- App description: Searches Hash APIs to crack your hash quickly
- App Version: 0.2.8
- App Website: https://github.com/HashPals/Search-That-Hash

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install search-that-hash with the following command
   ```
   brew install search-that-hash
   ```
4. search-that-hash is ready to use now!