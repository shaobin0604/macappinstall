---
title: "Install openliberty-jakartaee8 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Lightweight open framework for Java (Jakarta EE 8)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install openliberty-jakartaee8 on MacOS using homebrew

- App Name: openliberty-jakartaee8
- App description: Lightweight open framework for Java (Jakarta EE 8)
- App Version: 22.0.0.1
- App Website: https://openliberty.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install openliberty-jakartaee8 with the following command
   ```
   brew install openliberty-jakartaee8
   ```
4. openliberty-jakartaee8 is ready to use now!