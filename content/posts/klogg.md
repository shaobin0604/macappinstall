---
title: "Install Klogg on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Fast, advanced log explorer"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Klogg on MacOS using homebrew

- App Name: Klogg
- App description: Fast, advanced log explorer
- App Version: 20.12.0.813
- App Website: https://github.com/variar/klogg

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Klogg with the following command
   ```
   brew install --cask klogg
   ```
4. Klogg is ready to use now!