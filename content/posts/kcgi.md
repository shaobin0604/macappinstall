---
title: "Install kcgi on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Minimal CGI and FastCGI library for C/C++"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install kcgi on MacOS using homebrew

- App Name: kcgi
- App description: Minimal CGI and FastCGI library for C/C++
- App Version: 0.13.0
- App Website: https://kristaps.bsd.lv/kcgi/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install kcgi with the following command
   ```
   brew install kcgi
   ```
4. kcgi is ready to use now!