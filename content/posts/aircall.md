---
title: "Install Aircall on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cloud-based call center and phone system software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Aircall on MacOS using homebrew

- App Name: Aircall
- App description: Cloud-based call center and phone system software
- App Version: 2.19.7
- App Website: https://aircall.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Aircall with the following command
   ```
   brew install --cask aircall
   ```
4. Aircall is ready to use now!