---
title: "Install Wondershare Filmora on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Video editor"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Wondershare Filmora on MacOS using homebrew

- App Name: Wondershare Filmora
- App description: Video editor
- App Version: 10.5.0.23
- App Website: https://filmora.wondershare.com/video-editor-mac/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Wondershare Filmora with the following command
   ```
   brew install --cask wondershare-filmora
   ```
4. Wondershare Filmora is ready to use now!