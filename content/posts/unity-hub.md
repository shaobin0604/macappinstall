---
title: "Install Unity Hub on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Management tool for Unity"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Unity Hub on MacOS using homebrew

- App Name: Unity Hub
- App description: Management tool for Unity
- App Version: 3.0.1
- App Website: https://unity3d.com/get-unity/download

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Unity Hub with the following command
   ```
   brew install --cask unity-hub
   ```
4. Unity Hub is ready to use now!