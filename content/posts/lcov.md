---
title: "Install lcov on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Graphical front-end for GCC's coverage testing tool (gcov)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install lcov on MacOS using homebrew

- App Name: lcov
- App description: Graphical front-end for GCC's coverage testing tool (gcov)
- App Version: 1.15
- App Website: https://github.com/linux-test-project/lcov

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install lcov with the following command
   ```
   brew install lcov
   ```
4. lcov is ready to use now!