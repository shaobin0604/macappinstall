---
title: "Install FontExplorer X Pro on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Font finder and organizer"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install FontExplorer X Pro on MacOS using homebrew

- App Name: FontExplorer X Pro
- App description: Font finder and organizer
- App Version: 7.2.6
- App Website: https://www.fontexplorerx.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install FontExplorer X Pro with the following command
   ```
   brew install --cask fontexplorer-x-pro
   ```
4. FontExplorer X Pro is ready to use now!