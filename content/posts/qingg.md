---
title: "Install QinggIM on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Wubi input method"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install QinggIM on MacOS using homebrew

- App Name: QinggIM
- App description: Wubi input method
- App Version: 2.11,3411
- App Website: https://qingg.im/mac/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install QinggIM with the following command
   ```
   brew install --cask qingg
   ```
4. QinggIM is ready to use now!