---
title: "Install gPodder on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Podcast client"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gPodder on MacOS using homebrew

- App Name: gPodder
- App description: Podcast client
- App Version: 3.10.21
- App Website: https://gpodder.github.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gPodder with the following command
   ```
   brew install --cask gpodder
   ```
4. gPodder is ready to use now!