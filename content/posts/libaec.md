---
title: "Install libaec on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Adaptive Entropy Coding implementing Golomb-Rice algorithm"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libaec on MacOS using homebrew

- App Name: libaec
- App description: Adaptive Entropy Coding implementing Golomb-Rice algorithm
- App Version: 1.0.6
- App Website: https://gitlab.dkrz.de/k202009/libaec

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libaec with the following command
   ```
   brew install libaec
   ```
4. libaec is ready to use now!