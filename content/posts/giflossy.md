---
title: "Install giflossy on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Lossy LZW compression, reduces GIF file sizes by 30-50%"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install giflossy on MacOS using homebrew

- App Name: giflossy
- App description: Lossy LZW compression, reduces GIF file sizes by 30-50%
- App Version: 1.91
- App Website: https://pornel.net/lossygif

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install giflossy with the following command
   ```
   brew install giflossy
   ```
4. giflossy is ready to use now!