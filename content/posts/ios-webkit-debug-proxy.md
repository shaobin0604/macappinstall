---
title: "Install ios-webkit-debug-proxy on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "DevTools proxy for iOS devices"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ios-webkit-debug-proxy on MacOS using homebrew

- App Name: ios-webkit-debug-proxy
- App description: DevTools proxy for iOS devices
- App Version: 1.8.8
- App Website: https://github.com/google/ios-webkit-debug-proxy

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ios-webkit-debug-proxy with the following command
   ```
   brew install ios-webkit-debug-proxy
   ```
4. ios-webkit-debug-proxy is ready to use now!