---
title: "Install Snagit on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Screen capture software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Snagit on MacOS using homebrew

- App Name: Snagit
- App description: Screen capture software
- App Version: 2022.0.3
- App Website: https://www.techsmith.com/screen-capture.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Snagit with the following command
   ```
   brew install --cask snagit
   ```
4. Snagit is ready to use now!