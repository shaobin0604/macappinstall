---
title: "Install pius on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "PGP individual UID signer"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pius on MacOS using homebrew

- App Name: pius
- App description: PGP individual UID signer
- App Version: 3.0.0
- App Website: https://www.phildev.net/pius/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pius with the following command
   ```
   brew install pius
   ```
4. pius is ready to use now!