---
title: "Install efm-langserver on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "General purpose Language Server"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install efm-langserver on MacOS using homebrew

- App Name: efm-langserver
- App description: General purpose Language Server
- App Version: 0.0.40
- App Website: https://github.com/mattn/efm-langserver

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install efm-langserver with the following command
   ```
   brew install efm-langserver
   ```
4. efm-langserver is ready to use now!