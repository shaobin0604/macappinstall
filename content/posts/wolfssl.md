---
title: "Install wolfssl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Embedded SSL Library written in C"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install wolfssl on MacOS using homebrew

- App Name: wolfssl
- App description: Embedded SSL Library written in C
- App Version: 5.1.1
- App Website: https://www.wolfssl.com

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install wolfssl with the following command
   ```
   brew install wolfssl
   ```
4. wolfssl is ready to use now!