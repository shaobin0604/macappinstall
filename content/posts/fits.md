---
title: "Install fits on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "File Information Tool Set"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fits on MacOS using homebrew

- App Name: fits
- App description: File Information Tool Set
- App Version: 1.5.1
- App Website: https://projects.iq.harvard.edu/fits

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fits with the following command
   ```
   brew install fits
   ```
4. fits is ready to use now!