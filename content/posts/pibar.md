---
title: "Install PiBar on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Pi-hole(s) management in the menu bar"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install PiBar on MacOS using homebrew

- App Name: PiBar
- App description: Pi-hole(s) management in the menu bar
- App Version: 1.1.1
- App Website: https://github.com/amiantos/pibar

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install PiBar with the following command
   ```
   brew install --cask pibar
   ```
4. PiBar is ready to use now!