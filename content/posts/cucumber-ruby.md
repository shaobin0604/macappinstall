---
title: "Install cucumber-ruby on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cucumber for Ruby"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cucumber-ruby on MacOS using homebrew

- App Name: cucumber-ruby
- App description: Cucumber for Ruby
- App Version: 7.1.0
- App Website: https://cucumber.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cucumber-ruby with the following command
   ```
   brew install cucumber-ruby
   ```
4. cucumber-ruby is ready to use now!