---
title: "Install atmos on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Universal Tool for DevOps and Cloud Automation"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install atmos on MacOS using homebrew

- App Name: atmos
- App description: Universal Tool for DevOps and Cloud Automation
- App Version: 1.3.27
- App Website: https://github.com/cloudposse/atmos

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install atmos with the following command
   ```
   brew install atmos
   ```
4. atmos is ready to use now!