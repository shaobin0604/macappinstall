---
title: "Install nano on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Free (GNU) replacement for the Pico text editor"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install nano on MacOS using homebrew

- App Name: nano
- App description: Free (GNU) replacement for the Pico text editor
- App Version: 6.1
- App Website: https://www.nano-editor.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install nano with the following command
   ```
   brew install nano
   ```
4. nano is ready to use now!