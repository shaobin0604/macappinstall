---
title: "Install crosstool-ng on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool for building toolchains"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install crosstool-ng on MacOS using homebrew

- App Name: crosstool-ng
- App description: Tool for building toolchains
- App Version: 1.24.0
- App Website: https://crosstool-ng.github.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install crosstool-ng with the following command
   ```
   brew install crosstool-ng
   ```
4. crosstool-ng is ready to use now!