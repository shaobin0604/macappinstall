---
title: "Install sourcery on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Meta-programming for Swift, stop writing boilerplate code"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sourcery on MacOS using homebrew

- App Name: sourcery
- App description: Meta-programming for Swift, stop writing boilerplate code
- App Version: 1.6.1
- App Website: https://github.com/krzysztofzablocki/Sourcery

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sourcery with the following command
   ```
   brew install sourcery
   ```
4. sourcery is ready to use now!