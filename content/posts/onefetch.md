---
title: "Install onefetch on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Git repository summary on your terminal"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install onefetch on MacOS using homebrew

- App Name: onefetch
- App description: Git repository summary on your terminal
- App Version: 2.11.0
- App Website: https://github.com/o2sh/onefetch

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install onefetch with the following command
   ```
   brew install onefetch
   ```
4. onefetch is ready to use now!