---
title: "Install swagger2markup-cli on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Swagger to AsciiDoc or Markdown converter"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install swagger2markup-cli on MacOS using homebrew

- App Name: swagger2markup-cli
- App description: Swagger to AsciiDoc or Markdown converter
- App Version: 1.3.3
- App Website: https://github.com/Swagger2Markup/swagger2markup

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install swagger2markup-cli with the following command
   ```
   brew install swagger2markup-cli
   ```
4. swagger2markup-cli is ready to use now!