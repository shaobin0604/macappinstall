---
title: "Install roll on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CLI program for rolling a dice sequence"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install roll on MacOS using homebrew

- App Name: roll
- App description: CLI program for rolling a dice sequence
- App Version: 2.6.0
- App Website: https://matteocorti.github.io/roll/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install roll with the following command
   ```
   brew install roll
   ```
4. roll is ready to use now!