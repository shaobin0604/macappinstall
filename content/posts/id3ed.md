---
title: "Install id3ed on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "ID3 tag editor for MP3 files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install id3ed on MacOS using homebrew

- App Name: id3ed
- App description: ID3 tag editor for MP3 files
- App Version: 1.10.4
- App Website: http://code.fluffytapeworm.com/projects/id3ed

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install id3ed with the following command
   ```
   brew install id3ed
   ```
4. id3ed is ready to use now!