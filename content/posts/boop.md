---
title: "Install Boop on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Scriptable scratchpad for developers"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Boop on MacOS using homebrew

- App Name: Boop
- App description: Scriptable scratchpad for developers
- App Version: 1.4.0
- App Website: https://boop.okat.best/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Boop with the following command
   ```
   brew install --cask boop
   ```
4. Boop is ready to use now!