---
title: "Install KnockKnock on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to show what is persistently installed on the computer"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install KnockKnock on MacOS using homebrew

- App Name: KnockKnock
- App description: Tool to show what is persistently installed on the computer
- App Version: 2.3.0
- App Website: https://objective-see.com/products/knockknock.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install KnockKnock with the following command
   ```
   brew install --cask knockknock
   ```
4. KnockKnock is ready to use now!