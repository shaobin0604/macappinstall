---
title: "Install luv on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Bare libuv bindings for lua"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install luv on MacOS using homebrew

- App Name: luv
- App description: Bare libuv bindings for lua
- App Version: 1.43.0-0
- App Website: https://github.com/luvit/luv

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install luv with the following command
   ```
   brew install luv
   ```
4. luv is ready to use now!