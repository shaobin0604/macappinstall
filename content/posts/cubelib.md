---
title: "Install cubelib on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cube, is a performance report explorer for Scalasca and Score-P"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cubelib on MacOS using homebrew

- App Name: cubelib
- App description: Cube, is a performance report explorer for Scalasca and Score-P
- App Version: 4.6
- App Website: https://scalasca.org/software/cube-4.x/download.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cubelib with the following command
   ```
   brew install cubelib
   ```
4. cubelib is ready to use now!