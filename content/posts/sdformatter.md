---
title: "Install SD Formatter on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to format memory cards complying with the SD File System spec"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install SD Formatter on MacOS using homebrew

- App Name: SD Formatter
- App description: Tool to format memory cards complying with the SD File System spec
- App Version: 5.0.1
- App Website: https://www.sdcard.org/downloads/formatter/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install SD Formatter with the following command
   ```
   brew install --cask sdformatter
   ```
4. SD Formatter is ready to use now!