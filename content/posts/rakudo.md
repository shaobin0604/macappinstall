---
title: "Install rakudo on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Perl 6 compiler targeting MoarVM"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install rakudo on MacOS using homebrew

- App Name: rakudo
- App description: Perl 6 compiler targeting MoarVM
- App Version: 2021.12
- App Website: https://rakudo.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install rakudo with the following command
   ```
   brew install rakudo
   ```
4. rakudo is ready to use now!