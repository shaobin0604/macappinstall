---
title: "Install mono-libgdiplus on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GDI+-compatible API on non-Windows operating systems"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mono-libgdiplus on MacOS using homebrew

- App Name: mono-libgdiplus
- App description: GDI+-compatible API on non-Windows operating systems
- App Version: 6.1
- App Website: https://www.mono-project.com/docs/gui/libgdiplus/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mono-libgdiplus with the following command
   ```
   brew install mono-libgdiplus
   ```
4. mono-libgdiplus is ready to use now!