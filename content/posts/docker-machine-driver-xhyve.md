---
title: "Install docker-machine-driver-xhyve on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Docker Machine driver for xhyve"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install docker-machine-driver-xhyve on MacOS using homebrew

- App Name: docker-machine-driver-xhyve
- App description: Docker Machine driver for xhyve
- App Version: 0.4.0
- App Website: https://github.com/machine-drivers/docker-machine-driver-xhyve

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install docker-machine-driver-xhyve with the following command
   ```
   brew install docker-machine-driver-xhyve
   ```
4. docker-machine-driver-xhyve is ready to use now!