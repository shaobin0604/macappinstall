---
title: "Install Preference Manager on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Trash, backup, lock and restore video editor preferences"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Preference Manager on MacOS using homebrew

- App Name: Preference Manager
- App description: Trash, backup, lock and restore video editor preferences
- App Version: 4.5.2.0
- App Website: https://www.digitalrebellion.com/prefman/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Preference Manager with the following command
   ```
   brew install --cask preference-manager
   ```
4. Preference Manager is ready to use now!