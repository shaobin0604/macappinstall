---
title: "Install Mountain Duck on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Mounts servers and cloud storages as a disk on the desktop"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Mountain Duck on MacOS using homebrew

- App Name: Mountain Duck
- App description: Mounts servers and cloud storages as a disk on the desktop
- App Version: 4.10.3,19145
- App Website: https://mountainduck.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Mountain Duck with the following command
   ```
   brew install --cask mountain-duck
   ```
4. Mountain Duck is ready to use now!