---
title: "Install eralchemy on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Simple entity relation (ER) diagrams generation"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install eralchemy on MacOS using homebrew

- App Name: eralchemy
- App description: Simple entity relation (ER) diagrams generation
- App Version: 1.2.10
- App Website: https://github.com/Alexis-benoist/eralchemy

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install eralchemy with the following command
   ```
   brew install eralchemy
   ```
4. eralchemy is ready to use now!