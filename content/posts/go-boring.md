---
title: "Install go-boring on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Go programming language with BoringCrypto"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install go-boring on MacOS using homebrew

- App Name: go-boring
- App description: Go programming language with BoringCrypto
- App Version: 1.17.7b7
- App Website: https://go.googlesource.com/go/+/dev.boringcrypto/README.boringcrypto.md

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install go-boring with the following command
   ```
   brew install go-boring
   ```
4. go-boring is ready to use now!