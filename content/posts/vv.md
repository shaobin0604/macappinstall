---
title: "Install VV on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Neovim client"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install VV on MacOS using homebrew

- App Name: VV
- App description: Neovim client
- App Version: 2.4.10
- App Website: https://github.com/vv-vim/vv

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install VV with the following command
   ```
   brew install --cask vv
   ```
4. VV is ready to use now!