---
title: "Install cyral-gimme-db-token on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Eases using Cyral for SSO login to databases"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cyral-gimme-db-token on MacOS using homebrew

- App Name: cyral-gimme-db-token
- App description: Eases using Cyral for SSO login to databases
- App Version: 0.7.1
- App Website: https://cyral.com/docs/connect/repo-connect/#cli-token-retriever-for-sso

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cyral-gimme-db-token with the following command
   ```
   brew install cyral-gimme-db-token
   ```
4. cyral-gimme-db-token is ready to use now!