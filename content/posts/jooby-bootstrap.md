---
title: "Install jooby-bootstrap on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Script to simplify the creation of jooby apps"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install jooby-bootstrap on MacOS using homebrew

- App Name: jooby-bootstrap
- App description: Script to simplify the creation of jooby apps
- App Version: 0.2.2
- App Website: https://github.com/jooby-project/jooby-bootstrap

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install jooby-bootstrap with the following command
   ```
   brew install jooby-bootstrap
   ```
4. jooby-bootstrap is ready to use now!