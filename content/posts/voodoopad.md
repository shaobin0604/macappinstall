---
title: "Install VoodooPad on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Notes organizer"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install VoodooPad on MacOS using homebrew

- App Name: VoodooPad
- App description: Notes organizer
- App Version: 5.4.0,6608
- App Website: https://www.voodoopad.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install VoodooPad with the following command
   ```
   brew install --cask voodoopad
   ```
4. VoodooPad is ready to use now!