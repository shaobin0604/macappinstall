---
title: "Install NetBeans IDE on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Development environment, tooling platform and application framework"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install NetBeans IDE on MacOS using homebrew

- App Name: NetBeans IDE
- App description: Development environment, tooling platform and application framework
- App Version: 12.6
- App Website: https://netbeans.apache.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install NetBeans IDE with the following command
   ```
   brew install --cask netbeans
   ```
4. NetBeans IDE is ready to use now!