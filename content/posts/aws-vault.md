---
title: "Install aws-vault on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Securely store and access AWS credentials in development environments"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install aws-vault on MacOS using homebrew

- App Name: aws-vault
- App description: Securely store and access AWS credentials in development environments
- App Version: 6.5.0
- App Website: https://github.com/99designs/aws-vault

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install aws-vault with the following command
   ```
   brew install aws-vault
   ```
4. aws-vault is ready to use now!