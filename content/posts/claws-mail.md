---
title: "Install claws-mail on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "User-friendly, lightweight, and fast email client"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install claws-mail on MacOS using homebrew

- App Name: claws-mail
- App description: User-friendly, lightweight, and fast email client
- App Version: 3.18.0
- App Website: https://www.claws-mail.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install claws-mail with the following command
   ```
   brew install claws-mail
   ```
4. claws-mail is ready to use now!