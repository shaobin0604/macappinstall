---
title: "Install Tempo on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Email client that delivers all email in batches"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Tempo on MacOS using homebrew

- App Name: Tempo
- App description: Email client that delivers all email in batches
- App Version: 6.0.0
- App Website: https://www.yourtempo.co/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Tempo with the following command
   ```
   brew install --cask tempo
   ```
4. Tempo is ready to use now!