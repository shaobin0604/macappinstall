---
title: "Install arcade-learning-environment on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Platform for AI research"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install arcade-learning-environment on MacOS using homebrew

- App Name: arcade-learning-environment
- App description: Platform for AI research
- App Version: 0.6.1
- App Website: https://github.com/mgbellemare/Arcade-Learning-Environment

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install arcade-learning-environment with the following command
   ```
   brew install arcade-learning-environment
   ```
4. arcade-learning-environment is ready to use now!