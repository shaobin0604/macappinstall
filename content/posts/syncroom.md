---
title: "Install SYNCROOM on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Online remote concert service: enjoy playing even if far away from each other"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install SYNCROOM on MacOS using homebrew

- App Name: SYNCROOM
- App description: Online remote concert service: enjoy playing even if far away from each other
- App Version: 1.1.0
- App Website: https://syncroom.yamaha.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install SYNCROOM with the following command
   ```
   brew install --cask syncroom
   ```
4. SYNCROOM is ready to use now!