---
title: "Install Transmission Remote GUI on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Transmission Remote GUI on MacOS using homebrew

- App Name: Transmission Remote GUI
- App description: null
- App Version: 5.18.0
- App Website: https://github.com/transmission-remote-gui/transgui

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Transmission Remote GUI with the following command
   ```
   brew install --cask transmission-remote-gui
   ```
4. Transmission Remote GUI is ready to use now!