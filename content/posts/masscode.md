---
title: "Install massCode on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Open-source code snippets manager for developers"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install massCode on MacOS using homebrew

- App Name: massCode
- App description: Open-source code snippets manager for developers
- App Version: 1.3.0
- App Website: https://masscode.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install massCode with the following command
   ```
   brew install --cask masscode
   ```
4. massCode is ready to use now!