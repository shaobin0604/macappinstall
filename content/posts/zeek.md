---
title: "Install zeek on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Network security monitor"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install zeek on MacOS using homebrew

- App Name: zeek
- App description: Network security monitor
- App Version: 4.2.0
- App Website: https://www.zeek.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install zeek with the following command
   ```
   brew install zeek
   ```
4. zeek is ready to use now!