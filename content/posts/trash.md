---
title: "Install trash on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CLI tool that moves files or folder to the trash"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install trash on MacOS using homebrew

- App Name: trash
- App description: CLI tool that moves files or folder to the trash
- App Version: 0.9.2
- App Website: https://hasseg.org/trash/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install trash with the following command
   ```
   brew install trash
   ```
4. trash is ready to use now!