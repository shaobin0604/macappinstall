---
title: "Install Fiji on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Open-source image processing package"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Fiji on MacOS using homebrew

- App Name: Fiji
- App description: Open-source image processing package
- App Version: 1.0
- App Website: https://fiji.sc/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Fiji with the following command
   ```
   brew install --cask fiji
   ```
4. Fiji is ready to use now!