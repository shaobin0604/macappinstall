---
title: "Install lynis on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Security and system auditing tool to harden systems"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install lynis on MacOS using homebrew

- App Name: lynis
- App description: Security and system auditing tool to harden systems
- App Version: 3.0.7
- App Website: https://cisofy.com/lynis/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install lynis with the following command
   ```
   brew install lynis
   ```
4. lynis is ready to use now!