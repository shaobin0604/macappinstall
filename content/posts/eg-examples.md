---
title: "Install eg-examples on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Useful examples at the command-line"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install eg-examples on MacOS using homebrew

- App Name: eg-examples
- App description: Useful examples at the command-line
- App Version: 1.2.1
- App Website: https://github.com/srsudar/eg

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install eg-examples with the following command
   ```
   brew install eg-examples
   ```
4. eg-examples is ready to use now!