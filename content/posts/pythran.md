---
title: "Install pythran on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Ahead of Time compiler for numeric kernels"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pythran on MacOS using homebrew

- App Name: pythran
- App description: Ahead of Time compiler for numeric kernels
- App Version: 0.11.0
- App Website: https://pythran.readthedocs.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pythran with the following command
   ```
   brew install pythran
   ```
4. pythran is ready to use now!