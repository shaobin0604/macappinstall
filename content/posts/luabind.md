---
title: "Install luabind on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library for bindings between C++ and Lua"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install luabind on MacOS using homebrew

- App Name: luabind
- App description: Library for bindings between C++ and Lua
- App Version: 0.9.1
- App Website: https://github.com/luabind/luabind

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install luabind with the following command
   ```
   brew install luabind
   ```
4. luabind is ready to use now!