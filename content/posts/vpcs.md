---
title: "Install vpcs on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Virtual PC simulator for testing IP routing"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install vpcs on MacOS using homebrew

- App Name: vpcs
- App description: Virtual PC simulator for testing IP routing
- App Version: 0.8
- App Website: https://vpcs.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install vpcs with the following command
   ```
   brew install vpcs
   ```
4. vpcs is ready to use now!