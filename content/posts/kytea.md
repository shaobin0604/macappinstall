---
title: "Install kytea on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Toolkit for analyzing text, especially Japanese and Chinese"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install kytea on MacOS using homebrew

- App Name: kytea
- App description: Toolkit for analyzing text, especially Japanese and Chinese
- App Version: 0.4.7
- App Website: http://www.phontron.com/kytea/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install kytea with the following command
   ```
   brew install kytea
   ```
4. kytea is ready to use now!