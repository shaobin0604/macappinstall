---
title: "Install htmlcompressor on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Minify HTML or XML"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install htmlcompressor on MacOS using homebrew

- App Name: htmlcompressor
- App description: Minify HTML or XML
- App Version: 1.5.3
- App Website: https://code.google.com/archive/p/htmlcompressor/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install htmlcompressor with the following command
   ```
   brew install htmlcompressor
   ```
4. htmlcompressor is ready to use now!