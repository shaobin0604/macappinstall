---
title: "Install FontBase on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Font manager"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install FontBase on MacOS using homebrew

- App Name: FontBase
- App description: Font manager
- App Version: 2.16.9
- App Website: https://fontba.se/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install FontBase with the following command
   ```
   brew install --cask fontbase
   ```
4. FontBase is ready to use now!