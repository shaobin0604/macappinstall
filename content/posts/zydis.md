---
title: "Install zydis on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Fast and lightweight x86/x86_64 disassembler library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install zydis on MacOS using homebrew

- App Name: zydis
- App description: Fast and lightweight x86/x86_64 disassembler library
- App Version: 3.2.1
- App Website: https://zydis.re

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install zydis with the following command
   ```
   brew install zydis
   ```
4. zydis is ready to use now!