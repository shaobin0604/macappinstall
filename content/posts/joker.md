---
title: "Install Joker iOS kernelcache handling utility on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Joker iOS kernelcache handling utility on MacOS using homebrew

- App Name: Joker iOS kernelcache handling utility
- App description: null
- App Version: latest
- App Website: http://newosxbook.com/tools/joker.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Joker iOS kernelcache handling utility with the following command
   ```
   brew install --cask joker
   ```
4. Joker iOS kernelcache handling utility is ready to use now!