---
title: "Install 8BitDo Ultimate Software on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Control every piece of your controller"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install 8BitDo Ultimate Software on MacOS using homebrew

- App Name: 8BitDo Ultimate Software
- App description: Control every piece of your controller
- App Version: 2.0.6
- App Website: https://support.8bitdo.com/ultimate-software.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install 8BitDo Ultimate Software with the following command
   ```
   brew install --cask 8bitdo-ultimate-software
   ```
4. 8BitDo Ultimate Software is ready to use now!