---
title: "Install rbenv-vars on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Safely sets global and per-project environment variables"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install rbenv-vars on MacOS using homebrew

- App Name: rbenv-vars
- App description: Safely sets global and per-project environment variables
- App Version: 1.2.0
- App Website: https://github.com/sstephenson/rbenv-vars

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install rbenv-vars with the following command
   ```
   brew install rbenv-vars
   ```
4. rbenv-vars is ready to use now!