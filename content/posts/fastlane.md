---
title: "Install fastlane on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Easiest way to build and release mobile apps"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fastlane on MacOS using homebrew

- App Name: fastlane
- App description: Easiest way to build and release mobile apps
- App Version: 2.204.3
- App Website: https://fastlane.tools

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fastlane with the following command
   ```
   brew install fastlane
   ```
4. fastlane is ready to use now!