---
title: "Install Box Drive on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Client for the Box cloud storage service"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Box Drive on MacOS using homebrew

- App Name: Box Drive
- App description: Client for the Box cloud storage service
- App Version: 2.19.294
- App Website: https://www.box.com/drive

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Box Drive with the following command
   ```
   brew install --cask box-drive
   ```
4. Box Drive is ready to use now!