---
title: "Install flowgrind on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "TCP measurement tool, similar to iperf or netperf"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install flowgrind on MacOS using homebrew

- App Name: flowgrind
- App description: TCP measurement tool, similar to iperf or netperf
- App Version: 0.8.0
- App Website: https://launchpad.net/flowgrind

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install flowgrind with the following command
   ```
   brew install flowgrind
   ```
4. flowgrind is ready to use now!