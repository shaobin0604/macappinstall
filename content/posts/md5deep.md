---
title: "Install md5deep on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Recursively compute digests on files/directories"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install md5deep on MacOS using homebrew

- App Name: md5deep
- App description: Recursively compute digests on files/directories
- App Version: 4.4
- App Website: https://github.com/jessek/hashdeep

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install md5deep with the following command
   ```
   brew install md5deep
   ```
4. md5deep is ready to use now!