---
title: "Install Exodus on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Desktop wallet for cryptocurrency assets"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Exodus on MacOS using homebrew

- App Name: Exodus
- App description: Desktop wallet for cryptocurrency assets
- App Version: 22.2.10
- App Website: https://www.exodus.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Exodus with the following command
   ```
   brew install --cask exodus
   ```
4. Exodus is ready to use now!