---
title: "Install Code42 CrashPlan on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Endpoint backup and recovery"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Code42 CrashPlan on MacOS using homebrew

- App Name: Code42 CrashPlan
- App description: Endpoint backup and recovery
- App Version: 8.2.3,1525200006823,22
- App Website: https://www.crashplan.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Code42 CrashPlan with the following command
   ```
   brew install --cask code42-crashplan
   ```
4. Code42 CrashPlan is ready to use now!