---
title: "Install k9s on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Kubernetes CLI To Manage Your Clusters In Style!"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install k9s on MacOS using homebrew

- App Name: k9s
- App description: Kubernetes CLI To Manage Your Clusters In Style!
- App Version: 0.25.18
- App Website: https://k9scli.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install k9s with the following command
   ```
   brew install k9s
   ```
4. k9s is ready to use now!