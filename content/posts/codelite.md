---
title: "Install CodeLite on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "IDE for C, C++, PHP and Node.js"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install CodeLite on MacOS using homebrew

- App Name: CodeLite
- App description: IDE for C, C++, PHP and Node.js
- App Version: 15.0.0
- App Website: https://codelite.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install CodeLite with the following command
   ```
   brew install --cask codelite
   ```
4. CodeLite is ready to use now!