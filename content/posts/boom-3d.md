---
title: "Install Boom 3D on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Volume booster and equalizer software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Boom 3D on MacOS using homebrew

- App Name: Boom 3D
- App description: Volume booster and equalizer software
- App Version: 1.3.13,101.3.13065
- App Website: https://www.globaldelight.com/boom/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Boom 3D with the following command
   ```
   brew install --cask boom-3d
   ```
4. Boom 3D is ready to use now!