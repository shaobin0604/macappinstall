---
title: "Install hstr on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Bash and zsh history suggest box"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install hstr on MacOS using homebrew

- App Name: hstr
- App description: Bash and zsh history suggest box
- App Version: 2.5
- App Website: https://github.com/dvorka/hstr

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install hstr with the following command
   ```
   brew install hstr
   ```
4. hstr is ready to use now!