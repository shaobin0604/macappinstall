---
title: "Install dockview on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utility to preview application windows in the dock"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dockview on MacOS using homebrew

- App Name: dockview
- App description: Utility to preview application windows in the dock
- App Version: 1.03,103
- App Website: https://noteifyapp.com/dockview/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dockview with the following command
   ```
   brew install --cask dockview
   ```
4. dockview is ready to use now!