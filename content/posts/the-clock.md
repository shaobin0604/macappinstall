---
title: "Install The Clock on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Clock and time zone app"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install The Clock on MacOS using homebrew

- App Name: The Clock
- App description: Clock and time zone app
- App Version: 4.6.3,20211217
- App Website: https://seense.com/the_clock/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install The Clock with the following command
   ```
   brew install --cask the-clock
   ```
4. The Clock is ready to use now!