---
title: "Install micropython on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Python implementation for microcontrollers and constrained systems"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install micropython on MacOS using homebrew

- App Name: micropython
- App description: Python implementation for microcontrollers and constrained systems
- App Version: 1.18
- App Website: https://www.micropython.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install micropython with the following command
   ```
   brew install micropython
   ```
4. micropython is ready to use now!