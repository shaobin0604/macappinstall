---
title: "Install fabric-installer on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Installer for Fabric for the vanilla launcher"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fabric-installer on MacOS using homebrew

- App Name: fabric-installer
- App description: Installer for Fabric for the vanilla launcher
- App Version: 0.10.2
- App Website: https://fabricmc.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fabric-installer with the following command
   ```
   brew install fabric-installer
   ```
4. fabric-installer is ready to use now!