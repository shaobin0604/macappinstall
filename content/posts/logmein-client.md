---
title: "Install LogMeIn Client on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Remote access tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install LogMeIn Client on MacOS using homebrew

- App Name: LogMeIn Client
- App description: Remote access tool
- App Version: 4.1.7756,4.1.0.7756
- App Website: https://www.logmein.com/pro

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install LogMeIn Client with the following command
   ```
   brew install --cask logmein-client
   ```
4. LogMeIn Client is ready to use now!