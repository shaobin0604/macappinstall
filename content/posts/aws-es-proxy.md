---
title: "Install aws-es-proxy on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Small proxy between HTTP client and AWS Elasticsearch"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install aws-es-proxy on MacOS using homebrew

- App Name: aws-es-proxy
- App description: Small proxy between HTTP client and AWS Elasticsearch
- App Version: 1.3
- App Website: https://github.com/abutaha/aws-es-proxy

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install aws-es-proxy with the following command
   ```
   brew install aws-es-proxy
   ```
4. aws-es-proxy is ready to use now!