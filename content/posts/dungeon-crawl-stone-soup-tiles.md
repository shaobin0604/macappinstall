---
title: "Install Dungeon Crawl Stone Soup on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Game of dungeon exploration, combat and magic"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Dungeon Crawl Stone Soup on MacOS using homebrew

- App Name: Dungeon Crawl Stone Soup
- App description: Game of dungeon exploration, combat and magic
- App Version: 0.28.0
- App Website: https://crawl.develz.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Dungeon Crawl Stone Soup with the following command
   ```
   brew install --cask dungeon-crawl-stone-soup-tiles
   ```
4. Dungeon Crawl Stone Soup is ready to use now!