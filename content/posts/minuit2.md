---
title: "Install minuit2 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Physics analysis tool for function minimization"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install minuit2 on MacOS using homebrew

- App Name: minuit2
- App description: Physics analysis tool for function minimization
- App Version: 5.34.14
- App Website: https://seal.web.cern.ch/seal/snapshot/work-packages/mathlibs/minuit/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install minuit2 with the following command
   ```
   brew install minuit2
   ```
4. minuit2 is ready to use now!