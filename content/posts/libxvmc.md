---
title: "Install libxvmc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "X.Org: X-Video Motion Compensation API"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libxvmc on MacOS using homebrew

- App Name: libxvmc
- App description: X.Org: X-Video Motion Compensation API
- App Version: 1.0.12
- App Website: https://www.x.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libxvmc with the following command
   ```
   brew install libxvmc
   ```
4. libxvmc is ready to use now!