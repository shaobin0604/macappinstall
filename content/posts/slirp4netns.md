---
title: "Install slirp4netns on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "User-mode networking for unprivileged network namespaces"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install slirp4netns on MacOS using homebrew

- App Name: slirp4netns
- App description: User-mode networking for unprivileged network namespaces
- App Version: 1.1.12
- App Website: https://github.com/rootless-containers/slirp4netns

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install slirp4netns with the following command
   ```
   brew install slirp4netns
   ```
4. slirp4netns is ready to use now!