---
title: "Install Process Explorer on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Process Explorer on MacOS using homebrew

- App Name: Process Explorer
- App description: null
- App Version: latest
- App Website: http://www.newosxbook.com/tools/procexp.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Process Explorer with the following command
   ```
   brew install --cask procexp
   ```
4. Process Explorer is ready to use now!