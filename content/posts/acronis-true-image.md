---
title: "Install Acronis True Image on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Full image backup and cloning software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Acronis True Image on MacOS using homebrew

- App Name: Acronis True Image
- App description: Full image backup and cloning software
- App Version: 2021
- App Website: https://www.acronis.com/personal/computer-backup/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Acronis True Image with the following command
   ```
   brew install --cask acronis-true-image
   ```
4. Acronis True Image is ready to use now!