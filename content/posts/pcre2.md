---
title: "Install pcre2 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Perl compatible regular expressions library with a new API"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pcre2 on MacOS using homebrew

- App Name: pcre2
- App description: Perl compatible regular expressions library with a new API
- App Version: 10.39
- App Website: https://www.pcre.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pcre2 with the following command
   ```
   brew install pcre2
   ```
4. pcre2 is ready to use now!