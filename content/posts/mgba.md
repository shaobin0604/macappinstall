---
title: "Install mgba on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Game Boy Advance emulator"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mgba on MacOS using homebrew

- App Name: mgba
- App description: Game Boy Advance emulator
- App Version: 0.9.3
- App Website: https://mgba.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mgba with the following command
   ```
   brew install mgba
   ```
4. mgba is ready to use now!