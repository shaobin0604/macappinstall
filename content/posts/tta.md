---
title: "Install tta on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Lossless audio codec"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tta on MacOS using homebrew

- App Name: tta
- App description: Lossless audio codec
- App Version: 2.2
- App Website: https://web.archive.org/web/20100131140204/true-audio.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tta with the following command
   ```
   brew install tta
   ```
4. tta is ready to use now!