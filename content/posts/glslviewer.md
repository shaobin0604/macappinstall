---
title: "Install glslviewer on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Live-coding console tool that renders GLSL Shaders"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install glslviewer on MacOS using homebrew

- App Name: glslviewer
- App description: Live-coding console tool that renders GLSL Shaders
- App Version: 1.7.0
- App Website: http://patriciogonzalezvivo.com/2015/glslViewer/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install glslviewer with the following command
   ```
   brew install glslviewer
   ```
4. glslviewer is ready to use now!