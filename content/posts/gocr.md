---
title: "Install gocr on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Optical Character Recognition (OCR), converts images back to text"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gocr on MacOS using homebrew

- App Name: gocr
- App description: Optical Character Recognition (OCR), converts images back to text
- App Version: 0.52
- App Website: https://wasd.urz.uni-magdeburg.de/jschulen/ocr/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gocr with the following command
   ```
   brew install gocr
   ```
4. gocr is ready to use now!