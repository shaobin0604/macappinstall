---
title: "Install cadical on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Clean and efficient state-of-the-art SAT solver"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cadical on MacOS using homebrew

- App Name: cadical
- App description: Clean and efficient state-of-the-art SAT solver
- App Version: 1.5.2
- App Website: http://fmv.jku.at/cadical/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cadical with the following command
   ```
   brew install cadical
   ```
4. cadical is ready to use now!