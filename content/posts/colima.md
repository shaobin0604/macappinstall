---
title: "Install colima on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Container runtimes on MacOS with minimal setup"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install colima on MacOS using homebrew

- App Name: colima
- App description: Container runtimes on MacOS with minimal setup
- App Version: 0.3.3
- App Website: https://github.com/abiosoft/colima/blob/main/README.md

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install colima with the following command
   ```
   brew install colima
   ```
4. colima is ready to use now!