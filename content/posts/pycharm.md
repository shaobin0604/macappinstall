---
title: "Install PyCharm on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "IDE for professional Python development"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install PyCharm on MacOS using homebrew

- App Name: PyCharm
- App description: IDE for professional Python development
- App Version: 2021.3.2,213.6777.50
- App Website: https://www.jetbrains.com/pycharm/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install PyCharm with the following command
   ```
   brew install --cask pycharm
   ```
4. PyCharm is ready to use now!