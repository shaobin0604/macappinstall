---
title: "Install nave on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Virtual environments for Node.js"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install nave on MacOS using homebrew

- App Name: nave
- App description: Virtual environments for Node.js
- App Version: 3.2.3
- App Website: https://github.com/isaacs/nave

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install nave with the following command
   ```
   brew install nave
   ```
4. nave is ready to use now!