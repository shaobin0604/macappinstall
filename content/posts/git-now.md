---
title: "Install git-now on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Light, temporary commits for git"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install git-now on MacOS using homebrew

- App Name: git-now
- App description: Light, temporary commits for git
- App Version: 0.1.1.0
- App Website: https://github.com/iwata/git-now

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install git-now with the following command
   ```
   brew install git-now
   ```
4. git-now is ready to use now!