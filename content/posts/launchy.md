---
title: "Install Launchy on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utility desktop shortcut utility"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Launchy on MacOS using homebrew

- App Name: Launchy
- App description: Utility desktop shortcut utility
- App Version: 2.5
- App Website: https://www.launchy.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Launchy with the following command
   ```
   brew install --cask launchy
   ```
4. Launchy is ready to use now!