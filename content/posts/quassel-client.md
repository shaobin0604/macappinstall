---
title: "Install Quassel IRC on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Quassel IRC: Chat comfortably.  Everywhere"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Quassel IRC on MacOS using homebrew

- App Name: Quassel IRC
- App description: Quassel IRC: Chat comfortably.  Everywhere
- App Version: 0.14.0
- App Website: https://quassel-irc.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Quassel IRC with the following command
   ```
   brew install --cask quassel-client
   ```
4. Quassel IRC is ready to use now!