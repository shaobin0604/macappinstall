---
title: "Install tagref on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Refer to other locations in your codebase"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tagref on MacOS using homebrew

- App Name: tagref
- App description: Refer to other locations in your codebase
- App Version: 1.5.0
- App Website: https://github.com/stepchowfun/tagref

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tagref with the following command
   ```
   brew install tagref
   ```
4. tagref is ready to use now!