---
title: "Install Kotlin Native on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "LLVM backend for Kotlin"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Kotlin Native on MacOS using homebrew

- App Name: Kotlin Native
- App description: LLVM backend for Kotlin
- App Version: 1.6.10
- App Website: https://kotlinlang.org/docs/reference/native-overview.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Kotlin Native with the following command
   ```
   brew install --cask kotlin-native
   ```
4. Kotlin Native is ready to use now!