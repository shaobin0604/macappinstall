---
title: "Install mariadb-connector-c on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "MariaDB database connector for C applications"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mariadb-connector-c on MacOS using homebrew

- App Name: mariadb-connector-c
- App description: MariaDB database connector for C applications
- App Version: 3.2.5
- App Website: https://mariadb.org/download/?tab=connector&prod=connector-c

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mariadb-connector-c with the following command
   ```
   brew install mariadb-connector-c
   ```
4. mariadb-connector-c is ready to use now!