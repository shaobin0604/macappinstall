---
title: "Install chrpath on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to edit the rpath in ELF binaries"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install chrpath on MacOS using homebrew

- App Name: chrpath
- App description: Tool to edit the rpath in ELF binaries
- App Version: 0.16
- App Website: https://tracker.debian.org/pkg/chrpath

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install chrpath with the following command
   ```
   brew install chrpath
   ```
4. chrpath is ready to use now!