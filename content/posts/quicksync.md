---
title: "Install QuickSync on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Syncing software for Gigaset products"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install QuickSync on MacOS using homebrew

- App Name: QuickSync
- App description: Syncing software for Gigaset products
- App Version: 4.3.3
- App Website: https://www.gigaset.com/en_en/cms/start/service/support-detail/gigaset-quicksync-mac-version.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install QuickSync with the following command
   ```
   brew install --cask quicksync
   ```
4. QuickSync is ready to use now!