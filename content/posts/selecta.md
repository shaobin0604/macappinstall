---
title: "Install selecta on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Fuzzy text selector for files and anything else you need to select"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install selecta on MacOS using homebrew

- App Name: selecta
- App description: Fuzzy text selector for files and anything else you need to select
- App Version: 0.0.7
- App Website: https://github.com/garybernhardt/selecta

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install selecta with the following command
   ```
   brew install selecta
   ```
4. selecta is ready to use now!