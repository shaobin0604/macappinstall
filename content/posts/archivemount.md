---
title: "Install archivemount on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "File system for accessing archives using libarchive"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install archivemount on MacOS using homebrew

- App Name: archivemount
- App description: File system for accessing archives using libarchive
- App Version: 0.9.1
- App Website: https://www.cybernoia.de/software/archivemount.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install archivemount with the following command
   ```
   brew install archivemount
   ```
4. archivemount is ready to use now!