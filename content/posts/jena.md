---
title: "Install jena on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Framework for building semantic web and linked data apps"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install jena on MacOS using homebrew

- App Name: jena
- App description: Framework for building semantic web and linked data apps
- App Version: 4.4.0
- App Website: https://jena.apache.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install jena with the following command
   ```
   brew install jena
   ```
4. jena is ready to use now!