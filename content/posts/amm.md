---
title: "Install AMM on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Aria2 Menubar Monitor"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install AMM on MacOS using homebrew

- App Name: AMM
- App description: Aria2 Menubar Monitor
- App Version: 0.4.5
- App Website: https://github.com/15cm/AMM

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install AMM with the following command
   ```
   brew install --cask amm
   ```
4. AMM is ready to use now!