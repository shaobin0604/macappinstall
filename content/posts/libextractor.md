---
title: "Install libextractor on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library to extract meta data from files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libextractor on MacOS using homebrew

- App Name: libextractor
- App description: Library to extract meta data from files
- App Version: 1.11
- App Website: https://www.gnu.org/software/libextractor/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libextractor with the following command
   ```
   brew install libextractor
   ```
4. libextractor is ready to use now!