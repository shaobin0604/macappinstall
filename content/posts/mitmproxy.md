---
title: "Install mitmproxy on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Intercept, modify, replay, save HTTP/S traffic"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mitmproxy on MacOS using homebrew

- App Name: mitmproxy
- App description: Intercept, modify, replay, save HTTP/S traffic
- App Version: 7.0.4
- App Website: https://mitmproxy.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mitmproxy with the following command
   ```
   brew install mitmproxy
   ```
4. mitmproxy is ready to use now!