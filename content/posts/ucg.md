---
title: "Install ucg on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool for searching large bodies of source code (like grep)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ucg on MacOS using homebrew

- App Name: ucg
- App description: Tool for searching large bodies of source code (like grep)
- App Version: 0.3.3
- App Website: https://github.com/gvansickle/ucg

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ucg with the following command
   ```
   brew install ucg
   ```
4. ucg is ready to use now!