---
title: "Install caddy on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Powerful, enterprise-ready, open source web server with automatic HTTPS"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install caddy on MacOS using homebrew

- App Name: caddy
- App description: Powerful, enterprise-ready, open source web server with automatic HTTPS
- App Version: 2.4.6
- App Website: https://caddyserver.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install caddy with the following command
   ```
   brew install caddy
   ```
4. caddy is ready to use now!