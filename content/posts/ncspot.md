---
title: "Install ncspot on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cross-platform ncurses Spotify client written in Rust"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ncspot on MacOS using homebrew

- App Name: ncspot
- App description: Cross-platform ncurses Spotify client written in Rust
- App Version: 0.9.5
- App Website: https://github.com/hrkfdn/ncspot

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ncspot with the following command
   ```
   brew install ncspot
   ```
4. ncspot is ready to use now!