---
title: "Install CrystalMaker on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Visualize crystal and molecular structures"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install CrystalMaker on MacOS using homebrew

- App Name: CrystalMaker
- App description: Visualize crystal and molecular structures
- App Version: 10.7.1
- App Website: http://crystalmaker.com/index.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install CrystalMaker with the following command
   ```
   brew install --cask crystalmaker
   ```
4. CrystalMaker is ready to use now!