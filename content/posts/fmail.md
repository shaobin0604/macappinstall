---
title: "Install FMail on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Unofficial native application for Fastmail"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install FMail on MacOS using homebrew

- App Name: FMail
- App description: Unofficial native application for Fastmail
- App Version: 2.6.1,108
- App Website: https://arievanboxel.fr/fmail/en/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install FMail with the following command
   ```
   brew install --cask fmail
   ```
4. FMail is ready to use now!