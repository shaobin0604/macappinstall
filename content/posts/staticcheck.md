---
title: "Install staticcheck on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "State of the art linter for the Go programming language"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install staticcheck on MacOS using homebrew

- App Name: staticcheck
- App description: State of the art linter for the Go programming language
- App Version: 2021.1.2
- App Website: https://staticcheck.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install staticcheck with the following command
   ```
   brew install staticcheck
   ```
4. staticcheck is ready to use now!