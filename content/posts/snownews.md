---
title: "Install snownews on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Text mode RSS newsreader"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install snownews on MacOS using homebrew

- App Name: snownews
- App description: Text mode RSS newsreader
- App Version: 1.9
- App Website: https://github.com/msharov/snownews

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install snownews with the following command
   ```
   brew install snownews
   ```
4. snownews is ready to use now!