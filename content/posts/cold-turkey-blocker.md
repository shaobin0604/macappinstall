---
title: "Install Cold Turkey on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Block websites, games and applications"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Cold Turkey on MacOS using homebrew

- App Name: Cold Turkey
- App description: Block websites, games and applications
- App Version: 4.3
- App Website: https://getcoldturkey.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Cold Turkey with the following command
   ```
   brew install --cask cold-turkey-blocker
   ```
4. Cold Turkey is ready to use now!