---
title: "Install blink1 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Control blink(1) indicator light"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install blink1 on MacOS using homebrew

- App Name: blink1
- App description: Control blink(1) indicator light
- App Version: 2.3.0
- App Website: https://blink1.thingm.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install blink1 with the following command
   ```
   brew install blink1
   ```
4. blink1 is ready to use now!