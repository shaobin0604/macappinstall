---
title: "Install treecc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Aspect-oriented approach to writing compilers"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install treecc on MacOS using homebrew

- App Name: treecc
- App description: Aspect-oriented approach to writing compilers
- App Version: 0.3.10
- App Website: https://gnu.org/software/dotgnu/treecc/treecc.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install treecc with the following command
   ```
   brew install treecc
   ```
4. treecc is ready to use now!