---
title: "Install VLC Streamer on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Stream videos to mobile devices using VLC"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install VLC Streamer on MacOS using homebrew

- App Name: VLC Streamer
- App description: Stream videos to mobile devices using VLC
- App Version: 5.25
- App Website: https://hobbyistsoftware.com/vlcstreamer

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install VLC Streamer with the following command
   ```
   brew install --cask vlcstreamer
   ```
4. VLC Streamer is ready to use now!