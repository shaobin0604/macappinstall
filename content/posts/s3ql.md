---
title: "Install s3ql on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "POSIX-compliant FUSE filesystem using object store as block storage"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install s3ql on MacOS using homebrew

- App Name: s3ql
- App description: POSIX-compliant FUSE filesystem using object store as block storage
- App Version: 3.3.2
- App Website: https://github.com/s3ql/s3ql

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install s3ql with the following command
   ```
   brew install s3ql
   ```
4. s3ql is ready to use now!