---
title: "Install cucumber-cpp on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Support for writing Cucumber step definitions in C++"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cucumber-cpp on MacOS using homebrew

- App Name: cucumber-cpp
- App description: Support for writing Cucumber step definitions in C++
- App Version: 0.5
- App Website: https://cucumber.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cucumber-cpp with the following command
   ```
   brew install cucumber-cpp
   ```
4. cucumber-cpp is ready to use now!