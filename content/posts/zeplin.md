---
title: "Install Zeplin on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Share, organize and collaborate on designs"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Zeplin on MacOS using homebrew

- App Name: Zeplin
- App description: Share, organize and collaborate on designs
- App Version: 4.0.1,1444
- App Website: https://zeplin.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Zeplin with the following command
   ```
   brew install --cask zeplin
   ```
4. Zeplin is ready to use now!