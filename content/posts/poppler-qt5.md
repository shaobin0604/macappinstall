---
title: "Install poppler-qt5 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "PDF rendering library (based on the xpdf-3.0 code base)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install poppler-qt5 on MacOS using homebrew

- App Name: poppler-qt5
- App description: PDF rendering library (based on the xpdf-3.0 code base)
- App Version: 21.12.0
- App Website: https://poppler.freedesktop.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install poppler-qt5 with the following command
   ```
   brew install poppler-qt5
   ```
4. poppler-qt5 is ready to use now!