---
title: "Install iTunes Volume Control on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Control the volume of Apple Music and Spotify using keyboard volume keys"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install iTunes Volume Control on MacOS using homebrew

- App Name: iTunes Volume Control
- App description: Control the volume of Apple Music and Spotify using keyboard volume keys
- App Version: 1.7.3
- App Website: https://github.com/alberti42/Volume-Control

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install iTunes Volume Control with the following command
   ```
   brew install --cask itunes-volume-control
   ```
4. iTunes Volume Control is ready to use now!