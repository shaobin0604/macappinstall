---
title: "Install datree on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CLI tool to run policies against Kubernetes manifests YAML files or Helm charts"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install datree on MacOS using homebrew

- App Name: datree
- App description: CLI tool to run policies against Kubernetes manifests YAML files or Helm charts
- App Version: 0.15.16
- App Website: https://www.datree.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install datree with the following command
   ```
   brew install datree
   ```
4. datree is ready to use now!