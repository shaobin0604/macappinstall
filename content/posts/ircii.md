---
title: "Install ircii on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "IRC and ICB client"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ircii on MacOS using homebrew

- App Name: ircii
- App description: IRC and ICB client
- App Version: 20210314
- App Website: http://www.eterna.com.au/ircii/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ircii with the following command
   ```
   brew install ircii
   ```
4. ircii is ready to use now!