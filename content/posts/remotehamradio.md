---
title: "Install RemoteHamRadio on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Desktop console app for RemoteHamRadio service"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install RemoteHamRadio on MacOS using homebrew

- App Name: RemoteHamRadio
- App description: Desktop console app for RemoteHamRadio service
- App Version: 2.2.1
- App Website: https://www.remotehamradio.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install RemoteHamRadio with the following command
   ```
   brew install --cask remotehamradio
   ```
4. RemoteHamRadio is ready to use now!