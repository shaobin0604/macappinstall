---
title: "Install genact on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Nonsense activity generator"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install genact on MacOS using homebrew

- App Name: genact
- App description: Nonsense activity generator
- App Version: 0.11.0
- App Website: https://github.com/svenstaro/genact

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install genact with the following command
   ```
   brew install genact
   ```
4. genact is ready to use now!