---
title: "Install deepstream on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Data-sync realtime server"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install deepstream on MacOS using homebrew

- App Name: deepstream
- App description: Data-sync realtime server
- App Version: 6.0.1
- App Website: https://deepstream.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install deepstream with the following command
   ```
   brew install --cask deepstream
   ```
4. deepstream is ready to use now!