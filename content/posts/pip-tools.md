---
title: "Install pip-tools on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Locking and sync for Pip requirements files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pip-tools on MacOS using homebrew

- App Name: pip-tools
- App description: Locking and sync for Pip requirements files
- App Version: 6.5.1
- App Website: https://pip-tools.readthedocs.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pip-tools with the following command
   ```
   brew install pip-tools
   ```
4. pip-tools is ready to use now!