---
title: "Install Dust3D on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Dust3D on MacOS using homebrew

- App Name: Dust3D
- App description: null
- App Version: 1.0.0-rc.6
- App Website: https://dust3d.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Dust3D with the following command
   ```
   brew install --cask dust3d
   ```
4. Dust3D is ready to use now!