---
title: "Install lolcat on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Rainbows and unicorns in your console!"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install lolcat on MacOS using homebrew

- App Name: lolcat
- App description: Rainbows and unicorns in your console!
- App Version: 100.0.1
- App Website: https://github.com/busyloop/lolcat

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install lolcat with the following command
   ```
   brew install lolcat
   ```
4. lolcat is ready to use now!