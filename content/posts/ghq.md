---
title: "Install ghq on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Remote repository management made easy"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ghq on MacOS using homebrew

- App Name: ghq
- App description: Remote repository management made easy
- App Version: 1.2.1
- App Website: https://github.com/x-motemen/ghq

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ghq with the following command
   ```
   brew install ghq
   ```
4. ghq is ready to use now!