---
title: "Install supermodel on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Sega Model 3 arcade emulator"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install supermodel on MacOS using homebrew

- App Name: supermodel
- App description: Sega Model 3 arcade emulator
- App Version: 0.2a
- App Website: https://www.supermodel3.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install supermodel with the following command
   ```
   brew install supermodel
   ```
4. supermodel is ready to use now!