---
title: "Install Azure Data Studio on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Data management tool that enables working with SQL Server"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Azure Data Studio on MacOS using homebrew

- App Name: Azure Data Studio
- App description: Data management tool that enables working with SQL Server
- App Version: 1.34.0
- App Website: https://docs.microsoft.com/en-us/sql/azure-data-studio/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Azure Data Studio with the following command
   ```
   brew install --cask azure-data-studio
   ```
4. Azure Data Studio is ready to use now!