---
title: "Install basis_universal on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Basis Universal GPU texture codec command-line compression tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install basis_universal on MacOS using homebrew

- App Name: basis_universal
- App description: Basis Universal GPU texture codec command-line compression tool
- App Version: 1.16.2
- App Website: https://github.com/BinomialLLC/basis_universal

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install basis_universal with the following command
   ```
   brew install basis_universal
   ```
4. basis_universal is ready to use now!