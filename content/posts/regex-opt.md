---
title: "Install regex-opt on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Perl-compatible regular expression optimizer"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install regex-opt on MacOS using homebrew

- App Name: regex-opt
- App description: Perl-compatible regular expression optimizer
- App Version: 1.2.4
- App Website: https://bisqwit.iki.fi/source/regexopt.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install regex-opt with the following command
   ```
   brew install regex-opt
   ```
4. regex-opt is ready to use now!