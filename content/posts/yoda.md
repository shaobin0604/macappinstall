---
title: "Install Yoda on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "App to browse and download YouTube videos"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Yoda on MacOS using homebrew

- App Name: Yoda
- App description: App to browse and download YouTube videos
- App Version: 1.0.1
- App Website: https://github.com/whoisandy/yoda

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Yoda with the following command
   ```
   brew install --cask yoda
   ```
4. Yoda is ready to use now!