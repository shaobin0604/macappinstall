---
title: "Install libcroco on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CSS parsing and manipulation toolkit for GNOME"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libcroco on MacOS using homebrew

- App Name: libcroco
- App description: CSS parsing and manipulation toolkit for GNOME
- App Version: 0.6.13
- App Website: https://gitlab.gnome.org/GNOME/libcroco

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libcroco with the following command
   ```
   brew install libcroco
   ```
4. libcroco is ready to use now!