---
title: "Install dtach on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Emulates the detach feature of screen"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dtach on MacOS using homebrew

- App Name: dtach
- App description: Emulates the detach feature of screen
- App Version: 0.9
- App Website: https://dtach.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dtach with the following command
   ```
   brew install dtach
   ```
4. dtach is ready to use now!