---
title: "Install Signal on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Instant messaging application focusing on security"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Signal on MacOS using homebrew

- App Name: Signal
- App description: Instant messaging application focusing on security
- App Version: 5.32.0
- App Website: https://signal.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Signal with the following command
   ```
   brew install --cask signal
   ```
4. Signal is ready to use now!