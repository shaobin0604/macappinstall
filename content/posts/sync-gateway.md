---
title: "Install sync_gateway on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Make Couchbase Server a replication endpoint for Couchbase Lite"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sync_gateway on MacOS using homebrew

- App Name: sync_gateway
- App description: Make Couchbase Server a replication endpoint for Couchbase Lite
- App Version: 2.8.3
- App Website: https://docs.couchbase.com/sync-gateway/current/index.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sync_gateway with the following command
   ```
   brew install sync_gateway
   ```
4. sync_gateway is ready to use now!