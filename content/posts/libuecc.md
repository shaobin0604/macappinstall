---
title: "Install libuecc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Very small Elliptic Curve Cryptography library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libuecc on MacOS using homebrew

- App Name: libuecc
- App description: Very small Elliptic Curve Cryptography library
- App Version: 7
- App Website: https://git.universe-factory.net/libuecc/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libuecc with the following command
   ```
   brew install libuecc
   ```
4. libuecc is ready to use now!