---
title: "Install virustotal-cli on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line interface for VirusTotal"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install virustotal-cli on MacOS using homebrew

- App Name: virustotal-cli
- App description: Command-line interface for VirusTotal
- App Version: 0.10.0
- App Website: https://github.com/VirusTotal/vt-cli

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install virustotal-cli with the following command
   ```
   brew install virustotal-cli
   ```
4. virustotal-cli is ready to use now!