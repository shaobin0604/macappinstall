---
title: "Install go-critic on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Opinionated Go source code linter"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install go-critic on MacOS using homebrew

- App Name: go-critic
- App description: Opinionated Go source code linter
- App Version: 0.6.2
- App Website: https://go-critic.com

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install go-critic with the following command
   ```
   brew install go-critic
   ```
4. go-critic is ready to use now!