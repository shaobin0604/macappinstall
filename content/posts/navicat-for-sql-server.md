---
title: "Install Navicat for SQL Server on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Database administration and development tool for SQL-server"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Navicat for SQL Server on MacOS using homebrew

- App Name: Navicat for SQL Server
- App description: Database administration and development tool for SQL-server
- App Version: 16.0.7
- App Website: https://www.navicat.com/products/navicat-for-sqlserver

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Navicat for SQL Server with the following command
   ```
   brew install --cask navicat-for-sql-server
   ```
4. Navicat for SQL Server is ready to use now!