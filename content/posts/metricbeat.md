---
title: "Install metricbeat on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Collect metrics from your systems and services"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install metricbeat on MacOS using homebrew

- App Name: metricbeat
- App description: Collect metrics from your systems and services
- App Version: 8.0.0
- App Website: https://www.elastic.co/beats/metricbeat

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install metricbeat with the following command
   ```
   brew install metricbeat
   ```
4. metricbeat is ready to use now!