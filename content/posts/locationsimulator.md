---
title: "Install LocationSimulator on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Application to spoof your iOS, iPadOS or iPhoneSimulator device location"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install LocationSimulator on MacOS using homebrew

- App Name: LocationSimulator
- App description: Application to spoof your iOS, iPadOS or iPhoneSimulator device location
- App Version: 0.1.8
- App Website: https://github.com/Schlaubischlump/LocationSimulator

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install LocationSimulator with the following command
   ```
   brew install --cask locationsimulator
   ```
4. LocationSimulator is ready to use now!