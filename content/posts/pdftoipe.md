---
title: "Install pdftoipe on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Reads arbitrary PDF files and generates an XML file readable by Ipe"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pdftoipe on MacOS using homebrew

- App Name: pdftoipe
- App description: Reads arbitrary PDF files and generates an XML file readable by Ipe
- App Version: 7.2.24.1
- App Website: https://github.com/otfried/ipe-tools

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pdftoipe with the following command
   ```
   brew install pdftoipe
   ```
4. pdftoipe is ready to use now!