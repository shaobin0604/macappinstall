---
title: "Install liblunar on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Lunar date calendar"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install liblunar on MacOS using homebrew

- App Name: liblunar
- App description: Lunar date calendar
- App Version: 2.2.5
- App Website: https://code.google.com/archive/p/liblunar/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install liblunar with the following command
   ```
   brew install liblunar
   ```
4. liblunar is ready to use now!