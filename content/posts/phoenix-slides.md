---
title: "Install Phoenix Slides on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Full-screen slideshow program"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Phoenix Slides on MacOS using homebrew

- App Name: Phoenix Slides
- App description: Full-screen slideshow program
- App Version: 1.4.7
- App Website: https://blyt.net/phxslides/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Phoenix Slides with the following command
   ```
   brew install --cask phoenix-slides
   ```
4. Phoenix Slides is ready to use now!