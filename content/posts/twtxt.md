---
title: "Install twtxt on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Decentralised, minimalist microblogging service for hackers"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install twtxt on MacOS using homebrew

- App Name: twtxt
- App description: Decentralised, minimalist microblogging service for hackers
- App Version: 1.2.3
- App Website: https://github.com/buckket/twtxt

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install twtxt with the following command
   ```
   brew install twtxt
   ```
4. twtxt is ready to use now!