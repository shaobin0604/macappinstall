---
title: "Install PacketProxy on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Local proxy written in Java"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install PacketProxy on MacOS using homebrew

- App Name: PacketProxy
- App description: Local proxy written in Java
- App Version: 2.1.8
- App Website: https://github.com/DeNA/PacketProxy

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install PacketProxy with the following command
   ```
   brew install --cask packetproxy
   ```
4. PacketProxy is ready to use now!