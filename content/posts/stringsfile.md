---
title: "Install StringsFile QuickLook plugin on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install StringsFile QuickLook plugin on MacOS using homebrew

- App Name: StringsFile QuickLook plugin
- App description: null
- App Version: 1.0
- App Website: https://blog.timac.org/?p=933

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install StringsFile QuickLook plugin with the following command
   ```
   brew install --cask stringsfile
   ```
4. StringsFile QuickLook plugin is ready to use now!