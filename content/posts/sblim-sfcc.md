---
title: "Install sblim-sfcc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Project to enhance the manageability of GNU/Linux system"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sblim-sfcc on MacOS using homebrew

- App Name: sblim-sfcc
- App description: Project to enhance the manageability of GNU/Linux system
- App Version: 2.2.8
- App Website: https://sourceforge.net/projects/sblim/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sblim-sfcc with the following command
   ```
   brew install sblim-sfcc
   ```
4. sblim-sfcc is ready to use now!