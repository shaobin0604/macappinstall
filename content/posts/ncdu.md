---
title: "Install ncdu on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "NCurses Disk Usage"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ncdu on MacOS using homebrew

- App Name: ncdu
- App description: NCurses Disk Usage
- App Version: 2.1
- App Website: https://dev.yorhel.nl/ncdu

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ncdu with the following command
   ```
   brew install ncdu
   ```
4. ncdu is ready to use now!