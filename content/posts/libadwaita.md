---
title: "Install libadwaita on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Building blocks for modern adaptive GNOME applications"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libadwaita on MacOS using homebrew

- App Name: libadwaita
- App description: Building blocks for modern adaptive GNOME applications
- App Version: 1.0.1
- App Website: https://gnome.pages.gitlab.gnome.org/libadwaita/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libadwaita with the following command
   ```
   brew install libadwaita
   ```
4. libadwaita is ready to use now!