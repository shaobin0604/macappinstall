---
title: "Install mk-configure on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Lightweight replacement for GNU autotools"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mk-configure on MacOS using homebrew

- App Name: mk-configure
- App description: Lightweight replacement for GNU autotools
- App Version: 0.37.0
- App Website: https://github.com/cheusov/mk-configure

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mk-configure with the following command
   ```
   brew install mk-configure
   ```
4. mk-configure is ready to use now!