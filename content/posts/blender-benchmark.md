---
title: "Install Blender Open Data Benchmark on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "3D performance benchmarking tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Blender Open Data Benchmark on MacOS using homebrew

- App Name: Blender Open Data Benchmark
- App description: 3D performance benchmarking tool
- App Version: 2.0.4
- App Website: https://opendata.blender.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Blender Open Data Benchmark with the following command
   ```
   brew install --cask blender-benchmark
   ```
4. Blender Open Data Benchmark is ready to use now!