---
title: "Install snapraid on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Backup program for disk arrays"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install snapraid on MacOS using homebrew

- App Name: snapraid
- App description: Backup program for disk arrays
- App Version: 12.1
- App Website: https://www.snapraid.it/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install snapraid with the following command
   ```
   brew install snapraid
   ```
4. snapraid is ready to use now!