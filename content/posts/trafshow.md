---
title: "Install trafshow on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Continuous network traffic display"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install trafshow on MacOS using homebrew

- App Name: trafshow
- App description: Continuous network traffic display
- App Version: 5.2.3
- App Website: https://web.archive.org/web/20130707021442/soft.risp.ru/trafshow/index_en.shtml

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install trafshow with the following command
   ```
   brew install trafshow
   ```
4. trafshow is ready to use now!