---
title: "Install libosmium on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Fast and flexible C++ library for working with OpenStreetMap data"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libosmium on MacOS using homebrew

- App Name: libosmium
- App description: Fast and flexible C++ library for working with OpenStreetMap data
- App Version: 2.17.3
- App Website: https://osmcode.org/libosmium/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libosmium with the following command
   ```
   brew install libosmium
   ```
4. libosmium is ready to use now!