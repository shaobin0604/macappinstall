---
title: "Install mikutter on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Extensible Twitter client"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mikutter on MacOS using homebrew

- App Name: mikutter
- App description: Extensible Twitter client
- App Version: 4.1.1
- App Website: https://mikutter.hachune.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mikutter with the following command
   ```
   brew install mikutter
   ```
4. mikutter is ready to use now!