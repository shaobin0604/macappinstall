---
title: "Install Facebook Flipper on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Desktop debugging platform for mobile developers"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Facebook Flipper on MacOS using homebrew

- App Name: Facebook Flipper
- App description: Desktop debugging platform for mobile developers
- App Version: 0.135.0
- App Website: https://fbflipper.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Facebook Flipper with the following command
   ```
   brew install --cask flipper
   ```
4. Facebook Flipper is ready to use now!