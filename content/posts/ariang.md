---
title: "Install AriaNg Native on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Better aria2 desktop frontend than AriaNg"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install AriaNg Native on MacOS using homebrew

- App Name: AriaNg Native
- App description: Better aria2 desktop frontend than AriaNg
- App Version: 1.2.3
- App Website: https://github.com/mayswind/AriaNg-Native

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install AriaNg Native with the following command
   ```
   brew install --cask ariang
   ```
4. AriaNg Native is ready to use now!