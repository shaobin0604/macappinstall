---
title: "Install Vysor on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Mirror and control your phone"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Vysor on MacOS using homebrew

- App Name: Vysor
- App description: Mirror and control your phone
- App Version: 4.1.77
- App Website: https://www.vysor.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Vysor with the following command
   ```
   brew install --cask vysor
   ```
4. Vysor is ready to use now!