---
title: "Install wxMacMolPlt on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cross-platform GUI input generator for GAMESS"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install wxMacMolPlt on MacOS using homebrew

- App Name: wxMacMolPlt
- App description: Cross-platform GUI input generator for GAMESS
- App Version: 7.7,ijfreodzydtqn214qui3snyvj5vdovqr
- App Website: https://brettbode.github.io/wxmacmolplt

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install wxMacMolPlt with the following command
   ```
   brew install --cask wxmacmolplt
   ```
4. wxMacMolPlt is ready to use now!