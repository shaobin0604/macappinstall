---
title: "Install apparix on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "File system navigation via bookmarking directories"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install apparix on MacOS using homebrew

- App Name: apparix
- App description: File system navigation via bookmarking directories
- App Version: 11-062
- App Website: https://micans.org/apparix/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install apparix with the following command
   ```
   brew install apparix
   ```
4. apparix is ready to use now!