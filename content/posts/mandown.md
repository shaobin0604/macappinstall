---
title: "Install mandown on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Man-page inspired Markdown viewer"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mandown on MacOS using homebrew

- App Name: mandown
- App description: Man-page inspired Markdown viewer
- App Version: 1.0.1
- App Website: https://github.com/Titor8115/mandown

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mandown with the following command
   ```
   brew install mandown
   ```
4. mandown is ready to use now!