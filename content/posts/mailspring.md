---
title: "Install Mailspring on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Fork of Nylas Mail"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Mailspring on MacOS using homebrew

- App Name: Mailspring
- App description: Fork of Nylas Mail
- App Version: 1.9.2
- App Website: https://getmailspring.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Mailspring with the following command
   ```
   brew install --cask mailspring
   ```
4. Mailspring is ready to use now!