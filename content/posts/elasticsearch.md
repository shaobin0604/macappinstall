---
title: "Install elasticsearch on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Distributed search & analytics engine"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install elasticsearch on MacOS using homebrew

- App Name: elasticsearch
- App description: Distributed search & analytics engine
- App Version: 7.10.2
- App Website: https://www.elastic.co/products/elasticsearch

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install elasticsearch with the following command
   ```
   brew install elasticsearch
   ```
4. elasticsearch is ready to use now!