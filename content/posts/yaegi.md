---
title: "Install yaegi on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Yet another elegant Go interpreter"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install yaegi on MacOS using homebrew

- App Name: yaegi
- App description: Yet another elegant Go interpreter
- App Version: 0.11.2
- App Website: https://github.com/containous/yaegi

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install yaegi with the following command
   ```
   brew install yaegi
   ```
4. yaegi is ready to use now!