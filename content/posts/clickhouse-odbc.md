---
title: "Install clickhouse-odbc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Official ODBC driver implementation for accessing ClickHouse as a data source"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install clickhouse-odbc on MacOS using homebrew

- App Name: clickhouse-odbc
- App description: Official ODBC driver implementation for accessing ClickHouse as a data source
- App Version: 1.1.10.20210822
- App Website: https://github.com/ClickHouse/clickhouse-odbc#readme

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install clickhouse-odbc with the following command
   ```
   brew install clickhouse-odbc
   ```
4. clickhouse-odbc is ready to use now!