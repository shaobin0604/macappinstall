---
title: "Install ZTerm on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Terminal emulation program"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ZTerm on MacOS using homebrew

- App Name: ZTerm
- App description: Terminal emulation program
- App Version: 1.2
- App Website: https://www.dalverson.com/zterm/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ZTerm with the following command
   ```
   brew install --cask zterm
   ```
4. ZTerm is ready to use now!