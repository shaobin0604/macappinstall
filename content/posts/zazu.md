---
title: "Install Zazu on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Extensible and open-source launcher for hackers, creators and dabblers"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Zazu on MacOS using homebrew

- App Name: Zazu
- App description: Extensible and open-source launcher for hackers, creators and dabblers
- App Version: 0.6.0
- App Website: https://zazuapp.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Zazu with the following command
   ```
   brew install --cask zazu
   ```
4. Zazu is ready to use now!