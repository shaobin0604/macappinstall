---
title: "Install Tip on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Programmable tooltip that can be used with any app"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Tip on MacOS using homebrew

- App Name: Tip
- App description: Programmable tooltip that can be used with any app
- App Version: 2.0.0
- App Website: https://github.com/tanin47/tip

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Tip with the following command
   ```
   brew install --cask tip
   ```
4. Tip is ready to use now!