---
title: "Install kanif on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cluster management and administration tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install kanif on MacOS using homebrew

- App Name: kanif
- App description: Cluster management and administration tool
- App Version: 1.2.2
- App Website: https://web.archive.org/web/20200523045112/https://taktuk.gforge.inria.fr/kanif/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install kanif with the following command
   ```
   brew install kanif
   ```
4. kanif is ready to use now!