---
title: "Install SyncSettings on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Sync and save settings of your apps"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install SyncSettings on MacOS using homebrew

- App Name: SyncSettings
- App description: Sync and save settings of your apps
- App Version: 1.2.4,6008
- App Website: https://neededapps.com/syncsettings/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install SyncSettings with the following command
   ```
   brew install --cask syncsettings
   ```
4. SyncSettings is ready to use now!