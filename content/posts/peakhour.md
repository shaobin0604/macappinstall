---
title: "Install PeakHour on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Network bandwidth and network quality visualiser"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install PeakHour on MacOS using homebrew

- App Name: PeakHour
- App description: Network bandwidth and network quality visualiser
- App Version: 4.1.14,35461
- App Website: https://www.peakhourapp.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install PeakHour with the following command
   ```
   brew install --cask peakhour
   ```
4. PeakHour is ready to use now!