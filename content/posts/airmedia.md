---
title: "Install Crestron AirMedia on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Touchless presentation and collaboration software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Crestron AirMedia on MacOS using homebrew

- App Name: Crestron AirMedia
- App description: Touchless presentation and collaboration software
- App Version: 4.1.4
- App Website: https://www.crestron.com/microsites/airmedia-mobile-wireless-hd-presentations

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Crestron AirMedia with the following command
   ```
   brew install --cask airmedia
   ```
4. Crestron AirMedia is ready to use now!