---
title: "Install torchvision on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Datasets, transforms, and models for computer vision"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install torchvision on MacOS using homebrew

- App Name: torchvision
- App description: Datasets, transforms, and models for computer vision
- App Version: 0.11.3
- App Website: https://github.com/pytorch/vision

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install torchvision with the following command
   ```
   brew install torchvision
   ```
4. torchvision is ready to use now!