---
title: "Install cvsutils on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CVS utilities for use in working directories"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cvsutils on MacOS using homebrew

- App Name: cvsutils
- App description: CVS utilities for use in working directories
- App Version: 0.2.6
- App Website: https://www.red-bean.com/cvsutils/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cvsutils with the following command
   ```
   brew install cvsutils
   ```
4. cvsutils is ready to use now!