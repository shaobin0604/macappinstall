---
title: "Install rabbitmq-c on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C AMQP client library for RabbitMQ"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install rabbitmq-c on MacOS using homebrew

- App Name: rabbitmq-c
- App description: C AMQP client library for RabbitMQ
- App Version: 0.11.0
- App Website: https://github.com/alanxz/rabbitmq-c

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install rabbitmq-c with the following command
   ```
   brew install rabbitmq-c
   ```
4. rabbitmq-c is ready to use now!