---
title: "Install Clip Studio Paint on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Software for drawing and painting"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Clip Studio Paint on MacOS using homebrew

- App Name: Clip Studio Paint
- App description: Software for drawing and painting
- App Version: 1.11.8
- App Website: https://www.clipstudio.net/en

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Clip Studio Paint with the following command
   ```
   brew install --cask clip-studio-paint
   ```
4. Clip Studio Paint is ready to use now!