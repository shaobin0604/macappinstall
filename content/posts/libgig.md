---
title: "Install libgig on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library for Gigasampler and DLS (Downloadable Sounds) Level 1/2 files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libgig on MacOS using homebrew

- App Name: libgig
- App description: Library for Gigasampler and DLS (Downloadable Sounds) Level 1/2 files
- App Version: 4.3.0
- App Website: https://www.linuxsampler.org/libgig/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libgig with the following command
   ```
   brew install libgig
   ```
4. libgig is ready to use now!