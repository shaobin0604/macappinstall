---
title: "Install tokyo-dystopia on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Lightweight full-text search system"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tokyo-dystopia on MacOS using homebrew

- App Name: tokyo-dystopia
- App description: Lightweight full-text search system
- App Version: 0.9.15
- App Website: https://dbmx.net/tokyodystopia/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tokyo-dystopia with the following command
   ```
   brew install tokyo-dystopia
   ```
4. tokyo-dystopia is ready to use now!