---
title: "Install consul on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool for service discovery, monitoring and configuration"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install consul on MacOS using homebrew

- App Name: consul
- App description: Tool for service discovery, monitoring and configuration
- App Version: 1.11.3
- App Website: https://www.consul.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install consul with the following command
   ```
   brew install consul
   ```
4. consul is ready to use now!