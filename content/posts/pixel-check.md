---
title: "Install PiXel Check on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install PiXel Check on MacOS using homebrew

- App Name: PiXel Check
- App description: null
- App Version: 1.3
- App Website: http://macguitar.me/apps/pixelcheck/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install PiXel Check with the following command
   ```
   brew install --cask pixel-check
   ```
4. PiXel Check is ready to use now!