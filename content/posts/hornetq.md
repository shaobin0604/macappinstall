---
title: "Install hornetq on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Multi-protocol, embeddable, clustered, asynchronous messaging system"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install hornetq on MacOS using homebrew

- App Name: hornetq
- App description: Multi-protocol, embeddable, clustered, asynchronous messaging system
- App Version: 2.4.0
- App Website: https://hornetq.jboss.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install hornetq with the following command
   ```
   brew install hornetq
   ```
4. hornetq is ready to use now!