---
title: "Install Youku on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Chinese video streaming and sharing platform"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Youku on MacOS using homebrew

- App Name: Youku
- App description: Chinese video streaming and sharing platform
- App Version: 1.9.5.6.11184
- App Website: https://pd.youku.com/pc

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Youku with the following command
   ```
   brew install --cask youku
   ```
4. Youku is ready to use now!