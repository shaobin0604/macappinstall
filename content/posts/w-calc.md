---
title: "Install w-calc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Very capable calculator"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install w-calc on MacOS using homebrew

- App Name: w-calc
- App description: Very capable calculator
- App Version: 2.5
- App Website: https://w-calc.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install w-calc with the following command
   ```
   brew install w-calc
   ```
4. w-calc is ready to use now!