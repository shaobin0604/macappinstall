---
title: "Install Wire on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Collaboration platform focusing on security"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Wire on MacOS using homebrew

- App Name: Wire
- App description: Collaboration platform focusing on security
- App Version: 3.26.4145
- App Website: https://wire.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Wire with the following command
   ```
   brew install --cask wire
   ```
4. Wire is ready to use now!