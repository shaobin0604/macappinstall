---
title: "Install bsponmpi on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Implements the BSPlib standard on top of MPI"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bsponmpi on MacOS using homebrew

- App Name: bsponmpi
- App description: Implements the BSPlib standard on top of MPI
- App Version: 0.3
- App Website: https://sourceforge.net/projects/bsponmpi/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bsponmpi with the following command
   ```
   brew install bsponmpi
   ```
4. bsponmpi is ready to use now!