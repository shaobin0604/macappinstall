---
title: "Install orbit on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CORBA 2.4-compliant object request broker (ORB)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install orbit on MacOS using homebrew

- App Name: orbit
- App description: CORBA 2.4-compliant object request broker (ORB)
- App Version: 2.14.19
- App Website: https://web.archive.org/web/20191222075841/projects-old.gnome.org/ORBit2/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install orbit with the following command
   ```
   brew install orbit
   ```
4. orbit is ready to use now!