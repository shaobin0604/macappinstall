---
title: "Install iproute2 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Linux routing utilities"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install iproute2 on MacOS using homebrew

- App Name: iproute2
- App description: Linux routing utilities
- App Version: 5.16.0
- App Website: https://wiki.linuxfoundation.org/networking/iproute2

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install iproute2 with the following command
   ```
   brew install iproute2
   ```
4. iproute2 is ready to use now!