---
title: "Install MacCPUID on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Displays information via the CPUID instruction"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install MacCPUID on MacOS using homebrew

- App Name: MacCPUID
- App description: Displays information via the CPUID instruction
- App Version: 3.1
- App Website: https://software.intel.com/content/www/us/en/develop/download/download-maccpuid

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install MacCPUID with the following command
   ```
   brew install --cask maccpuid
   ```
4. MacCPUID is ready to use now!