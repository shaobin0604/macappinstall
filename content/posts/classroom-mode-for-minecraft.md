---
title: "Install Classroom Mode for Minecraft on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Classroom management app for Minecraft Education Edition"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Classroom Mode for Minecraft on MacOS using homebrew

- App Name: Classroom Mode for Minecraft
- App description: Classroom management app for Minecraft Education Edition
- App Version: 1.81.v2
- App Website: https://education.minecraft.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Classroom Mode for Minecraft with the following command
   ```
   brew install --cask classroom-mode-for-minecraft
   ```
4. Classroom Mode for Minecraft is ready to use now!