---
title: "Install csmith on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Generates random C programs conforming to the C99 standard"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install csmith on MacOS using homebrew

- App Name: csmith
- App description: Generates random C programs conforming to the C99 standard
- App Version: 2.3.0
- App Website: https://embed.cs.utah.edu/csmith/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install csmith with the following command
   ```
   brew install csmith
   ```
4. csmith is ready to use now!