---
title: "Install gom on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GObject wrapper around SQLite"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gom on MacOS using homebrew

- App Name: gom
- App description: GObject wrapper around SQLite
- App Version: 0.4
- App Website: https://wiki.gnome.org/Projects/Gom

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gom with the following command
   ```
   brew install gom
   ```
4. gom is ready to use now!