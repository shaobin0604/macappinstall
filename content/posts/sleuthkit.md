---
title: "Install sleuthkit on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Forensic toolkit"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sleuthkit on MacOS using homebrew

- App Name: sleuthkit
- App description: Forensic toolkit
- App Version: 4.11.1
- App Website: https://www.sleuthkit.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sleuthkit with the following command
   ```
   brew install sleuthkit
   ```
4. sleuthkit is ready to use now!