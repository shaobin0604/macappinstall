---
title: "Install FastRawViewer on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Opens RAW files and renders them on-the-fly"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install FastRawViewer on MacOS using homebrew

- App Name: FastRawViewer
- App description: Opens RAW files and renders them on-the-fly
- App Version: 2.0.3.1902
- App Website: https://www.fastrawviewer.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install FastRawViewer with the following command
   ```
   brew install --cask fastrawviewer
   ```
4. FastRawViewer is ready to use now!