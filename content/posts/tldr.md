---
title: "Install tldr on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Simplified and community-driven man pages"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tldr on MacOS using homebrew

- App Name: tldr
- App description: Simplified and community-driven man pages
- App Version: 1.4.2
- App Website: https://tldr.sh/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tldr with the following command
   ```
   brew install tldr
   ```
4. tldr is ready to use now!