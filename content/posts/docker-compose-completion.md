---
title: "Install docker-compose-completion on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Completion script for docker-compose"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install docker-compose-completion on MacOS using homebrew

- App Name: docker-compose-completion
- App description: Completion script for docker-compose
- App Version: 1.29.2
- App Website: https://docs.docker.com/compose/completion/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install docker-compose-completion with the following command
   ```
   brew install docker-compose-completion
   ```
4. docker-compose-completion is ready to use now!