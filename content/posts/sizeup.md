---
title: "Install SizeUp on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utility to resize and position application windows"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install SizeUp on MacOS using homebrew

- App Name: SizeUp
- App description: Utility to resize and position application windows
- App Version: 1.7.4,1360
- App Website: https://www.irradiatedsoftware.com/sizeup/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install SizeUp with the following command
   ```
   brew install --cask sizeup
   ```
4. SizeUp is ready to use now!