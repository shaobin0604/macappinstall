---
title: "Install Postman on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Collaboration platform for API development"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Postman on MacOS using homebrew

- App Name: Postman
- App description: Collaboration platform for API development
- App Version: 9.13.0
- App Website: https://www.postman.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Postman with the following command
   ```
   brew install --cask postman
   ```
4. Postman is ready to use now!