---
title: "Install Flutter SDK on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "UI toolkit for building applications for mobile, web and desktop"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Flutter SDK on MacOS using homebrew

- App Name: Flutter SDK
- App description: UI toolkit for building applications for mobile, web and desktop
- App Version: 2.10.1
- App Website: https://flutter.dev/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Flutter SDK with the following command
   ```
   brew install --cask flutter
   ```
4. Flutter SDK is ready to use now!