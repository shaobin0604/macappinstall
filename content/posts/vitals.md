---
title: "Install Vitals on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tiny process monitor"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Vitals on MacOS using homebrew

- App Name: Vitals
- App description: Tiny process monitor
- App Version: 0.7
- App Website: https://github.com/hmarr/vitals/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Vitals with the following command
   ```
   brew install --cask vitals
   ```
4. Vitals is ready to use now!