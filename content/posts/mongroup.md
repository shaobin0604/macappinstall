---
title: "Install mongroup on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Monitor a group of processes with mon"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mongroup on MacOS using homebrew

- App Name: mongroup
- App description: Monitor a group of processes with mon
- App Version: 0.4.1
- App Website: https://github.com/jgallen23/mongroup

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mongroup with the following command
   ```
   brew install mongroup
   ```
4. mongroup is ready to use now!