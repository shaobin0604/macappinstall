---
title: "Install Natron on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Open-source node-graph based video compositing software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Natron on MacOS using homebrew

- App Name: Natron
- App description: Open-source node-graph based video compositing software
- App Version: 2.4.2
- App Website: https://NatronGitHub.github.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Natron with the following command
   ```
   brew install --cask natron
   ```
4. Natron is ready to use now!