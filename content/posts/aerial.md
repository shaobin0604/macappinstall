---
title: "Install Aerial Screensaver on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Apple TV Aerial screensaver"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Aerial Screensaver on MacOS using homebrew

- App Name: Aerial Screensaver
- App description: Apple TV Aerial screensaver
- App Version: 3.0.6
- App Website: https://github.com/JohnCoates/Aerial

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Aerial Screensaver with the following command
   ```
   brew install --cask aerial
   ```
4. Aerial Screensaver is ready to use now!