---
title: "Install hexedit on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "View and edit files in hexadecimal or ASCII"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install hexedit on MacOS using homebrew

- App Name: hexedit
- App description: View and edit files in hexadecimal or ASCII
- App Version: 1.5
- App Website: http://rigaux.org/hexedit.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install hexedit with the following command
   ```
   brew install hexedit
   ```
4. hexedit is ready to use now!