---
title: "Install ansible@2.9 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Automate deployment, configuration, and upgrading"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ansible@2.9 on MacOS using homebrew

- App Name: ansible@2.9
- App description: Automate deployment, configuration, and upgrading
- App Version: 2.9.27
- App Website: https://www.ansible.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ansible@2.9 with the following command
   ```
   brew install ansible@2.9
   ```
4. ansible@2.9 is ready to use now!