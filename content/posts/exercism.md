---
title: "Install exercism on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line tool to interact with exercism.io"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install exercism on MacOS using homebrew

- App Name: exercism
- App description: Command-line tool to interact with exercism.io
- App Version: 3.0.13
- App Website: https://exercism.io/cli/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install exercism with the following command
   ```
   brew install exercism
   ```
4. exercism is ready to use now!