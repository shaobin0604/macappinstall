---
title: "Install Lofi on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Mini Spotify player with WebGL visualizations"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Lofi on MacOS using homebrew

- App Name: Lofi
- App description: Mini Spotify player with WebGL visualizations
- App Version: 1.6.0
- App Website: https://www.lofi.rocks/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Lofi with the following command
   ```
   brew install --cask lofi
   ```
4. Lofi is ready to use now!