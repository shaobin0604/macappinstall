---
title: "Install metabase on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Business intelligence report server"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install metabase on MacOS using homebrew

- App Name: metabase
- App description: Business intelligence report server
- App Version: 0.42.1
- App Website: https://www.metabase.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install metabase with the following command
   ```
   brew install metabase
   ```
4. metabase is ready to use now!