---
title: "Install libusb-compat on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library for USB device access"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libusb-compat on MacOS using homebrew

- App Name: libusb-compat
- App description: Library for USB device access
- App Version: 0.1.5
- App Website: https://libusb.info/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libusb-compat with the following command
   ```
   brew install libusb-compat
   ```
4. libusb-compat is ready to use now!