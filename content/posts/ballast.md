---
title: "Install ballast on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Status Bar app to keep the audio balance from drifting"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ballast on MacOS using homebrew

- App Name: ballast
- App description: Status Bar app to keep the audio balance from drifting
- App Version: 1.2.1
- App Website: https://jamsinclair.nz/ballast

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ballast with the following command
   ```
   brew install --cask ballast
   ```
4. ballast is ready to use now!