---
title: "Install rust-analyzer on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Experimental Rust compiler front-end for IDEs"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install rust-analyzer on MacOS using homebrew

- App Name: rust-analyzer
- App description: Experimental Rust compiler front-end for IDEs
- App Version: 2022-02-14
- App Website: https://rust-analyzer.github.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install rust-analyzer with the following command
   ```
   brew install rust-analyzer
   ```
4. rust-analyzer is ready to use now!