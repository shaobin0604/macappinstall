---
title: "Install tcpsplit on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Break a packet trace into some number of sub-traces"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tcpsplit on MacOS using homebrew

- App Name: tcpsplit
- App description: Break a packet trace into some number of sub-traces
- App Version: 0.2
- App Website: https://www.icir.org/mallman/software/tcpsplit/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tcpsplit with the following command
   ```
   brew install tcpsplit
   ```
4. tcpsplit is ready to use now!