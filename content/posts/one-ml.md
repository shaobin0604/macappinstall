---
title: "Install one-ml on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Reboot of ML, unifying its core and (now first-class) module layers"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install one-ml on MacOS using homebrew

- App Name: one-ml
- App description: Reboot of ML, unifying its core and (now first-class) module layers
- App Version: 0.1
- App Website: https://people.mpi-sws.org/~rossberg/1ml/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install one-ml with the following command
   ```
   brew install one-ml
   ```
4. one-ml is ready to use now!