---
title: "Install tgui on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GUI library for use with sfml"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tgui on MacOS using homebrew

- App Name: tgui
- App description: GUI library for use with sfml
- App Version: 0.9.3
- App Website: https://tgui.eu

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tgui with the following command
   ```
   brew install tgui
   ```
4. tgui is ready to use now!