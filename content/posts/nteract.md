---
title: "Install nteract on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Interactive computing suite"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install nteract on MacOS using homebrew

- App Name: nteract
- App description: Interactive computing suite
- App Version: 0.28.0
- App Website: https://github.com/nteract/nteract

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install nteract with the following command
   ```
   brew install --cask nteract
   ```
4. nteract is ready to use now!