---
title: "Install tomcat@7 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Implementation of Java Servlet and JavaServer Pages"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tomcat@7 on MacOS using homebrew

- App Name: tomcat@7
- App description: Implementation of Java Servlet and JavaServer Pages
- App Version: 7.0.109
- App Website: https://tomcat.apache.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tomcat@7 with the following command
   ```
   brew install tomcat@7
   ```
4. tomcat@7 is ready to use now!