---
title: "Install GrowlNotify on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Notification system"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install GrowlNotify on MacOS using homebrew

- App Name: GrowlNotify
- App description: Notification system
- App Version: 2.1
- App Website: https://growl.github.io/growl/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install GrowlNotify with the following command
   ```
   brew install --cask growlnotify
   ```
4. GrowlNotify is ready to use now!