---
title: "Install cloud-nuke on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CLI tool to nuke (delete) cloud resources"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cloud-nuke on MacOS using homebrew

- App Name: cloud-nuke
- App description: CLI tool to nuke (delete) cloud resources
- App Version: 0.10.0
- App Website: https://gruntwork.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cloud-nuke with the following command
   ```
   brew install cloud-nuke
   ```
4. cloud-nuke is ready to use now!