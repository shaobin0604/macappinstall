---
title: "Install iTerm2 on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Terminal emulator as alternative to Apple's Terminal app"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install iTerm2 on MacOS using homebrew

- App Name: iTerm2
- App description: Terminal emulator as alternative to Apple's Terminal app
- App Version: 3.4.15
- App Website: https://www.iterm2.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install iTerm2 with the following command
   ```
   brew install --cask iterm2
   ```
4. iTerm2 is ready to use now!