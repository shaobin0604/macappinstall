---
title: "Install TeamSpeak Client on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Voice communication client"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install TeamSpeak Client on MacOS using homebrew

- App Name: TeamSpeak Client
- App description: Voice communication client
- App Version: 3.5.7-beta.1
- App Website: https://www.teamspeak.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install TeamSpeak Client with the following command
   ```
   brew install --cask teamspeak-client
   ```
4. TeamSpeak Client is ready to use now!