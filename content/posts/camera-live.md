---
title: "Install Camera Live on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Live Syphon Camera"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Camera Live on MacOS using homebrew

- App Name: Camera Live
- App description: Live Syphon Camera
- App Version: 11
- App Website: https://github.com/v002/v002-Camera-Live

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Camera Live with the following command
   ```
   brew install --cask camera-live
   ```
4. Camera Live is ready to use now!