---
title: "Install UI Browser on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Assistant for Apple's Accessibility and AppleScript GUI scripting"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install UI Browser on MacOS using homebrew

- App Name: UI Browser
- App description: Assistant for Apple's Accessibility and AppleScript GUI scripting
- App Version: 3.0.2
- App Website: https://pfiddlesoft.com/uibrowser/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install UI Browser with the following command
   ```
   brew install --cask ui-browser
   ```
4. UI Browser is ready to use now!