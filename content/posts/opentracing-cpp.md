---
title: "Install opentracing-cpp on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "OpenTracing API for C++"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install opentracing-cpp on MacOS using homebrew

- App Name: opentracing-cpp
- App description: OpenTracing API for C++
- App Version: 1.6.0
- App Website: https://opentracing.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install opentracing-cpp with the following command
   ```
   brew install opentracing-cpp
   ```
4. opentracing-cpp is ready to use now!