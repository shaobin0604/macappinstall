---
title: "Install defaultbrowser on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line tool for getting & setting the default browser"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install defaultbrowser on MacOS using homebrew

- App Name: defaultbrowser
- App description: Command-line tool for getting & setting the default browser
- App Version: 1.1
- App Website: https://github.com/kerma/defaultbrowser

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install defaultbrowser with the following command
   ```
   brew install defaultbrowser
   ```
4. defaultbrowser is ready to use now!