---
title: "Install Particle Dev on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "IDE for programming Particle devices"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Particle Dev on MacOS using homebrew

- App Name: Particle Dev
- App description: IDE for programming Particle devices
- App Version: 1.19.0
- App Website: https://www.particle.io/products/development-tools/particle-desktop-ide

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Particle Dev with the following command
   ```
   brew install --cask particle-dev
   ```
4. Particle Dev is ready to use now!