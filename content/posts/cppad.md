---
title: "Install cppad on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Differentiation of C++ Algorithms"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cppad on MacOS using homebrew

- App Name: cppad
- App description: Differentiation of C++ Algorithms
- App Version: 20220000.2
- App Website: https://www.coin-or.org/CppAD

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cppad with the following command
   ```
   brew install cppad
   ```
4. cppad is ready to use now!