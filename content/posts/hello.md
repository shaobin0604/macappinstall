---
title: "Install hello on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Program providing model for GNU coding standards and practices"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install hello on MacOS using homebrew

- App Name: hello
- App description: Program providing model for GNU coding standards and practices
- App Version: 2.12
- App Website: https://www.gnu.org/software/hello/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install hello with the following command
   ```
   brew install hello
   ```
4. hello is ready to use now!