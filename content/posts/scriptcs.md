---
title: "Install scriptcs on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tools to write and execute C#"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install scriptcs on MacOS using homebrew

- App Name: scriptcs
- App description: Tools to write and execute C#
- App Version: 0.17.1
- App Website: https://github.com/scriptcs/scriptcs

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install scriptcs with the following command
   ```
   brew install scriptcs
   ```
4. scriptcs is ready to use now!