---
title: "Install Ripcord on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Desktop chat client for Slack (and Discord)"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Ripcord on MacOS using homebrew

- App Name: Ripcord
- App description: Desktop chat client for Slack (and Discord)
- App Version: 0.4.29
- App Website: https://cancel.fm/ripcord/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Ripcord with the following command
   ```
   brew install --cask ripcord
   ```
4. Ripcord is ready to use now!