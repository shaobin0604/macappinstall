---
title: "Install Lidarr on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Looks and smells like Sonarr but made for music"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Lidarr on MacOS using homebrew

- App Name: Lidarr
- App description: Looks and smells like Sonarr but made for music
- App Version: 0.8.1.2135
- App Website: https://lidarr.audio/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Lidarr with the following command
   ```
   brew install --cask lidarr
   ```
4. Lidarr is ready to use now!