---
title: "Install Deskreen on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Turns any device with a web browser into a secondary screen"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Deskreen on MacOS using homebrew

- App Name: Deskreen
- App description: Turns any device with a web browser into a secondary screen
- App Version: 1.0.11
- App Website: https://deskreen.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Deskreen with the following command
   ```
   brew install --cask deskreen
   ```
4. Deskreen is ready to use now!