---
title: "Install GoPro Quik on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Access and edit GoPro photos and videos"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install GoPro Quik on MacOS using homebrew

- App Name: GoPro Quik
- App description: Access and edit GoPro photos and videos
- App Version: 2.7.0.874
- App Website: https://community.gopro.com/t5/en/GoPro-Quik-for-desktop/ta-p/394305?profile.language=en

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install GoPro Quik with the following command
   ```
   brew install --cask quik
   ```
4. GoPro Quik is ready to use now!