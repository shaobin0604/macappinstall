---
title: "Install gst-rtsp-server on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "RTSP server library based on GStreamer"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gst-rtsp-server on MacOS using homebrew

- App Name: gst-rtsp-server
- App description: RTSP server library based on GStreamer
- App Version: 1.18.5
- App Website: https://gstreamer.freedesktop.org/modules/gst-rtsp-server.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gst-rtsp-server with the following command
   ```
   brew install gst-rtsp-server
   ```
4. gst-rtsp-server is ready to use now!