---
title: "Install ReactStudio on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ReactStudio on MacOS using homebrew

- App Name: ReactStudio
- App description: null
- App Version: 1.7.35,404
- App Website: https://reactstudio.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ReactStudio with the following command
   ```
   brew install --cask react-studio
   ```
4. ReactStudio is ready to use now!