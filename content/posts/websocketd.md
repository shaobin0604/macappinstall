---
title: "Install websocketd on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "WebSockets the Unix way"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install websocketd on MacOS using homebrew

- App Name: websocketd
- App description: WebSockets the Unix way
- App Version: 0.4.1
- App Website: http://websocketd.com

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install websocketd with the following command
   ```
   brew install websocketd
   ```
4. websocketd is ready to use now!