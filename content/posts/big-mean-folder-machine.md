---
title: "Install Big Mean Folder Machine on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "File/folder management utility"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Big Mean Folder Machine on MacOS using homebrew

- App Name: Big Mean Folder Machine
- App description: File/folder management utility
- App Version: 2.43
- App Website: https://www.publicspace.net/BigMeanFolderMachine/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Big Mean Folder Machine with the following command
   ```
   brew install --cask big-mean-folder-machine
   ```
4. Big Mean Folder Machine is ready to use now!