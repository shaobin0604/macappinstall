---
title: "Install Physics 101 on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Collection of simulations, tools, and equations across the field of physics"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Physics 101 on MacOS using homebrew

- App Name: Physics 101
- App description: Collection of simulations, tools, and equations across the field of physics
- App Version: 9.1,9.9.9.3.9
- App Website: http://www.praetersoftware.com/new/physics101/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Physics 101 with the following command
   ```
   brew install --cask physics-101
   ```
4. Physics 101 is ready to use now!