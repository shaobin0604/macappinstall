---
title: "Install Twist on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Team communication and collaboration software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Twist on MacOS using homebrew

- App Name: Twist
- App description: Team communication and collaboration software
- App Version: 1.0.4
- App Website: https://twist.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Twist with the following command
   ```
   brew install --cask twist
   ```
4. Twist is ready to use now!