---
title: "Install QuickBooks Desktop on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Accounting software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install QuickBooks Desktop on MacOS using homebrew

- App Name: QuickBooks Desktop
- App description: Accounting software
- App Version: 20.0.7.908,2021
- App Website: https://quickbooks.intuit.com/desktop/mac/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install QuickBooks Desktop with the following command
   ```
   brew install --cask quickbooks
   ```
4. QuickBooks Desktop is ready to use now!