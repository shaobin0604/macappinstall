---
title: "Install clipsafe on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line interface to Password Safe"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install clipsafe on MacOS using homebrew

- App Name: clipsafe
- App description: Command-line interface to Password Safe
- App Version: 1.1
- App Website: https://waxandwane.org/clipsafe.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install clipsafe with the following command
   ```
   brew install clipsafe
   ```
4. clipsafe is ready to use now!