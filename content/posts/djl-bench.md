---
title: "Install djl-bench on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Universal machine learning model benchmark tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install djl-bench on MacOS using homebrew

- App Name: djl-bench
- App description: Universal machine learning model benchmark tool
- App Version: 0.15.0
- App Website: https://djl.ai/extensions/benchmark/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install djl-bench with the following command
   ```
   brew install --cask djl-bench
   ```
4. djl-bench is ready to use now!