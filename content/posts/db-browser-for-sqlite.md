---
title: "Install DB Browser for SQLite on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Browser for SQLite databases"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install DB Browser for SQLite on MacOS using homebrew

- App Name: DB Browser for SQLite
- App description: Browser for SQLite databases
- App Version: 3.12.2
- App Website: https://sqlitebrowser.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install DB Browser for SQLite with the following command
   ```
   brew install --cask db-browser-for-sqlite
   ```
4. DB Browser for SQLite is ready to use now!