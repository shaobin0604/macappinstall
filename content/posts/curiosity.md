---
title: "Install Curiosity on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "SwiftUI Reddit client"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Curiosity on MacOS using homebrew

- App Name: Curiosity
- App description: SwiftUI Reddit client
- App Version: 0.5.5
- App Website: https://github.com/Dimillian/RedditOS

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Curiosity with the following command
   ```
   brew install --cask curiosity
   ```
4. Curiosity is ready to use now!