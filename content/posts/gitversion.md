---
title: "Install gitversion on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Easy semantic versioning for projects using Git"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gitversion on MacOS using homebrew

- App Name: gitversion
- App description: Easy semantic versioning for projects using Git
- App Version: 5.8.1
- App Website: https://gitversion.net

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gitversion with the following command
   ```
   brew install gitversion
   ```
4. gitversion is ready to use now!