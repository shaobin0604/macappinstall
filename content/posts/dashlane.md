---
title: "Install Dashlane on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Password manager and digital wallet application"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Dashlane on MacOS using homebrew

- App Name: Dashlane
- App description: Password manager and digital wallet application
- App Version: 6.2148.0.52190
- App Website: https://www.dashlane.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Dashlane with the following command
   ```
   brew install --cask dashlane
   ```
4. Dashlane is ready to use now!