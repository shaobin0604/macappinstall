---
title: "Install dns2tcp on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "TCP over DNS tunnel"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dns2tcp on MacOS using homebrew

- App Name: dns2tcp
- App description: TCP over DNS tunnel
- App Version: 0.5.2
- App Website: https://packages.debian.org/sid/dns2tcp

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dns2tcp with the following command
   ```
   brew install dns2tcp
   ```
4. dns2tcp is ready to use now!