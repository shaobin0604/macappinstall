---
title: "Install tmx on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Portable C library to load tiled maps in your games"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tmx on MacOS using homebrew

- App Name: tmx
- App description: Portable C library to load tiled maps in your games
- App Version: 1.4.0
- App Website: https://github.com/baylej/tmx

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tmx with the following command
   ```
   brew install tmx
   ```
4. tmx is ready to use now!