---
title: "Install MacX YouTube Downloader on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to download videos from YouTube"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install MacX YouTube Downloader on MacOS using homebrew

- App Name: MacX YouTube Downloader
- App description: Tool to download videos from YouTube
- App Version: 5.2.1,20211018
- App Website: https://www.macxdvd.com/free-youtube-video-downloader-mac/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install MacX YouTube Downloader with the following command
   ```
   brew install --cask macx-youtube-downloader
   ```
4. MacX YouTube Downloader is ready to use now!