---
title: "Install log4cplus on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Logging Framework for C++"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install log4cplus on MacOS using homebrew

- App Name: log4cplus
- App description: Logging Framework for C++
- App Version: 2.0.7
- App Website: https://sourceforge.net/p/log4cplus/wiki/Home/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install log4cplus with the following command
   ```
   brew install log4cplus
   ```
4. log4cplus is ready to use now!