---
title: "Install quantlib on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library for quantitative finance"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install quantlib on MacOS using homebrew

- App Name: quantlib
- App description: Library for quantitative finance
- App Version: 1.25
- App Website: https://www.quantlib.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install quantlib with the following command
   ```
   brew install quantlib
   ```
4. quantlib is ready to use now!