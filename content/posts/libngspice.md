---
title: "Install libngspice on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Spice circuit simulator as shared library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libngspice on MacOS using homebrew

- App Name: libngspice
- App description: Spice circuit simulator as shared library
- App Version: 36
- App Website: https://ngspice.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libngspice with the following command
   ```
   brew install libngspice
   ```
4. libngspice is ready to use now!