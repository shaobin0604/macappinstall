---
title: "Install asyncplusplus on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Concurrency framework for C++11"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install asyncplusplus on MacOS using homebrew

- App Name: asyncplusplus
- App description: Concurrency framework for C++11
- App Version: 1.1
- App Website: https://github.com/Amanieu/asyncplusplus

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install asyncplusplus with the following command
   ```
   brew install asyncplusplus
   ```
4. asyncplusplus is ready to use now!