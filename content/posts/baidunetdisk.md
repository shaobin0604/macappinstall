---
title: "Install Baidu NetDisk on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cloud storage service"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Baidu NetDisk on MacOS using homebrew

- App Name: Baidu NetDisk
- App description: Cloud storage service
- App Version: 4.5.5
- App Website: https://pan.baidu.com/download

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Baidu NetDisk with the following command
   ```
   brew install --cask baidunetdisk
   ```
4. Baidu NetDisk is ready to use now!