---
title: "Install foma on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Finite-state compiler and C library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install foma on MacOS using homebrew

- App Name: foma
- App description: Finite-state compiler and C library
- App Version: 0.9.18
- App Website: https://code.google.com/p/foma/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install foma with the following command
   ```
   brew install foma
   ```
4. foma is ready to use now!