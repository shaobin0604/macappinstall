---
title: "Install WeChat for Mac on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Free messaging and calling application"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install WeChat for Mac on MacOS using homebrew

- App Name: WeChat for Mac
- App description: Free messaging and calling application
- App Version: 3.3.0.19,20901
- App Website: https://mac.weixin.qq.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install WeChat for Mac with the following command
   ```
   brew install --cask wechat
   ```
4. WeChat for Mac is ready to use now!