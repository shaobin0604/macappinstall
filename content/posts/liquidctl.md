---
title: "Install liquidctl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cross-platform tool and drivers for liquid coolers and other devices"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install liquidctl on MacOS using homebrew

- App Name: liquidctl
- App description: Cross-platform tool and drivers for liquid coolers and other devices
- App Version: 1.8.1
- App Website: https://github.com/liquidctl/liquidctl

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install liquidctl with the following command
   ```
   brew install liquidctl
   ```
4. liquidctl is ready to use now!