---
title: "Install Atlassian SourceTree on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Graphical client for Git version control"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Atlassian SourceTree on MacOS using homebrew

- App Name: Atlassian SourceTree
- App description: Graphical client for Git version control
- App Version: 4.1.6,242
- App Website: https://www.sourcetreeapp.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Atlassian SourceTree with the following command
   ```
   brew install --cask sourcetree
   ```
4. Atlassian SourceTree is ready to use now!