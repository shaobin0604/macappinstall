---
title: "Install MacGDBp on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Live, interactive debugging of your running PHP applications"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install MacGDBp on MacOS using homebrew

- App Name: MacGDBp
- App description: Live, interactive debugging of your running PHP applications
- App Version: 2.1,204.5
- App Website: https://www.bluestatic.org/software/macgdbp/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install MacGDBp with the following command
   ```
   brew install --cask macgdbp
   ```
4. MacGDBp is ready to use now!