---
title: "Install ownCloud on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Desktop syncing client for ownCloud"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ownCloud on MacOS using homebrew

- App Name: ownCloud
- App description: Desktop syncing client for ownCloud
- App Version: 2.10.0.6519
- App Website: https://owncloud.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ownCloud with the following command
   ```
   brew install --cask owncloud
   ```
4. ownCloud is ready to use now!