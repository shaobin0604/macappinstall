---
title: "Install sbtenv on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line tool for managing sbt environments"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sbtenv on MacOS using homebrew

- App Name: sbtenv
- App description: Command-line tool for managing sbt environments
- App Version: 0.0.24
- App Website: https://github.com/sbtenv/sbtenv

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sbtenv with the following command
   ```
   brew install sbtenv
   ```
4. sbtenv is ready to use now!