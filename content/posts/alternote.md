---
title: "Install Alternote on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Note-taking App for Evernote"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Alternote on MacOS using homebrew

- App Name: Alternote
- App description: Note-taking App for Evernote
- App Version: 1.0.18,1018
- App Website: https://alternoteapp.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Alternote with the following command
   ```
   brew install --cask alternote
   ```
4. Alternote is ready to use now!