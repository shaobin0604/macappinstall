---
title: "Install X Lossless Decoder on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Lossless audio decoder"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install X Lossless Decoder on MacOS using homebrew

- App Name: X Lossless Decoder
- App description: Lossless audio decoder
- App Version: 20211018,154.0
- App Website: https://tmkk.undo.jp/xld/index_e.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install X Lossless Decoder with the following command
   ```
   brew install --cask xld
   ```
4. X Lossless Decoder is ready to use now!