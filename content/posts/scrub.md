---
title: "Install scrub on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Writes patterns on magnetic media to thwart data recovery"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install scrub on MacOS using homebrew

- App Name: scrub
- App description: Writes patterns on magnetic media to thwart data recovery
- App Version: 2.6.1
- App Website: https://code.google.com/archive/p/diskscrub/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install scrub with the following command
   ```
   brew install scrub
   ```
4. scrub is ready to use now!