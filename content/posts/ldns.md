---
title: "Install ldns on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "DNS library written in C"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ldns on MacOS using homebrew

- App Name: ldns
- App description: DNS library written in C
- App Version: 1.8.1
- App Website: https://nlnetlabs.nl/projects/ldns/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ldns with the following command
   ```
   brew install ldns
   ```
4. ldns is ready to use now!