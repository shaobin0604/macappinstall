---
title: "Install gau on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Open Threat Exchange, Wayback Machine, and Common Crawl URL fetcher"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gau on MacOS using homebrew

- App Name: gau
- App description: Open Threat Exchange, Wayback Machine, and Common Crawl URL fetcher
- App Version: 2.0.8
- App Website: https://github.com/lc/gau

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gau with the following command
   ```
   brew install gau
   ```
4. gau is ready to use now!