---
title: "Install ko on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Build and deploy Go applications on Kubernetes"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ko on MacOS using homebrew

- App Name: ko
- App description: Build and deploy Go applications on Kubernetes
- App Version: 0.10.0
- App Website: https://github.com/google/ko

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ko with the following command
   ```
   brew install ko
   ```
4. ko is ready to use now!