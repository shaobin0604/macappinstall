---
title: "Install Amazon DynamoDB Local on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Amazon DynamoDB Local on MacOS using homebrew

- App Name: Amazon DynamoDB Local
- App description: null
- App Version: latest
- App Website: https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/DynamoDBLocal.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Amazon DynamoDB Local with the following command
   ```
   brew install --cask dynamodb-local
   ```
4. Amazon DynamoDB Local is ready to use now!