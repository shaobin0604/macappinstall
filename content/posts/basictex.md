---
title: "Install BasicTeX on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Compact TeX distribution as alternative to the full TeX Live / MacTeX"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install BasicTeX on MacOS using homebrew

- App Name: BasicTeX
- App description: Compact TeX distribution as alternative to the full TeX Live / MacTeX
- App Version: 2021.0325
- App Website: https://www.tug.org/mactex/morepackages.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install BasicTeX with the following command
   ```
   brew install --cask basictex
   ```
4. BasicTeX is ready to use now!