---
title: "Install format-udf on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Bash script to format a block device to UDF"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install format-udf on MacOS using homebrew

- App Name: format-udf
- App description: Bash script to format a block device to UDF
- App Version: 1.8.0
- App Website: https://github.com/JElchison/format-udf

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install format-udf with the following command
   ```
   brew install format-udf
   ```
4. format-udf is ready to use now!