---
title: "Install Swisscom myCloud Desktop on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Swiss cloud storage desktop app"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Swisscom myCloud Desktop on MacOS using homebrew

- App Name: Swisscom myCloud Desktop
- App description: Swiss cloud storage desktop app
- App Version: 21.26.6
- App Website: https://desktop.mycloud.ch/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Swisscom myCloud Desktop with the following command
   ```
   brew install --cask mycloud
   ```
4. Swisscom myCloud Desktop is ready to use now!