---
title: "Install Responsively on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Modified browser that helps in responsive web development"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Responsively on MacOS using homebrew

- App Name: Responsively
- App description: Modified browser that helps in responsive web development
- App Version: 0.18.0
- App Website: https://responsively.app/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Responsively with the following command
   ```
   brew install --cask responsively
   ```
4. Responsively is ready to use now!