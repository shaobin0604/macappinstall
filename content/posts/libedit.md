---
title: "Install libedit on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "BSD-style licensed readline alternative"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libedit on MacOS using homebrew

- App Name: libedit
- App description: BSD-style licensed readline alternative
- App Version: 20210910-3.1
- App Website: https://thrysoee.dk/editline/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libedit with the following command
   ```
   brew install libedit
   ```
4. libedit is ready to use now!