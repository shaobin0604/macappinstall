---
title: "Install Company Portal on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "App to manage access to corporate apps, data, and resources"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Company Portal on MacOS using homebrew

- App Name: Company Portal
- App description: App to manage access to corporate apps, data, and resources
- App Version: 2.12.210101
- App Website: https://docs.microsoft.com/en-us/mem/intune/user-help/enroll-your-device-in-intune-macos-cp

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Company Portal with the following command
   ```
   brew install --cask intune-company-portal
   ```
4. Company Portal is ready to use now!