---
title: "Install itch.io on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Game client for itch.io"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install itch.io on MacOS using homebrew

- App Name: itch.io
- App description: Game client for itch.io
- App Version: 25.5.1
- App Website: https://itch.io/app

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install itch.io with the following command
   ```
   brew install --cask itch
   ```
4. itch.io is ready to use now!