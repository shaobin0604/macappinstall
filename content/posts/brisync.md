---
title: "Install Brisync on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utility to automatically control the brightness of external displays"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Brisync on MacOS using homebrew

- App Name: Brisync
- App description: Utility to automatically control the brightness of external displays
- App Version: 1.3.1
- App Website: https://github.com/czarny/Brisync/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Brisync with the following command
   ```
   brew install --cask brisync
   ```
4. Brisync is ready to use now!