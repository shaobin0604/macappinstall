---
title: "Install cfengine on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Help manage and understand IT infrastructure"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cfengine on MacOS using homebrew

- App Name: cfengine
- App description: Help manage and understand IT infrastructure
- App Version: 3.19.0
- App Website: https://cfengine.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cfengine with the following command
   ```
   brew install cfengine
   ```
4. cfengine is ready to use now!