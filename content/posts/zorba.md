---
title: "Install zorba on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "NoSQL query processor"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install zorba on MacOS using homebrew

- App Name: zorba
- App description: NoSQL query processor
- App Version: 3.1
- App Website: http://www.zorba.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install zorba with the following command
   ```
   brew install zorba
   ```
4. zorba is ready to use now!