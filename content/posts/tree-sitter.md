---
title: "Install tree-sitter on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Parser generator tool and incremental parsing library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tree-sitter on MacOS using homebrew

- App Name: tree-sitter
- App description: Parser generator tool and incremental parsing library
- App Version: 0.20.4
- App Website: https://tree-sitter.github.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tree-sitter with the following command
   ```
   brew install tree-sitter
   ```
4. tree-sitter is ready to use now!