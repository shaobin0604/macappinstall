---
title: "Install ssldump on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "SSLv3/TLS network protocol analyzer"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ssldump on MacOS using homebrew

- App Name: ssldump
- App description: SSLv3/TLS network protocol analyzer
- App Version: 0.9b3
- App Website: https://ssldump.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ssldump with the following command
   ```
   brew install ssldump
   ```
4. ssldump is ready to use now!