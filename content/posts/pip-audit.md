---
title: "Install pip-audit on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Audits Python environments and dependency trees for known vulnerabilities"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pip-audit on MacOS using homebrew

- App Name: pip-audit
- App description: Audits Python environments and dependency trees for known vulnerabilities
- App Version: 1.1.2
- App Website: https://pypi.org/project/pip-audit/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pip-audit with the following command
   ```
   brew install pip-audit
   ```
4. pip-audit is ready to use now!