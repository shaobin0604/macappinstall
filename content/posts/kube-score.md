---
title: "Install kube-score on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Kubernetes object analysis recommendations for improved reliability and security"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install kube-score on MacOS using homebrew

- App Name: kube-score
- App description: Kubernetes object analysis recommendations for improved reliability and security
- App Version: 1.14.0
- App Website: https://kube-score.com

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install kube-score with the following command
   ```
   brew install kube-score
   ```
4. kube-score is ready to use now!