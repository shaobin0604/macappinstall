---
title: "Install HWSensors on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to access information from available hardware sensors"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install HWSensors on MacOS using homebrew

- App Name: HWSensors
- App description: Tool to access information from available hardware sensors
- App Version: 6.26.1440
- App Website: https://github.com/kozlekek/HWSensors/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install HWSensors with the following command
   ```
   brew install --cask hwsensors
   ```
4. HWSensors is ready to use now!