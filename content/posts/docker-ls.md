---
title: "Install docker-ls on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tools for browsing and manipulating docker registries"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install docker-ls on MacOS using homebrew

- App Name: docker-ls
- App description: Tools for browsing and manipulating docker registries
- App Version: 0.5.1
- App Website: https://github.com/mayflower/docker-ls

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install docker-ls with the following command
   ```
   brew install docker-ls
   ```
4. docker-ls is ready to use now!