---
title: "Install OverDrive Media Console on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Get eBooks, audiobooks, and videos from your local library"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install OverDrive Media Console on MacOS using homebrew

- App Name: OverDrive Media Console
- App description: Get eBooks, audiobooks, and videos from your local library
- App Version: 1.2
- App Website: https://www.overdrive.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install OverDrive Media Console with the following command
   ```
   brew install --cask overdrive-media-console
   ```
4. OverDrive Media Console is ready to use now!