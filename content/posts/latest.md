---
title: "Install Latest on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utility that shows the latest app updates"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Latest on MacOS using homebrew

- App Name: Latest
- App description: Utility that shows the latest app updates
- App Version: 0.7.3,462
- App Website: https://max.codes/latest

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Latest with the following command
   ```
   brew install --cask latest
   ```
4. Latest is ready to use now!