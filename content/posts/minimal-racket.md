---
title: "Install minimal-racket on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Modern programming language in the Lisp/Scheme family"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install minimal-racket on MacOS using homebrew

- App Name: minimal-racket
- App description: Modern programming language in the Lisp/Scheme family
- App Version: 8.4
- App Website: https://racket-lang.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install minimal-racket with the following command
   ```
   brew install minimal-racket
   ```
4. minimal-racket is ready to use now!