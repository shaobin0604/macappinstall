---
title: "Install Poedit on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Translation editor"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Poedit on MacOS using homebrew

- App Name: Poedit
- App description: Translation editor
- App Version: 3.0.1,6415
- App Website: https://poedit.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Poedit with the following command
   ```
   brew install --cask poedit
   ```
4. Poedit is ready to use now!