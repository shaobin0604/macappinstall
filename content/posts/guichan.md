---
title: "Install guichan on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Small, efficient C++ GUI library designed for games"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install guichan on MacOS using homebrew

- App Name: guichan
- App description: Small, efficient C++ GUI library designed for games
- App Version: 0.8.2
- App Website: https://guichan.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install guichan with the following command
   ```
   brew install guichan
   ```
4. guichan is ready to use now!