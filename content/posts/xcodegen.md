---
title: "Install xcodegen on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Generate your Xcode project from a spec file and your folder structure"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install xcodegen on MacOS using homebrew

- App Name: xcodegen
- App description: Generate your Xcode project from a spec file and your folder structure
- App Version: 2.26.0
- App Website: https://github.com/yonaskolb/XcodeGen

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install xcodegen with the following command
   ```
   brew install xcodegen
   ```
4. xcodegen is ready to use now!