---
title: "Install Backblaze on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Data backup and storage service"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Backblaze on MacOS using homebrew

- App Name: Backblaze
- App description: Data backup and storage service
- App Version: 8.0.1.584
- App Website: https://backblaze.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Backblaze with the following command
   ```
   brew install --cask backblaze
   ```
4. Backblaze is ready to use now!