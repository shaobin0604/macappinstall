---
title: "Install biglybt on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Bittorrent client based on the Azureus open source project"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install biglybt on MacOS using homebrew

- App Name: biglybt
- App description: Bittorrent client based on the Azureus open source project
- App Version: 2.9.0.0
- App Website: https://www.biglybt.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install biglybt with the following command
   ```
   brew install --cask biglybt
   ```
4. biglybt is ready to use now!