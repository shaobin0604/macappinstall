---
title: "Install FroYoSoft Sound Booster Lite on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "App for an enhanced audio experience"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install FroYoSoft Sound Booster Lite on MacOS using homebrew

- App Name: FroYoSoft Sound Booster Lite
- App description: App for an enhanced audio experience
- App Version: latest
- App Website: https://froyosoft.com/soundbooster.php

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install FroYoSoft Sound Booster Lite with the following command
   ```
   brew install --cask soundboosterlite
   ```
4. FroYoSoft Sound Booster Lite is ready to use now!