---
title: "Install ZY Player on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Video resource player"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ZY Player on MacOS using homebrew

- App Name: ZY Player
- App description: Video resource player
- App Version: 2.8.5
- App Website: https://github.com/Hunlongyu/ZY-Player

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ZY Player with the following command
   ```
   brew install --cask zy-player
   ```
4. ZY Player is ready to use now!