---
title: "Install osm2pgrouting on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Import OSM data into pgRouting database"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install osm2pgrouting on MacOS using homebrew

- App Name: osm2pgrouting
- App description: Import OSM data into pgRouting database
- App Version: 2.3.8
- App Website: https://pgrouting.org/docs/tools/osm2pgrouting.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install osm2pgrouting with the following command
   ```
   brew install osm2pgrouting
   ```
4. osm2pgrouting is ready to use now!