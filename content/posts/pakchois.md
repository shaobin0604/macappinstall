---
title: "Install pakchois on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "PKCS #11 wrapper library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pakchois on MacOS using homebrew

- App Name: pakchois
- App description: PKCS #11 wrapper library
- App Version: 0.4
- App Website: http://www.manyfish.co.uk/pakchois/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pakchois with the following command
   ```
   brew install pakchois
   ```
4. pakchois is ready to use now!