---
title: "Install Tor Browser on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Web browser focusing on security"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Tor Browser on MacOS using homebrew

- App Name: Tor Browser
- App description: Web browser focusing on security
- App Version: 11.0.6
- App Website: https://www.torproject.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Tor Browser with the following command
   ```
   brew install --cask tor-browser
   ```
4. Tor Browser is ready to use now!