---
title: "Install Material Colors for Mac on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Material Colors for Mac on MacOS using homebrew

- App Name: Material Colors for Mac
- App description: null
- App Version: 2.0.3
- App Website: https://github.com/romannurik/MaterialColorsApp

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Material Colors for Mac with the following command
   ```
   brew install --cask material-colors
   ```
4. Material Colors for Mac is ready to use now!