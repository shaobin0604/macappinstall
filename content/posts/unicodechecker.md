---
title: "Install UnicodeChecker on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Explore and convert Unicode"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install UnicodeChecker on MacOS using homebrew

- App Name: UnicodeChecker
- App description: Explore and convert Unicode
- App Version: 1.23,796
- App Website: https://earthlingsoft.net/UnicodeChecker/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install UnicodeChecker with the following command
   ```
   brew install --cask unicodechecker
   ```
4. UnicodeChecker is ready to use now!