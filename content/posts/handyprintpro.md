---
title: "Install handyPrintPRO on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "AirPrint server"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install handyPrintPRO on MacOS using homebrew

- App Name: handyPrintPRO
- App description: AirPrint server
- App Version: 5.5.0
- App Website: http://www.netputing.com/handyprintpro/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install handyPrintPRO with the following command
   ```
   brew install --cask handyprintpro
   ```
4. handyPrintPRO is ready to use now!