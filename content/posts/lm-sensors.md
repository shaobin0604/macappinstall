---
title: "Install lm-sensors on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tools for monitoring the temperatures, voltages, and fans"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install lm-sensors on MacOS using homebrew

- App Name: lm-sensors
- App description: Tools for monitoring the temperatures, voltages, and fans
- App Version: 3.6.0
- App Website: https://github.com/groeck/lm-sensors

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install lm-sensors with the following command
   ```
   brew install lm-sensors
   ```
4. lm-sensors is ready to use now!