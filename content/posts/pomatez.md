---
title: "Install Pomatez on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Pomodoro timer"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Pomatez on MacOS using homebrew

- App Name: Pomatez
- App description: Pomodoro timer
- App Version: 1.1.0
- App Website: https://roldanjr.github.io/pomatez

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Pomatez with the following command
   ```
   brew install --cask pomatez
   ```
4. Pomatez is ready to use now!