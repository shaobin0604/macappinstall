---
title: "Install libmemcached on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C and C++ client library to the memcached server"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libmemcached on MacOS using homebrew

- App Name: libmemcached
- App description: C and C++ client library to the memcached server
- App Version: 1.0.18
- App Website: https://libmemcached.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libmemcached with the following command
   ```
   brew install libmemcached
   ```
4. libmemcached is ready to use now!