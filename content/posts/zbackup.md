---
title: "Install zbackup on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Globally-deduplicating backup tool (based on ideas in rsync)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install zbackup on MacOS using homebrew

- App Name: zbackup
- App description: Globally-deduplicating backup tool (based on ideas in rsync)
- App Version: 1.4.4
- App Website: http://zbackup.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install zbackup with the following command
   ```
   brew install zbackup
   ```
4. zbackup is ready to use now!