---
title: "Install xml2 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Makes XML and HTML more amenable to classic UNIX text tools"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install xml2 on MacOS using homebrew

- App Name: xml2
- App description: Makes XML and HTML more amenable to classic UNIX text tools
- App Version: 0.5
- App Website: https://web.archive.org/web/20160730094113/www.ofb.net/~egnor/xml2/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install xml2 with the following command
   ```
   brew install xml2
   ```
4. xml2 is ready to use now!