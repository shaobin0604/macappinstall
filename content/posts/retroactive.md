---
title: "Install Retroactive on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Run Apple apps on incompatible OS versions"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Retroactive on MacOS using homebrew

- App Name: Retroactive
- App description: Run Apple apps on incompatible OS versions
- App Version: 1.9
- App Website: https://github.com/cormiertyshawn895/Retroactive

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Retroactive with the following command
   ```
   brew install --cask retroactive
   ```
4. Retroactive is ready to use now!