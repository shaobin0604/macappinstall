---
title: "Install ghex on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GNOME hex editor"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ghex on MacOS using homebrew

- App Name: ghex
- App description: GNOME hex editor
- App Version: 3.18.4
- App Website: https://wiki.gnome.org/Apps/Ghex

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ghex with the following command
   ```
   brew install ghex
   ```
4. ghex is ready to use now!