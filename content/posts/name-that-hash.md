---
title: "Install name-that-hash on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Modern hash identification system"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install name-that-hash on MacOS using homebrew

- App Name: name-that-hash
- App description: Modern hash identification system
- App Version: 1.10.0
- App Website: https://nth.skerritt.blog/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install name-that-hash with the following command
   ```
   brew install name-that-hash
   ```
4. name-that-hash is ready to use now!