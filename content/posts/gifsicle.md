---
title: "Install gifsicle on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GIF image/animation creator/editor"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gifsicle on MacOS using homebrew

- App Name: gifsicle
- App description: GIF image/animation creator/editor
- App Version: 1.93
- App Website: https://www.lcdf.org/gifsicle/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gifsicle with the following command
   ```
   brew install gifsicle
   ```
4. gifsicle is ready to use now!