---
title: "Install exa on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Modern replacement for 'ls'"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install exa on MacOS using homebrew

- App Name: exa
- App description: Modern replacement for 'ls'
- App Version: 0.10.1
- App Website: https://the.exa.website

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install exa with the following command
   ```
   brew install exa
   ```
4. exa is ready to use now!