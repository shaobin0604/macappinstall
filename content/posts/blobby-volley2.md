---
title: "Install Blobby Volley 2 on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Head-to-head multiplayer ball game"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Blobby Volley 2 on MacOS using homebrew

- App Name: Blobby Volley 2
- App description: Head-to-head multiplayer ball game
- App Version: 1.0
- App Website: https://blobby.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Blobby Volley 2 with the following command
   ```
   brew install --cask blobby-volley2
   ```
4. Blobby Volley 2 is ready to use now!