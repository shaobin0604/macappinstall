---
title: "Install Dyalog APL on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "APL-based development environment"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Dyalog APL on MacOS using homebrew

- App Name: Dyalog APL
- App description: APL-based development environment
- App Version: 18.0.40684
- App Website: https://www.dyalog.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Dyalog APL with the following command
   ```
   brew install --cask dyalog
   ```
4. Dyalog APL is ready to use now!