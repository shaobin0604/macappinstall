---
title: "Install google-benchmark on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C++ microbenchmark support library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install google-benchmark on MacOS using homebrew

- App Name: google-benchmark
- App description: C++ microbenchmark support library
- App Version: 1.6.1
- App Website: https://github.com/google/benchmark

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install google-benchmark with the following command
   ```
   brew install google-benchmark
   ```
4. google-benchmark is ready to use now!