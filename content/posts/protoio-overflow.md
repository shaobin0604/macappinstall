---
title: "Install Overflow on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Create interactive user flow diagrams"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Overflow on MacOS using homebrew

- App Name: Overflow
- App description: Create interactive user flow diagrams
- App Version: 1.10.1
- App Website: https://overflow.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Overflow with the following command
   ```
   brew install --cask protoio-overflow
   ```
4. Overflow is ready to use now!