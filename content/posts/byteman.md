---
title: "Install byteman on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Java bytecode manipulation tool for testing, monitoring and tracing"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install byteman on MacOS using homebrew

- App Name: byteman
- App description: Java bytecode manipulation tool for testing, monitoring and tracing
- App Version: 4.0.18
- App Website: https://byteman.jboss.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install byteman with the following command
   ```
   brew install byteman
   ```
4. byteman is ready to use now!