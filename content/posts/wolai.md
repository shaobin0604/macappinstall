---
title: "Install wolai for mac on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cloud notes"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install wolai for mac on MacOS using homebrew

- App Name: wolai for mac
- App description: Cloud notes
- App Version: 1.1.3
- App Website: https://www.wolai.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install wolai for mac with the following command
   ```
   brew install --cask wolai
   ```
4. wolai for mac is ready to use now!