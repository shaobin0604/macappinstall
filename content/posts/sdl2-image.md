---
title: "Install sdl2_image on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library for loading images as SDL surfaces and textures"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sdl2_image on MacOS using homebrew

- App Name: sdl2_image
- App description: Library for loading images as SDL surfaces and textures
- App Version: 2.0.5
- App Website: https://www.libsdl.org/projects/SDL_image/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sdl2_image with the following command
   ```
   brew install sdl2_image
   ```
4. sdl2_image is ready to use now!