---
title: "Install Notch Simulator on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Simulate the notch on the MacBook Pro"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Notch Simulator on MacOS using homebrew

- App Name: Notch Simulator
- App description: Simulate the notch on the MacBook Pro
- App Version: 2.1
- App Website: https://github.com/megabitsenmzq/Notch-Simulator

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Notch Simulator with the following command
   ```
   brew install --cask notch-simulator
   ```
4. Notch Simulator is ready to use now!