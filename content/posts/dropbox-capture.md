---
title: "Install Dropbox Capture on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Share your work and ideas with video messages and screenshots"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Dropbox Capture on MacOS using homebrew

- App Name: Dropbox Capture
- App description: Share your work and ideas with video messages and screenshots
- App Version: 68.0.0
- App Website: https://dropbox.com/capture/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Dropbox Capture with the following command
   ```
   brew install --cask dropbox-capture
   ```
4. Dropbox Capture is ready to use now!