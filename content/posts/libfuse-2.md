---
title: "Install libfuse@2 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Reference implementation of the Linux FUSE interface"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libfuse@2 on MacOS using homebrew

- App Name: libfuse@2
- App description: Reference implementation of the Linux FUSE interface
- App Version: 2.9.9
- App Website: https://github.com/libfuse/libfuse

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libfuse@2 with the following command
   ```
   brew install libfuse@2
   ```
4. libfuse@2 is ready to use now!