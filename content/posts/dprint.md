---
title: "Install dprint on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Pluggable and configurable code formatting platform written in Rust"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dprint on MacOS using homebrew

- App Name: dprint
- App description: Pluggable and configurable code formatting platform written in Rust
- App Version: 0.22.2
- App Website: https://dprint.dev/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dprint with the following command
   ```
   brew install dprint
   ```
4. dprint is ready to use now!