---
title: "Install rpg on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Ruby package management for UNIX"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install rpg on MacOS using homebrew

- App Name: rpg
- App description: Ruby package management for UNIX
- App Version: 0.3.0
- App Website: https://github.com/rtomayko/rpg

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install rpg with the following command
   ```
   brew install rpg
   ```
4. rpg is ready to use now!