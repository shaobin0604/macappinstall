---
title: "Install pipenv on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Python dependency management tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pipenv on MacOS using homebrew

- App Name: pipenv
- App description: Python dependency management tool
- App Version: 2022.1.8
- App Website: https://github.com/pypa/pipenv

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pipenv with the following command
   ```
   brew install pipenv
   ```
4. pipenv is ready to use now!