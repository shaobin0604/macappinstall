---
title: "Install libxfont on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "X.Org: Core of the legacy X11 font system"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libxfont on MacOS using homebrew

- App Name: libxfont
- App description: X.Org: Core of the legacy X11 font system
- App Version: 1.5.4
- App Website: https://www.x.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libxfont with the following command
   ```
   brew install libxfont
   ```
4. libxfont is ready to use now!