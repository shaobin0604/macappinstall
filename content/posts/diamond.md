---
title: "Install diamond on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Accelerated BLAST compatible local sequence aligner"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install diamond on MacOS using homebrew

- App Name: diamond
- App description: Accelerated BLAST compatible local sequence aligner
- App Version: 2.0.14
- App Website: https://www.wsi.uni-tuebingen.de/lehrstuehle/algorithms-in-bioinformatics/software/diamond/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install diamond with the following command
   ```
   brew install diamond
   ```
4. diamond is ready to use now!