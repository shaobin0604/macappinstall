---
title: "Install neovim-qt on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Neovim GUI, in Qt5"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install neovim-qt on MacOS using homebrew

- App Name: neovim-qt
- App description: Neovim GUI, in Qt5
- App Version: 0.2.16.1
- App Website: https://github.com/equalsraf/neovim-qt

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install neovim-qt with the following command
   ```
   brew install neovim-qt
   ```
4. neovim-qt is ready to use now!