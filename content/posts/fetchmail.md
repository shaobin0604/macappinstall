---
title: "Install fetchmail on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Client for fetching mail from POP, IMAP, ETRN or ODMR-capable servers"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fetchmail on MacOS using homebrew

- App Name: fetchmail
- App description: Client for fetching mail from POP, IMAP, ETRN or ODMR-capable servers
- App Version: 6.4.27
- App Website: https://www.fetchmail.info/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fetchmail with the following command
   ```
   brew install fetchmail
   ```
4. fetchmail is ready to use now!