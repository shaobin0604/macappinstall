---
title: "Install just on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Handy way to save and run project-specific commands"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install just on MacOS using homebrew

- App Name: just
- App description: Handy way to save and run project-specific commands
- App Version: 0.11.2
- App Website: https://github.com/casey/just

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install just with the following command
   ```
   brew install just
   ```
4. just is ready to use now!