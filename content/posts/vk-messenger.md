---
title: "Install VK Messenger on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Messenger app"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install VK Messenger on MacOS using homebrew

- App Name: VK Messenger
- App description: Messenger app
- App Version: 5.3.2,723
- App Website: https://vk.com/messenger

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install VK Messenger with the following command
   ```
   brew install --cask vk-messenger
   ```
4. VK Messenger is ready to use now!