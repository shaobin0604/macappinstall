---
title: "Install pybind11 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Seamless operability between C++11 and Python"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pybind11 on MacOS using homebrew

- App Name: pybind11
- App description: Seamless operability between C++11 and Python
- App Version: 2.9.0
- App Website: https://github.com/pybind/pybind11

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pybind11 with the following command
   ```
   brew install pybind11
   ```
4. pybind11 is ready to use now!