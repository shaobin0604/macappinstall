---
title: "Install gnu-prolog on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Prolog compiler with constraint solving"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gnu-prolog on MacOS using homebrew

- App Name: gnu-prolog
- App description: Prolog compiler with constraint solving
- App Version: 1.5.0
- App Website: http://www.gprolog.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gnu-prolog with the following command
   ```
   brew install gnu-prolog
   ```
4. gnu-prolog is ready to use now!