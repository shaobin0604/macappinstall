---
title: "Install kubie on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Much more powerful alternative to kubectx and kubens"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install kubie on MacOS using homebrew

- App Name: kubie
- App description: Much more powerful alternative to kubectx and kubens
- App Version: 0.16.0
- App Website: https://blog.sbstp.ca/introducing-kubie/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install kubie with the following command
   ```
   brew install kubie
   ```
4. kubie is ready to use now!