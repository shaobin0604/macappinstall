---
title: "Install libgweather on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GNOME library for weather, locations and timezones"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libgweather on MacOS using homebrew

- App Name: libgweather
- App description: GNOME library for weather, locations and timezones
- App Version: 40.0
- App Website: https://wiki.gnome.org/Projects/LibGWeather

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libgweather with the following command
   ```
   brew install libgweather
   ```
4. libgweather is ready to use now!