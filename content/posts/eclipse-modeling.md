---
title: "Install Eclipse Modeling Tools on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tools and runtimes for building model-based applications"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Eclipse Modeling Tools on MacOS using homebrew

- App Name: Eclipse Modeling Tools
- App description: Tools and runtimes for building model-based applications
- App Version: 4.22.0,2021-12
- App Website: https://eclipse.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Eclipse Modeling Tools with the following command
   ```
   brew install --cask eclipse-modeling
   ```
4. Eclipse Modeling Tools is ready to use now!