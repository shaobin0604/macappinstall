---
title: "Install aws-shell on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Integrated shell for working with the AWS CLI"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install aws-shell on MacOS using homebrew

- App Name: aws-shell
- App description: Integrated shell for working with the AWS CLI
- App Version: 0.2.2
- App Website: https://github.com/awslabs/aws-shell

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install aws-shell with the following command
   ```
   brew install aws-shell
   ```
4. aws-shell is ready to use now!