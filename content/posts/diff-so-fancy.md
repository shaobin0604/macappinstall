---
title: "Install diff-so-fancy on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Good-lookin' diffs with diff-highlight and more"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install diff-so-fancy on MacOS using homebrew

- App Name: diff-so-fancy
- App description: Good-lookin' diffs with diff-highlight and more
- App Version: 1.4.3
- App Website: https://github.com/so-fancy/diff-so-fancy

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install diff-so-fancy with the following command
   ```
   brew install diff-so-fancy
   ```
4. diff-so-fancy is ready to use now!