---
title: "Install macvim on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GUI for vim, made for macOS"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install macvim on MacOS using homebrew

- App Name: macvim
- App description: GUI for vim, made for macOS
- App Version: 8.2-172
- App Website: https://github.com/macvim-dev/macvim

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install macvim with the following command
   ```
   brew install macvim
   ```
4. macvim is ready to use now!