---
title: "Install ImageX on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Visually explore and search an image collection"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ImageX on MacOS using homebrew

- App Name: ImageX
- App description: Visually explore and search an image collection
- App Version: 0.1.2
- App Website: https://visual-computing.com/project/imagex/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ImageX with the following command
   ```
   brew install --cask imagex
   ```
4. ImageX is ready to use now!