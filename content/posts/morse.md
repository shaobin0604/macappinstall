---
title: "Install morse on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "QSO generator and morse code trainer"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install morse on MacOS using homebrew

- App Name: morse
- App description: QSO generator and morse code trainer
- App Version: 2.5
- App Website: http://www.catb.org/~esr/morse/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install morse with the following command
   ```
   brew install morse
   ```
4. morse is ready to use now!