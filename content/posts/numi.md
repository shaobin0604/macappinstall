---
title: "Install Numi on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Calculator and converter application"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Numi on MacOS using homebrew

- App Name: Numi
- App description: Calculator and converter application
- App Version: 3.29.680
- App Website: https://numi.app/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Numi with the following command
   ```
   brew install --cask numi
   ```
4. Numi is ready to use now!