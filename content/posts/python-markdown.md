---
title: "Install python-markdown on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Python implementation of Markdown"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install python-markdown on MacOS using homebrew

- App Name: python-markdown
- App description: Python implementation of Markdown
- App Version: 3.3.6
- App Website: https://python-markdown.github.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install python-markdown with the following command
   ```
   brew install python-markdown
   ```
4. python-markdown is ready to use now!