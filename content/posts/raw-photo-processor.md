---
title: "Install Raw Photo Processor on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Process raw photos"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Raw Photo Processor on MacOS using homebrew

- App Name: Raw Photo Processor
- App description: Process raw photos
- App Version: 1922Beta
- App Website: https://www.raw-photo-processor.com/RPP/Overview.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Raw Photo Processor with the following command
   ```
   brew install --cask raw-photo-processor
   ```
4. Raw Photo Processor is ready to use now!