---
title: "Install envoy on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cloud-native high-performance edge/middle/service proxy"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install envoy on MacOS using homebrew

- App Name: envoy
- App description: Cloud-native high-performance edge/middle/service proxy
- App Version: 1.21.0
- App Website: https://www.envoyproxy.io/index.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install envoy with the following command
   ```
   brew install envoy
   ```
4. envoy is ready to use now!