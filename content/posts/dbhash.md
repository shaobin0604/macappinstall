---
title: "Install dbhash on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Computes the SHA1 hash of schema and content of a SQLite database"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dbhash on MacOS using homebrew

- App Name: dbhash
- App description: Computes the SHA1 hash of schema and content of a SQLite database
- App Version: 3.37.2
- App Website: https://www.sqlite.org/dbhash.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dbhash with the following command
   ```
   brew install dbhash
   ```
4. dbhash is ready to use now!