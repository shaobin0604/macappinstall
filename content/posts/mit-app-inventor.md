---
title: "Install MIT App Inventor on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install MIT App Inventor on MacOS using homebrew

- App Name: MIT App Inventor
- App description: null
- App Version: 3.0rc3
- App Website: https://appinventor.mit.edu/explore/ai2/mac

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install MIT App Inventor with the following command
   ```
   brew install --cask mit-app-inventor
   ```
4. MIT App Inventor is ready to use now!