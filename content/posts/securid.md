---
title: "Install RSA SecurID on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Authentication software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install RSA SecurID on MacOS using homebrew

- App Name: RSA SecurID
- App description: Authentication software
- App Version: 4.2.3
- App Website: https://community.rsa.com/t5/securid-software-token-for-macos/tkb-p/securid-software-token-macos

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install RSA SecurID with the following command
   ```
   brew install --cask securid
   ```
4. RSA SecurID is ready to use now!