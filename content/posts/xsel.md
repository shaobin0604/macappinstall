---
title: "Install xsel on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line program for getting and setting the contents of the X selection"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install xsel on MacOS using homebrew

- App Name: xsel
- App description: Command-line program for getting and setting the contents of the X selection
- App Version: 1.2.0
- App Website: http://www.vergenet.net/~conrad/software/xsel/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install xsel with the following command
   ```
   brew install xsel
   ```
4. xsel is ready to use now!