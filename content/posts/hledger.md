---
title: "Install hledger on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Easy plain text accounting with command-line, terminal and web UIs"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install hledger on MacOS using homebrew

- App Name: hledger
- App description: Easy plain text accounting with command-line, terminal and web UIs
- App Version: 1.24.1
- App Website: https://hledger.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install hledger with the following command
   ```
   brew install hledger
   ```
4. hledger is ready to use now!