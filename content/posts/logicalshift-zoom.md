---
title: "Install Zoom on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Player for Z-Code, TADS, and HUGO stories or games"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Zoom on MacOS using homebrew

- App Name: Zoom
- App description: Player for Z-Code, TADS, and HUGO stories or games
- App Version: 1.1.5
- App Website: https://www.logicalshift.co.uk/unix/zoom/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Zoom with the following command
   ```
   brew install --cask logicalshift-zoom
   ```
4. Zoom is ready to use now!