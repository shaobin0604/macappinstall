---
title: "Install qrupdate on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Fast updates of QR and Cholesky decompositions"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install qrupdate on MacOS using homebrew

- App Name: qrupdate
- App description: Fast updates of QR and Cholesky decompositions
- App Version: 1.1.2
- App Website: https://sourceforge.net/projects/qrupdate/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install qrupdate with the following command
   ```
   brew install qrupdate
   ```
4. qrupdate is ready to use now!