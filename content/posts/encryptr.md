---
title: "Install SpiderOak Encryptr on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Zero-knowledge cloud-based password manager"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install SpiderOak Encryptr on MacOS using homebrew

- App Name: SpiderOak Encryptr
- App description: Zero-knowledge cloud-based password manager
- App Version: 2.1.0
- App Website: https://spideroak.com/encryptr/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install SpiderOak Encryptr with the following command
   ```
   brew install --cask encryptr
   ```
4. SpiderOak Encryptr is ready to use now!