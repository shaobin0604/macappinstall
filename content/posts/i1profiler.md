---
title: "Install i1Profiler on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Automation and creative controls for photographers and designers"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install i1Profiler on MacOS using homebrew

- App Name: i1Profiler
- App description: Automation and creative controls for photographers and designers
- App Version: 3.5.0.15651
- App Website: https://www.xrite.com/service-support/downloads/I/i1Profiler-i1Publish_V3_5_0

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install i1Profiler with the following command
   ```
   brew install --cask i1profiler
   ```
4. i1Profiler is ready to use now!