---
title: "Install juju on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "DevOps management tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install juju on MacOS using homebrew

- App Name: juju
- App description: DevOps management tool
- App Version: 2.9.25
- App Website: https://juju.is/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install juju with the following command
   ```
   brew install juju
   ```
4. juju is ready to use now!