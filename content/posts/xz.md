---
title: "Install xz on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "General-purpose data compression with high compression ratio"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install xz on MacOS using homebrew

- App Name: xz
- App description: General-purpose data compression with high compression ratio
- App Version: 5.2.5
- App Website: https://tukaani.org/xz/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install xz with the following command
   ```
   brew install xz
   ```
4. xz is ready to use now!