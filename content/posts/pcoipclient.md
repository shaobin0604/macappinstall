---
title: "Install Teradici PCoIP Software Client for macOS on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Client for VM agents and remote workstation cards"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Teradici PCoIP Software Client for macOS on MacOS using homebrew

- App Name: Teradici PCoIP Software Client for macOS
- App description: Client for VM agents and remote workstation cards
- App Version: 21.03.0
- App Website: https://docs.teradici.com/find/product/software-and-mobile-clients/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Teradici PCoIP Software Client for macOS with the following command
   ```
   brew install --cask pcoipclient
   ```
4. Teradici PCoIP Software Client for macOS is ready to use now!