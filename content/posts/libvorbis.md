---
title: "Install libvorbis on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Vorbis General Audio Compression Codec"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libvorbis on MacOS using homebrew

- App Name: libvorbis
- App description: Vorbis General Audio Compression Codec
- App Version: 1.3.7
- App Website: https://xiph.org/vorbis/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libvorbis with the following command
   ```
   brew install libvorbis
   ```
4. libvorbis is ready to use now!