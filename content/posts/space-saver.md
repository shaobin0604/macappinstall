---
title: "Install Space Saver on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Delete local Time Machine backups"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Space Saver on MacOS using homebrew

- App Name: Space Saver
- App description: Delete local Time Machine backups
- App Version: 0.6
- App Website: https://www.mariogt.com/space-saver.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Space Saver with the following command
   ```
   brew install --cask space-saver
   ```
4. Space Saver is ready to use now!