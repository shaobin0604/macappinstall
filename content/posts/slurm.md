---
title: "Install slurm on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Yet another network load monitor"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install slurm on MacOS using homebrew

- App Name: slurm
- App description: Yet another network load monitor
- App Version: 0.4.3
- App Website: https://github.com/mattthias/slurm

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install slurm with the following command
   ```
   brew install slurm
   ```
4. slurm is ready to use now!