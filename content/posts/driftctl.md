---
title: "Install driftctl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Detect, track and alert on infrastructure drift"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install driftctl on MacOS using homebrew

- App Name: driftctl
- App description: Detect, track and alert on infrastructure drift
- App Version: 0.20.0
- App Website: https://driftctl.com

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install driftctl with the following command
   ```
   brew install driftctl
   ```
4. driftctl is ready to use now!