---
title: "Install precomp on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line precompressor to achieve better compression"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install precomp on MacOS using homebrew

- App Name: precomp
- App description: Command-line precompressor to achieve better compression
- App Version: 0.4.7
- App Website: http://schnaader.info/precomp.php

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install precomp with the following command
   ```
   brew install precomp
   ```
4. precomp is ready to use now!