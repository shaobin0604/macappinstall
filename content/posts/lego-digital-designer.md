---
title: "Install Lego Digital Designer on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Build models using virtual Lego bricks"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Lego Digital Designer on MacOS using homebrew

- App Name: Lego Digital Designer
- App description: Build models using virtual Lego bricks
- App Version: 4.3.11
- App Website: https://www.lego.com/en-us/ldd

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Lego Digital Designer with the following command
   ```
   brew install --cask lego-digital-designer
   ```
4. Lego Digital Designer is ready to use now!