---
title: "Install poster on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Create large posters out of PostScript pages"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install poster on MacOS using homebrew

- App Name: poster
- App description: Create large posters out of PostScript pages
- App Version: 1.0.0
- App Website: https://schrfr.github.io/poster/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install poster with the following command
   ```
   brew install poster
   ```
4. poster is ready to use now!