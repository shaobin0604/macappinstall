---
title: "Install Synology Note Station Client on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Write, view, manage and share content-rich notes"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Synology Note Station Client on MacOS using homebrew

- App Name: Synology Note Station Client
- App description: Write, view, manage and share content-rich notes
- App Version: 2.2.1-553
- App Website: https://www.synology.com/en-us/dsm/packages/NoteStation

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Synology Note Station Client with the following command
   ```
   brew install --cask synology-note-station-client
   ```
4. Synology Note Station Client is ready to use now!