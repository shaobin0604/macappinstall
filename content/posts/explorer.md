---
title: "Install Explorer on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Data Explorer"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Explorer on MacOS using homebrew

- App Name: Explorer
- App description: Data Explorer
- App Version: 1.104
- App Website: https://github.com/jfbouzereau/explorer

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Explorer with the following command
   ```
   brew install --cask explorer
   ```
4. Explorer is ready to use now!