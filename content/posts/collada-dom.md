---
title: "Install collada-dom on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C++ library for loading and saving COLLADA data"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install collada-dom on MacOS using homebrew

- App Name: collada-dom
- App description: C++ library for loading and saving COLLADA data
- App Version: 2.5.0
- App Website: https://www.khronos.org/collada/wiki/Portal:COLLADA_DOM

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install collada-dom with the following command
   ```
   brew install collada-dom
   ```
4. collada-dom is ready to use now!