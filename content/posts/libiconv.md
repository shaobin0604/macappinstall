---
title: "Install libiconv on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Conversion library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libiconv on MacOS using homebrew

- App Name: libiconv
- App description: Conversion library
- App Version: 1.16
- App Website: https://www.gnu.org/software/libiconv/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libiconv with the following command
   ```
   brew install libiconv
   ```
4. libiconv is ready to use now!