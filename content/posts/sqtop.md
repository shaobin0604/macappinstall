---
title: "Install sqtop on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Display information about active connections for a Squid proxy"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sqtop on MacOS using homebrew

- App Name: sqtop
- App description: Display information about active connections for a Squid proxy
- App Version: 2015-02-08
- App Website: https://github.com/paleg/sqtop

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sqtop with the following command
   ```
   brew install sqtop
   ```
4. sqtop is ready to use now!