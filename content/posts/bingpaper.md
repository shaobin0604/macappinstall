---
title: "Install BingPaper on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Use the Bing daily photo as your wallpaper"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install BingPaper on MacOS using homebrew

- App Name: BingPaper
- App description: Use the Bing daily photo as your wallpaper
- App Version: 0.11.1,46
- App Website: https://github.com/pengsrc/BingPaper

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install BingPaper with the following command
   ```
   brew install --cask bingpaper
   ```
4. BingPaper is ready to use now!