---
title: "Install KeyboardCleanTool on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Blocks all Keyboard and TouchBar input"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install KeyboardCleanTool on MacOS using homebrew

- App Name: KeyboardCleanTool
- App description: Blocks all Keyboard and TouchBar input
- App Version: 3
- App Website: https://folivora.ai/keyboardcleantool

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install KeyboardCleanTool with the following command
   ```
   brew install --cask keyboardcleantool
   ```
4. KeyboardCleanTool is ready to use now!