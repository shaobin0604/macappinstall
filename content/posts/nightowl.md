---
title: "Install NightOwl on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utility to toggle dark mode"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install NightOwl on MacOS using homebrew

- App Name: NightOwl
- App description: Utility to toggle dark mode
- App Version: 0.3.0
- App Website: https://nightowl.kramser.xyz/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install NightOwl with the following command
   ```
   brew install --cask nightowl
   ```
4. NightOwl is ready to use now!