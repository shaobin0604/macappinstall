---
title: "Install STACK on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Personal online hard drive to store, view and share files"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install STACK on MacOS using homebrew

- App Name: STACK
- App description: Personal online hard drive to store, view and share files
- App Version: 2.9.2-20211214
- App Website: https://www.transip.nl/stack

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install STACK with the following command
   ```
   brew install --cask stack
   ```
4. STACK is ready to use now!