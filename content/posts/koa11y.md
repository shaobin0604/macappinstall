---
title: "Install Koa11y on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Easily check for website accessibility issues"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Koa11y on MacOS using homebrew

- App Name: Koa11y
- App description: Easily check for website accessibility issues
- App Version: 3.0.0
- App Website: https://open-indy.github.io/Koa11y/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Koa11y with the following command
   ```
   brew install --cask koa11y
   ```
4. Koa11y is ready to use now!