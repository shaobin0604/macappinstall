---
title: "Install dnsviz on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tools for analyzing and visualizing DNS and DNSSEC behavior"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dnsviz on MacOS using homebrew

- App Name: dnsviz
- App description: Tools for analyzing and visualizing DNS and DNSSEC behavior
- App Version: 0.9.4
- App Website: https://github.com/dnsviz/dnsviz/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dnsviz with the following command
   ```
   brew install dnsviz
   ```
4. dnsviz is ready to use now!