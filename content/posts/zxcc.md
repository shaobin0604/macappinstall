---
title: "Install zxcc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CP/M 2/3 emulator for cross-compiling and CP/M tools under UNIX"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install zxcc on MacOS using homebrew

- App Name: zxcc
- App description: CP/M 2/3 emulator for cross-compiling and CP/M tools under UNIX
- App Version: 0.5.7
- App Website: https://www.seasip.info/Unix/Zxcc/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install zxcc with the following command
   ```
   brew install zxcc
   ```
4. zxcc is ready to use now!