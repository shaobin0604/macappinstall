---
title: "Install sleek on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Todo app based on todo.txt"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sleek on MacOS using homebrew

- App Name: sleek
- App description: Todo app based on todo.txt
- App Version: 1.1.6
- App Website: https://github.com/ransome1/sleek

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sleek with the following command
   ```
   brew install --cask sleek
   ```
4. sleek is ready to use now!