---
title: "Install pax-runner on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to provision OSGi bundles"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pax-runner on MacOS using homebrew

- App Name: pax-runner
- App description: Tool to provision OSGi bundles
- App Version: 1.9.0
- App Website: https://ops4j1.jira.com/wiki/spaces/paxrunner/overview

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pax-runner with the following command
   ```
   brew install pax-runner
   ```
4. pax-runner is ready to use now!