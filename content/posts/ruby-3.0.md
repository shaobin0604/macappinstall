---
title: "Install ruby@3.0 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Powerful, clean, object-oriented scripting language"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ruby@3.0 on MacOS using homebrew

- App Name: ruby@3.0
- App description: Powerful, clean, object-oriented scripting language
- App Version: 3.0.3
- App Website: https://www.ruby-lang.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ruby@3.0 with the following command
   ```
   brew install ruby@3.0
   ```
4. ruby@3.0 is ready to use now!