---
title: "Install DeltaChat on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Chat via the e-mail server network"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install DeltaChat on MacOS using homebrew

- App Name: DeltaChat
- App description: Chat via the e-mail server network
- App Version: 1.26.0
- App Website: https://delta.chat/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install DeltaChat with the following command
   ```
   brew install --cask deltachat
   ```
4. DeltaChat is ready to use now!