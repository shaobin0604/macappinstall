---
title: "Install TigerVNC on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Multi-platform VNC client and server"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install TigerVNC on MacOS using homebrew

- App Name: TigerVNC
- App description: Multi-platform VNC client and server
- App Version: 1.12.0
- App Website: https://tigervnc.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install TigerVNC with the following command
   ```
   brew install --cask tigervnc-viewer
   ```
4. TigerVNC is ready to use now!