---
title: "Install gdbgui on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Modern, browser-based frontend to gdb (gnu debugger)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gdbgui on MacOS using homebrew

- App Name: gdbgui
- App description: Modern, browser-based frontend to gdb (gnu debugger)
- App Version: 0.15.0.1
- App Website: https://www.gdbgui.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gdbgui with the following command
   ```
   brew install gdbgui
   ```
4. gdbgui is ready to use now!