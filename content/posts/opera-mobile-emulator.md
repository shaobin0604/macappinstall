---
title: "Install Opera Mobile Classic Emulator on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Opera Mobile Classic Emulator on MacOS using homebrew

- App Name: Opera Mobile Classic Emulator
- App description: null
- App Version: 12.1
- App Website: https://www.opera.com/developer/mobile-emulator

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Opera Mobile Classic Emulator with the following command
   ```
   brew install --cask opera-mobile-emulator
   ```
4. Opera Mobile Classic Emulator is ready to use now!