---
title: "Install ronn on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Builds manuals - the opposite of roff"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ronn on MacOS using homebrew

- App Name: ronn
- App description: Builds manuals - the opposite of roff
- App Version: 0.7.3
- App Website: https://rtomayko.github.io/ronn/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ronn with the following command
   ```
   brew install ronn
   ```
4. ronn is ready to use now!