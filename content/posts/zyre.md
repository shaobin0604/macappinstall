---
title: "Install zyre on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Local Area Clustering for Peer-to-Peer Applications"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install zyre on MacOS using homebrew

- App Name: zyre
- App description: Local Area Clustering for Peer-to-Peer Applications
- App Version: 2.0.1
- App Website: https://github.com/zeromq/zyre

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install zyre with the following command
   ```
   brew install zyre
   ```
4. zyre is ready to use now!