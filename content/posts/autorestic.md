---
title: "Install autorestic on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "High level CLI utility for restic"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install autorestic on MacOS using homebrew

- App Name: autorestic
- App description: High level CLI utility for restic
- App Version: 1.5.5
- App Website: https://autorestic.vercel.app/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install autorestic with the following command
   ```
   brew install autorestic
   ```
4. autorestic is ready to use now!