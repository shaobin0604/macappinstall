---
title: "Install libagg on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "High fidelity 2D graphics library for C++"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libagg on MacOS using homebrew

- App Name: libagg
- App description: High fidelity 2D graphics library for C++
- App Version: 2.5
- App Website: https://antigrain.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libagg with the following command
   ```
   brew install libagg
   ```
4. libagg is ready to use now!