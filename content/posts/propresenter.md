---
title: "Install ProPresenter on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Presentation and production application for live events"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ProPresenter on MacOS using homebrew

- App Name: ProPresenter
- App description: Presentation and production application for live events
- App Version: 7.8,117964825
- App Website: https://www.renewedvision.com/propresenter.php

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ProPresenter with the following command
   ```
   brew install --cask propresenter
   ```
4. ProPresenter is ready to use now!