---
title: "Install MacGameStore on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Buy, download, and play your games"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install MacGameStore on MacOS using homebrew

- App Name: MacGameStore
- App description: Buy, download, and play your games
- App Version: 4.3.4,6084
- App Website: https://www.macgamestore.com/app/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install MacGameStore with the following command
   ```
   brew install --cask macgamestore
   ```
4. MacGameStore is ready to use now!