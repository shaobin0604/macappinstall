---
title: "Install quill on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C++14 Asynchronous Low Latency Logging Library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install quill on MacOS using homebrew

- App Name: quill
- App description: C++14 Asynchronous Low Latency Logging Library
- App Version: 1.6.3
- App Website: https://github.com/odygrd/quill

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install quill with the following command
   ```
   brew install quill
   ```
4. quill is ready to use now!