---
title: "Install libsoundio on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cross-platform audio input and output"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libsoundio on MacOS using homebrew

- App Name: libsoundio
- App description: Cross-platform audio input and output
- App Version: 2.0.0
- App Website: http://libsound.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libsoundio with the following command
   ```
   brew install libsoundio
   ```
4. libsoundio is ready to use now!