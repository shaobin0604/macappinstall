---
title: "Install Unclutter on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Desktop storage area for notes, files and pasteboard clips"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Unclutter on MacOS using homebrew

- App Name: Unclutter
- App description: Desktop storage area for notes, files and pasteboard clips
- App Version: 2.1.24d,2.1.2400
- App Website: https://unclutterapp.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Unclutter with the following command
   ```
   brew install --cask unclutter
   ```
4. Unclutter is ready to use now!