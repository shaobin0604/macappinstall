---
title: "Install vsearch on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Versatile open-source tool for microbiome analysis"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install vsearch on MacOS using homebrew

- App Name: vsearch
- App description: Versatile open-source tool for microbiome analysis
- App Version: 2.21.1
- App Website: https://github.com/torognes/vsearch

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install vsearch with the following command
   ```
   brew install vsearch
   ```
4. vsearch is ready to use now!