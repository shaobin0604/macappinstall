---
title: "Install baudline on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Time-frequency browser"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install baudline on MacOS using homebrew

- App Name: baudline
- App description: Time-frequency browser
- App Version: 1.08
- App Website: https://www.baudline.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install baudline with the following command
   ```
   brew install --cask baudline
   ```
4. baudline is ready to use now!