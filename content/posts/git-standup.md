---
title: "Install git-standup on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Git extension to generate reports for standup meetings"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install git-standup on MacOS using homebrew

- App Name: git-standup
- App description: Git extension to generate reports for standup meetings
- App Version: 2.3.2
- App Website: https://github.com/kamranahmedse/git-standup

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install git-standup with the following command
   ```
   brew install git-standup
   ```
4. git-standup is ready to use now!