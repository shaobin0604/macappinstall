---
title: "Install denominator on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Portable Java library for manipulating DNS clouds"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install denominator on MacOS using homebrew

- App Name: denominator
- App description: Portable Java library for manipulating DNS clouds
- App Version: 4.7.1
- App Website: https://github.com/Netflix/denominator/tree/v4.7.1/cli

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install denominator with the following command
   ```
   brew install denominator
   ```
4. denominator is ready to use now!