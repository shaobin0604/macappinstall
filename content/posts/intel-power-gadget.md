---
title: "Install Intel Power Gadget on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Power usage monitoring tool enabled for Intel Core processors"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Intel Power Gadget on MacOS using homebrew

- App Name: Intel Power Gadget
- App description: Power usage monitoring tool enabled for Intel Core processors
- App Version: 3.7.0,b7b1b3e1dffd9b20
- App Website: https://software.intel.com/content/www/us/en/develop/articles/intel-power-gadget.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Intel Power Gadget with the following command
   ```
   brew install --cask intel-power-gadget
   ```
4. Intel Power Gadget is ready to use now!