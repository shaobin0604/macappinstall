---
title: "Install sersniff on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Program to tunnel/sniff between 2 serial ports"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sersniff on MacOS using homebrew

- App Name: sersniff
- App description: Program to tunnel/sniff between 2 serial ports
- App Version: 0.0.5
- App Website: https://www.earth.li/projectpurple/progs/sersniff.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sersniff with the following command
   ```
   brew install sersniff
   ```
4. sersniff is ready to use now!