---
title: "Install tengo on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Fast script language for Go"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tengo on MacOS using homebrew

- App Name: tengo
- App description: Fast script language for Go
- App Version: 2.10.1
- App Website: https://tengolang.com

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tengo with the following command
   ```
   brew install tengo
   ```
4. tengo is ready to use now!