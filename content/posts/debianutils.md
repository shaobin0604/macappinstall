---
title: "Install debianutils on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Miscellaneous utilities specific to Debian"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install debianutils on MacOS using homebrew

- App Name: debianutils
- App description: Miscellaneous utilities specific to Debian
- App Version: 4.11.2
- App Website: https://packages.debian.org/sid/debianutils

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install debianutils with the following command
   ```
   brew install debianutils
   ```
4. debianutils is ready to use now!