---
title: "Install dmenu-mac on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Keyboard-only application launcher"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dmenu-mac on MacOS using homebrew

- App Name: dmenu-mac
- App description: Keyboard-only application launcher
- App Version: 0.7.2
- App Website: https://github.com/oNaiPs/dmenu-mac

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dmenu-mac with the following command
   ```
   brew install --cask dmenu-mac
   ```
4. dmenu-mac is ready to use now!