---
title: "Install cargo-outdated on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cargo subcommand for displaying when Rust dependencies are out of date"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cargo-outdated on MacOS using homebrew

- App Name: cargo-outdated
- App description: Cargo subcommand for displaying when Rust dependencies are out of date
- App Version: 0.10.2
- App Website: https://github.com/kbknapp/cargo-outdated

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cargo-outdated with the following command
   ```
   brew install cargo-outdated
   ```
4. cargo-outdated is ready to use now!