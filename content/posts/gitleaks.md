---
title: "Install gitleaks on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Audit git repos for secrets"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gitleaks on MacOS using homebrew

- App Name: gitleaks
- App description: Audit git repos for secrets
- App Version: 8.2.7
- App Website: https://github.com/zricethezav/gitleaks

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gitleaks with the following command
   ```
   brew install gitleaks
   ```
4. gitleaks is ready to use now!