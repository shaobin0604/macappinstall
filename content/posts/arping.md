---
title: "Install arping on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utility to check whether MAC addresses are already taken on a LAN"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install arping on MacOS using homebrew

- App Name: arping
- App description: Utility to check whether MAC addresses are already taken on a LAN
- App Version: 2.22
- App Website: https://github.com/ThomasHabets/arping

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install arping with the following command
   ```
   brew install arping
   ```
4. arping is ready to use now!