---
title: "Install Whale on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Unofficial Trello app :whale:"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Whale on MacOS using homebrew

- App Name: Whale
- App description: Unofficial Trello app :whale:
- App Version: 2.4.0
- App Website: https://github.com/1000ch/whale

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Whale with the following command
   ```
   brew install --cask whale
   ```
4. Whale is ready to use now!