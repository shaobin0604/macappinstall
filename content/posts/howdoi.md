---
title: "Install howdoi on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Instant coding answers via the command-line"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install howdoi on MacOS using homebrew

- App Name: howdoi
- App description: Instant coding answers via the command-line
- App Version: 2.0.19
- App Website: https://github.com/gleitz/howdoi

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install howdoi with the following command
   ```
   brew install howdoi
   ```
4. howdoi is ready to use now!