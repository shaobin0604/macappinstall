---
title: "Install uchardet on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Encoding detector library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install uchardet on MacOS using homebrew

- App Name: uchardet
- App description: Encoding detector library
- App Version: 0.0.7
- App Website: https://www.freedesktop.org/wiki/Software/uchardet/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install uchardet with the following command
   ```
   brew install uchardet
   ```
4. uchardet is ready to use now!