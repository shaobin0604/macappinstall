---
title: "Install GarageSale on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Manage eBay Listings"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install GarageSale on MacOS using homebrew

- App Name: GarageSale
- App description: Manage eBay Listings
- App Version: 8.4.1
- App Website: https://www.iwascoding.com/GarageSale/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install GarageSale with the following command
   ```
   brew install --cask garagesale
   ```
4. GarageSale is ready to use now!