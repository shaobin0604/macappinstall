---
title: "Install Airfoil on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Sends audio from computer to outputs"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Airfoil on MacOS using homebrew

- App Name: Airfoil
- App description: Sends audio from computer to outputs
- App Version: 5.10.7
- App Website: https://www.rogueamoeba.com/airfoil/mac/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Airfoil with the following command
   ```
   brew install --cask airfoil
   ```
4. Airfoil is ready to use now!