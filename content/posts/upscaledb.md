---
title: "Install upscaledb on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Database for embedded devices"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install upscaledb on MacOS using homebrew

- App Name: upscaledb
- App description: Database for embedded devices
- App Version: 2.2.1
- App Website: https://upscaledb.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install upscaledb with the following command
   ```
   brew install upscaledb
   ```
4. upscaledb is ready to use now!