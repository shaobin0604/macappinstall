---
title: "Install protobuf-c on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Protocol buffers library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install protobuf-c on MacOS using homebrew

- App Name: protobuf-c
- App description: Protocol buffers library
- App Version: 1.4.0
- App Website: https://github.com/protobuf-c/protobuf-c

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install protobuf-c with the following command
   ```
   brew install protobuf-c
   ```
4. protobuf-c is ready to use now!