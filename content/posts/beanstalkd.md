---
title: "Install beanstalkd on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Generic work queue originally designed to reduce web latency"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install beanstalkd on MacOS using homebrew

- App Name: beanstalkd
- App description: Generic work queue originally designed to reduce web latency
- App Version: 1.12
- App Website: https://beanstalkd.github.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install beanstalkd with the following command
   ```
   brew install beanstalkd
   ```
4. beanstalkd is ready to use now!