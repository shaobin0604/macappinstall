---
title: "Install pandoc-plot on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Render and include figures in Pandoc documents using many plotting toolkits"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pandoc-plot on MacOS using homebrew

- App Name: pandoc-plot
- App description: Render and include figures in Pandoc documents using many plotting toolkits
- App Version: 1.4.0
- App Website: https://github.com/LaurentRDC/pandoc-plot

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pandoc-plot with the following command
   ```
   brew install pandoc-plot
   ```
4. pandoc-plot is ready to use now!