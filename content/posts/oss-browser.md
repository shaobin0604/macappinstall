---
title: "Install oss-browser on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Graphical management tool for OSS (Object Storage Service)"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install oss-browser on MacOS using homebrew

- App Name: oss-browser
- App description: Graphical management tool for OSS (Object Storage Service)
- App Version: 1.16.0
- App Website: https://github.com/aliyun/oss-browser/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install oss-browser with the following command
   ```
   brew install --cask oss-browser
   ```
4. oss-browser is ready to use now!