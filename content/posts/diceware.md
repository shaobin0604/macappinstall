---
title: "Install diceware on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Passphrases to remember"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install diceware on MacOS using homebrew

- App Name: diceware
- App description: Passphrases to remember
- App Version: 0.10
- App Website: https://github.com/ulif/diceware

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install diceware with the following command
   ```
   brew install diceware
   ```
4. diceware is ready to use now!