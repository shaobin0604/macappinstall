---
title: "Install Tabula on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool for liberating data tables trapped inside PDF files"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Tabula on MacOS using homebrew

- App Name: Tabula
- App description: Tool for liberating data tables trapped inside PDF files
- App Version: 1.2.1
- App Website: https://tabula.technology/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Tabula with the following command
   ```
   brew install --cask tabula
   ```
4. Tabula is ready to use now!