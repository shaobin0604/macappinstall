---
title: "Install CodeRunner on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Multi-language programming editor"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install CodeRunner on MacOS using homebrew

- App Name: CodeRunner
- App description: Multi-language programming editor
- App Version: 4.1,62956
- App Website: https://coderunnerapp.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install CodeRunner with the following command
   ```
   brew install --cask coderunner
   ```
4. CodeRunner is ready to use now!