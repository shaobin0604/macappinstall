---
title: "Install resty on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line REST client that can be used in pipelines"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install resty on MacOS using homebrew

- App Name: resty
- App description: Command-line REST client that can be used in pipelines
- App Version: 3.0
- App Website: https://github.com/micha/resty

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install resty with the following command
   ```
   brew install resty
   ```
4. resty is ready to use now!