---
title: "Install byacc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "(Arguably) the best yacc variant"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install byacc on MacOS using homebrew

- App Name: byacc
- App description: (Arguably) the best yacc variant
- App Version: 20220128
- App Website: https://invisible-island.net/byacc/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install byacc with the following command
   ```
   brew install byacc
   ```
4. byacc is ready to use now!