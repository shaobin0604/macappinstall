---
title: "Install beaTunes on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Analyze, inspect, and play songs"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install beaTunes on MacOS using homebrew

- App Name: beaTunes
- App description: Analyze, inspect, and play songs
- App Version: 5.2.26
- App Website: https://www.beatunes.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install beaTunes with the following command
   ```
   brew install --cask beatunes
   ```
4. beaTunes is ready to use now!