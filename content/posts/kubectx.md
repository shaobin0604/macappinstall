---
title: "Install kubectx on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool that can switch between kubectl contexts easily and create aliases"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install kubectx on MacOS using homebrew

- App Name: kubectx
- App description: Tool that can switch between kubectl contexts easily and create aliases
- App Version: 0.9.4
- App Website: https://github.com/ahmetb/kubectx

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install kubectx with the following command
   ```
   brew install kubectx
   ```
4. kubectx is ready to use now!