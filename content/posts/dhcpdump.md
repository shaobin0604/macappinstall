---
title: "Install dhcpdump on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Monitor DHCP traffic for debugging purposes"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dhcpdump on MacOS using homebrew

- App Name: dhcpdump
- App description: Monitor DHCP traffic for debugging purposes
- App Version: 1.8
- App Website: https://www.mavetju.org/unix/general.php

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dhcpdump with the following command
   ```
   brew install dhcpdump
   ```
4. dhcpdump is ready to use now!