---
title: "Install brew-gem on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Install RubyGems as Homebrew formulae"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install brew-gem on MacOS using homebrew

- App Name: brew-gem
- App description: Install RubyGems as Homebrew formulae
- App Version: 1.1.1
- App Website: https://github.com/sportngin/brew-gem

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install brew-gem with the following command
   ```
   brew install brew-gem
   ```
4. brew-gem is ready to use now!