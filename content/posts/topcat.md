---
title: "Install TOPCAT on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Interactive graphical viewer and editor for tabular data"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install TOPCAT on MacOS using homebrew

- App Name: TOPCAT
- App description: Interactive graphical viewer and editor for tabular data
- App Version: 4.8-3
- App Website: http://www.star.bris.ac.uk/~mbt/topcat/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install TOPCAT with the following command
   ```
   brew install --cask topcat
   ```
4. TOPCAT is ready to use now!