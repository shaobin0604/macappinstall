---
title: "Install apm-bash-completion on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Completion for Atom Package Manager"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install apm-bash-completion on MacOS using homebrew

- App Name: apm-bash-completion
- App description: Completion for Atom Package Manager
- App Version: 1.0.0
- App Website: https://github.com/vigo/apm-bash-completion

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install apm-bash-completion with the following command
   ```
   brew install apm-bash-completion
   ```
4. apm-bash-completion is ready to use now!