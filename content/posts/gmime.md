---
title: "Install gmime on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "MIME mail utilities"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gmime on MacOS using homebrew

- App Name: gmime
- App description: MIME mail utilities
- App Version: 3.2.7
- App Website: https://spruce.sourceforge.io/gmime/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gmime with the following command
   ```
   brew install gmime
   ```
4. gmime is ready to use now!