---
title: "Install MailTrackerBlocker on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Email tracker, read receipt and spy pixel blocker plugin for Apple Mail"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install MailTrackerBlocker on MacOS using homebrew

- App Name: MailTrackerBlocker
- App description: Email tracker, read receipt and spy pixel blocker plugin for Apple Mail
- App Version: 0.6.0
- App Website: https://apparition47.github.io/MailTrackerBlocker/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install MailTrackerBlocker with the following command
   ```
   brew install --cask mailtrackerblocker
   ```
4. MailTrackerBlocker is ready to use now!