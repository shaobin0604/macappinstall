---
title: "Install dosbox on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "DOS Emulator"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dosbox on MacOS using homebrew

- App Name: dosbox
- App description: DOS Emulator
- App Version: 0.74-3
- App Website: https://www.dosbox.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dosbox with the following command
   ```
   brew install dosbox
   ```
4. dosbox is ready to use now!