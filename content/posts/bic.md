---
title: "Install bic on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C interpreter and API explorer"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bic on MacOS using homebrew

- App Name: bic
- App description: C interpreter and API explorer
- App Version: 1.0.0
- App Website: https://github.com/hexagonal-sun/bic

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bic with the following command
   ```
   brew install bic
   ```
4. bic is ready to use now!