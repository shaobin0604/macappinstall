---
title: "Install ser2net on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Allow network connections to serial ports"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ser2net on MacOS using homebrew

- App Name: ser2net
- App description: Allow network connections to serial ports
- App Version: 4.3.5
- App Website: https://ser2net.sourceforge.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ser2net with the following command
   ```
   brew install ser2net
   ```
4. ser2net is ready to use now!