---
title: "Install Resilio Sync on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "File sync and share software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Resilio Sync on MacOS using homebrew

- App Name: Resilio Sync
- App description: File sync and share software
- App Version: 2.7.2
- App Website: https://www.resilio.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Resilio Sync with the following command
   ```
   brew install --cask resilio-sync
   ```
4. Resilio Sync is ready to use now!