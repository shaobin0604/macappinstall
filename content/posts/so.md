---
title: "Install so on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Terminal interface for StackOverflow"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install so on MacOS using homebrew

- App Name: so
- App description: Terminal interface for StackOverflow
- App Version: 0.4.5
- App Website: https://github.com/samtay/so

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install so with the following command
   ```
   brew install so
   ```
4. so is ready to use now!