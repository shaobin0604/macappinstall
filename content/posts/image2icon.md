---
title: "Install Image2Icon on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Icon creator and file and folder customizer"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Image2Icon on MacOS using homebrew

- App Name: Image2Icon
- App description: Icon creator and file and folder customizer
- App Version: 2.16,930
- App Website: https://www.img2icnsapp.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Image2Icon with the following command
   ```
   brew install --cask image2icon
   ```
4. Image2Icon is ready to use now!