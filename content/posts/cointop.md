---
title: "Install cointop on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Interactive terminal based UI application for tracking cryptocurrencies"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cointop on MacOS using homebrew

- App Name: cointop
- App description: Interactive terminal based UI application for tracking cryptocurrencies
- App Version: 1.6.10
- App Website: https://cointop.sh

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cointop with the following command
   ```
   brew install cointop
   ```
4. cointop is ready to use now!