---
title: "Install dosbox-staging on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Modernized DOSBox soft-fork"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dosbox-staging on MacOS using homebrew

- App Name: dosbox-staging
- App description: Modernized DOSBox soft-fork
- App Version: 0.78.1
- App Website: https://dosbox-staging.github.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dosbox-staging with the following command
   ```
   brew install dosbox-staging
   ```
4. dosbox-staging is ready to use now!