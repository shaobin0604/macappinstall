---
title: "Install Spyder on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Scientific Python IDE"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Spyder on MacOS using homebrew

- App Name: Spyder
- App description: Scientific Python IDE
- App Version: 5.2.2
- App Website: https://www.spyder-ide.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Spyder with the following command
   ```
   brew install --cask spyder
   ```
4. Spyder is ready to use now!