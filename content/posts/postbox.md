---
title: "Install Postbox on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Email client focusing on privacy protection"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Postbox on MacOS using homebrew

- App Name: Postbox
- App description: Email client focusing on privacy protection
- App Version: 7.0.54
- App Website: https://www.postbox-inc.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Postbox with the following command
   ```
   brew install --cask postbox
   ```
4. Postbox is ready to use now!