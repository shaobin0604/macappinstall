---
title: "Install gistit on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line utility for creating Gists"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gistit on MacOS using homebrew

- App Name: gistit
- App description: Command-line utility for creating Gists
- App Version: 0.1.4
- App Website: https://gistit.herokuapp.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gistit with the following command
   ```
   brew install gistit
   ```
4. gistit is ready to use now!