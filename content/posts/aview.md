---
title: "Install aview on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "ASCII-art image browser and animation viewer"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install aview on MacOS using homebrew

- App Name: aview
- App description: ASCII-art image browser and animation viewer
- App Version: 1.3.0rc1
- App Website: https://aa-project.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install aview with the following command
   ```
   brew install aview
   ```
4. aview is ready to use now!