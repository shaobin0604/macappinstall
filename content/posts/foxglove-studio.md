---
title: "Install Foxglove Studio on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Visualization and debugging tool for robotics"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Foxglove Studio on MacOS using homebrew

- App Name: Foxglove Studio
- App description: Visualization and debugging tool for robotics
- App Version: 0.29.0
- App Website: https://foxglove.dev/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Foxglove Studio with the following command
   ```
   brew install --cask foxglove-studio
   ```
4. Foxglove Studio is ready to use now!