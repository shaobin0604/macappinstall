---
title: "Install normalize on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Adjust volume of audio files to a standard level"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install normalize on MacOS using homebrew

- App Name: normalize
- App description: Adjust volume of audio files to a standard level
- App Version: 0.7.7
- App Website: https://www.nongnu.org/normalize/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install normalize with the following command
   ```
   brew install normalize
   ```
4. normalize is ready to use now!