---
title: "Install Electorrent on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Desktop remote torrenting application"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Electorrent on MacOS using homebrew

- App Name: Electorrent
- App description: Desktop remote torrenting application
- App Version: 2.7.3
- App Website: https://github.com/tympanix/Electorrent

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Electorrent with the following command
   ```
   brew install --cask electorrent
   ```
4. Electorrent is ready to use now!