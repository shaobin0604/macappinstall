---
title: "Install re2 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Alternative to backtracking PCRE-style regular expression engines"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install re2 on MacOS using homebrew

- App Name: re2
- App description: Alternative to backtracking PCRE-style regular expression engines
- App Version: 20211101
- App Website: https://github.com/google/re2

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install re2 with the following command
   ```
   brew install re2
   ```
4. re2 is ready to use now!