---
title: "Install Leech on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Lightweight download manager"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Leech on MacOS using homebrew

- App Name: Leech
- App description: Lightweight download manager
- App Version: 3.1.6,3157
- App Website: https://manytricks.com/leech/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Leech with the following command
   ```
   brew install --cask leech
   ```
4. Leech is ready to use now!