---
title: "Install exim on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Complete replacement for sendmail"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install exim on MacOS using homebrew

- App Name: exim
- App description: Complete replacement for sendmail
- App Version: 4.95
- App Website: https://exim.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install exim with the following command
   ```
   brew install exim
   ```
4. exim is ready to use now!