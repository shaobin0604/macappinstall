---
title: "Install gsmartcontrol on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Graphical user interface for smartctl"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gsmartcontrol on MacOS using homebrew

- App Name: gsmartcontrol
- App description: Graphical user interface for smartctl
- App Version: 1.1.3
- App Website: https://gsmartcontrol.sourceforge.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gsmartcontrol with the following command
   ```
   brew install gsmartcontrol
   ```
4. gsmartcontrol is ready to use now!