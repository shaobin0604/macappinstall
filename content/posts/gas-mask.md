---
title: "Install Gas Mask on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Hosts file editor/manager"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Gas Mask on MacOS using homebrew

- App Name: Gas Mask
- App description: Hosts file editor/manager
- App Version: 0.8.6
- App Website: http://clockwise.ee/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Gas Mask with the following command
   ```
   brew install --cask gas-mask
   ```
4. Gas Mask is ready to use now!