---
title: "Install adns on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C/C++ resolver library and DNS resolver utilities"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install adns on MacOS using homebrew

- App Name: adns
- App description: C/C++ resolver library and DNS resolver utilities
- App Version: 1.6.0
- App Website: https://www.chiark.greenend.org.uk/~ian/adns/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install adns with the following command
   ```
   brew install adns
   ```
4. adns is ready to use now!