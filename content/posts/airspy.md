---
title: "Install airspy on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Driver and tools for a software-defined radio"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install airspy on MacOS using homebrew

- App Name: airspy
- App description: Driver and tools for a software-defined radio
- App Version: 1.0.10
- App Website: https://airspy.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install airspy with the following command
   ```
   brew install airspy
   ```
4. airspy is ready to use now!