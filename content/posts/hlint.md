---
title: "Install hlint on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Haskell source code suggestions"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install hlint on MacOS using homebrew

- App Name: hlint
- App description: Haskell source code suggestions
- App Version: 3.3.6
- App Website: https://github.com/ndmitchell/hlint

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install hlint with the following command
   ```
   brew install hlint
   ```
4. hlint is ready to use now!