---
title: "Install nzbget on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Binary newsgrabber for nzb files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install nzbget on MacOS using homebrew

- App Name: nzbget
- App description: Binary newsgrabber for nzb files
- App Version: 21.1
- App Website: https://nzbget.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install nzbget with the following command
   ```
   brew install nzbget
   ```
4. nzbget is ready to use now!