---
title: "Install Tiger Trade on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Trading platform"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Tiger Trade on MacOS using homebrew

- App Name: Tiger Trade
- App description: Trading platform
- App Version: 7.9.3,20220131,14049E
- App Website: https://www.tigerbrokers.com.sg/download/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Tiger Trade with the following command
   ```
   brew install --cask tiger-trade
   ```
4. Tiger Trade is ready to use now!