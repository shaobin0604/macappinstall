---
title: "Install gtkmm on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C++ interfaces for GTK+ and GNOME"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gtkmm on MacOS using homebrew

- App Name: gtkmm
- App description: C++ interfaces for GTK+ and GNOME
- App Version: 2.24.5
- App Website: https://www.gtkmm.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gtkmm with the following command
   ```
   brew install gtkmm
   ```
4. gtkmm is ready to use now!