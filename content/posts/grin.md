---
title: "Install grin on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Minimal implementation of the Mimblewimble protocol"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install grin on MacOS using homebrew

- App Name: grin
- App description: Minimal implementation of the Mimblewimble protocol
- App Version: 5.1.1
- App Website: https://grin.mw/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install grin with the following command
   ```
   brew install grin
   ```
4. grin is ready to use now!