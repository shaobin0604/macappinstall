---
title: "Install nsq on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Realtime distributed messaging platform"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install nsq on MacOS using homebrew

- App Name: nsq
- App description: Realtime distributed messaging platform
- App Version: 1.2.1
- App Website: https://nsq.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install nsq with the following command
   ```
   brew install nsq
   ```
4. nsq is ready to use now!