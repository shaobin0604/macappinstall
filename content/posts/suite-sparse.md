---
title: "Install suite-sparse on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Suite of Sparse Matrix Software"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install suite-sparse on MacOS using homebrew

- App Name: suite-sparse
- App description: Suite of Sparse Matrix Software
- App Version: 5.10.1
- App Website: https://people.engr.tamu.edu/davis/suitesparse.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install suite-sparse with the following command
   ```
   brew install suite-sparse
   ```
4. suite-sparse is ready to use now!