---
title: "Install ptex on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Texture mapping system"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ptex on MacOS using homebrew

- App Name: ptex
- App description: Texture mapping system
- App Version: 2.4.1
- App Website: https://ptex.us/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ptex with the following command
   ```
   brew install ptex
   ```
4. ptex is ready to use now!