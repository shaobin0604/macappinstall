---
title: "Install cig on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CLI app for checking the state of your git repositories"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cig on MacOS using homebrew

- App Name: cig
- App description: CLI app for checking the state of your git repositories
- App Version: 0.1.5
- App Website: https://github.com/stevenjack/cig

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cig with the following command
   ```
   brew install cig
   ```
4. cig is ready to use now!