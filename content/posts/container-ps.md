---
title: "Install Container PS on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "App to show all docker images"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Container PS on MacOS using homebrew

- App Name: Container PS
- App description: App to show all docker images
- App Version: 1.3.0
- App Website: https://github.com/Toinane/container-ps

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Container PS with the following command
   ```
   brew install --cask container-ps
   ```
4. Container PS is ready to use now!