---
title: "Install Privileges on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Admin rights switcher"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Privileges on MacOS using homebrew

- App Name: Privileges
- App description: Admin rights switcher
- App Version: 1.5.2
- App Website: https://github.com/SAP/macOS-enterprise-privileges

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Privileges with the following command
   ```
   brew install --cask privileges
   ```
4. Privileges is ready to use now!