---
title: "Install Deeper on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to enable and disable hidden functions of Finder and other apps"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Deeper on MacOS using homebrew

- App Name: Deeper
- App description: Tool to enable and disable hidden functions of Finder and other apps
- App Version: 2.7.0
- App Website: https://www.titanium-software.fr/en/deeper.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Deeper with the following command
   ```
   brew install --cask deeper
   ```
4. Deeper is ready to use now!