---
title: "Install sqlcl on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Oracle SQLcl is the modern command-line interface for the Oracle Database"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sqlcl on MacOS using homebrew

- App Name: sqlcl
- App description: Oracle SQLcl is the modern command-line interface for the Oracle Database
- App Version: 21.4.0.348.1716
- App Website: https://www.oracle.com/sqlcl

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sqlcl with the following command
   ```
   brew install --cask sqlcl
   ```
4. sqlcl is ready to use now!