---
title: "Install scs on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Conic optimization via operator splitting"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install scs on MacOS using homebrew

- App Name: scs
- App description: Conic optimization via operator splitting
- App Version: 3.1.0
- App Website: https://web.stanford.edu/~boyd/papers/scs.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install scs with the following command
   ```
   brew install scs
   ```
4. scs is ready to use now!