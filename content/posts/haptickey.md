---
title: "Install HapticKey on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Trigger haptic feedback when tapping Touch Bar"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install HapticKey on MacOS using homebrew

- App Name: HapticKey
- App description: Trigger haptic feedback when tapping Touch Bar
- App Version: 0.7.0
- App Website: https://github.com/niw/HapticKey

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install HapticKey with the following command
   ```
   brew install --cask haptickey
   ```
4. HapticKey is ready to use now!