---
title: "Install couchdb on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Apache CouchDB database server"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install couchdb on MacOS using homebrew

- App Name: couchdb
- App description: Apache CouchDB database server
- App Version: 3.2.1
- App Website: https://couchdb.apache.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install couchdb with the following command
   ```
   brew install couchdb
   ```
4. couchdb is ready to use now!