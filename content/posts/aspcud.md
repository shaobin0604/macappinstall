---
title: "Install aspcud on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Package dependency solver"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install aspcud on MacOS using homebrew

- App Name: aspcud
- App description: Package dependency solver
- App Version: 1.9.5
- App Website: https://potassco.org/aspcud/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install aspcud with the following command
   ```
   brew install aspcud
   ```
4. aspcud is ready to use now!