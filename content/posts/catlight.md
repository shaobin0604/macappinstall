---
title: "Install catlight on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Action center for developers"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install catlight on MacOS using homebrew

- App Name: catlight
- App description: Action center for developers
- App Version: 2.35.1
- App Website: https://catlight.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install catlight with the following command
   ```
   brew install --cask catlight
   ```
4. catlight is ready to use now!