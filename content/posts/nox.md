---
title: "Install nox on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Flexible test automation for Python"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install nox on MacOS using homebrew

- App Name: nox
- App description: Flexible test automation for Python
- App Version: 2022.1.7
- App Website: https://nox.thea.codes/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install nox with the following command
   ```
   brew install nox
   ```
4. nox is ready to use now!