---
title: "Install Bluetility on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Bluetooth Low Energy browser"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Bluetility on MacOS using homebrew

- App Name: Bluetility
- App description: Bluetooth Low Energy browser
- App Version: 1.3
- App Website: https://github.com/jnross/Bluetility

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Bluetility with the following command
   ```
   brew install --cask bluetility
   ```
4. Bluetility is ready to use now!