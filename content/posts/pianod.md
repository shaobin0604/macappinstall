---
title: "Install pianod on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Pandora client with multiple control interfaces"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pianod on MacOS using homebrew

- App Name: pianod
- App description: Pandora client with multiple control interfaces
- App Version: 388
- App Website: https://deviousfish.com/pianod/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pianod with the following command
   ```
   brew install pianod
   ```
4. pianod is ready to use now!