---
title: "Install Sabaki on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Go board and SGF editor"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Sabaki on MacOS using homebrew

- App Name: Sabaki
- App description: Go board and SGF editor
- App Version: 0.52.0
- App Website: https://sabaki.yichuanshen.de/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Sabaki with the following command
   ```
   brew install --cask sabaki
   ```
4. Sabaki is ready to use now!