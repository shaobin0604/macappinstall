---
title: "Install jbigkit on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "JBIG1 data compression standard implementation"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install jbigkit on MacOS using homebrew

- App Name: jbigkit
- App description: JBIG1 data compression standard implementation
- App Version: 2.1
- App Website: https://www.cl.cam.ac.uk/~mgk25/jbigkit/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install jbigkit with the following command
   ```
   brew install jbigkit
   ```
4. jbigkit is ready to use now!