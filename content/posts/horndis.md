---
title: "Install HoRNDIS on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Android USB tethering driver"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install HoRNDIS on MacOS using homebrew

- App Name: HoRNDIS
- App description: Android USB tethering driver
- App Version: 9.2
- App Website: https://github.com/jwise/HoRNDIS

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install HoRNDIS with the following command
   ```
   brew install --cask horndis
   ```
4. HoRNDIS is ready to use now!