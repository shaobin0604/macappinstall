---
title: "Install bandwhich on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Terminal bandwidth utilization tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bandwhich on MacOS using homebrew

- App Name: bandwhich
- App description: Terminal bandwidth utilization tool
- App Version: 0.20.0
- App Website: https://github.com/imsnif/bandwhich

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bandwhich with the following command
   ```
   brew install bandwhich
   ```
4. bandwhich is ready to use now!