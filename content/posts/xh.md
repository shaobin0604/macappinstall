---
title: "Install xh on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Friendly and fast tool for sending HTTP requests"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install xh on MacOS using homebrew

- App Name: xh
- App description: Friendly and fast tool for sending HTTP requests
- App Version: 0.15.0
- App Website: https://github.com/ducaale/xh

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install xh with the following command
   ```
   brew install xh
   ```
4. xh is ready to use now!