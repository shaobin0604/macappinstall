---
title: "Install Lunar on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Adaptive brightness for external displays"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Lunar on MacOS using homebrew

- App Name: Lunar
- App description: Adaptive brightness for external displays
- App Version: 5.3.2
- App Website: https://lunar.fyi/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Lunar with the following command
   ```
   brew install --cask lunar
   ```
4. Lunar is ready to use now!