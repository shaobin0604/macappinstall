---
title: "Install crispy-doom on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Limit-removing enhanced-resolution Doom source port based on Chocolate Doom"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install crispy-doom on MacOS using homebrew

- App Name: crispy-doom
- App description: Limit-removing enhanced-resolution Doom source port based on Chocolate Doom
- App Version: 5.11.1
- App Website: https://github.com/fabiangreffrath/crispy-doom

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install crispy-doom with the following command
   ```
   brew install crispy-doom
   ```
4. crispy-doom is ready to use now!