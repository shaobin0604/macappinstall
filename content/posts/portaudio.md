---
title: "Install portaudio on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cross-platform library for audio I/O"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install portaudio on MacOS using homebrew

- App Name: portaudio
- App description: Cross-platform library for audio I/O
- App Version: 19.7.0
- App Website: http://www.portaudio.com

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install portaudio with the following command
   ```
   brew install portaudio
   ```
4. portaudio is ready to use now!