---
title: "Install Mozilla Firefox on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Web browser"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Mozilla Firefox on MacOS using homebrew

- App Name: Mozilla Firefox
- App description: Web browser
- App Version: 97.0
- App Website: https://www.mozilla.org/firefox/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Mozilla Firefox with the following command
   ```
   brew install --cask firefox
   ```
4. Mozilla Firefox is ready to use now!