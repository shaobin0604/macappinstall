---
title: "Install mkhexgrid on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Fully-configurable hex grid generator"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mkhexgrid on MacOS using homebrew

- App Name: mkhexgrid
- App description: Fully-configurable hex grid generator
- App Version: 0.1.1
- App Website: https://www.nomic.net/~uckelman/mkhexgrid/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mkhexgrid with the following command
   ```
   brew install mkhexgrid
   ```
4. mkhexgrid is ready to use now!