---
title: "Install antibody on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Shell plugin manager"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install antibody on MacOS using homebrew

- App Name: antibody
- App description: Shell plugin manager
- App Version: 6.1.1
- App Website: https://getantibody.github.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install antibody with the following command
   ```
   brew install antibody
   ```
4. antibody is ready to use now!