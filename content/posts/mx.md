---
title: "Install mx on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line tool used for the development of Graal projects"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mx on MacOS using homebrew

- App Name: mx
- App description: Command-line tool used for the development of Graal projects
- App Version: 5.296.0
- App Website: https://github.com/graalvm/mx

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mx with the following command
   ```
   brew install mx
   ```
4. mx is ready to use now!