---
title: "Install Nota Gyazo GIF on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Screenshot and screen recording tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Nota Gyazo GIF on MacOS using homebrew

- App Name: Nota Gyazo GIF
- App description: Screenshot and screen recording tool
- App Version: 3.9.2
- App Website: https://gyazo.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Nota Gyazo GIF with the following command
   ```
   brew install --cask gyazo
   ```
4. Nota Gyazo GIF is ready to use now!