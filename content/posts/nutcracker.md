---
title: "Install nutcracker on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Proxy for memcached and redis"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install nutcracker on MacOS using homebrew

- App Name: nutcracker
- App description: Proxy for memcached and redis
- App Version: 0.5.0
- App Website: https://github.com/twitter/twemproxy

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install nutcracker with the following command
   ```
   brew install nutcracker
   ```
4. nutcracker is ready to use now!