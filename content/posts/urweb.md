---
title: "Install urweb on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Ur/Web programming language"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install urweb on MacOS using homebrew

- App Name: urweb
- App description: Ur/Web programming language
- App Version: 20200209
- App Website: http://www.impredicative.com/ur/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install urweb with the following command
   ```
   brew install urweb
   ```
4. urweb is ready to use now!