---
title: "Install softhsm on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cryptographic store accessible through a PKCS#11 interface"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install softhsm on MacOS using homebrew

- App Name: softhsm
- App description: Cryptographic store accessible through a PKCS#11 interface
- App Version: 2.6.1
- App Website: https://www.opendnssec.org/softhsm/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install softhsm with the following command
   ```
   brew install softhsm
   ```
4. softhsm is ready to use now!