---
title: "Install virtualenvwrapper on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Python virtualenv extensions"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install virtualenvwrapper on MacOS using homebrew

- App Name: virtualenvwrapper
- App description: Python virtualenv extensions
- App Version: 4.8.4
- App Website: https://virtualenvwrapper.readthedocs.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install virtualenvwrapper with the following command
   ```
   brew install virtualenvwrapper
   ```
4. virtualenvwrapper is ready to use now!