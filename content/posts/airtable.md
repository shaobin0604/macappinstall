---
title: "Install Airtable on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Spreadsheet-database hybrid cloud collaboration"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Airtable on MacOS using homebrew

- App Name: Airtable
- App description: Spreadsheet-database hybrid cloud collaboration
- App Version: 1.4.5
- App Website: https://airtable.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Airtable with the following command
   ```
   brew install --cask airtable
   ```
4. Airtable is ready to use now!