---
title: "Install smartmontools on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "SMART hard drive monitoring"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install smartmontools on MacOS using homebrew

- App Name: smartmontools
- App description: SMART hard drive monitoring
- App Version: 7.2
- App Website: https://www.smartmontools.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install smartmontools with the following command
   ```
   brew install smartmontools
   ```
4. smartmontools is ready to use now!