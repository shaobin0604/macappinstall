---
title: "Install EBMac on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Electronic dictionary viewer"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install EBMac on MacOS using homebrew

- App Name: EBMac
- App description: Electronic dictionary viewer
- App Version: 1.46.1
- App Website: http://ebstudio.info/manual/EBMac/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install EBMac with the following command
   ```
   brew install --cask ebmac
   ```
4. EBMac is ready to use now!