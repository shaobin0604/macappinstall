---
title: "Install burl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Shell script wrapper that offers helpful shortcuts for curl(1)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install burl on MacOS using homebrew

- App Name: burl
- App description: Shell script wrapper that offers helpful shortcuts for curl(1)
- App Version: 1.0.1
- App Website: https://github.com/tj/burl

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install burl with the following command
   ```
   brew install burl
   ```
4. burl is ready to use now!