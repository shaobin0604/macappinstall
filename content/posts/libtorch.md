---
title: "Install libtorch on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tensors and dynamic neural networks"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libtorch on MacOS using homebrew

- App Name: libtorch
- App description: Tensors and dynamic neural networks
- App Version: 1.10.2
- App Website: https://pytorch.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libtorch with the following command
   ```
   brew install libtorch
   ```
4. libtorch is ready to use now!