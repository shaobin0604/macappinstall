---
title: "Install ElectrumSV on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Desktop wallet for Bitcoin SV"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ElectrumSV on MacOS using homebrew

- App Name: ElectrumSV
- App description: Desktop wallet for Bitcoin SV
- App Version: 1.4.0b1
- App Website: https://electrumsv.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ElectrumSV with the following command
   ```
   brew install --cask electrumsv
   ```
4. ElectrumSV is ready to use now!