---
title: "Install nuttcp on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Network performance measurement tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install nuttcp on MacOS using homebrew

- App Name: nuttcp
- App description: Network performance measurement tool
- App Version: 8.2.2
- App Website: https://www.nuttcp.net/nuttcp/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install nuttcp with the following command
   ```
   brew install nuttcp
   ```
4. nuttcp is ready to use now!