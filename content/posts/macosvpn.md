---
title: "Install macosvpn on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Create Mac OS VPNs programmatically"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install macosvpn on MacOS using homebrew

- App Name: macosvpn
- App description: Create Mac OS VPNs programmatically
- App Version: 1.0.3
- App Website: https://github.com/halo/macosvpn

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install macosvpn with the following command
   ```
   brew install macosvpn
   ```
4. macosvpn is ready to use now!