---
title: "Install TigerJython on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Jython-based educational programming environment"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install TigerJython on MacOS using homebrew

- App Name: TigerJython
- App description: Jython-based educational programming environment
- App Version: latest
- App Website: https://www.tjgroup.ch/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install TigerJython with the following command
   ```
   brew install --cask tigerjython
   ```
4. TigerJython is ready to use now!