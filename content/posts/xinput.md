---
title: "Install xinput on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utility to configure and test X input devices"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install xinput on MacOS using homebrew

- App Name: xinput
- App description: Utility to configure and test X input devices
- App Version: 1.6.3
- App Website: https://gitlab.freedesktop.org/xorg/app/xinput

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install xinput with the following command
   ```
   brew install xinput
   ```
4. xinput is ready to use now!