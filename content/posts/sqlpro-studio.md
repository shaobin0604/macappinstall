---
title: "Install SQLPro Studio on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Database management tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install SQLPro Studio on MacOS using homebrew

- App Name: SQLPro Studio
- App description: Database management tool
- App Version: 2021.102
- App Website: https://www.sqlprostudio.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install SQLPro Studio with the following command
   ```
   brew install --cask sqlpro-studio
   ```
4. SQLPro Studio is ready to use now!