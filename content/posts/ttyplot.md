---
title: "Install ttyplot on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Realtime plotting utility for terminal with data input from stdin"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ttyplot on MacOS using homebrew

- App Name: ttyplot
- App description: Realtime plotting utility for terminal with data input from stdin
- App Version: 1.4
- App Website: https://github.com/tenox7/ttyplot

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ttyplot with the following command
   ```
   brew install ttyplot
   ```
4. ttyplot is ready to use now!