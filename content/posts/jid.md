---
title: "Install jid on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Json incremental digger"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install jid on MacOS using homebrew

- App Name: jid
- App description: Json incremental digger
- App Version: 0.7.6
- App Website: https://github.com/simeji/jid

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install jid with the following command
   ```
   brew install jid
   ```
4. jid is ready to use now!