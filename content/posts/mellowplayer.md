---
title: "Install MellowPlayer on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Moved to gitlab"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install MellowPlayer on MacOS using homebrew

- App Name: MellowPlayer
- App description: Moved to gitlab
- App Version: 3.4.0
- App Website: https://colinduquesnoy.github.io/MellowPlayer/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install MellowPlayer with the following command
   ```
   brew install --cask mellowplayer
   ```
4. MellowPlayer is ready to use now!