---
title: "Install gloox on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C++ Jabber/XMPP library that handles the low-level protocol"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gloox on MacOS using homebrew

- App Name: gloox
- App description: C++ Jabber/XMPP library that handles the low-level protocol
- App Version: 1.0.24
- App Website: https://camaya.net/gloox/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gloox with the following command
   ```
   brew install gloox
   ```
4. gloox is ready to use now!