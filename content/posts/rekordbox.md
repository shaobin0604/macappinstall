---
title: "Install rekordbox on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Free Dj app to prepare and manage your music files"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install rekordbox on MacOS using homebrew

- App Name: rekordbox
- App description: Free Dj app to prepare and manage your music files
- App Version: 6.6.1,20211125100827
- App Website: https://rekordbox.com/en/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install rekordbox with the following command
   ```
   brew install --cask rekordbox
   ```
4. rekordbox is ready to use now!