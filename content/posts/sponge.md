---
title: "Install sponge on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Soak up standard input and write to a file"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sponge on MacOS using homebrew

- App Name: sponge
- App description: Soak up standard input and write to a file
- App Version: 0.67
- App Website: https://joeyh.name/code/moreutils/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sponge with the following command
   ```
   brew install sponge
   ```
4. sponge is ready to use now!