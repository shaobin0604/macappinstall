---
title: "Install etcd-cpp-apiv3 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C++ implementation for etcd's v3 client API, i.e., ETCDCTL_API=3"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install etcd-cpp-apiv3 on MacOS using homebrew

- App Name: etcd-cpp-apiv3
- App description: C++ implementation for etcd's v3 client API, i.e., ETCDCTL_API=3
- App Version: 0.2.3
- App Website: https://github.com/etcd-cpp-apiv3/etcd-cpp-apiv3

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install etcd-cpp-apiv3 with the following command
   ```
   brew install etcd-cpp-apiv3
   ```
4. etcd-cpp-apiv3 is ready to use now!