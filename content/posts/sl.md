---
title: "Install sl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Prints a steam locomotive if you type sl instead of ls"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sl on MacOS using homebrew

- App Name: sl
- App description: Prints a steam locomotive if you type sl instead of ls
- App Version: 5.02
- App Website: https://github.com/mtoyoda/sl

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sl with the following command
   ```
   brew install sl
   ```
4. sl is ready to use now!