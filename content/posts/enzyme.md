---
title: "Install enzyme on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "High-performance automatic differentiation of LLVM"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install enzyme on MacOS using homebrew

- App Name: enzyme
- App description: High-performance automatic differentiation of LLVM
- App Version: 0.0.27
- App Website: https://enzyme.mit.edu

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install enzyme with the following command
   ```
   brew install enzyme
   ```
4. enzyme is ready to use now!