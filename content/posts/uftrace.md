---
title: "Install uftrace on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Function graph tracer for C/C++/Rust"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install uftrace on MacOS using homebrew

- App Name: uftrace
- App description: Function graph tracer for C/C++/Rust
- App Version: 0.11
- App Website: https://uftrace.github.io/slide/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install uftrace with the following command
   ```
   brew install uftrace
   ```
4. uftrace is ready to use now!