---
title: "Install snobol4 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "String oriented and symbolic programming language"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install snobol4 on MacOS using homebrew

- App Name: snobol4
- App description: String oriented and symbolic programming language
- App Version: 1.5
- App Website: https://www.snobol4.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install snobol4 with the following command
   ```
   brew install snobol4
   ```
4. snobol4 is ready to use now!