---
title: "Install MongoDB Realm Studio on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool for the Realm Database and Realm Platform"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install MongoDB Realm Studio on MacOS using homebrew

- App Name: MongoDB Realm Studio
- App description: Tool for the Realm Database and Realm Platform
- App Version: 11.1.1
- App Website: https://docs.mongodb.com/realm-legacy/products/realm-studio.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install MongoDB Realm Studio with the following command
   ```
   brew install --cask mongodb-realm-studio
   ```
4. MongoDB Realm Studio is ready to use now!