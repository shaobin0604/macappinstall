---
title: "Install CCMenu on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Application to monitor continuous integration servers"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install CCMenu on MacOS using homebrew

- App Name: CCMenu
- App description: Application to monitor continuous integration servers
- App Version: 15.0
- App Website: https://ccmenu.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install CCMenu with the following command
   ```
   brew install --cask ccmenu
   ```
4. CCMenu is ready to use now!