---
title: "Install Time Lapse Assembler on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to create movies from a sequence of images"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Time Lapse Assembler on MacOS using homebrew

- App Name: Time Lapse Assembler
- App description: Tool to create movies from a sequence of images
- App Version: 1.5.3
- App Website: http://www.dayofthenewdan.com/projects/time-lapse-assembler-1/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Time Lapse Assembler with the following command
   ```
   brew install --cask time-lapse-assembler
   ```
4. Time Lapse Assembler is ready to use now!