---
title: "Install SQLPro for MSSQL on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Microsoft SQL Server database client"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install SQLPro for MSSQL on MacOS using homebrew

- App Name: SQLPro for MSSQL
- App description: Microsoft SQL Server database client
- App Version: 2021.106
- App Website: https://www.macsqlclient.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install SQLPro for MSSQL with the following command
   ```
   brew install --cask sqlpro-for-mssql
   ```
4. SQLPro for MSSQL is ready to use now!