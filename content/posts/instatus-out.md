---
title: "Install Instatus Out on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Monitor services in your menu bar"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Instatus Out on MacOS using homebrew

- App Name: Instatus Out
- App description: Monitor services in your menu bar
- App Version: 1.0.8
- App Website: https://instatus.com/out

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Instatus Out with the following command
   ```
   brew install --cask instatus-out
   ```
4. Instatus Out is ready to use now!