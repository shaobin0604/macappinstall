---
title: "Install REALFORCE for Mac on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Software for Realforce keyboards and mice"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install REALFORCE for Mac on MacOS using homebrew

- App Name: REALFORCE for Mac
- App description: Software for Realforce keyboards and mice
- App Version: 2.1.10
- App Website: https://www.realforce.co.jp/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install REALFORCE for Mac with the following command
   ```
   brew install --cask realforce
   ```
4. REALFORCE for Mac is ready to use now!