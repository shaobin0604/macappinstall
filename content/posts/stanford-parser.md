---
title: "Install stanford-parser on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Statistical NLP parser"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install stanford-parser on MacOS using homebrew

- App Name: stanford-parser
- App description: Statistical NLP parser
- App Version: 4.2.0
- App Website: https://nlp.stanford.edu/software/lex-parser.shtml

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install stanford-parser with the following command
   ```
   brew install stanford-parser
   ```
4. stanford-parser is ready to use now!