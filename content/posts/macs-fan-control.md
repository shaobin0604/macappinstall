---
title: "Install Macs Fan Control on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Controls and monitors all fans on Apple computers"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Macs Fan Control on MacOS using homebrew

- App Name: Macs Fan Control
- App description: Controls and monitors all fans on Apple computers
- App Version: 1.5.11
- App Website: https://www.crystalidea.com/macs-fan-control

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Macs Fan Control with the following command
   ```
   brew install --cask macs-fan-control
   ```
4. Macs Fan Control is ready to use now!