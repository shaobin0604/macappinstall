---
title: "Install Dyn Updater on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Dyn Updater on MacOS using homebrew

- App Name: Dyn Updater
- App description: null
- App Version: 5.5.0
- App Website: https://dyn.com/updater/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Dyn Updater with the following command
   ```
   brew install --cask dyn-updater
   ```
4. Dyn Updater is ready to use now!