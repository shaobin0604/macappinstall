---
title: "Install airtrash on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Clone of Apple's Airdrop - easy P2P file transfer"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install airtrash on MacOS using homebrew

- App Name: airtrash
- App description: Clone of Apple's Airdrop - easy P2P file transfer
- App Version: 1.0.0
- App Website: https://github.com/maciejczyzewski/airtrash/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install airtrash with the following command
   ```
   brew install --cask airtrash
   ```
4. airtrash is ready to use now!