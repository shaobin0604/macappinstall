---
title: "Install ForkLift on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Finder replacement and FTP, SFTP, WebDAV and Amazon s3 client"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ForkLift on MacOS using homebrew

- App Name: ForkLift
- App description: Finder replacement and FTP, SFTP, WebDAV and Amazon s3 client
- App Version: 3.5.4,216
- App Website: https://binarynights.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ForkLift with the following command
   ```
   brew install --cask forklift
   ```
4. ForkLift is ready to use now!