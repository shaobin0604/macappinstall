---
title: "Install pacapt on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Package manager in the style of Arch's pacman"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pacapt on MacOS using homebrew

- App Name: pacapt
- App description: Package manager in the style of Arch's pacman
- App Version: 3.0.7
- App Website: https://github.com/icy/pacapt

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pacapt with the following command
   ```
   brew install pacapt
   ```
4. pacapt is ready to use now!