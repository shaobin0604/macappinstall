---
title: "Install mmctl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Remote CLI tool for Mattermost server"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mmctl on MacOS using homebrew

- App Name: mmctl
- App description: Remote CLI tool for Mattermost server
- App Version: 6.3.3
- App Website: https://github.com/mattermost/mmctl

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mmctl with the following command
   ```
   brew install mmctl
   ```
4. mmctl is ready to use now!