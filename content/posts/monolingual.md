---
title: "Install Monolingual on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utility to remove unnecessary language resources from the system"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Monolingual on MacOS using homebrew

- App Name: Monolingual
- App description: Utility to remove unnecessary language resources from the system
- App Version: 1.8.2
- App Website: https://ingmarstein.github.io/Monolingual/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Monolingual with the following command
   ```
   brew install --cask monolingual
   ```
4. Monolingual is ready to use now!