---
title: "Install ColorPicker on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utility to use the Apple color picker anywhere"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ColorPicker on MacOS using homebrew

- App Name: ColorPicker
- App description: Utility to use the Apple color picker anywhere
- App Version: 1.7
- App Website: http://www.northernspysoftware.com/software/colorpicker

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ColorPicker with the following command
   ```
   brew install --cask northernspysoftware-colorpicker
   ```
4. ColorPicker is ready to use now!