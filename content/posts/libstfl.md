---
title: "Install libstfl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library implementing a curses-based widget set for terminals"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libstfl on MacOS using homebrew

- App Name: libstfl
- App description: Library implementing a curses-based widget set for terminals
- App Version: 0.24
- App Website: http://www.clifford.at/stfl/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libstfl with the following command
   ```
   brew install libstfl
   ```
4. libstfl is ready to use now!