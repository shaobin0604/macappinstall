---
title: "Install coin3d on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Open Inventor 2.1 API implementation (Coin) with Python bindings (Pivy)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install coin3d on MacOS using homebrew

- App Name: coin3d
- App description: Open Inventor 2.1 API implementation (Coin) with Python bindings (Pivy)
- App Version: 4.0.0
- App Website: https://coin3d.github.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install coin3d with the following command
   ```
   brew install coin3d
   ```
4. coin3d is ready to use now!