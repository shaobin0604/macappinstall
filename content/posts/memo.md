---
title: "Install Memo on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Note taking app using GitHub Gists"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Memo on MacOS using homebrew

- App Name: Memo
- App description: Note taking app using GitHub Gists
- App Version: 1.0.3
- App Website: https://usememo.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Memo with the following command
   ```
   brew install --cask memo
   ```
4. Memo is ready to use now!