---
title: "Install quazip on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C++ wrapper over Gilles Vollant's ZIP/UNZIP package"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install quazip on MacOS using homebrew

- App Name: quazip
- App description: C++ wrapper over Gilles Vollant's ZIP/UNZIP package
- App Version: 1.2
- App Website: https://github.com/stachenov/quazip/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install quazip with the following command
   ```
   brew install quazip
   ```
4. quazip is ready to use now!