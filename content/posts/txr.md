---
title: "Install txr on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Original, new programming language for convenient data munging"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install txr on MacOS using homebrew

- App Name: txr
- App description: Original, new programming language for convenient data munging
- App Version: 273
- App Website: https://www.nongnu.org/txr/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install txr with the following command
   ```
   brew install txr
   ```
4. txr is ready to use now!