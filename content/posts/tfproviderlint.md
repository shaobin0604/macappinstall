---
title: "Install tfproviderlint on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Terraform Provider Lint Tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tfproviderlint on MacOS using homebrew

- App Name: tfproviderlint
- App description: Terraform Provider Lint Tool
- App Version: 0.28.1
- App Website: https://github.com/bflad/tfproviderlint

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tfproviderlint with the following command
   ```
   brew install tfproviderlint
   ```
4. tfproviderlint is ready to use now!