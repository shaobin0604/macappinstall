---
title: "Install ott on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool for writing definitions of programming languages and calculi"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ott on MacOS using homebrew

- App Name: ott
- App description: Tool for writing definitions of programming languages and calculi
- App Version: 0.31
- App Website: https://www.cl.cam.ac.uk/~pes20/ott/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ott with the following command
   ```
   brew install ott
   ```
4. ott is ready to use now!