---
title: "Install salt on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Dynamic infrastructure communication bus"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install salt on MacOS using homebrew

- App Name: salt
- App description: Dynamic infrastructure communication bus
- App Version: 3004
- App Website: https://saltproject.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install salt with the following command
   ```
   brew install salt
   ```
4. salt is ready to use now!