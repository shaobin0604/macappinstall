---
title: "Install php-cs-fixer@2 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to automatically fix PHP coding standards issues"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install php-cs-fixer@2 on MacOS using homebrew

- App Name: php-cs-fixer@2
- App description: Tool to automatically fix PHP coding standards issues
- App Version: 2.19.3
- App Website: https://cs.symfony.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install php-cs-fixer@2 with the following command
   ```
   brew install php-cs-fixer@2
   ```
4. php-cs-fixer@2 is ready to use now!