---
title: "Install xattred on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Extended attribute editor"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install xattred on MacOS using homebrew

- App Name: xattred
- App description: Extended attribute editor
- App Version: 1.3,2020.08
- App Website: https://eclecticlight.co/xattred-sandstrip-xattr-tools/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install xattred with the following command
   ```
   brew install --cask xattred
   ```
4. xattred is ready to use now!