---
title: "Install ttyd on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line tool for sharing terminal over the web"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ttyd on MacOS using homebrew

- App Name: ttyd
- App description: Command-line tool for sharing terminal over the web
- App Version: 1.6.3
- App Website: https://tsl0922.github.io/ttyd/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ttyd with the following command
   ```
   brew install ttyd
   ```
4. ttyd is ready to use now!