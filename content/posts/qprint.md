---
title: "Install qprint on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Encoder and decoder for quoted-printable encoding"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install qprint on MacOS using homebrew

- App Name: qprint
- App description: Encoder and decoder for quoted-printable encoding
- App Version: 1.1
- App Website: https://www.fourmilab.ch/webtools/qprint/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install qprint with the following command
   ```
   brew install qprint
   ```
4. qprint is ready to use now!