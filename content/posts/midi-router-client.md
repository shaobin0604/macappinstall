---
title: "Install Midi Router Client on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Create routes from anywhere to anywhere"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Midi Router Client on MacOS using homebrew

- App Name: Midi Router Client
- App description: Create routes from anywhere to anywhere
- App Version: 1.2.11
- App Website: https://sourceforge.net/projects/midi-router-client/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Midi Router Client with the following command
   ```
   brew install --cask midi-router-client
   ```
4. Midi Router Client is ready to use now!