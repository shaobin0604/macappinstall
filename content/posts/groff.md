---
title: "Install groff on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GNU troff text-formatting system"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install groff on MacOS using homebrew

- App Name: groff
- App description: GNU troff text-formatting system
- App Version: 1.22.4
- App Website: https://www.gnu.org/software/groff/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install groff with the following command
   ```
   brew install groff
   ```
4. groff is ready to use now!