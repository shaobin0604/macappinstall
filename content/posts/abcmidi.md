---
title: "Install abcmidi on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Converts abc music notation files to MIDI files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install abcmidi on MacOS using homebrew

- App Name: abcmidi
- App description: Converts abc music notation files to MIDI files
- App Version: 2022.02.13
- App Website: https://ifdo.ca/~seymour/runabc/top.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install abcmidi with the following command
   ```
   brew install abcmidi
   ```
4. abcmidi is ready to use now!