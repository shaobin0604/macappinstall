---
title: "Install cpr on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C++ Requests, a spiritual port of Python Requests"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cpr on MacOS using homebrew

- App Name: cpr
- App description: C++ Requests, a spiritual port of Python Requests
- App Version: 1.7.2
- App Website: https://docs.libcpr.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cpr with the following command
   ```
   brew install cpr
   ```
4. cpr is ready to use now!