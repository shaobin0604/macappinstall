---
title: "Install Mailplane on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Gmail client"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Mailplane on MacOS using homebrew

- App Name: Mailplane
- App description: Gmail client
- App Version: 4.3.4,4912
- App Website: https://mailplaneapp.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Mailplane with the following command
   ```
   brew install --cask mailplane
   ```
4. Mailplane is ready to use now!