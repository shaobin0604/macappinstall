---
title: "Install Gray on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to set light or dark appearance on a per-app basis"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Gray on MacOS using homebrew

- App Name: Gray
- App description: Tool to set light or dark appearance on a per-app basis
- App Version: 0.17.0
- App Website: https://github.com/zenangst/Gray

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Gray with the following command
   ```
   brew install --cask gray
   ```
4. Gray is ready to use now!