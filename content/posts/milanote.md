---
title: "Install Milanote on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Organize your ideas and projects into visual boards"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Milanote on MacOS using homebrew

- App Name: Milanote
- App description: Organize your ideas and projects into visual boards
- App Version: 3.0.15
- App Website: https://www.milanote.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Milanote with the following command
   ```
   brew install --cask milanote
   ```
4. Milanote is ready to use now!