---
title: "Install kopia on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Fast and secure open-source backup"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install kopia on MacOS using homebrew

- App Name: kopia
- App description: Fast and secure open-source backup
- App Version: 0.10.5
- App Website: https://kopia.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install kopia with the following command
   ```
   brew install kopia
   ```
4. kopia is ready to use now!