---
title: "Install CleanMyMac X Chinese on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to remove unnecessary files and folders from disk Chinese edition"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install CleanMyMac X Chinese on MacOS using homebrew

- App Name: CleanMyMac X Chinese
- App description: Tool to remove unnecessary files and folders from disk Chinese edition
- App Version: 4.10.1,41001.0.2201241335
- App Website: https://www.mycleanmymac.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install CleanMyMac X Chinese with the following command
   ```
   brew install --cask cleanmymac-zh
   ```
4. CleanMyMac X Chinese is ready to use now!