---
title: "Install faust on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Functional programming language for real time signal processing"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install faust on MacOS using homebrew

- App Name: faust
- App description: Functional programming language for real time signal processing
- App Version: 2.37.3
- App Website: https://faust.grame.fr

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install faust with the following command
   ```
   brew install faust
   ```
4. faust is ready to use now!