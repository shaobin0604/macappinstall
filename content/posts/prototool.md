---
title: "Install prototool on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Your Swiss Army Knife for Protocol Buffers"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install prototool on MacOS using homebrew

- App Name: prototool
- App description: Your Swiss Army Knife for Protocol Buffers
- App Version: 1.10.0
- App Website: https://github.com/uber/prototool

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install prototool with the following command
   ```
   brew install prototool
   ```
4. prototool is ready to use now!