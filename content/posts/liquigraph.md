---
title: "Install liquigraph on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Migration runner for Neo4j"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install liquigraph on MacOS using homebrew

- App Name: liquigraph
- App description: Migration runner for Neo4j
- App Version: 4.0.4
- App Website: https://www.liquigraph.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install liquigraph with the following command
   ```
   brew install liquigraph
   ```
4. liquigraph is ready to use now!