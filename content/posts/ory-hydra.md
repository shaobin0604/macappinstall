---
title: "Install ory-hydra on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "OpenID Certified OAuth 2.0 Server and OpenID Connect Provider"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ory-hydra on MacOS using homebrew

- App Name: ory-hydra
- App description: OpenID Certified OAuth 2.0 Server and OpenID Connect Provider
- App Version: 1.11.4
- App Website: https://www.ory.sh/hydra/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ory-hydra with the following command
   ```
   brew install ory-hydra
   ```
4. ory-hydra is ready to use now!