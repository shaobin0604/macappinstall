---
title: "Install shallow-backup on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Git-integrated backup tool for macOS and Linux devs"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install shallow-backup on MacOS using homebrew

- App Name: shallow-backup
- App description: Git-integrated backup tool for macOS and Linux devs
- App Version: 5.1
- App Website: https://github.com/alichtman/shallow-backup

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install shallow-backup with the following command
   ```
   brew install shallow-backup
   ```
4. shallow-backup is ready to use now!