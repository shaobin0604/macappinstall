---
title: "Install Loom on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Screen and video recording software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Loom on MacOS using homebrew

- App Name: Loom
- App description: Screen and video recording software
- App Version: 0.112.19
- App Website: https://www.loom.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Loom with the following command
   ```
   brew install --cask loom
   ```
4. Loom is ready to use now!