---
title: "Install jove on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Emacs-style editor with vi-like memory, CPU, and size requirements"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install jove on MacOS using homebrew

- App Name: jove
- App description: Emacs-style editor with vi-like memory, CPU, and size requirements
- App Version: 4.16.0.73
- App Website: https://directory.fsf.org/wiki/Jove

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install jove with the following command
   ```
   brew install jove
   ```
4. jove is ready to use now!