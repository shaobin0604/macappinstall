---
title: "Install pcsc-lite on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Middleware to access a smart card using SCard API"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pcsc-lite on MacOS using homebrew

- App Name: pcsc-lite
- App description: Middleware to access a smart card using SCard API
- App Version: 1.9.5
- App Website: https://pcsclite.apdu.fr/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pcsc-lite with the following command
   ```
   brew install pcsc-lite
   ```
4. pcsc-lite is ready to use now!