---
title: "Install Pencil2D on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Open-source tool to make 2D hand-drawn animations"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Pencil2D on MacOS using homebrew

- App Name: Pencil2D
- App description: Open-source tool to make 2D hand-drawn animations
- App Version: 0.6.6
- App Website: https://www.pencil2d.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Pencil2D with the following command
   ```
   brew install --cask pencil2d
   ```
4. Pencil2D is ready to use now!