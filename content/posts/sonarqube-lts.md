---
title: "Install sonarqube-lts on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Manage code quality"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sonarqube-lts on MacOS using homebrew

- App Name: sonarqube-lts
- App description: Manage code quality
- App Version: 8.9.7.52159
- App Website: https://www.sonarqube.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sonarqube-lts with the following command
   ```
   brew install sonarqube-lts
   ```
4. sonarqube-lts is ready to use now!