---
title: "Install MacUpdater on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Track and update to the latest versions of installed software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install MacUpdater on MacOS using homebrew

- App Name: MacUpdater
- App description: Track and update to the latest versions of installed software
- App Version: 2.1.3,13090
- App Website: https://www.corecode.io/macupdater/index.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install MacUpdater with the following command
   ```
   brew install --cask macupdater
   ```
4. MacUpdater is ready to use now!