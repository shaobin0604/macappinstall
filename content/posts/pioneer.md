---
title: "Install pioneer on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Game of lonely space adventure"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pioneer on MacOS using homebrew

- App Name: pioneer
- App description: Game of lonely space adventure
- App Version: 20220203
- App Website: https://pioneerspacesim.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pioneer with the following command
   ```
   brew install pioneer
   ```
4. pioneer is ready to use now!