---
title: "Install libtirpc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Port of Sun's Transport-Independent RPC library to Linux"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libtirpc on MacOS using homebrew

- App Name: libtirpc
- App description: Port of Sun's Transport-Independent RPC library to Linux
- App Version: 1.3.2
- App Website: https://sourceforge.net/projects/libtirpc/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libtirpc with the following command
   ```
   brew install libtirpc
   ```
4. libtirpc is ready to use now!