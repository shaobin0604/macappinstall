---
title: "Install libslax on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Implementation of the SLAX language (an XSLT alternative)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libslax on MacOS using homebrew

- App Name: libslax
- App description: Implementation of the SLAX language (an XSLT alternative)
- App Version: 0.22.1
- App Website: http://www.libslax.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libslax with the following command
   ```
   brew install libslax
   ```
4. libslax is ready to use now!