---
title: "Install vapoursynth-sub on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "VapourSynth filters - Subtitling filter"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install vapoursynth-sub on MacOS using homebrew

- App Name: vapoursynth-sub
- App description: VapourSynth filters - Subtitling filter
- App Version: 2
- App Website: https://www.vapoursynth.com

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install vapoursynth-sub with the following command
   ```
   brew install vapoursynth-sub
   ```
4. vapoursynth-sub is ready to use now!