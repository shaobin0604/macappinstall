---
title: "Install Mu on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Small, simple editor for beginner Python programmers"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Mu on MacOS using homebrew

- App Name: Mu
- App description: Small, simple editor for beginner Python programmers
- App Version: 1.0.3
- App Website: https://codewith.mu/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Mu with the following command
   ```
   brew install --cask mu-editor
   ```
4. Mu is ready to use now!