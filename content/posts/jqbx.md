---
title: "Install JQBX on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Listen to Spotify music in sync with others"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install JQBX on MacOS using homebrew

- App Name: JQBX
- App description: Listen to Spotify music in sync with others
- App Version: 0.9.190
- App Website: https://www.jqbx.fm/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install JQBX with the following command
   ```
   brew install --cask jqbx
   ```
4. JQBX is ready to use now!