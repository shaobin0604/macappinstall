---
title: "Install dxpy on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "DNAnexus toolkit utilities and platform API bindings for Python"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dxpy on MacOS using homebrew

- App Name: dxpy
- App description: DNAnexus toolkit utilities and platform API bindings for Python
- App Version: 0.320.0
- App Website: https://github.com/dnanexus/dx-toolkit

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dxpy with the following command
   ```
   brew install dxpy
   ```
4. dxpy is ready to use now!