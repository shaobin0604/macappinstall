---
title: "Install haskell-stack on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cross-platform program for developing Haskell projects"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install haskell-stack on MacOS using homebrew

- App Name: haskell-stack
- App description: Cross-platform program for developing Haskell projects
- App Version: 2.7.3
- App Website: https://haskellstack.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install haskell-stack with the following command
   ```
   brew install haskell-stack
   ```
4. haskell-stack is ready to use now!