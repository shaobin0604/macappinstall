---
title: "Install go@1.13 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Go programming environment (1.13)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install go@1.13 on MacOS using homebrew

- App Name: go@1.13
- App description: Go programming environment (1.13)
- App Version: 1.13.15
- App Website: https://golang.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install go@1.13 with the following command
   ```
   brew install go@1.13
   ```
4. go@1.13 is ready to use now!