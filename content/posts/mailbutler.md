---
title: "Install Mailbutler on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Personal assistant and productivity tool for Apple Mail"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Mailbutler on MacOS using homebrew

- App Name: Mailbutler
- App description: Personal assistant and productivity tool for Apple Mail
- App Version: 4709,2071582
- App Website: https://www.mailbutler.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Mailbutler with the following command
   ```
   brew install --cask mailbutler
   ```
4. Mailbutler is ready to use now!