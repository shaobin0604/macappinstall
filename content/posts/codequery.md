---
title: "Install codequery on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Code-understanding, code-browsing or code-search tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install codequery on MacOS using homebrew

- App Name: codequery
- App description: Code-understanding, code-browsing or code-search tool
- App Version: 0.24.0
- App Website: https://github.com/ruben2020/codequery

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install codequery with the following command
   ```
   brew install codequery
   ```
4. codequery is ready to use now!