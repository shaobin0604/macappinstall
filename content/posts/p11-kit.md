---
title: "Install p11-kit on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library to load and enumerate PKCS#11 modules"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install p11-kit on MacOS using homebrew

- App Name: p11-kit
- App description: Library to load and enumerate PKCS#11 modules
- App Version: 0.24.1
- App Website: https://p11-glue.freedesktop.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install p11-kit with the following command
   ```
   brew install p11-kit
   ```
4. p11-kit is ready to use now!