---
title: "Install gforth on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Implementation of the ANS Forth language"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gforth on MacOS using homebrew

- App Name: gforth
- App description: Implementation of the ANS Forth language
- App Version: 0.7.3
- App Website: https://www.gnu.org/software/gforth/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gforth with the following command
   ```
   brew install gforth
   ```
4. gforth is ready to use now!