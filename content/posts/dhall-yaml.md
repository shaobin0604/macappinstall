---
title: "Install dhall-yaml on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Convert between Dhall and YAML"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dhall-yaml on MacOS using homebrew

- App Name: dhall-yaml
- App description: Convert between Dhall and YAML
- App Version: 1.2.9
- App Website: https://github.com/dhall-lang/dhall-haskell/tree/master/dhall-yaml

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dhall-yaml with the following command
   ```
   brew install dhall-yaml
   ```
4. dhall-yaml is ready to use now!