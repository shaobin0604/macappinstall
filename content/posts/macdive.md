---
title: "Install MacDive on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Digital dive log"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install MacDive on MacOS using homebrew

- App Name: MacDive
- App description: Digital dive log
- App Version: 2.15.0
- App Website: https://www.mac-dive.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install MacDive with the following command
   ```
   brew install --cask macdive
   ```
4. MacDive is ready to use now!