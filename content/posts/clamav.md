---
title: "Install clamav on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Anti-virus software"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install clamav on MacOS using homebrew

- App Name: clamav
- App description: Anti-virus software
- App Version: 0.104.2
- App Website: https://www.clamav.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install clamav with the following command
   ```
   brew install clamav
   ```
4. clamav is ready to use now!