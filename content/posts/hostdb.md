---
title: "Install hostdb on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Generate DNS zones and DHCP configuration from hostlist.txt"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install hostdb on MacOS using homebrew

- App Name: hostdb
- App description: Generate DNS zones and DHCP configuration from hostlist.txt
- App Version: 1.004
- App Website: https://code.google.com/archive/p/hostdb/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install hostdb with the following command
   ```
   brew install hostdb
   ```
4. hostdb is ready to use now!