---
title: "Install duplicity on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Bandwidth-efficient encrypted backup"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install duplicity on MacOS using homebrew

- App Name: duplicity
- App description: Bandwidth-efficient encrypted backup
- App Version: 0.8.20
- App Website: https://gitlab.com/duplicity/duplicity

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install duplicity with the following command
   ```
   brew install duplicity
   ```
4. duplicity is ready to use now!