---
title: "Install liboauth on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C library for the OAuth Core RFC 5849 standard"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install liboauth on MacOS using homebrew

- App Name: liboauth
- App description: C library for the OAuth Core RFC 5849 standard
- App Version: 1.0.3
- App Website: https://liboauth.sourceforge.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install liboauth with the following command
   ```
   brew install liboauth
   ```
4. liboauth is ready to use now!