---
title: "Install geoserver on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Java server to share and edit geospatial data"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install geoserver on MacOS using homebrew

- App Name: geoserver
- App description: Java server to share and edit geospatial data
- App Version: 2.20.2
- App Website: http://geoserver.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install geoserver with the following command
   ```
   brew install geoserver
   ```
4. geoserver is ready to use now!