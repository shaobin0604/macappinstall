---
title: "Install nicovideo-dl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line program to download videos from www.nicovideo.jp"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install nicovideo-dl on MacOS using homebrew

- App Name: nicovideo-dl
- App description: Command-line program to download videos from www.nicovideo.jp
- App Version: 0.0.20190126
- App Website: https://osdn.net/projects/nicovideo-dl/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install nicovideo-dl with the following command
   ```
   brew install nicovideo-dl
   ```
4. nicovideo-dl is ready to use now!