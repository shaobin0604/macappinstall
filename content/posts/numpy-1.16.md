---
title: "Install numpy@1.16 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Package for scientific computing with Python"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install numpy@1.16 on MacOS using homebrew

- App Name: numpy@1.16
- App description: Package for scientific computing with Python
- App Version: 1.16.6
- App Website: https://www.numpy.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install numpy@1.16 with the following command
   ```
   brew install numpy@1.16
   ```
4. numpy@1.16 is ready to use now!