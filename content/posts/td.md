---
title: "Install td on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Your todo list in your terminal"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install td on MacOS using homebrew

- App Name: td
- App description: Your todo list in your terminal
- App Version: 1.4.2
- App Website: https://github.com/Swatto/td

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install td with the following command
   ```
   brew install td
   ```
4. td is ready to use now!