---
title: "Install Freeplane on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Mind mapping and knowledge management software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Freeplane on MacOS using homebrew

- App Name: Freeplane
- App description: Mind mapping and knowledge management software
- App Version: 1.9.12
- App Website: https://www.freeplane.org/wiki/index.php/Home

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Freeplane with the following command
   ```
   brew install --cask freeplane
   ```
4. Freeplane is ready to use now!