---
title: "Install PDF Squeezer on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "PDF compression tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install PDF Squeezer on MacOS using homebrew

- App Name: PDF Squeezer
- App description: PDF compression tool
- App Version: 4.3.1,634b
- App Website: https://witt-software.com/pdfsqueezer/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install PDF Squeezer with the following command
   ```
   brew install --cask pdf-squeezer
   ```
4. PDF Squeezer is ready to use now!