---
title: "Install pidgin on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Multi-protocol chat client"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pidgin on MacOS using homebrew

- App Name: pidgin
- App description: Multi-protocol chat client
- App Version: 2.14.8
- App Website: https://pidgin.im/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pidgin with the following command
   ```
   brew install pidgin
   ```
4. pidgin is ready to use now!