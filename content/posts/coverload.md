---
title: "Install CoverLoad on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Download high quality artwork for movies, music albums, and more"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install CoverLoad on MacOS using homebrew

- App Name: CoverLoad
- App description: Download high quality artwork for movies, music albums, and more
- App Version: 2.2.0-757
- App Website: https://coverloadapp.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install CoverLoad with the following command
   ```
   brew install --cask coverload
   ```
4. CoverLoad is ready to use now!