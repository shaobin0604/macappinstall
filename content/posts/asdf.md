---
title: "Install asdf on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Extendable version manager with support for Ruby, Node.js, Erlang & more"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install asdf on MacOS using homebrew

- App Name: asdf
- App description: Extendable version manager with support for Ruby, Node.js, Erlang & more
- App Version: 0.9.0
- App Website: https://asdf-vm.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install asdf with the following command
   ```
   brew install asdf
   ```
4. asdf is ready to use now!