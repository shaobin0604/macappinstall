---
title: "Install cdk8s on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Define k8s native apps and abstractions using object-oriented programming"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cdk8s on MacOS using homebrew

- App Name: cdk8s
- App description: Define k8s native apps and abstractions using object-oriented programming
- App Version: 1.0.101
- App Website: https://cdk8s.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cdk8s with the following command
   ```
   brew install cdk8s
   ```
4. cdk8s is ready to use now!