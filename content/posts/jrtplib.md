---
title: "Install jrtplib on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Fully featured C++ Library for RTP (Real-time Transport Protocol)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install jrtplib on MacOS using homebrew

- App Name: jrtplib
- App description: Fully featured C++ Library for RTP (Real-time Transport Protocol)
- App Version: 3.11.2
- App Website: https://research.edm.uhasselt.be/jori/jrtplib

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install jrtplib with the following command
   ```
   brew install jrtplib
   ```
4. jrtplib is ready to use now!