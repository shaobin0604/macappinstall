---
title: "Install Screens on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Remote access software focusing on usability"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Screens on MacOS using homebrew

- App Name: Screens
- App description: Remote access software focusing on usability
- App Version: 4.9.3,1643990258
- App Website: https://edovia.com/screens-mac/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Screens with the following command
   ```
   brew install --cask screens
   ```
4. Screens is ready to use now!