---
title: "Install xrootd on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "High performance, scalable, fault-tolerant access to data"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install xrootd on MacOS using homebrew

- App Name: xrootd
- App description: High performance, scalable, fault-tolerant access to data
- App Version: 5.4.0
- App Website: https://xrootd.slac.stanford.edu/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install xrootd with the following command
   ```
   brew install xrootd
   ```
4. xrootd is ready to use now!