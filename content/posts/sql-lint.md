---
title: "Install sql-lint on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "SQL linter to do sanity checks on your queries and bring errors back from the DB"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sql-lint on MacOS using homebrew

- App Name: sql-lint
- App description: SQL linter to do sanity checks on your queries and bring errors back from the DB
- App Version: 0.0.19
- App Website: https://github.com/joereynolds/sql-lint

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sql-lint with the following command
   ```
   brew install sql-lint
   ```
4. sql-lint is ready to use now!