---
title: "Install Cevelop on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C++ IDE"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Cevelop on MacOS using homebrew

- App Name: Cevelop
- App description: C++ IDE
- App Version: 1.14.1-202002280945
- App Website: https://www.cevelop.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Cevelop with the following command
   ```
   brew install --cask cevelop
   ```
4. Cevelop is ready to use now!