---
title: "Install rancher-compose on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Docker Compose compatible client to deploy to Rancher"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install rancher-compose on MacOS using homebrew

- App Name: rancher-compose
- App description: Docker Compose compatible client to deploy to Rancher
- App Version: 0.12.5
- App Website: https://github.com/rancher/rancher-compose

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install rancher-compose with the following command
   ```
   brew install rancher-compose
   ```
4. rancher-compose is ready to use now!