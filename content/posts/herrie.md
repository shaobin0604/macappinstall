---
title: "Install herrie on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Minimalistic audio player built upon Ncurses"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install herrie on MacOS using homebrew

- App Name: herrie
- App description: Minimalistic audio player built upon Ncurses
- App Version: 2.2
- App Website: https://github.com/EdSchouten/herrie

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install herrie with the following command
   ```
   brew install herrie
   ```
4. herrie is ready to use now!