---
title: "Install goad on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "AWS Lambda powered, highly distributed, load testing tool built in Go"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install goad on MacOS using homebrew

- App Name: goad
- App description: AWS Lambda powered, highly distributed, load testing tool built in Go
- App Version: 2.0.4
- App Website: https://goad.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install goad with the following command
   ```
   brew install goad
   ```
4. goad is ready to use now!