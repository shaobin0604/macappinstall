---
title: "Install conserver on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Allows multiple users to watch a serial console at the same time"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install conserver on MacOS using homebrew

- App Name: conserver
- App description: Allows multiple users to watch a serial console at the same time
- App Version: 8.2.6
- App Website: https://www.conserver.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install conserver with the following command
   ```
   brew install conserver
   ```
4. conserver is ready to use now!