---
title: "Install planck on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Stand-alone ClojureScript REPL"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install planck on MacOS using homebrew

- App Name: planck
- App description: Stand-alone ClojureScript REPL
- App Version: 2.26.0
- App Website: https://planck-repl.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install planck with the following command
   ```
   brew install planck
   ```
4. planck is ready to use now!