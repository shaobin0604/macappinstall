---
title: "Install swagger-codegen@2 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Generate clients, server stubs, and docs from an OpenAPI spec"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install swagger-codegen@2 on MacOS using homebrew

- App Name: swagger-codegen@2
- App description: Generate clients, server stubs, and docs from an OpenAPI spec
- App Version: 2.4.26
- App Website: https://swagger.io/swagger-codegen/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install swagger-codegen@2 with the following command
   ```
   brew install swagger-codegen@2
   ```
4. swagger-codegen@2 is ready to use now!