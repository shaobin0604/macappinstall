---
title: "Install shellshare on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Live Terminal Broadcast"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install shellshare on MacOS using homebrew

- App Name: shellshare
- App description: Live Terminal Broadcast
- App Version: 1.1.0
- App Website: https://shellshare.net

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install shellshare with the following command
   ```
   brew install shellshare
   ```
4. shellshare is ready to use now!