---
title: "Install openmotif on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "LGPL release of the Motif toolkit"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install openmotif on MacOS using homebrew

- App Name: openmotif
- App description: LGPL release of the Motif toolkit
- App Version: 2.3.8
- App Website: https://motif.ics.com/motif

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install openmotif with the following command
   ```
   brew install openmotif
   ```
4. openmotif is ready to use now!