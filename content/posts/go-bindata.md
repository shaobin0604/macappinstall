---
title: "Install go-bindata on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Small utility that generates Go code from any file"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install go-bindata on MacOS using homebrew

- App Name: go-bindata
- App description: Small utility that generates Go code from any file
- App Version: 3.23.0
- App Website: https://github.com/kevinburke/go-bindata

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install go-bindata with the following command
   ```
   brew install go-bindata
   ```
4. go-bindata is ready to use now!