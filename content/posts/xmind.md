---
title: "Install XMind on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Mind mapping and brainstorming tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install XMind on MacOS using homebrew

- App Name: XMind
- App description: Mind mapping and brainstorming tool
- App Version: 3.7.9.201912052356,8-update9
- App Website: https://www.xmind.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install XMind with the following command
   ```
   brew install --cask xmind
   ```
4. XMind is ready to use now!