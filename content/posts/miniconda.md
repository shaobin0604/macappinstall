---
title: "Install Continuum Analytics Miniconda on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Minimal installer for conda"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Continuum Analytics Miniconda on MacOS using homebrew

- App Name: Continuum Analytics Miniconda
- App description: Minimal installer for conda
- App Version: py39_4.11.0
- App Website: https://conda.io/miniconda.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Continuum Analytics Miniconda with the following command
   ```
   brew install --cask miniconda
   ```
4. Continuum Analytics Miniconda is ready to use now!