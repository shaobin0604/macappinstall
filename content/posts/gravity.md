---
title: "Install gravity on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Embeddable programming language"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gravity on MacOS using homebrew

- App Name: gravity
- App description: Embeddable programming language
- App Version: 0.8.5
- App Website: https://marcobambini.github.io/gravity/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gravity with the following command
   ```
   brew install gravity
   ```
4. gravity is ready to use now!