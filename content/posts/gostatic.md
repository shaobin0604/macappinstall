---
title: "Install gostatic on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Fast static site generator"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gostatic on MacOS using homebrew

- App Name: gostatic
- App description: Fast static site generator
- App Version: 2.32
- App Website: https://github.com/piranha/gostatic

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gostatic with the following command
   ```
   brew install gostatic
   ```
4. gostatic is ready to use now!