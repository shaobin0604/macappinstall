---
title: "Install AppCode on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "IDE for Swift, Objective-C, C, and C++ development"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install AppCode on MacOS using homebrew

- App Name: AppCode
- App description: IDE for Swift, Objective-C, C, and C++ development
- App Version: 2021.3.2,213.6777.48
- App Website: https://www.jetbrains.com/objc/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install AppCode with the following command
   ```
   brew install --cask appcode
   ```
4. AppCode is ready to use now!