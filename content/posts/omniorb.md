---
title: "Install omniorb on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "IOR and naming service utilities for omniORB"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install omniorb on MacOS using homebrew

- App Name: omniorb
- App description: IOR and naming service utilities for omniORB
- App Version: 4.2.4
- App Website: https://omniorb.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install omniorb with the following command
   ```
   brew install omniorb
   ```
4. omniorb is ready to use now!