---
title: "Install PhotoSync Companion on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install PhotoSync Companion on MacOS using homebrew

- App Name: PhotoSync Companion
- App description: null
- App Version: 4.1,410
- App Website: https://www.photosync-app.com/home.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install PhotoSync Companion with the following command
   ```
   brew install --cask photosync
   ```
4. PhotoSync Companion is ready to use now!