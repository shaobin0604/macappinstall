---
title: "Install vip on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Program that provides for interactive editing in a pipeline"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install vip on MacOS using homebrew

- App Name: vip
- App description: Program that provides for interactive editing in a pipeline
- App Version: 19971113
- App Website: https://www.cs.duke.edu/~des/vip.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install vip with the following command
   ```
   brew install vip
   ```
4. vip is ready to use now!