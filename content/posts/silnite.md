---
title: "Install silnite on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Checks EFI firmware and security data file updates"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install silnite on MacOS using homebrew

- App Name: silnite
- App description: Checks EFI firmware and security data file updates
- App Version: 6a,2022.02
- App Website: https://eclecticlight.co/lockrattler-systhist/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install silnite with the following command
   ```
   brew install --cask silnite
   ```
4. silnite is ready to use now!