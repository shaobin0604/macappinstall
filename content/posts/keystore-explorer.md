---
title: "Install KeyStore Explorer on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GUI replacement for the Java command-line utilities keytool and jarsigner"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install KeyStore Explorer on MacOS using homebrew

- App Name: KeyStore Explorer
- App description: GUI replacement for the Java command-line utilities keytool and jarsigner
- App Version: 5.5.1
- App Website: https://keystore-explorer.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install KeyStore Explorer with the following command
   ```
   brew install --cask keystore-explorer
   ```
4. KeyStore Explorer is ready to use now!