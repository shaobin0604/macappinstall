---
title: "Install bgrep on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Like grep but for binary strings"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bgrep on MacOS using homebrew

- App Name: bgrep
- App description: Like grep but for binary strings
- App Version: 0.2
- App Website: https://github.com/tmbinc/bgrep

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bgrep with the following command
   ```
   brew install bgrep
   ```
4. bgrep is ready to use now!