---
title: "Install Unlox on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Unlock your computer with your fingerprint"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Unlox on MacOS using homebrew

- App Name: Unlox
- App description: Unlock your computer with your fingerprint
- App Version: 3.0.4.2
- App Website: https://unlox.it/get

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Unlox with the following command
   ```
   brew install --cask unlox
   ```
4. Unlox is ready to use now!