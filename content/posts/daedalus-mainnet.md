---
title: "Install Daedalus Mainnet on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cryptocurrency wallet for ada on the Cardano blockchain"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Daedalus Mainnet on MacOS using homebrew

- App Name: Daedalus Mainnet
- App description: Cryptocurrency wallet for ada on the Cardano blockchain
- App Version: 4.8.0,20689
- App Website: https://daedaluswallet.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Daedalus Mainnet with the following command
   ```
   brew install --cask daedalus-mainnet
   ```
4. Daedalus Mainnet is ready to use now!