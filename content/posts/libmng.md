---
title: "Install libmng on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "MNG/JNG reference library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libmng on MacOS using homebrew

- App Name: libmng
- App description: MNG/JNG reference library
- App Version: 2.0.3
- App Website: https://libmng.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libmng with the following command
   ```
   brew install libmng
   ```
4. libmng is ready to use now!