---
title: "Install eqMac on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "System-wide audio equalizer"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install eqMac on MacOS using homebrew

- App Name: eqMac
- App description: System-wide audio equalizer
- App Version: 1.4.6
- App Website: https://eqmac.app/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install eqMac with the following command
   ```
   brew install --cask eqmac
   ```
4. eqMac is ready to use now!