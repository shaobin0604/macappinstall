---
title: "Install libgnomecanvas on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Highlevel, structured graphics library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libgnomecanvas on MacOS using homebrew

- App Name: libgnomecanvas
- App description: Highlevel, structured graphics library
- App Version: 2.30.3
- App Website: https://gitlab.gnome.org/Archive/libgnomecanvas

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libgnomecanvas with the following command
   ```
   brew install libgnomecanvas
   ```
4. libgnomecanvas is ready to use now!