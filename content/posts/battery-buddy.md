---
title: "Install Battery Buddy on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Replacement of the default battery indicator in the menu bar"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Battery Buddy on MacOS using homebrew

- App Name: Battery Buddy
- App description: Replacement of the default battery indicator in the menu bar
- App Version: 1.0.3,11
- App Website: https://batterybuddy.app/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Battery Buddy with the following command
   ```
   brew install --cask battery-buddy
   ```
4. Battery Buddy is ready to use now!