---
title: "Install Zappy on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Screen capture tool for remote teams"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Zappy on MacOS using homebrew

- App Name: Zappy
- App description: Screen capture tool for remote teams
- App Version: 3.0.3
- App Website: https://zapier.com/zappy

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Zappy with the following command
   ```
   brew install --cask zappy
   ```
4. Zappy is ready to use now!