---
title: "Install liquidprompt on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Adaptive prompt for bash and zsh shells"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install liquidprompt on MacOS using homebrew

- App Name: liquidprompt
- App description: Adaptive prompt for bash and zsh shells
- App Version: 2.0.4
- App Website: https://github.com/nojhan/liquidprompt

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install liquidprompt with the following command
   ```
   brew install liquidprompt
   ```
4. liquidprompt is ready to use now!