---
title: "Install nxengine on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Rewrite of Cave Story (Doukutsu Monogatari)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install nxengine on MacOS using homebrew

- App Name: nxengine
- App description: Rewrite of Cave Story (Doukutsu Monogatari)
- App Version: 1.0.0.6
- App Website: https://nxengine.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install nxengine with the following command
   ```
   brew install nxengine
   ```
4. nxengine is ready to use now!