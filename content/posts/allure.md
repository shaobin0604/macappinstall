---
title: "Install allure on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Flexible lightweight test report tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install allure on MacOS using homebrew

- App Name: allure
- App description: Flexible lightweight test report tool
- App Version: 2.17.3
- App Website: https://github.com/allure-framework/allure2

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install allure with the following command
   ```
   brew install allure
   ```
4. allure is ready to use now!