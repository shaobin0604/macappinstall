---
title: "Install Blender on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "3D creation suite"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Blender on MacOS using homebrew

- App Name: Blender
- App description: 3D creation suite
- App Version: 3.0.1
- App Website: https://www.blender.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Blender with the following command
   ```
   brew install --cask blender
   ```
4. Blender is ready to use now!