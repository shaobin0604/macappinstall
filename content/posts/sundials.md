---
title: "Install sundials on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Nonlinear and differential/algebraic equations solver"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sundials on MacOS using homebrew

- App Name: sundials
- App description: Nonlinear and differential/algebraic equations solver
- App Version: 5.8.0
- App Website: https://computing.llnl.gov/projects/sundials

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sundials with the following command
   ```
   brew install sundials
   ```
4. sundials is ready to use now!