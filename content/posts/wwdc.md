---
title: "Install WWDC on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Allows access to WWDC livestreams, videos and sessions"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install WWDC on MacOS using homebrew

- App Name: WWDC
- App description: Allows access to WWDC livestreams, videos and sessions
- App Version: 7.3.3,1024
- App Website: https://wwdc.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install WWDC with the following command
   ```
   brew install --cask wwdc
   ```
4. WWDC is ready to use now!