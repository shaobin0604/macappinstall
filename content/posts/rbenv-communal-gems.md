---
title: "Install rbenv-communal-gems on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Share gems across multiple rbenv Ruby installs"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install rbenv-communal-gems on MacOS using homebrew

- App Name: rbenv-communal-gems
- App description: Share gems across multiple rbenv Ruby installs
- App Version: 1.0.1
- App Website: https://github.com/tpope/rbenv-communal-gems

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install rbenv-communal-gems with the following command
   ```
   brew install rbenv-communal-gems
   ```
4. rbenv-communal-gems is ready to use now!