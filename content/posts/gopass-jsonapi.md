---
title: "Install gopass-jsonapi on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Gopass Browser Bindings"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gopass-jsonapi on MacOS using homebrew

- App Name: gopass-jsonapi
- App description: Gopass Browser Bindings
- App Version: 1.11.1
- App Website: https://github.com/gopasspw/gopass-jsonapi

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gopass-jsonapi with the following command
   ```
   brew install gopass-jsonapi
   ```
4. gopass-jsonapi is ready to use now!