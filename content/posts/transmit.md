---
title: "Install Transmit on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "File transfer application"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Transmit on MacOS using homebrew

- App Name: Transmit
- App description: File transfer application
- App Version: 5.8.4
- App Website: https://panic.com/transmit/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Transmit with the following command
   ```
   brew install --cask transmit
   ```
4. Transmit is ready to use now!