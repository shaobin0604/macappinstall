---
title: "Install gatsby-cli on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Gatsby command-line interface"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gatsby-cli on MacOS using homebrew

- App Name: gatsby-cli
- App description: Gatsby command-line interface
- App Version: 4.7.0
- App Website: https://www.gatsbyjs.org/docs/gatsby-cli/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gatsby-cli with the following command
   ```
   brew install gatsby-cli
   ```
4. gatsby-cli is ready to use now!