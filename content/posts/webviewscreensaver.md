---
title: "Install WebViewScreenSaver on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Screen saver that displays web pages"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install WebViewScreenSaver on MacOS using homebrew

- App Name: WebViewScreenSaver
- App description: Screen saver that displays web pages
- App Version: 2.2.1
- App Website: https://github.com/liquidx/webviewscreensaver

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install WebViewScreenSaver with the following command
   ```
   brew install --cask webviewscreensaver
   ```
4. WebViewScreenSaver is ready to use now!