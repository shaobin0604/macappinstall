---
title: "Install pdf2djvu on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Create DjVu files from PDF files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pdf2djvu on MacOS using homebrew

- App Name: pdf2djvu
- App description: Create DjVu files from PDF files
- App Version: 0.9.18.2
- App Website: https://jwilk.net/software/pdf2djvu

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pdf2djvu with the following command
   ```
   brew install pdf2djvu
   ```
4. pdf2djvu is ready to use now!