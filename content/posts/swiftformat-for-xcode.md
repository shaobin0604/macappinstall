---
title: "Install SwiftFormat for Xcode on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Xcode Extension for reformatting Swift code"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install SwiftFormat for Xcode on MacOS using homebrew

- App Name: SwiftFormat for Xcode
- App description: Xcode Extension for reformatting Swift code
- App Version: 0.49.4
- App Website: https://github.com/nicklockwood/SwiftFormat

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install SwiftFormat for Xcode with the following command
   ```
   brew install --cask swiftformat-for-xcode
   ```
4. SwiftFormat for Xcode is ready to use now!