---
title: "Install b2-tools on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "B2 Cloud Storage Command-Line Tools"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install b2-tools on MacOS using homebrew

- App Name: b2-tools
- App description: B2 Cloud Storage Command-Line Tools
- App Version: 3.2.0
- App Website: https://github.com/Backblaze/B2_Command_Line_Tool

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install b2-tools with the following command
   ```
   brew install b2-tools
   ```
4. b2-tools is ready to use now!