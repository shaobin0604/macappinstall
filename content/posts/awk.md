---
title: "Install awk on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Text processing scripting language"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install awk on MacOS using homebrew

- App Name: awk
- App description: Text processing scripting language
- App Version: 20220122
- App Website: https://www.cs.princeton.edu/~bwk/btl.mirror/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install awk with the following command
   ```
   brew install awk
   ```
4. awk is ready to use now!