---
title: "Install mp3val on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Program for MPEG audio stream validation"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mp3val on MacOS using homebrew

- App Name: mp3val
- App description: Program for MPEG audio stream validation
- App Version: 0.1.8
- App Website: https://mp3val.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mp3val with the following command
   ```
   brew install mp3val
   ```
4. mp3val is ready to use now!