---
title: "Install SpamSieve on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Spam filtering extension for e-mail clients"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install SpamSieve on MacOS using homebrew

- App Name: SpamSieve
- App description: Spam filtering extension for e-mail clients
- App Version: 2.9.48
- App Website: https://c-command.com/spamsieve/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install SpamSieve with the following command
   ```
   brew install --cask spamsieve
   ```
4. SpamSieve is ready to use now!