---
title: "Install iBetterCharge on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Battery level monitoring software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install iBetterCharge on MacOS using homebrew

- App Name: iBetterCharge
- App description: Battery level monitoring software
- App Version: 1.0.12,1568119585
- App Website: https://softorino.com/ibettercharge/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install iBetterCharge with the following command
   ```
   brew install --cask ibettercharge
   ```
4. iBetterCharge is ready to use now!