---
title: "Install libchaos on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Advanced library for randomization, hashing and statistical analysis"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libchaos on MacOS using homebrew

- App Name: libchaos
- App description: Advanced library for randomization, hashing and statistical analysis
- App Version: 1.0
- App Website: https://github.com/maciejczyzewski/libchaos

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libchaos with the following command
   ```
   brew install libchaos
   ```
4. libchaos is ready to use now!