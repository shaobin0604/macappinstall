---
title: "Install libev on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Asynchronous event library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libev on MacOS using homebrew

- App Name: libev
- App description: Asynchronous event library
- App Version: 4.33
- App Website: http://software.schmorp.de/pkg/libev.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libev with the following command
   ```
   brew install libev
   ```
4. libev is ready to use now!