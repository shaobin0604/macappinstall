---
title: "Install libao on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cross-platform Audio Library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libao on MacOS using homebrew

- App Name: libao
- App description: Cross-platform Audio Library
- App Version: 1.2.2
- App Website: https://www.xiph.org/ao/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libao with the following command
   ```
   brew install libao
   ```
4. libao is ready to use now!