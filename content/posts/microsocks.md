---
title: "Install microsocks on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tiny, portable SOCKS5 server with very moderate resource usage"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install microsocks on MacOS using homebrew

- App Name: microsocks
- App description: Tiny, portable SOCKS5 server with very moderate resource usage
- App Version: 1.0.3
- App Website: https://github.com/rofl0r/microsocks

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install microsocks with the following command
   ```
   brew install microsocks
   ```
4. microsocks is ready to use now!