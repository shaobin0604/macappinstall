---
title: "Install Manyverse on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Social network built on the peer-to-peer SSB protocol"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Manyverse on MacOS using homebrew

- App Name: Manyverse
- App description: Social network built on the peer-to-peer SSB protocol
- App Version: 0.2202.7-beta
- App Website: https://www.manyver.se/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Manyverse with the following command
   ```
   brew install --cask manyverse
   ```
4. Manyverse is ready to use now!