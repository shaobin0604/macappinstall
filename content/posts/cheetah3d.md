---
title: "Install Cheetah3D on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "3D modeling, rendering and animation software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Cheetah3D on MacOS using homebrew

- App Name: Cheetah3D
- App description: 3D modeling, rendering and animation software
- App Version: 7.5.1
- App Website: https://www.cheetah3d.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Cheetah3D with the following command
   ```
   brew install --cask cheetah3d
   ```
4. Cheetah3D is ready to use now!