---
title: "Install Modern CSV on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CSV editor"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Modern CSV on MacOS using homebrew

- App Name: Modern CSV
- App description: CSV editor
- App Version: 1.3.35
- App Website: https://www.moderncsv.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Modern CSV with the following command
   ```
   brew install --cask modern-csv
   ```
4. Modern CSV is ready to use now!