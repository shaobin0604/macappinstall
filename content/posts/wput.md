---
title: "Install wput on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tiny, wget-like FTP client for uploading files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install wput on MacOS using homebrew

- App Name: wput
- App description: Tiny, wget-like FTP client for uploading files
- App Version: 0.6.2
- App Website: https://wput.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install wput with the following command
   ```
   brew install wput
   ```
4. wput is ready to use now!