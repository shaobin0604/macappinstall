---
title: "Install Nuclino on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Collaborative wiki and knowledgebase"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Nuclino on MacOS using homebrew

- App Name: Nuclino
- App description: Collaborative wiki and knowledgebase
- App Version: 1.5.0
- App Website: https://www.nuclino.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Nuclino with the following command
   ```
   brew install --cask nuclino
   ```
4. Nuclino is ready to use now!