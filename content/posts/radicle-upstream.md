---
title: "Install Radicle Upstream on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Desktop client for Radicle, a p2p stack for code collaboration"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Radicle Upstream on MacOS using homebrew

- App Name: Radicle Upstream
- App description: Desktop client for Radicle, a p2p stack for code collaboration
- App Version: 0.2.13
- App Website: https://radicle.xyz/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Radicle Upstream with the following command
   ```
   brew install --cask radicle-upstream
   ```
4. Radicle Upstream is ready to use now!