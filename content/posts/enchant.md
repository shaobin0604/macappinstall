---
title: "Install enchant on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Spellchecker wrapping library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install enchant on MacOS using homebrew

- App Name: enchant
- App description: Spellchecker wrapping library
- App Version: 2.3.2
- App Website: https://abiword.github.io/enchant/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install enchant with the following command
   ```
   brew install enchant
   ```
4. enchant is ready to use now!