---
title: "Install opentsdb on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Scalable, distributed Time Series Database"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install opentsdb on MacOS using homebrew

- App Name: opentsdb
- App description: Scalable, distributed Time Series Database
- App Version: 2.4.0
- App Website: http://opentsdb.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install opentsdb with the following command
   ```
   brew install opentsdb
   ```
4. opentsdb is ready to use now!