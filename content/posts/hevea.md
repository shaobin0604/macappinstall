---
title: "Install hevea on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "LaTeX-to-HTML translator"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install hevea on MacOS using homebrew

- App Name: hevea
- App description: LaTeX-to-HTML translator
- App Version: 2.35
- App Website: http://hevea.inria.fr/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install hevea with the following command
   ```
   brew install hevea
   ```
4. hevea is ready to use now!