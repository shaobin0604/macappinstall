---
title: "Install rancid on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Really Awesome New Cisco confIg Differ"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install rancid on MacOS using homebrew

- App Name: rancid
- App description: Really Awesome New Cisco confIg Differ
- App Version: 3.13
- App Website: https://www.shrubbery.net/rancid/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install rancid with the following command
   ```
   brew install rancid
   ```
4. rancid is ready to use now!