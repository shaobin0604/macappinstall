---
title: "Install PhotoZoom Pro on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Software for enlarging and downsizing digital photos and graphics"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install PhotoZoom Pro on MacOS using homebrew

- App Name: PhotoZoom Pro
- App description: Software for enlarging and downsizing digital photos and graphics
- App Version: 8
- App Website: https://www.benvista.com/photozoompro

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install PhotoZoom Pro with the following command
   ```
   brew install --cask photozoom-pro
   ```
4. PhotoZoom Pro is ready to use now!