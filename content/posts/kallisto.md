---
title: "Install kallisto on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Quantify abundances of transcripts from RNA-Seq data"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install kallisto on MacOS using homebrew

- App Name: kallisto
- App description: Quantify abundances of transcripts from RNA-Seq data
- App Version: 0.48.0
- App Website: https://pachterlab.github.io/kallisto/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install kallisto with the following command
   ```
   brew install kallisto
   ```
4. kallisto is ready to use now!