---
title: "Install libxslt on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C XSLT library for GNOME"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libxslt on MacOS using homebrew

- App Name: libxslt
- App description: C XSLT library for GNOME
- App Version: 1.1.34
- App Website: http://xmlsoft.org/XSLT/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libxslt with the following command
   ```
   brew install libxslt
   ```
4. libxslt is ready to use now!