---
title: "Install celero on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C++ Benchmark Authoring Library/Framework"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install celero on MacOS using homebrew

- App Name: celero
- App description: C++ Benchmark Authoring Library/Framework
- App Version: 2.8.3
- App Website: https://github.com/DigitalInBlue/Celero

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install celero with the following command
   ```
   brew install celero
   ```
4. celero is ready to use now!