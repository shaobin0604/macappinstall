---
title: "Install AMPPS on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Software stack for website development"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install AMPPS on MacOS using homebrew

- App Name: AMPPS
- App description: Software stack for website development
- App Version: 3.9
- App Website: https://www.ampps.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install AMPPS with the following command
   ```
   brew install --cask ampps
   ```
4. AMPPS is ready to use now!