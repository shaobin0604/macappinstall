---
title: "Install binwalk on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Searches a binary image for embedded files and executable code"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install binwalk on MacOS using homebrew

- App Name: binwalk
- App description: Searches a binary image for embedded files and executable code
- App Version: 2.3.3
- App Website: https://github.com/ReFirmLabs/binwalk

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install binwalk with the following command
   ```
   brew install binwalk
   ```
4. binwalk is ready to use now!