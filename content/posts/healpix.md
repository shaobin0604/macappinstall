---
title: "Install healpix on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Hierarchical Equal Area isoLatitude Pixelization of a sphere"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install healpix on MacOS using homebrew

- App Name: healpix
- App description: Hierarchical Equal Area isoLatitude Pixelization of a sphere
- App Version: 3.80
- App Website: https://healpix.jpl.nasa.gov

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install healpix with the following command
   ```
   brew install healpix
   ```
4. healpix is ready to use now!