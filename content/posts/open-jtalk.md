---
title: "Install open-jtalk on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Japanese text-to-speech system"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install open-jtalk on MacOS using homebrew

- App Name: open-jtalk
- App description: Japanese text-to-speech system
- App Version: 1.11
- App Website: https://open-jtalk.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install open-jtalk with the following command
   ```
   brew install open-jtalk
   ```
4. open-jtalk is ready to use now!