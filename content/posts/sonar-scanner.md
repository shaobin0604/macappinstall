---
title: "Install sonar-scanner on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Launcher to analyze a project with SonarQube"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sonar-scanner on MacOS using homebrew

- App Name: sonar-scanner
- App description: Launcher to analyze a project with SonarQube
- App Version: 4.6.2.2472
- App Website: https://docs.sonarqube.org/display/SCAN/Analyzing+with+SonarQube+Scanner

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sonar-scanner with the following command
   ```
   brew install sonar-scanner
   ```
4. sonar-scanner is ready to use now!