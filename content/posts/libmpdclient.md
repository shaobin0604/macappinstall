---
title: "Install libmpdclient on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library for MPD in the C, C++, and Objective-C languages"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libmpdclient on MacOS using homebrew

- App Name: libmpdclient
- App description: Library for MPD in the C, C++, and Objective-C languages
- App Version: 2.20
- App Website: https://www.musicpd.org/libs/libmpdclient/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libmpdclient with the following command
   ```
   brew install libmpdclient
   ```
4. libmpdclient is ready to use now!