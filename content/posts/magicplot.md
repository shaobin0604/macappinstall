---
title: "Install MagicPlot on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Software for nonlinear fitting, plotting and data analysis"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install MagicPlot on MacOS using homebrew

- App Name: MagicPlot
- App description: Software for nonlinear fitting, plotting and data analysis
- App Version: 3.0.1
- App Website: https://magicplot.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install MagicPlot with the following command
   ```
   brew install --cask magicplot
   ```
4. MagicPlot is ready to use now!