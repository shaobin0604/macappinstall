---
title: "Install transcrypt on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Configure transparent encryption of files in a Git repo"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install transcrypt on MacOS using homebrew

- App Name: transcrypt
- App description: Configure transparent encryption of files in a Git repo
- App Version: 2.1.0
- App Website: https://github.com/elasticdog/transcrypt

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install transcrypt with the following command
   ```
   brew install transcrypt
   ```
4. transcrypt is ready to use now!