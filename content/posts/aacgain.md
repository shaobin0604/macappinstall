---
title: "Install aacgain on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "AAC-supporting version of mp3gain"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install aacgain on MacOS using homebrew

- App Name: aacgain
- App description: AAC-supporting version of mp3gain
- App Version: 1.8
- App Website: https://aacgain.altosdesign.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install aacgain with the following command
   ```
   brew install aacgain
   ```
4. aacgain is ready to use now!