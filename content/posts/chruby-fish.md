---
title: "Install chruby-fish on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Thin wrapper around chruby to make it work with the Fish shell"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install chruby-fish on MacOS using homebrew

- App Name: chruby-fish
- App description: Thin wrapper around chruby to make it work with the Fish shell
- App Version: 0.8.2
- App Website: https://github.com/JeanMertz/chruby-fish#readme

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install chruby-fish with the following command
   ```
   brew install chruby-fish
   ```
4. chruby-fish is ready to use now!