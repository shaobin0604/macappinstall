---
title: "Install libnet on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C library for creating IP packets"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libnet on MacOS using homebrew

- App Name: libnet
- App description: C library for creating IP packets
- App Version: 1.2
- App Website: https://github.com/sam-github/libnet

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libnet with the following command
   ```
   brew install libnet
   ```
4. libnet is ready to use now!