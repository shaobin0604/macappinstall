---
title: "Install MavensMate on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Packaged desktop app for MavensMate server"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install MavensMate on MacOS using homebrew

- App Name: MavensMate
- App description: Packaged desktop app for MavensMate server
- App Version: 0.0.11
- App Website: https://github.com/joeferraro/MavensMate-Desktop

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install MavensMate with the following command
   ```
   brew install --cask mavensmate
   ```
4. MavensMate is ready to use now!