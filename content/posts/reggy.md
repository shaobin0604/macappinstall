---
title: "Install Reggy on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Interactive regular expression validator and tester"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Reggy on MacOS using homebrew

- App Name: Reggy
- App description: Interactive regular expression validator and tester
- App Version: 1.3
- App Website: http://reggyapp.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Reggy with the following command
   ```
   brew install --cask reggy
   ```
4. Reggy is ready to use now!