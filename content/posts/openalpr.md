---
title: "Install openalpr on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Automatic License Plate Recognition library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install openalpr on MacOS using homebrew

- App Name: openalpr
- App description: Automatic License Plate Recognition library
- App Version: 2.3.0
- App Website: https://www.openalpr.com

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install openalpr with the following command
   ```
   brew install openalpr
   ```
4. openalpr is ready to use now!