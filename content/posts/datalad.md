---
title: "Install datalad on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Data distribution geared toward scientific datasets"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install datalad on MacOS using homebrew

- App Name: datalad
- App description: Data distribution geared toward scientific datasets
- App Version: 0.15.5
- App Website: https://www.datalad.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install datalad with the following command
   ```
   brew install datalad
   ```
4. datalad is ready to use now!