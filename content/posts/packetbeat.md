---
title: "Install packetbeat on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Lightweight Shipper for Network Data"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install packetbeat on MacOS using homebrew

- App Name: packetbeat
- App description: Lightweight Shipper for Network Data
- App Version: 8.0.0
- App Website: https://www.elastic.co/products/beats/packetbeat

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install packetbeat with the following command
   ```
   brew install packetbeat
   ```
4. packetbeat is ready to use now!