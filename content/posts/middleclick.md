---
title: "Install MiddleClick on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utility to extend trackpad functionality"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install MiddleClick on MacOS using homebrew

- App Name: MiddleClick
- App description: Utility to extend trackpad functionality
- App Version: 2.5
- App Website: https://github.com/artginzburg/MiddleClick-BigSur

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install MiddleClick with the following command
   ```
   brew install --cask middleclick
   ```
4. MiddleClick is ready to use now!