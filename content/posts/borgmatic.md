---
title: "Install borgmatic on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Simple wrapper script for the Borg backup software"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install borgmatic on MacOS using homebrew

- App Name: borgmatic
- App description: Simple wrapper script for the Borg backup software
- App Version: 1.5.22
- App Website: https://torsion.org/borgmatic/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install borgmatic with the following command
   ```
   brew install borgmatic
   ```
4. borgmatic is ready to use now!