---
title: "Install ocamlbuild on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Generic build tool for OCaml"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ocamlbuild on MacOS using homebrew

- App Name: ocamlbuild
- App description: Generic build tool for OCaml
- App Version: 0.14.1
- App Website: https://github.com/ocaml/ocamlbuild

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ocamlbuild with the following command
   ```
   brew install ocamlbuild
   ```
4. ocamlbuild is ready to use now!