---
title: "Install adwaita-icon-theme on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Icons for the GNOME project"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install adwaita-icon-theme on MacOS using homebrew

- App Name: adwaita-icon-theme
- App description: Icons for the GNOME project
- App Version: 41.0
- App Website: https://developer.gnome.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install adwaita-icon-theme with the following command
   ```
   brew install adwaita-icon-theme
   ```
4. adwaita-icon-theme is ready to use now!