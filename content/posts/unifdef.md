---
title: "Install unifdef on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Selectively process conditional C preprocessor directives"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install unifdef on MacOS using homebrew

- App Name: unifdef
- App description: Selectively process conditional C preprocessor directives
- App Version: 2.12
- App Website: https://dotat.at/prog/unifdef/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install unifdef with the following command
   ```
   brew install unifdef
   ```
4. unifdef is ready to use now!