---
title: "Install GrandPerspective on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Graphically shows disk usage within a file system"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install GrandPerspective on MacOS using homebrew

- App Name: GrandPerspective
- App description: Graphically shows disk usage within a file system
- App Version: 2.6.2
- App Website: https://grandperspectiv.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install GrandPerspective with the following command
   ```
   brew install --cask grandperspective
   ```
4. GrandPerspective is ready to use now!