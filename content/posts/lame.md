---
title: "Install lame on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "High quality MPEG Audio Layer III (MP3) encoder"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install lame on MacOS using homebrew

- App Name: lame
- App description: High quality MPEG Audio Layer III (MP3) encoder
- App Version: 3.100
- App Website: https://lame.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install lame with the following command
   ```
   brew install lame
   ```
4. lame is ready to use now!