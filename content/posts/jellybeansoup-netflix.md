---
title: "Install Netflix on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Third-party app to use Netflix outside the browser"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Netflix on MacOS using homebrew

- App Name: Netflix
- App description: Third-party app to use Netflix outside the browser
- App Version: 1.0.5
- App Website: https://github.com/jellybeansoup/macos-netflix

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Netflix with the following command
   ```
   brew install --cask jellybeansoup-netflix
   ```
4. Netflix is ready to use now!