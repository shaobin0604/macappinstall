---
title: "Install opa on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Open source, general-purpose policy engine"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install opa on MacOS using homebrew

- App Name: opa
- App description: Open source, general-purpose policy engine
- App Version: 0.37.2
- App Website: https://www.openpolicyagent.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install opa with the following command
   ```
   brew install opa
   ```
4. opa is ready to use now!