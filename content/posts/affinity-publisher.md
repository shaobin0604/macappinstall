---
title: "Install Affinity Publisher on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Professional desktop publishing software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Affinity Publisher on MacOS using homebrew

- App Name: Affinity Publisher
- App description: Professional desktop publishing software
- App Version: 1.10.4
- App Website: https://affinity.serif.com/en-us/publisher/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Affinity Publisher with the following command
   ```
   brew install --cask affinity-publisher
   ```
4. Affinity Publisher is ready to use now!