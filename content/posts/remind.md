---
title: "Install remind on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Sophisticated calendar and alarm"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install remind on MacOS using homebrew

- App Name: remind
- App description: Sophisticated calendar and alarm
- App Version: 03.04.00
- App Website: https://dianne.skoll.ca/projects/remind/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install remind with the following command
   ```
   brew install remind
   ```
4. remind is ready to use now!