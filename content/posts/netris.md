---
title: "Install netris on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Networked variant of tetris"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install netris on MacOS using homebrew

- App Name: netris
- App description: Networked variant of tetris
- App Version: 0.52
- App Website: https://web.archive.org/web/20071223041235/www.netris.be/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install netris with the following command
   ```
   brew install netris
   ```
4. netris is ready to use now!