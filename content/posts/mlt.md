---
title: "Install mlt on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Author, manage, and run multitrack audio/video compositions"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mlt on MacOS using homebrew

- App Name: mlt
- App description: Author, manage, and run multitrack audio/video compositions
- App Version: 7.4.0
- App Website: https://www.mltframework.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mlt with the following command
   ```
   brew install mlt
   ```
4. mlt is ready to use now!