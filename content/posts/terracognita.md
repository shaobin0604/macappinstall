---
title: "Install terracognita on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Reads from existing Cloud Providers and generates Terraform code"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install terracognita on MacOS using homebrew

- App Name: terracognita
- App description: Reads from existing Cloud Providers and generates Terraform code
- App Version: 0.7.4
- App Website: https://github.com/cycloidio/terracognita

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install terracognita with the following command
   ```
   brew install terracognita
   ```
4. terracognita is ready to use now!