---
title: "Install MacBreakZ on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Ergonomic Assistant to prevent health problems"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install MacBreakZ on MacOS using homebrew

- App Name: MacBreakZ
- App description: Ergonomic Assistant to prevent health problems
- App Version: 5.44
- App Website: https://www.publicspace.net/MacBreakZ/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install MacBreakZ with the following command
   ```
   brew install --cask macbreakz
   ```
4. MacBreakZ is ready to use now!