---
title: "Install unison on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "File synchronization tool for OSX"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install unison on MacOS using homebrew

- App Name: unison
- App description: File synchronization tool for OSX
- App Version: 2.51.5
- App Website: https://www.cis.upenn.edu/~bcpierce/unison/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install unison with the following command
   ```
   brew install unison
   ```
4. unison is ready to use now!