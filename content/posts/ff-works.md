---
title: "Install ff·Works on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Video-encoding and transcoding app"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ff·Works on MacOS using homebrew

- App Name: ff·Works
- App description: Video-encoding and transcoding app
- App Version: 3.0.1
- App Website: https://www.ffworks.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ff·Works with the following command
   ```
   brew install --cask ff-works
   ```
4. ff·Works is ready to use now!