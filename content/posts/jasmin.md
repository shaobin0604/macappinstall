---
title: "Install jasmin on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Assembler for the Java Virtual Machine"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install jasmin on MacOS using homebrew

- App Name: jasmin
- App description: Assembler for the Java Virtual Machine
- App Version: 2.4
- App Website: https://jasmin.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install jasmin with the following command
   ```
   brew install jasmin
   ```
4. jasmin is ready to use now!