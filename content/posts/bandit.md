---
title: "Install bandit on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Security-oriented static analyser for Python code"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bandit on MacOS using homebrew

- App Name: bandit
- App description: Security-oriented static analyser for Python code
- App Version: 1.7.2
- App Website: https://github.com/PyCQA/bandit

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bandit with the following command
   ```
   brew install bandit
   ```
4. bandit is ready to use now!