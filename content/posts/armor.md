---
title: "Install armor on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Uncomplicated, modern HTTP server"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install armor on MacOS using homebrew

- App Name: armor
- App description: Uncomplicated, modern HTTP server
- App Version: 0.4.14
- App Website: https://github.com/labstack/armor

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install armor with the following command
   ```
   brew install armor
   ```
4. armor is ready to use now!