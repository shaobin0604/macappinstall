---
title: "Install libdvdnav on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "DVD navigation library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libdvdnav on MacOS using homebrew

- App Name: libdvdnav
- App description: DVD navigation library
- App Version: 6.1.1
- App Website: https://www.videolan.org/developers/libdvdnav.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libdvdnav with the following command
   ```
   brew install libdvdnav
   ```
4. libdvdnav is ready to use now!