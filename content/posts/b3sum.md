---
title: "Install b3sum on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "BLAKE3 cryptographic hash function"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install b3sum on MacOS using homebrew

- App Name: b3sum
- App description: BLAKE3 cryptographic hash function
- App Version: 1.3.1
- App Website: https://github.com/BLAKE3-team/BLAKE3

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install b3sum with the following command
   ```
   brew install b3sum
   ```
4. b3sum is ready to use now!