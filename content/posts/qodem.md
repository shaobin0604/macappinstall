---
title: "Install qodem on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Terminal emulator and BBS client"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install qodem on MacOS using homebrew

- App Name: qodem
- App description: Terminal emulator and BBS client
- App Version: 1.0.1
- App Website: https://qodem.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install qodem with the following command
   ```
   brew install qodem
   ```
4. qodem is ready to use now!