---
title: "Install libxo on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Allows an application to generate text, XML, JSON, and HTML output"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libxo on MacOS using homebrew

- App Name: libxo
- App description: Allows an application to generate text, XML, JSON, and HTML output
- App Version: 1.6.0
- App Website: https://juniper.github.io/libxo/libxo-manual.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libxo with the following command
   ```
   brew install libxo
   ```
4. libxo is ready to use now!