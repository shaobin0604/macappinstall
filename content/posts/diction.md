---
title: "Install diction on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GNU diction and style"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install diction on MacOS using homebrew

- App Name: diction
- App description: GNU diction and style
- App Version: 1.11
- App Website: https://www.gnu.org/software/diction/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install diction with the following command
   ```
   brew install diction
   ```
4. diction is ready to use now!