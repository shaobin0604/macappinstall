---
title: "Install beecrypt on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C/C++ cryptography library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install beecrypt on MacOS using homebrew

- App Name: beecrypt
- App description: C/C++ cryptography library
- App Version: 4.2.1
- App Website: https://beecrypt.sourceforge.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install beecrypt with the following command
   ```
   brew install beecrypt
   ```
4. beecrypt is ready to use now!