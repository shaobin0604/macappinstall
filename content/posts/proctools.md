---
title: "Install proctools on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "OpenBSD and Darwin versions of pgrep, pkill, and pfind"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install proctools on MacOS using homebrew

- App Name: proctools
- App description: OpenBSD and Darwin versions of pgrep, pkill, and pfind
- App Version: 0.4pre1
- App Website: https://proctools.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install proctools with the following command
   ```
   brew install proctools
   ```
4. proctools is ready to use now!