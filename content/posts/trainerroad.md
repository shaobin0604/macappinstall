---
title: "Install TrainerRoad on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cycling training system"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install TrainerRoad on MacOS using homebrew

- App Name: TrainerRoad
- App description: Cycling training system
- App Version: 2021.49.0.156
- App Website: https://www.trainerroad.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install TrainerRoad with the following command
   ```
   brew install --cask trainerroad
   ```
4. TrainerRoad is ready to use now!