---
title: "Install logstalgia on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Web server access log visualizer with retro style"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install logstalgia on MacOS using homebrew

- App Name: logstalgia
- App description: Web server access log visualizer with retro style
- App Version: 1.1.2
- App Website: https://logstalgia.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install logstalgia with the following command
   ```
   brew install logstalgia
   ```
4. logstalgia is ready to use now!