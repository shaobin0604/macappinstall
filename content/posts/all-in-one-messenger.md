---
title: "Install All-in-One Messenger on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Combined interface for various messaging platforms"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install All-in-One Messenger on MacOS using homebrew

- App Name: All-in-One Messenger
- App description: Combined interface for various messaging platforms
- App Version: 2.5.0
- App Website: https://allinone.im/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install All-in-One Messenger with the following command
   ```
   brew install --cask all-in-one-messenger
   ```
4. All-in-One Messenger is ready to use now!