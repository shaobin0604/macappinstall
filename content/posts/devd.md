---
title: "Install devd on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Local webserver for developers"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install devd on MacOS using homebrew

- App Name: devd
- App description: Local webserver for developers
- App Version: 0.9
- App Website: https://github.com/cortesi/devd

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install devd with the following command
   ```
   brew install devd
   ```
4. devd is ready to use now!