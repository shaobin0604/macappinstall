---
title: "Install Cisdem Document Reader on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Document reader to open and view Windows-based files"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Cisdem Document Reader on MacOS using homebrew

- App Name: Cisdem Document Reader
- App description: Document reader to open and view Windows-based files
- App Version: 5.4.0
- App Website: https://www.cisdem.com/document-reader-mac.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Cisdem Document Reader with the following command
   ```
   brew install --cask cisdem-document-reader
   ```
4. Cisdem Document Reader is ready to use now!