---
title: "Install LX Music Assistant Desktop Edition on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Music app base on Electron & Vue"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install LX Music Assistant Desktop Edition on MacOS using homebrew

- App Name: LX Music Assistant Desktop Edition
- App description: Music app base on Electron & Vue
- App Version: 1.17.1
- App Website: https://github.com/lyswhut/lx-music-desktop/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install LX Music Assistant Desktop Edition with the following command
   ```
   brew install --cask lx-music
   ```
4. LX Music Assistant Desktop Edition is ready to use now!