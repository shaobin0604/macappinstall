---
title: "Install dmd on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "D programming language compiler for macOS"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dmd on MacOS using homebrew

- App Name: dmd
- App description: D programming language compiler for macOS
- App Version: 2.098.1
- App Website: https://dlang.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dmd with the following command
   ```
   brew install dmd
   ```
4. dmd is ready to use now!