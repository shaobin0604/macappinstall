---
title: "Install libu2f-host on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Host-side of the Universal 2nd Factor (U2F) protocol"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libu2f-host on MacOS using homebrew

- App Name: libu2f-host
- App description: Host-side of the Universal 2nd Factor (U2F) protocol
- App Version: 1.1.10
- App Website: https://developers.yubico.com/libu2f-host/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libu2f-host with the following command
   ```
   brew install libu2f-host
   ```
4. libu2f-host is ready to use now!