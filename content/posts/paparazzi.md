---
title: "Install Paparazzi! on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utility to take screenshots of webpages"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Paparazzi! on MacOS using homebrew

- App Name: Paparazzi!
- App description: Utility to take screenshots of webpages
- App Version: 1.0b11,868
- App Website: https://derailer.org/paparazzi/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Paparazzi! with the following command
   ```
   brew install --cask paparazzi
   ```
4. Paparazzi! is ready to use now!