---
title: "Install v2ray-plugin on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "SIP003 plugin based on v2ray for shadowsocks"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install v2ray-plugin on MacOS using homebrew

- App Name: v2ray-plugin
- App description: SIP003 plugin based on v2ray for shadowsocks
- App Version: 1.3.1
- App Website: https://github.com/shadowsocks/v2ray-plugin

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install v2ray-plugin with the following command
   ```
   brew install v2ray-plugin
   ```
4. v2ray-plugin is ready to use now!