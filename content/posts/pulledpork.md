---
title: "Install pulledpork on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Snort rule management"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pulledpork on MacOS using homebrew

- App Name: pulledpork
- App description: Snort rule management
- App Version: 0.7.4
- App Website: https://github.com/shirkdog/pulledpork

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pulledpork with the following command
   ```
   brew install pulledpork
   ```
4. pulledpork is ready to use now!