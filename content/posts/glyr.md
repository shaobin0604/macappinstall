---
title: "Install glyr on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Music related metadata search engine with command-line interface and C API"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install glyr on MacOS using homebrew

- App Name: glyr
- App description: Music related metadata search engine with command-line interface and C API
- App Version: 1.0.10
- App Website: https://github.com/sahib/glyr

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install glyr with the following command
   ```
   brew install glyr
   ```
4. glyr is ready to use now!