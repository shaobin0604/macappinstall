---
title: "Install MCEdit-Unified on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Minecraft world editor"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install MCEdit-Unified on MacOS using homebrew

- App Name: MCEdit-Unified
- App description: Minecraft world editor
- App Version: 1.5.6.0
- App Website: https://www.mcedit-unified.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install MCEdit-Unified with the following command
   ```
   brew install --cask mcedit
   ```
4. MCEdit-Unified is ready to use now!