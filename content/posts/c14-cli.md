---
title: "Install c14-cli on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Manage your Online C14 archives from the command-line"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install c14-cli on MacOS using homebrew

- App Name: c14-cli
- App description: Manage your Online C14 archives from the command-line
- App Version: 0.5.0
- App Website: https://github.com/scaleway/c14-cli

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install c14-cli with the following command
   ```
   brew install c14-cli
   ```
4. c14-cli is ready to use now!