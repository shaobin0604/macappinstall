---
title: "Install openj9 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "High performance, scalable, Java virtual machine"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install openj9 on MacOS using homebrew

- App Name: openj9
- App description: High performance, scalable, Java virtual machine
- App Version: 0.29.1
- App Website: https://www.eclipse.org/openj9/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install openj9 with the following command
   ```
   brew install openj9
   ```
4. openj9 is ready to use now!