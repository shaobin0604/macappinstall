---
title: "Install prettier on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Code formatter for JavaScript, CSS, JSON, GraphQL, Markdown, YAML"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install prettier on MacOS using homebrew

- App Name: prettier
- App description: Code formatter for JavaScript, CSS, JSON, GraphQL, Markdown, YAML
- App Version: 2.5.1
- App Website: https://prettier.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install prettier with the following command
   ```
   brew install prettier
   ```
4. prettier is ready to use now!