---
title: "Install Lockdown on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Audits and remediates security configuration settings"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Lockdown on MacOS using homebrew

- App Name: Lockdown
- App description: Audits and remediates security configuration settings
- App Version: 1.0.0
- App Website: https://objective-see.com/products/lockdown.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Lockdown with the following command
   ```
   brew install --cask lockdown
   ```
4. Lockdown is ready to use now!