---
title: "Install digiKam on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Digital photo manager"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install digiKam on MacOS using homebrew

- App Name: digiKam
- App description: Digital photo manager
- App Version: 7.5.0
- App Website: https://www.digikam.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install digiKam with the following command
   ```
   brew install --cask digikam
   ```
4. digiKam is ready to use now!