---
title: "Install proselint on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Linter for prose"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install proselint on MacOS using homebrew

- App Name: proselint
- App description: Linter for prose
- App Version: 0.13.0
- App Website: http://proselint.com

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install proselint with the following command
   ```
   brew install proselint
   ```
4. proselint is ready to use now!