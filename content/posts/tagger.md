---
title: "Install Tagger on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Music metadata editor supporting batch edits and importing VGMdb data"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Tagger on MacOS using homebrew

- App Name: Tagger
- App description: Music metadata editor supporting batch edits and importing VGMdb data
- App Version: 1.8.0.7
- App Website: https://bilalh.github.io/projects/tagger/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Tagger with the following command
   ```
   brew install --cask tagger
   ```
4. Tagger is ready to use now!