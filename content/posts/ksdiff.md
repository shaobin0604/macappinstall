---
title: "Install ksdiff on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line tool for the App Store version of Kaleidoscope"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ksdiff on MacOS using homebrew

- App Name: ksdiff
- App description: Command-line tool for the App Store version of Kaleidoscope
- App Version: 3.2.2,145
- App Website: https://kaleidoscope.app/ksdiff3

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ksdiff with the following command
   ```
   brew install --cask ksdiff
   ```
4. ksdiff is ready to use now!