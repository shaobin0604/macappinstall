---
title: "Install pwnat on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Proxy server that works behind a NAT"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pwnat on MacOS using homebrew

- App Name: pwnat
- App description: Proxy server that works behind a NAT
- App Version: 0.3-beta
- App Website: https://samy.pl/pwnat/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pwnat with the following command
   ```
   brew install pwnat
   ```
4. pwnat is ready to use now!