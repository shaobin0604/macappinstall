---
title: "Install linkerd on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line utility to interact with linkerd"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install linkerd on MacOS using homebrew

- App Name: linkerd
- App description: Command-line utility to interact with linkerd
- App Version: 2.11.1
- App Website: https://linkerd.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install linkerd with the following command
   ```
   brew install linkerd
   ```
4. linkerd is ready to use now!