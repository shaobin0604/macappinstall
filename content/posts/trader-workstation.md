---
title: "Install Trader Workstation on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Trading software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Trader Workstation on MacOS using homebrew

- App Name: Trader Workstation
- App description: Trading software
- App Version: 10.12.2i
- App Website: https://www.interactivebrokers.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Trader Workstation with the following command
   ```
   brew install --cask trader-workstation
   ```
4. Trader Workstation is ready to use now!