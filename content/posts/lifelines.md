---
title: "Install lifelines on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Text-based genealogy software"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install lifelines on MacOS using homebrew

- App Name: lifelines
- App description: Text-based genealogy software
- App Version: 3.0.62
- App Website: https://lifelines.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install lifelines with the following command
   ```
   brew install lifelines
   ```
4. lifelines is ready to use now!