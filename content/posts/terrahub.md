---
title: "Install terrahub on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Terraform automation and orchestration tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install terrahub on MacOS using homebrew

- App Name: terrahub
- App description: Terraform automation and orchestration tool
- App Version: 0.5.6
- App Website: https://docs.terrahub.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install terrahub with the following command
   ```
   brew install terrahub
   ```
4. terrahub is ready to use now!