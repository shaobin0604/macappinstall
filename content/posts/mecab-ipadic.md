---
title: "Install mecab-ipadic on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "IPA dictionary compiled for MeCab"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mecab-ipadic on MacOS using homebrew

- App Name: mecab-ipadic
- App description: IPA dictionary compiled for MeCab
- App Version: 2.7.0-20070801
- App Website: https://taku910.github.io/mecab/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mecab-ipadic with the following command
   ```
   brew install mecab-ipadic
   ```
4. mecab-ipadic is ready to use now!