---
title: "Install git-absorb on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Automatic git commit --fixup"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install git-absorb on MacOS using homebrew

- App Name: git-absorb
- App description: Automatic git commit --fixup
- App Version: 0.6.6
- App Website: https://github.com/tummychow/git-absorb

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install git-absorb with the following command
   ```
   brew install git-absorb
   ```
4. git-absorb is ready to use now!