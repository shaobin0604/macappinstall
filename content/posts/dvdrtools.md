---
title: "Install dvdrtools on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Fork of cdrtools DVD writer support"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dvdrtools on MacOS using homebrew

- App Name: dvdrtools
- App description: Fork of cdrtools DVD writer support
- App Version: 0.2.1
- App Website: https://savannah.nongnu.org/projects/dvdrtools/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dvdrtools with the following command
   ```
   brew install dvdrtools
   ```
4. dvdrtools is ready to use now!