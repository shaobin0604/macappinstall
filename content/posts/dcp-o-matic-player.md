---
title: "Install DCP-o-matic Player on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Play Digital Cinema Packages"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install DCP-o-matic Player on MacOS using homebrew

- App Name: DCP-o-matic Player
- App description: Play Digital Cinema Packages
- App Version: 2.14.57
- App Website: https://dcpomatic.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install DCP-o-matic Player with the following command
   ```
   brew install --cask dcp-o-matic-player
   ```
4. DCP-o-matic Player is ready to use now!