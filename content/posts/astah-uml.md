---
title: "Install Change Vision Astah UML on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "UML diagramming tool with mind mapping"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Change Vision Astah UML on MacOS using homebrew

- App Name: Change Vision Astah UML
- App description: UML diagramming tool with mind mapping
- App Version: 8.4.1,827bdf
- App Website: https://astah.net/products/astah-uml/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Change Vision Astah UML with the following command
   ```
   brew install --cask astah-uml
   ```
4. Change Vision Astah UML is ready to use now!