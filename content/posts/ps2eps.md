---
title: "Install ps2eps on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Convert PostScript to EPS files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ps2eps on MacOS using homebrew

- App Name: ps2eps
- App description: Convert PostScript to EPS files
- App Version: 1.70
- App Website: https://www.tm.uka.de/~bless/ps2eps

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ps2eps with the following command
   ```
   brew install ps2eps
   ```
4. ps2eps is ready to use now!