---
title: "Install minbif on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "IRC-to-other-IM-networks gateway using Pidgin library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install minbif on MacOS using homebrew

- App Name: minbif
- App description: IRC-to-other-IM-networks gateway using Pidgin library
- App Version: 1.0.5-20150505
- App Website: https://web.archive.org/web/20180831190920/https://symlink.me/projects/minbif/wiki/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install minbif with the following command
   ```
   brew install minbif
   ```
4. minbif is ready to use now!