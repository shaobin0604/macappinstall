---
title: "Install nim on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Statically typed compiled systems programming language"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install nim on MacOS using homebrew

- App Name: nim
- App description: Statically typed compiled systems programming language
- App Version: 1.6.4
- App Website: https://nim-lang.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install nim with the following command
   ```
   brew install nim
   ```
4. nim is ready to use now!