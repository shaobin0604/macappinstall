---
title: "Install Phoenix Firestorm viewer for OpenSim on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Viewer for accessing Virtual Worlds"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Phoenix Firestorm viewer for OpenSim on MacOS using homebrew

- App Name: Phoenix Firestorm viewer for OpenSim
- App description: Viewer for accessing Virtual Worlds
- App Version: 6.4.21.64531
- App Website: https://www.firestormviewer.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Phoenix Firestorm viewer for OpenSim with the following command
   ```
   brew install --cask firestormos
   ```
4. Phoenix Firestorm viewer for OpenSim is ready to use now!