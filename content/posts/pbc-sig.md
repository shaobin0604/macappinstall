---
title: "Install pbc-sig on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Signatures library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pbc-sig on MacOS using homebrew

- App Name: pbc-sig
- App description: Signatures library
- App Version: 0.0.8
- App Website: https://crypto.stanford.edu/pbc/sig/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pbc-sig with the following command
   ```
   brew install pbc-sig
   ```
4. pbc-sig is ready to use now!