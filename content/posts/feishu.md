---
title: "Install feishu on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Project management software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install feishu on MacOS using homebrew

- App Name: feishu
- App description: Project management software
- App Version: 5.6.9,4595c3
- App Website: https://www.feishu.cn/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install feishu with the following command
   ```
   brew install --cask feishu
   ```
4. feishu is ready to use now!