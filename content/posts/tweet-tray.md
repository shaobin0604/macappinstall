---
title: "Install Tweet Tray on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tweet quickly from the desktop without any  distractions"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Tweet Tray on MacOS using homebrew

- App Name: Tweet Tray
- App description: Tweet quickly from the desktop without any  distractions
- App Version: 1.1.5
- App Website: https://github.com/jonathontoon/tweet-tray

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Tweet Tray with the following command
   ```
   brew install --cask tweet-tray
   ```
4. Tweet Tray is ready to use now!