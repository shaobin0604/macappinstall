---
title: "Install 1Clipboard on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Clipboard managing app"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install 1Clipboard on MacOS using homebrew

- App Name: 1Clipboard
- App description: Clipboard managing app
- App Version: 0.1.8
- App Website: http://1clipboard.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install 1Clipboard with the following command
   ```
   brew install --cask 1clipboard
   ```
4. 1Clipboard is ready to use now!