---
title: "Install xorriso on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "ISO9660+RR manipulation tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install xorriso on MacOS using homebrew

- App Name: xorriso
- App description: ISO9660+RR manipulation tool
- App Version: 1.5.4
- App Website: https://www.gnu.org/software/xorriso/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install xorriso with the following command
   ```
   brew install xorriso
   ```
4. xorriso is ready to use now!