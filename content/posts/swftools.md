---
title: "Install swftools on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "SWF manipulation and generation tools"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install swftools on MacOS using homebrew

- App Name: swftools
- App description: SWF manipulation and generation tools
- App Version: 0.9.2
- App Website: http://www.swftools.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install swftools with the following command
   ```
   brew install swftools
   ```
4. swftools is ready to use now!