---
title: "Install ivykis on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Async I/O-assisting library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ivykis on MacOS using homebrew

- App Name: ivykis
- App description: Async I/O-assisting library
- App Version: 0.42.4
- App Website: https://sourceforge.net/projects/libivykis/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ivykis with the following command
   ```
   brew install ivykis
   ```
4. ivykis is ready to use now!