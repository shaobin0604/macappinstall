---
title: "Install magic_enum on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Static reflection for enums (to string, from string, iteration) for modern C++"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install magic_enum on MacOS using homebrew

- App Name: magic_enum
- App description: Static reflection for enums (to string, from string, iteration) for modern C++
- App Version: 0.7.3
- App Website: https://github.com/Neargye/magic_enum

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install magic_enum with the following command
   ```
   brew install magic_enum
   ```
4. magic_enum is ready to use now!