---
title: "Install jvgrep on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Grep for Japanese users of Vim"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install jvgrep on MacOS using homebrew

- App Name: jvgrep
- App description: Grep for Japanese users of Vim
- App Version: 5.8.9
- App Website: https://github.com/mattn/jvgrep

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install jvgrep with the following command
   ```
   brew install jvgrep
   ```
4. jvgrep is ready to use now!