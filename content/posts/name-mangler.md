---
title: "Install Name Mangler on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Multi-file renaming tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Name Mangler on MacOS using homebrew

- App Name: Name Mangler
- App description: Multi-file renaming tool
- App Version: 3.7.3,3327
- App Website: https://manytricks.com/namemangler/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Name Mangler with the following command
   ```
   brew install --cask name-mangler
   ```
4. Name Mangler is ready to use now!