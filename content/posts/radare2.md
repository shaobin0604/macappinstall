---
title: "Install radare2 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Reverse engineering framework"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install radare2 on MacOS using homebrew

- App Name: radare2
- App description: Reverse engineering framework
- App Version: 5.6.2
- App Website: https://radare.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install radare2 with the following command
   ```
   brew install radare2
   ```
4. radare2 is ready to use now!