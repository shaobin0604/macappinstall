---
title: "Install xsane on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Graphical scanning frontend"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install xsane on MacOS using homebrew

- App Name: xsane
- App description: Graphical scanning frontend
- App Version: 0.999
- App Website: https://wiki.ubuntuusers.de/XSane/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install xsane with the following command
   ```
   brew install xsane
   ```
4. xsane is ready to use now!