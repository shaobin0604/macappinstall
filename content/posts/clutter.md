---
title: "Install clutter on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Generic high-level canvas library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install clutter on MacOS using homebrew

- App Name: clutter
- App description: Generic high-level canvas library
- App Version: 1.26.4
- App Website: https://wiki.gnome.org/Projects/Clutter

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install clutter with the following command
   ```
   brew install clutter
   ```
4. clutter is ready to use now!