---
title: "Install minipro on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Open controller for the MiniPRO TL866xx series of chip programmers"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install minipro on MacOS using homebrew

- App Name: minipro
- App description: Open controller for the MiniPRO TL866xx series of chip programmers
- App Version: 0.5
- App Website: https://gitlab.com/DavidGriffith/minipro/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install minipro with the following command
   ```
   brew install minipro
   ```
4. minipro is ready to use now!