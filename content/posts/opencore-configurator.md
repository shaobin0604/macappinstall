---
title: "Install OpenCore Configurator on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "OpenCore EFI bootloader configuration helper"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install OpenCore Configurator on MacOS using homebrew

- App Name: OpenCore Configurator
- App description: OpenCore EFI bootloader configuration helper
- App Version: 2.57.0.0
- App Website: https://mackie100projects.altervista.org/opencore-configurator/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install OpenCore Configurator with the following command
   ```
   brew install --cask opencore-configurator
   ```
4. OpenCore Configurator is ready to use now!