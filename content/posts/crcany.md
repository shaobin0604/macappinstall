---
title: "Install crcany on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Compute any CRC, a bit at a time, a byte at a time, and a word at a time"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install crcany on MacOS using homebrew

- App Name: crcany
- App description: Compute any CRC, a bit at a time, a byte at a time, and a word at a time
- App Version: 2.1
- App Website: https://github.com/madler/crcany

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install crcany with the following command
   ```
   brew install crcany
   ```
4. crcany is ready to use now!