---
title: "Install xmltoman on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "XML to manpage converter"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install xmltoman on MacOS using homebrew

- App Name: xmltoman
- App description: XML to manpage converter
- App Version: 0.4
- App Website: https://sourceforge.net/projects/xmltoman/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install xmltoman with the following command
   ```
   brew install xmltoman
   ```
4. xmltoman is ready to use now!