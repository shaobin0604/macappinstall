---
title: "Install My Budget on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Budgeting tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install My Budget on MacOS using homebrew

- App Name: My Budget
- App description: Budgeting tool
- App Version: 3.4.2-beta
- App Website: https://rezach.github.io/my-budget/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install My Budget with the following command
   ```
   brew install --cask my-budget
   ```
4. My Budget is ready to use now!