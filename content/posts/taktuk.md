---
title: "Install taktuk on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Deploy commands to (a potentially large set of) remote nodes"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install taktuk on MacOS using homebrew

- App Name: taktuk
- App description: Deploy commands to (a potentially large set of) remote nodes
- App Version: 3.7.7
- App Website: https://web.archive.org/web/20200806133931/https://taktuk.gforge.inria.fr/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install taktuk with the following command
   ```
   brew install taktuk
   ```
4. taktuk is ready to use now!