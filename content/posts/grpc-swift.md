---
title: "Install grpc-swift on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Swift language implementation of gRPC"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install grpc-swift on MacOS using homebrew

- App Name: grpc-swift
- App description: Swift language implementation of gRPC
- App Version: 1.7.0
- App Website: https://github.com/grpc/grpc-swift

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install grpc-swift with the following command
   ```
   brew install grpc-swift
   ```
4. grpc-swift is ready to use now!