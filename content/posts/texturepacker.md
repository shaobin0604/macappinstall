---
title: "Install TexturePacker on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Game sprite sheet packer"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install TexturePacker on MacOS using homebrew

- App Name: TexturePacker
- App description: Game sprite sheet packer
- App Version: 6.0.0
- App Website: https://www.codeandweb.com/texturepacker

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install TexturePacker with the following command
   ```
   brew install --cask texturepacker
   ```
4. TexturePacker is ready to use now!