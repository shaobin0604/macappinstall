---
title: "Install brew-cask-completion on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Fish completion for brew-cask"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install brew-cask-completion on MacOS using homebrew

- App Name: brew-cask-completion
- App description: Fish completion for brew-cask
- App Version: 2.1
- App Website: https://github.com/xyb/homebrew-cask-completion

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install brew-cask-completion with the following command
   ```
   brew install brew-cask-completion
   ```
4. brew-cask-completion is ready to use now!