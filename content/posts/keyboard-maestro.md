---
title: "Install Keyboard Maestro on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Automation software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Keyboard Maestro on MacOS using homebrew

- App Name: Keyboard Maestro
- App description: Automation software
- App Version: 10.0.2,1002
- App Website: https://www.keyboardmaestro.com/main/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Keyboard Maestro with the following command
   ```
   brew install --cask keyboard-maestro
   ```
4. Keyboard Maestro is ready to use now!