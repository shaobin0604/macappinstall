---
title: "Install Frescobaldi on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "LilyPond Editor"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Frescobaldi on MacOS using homebrew

- App Name: Frescobaldi
- App description: LilyPond Editor
- App Version: 3.1.3
- App Website: https://frescobaldi.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Frescobaldi with the following command
   ```
   brew install --cask frescobaldi
   ```
4. Frescobaldi is ready to use now!