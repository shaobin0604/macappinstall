---
title: "Install grip on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GitHub Markdown previewer"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install grip on MacOS using homebrew

- App Name: grip
- App description: GitHub Markdown previewer
- App Version: 4.6.0
- App Website: https://github.com/joeyespo/grip

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install grip with the following command
   ```
   brew install grip
   ```
4. grip is ready to use now!