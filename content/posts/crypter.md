---
title: "Install Crypter on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Encryption software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Crypter on MacOS using homebrew

- App Name: Crypter
- App description: Encryption software
- App Version: 5.0.0
- App Website: https://github.com/HR/Crypter

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Crypter with the following command
   ```
   brew install --cask crypter
   ```
4. Crypter is ready to use now!