---
title: "Install stm32flash on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Open source flash program for STM32 using the ST serial bootloader"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install stm32flash on MacOS using homebrew

- App Name: stm32flash
- App description: Open source flash program for STM32 using the ST serial bootloader
- App Version: 0.6
- App Website: https://sourceforge.net/projects/stm32flash/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install stm32flash with the following command
   ```
   brew install stm32flash
   ```
4. stm32flash is ready to use now!