---
title: "Install Workbench on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Seamless, automatic, “dotfile” sync to iCloud"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Workbench on MacOS using homebrew

- App Name: Workbench
- App description: Seamless, automatic, “dotfile” sync to iCloud
- App Version: 1.0.9
- App Website: https://github.com/mxcl/Workbench

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Workbench with the following command
   ```
   brew install --cask workbench
   ```
4. Workbench is ready to use now!