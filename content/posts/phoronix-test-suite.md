---
title: "Install phoronix-test-suite on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Open-source automated testing/benchmarking software"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install phoronix-test-suite on MacOS using homebrew

- App Name: phoronix-test-suite
- App description: Open-source automated testing/benchmarking software
- App Version: 10.8.2
- App Website: https://www.phoronix-test-suite.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install phoronix-test-suite with the following command
   ```
   brew install phoronix-test-suite
   ```
4. phoronix-test-suite is ready to use now!