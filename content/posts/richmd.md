---
title: "Install richmd on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Format Markdown in the terminal with Rich"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install richmd on MacOS using homebrew

- App Name: richmd
- App description: Format Markdown in the terminal with Rich
- App Version: 11.2.0
- App Website: https://github.com/willmcgugan/rich

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install richmd with the following command
   ```
   brew install richmd
   ```
4. richmd is ready to use now!