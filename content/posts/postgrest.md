---
title: "Install postgrest on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Serves a fully RESTful API from any existing PostgreSQL database"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install postgrest on MacOS using homebrew

- App Name: postgrest
- App description: Serves a fully RESTful API from any existing PostgreSQL database
- App Version: 9.0.0
- App Website: https://github.com/PostgREST/postgrest

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install postgrest with the following command
   ```
   brew install postgrest
   ```
4. postgrest is ready to use now!