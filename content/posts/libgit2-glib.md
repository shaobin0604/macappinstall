---
title: "Install libgit2-glib on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Glib wrapper library around libgit2 git access library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libgit2-glib on MacOS using homebrew

- App Name: libgit2-glib
- App description: Glib wrapper library around libgit2 git access library
- App Version: 1.0.0.1
- App Website: https://github.com/GNOME/libgit2-glib

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libgit2-glib with the following command
   ```
   brew install libgit2-glib
   ```
4. libgit2-glib is ready to use now!