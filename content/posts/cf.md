---
title: "Install cf on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Filter to replace numeric timestamps with a formatted date time"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cf on MacOS using homebrew

- App Name: cf
- App description: Filter to replace numeric timestamps with a formatted date time
- App Version: 1.2.5
- App Website: https://ee.lbl.gov/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cf with the following command
   ```
   brew install cf
   ```
4. cf is ready to use now!