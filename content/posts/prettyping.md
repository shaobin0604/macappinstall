---
title: "Install prettyping on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Wrapper to colorize and simplify ping's output"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install prettyping on MacOS using homebrew

- App Name: prettyping
- App description: Wrapper to colorize and simplify ping's output
- App Version: 1.0.1
- App Website: https://denilsonsa.github.io/prettyping/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install prettyping with the following command
   ```
   brew install prettyping
   ```
4. prettyping is ready to use now!