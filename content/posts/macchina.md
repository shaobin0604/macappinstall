---
title: "Install macchina on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "System information fetcher, with an emphasis on performance and minimalism"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install macchina on MacOS using homebrew

- App Name: macchina
- App description: System information fetcher, with an emphasis on performance and minimalism
- App Version: 6.0.6
- App Website: https://github.com/Macchina-CLI/macchina

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install macchina with the following command
   ```
   brew install macchina
   ```
4. macchina is ready to use now!