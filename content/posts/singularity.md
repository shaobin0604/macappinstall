---
title: "Install singularity on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Application container and unprivileged sandbox platform for Linux"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install singularity on MacOS using homebrew

- App Name: singularity
- App description: Application container and unprivileged sandbox platform for Linux
- App Version: 3.8.6
- App Website: https://singularity.hpcng.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install singularity with the following command
   ```
   brew install singularity
   ```
4. singularity is ready to use now!