---
title: "Install sng on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Enable lossless editing of PNGs via a textual representation"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sng on MacOS using homebrew

- App Name: sng
- App description: Enable lossless editing of PNGs via a textual representation
- App Version: 1.1.0
- App Website: https://sng.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sng with the following command
   ```
   brew install sng
   ```
4. sng is ready to use now!