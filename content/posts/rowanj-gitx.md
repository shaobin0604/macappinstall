---
title: "Install GitX-dev on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Native graphical client for the git version control system"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install GitX-dev on MacOS using homebrew

- App Name: GitX-dev
- App description: Native graphical client for the git version control system
- App Version: 0.15,1964
- App Website: https://rowanj.github.io/gitx/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install GitX-dev with the following command
   ```
   brew install --cask rowanj-gitx
   ```
4. GitX-dev is ready to use now!