---
title: "Install Gephi on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utility to visualize and explore GraphViz graphs"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Gephi on MacOS using homebrew

- App Name: Gephi
- App description: Utility to visualize and explore GraphViz graphs
- App Version: 0.9.2
- App Website: https://gephi.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Gephi with the following command
   ```
   brew install --cask gephi
   ```
4. Gephi is ready to use now!