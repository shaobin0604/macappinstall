---
title: "Install Mem on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Capture and access information from anywhere"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Mem on MacOS using homebrew

- App Name: Mem
- App description: Capture and access information from anywhere
- App Version: 0.28.26
- App Website: https://get.mem.ai/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Mem with the following command
   ```
   brew install --cask mem
   ```
4. Mem is ready to use now!