---
title: "Install apache-geode on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "In-memory Data Grid for fast transactional data processing"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install apache-geode on MacOS using homebrew

- App Name: apache-geode
- App description: In-memory Data Grid for fast transactional data processing
- App Version: 1.14.3
- App Website: https://geode.apache.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install apache-geode with the following command
   ```
   brew install apache-geode
   ```
4. apache-geode is ready to use now!