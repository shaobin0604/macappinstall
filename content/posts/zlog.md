---
title: "Install zlog on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "High-performance C logging library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install zlog on MacOS using homebrew

- App Name: zlog
- App description: High-performance C logging library
- App Version: 1.2.15
- App Website: https://github.com/HardySimpson/zlog

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install zlog with the following command
   ```
   brew install zlog
   ```
4. zlog is ready to use now!