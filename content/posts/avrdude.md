---
title: "Install avrdude on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Atmel AVR MCU programmer"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install avrdude on MacOS using homebrew

- App Name: avrdude
- App description: Atmel AVR MCU programmer
- App Version: 6.4
- App Website: https://savannah.nongnu.org/projects/avrdude/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install avrdude with the following command
   ```
   brew install avrdude
   ```
4. avrdude is ready to use now!