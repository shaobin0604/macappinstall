---
title: "Install ired on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Minimalistic hexadecimal editor designed to be used in scripts"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ired on MacOS using homebrew

- App Name: ired
- App description: Minimalistic hexadecimal editor designed to be used in scripts
- App Version: 0.6
- App Website: https://github.com/radare/ired

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ired with the following command
   ```
   brew install ired
   ```
4. ired is ready to use now!