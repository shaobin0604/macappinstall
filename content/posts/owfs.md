---
title: "Install owfs on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Monitor and control physical environment using Dallas/Maxim 1-wire system"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install owfs on MacOS using homebrew

- App Name: owfs
- App description: Monitor and control physical environment using Dallas/Maxim 1-wire system
- App Version: 3.2p4
- App Website: https://owfs.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install owfs with the following command
   ```
   brew install owfs
   ```
4. owfs is ready to use now!