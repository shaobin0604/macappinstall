---
title: "Install font-util on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "X.Org: Font package creation/installation utilities"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install font-util on MacOS using homebrew

- App Name: font-util
- App description: X.Org: Font package creation/installation utilities
- App Version: 1.3.2
- App Website: https://www.x.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install font-util with the following command
   ```
   brew install font-util
   ```
4. font-util is ready to use now!