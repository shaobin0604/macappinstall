---
title: "Install ftgl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Freetype / OpenGL bridge"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ftgl on MacOS using homebrew

- App Name: ftgl
- App description: Freetype / OpenGL bridge
- App Version: 2.1.3-rc5
- App Website: https://sourceforge.net/projects/ftgl/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ftgl with the following command
   ```
   brew install ftgl
   ```
4. ftgl is ready to use now!