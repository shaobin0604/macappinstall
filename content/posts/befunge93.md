---
title: "Install befunge93 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Esoteric programming language"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install befunge93 on MacOS using homebrew

- App Name: befunge93
- App description: Esoteric programming language
- App Version: 2.25
- App Website: https://catseye.tc/article/Languages.md#befunge-93

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install befunge93 with the following command
   ```
   brew install befunge93
   ```
4. befunge93 is ready to use now!