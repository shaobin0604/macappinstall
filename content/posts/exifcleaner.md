---
title: "Install ExifCleaner on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Metadata cleaner"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ExifCleaner on MacOS using homebrew

- App Name: ExifCleaner
- App description: Metadata cleaner
- App Version: 3.6.0
- App Website: https://exifcleaner.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ExifCleaner with the following command
   ```
   brew install --cask exifcleaner
   ```
4. ExifCleaner is ready to use now!