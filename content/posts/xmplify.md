---
title: "Install Xmplify on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "XML editor"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Xmplify on MacOS using homebrew

- App Name: Xmplify
- App description: XML editor
- App Version: 1.10.4
- App Website: http://xmplifyapp.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Xmplify with the following command
   ```
   brew install --cask xmplify
   ```
4. Xmplify is ready to use now!