---
title: "Install carrot2 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Search results clustering engine"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install carrot2 on MacOS using homebrew

- App Name: carrot2
- App description: Search results clustering engine
- App Version: 4.4.2
- App Website: https://search.carrot2.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install carrot2 with the following command
   ```
   brew install carrot2
   ```
4. carrot2 is ready to use now!