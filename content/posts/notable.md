---
title: "Install Notable on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Markdown-based note-taking app that doesn't suck"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Notable on MacOS using homebrew

- App Name: Notable
- App description: Markdown-based note-taking app that doesn't suck
- App Version: 1.8.4
- App Website: https://notable.app/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Notable with the following command
   ```
   brew install --cask notable
   ```
4. Notable is ready to use now!