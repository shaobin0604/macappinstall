---
title: "Install colordiff on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Color-highlighted diff(1) output"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install colordiff on MacOS using homebrew

- App Name: colordiff
- App description: Color-highlighted diff(1) output
- App Version: 1.0.20
- App Website: https://www.colordiff.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install colordiff with the following command
   ```
   brew install colordiff
   ```
4. colordiff is ready to use now!