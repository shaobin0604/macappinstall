---
title: "Install grokmirror on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Framework to smartly mirror git repositories"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install grokmirror on MacOS using homebrew

- App Name: grokmirror
- App description: Framework to smartly mirror git repositories
- App Version: 2.0.11
- App Website: https://github.com/mricon/grokmirror

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install grokmirror with the following command
   ```
   brew install grokmirror
   ```
4. grokmirror is ready to use now!