---
title: "Install GnuCash on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Double-entry accounting program"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install GnuCash on MacOS using homebrew

- App Name: GnuCash
- App description: Double-entry accounting program
- App Version: 4.9-1
- App Website: https://www.gnucash.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install GnuCash with the following command
   ```
   brew install --cask gnucash
   ```
4. GnuCash is ready to use now!