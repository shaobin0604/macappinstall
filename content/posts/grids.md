---
title: "Install Grids on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Instagram desktop application"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Grids on MacOS using homebrew

- App Name: Grids
- App description: Instagram desktop application
- App Version: 7.0.18
- App Website: https://gridsapp.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Grids with the following command
   ```
   brew install --cask grids
   ```
4. Grids is ready to use now!