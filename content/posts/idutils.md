---
title: "Install idutils on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "ID database and query tools"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install idutils on MacOS using homebrew

- App Name: idutils
- App description: ID database and query tools
- App Version: 4.6
- App Website: https://www.gnu.org/s/idutils/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install idutils with the following command
   ```
   brew install idutils
   ```
4. idutils is ready to use now!