---
title: "Install lexbor on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Fast embeddable web browser engine written in C with no dependencies"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install lexbor on MacOS using homebrew

- App Name: lexbor
- App description: Fast embeddable web browser engine written in C with no dependencies
- App Version: 2.1.0
- App Website: https://lexbor.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install lexbor with the following command
   ```
   brew install lexbor
   ```
4. lexbor is ready to use now!