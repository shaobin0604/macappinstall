---
title: "Install awscurl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Curl like simplicity to access AWS resources"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install awscurl on MacOS using homebrew

- App Name: awscurl
- App description: Curl like simplicity to access AWS resources
- App Version: 0.26
- App Website: https://github.com/okigan/awscurl

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install awscurl with the following command
   ```
   brew install awscurl
   ```
4. awscurl is ready to use now!