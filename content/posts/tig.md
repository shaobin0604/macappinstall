---
title: "Install tig on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Text interface for Git repositories"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tig on MacOS using homebrew

- App Name: tig
- App description: Text interface for Git repositories
- App Version: 2.5.5
- App Website: https://jonas.github.io/tig/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tig with the following command
   ```
   brew install tig
   ```
4. tig is ready to use now!