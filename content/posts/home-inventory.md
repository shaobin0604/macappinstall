---
title: "Install Home Inventory on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Documentation application for home and belongings"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Home Inventory on MacOS using homebrew

- App Name: Home Inventory
- App description: Documentation application for home and belongings
- App Version: 3.8.5,20201209
- App Website: https://binaryformations.com/products/home-inventory/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Home Inventory with the following command
   ```
   brew install --cask home-inventory
   ```
4. Home Inventory is ready to use now!