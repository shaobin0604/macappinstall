---
title: "Install mrbayes on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Bayesian inference of phylogenies and evolutionary models"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mrbayes on MacOS using homebrew

- App Name: mrbayes
- App description: Bayesian inference of phylogenies and evolutionary models
- App Version: 3.2.7
- App Website: https://nbisweden.github.io/MrBayes/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mrbayes with the following command
   ```
   brew install mrbayes
   ```
4. mrbayes is ready to use now!