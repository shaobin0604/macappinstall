---
title: "Install sevenzip on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "7-Zip is a file archiver with a high compression ratio"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sevenzip on MacOS using homebrew

- App Name: sevenzip
- App description: 7-Zip is a file archiver with a high compression ratio
- App Version: 21.07
- App Website: https://7-zip.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sevenzip with the following command
   ```
   brew install sevenzip
   ```
4. sevenzip is ready to use now!