---
title: "Install lbzip2 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Parallel bzip2 utility"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install lbzip2 on MacOS using homebrew

- App Name: lbzip2
- App description: Parallel bzip2 utility
- App Version: 2.5
- App Website: https://github.com/kjn/lbzip2

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install lbzip2 with the following command
   ```
   brew install lbzip2
   ```
4. lbzip2 is ready to use now!