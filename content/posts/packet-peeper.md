---
title: "Install Packet Peeper on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Network protocol analyzer"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Packet Peeper on MacOS using homebrew

- App Name: Packet Peeper
- App description: Network protocol analyzer
- App Version: 2021-07-18
- App Website: https://packetpeeper.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Packet Peeper with the following command
   ```
   brew install --cask packet-peeper
   ```
4. Packet Peeper is ready to use now!