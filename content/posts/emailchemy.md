---
title: "Install Emailchemy on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Email migration, conversion and archival software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Emailchemy on MacOS using homebrew

- App Name: Emailchemy
- App description: Email migration, conversion and archival software
- App Version: 14.4.5
- App Website: https://weirdkid.com/emailchemy/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Emailchemy with the following command
   ```
   brew install --cask emailchemy
   ```
4. Emailchemy is ready to use now!