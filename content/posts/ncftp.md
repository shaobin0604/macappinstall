---
title: "Install ncftp on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "FTP client with an advanced user interface"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ncftp on MacOS using homebrew

- App Name: ncftp
- App description: FTP client with an advanced user interface
- App Version: 3.2.6
- App Website: https://www.ncftp.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ncftp with the following command
   ```
   brew install ncftp
   ```
4. ncftp is ready to use now!