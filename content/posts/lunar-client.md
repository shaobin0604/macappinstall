---
title: "Install Lunar Client on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Modpack for Minecraft 1.7.10 and 1.8.9"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Lunar Client on MacOS using homebrew

- App Name: Lunar Client
- App description: Modpack for Minecraft 1.7.10 and 1.8.9
- App Version: 2.9.4
- App Website: https://www.lunarclient.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Lunar Client with the following command
   ```
   brew install --cask lunar-client
   ```
4. Lunar Client is ready to use now!