---
title: "Install esphome on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Make creating custom firmwares for ESP32/ESP8266 super easy"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install esphome on MacOS using homebrew

- App Name: esphome
- App description: Make creating custom firmwares for ESP32/ESP8266 super easy
- App Version: 2022.1.4
- App Website: https://github.com/esphome/esphome

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install esphome with the following command
   ```
   brew install esphome
   ```
4. esphome is ready to use now!