---
title: "Install prometheus on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Service monitoring system and time series database"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install prometheus on MacOS using homebrew

- App Name: prometheus
- App description: Service monitoring system and time series database
- App Version: 2.33.3
- App Website: https://prometheus.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install prometheus with the following command
   ```
   brew install prometheus
   ```
4. prometheus is ready to use now!