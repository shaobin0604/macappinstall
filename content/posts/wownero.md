---
title: "Install wownero on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Official wallet and node software for the Wownero cryptocurrency"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install wownero on MacOS using homebrew

- App Name: wownero
- App description: Official wallet and node software for the Wownero cryptocurrency
- App Version: 0.10.1.0
- App Website: https://wownero.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install wownero with the following command
   ```
   brew install wownero
   ```
4. wownero is ready to use now!