---
title: "Install GraphiQL App on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Light, Electron-based Wrapper around GraphiQL"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install GraphiQL App on MacOS using homebrew

- App Name: GraphiQL App
- App description: Light, Electron-based Wrapper around GraphiQL
- App Version: 0.7.2
- App Website: https://github.com/skevy/graphiql-app

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install GraphiQL App with the following command
   ```
   brew install --cask graphiql
   ```
4. GraphiQL App is ready to use now!