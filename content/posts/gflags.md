---
title: "Install gflags on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library for processing command-line flags"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gflags on MacOS using homebrew

- App Name: gflags
- App description: Library for processing command-line flags
- App Version: 2.2.2
- App Website: https://gflags.github.io/gflags/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gflags with the following command
   ```
   brew install gflags
   ```
4. gflags is ready to use now!