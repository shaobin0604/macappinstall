---
title: "Install libftdi on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library to talk to FTDI chips"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libftdi on MacOS using homebrew

- App Name: libftdi
- App description: Library to talk to FTDI chips
- App Version: 1.5
- App Website: https://www.intra2net.com/en/developer/libftdi

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libftdi with the following command
   ```
   brew install libftdi
   ```
4. libftdi is ready to use now!