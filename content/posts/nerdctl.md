---
title: "Install nerdctl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "ContaiNERD CTL - Docker-compatible CLI for containerd"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install nerdctl on MacOS using homebrew

- App Name: nerdctl
- App description: ContaiNERD CTL - Docker-compatible CLI for containerd
- App Version: 0.17.0
- App Website: https://github.com/containerd/nerdctl

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install nerdctl with the following command
   ```
   brew install nerdctl
   ```
4. nerdctl is ready to use now!