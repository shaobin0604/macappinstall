---
title: "Install Opera GX on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Alternate version of the Opera web browser to complement gaming"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Opera GX on MacOS using homebrew

- App Name: Opera GX
- App description: Alternate version of the Opera web browser to complement gaming
- App Version: 82.0.4227.50
- App Website: https://www.opera.com/gx

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Opera GX with the following command
   ```
   brew install --cask opera-gx
   ```
4. Opera GX is ready to use now!