---
title: "Install libvterm on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C99 library which implements a VT220 or xterm terminal emulator"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libvterm on MacOS using homebrew

- App Name: libvterm
- App description: C99 library which implements a VT220 or xterm terminal emulator
- App Version: 0.2
- App Website: http://www.leonerd.org.uk/code/libvterm/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libvterm with the following command
   ```
   brew install libvterm
   ```
4. libvterm is ready to use now!