---
title: "Install Spatial on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Binary for interaction with the Improbable SpatialOS platform"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Spatial on MacOS using homebrew

- App Name: Spatial
- App description: Binary for interaction with the Improbable SpatialOS platform
- App Version: latest
- App Website: https://documentation.improbable.io/spatialos-overview/docs

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Spatial with the following command
   ```
   brew install --cask spatial
   ```
4. Spatial is ready to use now!