---
title: "Install evernote2md on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Convert Evernote .enex file to Markdown"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install evernote2md on MacOS using homebrew

- App Name: evernote2md
- App description: Convert Evernote .enex file to Markdown
- App Version: 0.17.1
- App Website: https://github.com/wormi4ok/evernote2md

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install evernote2md with the following command
   ```
   brew install evernote2md
   ```
4. evernote2md is ready to use now!