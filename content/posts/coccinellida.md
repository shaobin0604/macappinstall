---
title: "Install Coccinellida on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Simple SSH tunnel manager"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Coccinellida on MacOS using homebrew

- App Name: Coccinellida
- App description: Simple SSH tunnel manager
- App Version: 0.7
- App Website: https://coccinellida.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Coccinellida with the following command
   ```
   brew install --cask coccinellida
   ```
4. Coccinellida is ready to use now!