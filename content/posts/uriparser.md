---
title: "Install uriparser on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "URI parsing library (strictly RFC 3986 compliant)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install uriparser on MacOS using homebrew

- App Name: uriparser
- App description: URI parsing library (strictly RFC 3986 compliant)
- App Version: 0.9.6
- App Website: https://uriparser.github.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install uriparser with the following command
   ```
   brew install uriparser
   ```
4. uriparser is ready to use now!