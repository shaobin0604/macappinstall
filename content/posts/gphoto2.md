---
title: "Install gphoto2 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line interface to libgphoto2"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gphoto2 on MacOS using homebrew

- App Name: gphoto2
- App description: Command-line interface to libgphoto2
- App Version: 2.5.28
- App Website: http://www.gphoto.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gphoto2 with the following command
   ```
   brew install gphoto2
   ```
4. gphoto2 is ready to use now!