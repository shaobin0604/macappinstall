---
title: "Install Augur on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "App that bundles Augur UI and ugur Node together and deploys them locally"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Augur on MacOS using homebrew

- App Name: Augur
- App description: App that bundles Augur UI and ugur Node together and deploys them locally
- App Version: 1.16.11
- App Website: https://github.com/AugurProject/augur-app/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Augur with the following command
   ```
   brew install --cask augur
   ```
4. Augur is ready to use now!