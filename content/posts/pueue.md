---
title: "Install pueue on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line tool for managing long-running shell commands"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pueue on MacOS using homebrew

- App Name: pueue
- App description: Command-line tool for managing long-running shell commands
- App Version: 1.0.6
- App Website: https://github.com/Nukesor/pueue

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pueue with the following command
   ```
   brew install pueue
   ```
4. pueue is ready to use now!