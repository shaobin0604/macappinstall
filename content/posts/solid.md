---
title: "Install solid on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Collision detection library for geometric objects in 3D space"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install solid on MacOS using homebrew

- App Name: solid
- App description: Collision detection library for geometric objects in 3D space
- App Version: 3.5.8
- App Website: https://github.com/dtecta/solid3/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install solid with the following command
   ```
   brew install solid
   ```
4. solid is ready to use now!