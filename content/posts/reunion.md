---
title: "Install Reunion on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Genealogy (family tree) app"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Reunion on MacOS using homebrew

- App Name: Reunion
- App description: Genealogy (family tree) app
- App Version: 13.0.0,211116unr
- App Website: https://www.leisterpro.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Reunion with the following command
   ```
   brew install --cask reunion
   ```
4. Reunion is ready to use now!