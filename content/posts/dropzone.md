---
title: "Install Dropzone on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Productivity app"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Dropzone on MacOS using homebrew

- App Name: Dropzone
- App description: Productivity app
- App Version: 4.2.5,1475
- App Website: https://aptonic.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Dropzone with the following command
   ```
   brew install --cask dropzone
   ```
4. Dropzone is ready to use now!