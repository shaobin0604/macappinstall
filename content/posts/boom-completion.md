---
title: "Install boom-completion on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Bash and Zsh completion for Boom"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install boom-completion on MacOS using homebrew

- App Name: boom-completion
- App description: Bash and Zsh completion for Boom
- App Version: 0.5.0
- App Website: https://zachholman.com/boom

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install boom-completion with the following command
   ```
   brew install boom-completion
   ```
4. boom-completion is ready to use now!