---
title: "Install jo on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "JSON output from a shell"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install jo on MacOS using homebrew

- App Name: jo
- App description: JSON output from a shell
- App Version: 1.6
- App Website: https://github.com/jpmens/jo

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install jo with the following command
   ```
   brew install jo
   ```
4. jo is ready to use now!