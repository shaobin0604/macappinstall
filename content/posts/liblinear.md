---
title: "Install liblinear on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library for large linear classification"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install liblinear on MacOS using homebrew

- App Name: liblinear
- App description: Library for large linear classification
- App Version: 2.43
- App Website: https://www.csie.ntu.edu.tw/~cjlin/liblinear/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install liblinear with the following command
   ```
   brew install liblinear
   ```
4. liblinear is ready to use now!