---
title: "Install Osculator on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Connect MIDI and OSC Controllers"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Osculator on MacOS using homebrew

- App Name: Osculator
- App description: Connect MIDI and OSC Controllers
- App Version: 3.4.3-11-g8d8b1d0c,9c7ed436-cd8e-4c4f-bc69-29b56674467f
- App Website: https://osculator.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Osculator with the following command
   ```
   brew install --cask osculator
   ```
4. Osculator is ready to use now!