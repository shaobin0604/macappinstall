---
title: "Install svg2pdf on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Renders SVG images to a PDF file (using Cairo)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install svg2pdf on MacOS using homebrew

- App Name: svg2pdf
- App description: Renders SVG images to a PDF file (using Cairo)
- App Version: 0.1.3
- App Website: https://cairographics.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install svg2pdf with the following command
   ```
   brew install svg2pdf
   ```
4. svg2pdf is ready to use now!