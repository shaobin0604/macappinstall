---
title: "Install keychain on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "User-friendly front-end to ssh-agent(1)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install keychain on MacOS using homebrew

- App Name: keychain
- App description: User-friendly front-end to ssh-agent(1)
- App Version: 2.8.5
- App Website: https://www.funtoo.org/Keychain

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install keychain with the following command
   ```
   brew install keychain
   ```
4. keychain is ready to use now!