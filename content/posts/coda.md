---
title: "Install Panic Coda on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Text editor"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Panic Coda on MacOS using homebrew

- App Name: Panic Coda
- App description: Text editor
- App Version: 2.7.7,217503
- App Website: https://panic.com/coda/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Panic Coda with the following command
   ```
   brew install --cask coda
   ```
4. Panic Coda is ready to use now!