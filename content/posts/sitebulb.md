---
title: "Install Sitebulb on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Website auditing tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Sitebulb on MacOS using homebrew

- App Name: Sitebulb
- App description: Website auditing tool
- App Version: 5.5
- App Website: https://sitebulb.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Sitebulb with the following command
   ```
   brew install --cask sitebulb
   ```
4. Sitebulb is ready to use now!