---
title: "Install Video DownloadHelper Companion App on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Allows video downloads from the Web"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Video DownloadHelper Companion App on MacOS using homebrew

- App Name: Video DownloadHelper Companion App
- App description: Allows video downloads from the Web
- App Version: 1.6.3
- App Website: https://www.downloadhelper.net/install-coapp

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Video DownloadHelper Companion App with the following command
   ```
   brew install --cask netdownloadhelpercoapp
   ```
4. Video DownloadHelper Companion App is ready to use now!