---
title: "Install cfv on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Test and create various files (e.g., .sfv, .csv, .crc., .torrent)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cfv on MacOS using homebrew

- App Name: cfv
- App description: Test and create various files (e.g., .sfv, .csv, .crc., .torrent)
- App Version: 1.18.3
- App Website: https://cfv.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cfv with the following command
   ```
   brew install cfv
   ```
4. cfv is ready to use now!