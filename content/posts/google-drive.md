---
title: "Install Google Drive on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Client for the Google Drive storage service"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Google Drive on MacOS using homebrew

- App Name: Google Drive
- App description: Client for the Google Drive storage service
- App Version: 55.0.3
- App Website: https://www.google.com/drive/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Google Drive with the following command
   ```
   brew install --cask google-drive
   ```
4. Google Drive is ready to use now!