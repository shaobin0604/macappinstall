---
title: "Install knot-resolver on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Minimalistic, caching, DNSSEC-validating DNS resolver"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install knot-resolver on MacOS using homebrew

- App Name: knot-resolver
- App description: Minimalistic, caching, DNSSEC-validating DNS resolver
- App Version: 5.4.4
- App Website: https://www.knot-resolver.cz

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install knot-resolver with the following command
   ```
   brew install knot-resolver
   ```
4. knot-resolver is ready to use now!