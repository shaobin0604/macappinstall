---
title: "Install pony-stable on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Dependency manager for the Pony language"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pony-stable on MacOS using homebrew

- App Name: pony-stable
- App description: Dependency manager for the Pony language
- App Version: 0.2.2
- App Website: https://github.com/ponylang/pony-stable

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pony-stable with the following command
   ```
   brew install pony-stable
   ```
4. pony-stable is ready to use now!