---
title: "Install cloudiscovery on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Help you discover resources in the cloud environment"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cloudiscovery on MacOS using homebrew

- App Name: cloudiscovery
- App description: Help you discover resources in the cloud environment
- App Version: 2.4.4
- App Website: https://github.com/Cloud-Architects/cloudiscovery

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cloudiscovery with the following command
   ```
   brew install cloudiscovery
   ```
4. cloudiscovery is ready to use now!