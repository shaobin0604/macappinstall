---
title: "Install Rocket on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Emoji picker optimized for blind people"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Rocket on MacOS using homebrew

- App Name: Rocket
- App description: Emoji picker optimized for blind people
- App Version: 1.9,80
- App Website: https://matthewpalmer.net/rocket/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Rocket with the following command
   ```
   brew install --cask rocket
   ```
4. Rocket is ready to use now!