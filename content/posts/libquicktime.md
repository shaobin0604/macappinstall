---
title: "Install libquicktime on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library for reading and writing quicktime files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libquicktime on MacOS using homebrew

- App Name: libquicktime
- App description: Library for reading and writing quicktime files
- App Version: 1.2.4
- App Website: https://libquicktime.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libquicktime with the following command
   ```
   brew install libquicktime
   ```
4. libquicktime is ready to use now!