---
title: "Install cgdb on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Curses-based interface to the GNU Debugger"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cgdb on MacOS using homebrew

- App Name: cgdb
- App description: Curses-based interface to the GNU Debugger
- App Version: 0.8.0
- App Website: https://cgdb.github.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cgdb with the following command
   ```
   brew install cgdb
   ```
4. cgdb is ready to use now!