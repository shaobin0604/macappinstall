---
title: "Install findent on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Indent and beautify Fortran sources and generate dependency information"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install findent on MacOS using homebrew

- App Name: findent
- App description: Indent and beautify Fortran sources and generate dependency information
- App Version: 4.1.3
- App Website: https://www.ratrabbit.nl/ratrabbit/findent/index.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install findent with the following command
   ```
   brew install findent
   ```
4. findent is ready to use now!