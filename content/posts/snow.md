---
title: "Install snow on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Whitespace steganography: coded messages using whitespace"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install snow on MacOS using homebrew

- App Name: snow
- App description: Whitespace steganography: coded messages using whitespace
- App Version: 20130616
- App Website: https://web.archive.org/web/20200701063014/www.darkside.com.au/snow/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install snow with the following command
   ```
   brew install snow
   ```
4. snow is ready to use now!