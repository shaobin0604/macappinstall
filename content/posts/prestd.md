---
title: "Install prestd on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Simplify and accelerate development on any Postgres application, existing or new"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install prestd on MacOS using homebrew

- App Name: prestd
- App description: Simplify and accelerate development on any Postgres application, existing or new
- App Version: 1.0.14
- App Website: https://github.com/prest/prest

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install prestd with the following command
   ```
   brew install prestd
   ```
4. prestd is ready to use now!