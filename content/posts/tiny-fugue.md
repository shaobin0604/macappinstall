---
title: "Install tiny-fugue on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Programmable MUD client"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tiny-fugue on MacOS using homebrew

- App Name: tiny-fugue
- App description: Programmable MUD client
- App Version: 5.0b8
- App Website: https://tinyfugue.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tiny-fugue with the following command
   ```
   brew install tiny-fugue
   ```
4. tiny-fugue is ready to use now!