---
title: "Install Neon Wallet on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Light wallet for the NEO blockchain"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Neon Wallet on MacOS using homebrew

- App Name: Neon Wallet
- App description: Light wallet for the NEO blockchain
- App Version: 2.12.4
- App Website: https://github.com/CityOfZion/neon-wallet

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Neon Wallet with the following command
   ```
   brew install --cask cityofzion-neon
   ```
4. Neon Wallet is ready to use now!