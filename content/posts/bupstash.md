---
title: "Install bupstash on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Easy and efficient encrypted backups"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bupstash on MacOS using homebrew

- App Name: bupstash
- App description: Easy and efficient encrypted backups
- App Version: 0.10.3
- App Website: https://bupstash.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bupstash with the following command
   ```
   brew install bupstash
   ```
4. bupstash is ready to use now!