---
title: "Install scm-manager on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Manage Git, Mercurial, and Subversion repos over HTTP"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install scm-manager on MacOS using homebrew

- App Name: scm-manager
- App description: Manage Git, Mercurial, and Subversion repos over HTTP
- App Version: 1.59
- App Website: https://www.scm-manager.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install scm-manager with the following command
   ```
   brew install scm-manager
   ```
4. scm-manager is ready to use now!