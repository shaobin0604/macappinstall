---
title: "Install Gitify on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "App that shows GitHub notifications on the desktop"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Gitify on MacOS using homebrew

- App Name: Gitify
- App description: App that shows GitHub notifications on the desktop
- App Version: 4.3.1
- App Website: https://github.com/manosim/gitify

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Gitify with the following command
   ```
   brew install --cask gitify
   ```
4. Gitify is ready to use now!