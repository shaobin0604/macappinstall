---
title: "Install mon on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Monitor hosts/services/whatever and alert about problems"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mon on MacOS using homebrew

- App Name: mon
- App description: Monitor hosts/services/whatever and alert about problems
- App Version: 1.2.3
- App Website: https://github.com/visionmedia/mon

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mon with the following command
   ```
   brew install mon
   ```
4. mon is ready to use now!