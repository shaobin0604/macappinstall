---
title: "Install darkstat on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Network traffic analyzer"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install darkstat on MacOS using homebrew

- App Name: darkstat
- App description: Network traffic analyzer
- App Version: 3.0.721
- App Website: https://unix4lyfe.org/darkstat/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install darkstat with the following command
   ```
   brew install darkstat
   ```
4. darkstat is ready to use now!