---
title: "Install tcptrace on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Analyze tcpdump output"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tcptrace on MacOS using homebrew

- App Name: tcptrace
- App description: Analyze tcpdump output
- App Version: 6.6.7
- App Website: http://www.tcptrace.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tcptrace with the following command
   ```
   brew install tcptrace
   ```
4. tcptrace is ready to use now!