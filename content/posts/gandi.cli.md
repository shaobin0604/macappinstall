---
title: "Install gandi.cli on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line interface to Gandi.net products using the public API"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gandi.cli on MacOS using homebrew

- App Name: gandi.cli
- App description: Command-line interface to Gandi.net products using the public API
- App Version: 1.6
- App Website: https://cli.gandi.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gandi.cli with the following command
   ```
   brew install gandi.cli
   ```
4. gandi.cli is ready to use now!