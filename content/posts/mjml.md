---
title: "Install MJML on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Desktop app for MJML"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install MJML on MacOS using homebrew

- App Name: MJML
- App description: Desktop app for MJML
- App Version: 3.0.3
- App Website: https://mjmlio.github.io/mjml-app/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install MJML with the following command
   ```
   brew install --cask mjml
   ```
4. MJML is ready to use now!