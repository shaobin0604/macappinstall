---
title: "Install phpbrew on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Brew & manage PHP versions in pure PHP at HOME"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install phpbrew on MacOS using homebrew

- App Name: phpbrew
- App description: Brew & manage PHP versions in pure PHP at HOME
- App Version: 1.27.0
- App Website: https://phpbrew.github.io/phpbrew

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install phpbrew with the following command
   ```
   brew install phpbrew
   ```
4. phpbrew is ready to use now!