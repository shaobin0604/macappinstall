---
title: "Install cosign on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Container Signing"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cosign on MacOS using homebrew

- App Name: cosign
- App description: Container Signing
- App Version: 1.5.1
- App Website: https://github.com/sigstore/cosign

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cosign with the following command
   ```
   brew install cosign
   ```
4. cosign is ready to use now!