---
title: "Install The Watcher on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Monitor ETH wallets, USD-ETH prices and gas prices"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install The Watcher on MacOS using homebrew

- App Name: The Watcher
- App description: Monitor ETH wallets, USD-ETH prices and gas prices
- App Version: 1.2
- App Website: https://watcher.app/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install The Watcher with the following command
   ```
   brew install --cask the-watcher
   ```
4. The Watcher is ready to use now!