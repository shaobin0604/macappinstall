---
title: "Install libvnc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cross-platform C libraries for easy implementation of VNC server or client"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libvnc on MacOS using homebrew

- App Name: libvnc
- App description: Cross-platform C libraries for easy implementation of VNC server or client
- App Version: 0.9.13
- App Website: https://libvnc.github.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libvnc with the following command
   ```
   brew install libvnc
   ```
4. libvnc is ready to use now!