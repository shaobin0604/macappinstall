---
title: "Install jflex on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Lexical analyzer generator for Java, written in Java"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install jflex on MacOS using homebrew

- App Name: jflex
- App description: Lexical analyzer generator for Java, written in Java
- App Version: 1.8.2
- App Website: https://jflex.de/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install jflex with the following command
   ```
   brew install jflex
   ```
4. jflex is ready to use now!