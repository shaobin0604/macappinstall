---
title: "Install Apple Events on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Unofficial Apple Events app"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Apple Events on MacOS using homebrew

- App Name: Apple Events
- App description: Unofficial Apple Events app
- App Version: 1.6
- App Website: https://github.com/insidegui/AppleEvents

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Apple Events with the following command
   ```
   brew install --cask apple-events
   ```
4. Apple Events is ready to use now!