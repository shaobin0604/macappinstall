---
title: "Install two-lame on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Optimized MPEG Audio Layer 2 (MP2) encoder"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install two-lame on MacOS using homebrew

- App Name: two-lame
- App description: Optimized MPEG Audio Layer 2 (MP2) encoder
- App Version: 0.4.0
- App Website: https://www.twolame.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install two-lame with the following command
   ```
   brew install two-lame
   ```
4. two-lame is ready to use now!