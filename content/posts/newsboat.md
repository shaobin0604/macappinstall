---
title: "Install newsboat on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "RSS/Atom feed reader for text terminals"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install newsboat on MacOS using homebrew

- App Name: newsboat
- App description: RSS/Atom feed reader for text terminals
- App Version: 2.26
- App Website: https://newsboat.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install newsboat with the following command
   ```
   brew install newsboat
   ```
4. newsboat is ready to use now!