---
title: "Install leakcanary-shark on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CLI Java memory leak explorer for LeakCanary"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install leakcanary-shark on MacOS using homebrew

- App Name: leakcanary-shark
- App description: CLI Java memory leak explorer for LeakCanary
- App Version: 2.8.1
- App Website: https://square.github.io/leakcanary/shark/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install leakcanary-shark with the following command
   ```
   brew install leakcanary-shark
   ```
4. leakcanary-shark is ready to use now!