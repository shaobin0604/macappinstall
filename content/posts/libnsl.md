---
title: "Install libnsl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Public client interface for NIS(YP) and NIS+"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libnsl on MacOS using homebrew

- App Name: libnsl
- App description: Public client interface for NIS(YP) and NIS+
- App Version: 2.0.0
- App Website: https://github.com/thkukuk/libnsl

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libnsl with the following command
   ```
   brew install libnsl
   ```
4. libnsl is ready to use now!