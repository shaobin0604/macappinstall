---
title: "Install visp on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Visual Servoing Platform library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install visp on MacOS using homebrew

- App Name: visp
- App description: Visual Servoing Platform library
- App Version: 3.5.0
- App Website: https://visp.inria.fr/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install visp with the following command
   ```
   brew install visp
   ```
4. visp is ready to use now!