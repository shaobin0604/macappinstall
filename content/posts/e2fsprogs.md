---
title: "Install e2fsprogs on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utilities for the ext2, ext3, and ext4 file systems"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install e2fsprogs on MacOS using homebrew

- App Name: e2fsprogs
- App description: Utilities for the ext2, ext3, and ext4 file systems
- App Version: 1.46.4
- App Website: https://e2fsprogs.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install e2fsprogs with the following command
   ```
   brew install e2fsprogs
   ```
4. e2fsprogs is ready to use now!