---
title: "Install gti on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "ASCII-art displaying typo-corrector for commands"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gti on MacOS using homebrew

- App Name: gti
- App description: ASCII-art displaying typo-corrector for commands
- App Version: 1.7.0
- App Website: https://r-wos.org/hacks/gti

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gti with the following command
   ```
   brew install gti
   ```
4. gti is ready to use now!