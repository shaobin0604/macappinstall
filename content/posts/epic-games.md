---
title: "Install Epic Games Launcher on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Launcher for *Epic Games* games"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Epic Games Launcher on MacOS using homebrew

- App Name: Epic Games Launcher
- App description: Launcher for *Epic Games* games
- App Version: 13.0.0
- App Website: https://www.epicgames.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Epic Games Launcher with the following command
   ```
   brew install --cask epic-games
   ```
4. Epic Games Launcher is ready to use now!