---
title: "Install emacsclient on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Chrome/Firefox extension that facilitates org-capture in emacs"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install emacsclient on MacOS using homebrew

- App Name: emacsclient
- App description: Chrome/Firefox extension that facilitates org-capture in emacs
- App Version: 1.0
- App Website: https://github.com/sprig/org-capture-extension

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install emacsclient with the following command
   ```
   brew install --cask emacsclient
   ```
4. emacsclient is ready to use now!