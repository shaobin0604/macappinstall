---
title: "Install OpenIn on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Open links/files with specific browsers/apps"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install OpenIn on MacOS using homebrew

- App Name: OpenIn
- App description: Open links/files with specific browsers/apps
- App Version: 3.0.3
- App Website: https://loshadki.app/openin/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install OpenIn with the following command
   ```
   brew install --cask openin
   ```
4. OpenIn is ready to use now!