---
title: "Install cquery on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C/C++ language server"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cquery on MacOS using homebrew

- App Name: cquery
- App description: C/C++ language server
- App Version: 20180718
- App Website: https://github.com/cquery-project/cquery

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cquery with the following command
   ```
   brew install cquery
   ```
4. cquery is ready to use now!