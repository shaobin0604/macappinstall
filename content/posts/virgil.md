---
title: "Install virgil on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CLI tool to manage your Virgil account and applications"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install virgil on MacOS using homebrew

- App Name: virgil
- App description: CLI tool to manage your Virgil account and applications
- App Version: 5.2.9
- App Website: https://github.com/VirgilSecurity/virgil-cli

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install virgil with the following command
   ```
   brew install virgil
   ```
4. virgil is ready to use now!