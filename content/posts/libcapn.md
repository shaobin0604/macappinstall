---
title: "Install libcapn on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C library to send push notifications to Apple devices"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libcapn on MacOS using homebrew

- App Name: libcapn
- App description: C library to send push notifications to Apple devices
- App Version: 2.0.0
- App Website: https://web.archive.org/web/20181220090839/libcapn.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libcapn with the following command
   ```
   brew install libcapn
   ```
4. libcapn is ready to use now!