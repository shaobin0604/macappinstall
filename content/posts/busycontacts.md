---
title: "Install BusyContacts on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Contact manager focusing on efficiency"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install BusyContacts on MacOS using homebrew

- App Name: BusyContacts
- App description: Contact manager focusing on efficiency
- App Version: 1.6.4,2022-01-26-09-15
- App Website: https://www.busymac.com/busycontacts/index.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install BusyContacts with the following command
   ```
   brew install --cask busycontacts
   ```
4. BusyContacts is ready to use now!