---
title: "Install Wormhole on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Browse & Control phone on PC, Screen Fusion for iOS & Android"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Wormhole on MacOS using homebrew

- App Name: Wormhole
- App description: Browse & Control phone on PC, Screen Fusion for iOS & Android
- App Version: 1.5.8
- App Website: https://er.run/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Wormhole with the following command
   ```
   brew install --cask wormhole
   ```
4. Wormhole is ready to use now!