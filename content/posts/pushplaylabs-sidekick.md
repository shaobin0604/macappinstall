---
title: "Install Sidekick on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Browser designed for modern work"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Sidekick on MacOS using homebrew

- App Name: Sidekick
- App description: Browser designed for modern work
- App Version: 96.15.1.15332,51480cd
- App Website: https://www.meetsidekick.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Sidekick with the following command
   ```
   brew install --cask pushplaylabs-sidekick
   ```
4. Sidekick is ready to use now!