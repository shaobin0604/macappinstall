---
title: "Install speedtest-cli on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line interface for https://speedtest.net bandwidth tests"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install speedtest-cli on MacOS using homebrew

- App Name: speedtest-cli
- App description: Command-line interface for https://speedtest.net bandwidth tests
- App Version: 2.1.3
- App Website: https://github.com/sivel/speedtest-cli

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install speedtest-cli with the following command
   ```
   brew install speedtest-cli
   ```
4. speedtest-cli is ready to use now!