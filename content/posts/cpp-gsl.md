---
title: "Install cpp-gsl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Microsoft's C++ Guidelines Support Library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cpp-gsl on MacOS using homebrew

- App Name: cpp-gsl
- App description: Microsoft's C++ Guidelines Support Library
- App Version: 4.0.0
- App Website: https://github.com/Microsoft/GSL

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cpp-gsl with the following command
   ```
   brew install cpp-gsl
   ```
4. cpp-gsl is ready to use now!