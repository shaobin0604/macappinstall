---
title: "Install Goofy on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Desktop client for Facebook Messenger"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Goofy on MacOS using homebrew

- App Name: Goofy
- App description: Desktop client for Facebook Messenger
- App Version: 3.5.4
- App Website: https://www.goofyapp.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Goofy with the following command
   ```
   brew install --cask goofy
   ```
4. Goofy is ready to use now!