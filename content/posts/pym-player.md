---
title: "Install PYM Player on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Media player that automatically searches for subtitles"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install PYM Player on MacOS using homebrew

- App Name: PYM Player
- App description: Media player that automatically searches for subtitles
- App Version: 7.1.2,7B68
- App Website: http://pym.uce.pl/pym-player/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install PYM Player with the following command
   ```
   brew install --cask pym-player
   ```
4. PYM Player is ready to use now!