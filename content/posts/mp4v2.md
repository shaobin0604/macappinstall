---
title: "Install mp4v2 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Read, create, and modify MP4 files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mp4v2 on MacOS using homebrew

- App Name: mp4v2
- App description: Read, create, and modify MP4 files
- App Version: 2.0.0
- App Website: https://code.google.com/archive/p/mp4v2/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mp4v2 with the following command
   ```
   brew install mp4v2
   ```
4. mp4v2 is ready to use now!