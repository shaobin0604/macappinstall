---
title: "Install typespeed on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Zap words flying across the screen by typing them correctly"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install typespeed on MacOS using homebrew

- App Name: typespeed
- App description: Zap words flying across the screen by typing them correctly
- App Version: 0.6.5
- App Website: https://typespeed.sourceforge.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install typespeed with the following command
   ```
   brew install typespeed
   ```
4. typespeed is ready to use now!