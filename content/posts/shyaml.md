---
title: "Install shyaml on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line YAML parser"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install shyaml on MacOS using homebrew

- App Name: shyaml
- App description: Command-line YAML parser
- App Version: 0.6.2
- App Website: https://github.com/0k/shyaml

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install shyaml with the following command
   ```
   brew install shyaml
   ```
4. shyaml is ready to use now!