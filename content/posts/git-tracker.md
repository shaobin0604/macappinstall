---
title: "Install git-tracker on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Integrate Pivotal Tracker into your Git workflow"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install git-tracker on MacOS using homebrew

- App Name: git-tracker
- App description: Integrate Pivotal Tracker into your Git workflow
- App Version: 2.0.0
- App Website: https://github.com/stevenharman/git_tracker

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install git-tracker with the following command
   ```
   brew install git-tracker
   ```
4. git-tracker is ready to use now!