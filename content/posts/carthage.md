---
title: "Install carthage on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Decentralized dependency manager for Cocoa"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install carthage on MacOS using homebrew

- App Name: carthage
- App description: Decentralized dependency manager for Cocoa
- App Version: 0.38.0
- App Website: https://github.com/Carthage/Carthage

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install carthage with the following command
   ```
   brew install carthage
   ```
4. carthage is ready to use now!