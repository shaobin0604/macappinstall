---
title: "Install falcon on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Multi-paradigm programming language and scripting engine"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install falcon on MacOS using homebrew

- App Name: falcon
- App description: Multi-paradigm programming language and scripting engine
- App Version: 0.9.6.8
- App Website: http://www.falconpl.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install falcon with the following command
   ```
   brew install falcon
   ```
4. falcon is ready to use now!