---
title: "Install Ramme on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Unofficial Instagram Desktop App"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Ramme on MacOS using homebrew

- App Name: Ramme
- App description: Unofficial Instagram Desktop App
- App Version: 3.2.5
- App Website: https://github.com/terkelg/ramme/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Ramme with the following command
   ```
   brew install --cask ramme
   ```
4. Ramme is ready to use now!