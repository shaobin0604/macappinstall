---
title: "Install http-prompt on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Interactive command-line HTTP client with autocomplete and syntax highlighting"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install http-prompt on MacOS using homebrew

- App Name: http-prompt
- App description: Interactive command-line HTTP client with autocomplete and syntax highlighting
- App Version: 2.1.0
- App Website: https://http-prompt.com

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install http-prompt with the following command
   ```
   brew install http-prompt
   ```
4. http-prompt is ready to use now!