---
title: "Install or-tools on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Google's Operations Research tools"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install or-tools on MacOS using homebrew

- App Name: or-tools
- App description: Google's Operations Research tools
- App Version: 9.2
- App Website: https://developers.google.com/optimization/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install or-tools with the following command
   ```
   brew install or-tools
   ```
4. or-tools is ready to use now!