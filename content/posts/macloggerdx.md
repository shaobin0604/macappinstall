---
title: "Install MacLoggerDX on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Ham radio logging and rig control software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install MacLoggerDX on MacOS using homebrew

- App Name: MacLoggerDX
- App description: Ham radio logging and rig control software
- App Version: 6.38
- App Website: https://www.dogparksoftware.com/MacLoggerDX.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install MacLoggerDX with the following command
   ```
   brew install --cask macloggerdx
   ```
4. MacLoggerDX is ready to use now!