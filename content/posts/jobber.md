---
title: "Install jobber on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Alternative to cron, with better status-reporting and error-handling"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install jobber on MacOS using homebrew

- App Name: jobber
- App description: Alternative to cron, with better status-reporting and error-handling
- App Version: 1.4.4
- App Website: https://dshearer.github.io/jobber/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install jobber with the following command
   ```
   brew install jobber
   ```
4. jobber is ready to use now!