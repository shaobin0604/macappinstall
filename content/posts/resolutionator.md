---
title: "Install Resolutionator on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Use any of your display's available resolutions"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Resolutionator on MacOS using homebrew

- App Name: Resolutionator
- App description: Use any of your display's available resolutions
- App Version: 2.2.0,122
- App Website: https://manytricks.com/resolutionator/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Resolutionator with the following command
   ```
   brew install --cask resolutionator
   ```
4. Resolutionator is ready to use now!