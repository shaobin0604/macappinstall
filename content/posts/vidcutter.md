---
title: "Install VidCutter on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Media cutter and joiner"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install VidCutter on MacOS using homebrew

- App Name: VidCutter
- App description: Media cutter and joiner
- App Version: 6.0.5.1
- App Website: https://github.com/ozmartian/vidcutter

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install VidCutter with the following command
   ```
   brew install --cask vidcutter
   ```
4. VidCutter is ready to use now!