---
title: "Install Google Featured Photos on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Screensaver photos provider"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Google Featured Photos on MacOS using homebrew

- App Name: Google Featured Photos
- App description: Screensaver photos provider
- App Version: 1.0.0.208
- App Website: https://plus.google.com/featuredphotos

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Google Featured Photos with the following command
   ```
   brew install --cask google-featured-photos
   ```
4. Google Featured Photos is ready to use now!