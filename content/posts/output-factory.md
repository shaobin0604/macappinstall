---
title: "Install Output Factory on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Automate printing and exporting from Adobe InDesign"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Output Factory on MacOS using homebrew

- App Name: Output Factory
- App description: Automate printing and exporting from Adobe InDesign
- App Version: 2.4.76
- App Website: https://zevrix.com/OutputFactory/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Output Factory with the following command
   ```
   brew install --cask output-factory
   ```
4. Output Factory is ready to use now!