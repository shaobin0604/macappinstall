---
title: "Install zbar on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Suite of barcodes-reading tools"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install zbar on MacOS using homebrew

- App Name: zbar
- App description: Suite of barcodes-reading tools
- App Version: 0.23.90
- App Website: https://github.com/mchehab/zbar

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install zbar with the following command
   ```
   brew install zbar
   ```
4. zbar is ready to use now!