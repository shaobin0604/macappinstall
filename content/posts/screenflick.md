---
title: "Install Screenflick on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Screen recorder with audio"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Screenflick on MacOS using homebrew

- App Name: Screenflick
- App description: Screen recorder with audio
- App Version: 3.0.9
- App Website: https://www.araelium.com/screenflick-mac-screen-recorder

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Screenflick with the following command
   ```
   brew install --cask screenflick
   ```
4. Screenflick is ready to use now!