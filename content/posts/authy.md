---
title: "Install Authy Desktop on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Two-factor authentication software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Authy Desktop on MacOS using homebrew

- App Name: Authy Desktop
- App description: Two-factor authentication software
- App Version: 1.9.0
- App Website: https://authy.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Authy Desktop with the following command
   ```
   brew install --cask authy
   ```
4. Authy Desktop is ready to use now!