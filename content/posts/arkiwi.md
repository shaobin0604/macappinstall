---
title: "Install ArKiwi on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "File archiver"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ArKiwi on MacOS using homebrew

- App Name: ArKiwi
- App description: File archiver
- App Version: 4.1.5,4015
- App Website: https://www.mariogt.com/arkiwi.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ArKiwi with the following command
   ```
   brew install --cask arkiwi
   ```
4. ArKiwi is ready to use now!