---
title: "Install qpid-proton on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "High-performance, lightweight AMQP 1.0 messaging library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install qpid-proton on MacOS using homebrew

- App Name: qpid-proton
- App description: High-performance, lightweight AMQP 1.0 messaging library
- App Version: 0.36.0
- App Website: https://qpid.apache.org/proton/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install qpid-proton with the following command
   ```
   brew install qpid-proton
   ```
4. qpid-proton is ready to use now!