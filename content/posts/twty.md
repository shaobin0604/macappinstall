---
title: "Install twty on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line twitter client written in golang"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install twty on MacOS using homebrew

- App Name: twty
- App description: Command-line twitter client written in golang
- App Version: 0.0.11
- App Website: https://mattn.kaoriya.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install twty with the following command
   ```
   brew install twty
   ```
4. twty is ready to use now!