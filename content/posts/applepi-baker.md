---
title: "Install ApplePi-Baker on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Backup and restore SD cards, USB drives, external HDD, etc"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ApplePi-Baker on MacOS using homebrew

- App Name: ApplePi-Baker
- App description: Backup and restore SD cards, USB drives, external HDD, etc
- App Version: 2.2.3
- App Website: https://www.tweaking4all.com/hardware/raspberry-pi/applepi-baker-v2/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ApplePi-Baker with the following command
   ```
   brew install --cask applepi-baker
   ```
4. ApplePi-Baker is ready to use now!