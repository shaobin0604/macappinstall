---
title: "Install termcolor on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Header-only C++ library for printing colored messages"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install termcolor on MacOS using homebrew

- App Name: termcolor
- App description: Header-only C++ library for printing colored messages
- App Version: 2.0.0
- App Website: https://termcolor.readthedocs.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install termcolor with the following command
   ```
   brew install termcolor
   ```
4. termcolor is ready to use now!