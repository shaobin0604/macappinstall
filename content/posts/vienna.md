---
title: "Install Vienna on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "RSS and Atom reader"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Vienna on MacOS using homebrew

- App Name: Vienna
- App description: RSS and Atom reader
- App Version: 3.7.2,7555
- App Website: https://www.vienna-rss.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Vienna with the following command
   ```
   brew install --cask vienna
   ```
4. Vienna is ready to use now!