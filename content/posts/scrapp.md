---
title: "Install Scrapp on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Screenshot tool with cloud storage"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Scrapp on MacOS using homebrew

- App Name: Scrapp
- App description: Screenshot tool with cloud storage
- App Version: 1.6
- App Website: https://scrapp.me/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Scrapp with the following command
   ```
   brew install --cask scrapp
   ```
4. Scrapp is ready to use now!