---
title: "Install APK Icon Editor on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Editor for changing APK icons, name, version and other data"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install APK Icon Editor on MacOS using homebrew

- App Name: APK Icon Editor
- App description: Editor for changing APK icons, name, version and other data
- App Version: 2.2.0
- App Website: https://kefir500.github.io/apk-icon-editor/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install APK Icon Editor with the following command
   ```
   brew install --cask apk-icon-editor
   ```
4. APK Icon Editor is ready to use now!