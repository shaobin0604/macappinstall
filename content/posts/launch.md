---
title: "Install launch on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line launcher for macOS, in the spirit of `open`"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install launch on MacOS using homebrew

- App Name: launch
- App description: Command-line launcher for macOS, in the spirit of `open`
- App Version: 1.2.5
- App Website: https://sabi.net/nriley/software/#launch

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install launch with the following command
   ```
   brew install launch
   ```
4. launch is ready to use now!