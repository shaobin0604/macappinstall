---
title: "Install libxmi on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C/C++ function library for rasterizing 2D vector graphics"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libxmi on MacOS using homebrew

- App Name: libxmi
- App description: C/C++ function library for rasterizing 2D vector graphics
- App Version: 1.2
- App Website: https://www.gnu.org/software/libxmi/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libxmi with the following command
   ```
   brew install libxmi
   ```
4. libxmi is ready to use now!