---
title: "Install frps on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Server app of fast reverse proxy to expose a local server to the internet"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install frps on MacOS using homebrew

- App Name: frps
- App description: Server app of fast reverse proxy to expose a local server to the internet
- App Version: 0.39.1
- App Website: https://github.com/fatedier/frp

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install frps with the following command
   ```
   brew install frps
   ```
4. frps is ready to use now!