---
title: "Install cmix on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Data compression program with high compression ratio"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cmix on MacOS using homebrew

- App Name: cmix
- App description: Data compression program with high compression ratio
- App Version: 18.0.0
- App Website: https://www.byronknoll.com/cmix.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cmix with the following command
   ```
   brew install cmix
   ```
4. cmix is ready to use now!