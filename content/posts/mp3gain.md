---
title: "Install mp3gain on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Lossless mp3 normalizer with statistical analysis"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mp3gain on MacOS using homebrew

- App Name: mp3gain
- App description: Lossless mp3 normalizer with statistical analysis
- App Version: 1.6.2
- App Website: https://mp3gain.sourceforge.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mp3gain with the following command
   ```
   brew install mp3gain
   ```
4. mp3gain is ready to use now!