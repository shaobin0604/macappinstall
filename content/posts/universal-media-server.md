---
title: "Install Universal Media Server on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Media server supporting DLNA, UPnP and HTTP(S)"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Universal Media Server on MacOS using homebrew

- App Name: Universal Media Server
- App description: Media server supporting DLNA, UPnP and HTTP(S)
- App Version: 10.16.0
- App Website: https://www.universalmediaserver.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Universal Media Server with the following command
   ```
   brew install --cask universal-media-server
   ```
4. Universal Media Server is ready to use now!