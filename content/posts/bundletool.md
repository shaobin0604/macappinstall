---
title: "Install bundletool on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line tool to manipulate Android App Bundles"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bundletool on MacOS using homebrew

- App Name: bundletool
- App description: Command-line tool to manipulate Android App Bundles
- App Version: 1.8.2
- App Website: https://github.com/google/bundletool

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bundletool with the following command
   ```
   brew install bundletool
   ```
4. bundletool is ready to use now!