---
title: "Install wakatime-cli on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line interface to the WakaTime api"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install wakatime-cli on MacOS using homebrew

- App Name: wakatime-cli
- App description: Command-line interface to the WakaTime api
- App Version: 1.37.0
- App Website: https://wakatime.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install wakatime-cli with the following command
   ```
   brew install wakatime-cli
   ```
4. wakatime-cli is ready to use now!