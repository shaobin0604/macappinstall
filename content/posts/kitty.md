---
title: "Install kitty on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GPU-based terminal emulator"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install kitty on MacOS using homebrew

- App Name: kitty
- App description: GPU-based terminal emulator
- App Version: 0.24.2
- App Website: https://github.com/kovidgoyal/kitty

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install kitty with the following command
   ```
   brew install --cask kitty
   ```
4. kitty is ready to use now!