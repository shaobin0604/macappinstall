---
title: "Install mongo-c-driver on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C driver for MongoDB"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mongo-c-driver on MacOS using homebrew

- App Name: mongo-c-driver
- App description: C driver for MongoDB
- App Version: 1.21.0
- App Website: https://github.com/mongodb/mongo-c-driver

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mongo-c-driver with the following command
   ```
   brew install mongo-c-driver
   ```
4. mongo-c-driver is ready to use now!