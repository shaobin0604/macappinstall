---
title: "Install TeX Live Utility on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Graphical user interface for TeX Live Manager"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install TeX Live Utility on MacOS using homebrew

- App Name: TeX Live Utility
- App description: Graphical user interface for TeX Live Manager
- App Version: 1.53
- App Website: https://github.com/amaxwell/tlutility

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install TeX Live Utility with the following command
   ```
   brew install --cask tex-live-utility
   ```
4. TeX Live Utility is ready to use now!