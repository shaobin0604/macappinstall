---
title: "Install Guppy on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Friendly application manager and task runner for React.js"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Guppy on MacOS using homebrew

- App Name: Guppy
- App description: Friendly application manager and task runner for React.js
- App Version: 0.3.0
- App Website: https://github.com/joshwcomeau/guppy

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Guppy with the following command
   ```
   brew install --cask guppy
   ```
4. Guppy is ready to use now!