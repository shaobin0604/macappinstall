---
title: "Install SpaceRadar on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Disk And Memory Space Visualization App built with Electron & d3.js"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install SpaceRadar on MacOS using homebrew

- App Name: SpaceRadar
- App description: Disk And Memory Space Visualization App built with Electron & d3.js
- App Version: 5.1.0
- App Website: https://github.com/zz85/space-radar

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install SpaceRadar with the following command
   ```
   brew install --cask spaceradar
   ```
4. SpaceRadar is ready to use now!