---
title: "Install Nulloy on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Music player"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Nulloy on MacOS using homebrew

- App Name: Nulloy
- App description: Music player
- App Version: 0.8.2
- App Website: https://nulloy.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Nulloy with the following command
   ```
   brew install --cask nulloy
   ```
4. Nulloy is ready to use now!