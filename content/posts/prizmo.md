---
title: "Install Prizmo on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Scanning application with Optical Character Recognition (OCR)"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Prizmo on MacOS using homebrew

- App Name: Prizmo
- App description: Scanning application with Optical Character Recognition (OCR)
- App Version: 4.3.1,4.277.1375
- App Website: https://creaceed.com/prizmo

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Prizmo with the following command
   ```
   brew install --cask prizmo
   ```
4. Prizmo is ready to use now!