---
title: "Install cpuinfo on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CPU meter menu bar app"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cpuinfo on MacOS using homebrew

- App Name: cpuinfo
- App description: CPU meter menu bar app
- App Version: 1.4.6
- App Website: https://github.com/yusukeshibata/cpuinfo/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cpuinfo with the following command
   ```
   brew install --cask cpuinfo
   ```
4. cpuinfo is ready to use now!