---
title: "Install kertish-dfs on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Kertish FileSystem and Cluster Administration CLI"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install kertish-dfs on MacOS using homebrew

- App Name: kertish-dfs
- App description: Kertish FileSystem and Cluster Administration CLI
- App Version: 21.2.0066
- App Website: https://github.com/freakmaxi/kertish-dfs

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install kertish-dfs with the following command
   ```
   brew install kertish-dfs
   ```
4. kertish-dfs is ready to use now!