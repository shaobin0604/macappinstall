---
title: "Install nudoku on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Ncurses based sudoku game"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install nudoku on MacOS using homebrew

- App Name: nudoku
- App description: Ncurses based sudoku game
- App Version: 2.1.0
- App Website: https://jubalh.github.io/nudoku/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install nudoku with the following command
   ```
   brew install nudoku
   ```
4. nudoku is ready to use now!