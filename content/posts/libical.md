---
title: "Install libical on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Implementation of iCalendar protocols and data formats"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libical on MacOS using homebrew

- App Name: libical
- App description: Implementation of iCalendar protocols and data formats
- App Version: 3.0.14
- App Website: https://libical.github.io/libical/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libical with the following command
   ```
   brew install libical
   ```
4. libical is ready to use now!