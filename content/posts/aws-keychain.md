---
title: "Install aws-keychain on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Uses macOS keychain for storage of AWS credentials"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install aws-keychain on MacOS using homebrew

- App Name: aws-keychain
- App description: Uses macOS keychain for storage of AWS credentials
- App Version: 3.0.0
- App Website: https://github.com/pda/aws-keychain

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install aws-keychain with the following command
   ```
   brew install aws-keychain
   ```
4. aws-keychain is ready to use now!