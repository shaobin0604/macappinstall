---
title: "Install svtplay-dl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Download videos from https://www.svtplay.se/"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install svtplay-dl on MacOS using homebrew

- App Name: svtplay-dl
- App description: Download videos from https://www.svtplay.se/
- App Version: 4.10
- App Website: https://svtplay-dl.se/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install svtplay-dl with the following command
   ```
   brew install svtplay-dl
   ```
4. svtplay-dl is ready to use now!