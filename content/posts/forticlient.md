---
title: "Install FortiClient on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Fabric agent with endpoint protection and cloud sandbox"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install FortiClient on MacOS using homebrew

- App Name: FortiClient
- App description: Fabric agent with endpoint protection and cloud sandbox
- App Version: 7.0.0.22
- App Website: https://forticlient.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install FortiClient with the following command
   ```
   brew install --cask forticlient
   ```
4. FortiClient is ready to use now!