---
title: "Install cmt on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Write consistent git commit messages based on a custom template"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cmt on MacOS using homebrew

- App Name: cmt
- App description: Write consistent git commit messages based on a custom template
- App Version: 0.7.1
- App Website: https://github.com/smallhadroncollider/cmt

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cmt with the following command
   ```
   brew install cmt
   ```
4. cmt is ready to use now!