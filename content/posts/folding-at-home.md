---
title: "Install Folding@home on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Graphical interface control for Folding"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Folding@home on MacOS using homebrew

- App Name: Folding@home
- App description: Graphical interface control for Folding
- App Version: 7.6.21
- App Website: https://foldingathome.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Folding@home with the following command
   ```
   brew install --cask folding-at-home
   ```
4. Folding@home is ready to use now!