---
title: "Install Guitar Pro on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Sheet music editor software for guitar, bass, keyboards, drums and more"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Guitar Pro on MacOS using homebrew

- App Name: Guitar Pro
- App description: Sheet music editor software for guitar, bass, keyboards, drums and more
- App Version: 7.6
- App Website: https://www.guitar-pro.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Guitar Pro with the following command
   ```
   brew install --cask guitar-pro
   ```
4. Guitar Pro is ready to use now!