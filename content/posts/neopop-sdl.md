---
title: "Install neopop-sdl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "NeoGeo Pocket emulator"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install neopop-sdl on MacOS using homebrew

- App Name: neopop-sdl
- App description: NeoGeo Pocket emulator
- App Version: 0.2
- App Website: https://nih.at/NeoPop-SDL/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install neopop-sdl with the following command
   ```
   brew install neopop-sdl
   ```
4. neopop-sdl is ready to use now!