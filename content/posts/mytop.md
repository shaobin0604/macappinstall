---
title: "Install mytop on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Top-like query monitor for MySQL"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mytop on MacOS using homebrew

- App Name: mytop
- App description: Top-like query monitor for MySQL
- App Version: 1.9.1
- App Website: https://web.archive.org/web/20200221154243/www.mysqlfanboy.com/mytop-3/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mytop with the following command
   ```
   brew install mytop
   ```
4. mytop is ready to use now!