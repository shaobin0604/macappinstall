---
title: "Install pinfo on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "User-friendly, console-based viewer for Info documents"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pinfo on MacOS using homebrew

- App Name: pinfo
- App description: User-friendly, console-based viewer for Info documents
- App Version: 0.6.13
- App Website: https://packages.debian.org/sid/pinfo

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pinfo with the following command
   ```
   brew install pinfo
   ```
4. pinfo is ready to use now!