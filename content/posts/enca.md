---
title: "Install enca on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Charset analyzer and converter"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install enca on MacOS using homebrew

- App Name: enca
- App description: Charset analyzer and converter
- App Version: 1.19
- App Website: https://cihar.com/software/enca/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install enca with the following command
   ```
   brew install enca
   ```
4. enca is ready to use now!