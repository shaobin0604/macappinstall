---
title: "Install libmicrohttpd on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Light HTTP/1.1 server library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libmicrohttpd on MacOS using homebrew

- App Name: libmicrohttpd
- App description: Light HTTP/1.1 server library
- App Version: 0.9.75
- App Website: https://www.gnu.org/software/libmicrohttpd/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libmicrohttpd with the following command
   ```
   brew install libmicrohttpd
   ```
4. libmicrohttpd is ready to use now!