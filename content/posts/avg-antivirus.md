---
title: "Install AVG Antivirus for Mac on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Antivirus software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install AVG Antivirus for Mac on MacOS using homebrew

- App Name: AVG Antivirus for Mac
- App description: Antivirus software
- App Version: 4.0,2.0
- App Website: https://www.avg.com/us-en/avg-antivirus-for-mac

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install AVG Antivirus for Mac with the following command
   ```
   brew install --cask avg-antivirus
   ```
4. AVG Antivirus for Mac is ready to use now!