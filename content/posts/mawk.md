---
title: "Install mawk on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Interpreter for the AWK Programming Language"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mawk on MacOS using homebrew

- App Name: mawk
- App description: Interpreter for the AWK Programming Language
- App Version: 1.3.4-20200120
- App Website: https://invisible-island.net/mawk/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mawk with the following command
   ```
   brew install mawk
   ```
4. mawk is ready to use now!