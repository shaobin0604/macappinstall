---
title: "Install nspr on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Platform-neutral API for system-level and libc-like functions"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install nspr on MacOS using homebrew

- App Name: nspr
- App description: Platform-neutral API for system-level and libc-like functions
- App Version: 4.33
- App Website: https://hg.mozilla.org/projects/nspr

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install nspr with the following command
   ```
   brew install nspr
   ```
4. nspr is ready to use now!