---
title: "Install fluid-synth on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Real-time software synthesizer based on the SoundFont 2 specs"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fluid-synth on MacOS using homebrew

- App Name: fluid-synth
- App description: Real-time software synthesizer based on the SoundFont 2 specs
- App Version: 2.2.5
- App Website: https://www.fluidsynth.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fluid-synth with the following command
   ```
   brew install fluid-synth
   ```
4. fluid-synth is ready to use now!