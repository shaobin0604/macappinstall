---
title: "Install Coq on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Formal proof management system"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Coq on MacOS using homebrew

- App Name: Coq
- App description: Formal proof management system
- App Version: 8.13.1
- App Website: https://coq.inria.fr/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Coq with the following command
   ```
   brew install --cask coqide
   ```
4. Coq is ready to use now!