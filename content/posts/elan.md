---
title: "Install ELAN on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Annotation tool for audio and video recordings"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ELAN on MacOS using homebrew

- App Name: ELAN
- App description: Annotation tool for audio and video recordings
- App Version: 6.3
- App Website: https://archive.mpi.nl/tla/elan

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ELAN with the following command
   ```
   brew install --cask elan
   ```
4. ELAN is ready to use now!