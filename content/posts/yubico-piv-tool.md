---
title: "Install yubico-piv-tool on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line tool for the YubiKey PIV application"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install yubico-piv-tool on MacOS using homebrew

- App Name: yubico-piv-tool
- App description: Command-line tool for the YubiKey PIV application
- App Version: 2.2.1
- App Website: https://developers.yubico.com/yubico-piv-tool/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install yubico-piv-tool with the following command
   ```
   brew install yubico-piv-tool
   ```
4. yubico-piv-tool is ready to use now!