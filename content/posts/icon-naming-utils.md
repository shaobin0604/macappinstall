---
title: "Install icon-naming-utils on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Script to handle icon names in desktop icon themes"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install icon-naming-utils on MacOS using homebrew

- App Name: icon-naming-utils
- App description: Script to handle icon names in desktop icon themes
- App Version: 0.8.90
- App Website: https://specifications.freedesktop.org/icon-naming-spec/icon-naming-spec-latest.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install icon-naming-utils with the following command
   ```
   brew install icon-naming-utils
   ```
4. icon-naming-utils is ready to use now!