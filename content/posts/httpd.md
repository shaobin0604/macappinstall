---
title: "Install httpd on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Apache HTTP server"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install httpd on MacOS using homebrew

- App Name: httpd
- App description: Apache HTTP server
- App Version: 2.4.52
- App Website: https://httpd.apache.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install httpd with the following command
   ```
   brew install httpd
   ```
4. httpd is ready to use now!