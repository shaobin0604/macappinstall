---
title: "Install frei0r on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Minimalistic plugin API for video effects"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install frei0r on MacOS using homebrew

- App Name: frei0r
- App description: Minimalistic plugin API for video effects
- App Version: 1.7.0
- App Website: https://frei0r.dyne.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install frei0r with the following command
   ```
   brew install frei0r
   ```
4. frei0r is ready to use now!