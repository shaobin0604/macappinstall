---
title: "Install speex on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Audio codec designed for speech"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install speex on MacOS using homebrew

- App Name: speex
- App description: Audio codec designed for speech
- App Version: 1.2.0
- App Website: https://speex.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install speex with the following command
   ```
   brew install speex
   ```
4. speex is ready to use now!