---
title: "Install libsoxr on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "High quality, one-dimensional sample-rate conversion library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libsoxr on MacOS using homebrew

- App Name: libsoxr
- App description: High quality, one-dimensional sample-rate conversion library
- App Version: 0.1.3
- App Website: https://sourceforge.net/projects/soxr/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libsoxr with the following command
   ```
   brew install libsoxr
   ```
4. libsoxr is ready to use now!