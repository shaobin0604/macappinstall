---
title: "Install JASP on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Statistical analysis application"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install JASP on MacOS using homebrew

- App Name: JASP
- App description: Statistical analysis application
- App Version: 0.16.1.0
- App Website: https://jasp-stats.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install JASP with the following command
   ```
   brew install --cask jasp
   ```
4. JASP is ready to use now!