---
title: "Install VirtualHostX on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Local server environment"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install VirtualHostX on MacOS using homebrew

- App Name: VirtualHostX
- App description: Local server environment
- App Version: 2021.01.10,1017
- App Website: https://clickontyler.com/virtualhostx/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install VirtualHostX with the following command
   ```
   brew install --cask virtualhostx
   ```
4. VirtualHostX is ready to use now!