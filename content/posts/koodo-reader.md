---
title: "Install Koodo Reader on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Open-source Epub reader with backup and restore support"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Koodo Reader on MacOS using homebrew

- App Name: Koodo Reader
- App description: Open-source Epub reader with backup and restore support
- App Version: 1.4.1
- App Website: https://koodo.960960.xyz/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Koodo Reader with the following command
   ```
   brew install --cask koodo-reader
   ```
4. Koodo Reader is ready to use now!