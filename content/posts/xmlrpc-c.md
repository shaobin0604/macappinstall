---
title: "Install xmlrpc-c on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Lightweight RPC library (based on XML and HTTP)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install xmlrpc-c on MacOS using homebrew

- App Name: xmlrpc-c
- App description: Lightweight RPC library (based on XML and HTTP)
- App Version: 1.51.08
- App Website: https://xmlrpc-c.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install xmlrpc-c with the following command
   ```
   brew install xmlrpc-c
   ```
4. xmlrpc-c is ready to use now!