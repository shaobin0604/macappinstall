---
title: "Install jpeg on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Image manipulation library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install jpeg on MacOS using homebrew

- App Name: jpeg
- App description: Image manipulation library
- App Version: 9e
- App Website: https://www.ijg.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install jpeg with the following command
   ```
   brew install jpeg
   ```
4. jpeg is ready to use now!