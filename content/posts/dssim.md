---
title: "Install dssim on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "RGBA Structural Similarity Rust implementation"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dssim on MacOS using homebrew

- App Name: dssim
- App description: RGBA Structural Similarity Rust implementation
- App Version: 3.2.0
- App Website: https://github.com/kornelski/dssim

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dssim with the following command
   ```
   brew install dssim
   ```
4. dssim is ready to use now!