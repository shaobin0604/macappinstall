---
title: "Install DMM Player on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Video player for the DMM.com platform"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install DMM Player on MacOS using homebrew

- App Name: DMM Player
- App description: Video player for the DMM.com platform
- App Version: 2.1.9
- App Website: https://www.dmm.com/digital/howto_dmmplayer_html/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install DMM Player with the following command
   ```
   brew install --cask dmm-player
   ```
4. DMM Player is ready to use now!