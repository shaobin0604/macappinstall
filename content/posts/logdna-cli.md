---
title: "Install LogDNA CLI on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install LogDNA CLI on MacOS using homebrew

- App Name: LogDNA CLI
- App description: null
- App Version: 2.0.0
- App Website: https://logdna.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install LogDNA CLI with the following command
   ```
   brew install --cask logdna-cli
   ```
4. LogDNA CLI is ready to use now!