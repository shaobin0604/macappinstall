---
title: "Install apache-forrest on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Publishing framework providing multiple output formats"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install apache-forrest on MacOS using homebrew

- App Name: apache-forrest
- App description: Publishing framework providing multiple output formats
- App Version: 0.9
- App Website: https://forrest.apache.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install apache-forrest with the following command
   ```
   brew install apache-forrest
   ```
4. apache-forrest is ready to use now!