---
title: "Install AnyBar on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Menu bar status indicator"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install AnyBar on MacOS using homebrew

- App Name: AnyBar
- App description: Menu bar status indicator
- App Version: 0.2.3
- App Website: https://github.com/tonsky/AnyBar

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install AnyBar with the following command
   ```
   brew install --cask anybar
   ```
4. AnyBar is ready to use now!