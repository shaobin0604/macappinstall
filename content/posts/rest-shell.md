---
title: "Install rest-shell on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Shell to work with Spring HATEOAS-compliant REST resources"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install rest-shell on MacOS using homebrew

- App Name: rest-shell
- App description: Shell to work with Spring HATEOAS-compliant REST resources
- App Version: 1.2.1.RELEASE
- App Website: https://github.com/spring-projects/rest-shell

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install rest-shell with the following command
   ```
   brew install rest-shell
   ```
4. rest-shell is ready to use now!