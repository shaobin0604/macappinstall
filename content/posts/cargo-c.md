---
title: "Install cargo-c on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Helper program to build and install c-like libraries"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cargo-c on MacOS using homebrew

- App Name: cargo-c
- App description: Helper program to build and install c-like libraries
- App Version: 0.9.7
- App Website: https://github.com/lu-zero/cargo-c

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cargo-c with the following command
   ```
   brew install cargo-c
   ```
4. cargo-c is ready to use now!