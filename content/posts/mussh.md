---
title: "Install mussh on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Multi-host SSH wrapper"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mussh on MacOS using homebrew

- App Name: mussh
- App description: Multi-host SSH wrapper
- App Version: 1.0
- App Website: https://mussh.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mussh with the following command
   ```
   brew install mussh
   ```
4. mussh is ready to use now!