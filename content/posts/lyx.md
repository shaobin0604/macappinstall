---
title: "Install LyX on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Open source, GUI document processor based on the LaTeX typesetting system"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install LyX on MacOS using homebrew

- App Name: LyX
- App description: Open source, GUI document processor based on the LaTeX typesetting system
- App Version: 2.3.6.2
- App Website: https://www.lyx.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install LyX with the following command
   ```
   brew install --cask lyx
   ```
4. LyX is ready to use now!