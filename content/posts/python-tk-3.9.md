---
title: "Install python-tk@3.9 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Python interface to Tcl/Tk"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install python-tk@3.9 on MacOS using homebrew

- App Name: python-tk@3.9
- App description: Python interface to Tcl/Tk
- App Version: 3.9.10
- App Website: https://www.python.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install python-tk@3.9 with the following command
   ```
   brew install python-tk@3.9
   ```
4. python-tk@3.9 is ready to use now!