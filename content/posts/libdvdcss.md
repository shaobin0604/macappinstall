---
title: "Install libdvdcss on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Access DVDs as block devices without the decryption"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libdvdcss on MacOS using homebrew

- App Name: libdvdcss
- App description: Access DVDs as block devices without the decryption
- App Version: 1.4.3
- App Website: https://www.videolan.org/developers/libdvdcss.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libdvdcss with the following command
   ```
   brew install libdvdcss
   ```
4. libdvdcss is ready to use now!