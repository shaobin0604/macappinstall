---
title: "Install clblas on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library containing BLAS functions written in OpenCL"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install clblas on MacOS using homebrew

- App Name: clblas
- App description: Library containing BLAS functions written in OpenCL
- App Version: 2.12
- App Website: https://github.com/clMathLibraries/clBLAS

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install clblas with the following command
   ```
   brew install clblas
   ```
4. clblas is ready to use now!