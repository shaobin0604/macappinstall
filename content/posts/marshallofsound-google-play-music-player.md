---
title: "Install Google Play Music Desktop Player on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Google Play Music Desktop Player on MacOS using homebrew

- App Name: Google Play Music Desktop Player
- App description: null
- App Version: 4.7.1
- App Website: https://www.googleplaymusicdesktopplayer.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Google Play Music Desktop Player with the following command
   ```
   brew install --cask marshallofsound-google-play-music-player
   ```
4. Google Play Music Desktop Player is ready to use now!