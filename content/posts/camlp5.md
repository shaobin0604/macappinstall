---
title: "Install camlp5 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Preprocessor and pretty-printer for OCaml"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install camlp5 on MacOS using homebrew

- App Name: camlp5
- App description: Preprocessor and pretty-printer for OCaml
- App Version: 8.00
- App Website: https://camlp5.github.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install camlp5 with the following command
   ```
   brew install camlp5
   ```
4. camlp5 is ready to use now!