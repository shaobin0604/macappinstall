---
title: "Install itpp on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library of math, signal, and communication classes and functions"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install itpp on MacOS using homebrew

- App Name: itpp
- App description: Library of math, signal, and communication classes and functions
- App Version: 4.3.1
- App Website: https://itpp.sourceforge.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install itpp with the following command
   ```
   brew install itpp
   ```
4. itpp is ready to use now!