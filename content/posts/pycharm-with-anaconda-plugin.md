---
title: "Install Jetbrains PyCharm with Anaconda plugin on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "PyCharm IDE with Anaconda plugin"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Jetbrains PyCharm with Anaconda plugin on MacOS using homebrew

- App Name: Jetbrains PyCharm with Anaconda plugin
- App description: PyCharm IDE with Anaconda plugin
- App Version: 2020.3.2,203.6682.179
- App Website: https://www.jetbrains.com/pycharm/promo/anaconda

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Jetbrains PyCharm with Anaconda plugin with the following command
   ```
   brew install --cask pycharm-with-anaconda-plugin
   ```
4. Jetbrains PyCharm with Anaconda plugin is ready to use now!