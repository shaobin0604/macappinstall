---
title: "Install ascii2binary on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Converting Text to Binary and Back"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ascii2binary on MacOS using homebrew

- App Name: ascii2binary
- App description: Converting Text to Binary and Back
- App Version: 2.14
- App Website: https://billposer.org/Software/a2b.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ascii2binary with the following command
   ```
   brew install ascii2binary
   ```
4. ascii2binary is ready to use now!