---
title: "Install rdup on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utility to create a file list suitable for making backups"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install rdup on MacOS using homebrew

- App Name: rdup
- App description: Utility to create a file list suitable for making backups
- App Version: 1.1.15
- App Website: https://github.com/miekg/rdup

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install rdup with the following command
   ```
   brew install rdup
   ```
4. rdup is ready to use now!