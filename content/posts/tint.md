---
title: "Install Tint on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tailwind CSS colour picker"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Tint on MacOS using homebrew

- App Name: Tint
- App description: Tailwind CSS colour picker
- App Version: 1.0.0
- App Website: https://beyondco.de/software/tint

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Tint with the following command
   ```
   brew install --cask tint
   ```
4. Tint is ready to use now!