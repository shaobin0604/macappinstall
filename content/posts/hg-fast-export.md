---
title: "Install hg-fast-export on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Fast Mercurial to Git converter"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install hg-fast-export on MacOS using homebrew

- App Name: hg-fast-export
- App description: Fast Mercurial to Git converter
- App Version: 210917
- App Website: https://repo.or.cz/fast-export.git

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install hg-fast-export with the following command
   ```
   brew install hg-fast-export
   ```
4. hg-fast-export is ready to use now!