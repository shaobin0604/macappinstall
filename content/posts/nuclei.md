---
title: "Install nuclei on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "HTTP/DNS scanner configurable via YAML templates"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install nuclei on MacOS using homebrew

- App Name: nuclei
- App description: HTTP/DNS scanner configurable via YAML templates
- App Version: 2.6.0
- App Website: https://nuclei.projectdiscovery.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install nuclei with the following command
   ```
   brew install nuclei
   ```
4. nuclei is ready to use now!