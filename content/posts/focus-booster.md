---
title: "Install Focus Booster on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Time tracker"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Focus Booster on MacOS using homebrew

- App Name: Focus Booster
- App description: Time tracker
- App Version: 2.2.0
- App Website: https://www.focusboosterapp.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Focus Booster with the following command
   ```
   brew install --cask focus-booster
   ```
4. Focus Booster is ready to use now!