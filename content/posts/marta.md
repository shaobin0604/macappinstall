---
title: "Install Marta File Manager on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Extensible two-pane file manager"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Marta File Manager on MacOS using homebrew

- App Name: Marta File Manager
- App description: Extensible two-pane file manager
- App Version: 0.8.1
- App Website: https://marta.sh/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Marta File Manager with the following command
   ```
   brew install --cask marta
   ```
4. Marta File Manager is ready to use now!