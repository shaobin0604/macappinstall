---
title: "Install cdogs-sdl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Classic overhead run-and-gun game"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cdogs-sdl on MacOS using homebrew

- App Name: cdogs-sdl
- App description: Classic overhead run-and-gun game
- App Version: 1.2.0
- App Website: https://cxong.github.io/cdogs-sdl/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cdogs-sdl with the following command
   ```
   brew install cdogs-sdl
   ```
4. cdogs-sdl is ready to use now!