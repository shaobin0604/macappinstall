---
title: "Install yara on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Malware identification and classification tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install yara on MacOS using homebrew

- App Name: yara
- App description: Malware identification and classification tool
- App Version: 4.1.3
- App Website: https://github.com/VirusTotal/yara/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install yara with the following command
   ```
   brew install yara
   ```
4. yara is ready to use now!