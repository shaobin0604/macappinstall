---
title: "Install aqbanking on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Generic online banking interface"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install aqbanking on MacOS using homebrew

- App Name: aqbanking
- App description: Generic online banking interface
- App Version: 6.4.1
- App Website: https://www.aquamaniac.de/sites/aqbanking/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install aqbanking with the following command
   ```
   brew install aqbanking
   ```
4. aqbanking is ready to use now!