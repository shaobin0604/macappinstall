---
title: "Install Wasabi Wallet on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Open-source, non-custodial, privacy focused Bitcoin wallet"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Wasabi Wallet on MacOS using homebrew

- App Name: Wasabi Wallet
- App description: Open-source, non-custodial, privacy focused Bitcoin wallet
- App Version: 1.1.13.0
- App Website: https://www.wasabiwallet.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Wasabi Wallet with the following command
   ```
   brew install --cask wasabi-wallet
   ```
4. Wasabi Wallet is ready to use now!