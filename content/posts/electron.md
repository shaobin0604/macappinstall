---
title: "Install Electron on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Build desktop apps with JavaScript, HTML, and CSS"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Electron on MacOS using homebrew

- App Name: Electron
- App description: Build desktop apps with JavaScript, HTML, and CSS
- App Version: 17.0.1
- App Website: https://electronjs.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Electron with the following command
   ```
   brew install --cask electron
   ```
4. Electron is ready to use now!