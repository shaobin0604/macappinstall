---
title: "Install fasttext on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library for fast text representation and classification"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fasttext on MacOS using homebrew

- App Name: fasttext
- App description: Library for fast text representation and classification
- App Version: 0.9.2
- App Website: https://fasttext.cc

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fasttext with the following command
   ```
   brew install fasttext
   ```
4. fasttext is ready to use now!