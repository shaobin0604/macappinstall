---
title: "Install SF Symbols on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool that provides consistent, highly configurable symbols for apps"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install SF Symbols on MacOS using homebrew

- App Name: SF Symbols
- App description: Tool that provides consistent, highly configurable symbols for apps
- App Version: 3.2
- App Website: https://developer.apple.com/design/human-interface-guidelines/sf-symbols/overview/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install SF Symbols with the following command
   ```
   brew install --cask sf-symbols
   ```
4. SF Symbols is ready to use now!