---
title: "Install Key Codes on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Display key code, unicode value and modifier keys state for any key combination"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Key Codes on MacOS using homebrew

- App Name: Key Codes
- App description: Display key code, unicode value and modifier keys state for any key combination
- App Version: 2.2.1,2027
- App Website: https://manytricks.com/keycodes/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Key Codes with the following command
   ```
   brew install --cask key-codes
   ```
4. Key Codes is ready to use now!