---
title: "Install Sketch on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Digital design and prototyping platform"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Sketch on MacOS using homebrew

- App Name: Sketch
- App description: Digital design and prototyping platform
- App Version: 83.2,138642
- App Website: https://www.sketch.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Sketch with the following command
   ```
   brew install --cask sketch
   ```
4. Sketch is ready to use now!