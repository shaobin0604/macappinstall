---
title: "Install jhipster on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Generate, develop and deploy Spring Boot + Angular/React applications"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install jhipster on MacOS using homebrew

- App Name: jhipster
- App description: Generate, develop and deploy Spring Boot + Angular/React applications
- App Version: 7.6.0
- App Website: https://www.jhipster.tech/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install jhipster with the following command
   ```
   brew install jhipster
   ```
4. jhipster is ready to use now!