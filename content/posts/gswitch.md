---
title: "Install gSwitch on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Set which graphics card to use"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gSwitch on MacOS using homebrew

- App Name: gSwitch
- App description: Set which graphics card to use
- App Version: 1.9.7
- App Website: https://codyschrank.github.io/gSwitch/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gSwitch with the following command
   ```
   brew install --cask gswitch
   ```
4. gSwitch is ready to use now!