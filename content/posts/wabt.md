---
title: "Install wabt on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Web Assembly Binary Toolkit"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install wabt on MacOS using homebrew

- App Name: wabt
- App description: Web Assembly Binary Toolkit
- App Version: 1.0.27
- App Website: https://github.com/WebAssembly/wabt

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install wabt with the following command
   ```
   brew install wabt
   ```
4. wabt is ready to use now!