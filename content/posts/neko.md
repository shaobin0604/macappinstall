---
title: "Install neko on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "High-level, dynamically typed programming language"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install neko on MacOS using homebrew

- App Name: neko
- App description: High-level, dynamically typed programming language
- App Version: 2.3.0
- App Website: https://nekovm.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install neko with the following command
   ```
   brew install neko
   ```
4. neko is ready to use now!