---
title: "Install ssh-copy-id on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Add a public key to a remote machine's authorized_keys file"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ssh-copy-id on MacOS using homebrew

- App Name: ssh-copy-id
- App description: Add a public key to a remote machine's authorized_keys file
- App Version: 8.8p1
- App Website: https://www.openssh.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ssh-copy-id with the following command
   ```
   brew install ssh-copy-id
   ```
4. ssh-copy-id is ready to use now!