---
title: "Install bison@2.7 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Parser generator"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bison@2.7 on MacOS using homebrew

- App Name: bison@2.7
- App description: Parser generator
- App Version: 2.7.1
- App Website: https://www.gnu.org/software/bison/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bison@2.7 with the following command
   ```
   brew install bison@2.7
   ```
4. bison@2.7 is ready to use now!