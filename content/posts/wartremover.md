---
title: "Install wartremover on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Flexible Scala code linting tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install wartremover on MacOS using homebrew

- App Name: wartremover
- App description: Flexible Scala code linting tool
- App Version: 2.4.16
- App Website: https://github.com/wartremover/wartremover

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install wartremover with the following command
   ```
   brew install wartremover
   ```
4. wartremover is ready to use now!