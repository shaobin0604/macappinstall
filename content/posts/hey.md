---
title: "Install hey on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "HTTP load generator, ApacheBench (ab) replacement"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install hey on MacOS using homebrew

- App Name: hey
- App description: HTTP load generator, ApacheBench (ab) replacement
- App Version: 0.1.4
- App Website: https://github.com/rakyll/hey

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install hey with the following command
   ```
   brew install hey
   ```
4. hey is ready to use now!