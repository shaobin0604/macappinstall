---
title: "Install Fantastical on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Calendar software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Fantastical on MacOS using homebrew

- App Name: Fantastical
- App description: Calendar software
- App Version: 3.6,1392
- App Website: https://flexibits.com/fantastical

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Fantastical with the following command
   ```
   brew install --cask fantastical
   ```
4. Fantastical is ready to use now!