---
title: "Install ipython on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Interactive computing in Python"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ipython on MacOS using homebrew

- App Name: ipython
- App description: Interactive computing in Python
- App Version: 8.0.1
- App Website: https://ipython.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ipython with the following command
   ```
   brew install ipython
   ```
4. ipython is ready to use now!