---
title: "Install volt on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Meta-level vim package manager"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install volt on MacOS using homebrew

- App Name: volt
- App description: Meta-level vim package manager
- App Version: 0.3.7
- App Website: https://github.com/vim-volt/volt

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install volt with the following command
   ```
   brew install volt
   ```
4. volt is ready to use now!