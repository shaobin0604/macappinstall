---
title: "Install micro on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Modern and intuitive terminal-based text editor"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install micro on MacOS using homebrew

- App Name: micro
- App description: Modern and intuitive terminal-based text editor
- App Version: 2.0.10
- App Website: https://github.com/zyedidia/micro

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install micro with the following command
   ```
   brew install micro
   ```
4. micro is ready to use now!