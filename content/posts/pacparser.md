---
title: "Install pacparser on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library to parse proxy auto-config (PAC) files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pacparser on MacOS using homebrew

- App Name: pacparser
- App description: Library to parse proxy auto-config (PAC) files
- App Version: 1.3.7
- App Website: https://github.com/pacparser/pacparser

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pacparser with the following command
   ```
   brew install pacparser
   ```
4. pacparser is ready to use now!