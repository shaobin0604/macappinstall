---
title: "Install minizip-ng on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Zip file manipulation library with minizip 1.x compatibility layer"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install minizip-ng on MacOS using homebrew

- App Name: minizip-ng
- App description: Zip file manipulation library with minizip 1.x compatibility layer
- App Version: 3.0.4
- App Website: https://github.com/zlib-ng/minizip-ng

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install minizip-ng with the following command
   ```
   brew install minizip-ng
   ```
4. minizip-ng is ready to use now!