---
title: "Install chart-testing on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Testing and linting Helm charts"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install chart-testing on MacOS using homebrew

- App Name: chart-testing
- App description: Testing and linting Helm charts
- App Version: 3.5.0
- App Website: https://github.com/helm/chart-testing

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install chart-testing with the following command
   ```
   brew install chart-testing
   ```
4. chart-testing is ready to use now!