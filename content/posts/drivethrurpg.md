---
title: "Install DriveThruRPG Library App on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Sync DriveThruRPG libraries to compatible devices"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install DriveThruRPG Library App on MacOS using homebrew

- App Name: DriveThruRPG Library App
- App description: Sync DriveThruRPG libraries to compatible devices
- App Version: 3.1.6.0
- App Website: https://www.drivethrurpg.com/library_client.php

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install DriveThruRPG Library App with the following command
   ```
   brew install --cask drivethrurpg
   ```
4. DriveThruRPG Library App is ready to use now!