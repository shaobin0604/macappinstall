---
title: "Install httping on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Ping-like tool for HTTP requests"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install httping on MacOS using homebrew

- App Name: httping
- App description: Ping-like tool for HTTP requests
- App Version: 2.5
- App Website: https://www.vanheusden.com/httping/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install httping with the following command
   ```
   brew install httping
   ```
4. httping is ready to use now!