---
title: "Install lib3ds on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library for managing 3D-Studio Release 3 and 4 '.3DS' files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install lib3ds on MacOS using homebrew

- App Name: lib3ds
- App description: Library for managing 3D-Studio Release 3 and 4 '.3DS' files
- App Version: 1.3.0
- App Website: https://code.google.com/archive/p/lib3ds/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install lib3ds with the following command
   ```
   brew install lib3ds
   ```
4. lib3ds is ready to use now!