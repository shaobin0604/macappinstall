---
title: "Install never on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Statically typed, embedded functional programming language"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install never on MacOS using homebrew

- App Name: never
- App description: Statically typed, embedded functional programming language
- App Version: 2.1.8
- App Website: https://never-lang.readthedocs.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install never with the following command
   ```
   brew install never
   ```
4. never is ready to use now!