---
title: "Install spotify-tui on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Terminal-based client for Spotify"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install spotify-tui on MacOS using homebrew

- App Name: spotify-tui
- App description: Terminal-based client for Spotify
- App Version: 0.25.0
- App Website: https://github.com/Rigellute/spotify-tui

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install spotify-tui with the following command
   ```
   brew install spotify-tui
   ```
4. spotify-tui is ready to use now!