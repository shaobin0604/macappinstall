---
title: "Install exiftran on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Transform digital camera jpegs and their EXIF data"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install exiftran on MacOS using homebrew

- App Name: exiftran
- App description: Transform digital camera jpegs and their EXIF data
- App Version: 2.14
- App Website: https://www.kraxel.org/blog/linux/fbida/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install exiftran with the following command
   ```
   brew install exiftran
   ```
4. exiftran is ready to use now!