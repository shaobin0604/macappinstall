---
title: "Install Cookie on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Protection from tracking and online profiling"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Cookie on MacOS using homebrew

- App Name: Cookie
- App description: Protection from tracking and online profiling
- App Version: 6.6.5
- App Website: https://sweetpproductions.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Cookie with the following command
   ```
   brew install --cask cookie
   ```
4. Cookie is ready to use now!