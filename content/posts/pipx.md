---
title: "Install pipx on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Execute binaries from Python packages in isolated environments"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pipx on MacOS using homebrew

- App Name: pipx
- App description: Execute binaries from Python packages in isolated environments
- App Version: 1.0.0
- App Website: https://pypa.github.io/pipx

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pipx with the following command
   ```
   brew install pipx
   ```
4. pipx is ready to use now!