---
title: "Install joe on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Full featured terminal-based screen editor"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install joe on MacOS using homebrew

- App Name: joe
- App description: Full featured terminal-based screen editor
- App Version: 4.6
- App Website: https://joe-editor.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install joe with the following command
   ```
   brew install joe
   ```
4. joe is ready to use now!