---
title: "Install Webpack Dashboard on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Electron Desktop GUI for Webpack Dashboard"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Webpack Dashboard on MacOS using homebrew

- App Name: Webpack Dashboard
- App description: Electron Desktop GUI for Webpack Dashboard
- App Version: 1.0.0
- App Website: https://github.com/FormidableLabs/electron-webpack-dashboard

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Webpack Dashboard with the following command
   ```
   brew install --cask webpack-dashboard
   ```
4. Webpack Dashboard is ready to use now!