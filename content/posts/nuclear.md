---
title: "Install Nuclear on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Streaming music player"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Nuclear on MacOS using homebrew

- App Name: Nuclear
- App description: Streaming music player
- App Version: 0.6.17
- App Website: https://nuclear.js.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Nuclear with the following command
   ```
   brew install --cask nuclear
   ```
4. Nuclear is ready to use now!