---
title: "Install atomist-cli on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Unified command-line tool for interacting with Atomist services"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install atomist-cli on MacOS using homebrew

- App Name: atomist-cli
- App description: Unified command-line tool for interacting with Atomist services
- App Version: 1.8.0
- App Website: https://github.com/atomist/cli#readme

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install atomist-cli with the following command
   ```
   brew install atomist-cli
   ```
4. atomist-cli is ready to use now!