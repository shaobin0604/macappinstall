---
title: "Install bento4 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Full-featured MP4 format and MPEG DASH library and tools"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bento4 on MacOS using homebrew

- App Name: bento4
- App description: Full-featured MP4 format and MPEG DASH library and tools
- App Version: 1.6.0-639
- App Website: https://www.bento4.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bento4 with the following command
   ```
   brew install bento4
   ```
4. bento4 is ready to use now!