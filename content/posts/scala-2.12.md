---
title: "Install scala@2.12 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "JVM-based programming language"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install scala@2.12 on MacOS using homebrew

- App Name: scala@2.12
- App description: JVM-based programming language
- App Version: 2.12.14
- App Website: https://www.scala-lang.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install scala@2.12 with the following command
   ```
   brew install scala@2.12
   ```
4. scala@2.12 is ready to use now!