---
title: "Install bam on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Build system that uses Lua to describe the build process"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bam on MacOS using homebrew

- App Name: bam
- App description: Build system that uses Lua to describe the build process
- App Version: 0.5.1
- App Website: https://matricks.github.io/bam/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bam with the following command
   ```
   brew install bam
   ```
4. bam is ready to use now!