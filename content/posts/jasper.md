---
title: "Install jasper on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library for manipulating JPEG-2000 images"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install jasper on MacOS using homebrew

- App Name: jasper
- App description: Library for manipulating JPEG-2000 images
- App Version: 2.0.33
- App Website: https://ece.engr.uvic.ca/~frodo/jasper/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install jasper with the following command
   ```
   brew install jasper
   ```
4. jasper is ready to use now!