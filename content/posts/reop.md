---
title: "Install reop on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Encrypted keypair management"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install reop on MacOS using homebrew

- App Name: reop
- App description: Encrypted keypair management
- App Version: 2.1.1
- App Website: https://flak.tedunangst.com/post/reop

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install reop with the following command
   ```
   brew install reop
   ```
4. reop is ready to use now!