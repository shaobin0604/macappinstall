---
title: "Install Polycom Content App on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Polycom Content App on MacOS using homebrew

- App Name: Polycom Content App
- App description: null
- App Version: 1.3.4.73535
- App Website: https://www.polycom.com/content-collaboration/content-sharing/content-app.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Polycom Content App with the following command
   ```
   brew install --cask polycom-content
   ```
4. Polycom Content App is ready to use now!