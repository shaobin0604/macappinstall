---
title: "Install bde on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Basic Development Environment: foundational C++ libraries used at Bloomberg"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bde on MacOS using homebrew

- App Name: bde
- App description: Basic Development Environment: foundational C++ libraries used at Bloomberg
- App Version: 3.61.0.0
- App Website: https://github.com/bloomberg/bde

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bde with the following command
   ```
   brew install bde
   ```
4. bde is ready to use now!