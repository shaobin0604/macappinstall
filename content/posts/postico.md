---
title: "Install Postico on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GUI client for PostgreSQL databases"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Postico on MacOS using homebrew

- App Name: Postico
- App description: GUI client for PostgreSQL databases
- App Version: 1.5.20
- App Website: https://eggerapps.at/postico/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Postico with the following command
   ```
   brew install --cask postico
   ```
4. Postico is ready to use now!