---
title: "Install mupdf-tools on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Lightweight PDF and XPS viewer"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mupdf-tools on MacOS using homebrew

- App Name: mupdf-tools
- App description: Lightweight PDF and XPS viewer
- App Version: 1.19.0
- App Website: https://mupdf.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mupdf-tools with the following command
   ```
   brew install mupdf-tools
   ```
4. mupdf-tools is ready to use now!