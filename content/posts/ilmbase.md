---
title: "Install ilmbase on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "OpenEXR ILM Base libraries (high dynamic-range image file format)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ilmbase on MacOS using homebrew

- App Name: ilmbase
- App description: OpenEXR ILM Base libraries (high dynamic-range image file format)
- App Version: 2.5.7
- App Website: https://www.openexr.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ilmbase with the following command
   ```
   brew install ilmbase
   ```
4. ilmbase is ready to use now!