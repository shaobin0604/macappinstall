---
title: "Install kepubify on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Convert ebooks from epub to kepub"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install kepubify on MacOS using homebrew

- App Name: kepubify
- App description: Convert ebooks from epub to kepub
- App Version: 4.0.3
- App Website: https://pgaskin.net/kepubify/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install kepubify with the following command
   ```
   brew install kepubify
   ```
4. kepubify is ready to use now!