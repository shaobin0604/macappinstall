---
title: "Install ld-find-code-refs on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Build tool for sending feature flag code references to LaunchDarkly"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ld-find-code-refs on MacOS using homebrew

- App Name: ld-find-code-refs
- App description: Build tool for sending feature flag code references to LaunchDarkly
- App Version: 2.5.0
- App Website: https://github.com/launchdarkly/ld-find-code-refs

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ld-find-code-refs with the following command
   ```
   brew install ld-find-code-refs
   ```
4. ld-find-code-refs is ready to use now!