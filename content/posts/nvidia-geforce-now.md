---
title: "Install NVIDIA GeForce NOW on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cloud gaming platform"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install NVIDIA GeForce NOW on MacOS using homebrew

- App Name: NVIDIA GeForce NOW
- App description: Cloud gaming platform
- App Version: 2.0.37.148
- App Website: https://www.nvidia.com/en-us/geforce-now/download/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install NVIDIA GeForce NOW with the following command
   ```
   brew install --cask nvidia-geforce-now
   ```
4. NVIDIA GeForce NOW is ready to use now!