---
title: "Install structurizr-cli on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line utility for Structurizr"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install structurizr-cli on MacOS using homebrew

- App Name: structurizr-cli
- App description: Command-line utility for Structurizr
- App Version: 1.17.0
- App Website: https://structurizr.com

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install structurizr-cli with the following command
   ```
   brew install structurizr-cli
   ```
4. structurizr-cli is ready to use now!