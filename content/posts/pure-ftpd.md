---
title: "Install pure-ftpd on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Secure and efficient FTP server"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pure-ftpd on MacOS using homebrew

- App Name: pure-ftpd
- App description: Secure and efficient FTP server
- App Version: 1.0.50
- App Website: https://www.pureftpd.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pure-ftpd with the following command
   ```
   brew install pure-ftpd
   ```
4. pure-ftpd is ready to use now!