---
title: "Install Get Backup Pro 3 on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Backup software with folder synchronization"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Get Backup Pro 3 on MacOS using homebrew

- App Name: Get Backup Pro 3
- App description: Backup software with folder synchronization
- App Version: 3.6.5,1616
- App Website: https://www.belightsoft.com/products/getbackup/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Get Backup Pro 3 with the following command
   ```
   brew install --cask get-backup-pro
   ```
4. Get Backup Pro 3 is ready to use now!