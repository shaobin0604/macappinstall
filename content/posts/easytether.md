---
title: "Install EasyTether on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Carrier-independent Internet connection sharing (tethering)"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install EasyTether on MacOS using homebrew

- App Name: EasyTether
- App description: Carrier-independent Internet connection sharing (tethering)
- App Version: 16
- App Website: http://www.mobile-stream.com/index.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install EasyTether with the following command
   ```
   brew install --cask easytether
   ```
4. EasyTether is ready to use now!