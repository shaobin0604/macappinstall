---
title: "Install JRiver Media Center on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Manage and play your media"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install JRiver Media Center on MacOS using homebrew

- App Name: JRiver Media Center
- App description: Manage and play your media
- App Version: 28.00.96
- App Website: https://www.jriver.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install JRiver Media Center with the following command
   ```
   brew install --cask media-center
   ```
4. JRiver Media Center is ready to use now!