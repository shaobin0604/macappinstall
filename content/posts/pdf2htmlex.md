---
title: "Install pdf2htmlex on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "PDF to HTML converter"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pdf2htmlex on MacOS using homebrew

- App Name: pdf2htmlex
- App description: PDF to HTML converter
- App Version: 0.14.6
- App Website: https://coolwanglu.github.io/pdf2htmlEX/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pdf2htmlex with the following command
   ```
   brew install pdf2htmlex
   ```
4. pdf2htmlex is ready to use now!