---
title: "Install Cloudup on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Instantly and securely share anything"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Cloudup on MacOS using homebrew

- App Name: Cloudup
- App description: Instantly and securely share anything
- App Version: 1.15.4
- App Website: https://cloudup.com/download

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Cloudup with the following command
   ```
   brew install --cask cloudup
   ```
4. Cloudup is ready to use now!