---
title: "Install Kactus on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "True version control tool for designers"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Kactus on MacOS using homebrew

- App Name: Kactus
- App description: True version control tool for designers
- App Version: 0.3.34
- App Website: https://kactus.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Kactus with the following command
   ```
   brew install --cask kactus
   ```
4. Kactus is ready to use now!