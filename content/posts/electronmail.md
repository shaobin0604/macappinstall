---
title: "Install ElectronMail on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Unofficial ProtonMail Desktop App"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ElectronMail on MacOS using homebrew

- App Name: ElectronMail
- App description: Unofficial ProtonMail Desktop App
- App Version: 4.13.2
- App Website: https://github.com/vladimiry/ElectronMail

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ElectronMail with the following command
   ```
   brew install --cask electronmail
   ```
4. ElectronMail is ready to use now!