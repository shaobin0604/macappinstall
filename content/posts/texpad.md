---
title: "Install Texpad on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "LaTeX editor"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Texpad on MacOS using homebrew

- App Name: Texpad
- App description: LaTeX editor
- App Version: 1.9.6,646,e78f29b
- App Website: https://www.texpad.com/mac

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Texpad with the following command
   ```
   brew install --cask texpad
   ```
4. Texpad is ready to use now!