---
title: "Install tomee-plus on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Everything in TomEE Web Profile and JAX-RS, plus more"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tomee-plus on MacOS using homebrew

- App Name: tomee-plus
- App description: Everything in TomEE Web Profile and JAX-RS, plus more
- App Version: 8.0.9
- App Website: https://tomee.apache.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tomee-plus with the following command
   ```
   brew install tomee-plus
   ```
4. tomee-plus is ready to use now!