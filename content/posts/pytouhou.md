---
title: "Install pytouhou on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Libre implementation of Touhou 6 engine"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pytouhou on MacOS using homebrew

- App Name: pytouhou
- App description: Libre implementation of Touhou 6 engine
- App Version: 634
- App Website: https://pytouhou.linkmauve.fr/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pytouhou with the following command
   ```
   brew install pytouhou
   ```
4. pytouhou is ready to use now!