---
title: "Install jandi on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Github Contributions in your status bar"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install jandi on MacOS using homebrew

- App Name: jandi
- App description: Github Contributions in your status bar
- App Version: 1.8
- App Website: https://github.com/techinpark/Jandi

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install jandi with the following command
   ```
   brew install --cask jandi-statusbar
   ```
4. jandi is ready to use now!