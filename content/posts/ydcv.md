---
title: "Install ydcv on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "YouDao Console Version"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ydcv on MacOS using homebrew

- App Name: ydcv
- App description: YouDao Console Version
- App Version: 0.7
- App Website: https://github.com/felixonmars/ydcv

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ydcv with the following command
   ```
   brew install ydcv
   ```
4. ydcv is ready to use now!