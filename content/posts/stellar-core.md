---
title: "Install stellar-core on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Backbone of the Stellar (XLM) network"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install stellar-core on MacOS using homebrew

- App Name: stellar-core
- App description: Backbone of the Stellar (XLM) network
- App Version: 18.2.0
- App Website: https://www.stellar.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install stellar-core with the following command
   ```
   brew install stellar-core
   ```
4. stellar-core is ready to use now!