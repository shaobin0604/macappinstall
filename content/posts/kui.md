---
title: "Install Kui on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Hybrid command-line/UI development experience for cloud-native development"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Kui on MacOS using homebrew

- App Name: Kui
- App description: Hybrid command-line/UI development experience for cloud-native development
- App Version: 11.2.5
- App Website: https://github.com/kubernetes-sigs/kui

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Kui with the following command
   ```
   brew install --cask kui
   ```
4. Kui is ready to use now!