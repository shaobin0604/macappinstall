---
title: "Install pyinvoke on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Pythonic task management & command execution"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pyinvoke on MacOS using homebrew

- App Name: pyinvoke
- App description: Pythonic task management & command execution
- App Version: 1.6.0
- App Website: https://www.pyinvoke.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pyinvoke with the following command
   ```
   brew install pyinvoke
   ```
4. pyinvoke is ready to use now!