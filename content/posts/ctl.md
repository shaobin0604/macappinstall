---
title: "Install ctl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Programming language for digital color management"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ctl on MacOS using homebrew

- App Name: ctl
- App description: Programming language for digital color management
- App Version: 1.5.2
- App Website: https://github.com/ampas/CTL

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ctl with the following command
   ```
   brew install ctl
   ```
4. ctl is ready to use now!