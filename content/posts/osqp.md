---
title: "Install osqp on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Operator splitting QP solver"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install osqp on MacOS using homebrew

- App Name: osqp
- App description: Operator splitting QP solver
- App Version: 0.6.2
- App Website: https://osqp.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install osqp with the following command
   ```
   brew install osqp
   ```
4. osqp is ready to use now!