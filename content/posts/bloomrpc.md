---
title: "Install BloomRPC on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GUI Client for GRPC Services"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install BloomRPC on MacOS using homebrew

- App Name: BloomRPC
- App description: GUI Client for GRPC Services
- App Version: 1.5.3
- App Website: https://github.com/uw-labs/bloomrpc

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install BloomRPC with the following command
   ```
   brew install --cask bloomrpc
   ```
4. BloomRPC is ready to use now!