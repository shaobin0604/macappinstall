---
title: "Install osi on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Open Solver Interface"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install osi on MacOS using homebrew

- App Name: osi
- App description: Open Solver Interface
- App Version: 0.108.7
- App Website: https://github.com/coin-or/Osi

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install osi with the following command
   ```
   brew install osi
   ```
4. osi is ready to use now!