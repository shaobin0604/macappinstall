---
title: "Install liblcf on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library for RPG Maker 2000/2003 games data"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install liblcf on MacOS using homebrew

- App Name: liblcf
- App description: Library for RPG Maker 2000/2003 games data
- App Version: 0.7.0
- App Website: https://easyrpg.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install liblcf with the following command
   ```
   brew install liblcf
   ```
4. liblcf is ready to use now!