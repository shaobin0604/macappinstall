---
title: "Install zlib-ng on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Zlib replacement with optimizations for next generation systems"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install zlib-ng on MacOS using homebrew

- App Name: zlib-ng
- App description: Zlib replacement with optimizations for next generation systems
- App Version: 2.0.6
- App Website: https://github.com/zlib-ng/zlib-ng

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install zlib-ng with the following command
   ```
   brew install zlib-ng
   ```
4. zlib-ng is ready to use now!