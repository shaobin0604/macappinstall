---
title: "Install argocd-vault-plugin on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Argo CD plugin to retrieve secrets from Secret Management tools"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install argocd-vault-plugin on MacOS using homebrew

- App Name: argocd-vault-plugin
- App description: Argo CD plugin to retrieve secrets from Secret Management tools
- App Version: 1.9.0
- App Website: https://argocd-vault-plugin.readthedocs.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install argocd-vault-plugin with the following command
   ```
   brew install argocd-vault-plugin
   ```
4. argocd-vault-plugin is ready to use now!