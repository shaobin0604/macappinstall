---
title: "Install axel on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Light UNIX download accelerator"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install axel on MacOS using homebrew

- App Name: axel
- App description: Light UNIX download accelerator
- App Version: 2.17.11
- App Website: https://github.com/eribertomota/axel

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install axel with the following command
   ```
   brew install axel
   ```
4. axel is ready to use now!