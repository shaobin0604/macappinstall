---
title: "Install chisel on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Collection of LLDB commands to assist debugging iOS apps"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install chisel on MacOS using homebrew

- App Name: chisel
- App description: Collection of LLDB commands to assist debugging iOS apps
- App Version: 2.0.1
- App Website: https://github.com/facebook/chisel

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install chisel with the following command
   ```
   brew install chisel
   ```
4. chisel is ready to use now!