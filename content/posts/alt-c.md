---
title: "Install Alt-C on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Two-way text copying"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Alt-C on MacOS using homebrew

- App Name: Alt-C
- App description: Two-way text copying
- App Version: 1.0.7
- App Website: https://altcopy.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Alt-C with the following command
   ```
   brew install --cask alt-c
   ```
4. Alt-C is ready to use now!