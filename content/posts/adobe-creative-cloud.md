---
title: "Install Adobe Creative Cloud on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Collection of apps and services for photography, design, video, web, and UX"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Adobe Creative Cloud on MacOS using homebrew

- App Name: Adobe Creative Cloud
- App description: Collection of apps and services for photography, design, video, web, and UX
- App Version: 5.6.5.58
- App Website: https://www.adobe.com/creativecloud.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Adobe Creative Cloud with the following command
   ```
   brew install --cask adobe-creative-cloud
   ```
4. Adobe Creative Cloud is ready to use now!