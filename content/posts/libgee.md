---
title: "Install libgee on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Collection library providing GObject-based interfaces"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libgee on MacOS using homebrew

- App Name: libgee
- App description: Collection library providing GObject-based interfaces
- App Version: 0.20.5
- App Website: https://wiki.gnome.org/Projects/Libgee

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libgee with the following command
   ```
   brew install libgee
   ```
4. libgee is ready to use now!