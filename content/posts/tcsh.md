---
title: "Install tcsh on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Enhanced, fully compatible version of the Berkeley C shell"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tcsh on MacOS using homebrew

- App Name: tcsh
- App description: Enhanced, fully compatible version of the Berkeley C shell
- App Version: 6.24.00
- App Website: https://www.tcsh.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tcsh with the following command
   ```
   brew install tcsh
   ```
4. tcsh is ready to use now!