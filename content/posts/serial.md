---
title: "Install Serial on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Connect to almost anything with a serial port"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Serial on MacOS using homebrew

- App Name: Serial
- App description: Connect to almost anything with a serial port
- App Version: 2.0.12,02001.27.50
- App Website: https://www.decisivetactics.com/products/serial/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Serial with the following command
   ```
   brew install --cask serial
   ```
4. Serial is ready to use now!