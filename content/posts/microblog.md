---
title: "Install Micro.blog on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Microblogging and social networking service"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Micro.blog on MacOS using homebrew

- App Name: Micro.blog
- App description: Microblogging and social networking service
- App Version: 2.3,98
- App Website: https://help.micro.blog/t/micro-blog-for-mac/45

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Micro.blog with the following command
   ```
   brew install --cask microblog
   ```
4. Micro.blog is ready to use now!