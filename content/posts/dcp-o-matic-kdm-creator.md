---
title: "Install DCP-o-matic KDM Creator on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Convert video, audio and subtitles into DCP (Digital Cinema Democratized)"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install DCP-o-matic KDM Creator on MacOS using homebrew

- App Name: DCP-o-matic KDM Creator
- App description: Convert video, audio and subtitles into DCP (Digital Cinema Democratized)
- App Version: 2.14.57
- App Website: https://dcpomatic.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install DCP-o-matic KDM Creator with the following command
   ```
   brew install --cask dcp-o-matic-kdm-creator
   ```
4. DCP-o-matic KDM Creator is ready to use now!