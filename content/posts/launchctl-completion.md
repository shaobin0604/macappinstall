---
title: "Install launchctl-completion on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Bash completion for Launchctl"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install launchctl-completion on MacOS using homebrew

- App Name: launchctl-completion
- App description: Bash completion for Launchctl
- App Version: 1.0
- App Website: https://github.com/CamJN/launchctl-completion

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install launchctl-completion with the following command
   ```
   brew install launchctl-completion
   ```
4. launchctl-completion is ready to use now!