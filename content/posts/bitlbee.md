---
title: "Install bitlbee on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "IRC to other chat networks gateway"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bitlbee on MacOS using homebrew

- App Name: bitlbee
- App description: IRC to other chat networks gateway
- App Version: 3.6
- App Website: https://www.bitlbee.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bitlbee with the following command
   ```
   brew install bitlbee
   ```
4. bitlbee is ready to use now!