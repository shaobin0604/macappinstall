---
title: "Install thors-serializer on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Declarative serialization library (JSON/YAML) for C++"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install thors-serializer on MacOS using homebrew

- App Name: thors-serializer
- App description: Declarative serialization library (JSON/YAML) for C++
- App Version: 2.2.10
- App Website: https://github.com/Loki-Astari/ThorsSerializer

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install thors-serializer with the following command
   ```
   brew install thors-serializer
   ```
4. thors-serializer is ready to use now!