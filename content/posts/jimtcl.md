---
title: "Install jimtcl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Small footprint implementation of Tcl"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install jimtcl on MacOS using homebrew

- App Name: jimtcl
- App description: Small footprint implementation of Tcl
- App Version: 0.81
- App Website: http://jim.tcl.tk/index.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install jimtcl with the following command
   ```
   brew install jimtcl
   ```
4. jimtcl is ready to use now!