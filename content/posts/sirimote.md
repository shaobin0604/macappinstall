---
title: "Install SiriMote on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Control your computer with your Apple TV Siri Remote"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install SiriMote on MacOS using homebrew

- App Name: SiriMote
- App description: Control your computer with your Apple TV Siri Remote
- App Version: 1.4.4,34
- App Website: https://eternalstorms.at/sirimote

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install SiriMote with the following command
   ```
   brew install --cask sirimote
   ```
4. SiriMote is ready to use now!