---
title: "Install Studio Link Standalone on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "SIP application to create high quality Audio over IP (AoIP) connections"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Studio Link Standalone on MacOS using homebrew

- App Name: Studio Link Standalone
- App description: SIP application to create high quality Audio over IP (AoIP) connections
- App Version: 21.05.0
- App Website: https://studio-link.de/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Studio Link Standalone with the following command
   ```
   brew install --cask studiolinkstandalone
   ```
4. Studio Link Standalone is ready to use now!