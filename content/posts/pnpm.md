---
title: "Install pnpm on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "📦🚀 Fast, disk space efficient package manager"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pnpm on MacOS using homebrew

- App Name: pnpm
- App description: 📦🚀 Fast, disk space efficient package manager
- App Version: 6.31.0
- App Website: https://pnpm.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pnpm with the following command
   ```
   brew install pnpm
   ```
4. pnpm is ready to use now!