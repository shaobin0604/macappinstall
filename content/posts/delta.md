---
title: "Install delta on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Programmatically minimize files to isolate features of interest"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install delta on MacOS using homebrew

- App Name: delta
- App description: Programmatically minimize files to isolate features of interest
- App Version: 2006.08.03
- App Website: https://web.archive.org/web/20170805142100/delta.tigris.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install delta with the following command
   ```
   brew install delta
   ```
4. delta is ready to use now!