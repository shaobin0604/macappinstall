---
title: "Install mecab-unidic-extended on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Extended morphological analyzer for MeCab"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mecab-unidic-extended on MacOS using homebrew

- App Name: mecab-unidic-extended
- App description: Extended morphological analyzer for MeCab
- App Version: 2.1.2
- App Website: https://osdn.net/projects/unidic/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mecab-unidic-extended with the following command
   ```
   brew install mecab-unidic-extended
   ```
4. mecab-unidic-extended is ready to use now!