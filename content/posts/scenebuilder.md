---
title: "Install Scene Builder on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Drag & drop GUI designer for JavaFX"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Scene Builder on MacOS using homebrew

- App Name: Scene Builder
- App description: Drag & drop GUI designer for JavaFX
- App Version: 17.0.0
- App Website: https://gluonhq.com/products/scene-builder/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Scene Builder with the following command
   ```
   brew install --cask scenebuilder
   ```
4. Scene Builder is ready to use now!