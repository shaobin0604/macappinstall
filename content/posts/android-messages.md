---
title: "Install Android Messages Desktop on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Desktop client for Android Messages"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Android Messages Desktop on MacOS using homebrew

- App Name: Android Messages Desktop
- App description: Desktop client for Android Messages
- App Version: 3.1.0
- App Website: https://github.com/chrisknepper/android-messages-desktop

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Android Messages Desktop with the following command
   ```
   brew install --cask android-messages
   ```
4. Android Messages Desktop is ready to use now!