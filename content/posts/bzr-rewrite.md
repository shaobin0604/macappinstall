---
title: "Install bzr-rewrite on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Bazaar plugin to support rewriting revisions and rebasing"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bzr-rewrite on MacOS using homebrew

- App Name: bzr-rewrite
- App description: Bazaar plugin to support rewriting revisions and rebasing
- App Version: 0.6.3
- App Website: https://launchpad.net/bzr-rewrite

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bzr-rewrite with the following command
   ```
   brew install bzr-rewrite
   ```
4. bzr-rewrite is ready to use now!