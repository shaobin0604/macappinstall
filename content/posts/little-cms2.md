---
title: "Install little-cms2 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Color management engine supporting ICC profiles"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install little-cms2 on MacOS using homebrew

- App Name: little-cms2
- App description: Color management engine supporting ICC profiles
- App Version: 2.13
- App Website: https://www.littlecms.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install little-cms2 with the following command
   ```
   brew install little-cms2
   ```
4. little-cms2 is ready to use now!