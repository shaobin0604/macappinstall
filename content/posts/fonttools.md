---
title: "Install fonttools on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library for manipulating fonts"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fonttools on MacOS using homebrew

- App Name: fonttools
- App description: Library for manipulating fonts
- App Version: 4.29.1
- App Website: https://github.com/fonttools/fonttools

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fonttools with the following command
   ```
   brew install fonttools
   ```
4. fonttools is ready to use now!