---
title: "Install scmpuff on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Adds numbered shortcuts for common git commands"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install scmpuff on MacOS using homebrew

- App Name: scmpuff
- App description: Adds numbered shortcuts for common git commands
- App Version: 0.4.0
- App Website: https://mroth.github.io/scmpuff/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install scmpuff with the following command
   ```
   brew install scmpuff
   ```
4. scmpuff is ready to use now!