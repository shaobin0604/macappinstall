---
title: "Install Azgaar's Fantasy Map Generator on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Generate interactive and highly customizable maps"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Azgaar's Fantasy Map Generator on MacOS using homebrew

- App Name: Azgaar's Fantasy Map Generator
- App description: Generate interactive and highly customizable maps
- App Version: 1.3
- App Website: https://azgaar.github.io/Fantasy-Map-Generator

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Azgaar's Fantasy Map Generator with the following command
   ```
   brew install --cask fantasy-map-generator
   ```
4. Azgaar's Fantasy Map Generator is ready to use now!