---
title: "Install Multitouch on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Add more gestures for Trackpad and Magic Mouse"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Multitouch on MacOS using homebrew

- App Name: Multitouch
- App description: Add more gestures for Trackpad and Magic Mouse
- App Version: 1.25.1,146
- App Website: https://multitouch.app/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Multitouch with the following command
   ```
   brew install --cask multitouch
   ```
4. Multitouch is ready to use now!