---
title: "Install nicotine-plus on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Graphical client for the Soulseek file sharing network"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install nicotine-plus on MacOS using homebrew

- App Name: nicotine-plus
- App description: Graphical client for the Soulseek file sharing network
- App Version: 3.2.1
- App Website: https://nicotine-plus.github.io/nicotine-plus/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install nicotine-plus with the following command
   ```
   brew install nicotine-plus
   ```
4. nicotine-plus is ready to use now!