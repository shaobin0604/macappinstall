---
title: "Install mdk on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GNU MIX development kit"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mdk on MacOS using homebrew

- App Name: mdk
- App description: GNU MIX development kit
- App Version: 1.3.0
- App Website: https://www.gnu.org/software/mdk/mdk.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mdk with the following command
   ```
   brew install mdk
   ```
4. mdk is ready to use now!