---
title: "Install spaceinvaders-go on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Space Invaders in your terminal written in Go"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install spaceinvaders-go on MacOS using homebrew

- App Name: spaceinvaders-go
- App description: Space Invaders in your terminal written in Go
- App Version: 1.2.1
- App Website: https://github.com/asib/spaceinvaders

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install spaceinvaders-go with the following command
   ```
   brew install spaceinvaders-go
   ```
4. spaceinvaders-go is ready to use now!