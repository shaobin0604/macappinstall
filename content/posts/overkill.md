---
title: "Install Overkill on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Stop iTunes from opening when you connect your iPhone"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Overkill on MacOS using homebrew

- App Name: Overkill
- App description: Stop iTunes from opening when you connect your iPhone
- App Version: 1.0
- App Website: https://github.com/KrauseFx/overkill-for-mac

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Overkill with the following command
   ```
   brew install --cask overkill
   ```
4. Overkill is ready to use now!