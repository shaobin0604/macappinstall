---
title: "Install xctool on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Drop-in replacement for xcodebuild with a few extra features"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install xctool on MacOS using homebrew

- App Name: xctool
- App description: Drop-in replacement for xcodebuild with a few extra features
- App Version: 0.3.7
- App Website: https://github.com/facebookarchive/xctool

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install xctool with the following command
   ```
   brew install xctool
   ```
4. xctool is ready to use now!