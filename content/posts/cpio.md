---
title: "Install cpio on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Copies files into or out of a cpio or tar archive"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cpio on MacOS using homebrew

- App Name: cpio
- App description: Copies files into or out of a cpio or tar archive
- App Version: 2.13
- App Website: https://www.gnu.org/software/cpio/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cpio with the following command
   ```
   brew install cpio
   ```
4. cpio is ready to use now!