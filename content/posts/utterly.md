---
title: "Install Utterly on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Remove background noise during your calls in any audio or video conferencing app"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Utterly on MacOS using homebrew

- App Name: Utterly
- App description: Remove background noise during your calls in any audio or video conferencing app
- App Version: 0.9.0-SNAPSHOT-140e1bc
- App Website: https://www.utterly.app/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Utterly with the following command
   ```
   brew install --cask utterly
   ```
4. Utterly is ready to use now!