---
title: "Install arb on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C library for arbitrary-precision interval arithmetic"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install arb on MacOS using homebrew

- App Name: arb
- App description: C library for arbitrary-precision interval arithmetic
- App Version: 2.22.1
- App Website: https://arblib.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install arb with the following command
   ```
   brew install arb
   ```
4. arb is ready to use now!