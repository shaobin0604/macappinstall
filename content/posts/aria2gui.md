---
title: "Install Aria2GUI on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Graphical user interface for Aria2"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Aria2GUI on MacOS using homebrew

- App Name: Aria2GUI
- App description: Graphical user interface for Aria2
- App Version: 1.4.1
- App Website: https://github.com/yangshun1029/aria2gui

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Aria2GUI with the following command
   ```
   brew install --cask aria2gui
   ```
4. Aria2GUI is ready to use now!