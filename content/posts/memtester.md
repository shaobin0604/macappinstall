---
title: "Install memtester on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utility for testing the memory subsystem"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install memtester on MacOS using homebrew

- App Name: memtester
- App description: Utility for testing the memory subsystem
- App Version: 4.5.1
- App Website: https://pyropus.ca/software/memtester/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install memtester with the following command
   ```
   brew install memtester
   ```
4. memtester is ready to use now!