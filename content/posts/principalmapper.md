---
title: "Install principalmapper on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Quickly evaluate IAM permissions in AWS"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install principalmapper on MacOS using homebrew

- App Name: principalmapper
- App description: Quickly evaluate IAM permissions in AWS
- App Version: 1.1.5
- App Website: https://github.com/nccgroup/PMapper

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install principalmapper with the following command
   ```
   brew install principalmapper
   ```
4. principalmapper is ready to use now!