---
title: "Install lz4 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Extremely Fast Compression algorithm"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install lz4 on MacOS using homebrew

- App Name: lz4
- App description: Extremely Fast Compression algorithm
- App Version: 1.9.3
- App Website: https://lz4.github.io/lz4/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install lz4 with the following command
   ```
   brew install lz4
   ```
4. lz4 is ready to use now!