---
title: "Install truecrack on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Brute-force password cracker for TrueCrypt"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install truecrack on MacOS using homebrew

- App Name: truecrack
- App description: Brute-force password cracker for TrueCrypt
- App Version: 3.5
- App Website: https://github.com/lvaccaro/truecrack

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install truecrack with the following command
   ```
   brew install truecrack
   ```
4. truecrack is ready to use now!