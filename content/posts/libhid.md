---
title: "Install libhid on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library to access and interact with USB HID devices"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libhid on MacOS using homebrew

- App Name: libhid
- App description: Library to access and interact with USB HID devices
- App Version: 0.2.16
- App Website: https://directory.fsf.org/wiki/Libhid

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libhid with the following command
   ```
   brew install libhid
   ```
4. libhid is ready to use now!