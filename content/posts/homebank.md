---
title: "Install homebank on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Manage your personal accounts at home"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install homebank on MacOS using homebrew

- App Name: homebank
- App description: Manage your personal accounts at home
- App Version: 5.5.4
- App Website: http://homebank.free.fr

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install homebank with the following command
   ```
   brew install homebank
   ```
4. homebank is ready to use now!