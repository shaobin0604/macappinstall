---
title: "Install gnu-barcode on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Convert text strings to printed bars"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gnu-barcode on MacOS using homebrew

- App Name: gnu-barcode
- App description: Convert text strings to printed bars
- App Version: 0.99
- App Website: https://www.gnu.org/software/barcode/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gnu-barcode with the following command
   ```
   brew install gnu-barcode
   ```
4. gnu-barcode is ready to use now!