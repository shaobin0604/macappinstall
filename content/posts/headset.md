---
title: "Install Headset on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Music player powered by YouTube and Reddit"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Headset on MacOS using homebrew

- App Name: Headset
- App description: Music player powered by YouTube and Reddit
- App Version: 3.3.3
- App Website: https://headsetapp.co/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Headset with the following command
   ```
   brew install --cask headset
   ```
4. Headset is ready to use now!