---
title: "Install mysql-sandbox on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Install one or more MySQL servers"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mysql-sandbox on MacOS using homebrew

- App Name: mysql-sandbox
- App description: Install one or more MySQL servers
- App Version: 3.2.17
- App Website: https://mysqlsandbox.net

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mysql-sandbox with the following command
   ```
   brew install mysql-sandbox
   ```
4. mysql-sandbox is ready to use now!