---
title: "Install clozure-cl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Common Lisp implementation with a long history"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install clozure-cl on MacOS using homebrew

- App Name: clozure-cl
- App description: Common Lisp implementation with a long history
- App Version: 1.12.1
- App Website: https://ccl.clozure.com

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install clozure-cl with the following command
   ```
   brew install clozure-cl
   ```
4. clozure-cl is ready to use now!