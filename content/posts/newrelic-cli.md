---
title: "Install newrelic-cli on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line interface for New Relic"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install newrelic-cli on MacOS using homebrew

- App Name: newrelic-cli
- App description: Command-line interface for New Relic
- App Version: 0.41.24
- App Website: https://github.com/newrelic/newrelic-cli

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install newrelic-cli with the following command
   ```
   brew install newrelic-cli
   ```
4. newrelic-cli is ready to use now!