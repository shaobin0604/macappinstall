---
title: "Install age on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Simple, modern, secure file encryption"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install age on MacOS using homebrew

- App Name: age
- App description: Simple, modern, secure file encryption
- App Version: 1.0.0
- App Website: https://filippo.io/age

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install age with the following command
   ```
   brew install age
   ```
4. age is ready to use now!