---
title: "Install Orange on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Component-based data mining software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Orange on MacOS using homebrew

- App Name: Orange
- App description: Component-based data mining software
- App Version: 3.31.1
- App Website: https://orange.biolab.si/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Orange with the following command
   ```
   brew install --cask orange
   ```
4. Orange is ready to use now!