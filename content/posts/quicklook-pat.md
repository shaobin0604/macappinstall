---
title: "Install Adobe Photoshop Patterns Quicklook Plugin on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Adobe Photoshop Patterns Quicklook Plugin on MacOS using homebrew

- App Name: Adobe Photoshop Patterns Quicklook Plugin
- App description: null
- App Version: 1.0.0
- App Website: https://github.com/pixelrowdies/quicklook-pat

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Adobe Photoshop Patterns Quicklook Plugin with the following command
   ```
   brew install --cask quicklook-pat
   ```
4. Adobe Photoshop Patterns Quicklook Plugin is ready to use now!