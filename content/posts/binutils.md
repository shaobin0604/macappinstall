---
title: "Install binutils on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GNU binary tools for native development"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install binutils on MacOS using homebrew

- App Name: binutils
- App description: GNU binary tools for native development
- App Version: 2.37
- App Website: https://www.gnu.org/software/binutils/binutils.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install binutils with the following command
   ```
   brew install binutils
   ```
4. binutils is ready to use now!