---
title: "Install sigrok-cli on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Sigrok command-line interface to use logic analyzers and more"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sigrok-cli on MacOS using homebrew

- App Name: sigrok-cli
- App description: Sigrok command-line interface to use logic analyzers and more
- App Version: 0.7.2
- App Website: https://sigrok.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sigrok-cli with the following command
   ```
   brew install sigrok-cli
   ```
4. sigrok-cli is ready to use now!