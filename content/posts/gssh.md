---
title: "Install gssh on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "SSH automation tool based on Groovy DSL"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gssh on MacOS using homebrew

- App Name: gssh
- App description: SSH automation tool based on Groovy DSL
- App Version: 2.10.1
- App Website: https://github.com/int128/groovy-ssh

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gssh with the following command
   ```
   brew install gssh
   ```
4. gssh is ready to use now!