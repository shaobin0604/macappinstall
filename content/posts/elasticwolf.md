---
title: "Install AWS ElasticWolf Client Console on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Manage Amazon Web Services (AWS) cloud resources"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install AWS ElasticWolf Client Console on MacOS using homebrew

- App Name: AWS ElasticWolf Client Console
- App description: Manage Amazon Web Services (AWS) cloud resources
- App Version: 5.1.7
- App Website: https://aws.amazon.com/tools/aws-elasticwolf-client-console/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install AWS ElasticWolf Client Console with the following command
   ```
   brew install --cask elasticwolf
   ```
4. AWS ElasticWolf Client Console is ready to use now!