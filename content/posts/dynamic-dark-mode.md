---
title: "Install Dynamic Dark Mode on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Automatic Dark Mode toggle"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Dynamic Dark Mode on MacOS using homebrew

- App Name: Dynamic Dark Mode
- App description: Automatic Dark Mode toggle
- App Version: 1.5.2
- App Website: https://github.com/ApolloZhu/Dynamic-Dark-Mode

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Dynamic Dark Mode with the following command
   ```
   brew install --cask dynamic-dark-mode
   ```
4. Dynamic Dark Mode is ready to use now!