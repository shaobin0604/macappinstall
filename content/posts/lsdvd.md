---
title: "Install lsdvd on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Read the content info of a DVD"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install lsdvd on MacOS using homebrew

- App Name: lsdvd
- App description: Read the content info of a DVD
- App Version: 0.17
- App Website: https://sourceforge.net/projects/lsdvd

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install lsdvd with the following command
   ```
   brew install lsdvd
   ```
4. lsdvd is ready to use now!