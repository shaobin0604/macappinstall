---
title: "Install dcled on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Linux driver for dream cheeky USB message board"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dcled on MacOS using homebrew

- App Name: dcled
- App description: Linux driver for dream cheeky USB message board
- App Version: 2.2
- App Website: https://www.jeffrika.com/~malakai/dcled/index.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dcled with the following command
   ```
   brew install dcled
   ```
4. dcled is ready to use now!