---
title: "Install liblouis on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Open-source braille translator and back-translator"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install liblouis on MacOS using homebrew

- App Name: liblouis
- App description: Open-source braille translator and back-translator
- App Version: 3.20.0
- App Website: http://liblouis.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install liblouis with the following command
   ```
   brew install liblouis
   ```
4. liblouis is ready to use now!