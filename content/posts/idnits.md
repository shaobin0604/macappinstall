---
title: "Install idnits on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Looks for problems in internet draft formatting"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install idnits on MacOS using homebrew

- App Name: idnits
- App description: Looks for problems in internet draft formatting
- App Version: 2.17.00
- App Website: https://tools.ietf.org/tools/idnits/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install idnits with the following command
   ```
   brew install idnits
   ```
4. idnits is ready to use now!