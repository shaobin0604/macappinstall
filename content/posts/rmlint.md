---
title: "Install rmlint on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Extremely fast tool to remove dupes and other lint from your filesystem"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install rmlint on MacOS using homebrew

- App Name: rmlint
- App description: Extremely fast tool to remove dupes and other lint from your filesystem
- App Version: 2.10.1
- App Website: https://github.com/sahib/rmlint

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install rmlint with the following command
   ```
   brew install rmlint
   ```
4. rmlint is ready to use now!