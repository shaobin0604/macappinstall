---
title: "Install rrdtool on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Round Robin Database"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install rrdtool on MacOS using homebrew

- App Name: rrdtool
- App description: Round Robin Database
- App Version: 1.7.2
- App Website: https://oss.oetiker.ch/rrdtool/index.en.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install rrdtool with the following command
   ```
   brew install rrdtool
   ```
4. rrdtool is ready to use now!