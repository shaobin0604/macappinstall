---
title: "Install libwpd on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "General purpose library for reading WordPerfect files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libwpd on MacOS using homebrew

- App Name: libwpd
- App description: General purpose library for reading WordPerfect files
- App Version: 0.10.3
- App Website: https://libwpd.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libwpd with the following command
   ```
   brew install libwpd
   ```
4. libwpd is ready to use now!