---
title: "Install subversion@1.8 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Version control system"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install subversion@1.8 on MacOS using homebrew

- App Name: subversion@1.8
- App description: Version control system
- App Version: 1.8.19
- App Website: https://subversion.apache.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install subversion@1.8 with the following command
   ```
   brew install subversion@1.8
   ```
4. subversion@1.8 is ready to use now!