---
title: "Install TREZOR Bridge on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Facilitates communication between the Trezor device and supported browsers"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install TREZOR Bridge on MacOS using homebrew

- App Name: TREZOR Bridge
- App description: Facilitates communication between the Trezor device and supported browsers
- App Version: 2.0.30
- App Website: https://wallet.trezor.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install TREZOR Bridge with the following command
   ```
   brew install --cask trezor-bridge
   ```
4. TREZOR Bridge is ready to use now!