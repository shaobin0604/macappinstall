---
title: "Install btfs on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "BitTorrent filesystem based on FUSE"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install btfs on MacOS using homebrew

- App Name: btfs
- App description: BitTorrent filesystem based on FUSE
- App Version: 2.22
- App Website: https://github.com/johang/btfs

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install btfs with the following command
   ```
   brew install btfs
   ```
4. btfs is ready to use now!