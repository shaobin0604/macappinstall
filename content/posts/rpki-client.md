---
title: "Install rpki-client on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "OpenBSD portable rpki-client"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install rpki-client on MacOS using homebrew

- App Name: rpki-client
- App description: OpenBSD portable rpki-client
- App Version: 7.6
- App Website: https://www.rpki-client.org/index.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install rpki-client with the following command
   ```
   brew install rpki-client
   ```
4. rpki-client is ready to use now!