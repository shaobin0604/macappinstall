---
title: "Install pkgconf on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Package compiler and linker metadata toolkit"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pkgconf on MacOS using homebrew

- App Name: pkgconf
- App description: Package compiler and linker metadata toolkit
- App Version: 1.8.0
- App Website: https://git.sr.ht/~kaniini/pkgconf

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pkgconf with the following command
   ```
   brew install pkgconf
   ```
4. pkgconf is ready to use now!