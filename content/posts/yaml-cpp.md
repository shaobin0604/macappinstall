---
title: "Install yaml-cpp on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C++ YAML parser and emitter for YAML 1.2 spec"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install yaml-cpp on MacOS using homebrew

- App Name: yaml-cpp
- App description: C++ YAML parser and emitter for YAML 1.2 spec
- App Version: 0.7.0
- App Website: https://github.com/jbeder/yaml-cpp

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install yaml-cpp with the following command
   ```
   brew install yaml-cpp
   ```
4. yaml-cpp is ready to use now!