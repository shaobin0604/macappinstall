---
title: "Install argo on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Get stuff done with container-native workflows for Kubernetes"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install argo on MacOS using homebrew

- App Name: argo
- App description: Get stuff done with container-native workflows for Kubernetes
- App Version: 3.2.8
- App Website: https://argoproj.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install argo with the following command
   ```
   brew install argo
   ```
4. argo is ready to use now!