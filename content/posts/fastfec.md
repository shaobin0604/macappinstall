---
title: "Install fastfec on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Extremely fast FEC filing parser written in C"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fastfec on MacOS using homebrew

- App Name: fastfec
- App description: Extremely fast FEC filing parser written in C
- App Version: 0.0.4
- App Website: https://github.com/washingtonpost/FastFEC

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fastfec with the following command
   ```
   brew install fastfec
   ```
4. fastfec is ready to use now!