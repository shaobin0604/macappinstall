---
title: "Install jruby on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Ruby implementation in pure Java"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install jruby on MacOS using homebrew

- App Name: jruby
- App description: Ruby implementation in pure Java
- App Version: 9.3.3.0
- App Website: https://www.jruby.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install jruby with the following command
   ```
   brew install jruby
   ```
4. jruby is ready to use now!