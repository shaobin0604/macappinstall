---
title: "Install Usenapp on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Newsreader and Usenet client"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Usenapp on MacOS using homebrew

- App Name: Usenapp
- App description: Newsreader and Usenet client
- App Version: 1.17,351
- App Website: https://www.usenapp.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Usenapp with the following command
   ```
   brew install --cask usenapp
   ```
4. Usenapp is ready to use now!