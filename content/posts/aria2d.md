---
title: "Install Aria2D on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Aria2 GUI"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Aria2D on MacOS using homebrew

- App Name: Aria2D
- App description: Aria2 GUI
- App Version: 1.3.6,509
- App Website: https://github.com/xjbeta/Aria2D

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Aria2D with the following command
   ```
   brew install --cask aria2d
   ```
4. Aria2D is ready to use now!