---
title: "Install Intel Parallel Studio XE Composer Edition for C++ on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Software development tools"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Intel Parallel Studio XE Composer Edition for C++ on MacOS using homebrew

- App Name: Intel Parallel Studio XE Composer Edition for C++
- App description: Software development tools
- App Version: 2020.2.899,16768
- App Website: https://software.intel.com/en-us/parallel-studio-xe

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Intel Parallel Studio XE Composer Edition for C++ with the following command
   ```
   brew install --cask intel-psxe-ce-c-plus-plus
   ```
4. Intel Parallel Studio XE Composer Edition for C++ is ready to use now!