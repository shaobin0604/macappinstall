---
title: "Install spiped on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Secure pipe daemon"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install spiped on MacOS using homebrew

- App Name: spiped
- App description: Secure pipe daemon
- App Version: 1.6.2
- App Website: https://www.tarsnap.com/spiped.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install spiped with the following command
   ```
   brew install spiped
   ```
4. spiped is ready to use now!