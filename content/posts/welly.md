---
title: "Install Welly on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "BBS client"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Welly on MacOS using homebrew

- App Name: Welly
- App description: BBS client
- App Version: 3.2.0
- App Website: https://github.com/clyang/welly

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Welly with the following command
   ```
   brew install --cask welly
   ```
4. Welly is ready to use now!