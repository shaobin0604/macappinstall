---
title: "Install dpkg on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Debian package management system"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dpkg on MacOS using homebrew

- App Name: dpkg
- App description: Debian package management system
- App Version: 1.21.1
- App Website: https://wiki.debian.org/Teams/Dpkg

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dpkg with the following command
   ```
   brew install dpkg
   ```
4. dpkg is ready to use now!