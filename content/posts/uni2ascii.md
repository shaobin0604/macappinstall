---
title: "Install uni2ascii on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Bi-directional conversion between UTF-8 and various ASCII flavors"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install uni2ascii on MacOS using homebrew

- App Name: uni2ascii
- App description: Bi-directional conversion between UTF-8 and various ASCII flavors
- App Version: 4.18
- App Website: https://billposer.org/Software/uni2ascii.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install uni2ascii with the following command
   ```
   brew install uni2ascii
   ```
4. uni2ascii is ready to use now!