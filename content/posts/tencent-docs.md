---
title: "Install Tencent Docs on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Online editor for Word, Excel and PPT documents"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Tencent Docs on MacOS using homebrew

- App Name: Tencent Docs
- App description: Online editor for Word, Excel and PPT documents
- App Version: 2.2.32
- App Website: https://docs.qq.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Tencent Docs with the following command
   ```
   brew install --cask tencent-docs
   ```
4. Tencent Docs is ready to use now!