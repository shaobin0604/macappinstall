---
title: "Install vitess on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Database clustering system for horizontal scaling of MySQL"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install vitess on MacOS using homebrew

- App Name: vitess
- App description: Database clustering system for horizontal scaling of MySQL
- App Version: 12.0.3
- App Website: https://vitess.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install vitess with the following command
   ```
   brew install vitess
   ```
4. vitess is ready to use now!