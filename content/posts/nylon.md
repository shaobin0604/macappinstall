---
title: "Install nylon on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Proxy server"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install nylon on MacOS using homebrew

- App Name: nylon
- App description: Proxy server
- App Version: 1.21
- App Website: https://github.com/smeinecke/nylon

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install nylon with the following command
   ```
   brew install nylon
   ```
4. nylon is ready to use now!