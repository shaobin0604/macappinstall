---
title: "Install Harzing Publish or Perish on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Harzing Publish or Perish on MacOS using homebrew

- App Name: Harzing Publish or Perish
- App description: null
- App Version: 7.28.3033.7654,2020.12.14.1151
- App Website: https://harzing.com/resources/publish-or-perish

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Harzing Publish or Perish with the following command
   ```
   brew install --cask publish-or-perish
   ```
4. Harzing Publish or Perish is ready to use now!