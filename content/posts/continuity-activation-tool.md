---
title: "Install Continuity Activation Tool on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Enable continuity features on compatible hardware"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Continuity Activation Tool on MacOS using homebrew

- App Name: Continuity Activation Tool
- App description: Enable continuity features on compatible hardware
- App Version: 2.0
- App Website: https://github.com/dokterdok/Continuity-Activation-Tool/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Continuity Activation Tool with the following command
   ```
   brew install --cask continuity-activation-tool
   ```
4. Continuity Activation Tool is ready to use now!