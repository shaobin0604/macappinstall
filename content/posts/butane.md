---
title: "Install butane on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Translates human-readable Butane Configs into machine-readable Ignition Configs"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install butane on MacOS using homebrew

- App Name: butane
- App description: Translates human-readable Butane Configs into machine-readable Ignition Configs
- App Version: 0.14.0
- App Website: https://github.com/coreos/butane

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install butane with the following command
   ```
   brew install butane
   ```
4. butane is ready to use now!