---
title: "Install tcptunnel on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "TCP port forwarder"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tcptunnel on MacOS using homebrew

- App Name: tcptunnel
- App description: TCP port forwarder
- App Version: 0.8
- App Website: http://www.vakuumverpackt.de/tcptunnel/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tcptunnel with the following command
   ```
   brew install tcptunnel
   ```
4. tcptunnel is ready to use now!