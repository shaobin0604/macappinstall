---
title: "Install DiskCatalogMaker on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Toast-bundled version of DiskCatalogMaker"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install DiskCatalogMaker on MacOS using homebrew

- App Name: DiskCatalogMaker
- App description: Toast-bundled version of DiskCatalogMaker
- App Version: 8.4.6
- App Website: https://diskcatalogmaker.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install DiskCatalogMaker with the following command
   ```
   brew install --cask diskcatalogmaker
   ```
4. DiskCatalogMaker is ready to use now!