---
title: "Install osc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line interface to work with an Open Build Service"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install osc on MacOS using homebrew

- App Name: osc
- App description: Command-line interface to work with an Open Build Service
- App Version: 0.175.0
- App Website: https://openbuildservice.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install osc with the following command
   ```
   brew install osc
   ```
4. osc is ready to use now!