---
title: "Install jtool2 on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to help out reverse engineering, security researchers, and tweak developers"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install jtool2 on MacOS using homebrew

- App Name: jtool2
- App description: Tool to help out reverse engineering, security researchers, and tweak developers
- App Version: 2020.02.10
- App Website: http://newosxbook.com/tools/jtool.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install jtool2 with the following command
   ```
   brew install --cask jtool2
   ```
4. jtool2 is ready to use now!