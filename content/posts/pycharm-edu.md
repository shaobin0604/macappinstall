---
title: "Install Jetbrains PyCharm Educational Edition on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Professional IDE for scientific and web Python development"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Jetbrains PyCharm Educational Edition on MacOS using homebrew

- App Name: Jetbrains PyCharm Educational Edition
- App description: Professional IDE for scientific and web Python development
- App Version: 2021.3.2,213.6461.89
- App Website: https://www.jetbrains.com/pycharm-edu/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Jetbrains PyCharm Educational Edition with the following command
   ```
   brew install --cask pycharm-edu
   ```
4. Jetbrains PyCharm Educational Edition is ready to use now!