---
title: "Install easy-rsa on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CLI utility to build and manage a PKI CA"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install easy-rsa on MacOS using homebrew

- App Name: easy-rsa
- App description: CLI utility to build and manage a PKI CA
- App Version: 3.0.8
- App Website: https://github.com/OpenVPN/easy-rsa

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install easy-rsa with the following command
   ```
   brew install easy-rsa
   ```
4. easy-rsa is ready to use now!