---
title: "Install tsung on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Load testing for HTTP, PostgreSQL, Jabber, and others"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tsung on MacOS using homebrew

- App Name: tsung
- App description: Load testing for HTTP, PostgreSQL, Jabber, and others
- App Version: 1.7.0
- App Website: http://tsung.erlang-projects.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tsung with the following command
   ```
   brew install tsung
   ```
4. tsung is ready to use now!