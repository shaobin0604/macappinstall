---
title: "Install Wintertime on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utility to freeze apps running in the background to save battery"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Wintertime on MacOS using homebrew

- App Name: Wintertime
- App description: Utility to freeze apps running in the background to save battery
- App Version: 0.0.7
- App Website: https://github.com/actuallymentor/wintertime-mac-background-freezer

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Wintertime with the following command
   ```
   brew install --cask wintertime
   ```
4. Wintertime is ready to use now!