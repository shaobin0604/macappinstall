---
title: "Install iZip on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "App to manage ZIP, ZIPX, RAR, TAR, 7ZIP and other compressed files"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install iZip on MacOS using homebrew

- App Name: iZip
- App description: App to manage ZIP, ZIPX, RAR, TAR, 7ZIP and other compressed files
- App Version: 3.99
- App Website: https://www.izip.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install iZip with the following command
   ```
   brew install --cask izip
   ```
4. iZip is ready to use now!