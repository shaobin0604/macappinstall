---
title: "Install OpenSubtitles FlixTools Lite on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Downloads subtitles for movies"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install OpenSubtitles FlixTools Lite on MacOS using homebrew

- App Name: OpenSubtitles FlixTools Lite
- App description: Downloads subtitles for movies
- App Version: 3.1.1
- App Website: https://www.flixtools.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install OpenSubtitles FlixTools Lite with the following command
   ```
   brew install --cask flixtools
   ```
4. OpenSubtitles FlixTools Lite is ready to use now!