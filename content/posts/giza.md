---
title: "Install giza on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Scientific plotting library for C/Fortran built on cairo"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install giza on MacOS using homebrew

- App Name: giza
- App description: Scientific plotting library for C/Fortran built on cairo
- App Version: 1.3.1
- App Website: https://danieljprice.github.io/giza/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install giza with the following command
   ```
   brew install giza
   ```
4. giza is ready to use now!