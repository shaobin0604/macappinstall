---
title: "Install irssi on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Modular IRC client"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install irssi on MacOS using homebrew

- App Name: irssi
- App description: Modular IRC client
- App Version: 1.2.3
- App Website: https://irssi.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install irssi with the following command
   ```
   brew install irssi
   ```
4. irssi is ready to use now!