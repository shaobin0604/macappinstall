---
title: "Install stow on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Organize software neatly under a single directory tree (e.g. /usr/local)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install stow on MacOS using homebrew

- App Name: stow
- App description: Organize software neatly under a single directory tree (e.g. /usr/local)
- App Version: 2.3.1
- App Website: https://www.gnu.org/software/stow/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install stow with the following command
   ```
   brew install stow
   ```
4. stow is ready to use now!