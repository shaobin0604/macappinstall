---
title: "Install acpica on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "OS-independent implementation of the ACPI specification"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install acpica on MacOS using homebrew

- App Name: acpica
- App description: OS-independent implementation of the ACPI specification
- App Version: 20211217
- App Website: https://www.acpica.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install acpica with the following command
   ```
   brew install acpica
   ```
4. acpica is ready to use now!