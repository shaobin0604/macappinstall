---
title: "Install Upwork on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Work marketplace for freelancing"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Upwork on MacOS using homebrew

- App Name: Upwork
- App description: Work marketplace for freelancing
- App Version: 5.6.10.0,b124e6f3a4944b32
- App Website: https://www.upwork.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Upwork with the following command
   ```
   brew install --cask upwork
   ```
4. Upwork is ready to use now!