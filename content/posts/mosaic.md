---
title: "Install Mosaic on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Resize and reposition apps"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Mosaic on MacOS using homebrew

- App Name: Mosaic
- App description: Resize and reposition apps
- App Version: 1.3.3
- App Website: https://lightpillar.com/mosaic.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Mosaic with the following command
   ```
   brew install --cask mosaic
   ```
4. Mosaic is ready to use now!