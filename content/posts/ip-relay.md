---
title: "Install ip_relay on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "TCP traffic shaping relay application"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ip_relay on MacOS using homebrew

- App Name: ip_relay
- App description: TCP traffic shaping relay application
- App Version: 0.71
- App Website: http://www.stewart.com.au/ip_relay/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ip_relay with the following command
   ```
   brew install ip_relay
   ```
4. ip_relay is ready to use now!