---
title: "Install MacTeX on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Full TeX Live distribution without GUI applications"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install MacTeX on MacOS using homebrew

- App Name: MacTeX
- App description: Full TeX Live distribution without GUI applications
- App Version: 2021.0328
- App Website: https://www.tug.org/mactex/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install MacTeX with the following command
   ```
   brew install --cask mactex-no-gui
   ```
4. MacTeX is ready to use now!