---
title: "Install Boxer on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "DOS game emulator"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Boxer on MacOS using homebrew

- App Name: Boxer
- App description: DOS game emulator
- App Version: 1.4.0
- App Website: http://boxerapp.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Boxer with the following command
   ```
   brew install --cask boxer
   ```
4. Boxer is ready to use now!