---
title: "Install Vine Server on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "VNC server"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Vine Server on MacOS using homebrew

- App Name: Vine Server
- App description: VNC server
- App Version: 5.3.1
- App Website: https://github.com/stweil/OSXvnc/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Vine Server with the following command
   ```
   brew install --cask vine-server
   ```
4. Vine Server is ready to use now!