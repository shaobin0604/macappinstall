---
title: "Install Mixed In Key on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Harmonic mixing for DJs and music producers"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Mixed In Key on MacOS using homebrew

- App Name: Mixed In Key
- App description: Harmonic mixing for DJs and music producers
- App Version: 10.1.0.4487
- App Website: https://mixedinkey.com/get10/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Mixed In Key with the following command
   ```
   brew install --cask mixed-in-key
   ```
4. Mixed In Key is ready to use now!