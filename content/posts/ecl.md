---
title: "Install ecl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Embeddable Common Lisp"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ecl on MacOS using homebrew

- App Name: ecl
- App description: Embeddable Common Lisp
- App Version: 21.2.1
- App Website: https://common-lisp.net/project/ecl/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ecl with the following command
   ```
   brew install ecl
   ```
4. ecl is ready to use now!