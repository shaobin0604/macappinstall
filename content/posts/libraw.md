---
title: "Install libraw on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library for reading RAW files from digital photo cameras"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libraw on MacOS using homebrew

- App Name: libraw
- App description: Library for reading RAW files from digital photo cameras
- App Version: 0.20.2
- App Website: https://www.libraw.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libraw with the following command
   ```
   brew install libraw
   ```
4. libraw is ready to use now!