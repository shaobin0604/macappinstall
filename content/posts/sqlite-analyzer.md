---
title: "Install sqlite-analyzer on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Analyze how space is allocated inside an SQLite file"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sqlite-analyzer on MacOS using homebrew

- App Name: sqlite-analyzer
- App description: Analyze how space is allocated inside an SQLite file
- App Version: 3.37.2
- App Website: https://www.sqlite.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sqlite-analyzer with the following command
   ```
   brew install sqlite-analyzer
   ```
4. sqlite-analyzer is ready to use now!