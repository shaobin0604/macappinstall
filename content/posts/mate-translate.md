---
title: "Install Mate Translate on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Select text in any app and translate it"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Mate Translate on MacOS using homebrew

- App Name: Mate Translate
- App description: Select text in any app and translate it
- App Version: 8.1.2,3038
- App Website: https://twopeoplesoftware.com/mate

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Mate Translate with the following command
   ```
   brew install --cask mate-translate
   ```
4. Mate Translate is ready to use now!