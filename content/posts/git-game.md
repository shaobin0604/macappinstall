---
title: "Install git-game on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Game for git to guess who made which commit"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install git-game on MacOS using homebrew

- App Name: git-game
- App description: Game for git to guess who made which commit
- App Version: 1.2
- App Website: https://github.com/jsomers/git-game

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install git-game with the following command
   ```
   brew install git-game
   ```
4. git-game is ready to use now!