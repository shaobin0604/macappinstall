---
title: "Install Get Lyrical on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Automatically add lyrics to songs in iTunes"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Get Lyrical on MacOS using homebrew

- App Name: Get Lyrical
- App description: Automatically add lyrics to songs in iTunes
- App Version: 3.8.1
- App Website: https://shullian.com/get_lyrical.php

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Get Lyrical with the following command
   ```
   brew install --cask get-lyrical
   ```
4. Get Lyrical is ready to use now!