---
title: "Install Gemini on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Disk space cleaner that finds and deletes duplicated and similar files"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Gemini on MacOS using homebrew

- App Name: Gemini
- App description: Disk space cleaner that finds and deletes duplicated and similar files
- App Version: 2.9.2,389,1643877588
- App Website: https://macpaw.com/gemini

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Gemini with the following command
   ```
   brew install --cask gemini
   ```
4. Gemini is ready to use now!