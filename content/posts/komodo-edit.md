---
title: "Install Komodo Edit on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Text editor"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Komodo Edit on MacOS using homebrew

- App Name: Komodo Edit
- App description: Text editor
- App Version: 12.0.1,18441
- App Website: https://www.activestate.com/komodo-edit/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Komodo Edit with the following command
   ```
   brew install --cask komodo-edit
   ```
4. Komodo Edit is ready to use now!