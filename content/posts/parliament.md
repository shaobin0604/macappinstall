---
title: "Install parliament on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "AWS IAM linting library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install parliament on MacOS using homebrew

- App Name: parliament
- App description: AWS IAM linting library
- App Version: 1.5.2
- App Website: https://github.com/duo-labs/parliament

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install parliament with the following command
   ```
   brew install parliament
   ```
4. parliament is ready to use now!