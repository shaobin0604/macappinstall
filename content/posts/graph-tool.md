---
title: "Install graph-tool on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Efficient network analysis for Python 3"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install graph-tool on MacOS using homebrew

- App Name: graph-tool
- App description: Efficient network analysis for Python 3
- App Version: 2.44
- App Website: https://graph-tool.skewed.de/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install graph-tool with the following command
   ```
   brew install graph-tool
   ```
4. graph-tool is ready to use now!