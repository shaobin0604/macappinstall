---
title: "Install gl2ps on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "OpenGL to PostScript printing library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gl2ps on MacOS using homebrew

- App Name: gl2ps
- App description: OpenGL to PostScript printing library
- App Version: 1.4.2
- App Website: https://www.geuz.org/gl2ps/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gl2ps with the following command
   ```
   brew install gl2ps
   ```
4. gl2ps is ready to use now!