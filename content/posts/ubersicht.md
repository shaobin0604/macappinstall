---
title: "Install Übersicht on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Run commands and display their output on the desktop"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Übersicht on MacOS using homebrew

- App Name: Übersicht
- App description: Run commands and display their output on the desktop
- App Version: 1.6.69
- App Website: https://tracesof.net/uebersicht/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Übersicht with the following command
   ```
   brew install --cask ubersicht
   ```
4. Übersicht is ready to use now!