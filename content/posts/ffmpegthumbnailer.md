---
title: "Install ffmpegthumbnailer on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Create thumbnails for your video files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ffmpegthumbnailer on MacOS using homebrew

- App Name: ffmpegthumbnailer
- App description: Create thumbnails for your video files
- App Version: 2.2.2
- App Website: https://github.com/dirkvdb/ffmpegthumbnailer

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ffmpegthumbnailer with the following command
   ```
   brew install ffmpegthumbnailer
   ```
4. ffmpegthumbnailer is ready to use now!