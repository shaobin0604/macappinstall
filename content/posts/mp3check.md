---
title: "Install mp3check on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to check mp3 files for consistency"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mp3check on MacOS using homebrew

- App Name: mp3check
- App description: Tool to check mp3 files for consistency
- App Version: 0.8.7
- App Website: https://code.google.com/archive/p/mp3check/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mp3check with the following command
   ```
   brew install mp3check
   ```
4. mp3check is ready to use now!