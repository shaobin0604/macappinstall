---
title: "Install cloudformation-guard on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Checks CloudFormation templates for compliance using a declarative syntax"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cloudformation-guard on MacOS using homebrew

- App Name: cloudformation-guard
- App description: Checks CloudFormation templates for compliance using a declarative syntax
- App Version: 2.0.4
- App Website: https://github.com/aws-cloudformation/cloudformation-guard

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cloudformation-guard with the following command
   ```
   brew install cloudformation-guard
   ```
4. cloudformation-guard is ready to use now!