---
title: "Install Switch Audio Converter on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Multiple format audio file converter"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Switch Audio Converter on MacOS using homebrew

- App Name: Switch Audio Converter
- App description: Multiple format audio file converter
- App Version: 9.14
- App Website: https://www.nch.com.au/switch/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Switch Audio Converter with the following command
   ```
   brew install --cask switch
   ```
4. Switch Audio Converter is ready to use now!