---
title: "Install smpeg2 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "SDL MPEG Player Library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install smpeg2 on MacOS using homebrew

- App Name: smpeg2
- App description: SDL MPEG Player Library
- App Version: 2.0.0
- App Website: https://icculus.org/smpeg/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install smpeg2 with the following command
   ```
   brew install smpeg2
   ```
4. smpeg2 is ready to use now!