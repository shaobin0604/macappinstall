---
title: "Install iam-policy-json-to-terraform on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Convert a JSON IAM Policy into terraform"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install iam-policy-json-to-terraform on MacOS using homebrew

- App Name: iam-policy-json-to-terraform
- App description: Convert a JSON IAM Policy into terraform
- App Version: 1.8.0
- App Website: https://github.com/flosell/iam-policy-json-to-terraform

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install iam-policy-json-to-terraform with the following command
   ```
   brew install iam-policy-json-to-terraform
   ```
4. iam-policy-json-to-terraform is ready to use now!