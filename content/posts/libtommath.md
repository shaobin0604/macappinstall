---
title: "Install libtommath on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C library for number theoretic multiple-precision integers"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libtommath on MacOS using homebrew

- App Name: libtommath
- App description: C library for number theoretic multiple-precision integers
- App Version: 1.2.0
- App Website: https://www.libtom.net/LibTomMath/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libtommath with the following command
   ```
   brew install libtommath
   ```
4. libtommath is ready to use now!