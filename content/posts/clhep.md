---
title: "Install clhep on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Class Library for High Energy Physics"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install clhep on MacOS using homebrew

- App Name: clhep
- App description: Class Library for High Energy Physics
- App Version: 2.4.5.1
- App Website: https://proj-clhep.web.cern.ch/proj-clhep/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install clhep with the following command
   ```
   brew install clhep
   ```
4. clhep is ready to use now!