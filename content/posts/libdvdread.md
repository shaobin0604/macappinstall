---
title: "Install libdvdread on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C library for reading DVD-video images"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libdvdread on MacOS using homebrew

- App Name: libdvdread
- App description: C library for reading DVD-video images
- App Version: 6.1.2
- App Website: https://www.videolan.org/developers/libdvdnav.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libdvdread with the following command
   ```
   brew install libdvdread
   ```
4. libdvdread is ready to use now!