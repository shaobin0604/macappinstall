---
title: "Install Cacher on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Code snippet organizer"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Cacher on MacOS using homebrew

- App Name: Cacher
- App description: Code snippet organizer
- App Version: 2.43.1
- App Website: https://www.cacher.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Cacher with the following command
   ```
   brew install --cask cacher
   ```
4. Cacher is ready to use now!