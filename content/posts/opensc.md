---
title: "Install opensc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tools and libraries for smart cards"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install opensc on MacOS using homebrew

- App Name: opensc
- App description: Tools and libraries for smart cards
- App Version: 0.22.0
- App Website: https://github.com/OpenSC/OpenSC/wiki

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install opensc with the following command
   ```
   brew install opensc
   ```
4. opensc is ready to use now!