---
title: "Install MagicaVoxel on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "8-bit 3D voxel editor and interactive path tracing renderer"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install MagicaVoxel on MacOS using homebrew

- App Name: MagicaVoxel
- App description: 8-bit 3D voxel editor and interactive path tracing renderer
- App Version: 0.99.6.2
- App Website: https://ephtracy.github.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install MagicaVoxel with the following command
   ```
   brew install --cask magicavoxel
   ```
4. MagicaVoxel is ready to use now!