---
title: "Install You'll Never Take Me Alive! on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utility to enhance the protection of encrypted data"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install You'll Never Take Me Alive! on MacOS using homebrew

- App Name: You'll Never Take Me Alive!
- App description: Utility to enhance the protection of encrypted data
- App Version: 1.0.2
- App Website: https://github.com/iSECPartners/yontma-mac

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install You'll Never Take Me Alive! with the following command
   ```
   brew install --cask youll-never-take-me-alive
   ```
4. You'll Never Take Me Alive! is ready to use now!