---
title: "Install Otto Matic on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Science fiction 3D action/adventure game from Pangea Software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Otto Matic on MacOS using homebrew

- App Name: Otto Matic
- App description: Science fiction 3D action/adventure game from Pangea Software
- App Version: 4.0.0
- App Website: https://pangeasoft.net/otto/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Otto Matic with the following command
   ```
   brew install --cask otto-matic
   ```
4. Otto Matic is ready to use now!