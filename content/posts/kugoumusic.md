---
title: "Install Kugou Music on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Digital music service"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Kugou Music on MacOS using homebrew

- App Name: Kugou Music
- App description: Digital music service
- App Version: 3.0.4
- App Website: https://www.kugou.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Kugou Music with the following command
   ```
   brew install --cask kugoumusic
   ```
4. Kugou Music is ready to use now!