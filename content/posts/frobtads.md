---
title: "Install frobtads on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "TADS interpreter and compilers"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install frobtads on MacOS using homebrew

- App Name: frobtads
- App description: TADS interpreter and compilers
- App Version: 2.0
- App Website: https://www.tads.org/frobtads.htm

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install frobtads with the following command
   ```
   brew install frobtads
   ```
4. frobtads is ready to use now!