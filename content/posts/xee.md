---
title: "Install Xee³ on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Image viewer and file browser"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Xee³ on MacOS using homebrew

- App Name: Xee³
- App description: Image viewer and file browser
- App Version: 3.5.4,69,1640101604
- App Website: https://theunarchiver.com/xee

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Xee³ with the following command
   ```
   brew install --cask xee
   ```
4. Xee³ is ready to use now!