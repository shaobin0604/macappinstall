---
title: "Install Solar2D on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Lua-based game engine"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Solar2D on MacOS using homebrew

- App Name: Solar2D
- App description: Lua-based game engine
- App Version: 2021.3663
- App Website: https://solar2d.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Solar2D with the following command
   ```
   brew install --cask solar2d
   ```
4. Solar2D is ready to use now!