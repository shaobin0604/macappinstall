---
title: "Install qjson on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Map JSON to QVariant objects"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install qjson on MacOS using homebrew

- App Name: qjson
- App description: Map JSON to QVariant objects
- App Version: 0.9.0
- App Website: https://qjson.sourceforge.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install qjson with the following command
   ```
   brew install qjson
   ```
4. qjson is ready to use now!