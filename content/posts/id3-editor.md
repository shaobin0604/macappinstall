---
title: "Install ID3 Editor on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "MP3 and AIFF ID3 tag editor"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ID3 Editor on MacOS using homebrew

- App Name: ID3 Editor
- App description: MP3 and AIFF ID3 tag editor
- App Version: 1.28.50
- App Website: http://www.pa-software.com/id3editor/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ID3 Editor with the following command
   ```
   brew install --cask id3-editor
   ```
4. ID3 Editor is ready to use now!