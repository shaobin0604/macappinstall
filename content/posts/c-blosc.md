---
title: "Install c-blosc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Blocking, shuffling and loss-less compression library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install c-blosc on MacOS using homebrew

- App Name: c-blosc
- App description: Blocking, shuffling and loss-less compression library
- App Version: 1.21.1
- App Website: https://www.blosc.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install c-blosc with the following command
   ```
   brew install c-blosc
   ```
4. c-blosc is ready to use now!