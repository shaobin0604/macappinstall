---
title: "Install Sloth on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Displays all open files and sockets in use by all running processes"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Sloth on MacOS using homebrew

- App Name: Sloth
- App description: Displays all open files and sockets in use by all running processes
- App Version: 3.2,300
- App Website: https://sveinbjorn.org/sloth

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Sloth with the following command
   ```
   brew install --cask sloth
   ```
4. Sloth is ready to use now!