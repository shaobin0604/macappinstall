---
title: "Install Lock Rattler on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Checks security systems and reports issues"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Lock Rattler on MacOS using homebrew

- App Name: Lock Rattler
- App description: Checks security systems and reports issues
- App Version: 4.32,2022.01
- App Website: https://eclecticlight.co/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Lock Rattler with the following command
   ```
   brew install --cask lockrattler
   ```
4. Lock Rattler is ready to use now!