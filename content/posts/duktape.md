---
title: "Install duktape on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Embeddable Javascript engine with compact footprint"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install duktape on MacOS using homebrew

- App Name: duktape
- App description: Embeddable Javascript engine with compact footprint
- App Version: 2.6.0
- App Website: https://duktape.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install duktape with the following command
   ```
   brew install duktape
   ```
4. duktape is ready to use now!