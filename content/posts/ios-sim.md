---
title: "Install ios-sim on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line application launcher for the iOS Simulator"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ios-sim on MacOS using homebrew

- App Name: ios-sim
- App description: Command-line application launcher for the iOS Simulator
- App Version: 9.0.0
- App Website: https://github.com/ios-control/ios-sim

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ios-sim with the following command
   ```
   brew install ios-sim
   ```
4. ios-sim is ready to use now!