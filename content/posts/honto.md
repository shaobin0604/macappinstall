---
title: "Install honto view app on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Ebook reader for the honto store"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install honto view app on MacOS using homebrew

- App Name: honto view app
- App description: Ebook reader for the honto store
- App Version: 6.54.0,20211105
- App Website: https://honto.jp/ebook/dlinfo.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install honto view app with the following command
   ```
   brew install --cask honto
   ```
4. honto view app is ready to use now!