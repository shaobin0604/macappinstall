---
title: "Install pass-git-helper on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Git credential helper interfacing with pass"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pass-git-helper on MacOS using homebrew

- App Name: pass-git-helper
- App description: Git credential helper interfacing with pass
- App Version: 1.1.2
- App Website: https://github.com/languitar/pass-git-helper

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pass-git-helper with the following command
   ```
   brew install pass-git-helper
   ```
4. pass-git-helper is ready to use now!