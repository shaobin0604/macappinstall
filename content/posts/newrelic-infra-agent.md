---
title: "Install newrelic-infra-agent on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "New Relic infrastructure agent"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install newrelic-infra-agent on MacOS using homebrew

- App Name: newrelic-infra-agent
- App description: New Relic infrastructure agent
- App Version: 1.23.2
- App Website: https://github.com/newrelic/infrastructure-agent

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install newrelic-infra-agent with the following command
   ```
   brew install newrelic-infra-agent
   ```
4. newrelic-infra-agent is ready to use now!