---
title: "Install Soundtoys on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Audio Effects Plugins"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Soundtoys on MacOS using homebrew

- App Name: Soundtoys
- App description: Audio Effects Plugins
- App Version: 5.3.6.16152
- App Website: https://www.soundtoys.com/product/soundtoys/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Soundtoys with the following command
   ```
   brew install --cask soundtoys
   ```
4. Soundtoys is ready to use now!