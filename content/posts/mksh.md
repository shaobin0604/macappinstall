---
title: "Install mksh on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "MirBSD Korn Shell"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mksh on MacOS using homebrew

- App Name: mksh
- App description: MirBSD Korn Shell
- App Version: 59c
- App Website: https://www.mirbsd.org/mksh.htm

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mksh with the following command
   ```
   brew install mksh
   ```
4. mksh is ready to use now!