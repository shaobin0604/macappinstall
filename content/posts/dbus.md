---
title: "Install dbus on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Message bus system, providing inter-application communication"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dbus on MacOS using homebrew

- App Name: dbus
- App description: Message bus system, providing inter-application communication
- App Version: 1.12.20
- App Website: https://wiki.freedesktop.org/www/Software/dbus

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dbus with the following command
   ```
   brew install dbus
   ```
4. dbus is ready to use now!