---
title: "Install Bitwig Studio on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Digital audio workstation"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Bitwig Studio on MacOS using homebrew

- App Name: Bitwig Studio
- App description: Digital audio workstation
- App Version: 4.1.6
- App Website: https://www.bitwig.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Bitwig Studio with the following command
   ```
   brew install --cask bitwig-studio
   ```
4. Bitwig Studio is ready to use now!