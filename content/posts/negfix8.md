---
title: "Install negfix8 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Turn scanned negative images into positives"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install negfix8 on MacOS using homebrew

- App Name: negfix8
- App description: Turn scanned negative images into positives
- App Version: 8.3
- App Website: https://sites.google.com/site/negfix

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install negfix8 with the following command
   ```
   brew install negfix8
   ```
4. negfix8 is ready to use now!