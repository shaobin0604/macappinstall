---
title: "Install RoboForm on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Password manager and form filler application"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install RoboForm on MacOS using homebrew

- App Name: RoboForm
- App description: Password manager and form filler application
- App Version: 9.2.8
- App Website: https://www.roboform.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install RoboForm with the following command
   ```
   brew install --cask roboform
   ```
4. RoboForm is ready to use now!