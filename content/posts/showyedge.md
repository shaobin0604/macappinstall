---
title: "Install ShowyEdge on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Visible indicator of the current input source"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ShowyEdge on MacOS using homebrew

- App Name: ShowyEdge
- App description: Visible indicator of the current input source
- App Version: 5.4.0
- App Website: https://showyedge.pqrs.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ShowyEdge with the following command
   ```
   brew install --cask showyedge
   ```
4. ShowyEdge is ready to use now!