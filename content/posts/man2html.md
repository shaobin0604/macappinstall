---
title: "Install man2html on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Convert nroff man pages to HTML"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install man2html on MacOS using homebrew

- App Name: man2html
- App description: Convert nroff man pages to HTML
- App Version: 3.0.1
- App Website: https://savannah.nongnu.org/projects/man2html/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install man2html with the following command
   ```
   brew install man2html
   ```
4. man2html is ready to use now!