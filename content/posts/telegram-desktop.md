---
title: "Install Telegram Desktop on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Desktop client for Telegram messenger"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Telegram Desktop on MacOS using homebrew

- App Name: Telegram Desktop
- App description: Desktop client for Telegram messenger
- App Version: 3.5.1
- App Website: https://desktop.telegram.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Telegram Desktop with the following command
   ```
   brew install --cask telegram-desktop
   ```
4. Telegram Desktop is ready to use now!