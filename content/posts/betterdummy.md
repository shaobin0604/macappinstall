---
title: "Install betterdummy on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Dummy Display for Apple Silicon Macs to achieve custom resolutions"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install betterdummy on MacOS using homebrew

- App Name: betterdummy
- App description: Dummy Display for Apple Silicon Macs to achieve custom resolutions
- App Version: 1.0.13
- App Website: https://github.com/waydabber/BetterDummy

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install betterdummy with the following command
   ```
   brew install --cask betterdummy
   ```
4. betterdummy is ready to use now!