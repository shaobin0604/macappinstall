---
title: "Install gnome-common on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Core files for GNOME"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gnome-common on MacOS using homebrew

- App Name: gnome-common
- App description: Core files for GNOME
- App Version: 3.18.0
- App Website: https://gitlab.gnome.org/GNOME/gnome-common

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gnome-common with the following command
   ```
   brew install gnome-common
   ```
4. gnome-common is ready to use now!