---
title: "Install SSH Tunnel Manager on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install SSH Tunnel Manager on MacOS using homebrew

- App Name: SSH Tunnel Manager
- App description: null
- App Version: 227.006,1478083232
- App Website: https://tynsoe.org/stm/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install SSH Tunnel Manager with the following command
   ```
   brew install --cask ssh-tunnel-manager
   ```
4. SSH Tunnel Manager is ready to use now!