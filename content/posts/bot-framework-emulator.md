---
title: "Install Microsoft Bot Framework Emulator on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Test and debug chat bots built with the Bot Framework SDK"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Microsoft Bot Framework Emulator on MacOS using homebrew

- App Name: Microsoft Bot Framework Emulator
- App description: Test and debug chat bots built with the Bot Framework SDK
- App Version: 4.14.1
- App Website: https://github.com/Microsoft/BotFramework-Emulator

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Microsoft Bot Framework Emulator with the following command
   ```
   brew install --cask bot-framework-emulator
   ```
4. Microsoft Bot Framework Emulator is ready to use now!