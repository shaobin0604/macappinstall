---
title: "Install Breitbandmessung on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Official internet speed test from the German Bundesnetzagentur"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Breitbandmessung on MacOS using homebrew

- App Name: Breitbandmessung
- App description: Official internet speed test from the German Bundesnetzagentur
- App Version: 3.1.0
- App Website: https://www.breitbandmessung.de/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Breitbandmessung with the following command
   ```
   brew install --cask breitbandmessung
   ```
4. Breitbandmessung is ready to use now!