---
title: "Install babeld on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Loop-avoiding distance-vector routing protocol"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install babeld on MacOS using homebrew

- App Name: babeld
- App description: Loop-avoiding distance-vector routing protocol
- App Version: 1.10
- App Website: https://www.irif.fr/~jch/software/babel/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install babeld with the following command
   ```
   brew install babeld
   ```
4. babeld is ready to use now!