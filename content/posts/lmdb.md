---
title: "Install lmdb on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Lightning memory-mapped database: key-value data store"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install lmdb on MacOS using homebrew

- App Name: lmdb
- App description: Lightning memory-mapped database: key-value data store
- App Version: 0.9.29
- App Website: https://www.symas.com/symas-embedded-database-lmdb

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install lmdb with the following command
   ```
   brew install lmdb
   ```
4. lmdb is ready to use now!