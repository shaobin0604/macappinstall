---
title: "Install gdmap on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to inspect the used space of folders"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gdmap on MacOS using homebrew

- App Name: gdmap
- App description: Tool to inspect the used space of folders
- App Version: 0.8.1
- App Website: https://sourceforge.net/projects/gdmap/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gdmap with the following command
   ```
   brew install gdmap
   ```
4. gdmap is ready to use now!