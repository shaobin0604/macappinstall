---
title: "Install yq@3 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Process YAML documents from the CLI - Version 3"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install yq@3 on MacOS using homebrew

- App Name: yq@3
- App description: Process YAML documents from the CLI - Version 3
- App Version: 3.4.1
- App Website: https://github.com/mikefarah/yq

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install yq@3 with the following command
   ```
   brew install yq@3
   ```
4. yq@3 is ready to use now!