---
title: "Install TopTracker on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Time tracking and invoice processing"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install TopTracker on MacOS using homebrew

- App Name: TopTracker
- App description: Time tracking and invoice processing
- App Version: 1.6.2,6524
- App Website: https://tracker.toptal.com/tracker/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install TopTracker with the following command
   ```
   brew install --cask toptracker
   ```
4. TopTracker is ready to use now!