---
title: "Install Audio Hijack on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Records audio from any application"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Audio Hijack on MacOS using homebrew

- App Name: Audio Hijack
- App description: Records audio from any application
- App Version: 3.8.10
- App Website: https://www.rogueamoeba.com/audiohijack/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Audio Hijack with the following command
   ```
   brew install --cask audio-hijack
   ```
4. Audio Hijack is ready to use now!