---
title: "Install aspell on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Spell checker with better logic than ispell"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install aspell on MacOS using homebrew

- App Name: aspell
- App description: Spell checker with better logic than ispell
- App Version: 0.60.8
- App Website: http://aspell.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install aspell with the following command
   ```
   brew install aspell
   ```
4. aspell is ready to use now!