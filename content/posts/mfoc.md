---
title: "Install mfoc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Implementation of 'offline nested' attack by Nethemba"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mfoc on MacOS using homebrew

- App Name: mfoc
- App description: Implementation of 'offline nested' attack by Nethemba
- App Version: 0.10.7
- App Website: https://github.com/nfc-tools/mfoc

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mfoc with the following command
   ```
   brew install mfoc
   ```
4. mfoc is ready to use now!