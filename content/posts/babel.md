---
title: "Install babel on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Compiler for writing next generation JavaScript"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install babel on MacOS using homebrew

- App Name: babel
- App description: Compiler for writing next generation JavaScript
- App Version: 7.17.5
- App Website: https://babeljs.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install babel with the following command
   ```
   brew install babel
   ```
4. babel is ready to use now!