---
title: "Install stunnel on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "SSL tunneling program"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install stunnel on MacOS using homebrew

- App Name: stunnel
- App description: SSL tunneling program
- App Version: 5.62
- App Website: https://www.stunnel.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install stunnel with the following command
   ```
   brew install stunnel
   ```
4. stunnel is ready to use now!