---
title: "Install bpython on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Fancy interface to the Python interpreter"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bpython on MacOS using homebrew

- App Name: bpython
- App description: Fancy interface to the Python interpreter
- App Version: 0.22.1
- App Website: https://bpython-interpreter.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bpython with the following command
   ```
   brew install bpython
   ```
4. bpython is ready to use now!