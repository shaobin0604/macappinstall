---
title: "Install mvnvm on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Maven version manager"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mvnvm on MacOS using homebrew

- App Name: mvnvm
- App description: Maven version manager
- App Version: 1.0.16
- App Website: https://mvnvm.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mvnvm with the following command
   ```
   brew install mvnvm
   ```
4. mvnvm is ready to use now!