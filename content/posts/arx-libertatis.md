---
title: "Install arx-libertatis on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cross-platform, open source port of Arx Fatalis"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install arx-libertatis on MacOS using homebrew

- App Name: arx-libertatis
- App description: Cross-platform, open source port of Arx Fatalis
- App Version: 1.2
- App Website: https://arx-libertatis.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install arx-libertatis with the following command
   ```
   brew install arx-libertatis
   ```
4. arx-libertatis is ready to use now!