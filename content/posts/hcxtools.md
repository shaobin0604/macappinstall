---
title: "Install hcxtools on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utils for conversion of cap/pcap/pcapng WiFi dump files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install hcxtools on MacOS using homebrew

- App Name: hcxtools
- App description: Utils for conversion of cap/pcap/pcapng WiFi dump files
- App Version: 6.2.5
- App Website: https://github.com/ZerBea/hcxtools

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install hcxtools with the following command
   ```
   brew install hcxtools
   ```
4. hcxtools is ready to use now!