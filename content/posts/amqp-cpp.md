---
title: "Install amqp-cpp on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C++ library for communicating with a RabbitMQ message broker"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install amqp-cpp on MacOS using homebrew

- App Name: amqp-cpp
- App description: C++ library for communicating with a RabbitMQ message broker
- App Version: 4.3.16
- App Website: https://github.com/CopernicaMarketingSoftware/AMQP-CPP

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install amqp-cpp with the following command
   ```
   brew install amqp-cpp
   ```
4. amqp-cpp is ready to use now!