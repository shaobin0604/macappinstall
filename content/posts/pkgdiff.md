---
title: "Install pkgdiff on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool for analyzing changes in software packages (e.g. RPM, DEB, TAR.GZ)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pkgdiff on MacOS using homebrew

- App Name: pkgdiff
- App description: Tool for analyzing changes in software packages (e.g. RPM, DEB, TAR.GZ)
- App Version: 1.7.2
- App Website: https://lvc.github.io/pkgdiff/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pkgdiff with the following command
   ```
   brew install pkgdiff
   ```
4. pkgdiff is ready to use now!