---
title: "Install subliminal on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library to search and download subtitles"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install subliminal on MacOS using homebrew

- App Name: subliminal
- App description: Library to search and download subtitles
- App Version: 2.1.0
- App Website: https://subliminal.readthedocs.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install subliminal with the following command
   ```
   brew install subliminal
   ```
4. subliminal is ready to use now!