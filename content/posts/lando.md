---
title: "Install Lando on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Local development environment and DevOps tool built on Docker"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Lando on MacOS using homebrew

- App Name: Lando
- App description: Local development environment and DevOps tool built on Docker
- App Version: 3.6.0
- App Website: https://lando.dev/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Lando with the following command
   ```
   brew install --cask lando
   ```
4. Lando is ready to use now!