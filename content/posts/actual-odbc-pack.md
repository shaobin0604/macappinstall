---
title: "Install Actual ODBC Driver Pack on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Actual ODBC Driver Pack on MacOS using homebrew

- App Name: Actual ODBC Driver Pack
- App description: null
- App Version: latest
- App Website: https://www.actualtech.com/products.php

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Actual ODBC Driver Pack with the following command
   ```
   brew install --cask actual-odbc-pack
   ```
4. Actual ODBC Driver Pack is ready to use now!