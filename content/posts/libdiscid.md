---
title: "Install libdiscid on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C library for creating MusicBrainz and freedb disc IDs"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libdiscid on MacOS using homebrew

- App Name: libdiscid
- App description: C library for creating MusicBrainz and freedb disc IDs
- App Version: 0.6.2
- App Website: https://musicbrainz.org/doc/libdiscid

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libdiscid with the following command
   ```
   brew install libdiscid
   ```
4. libdiscid is ready to use now!