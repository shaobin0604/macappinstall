---
title: "Install Capto on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Screen capture/recorder and video editor"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Capto on MacOS using homebrew

- App Name: Capto
- App description: Screen capture/recorder and video editor
- App Version: 1.2.24,1001.2.24005
- App Website: https://www.globaldelight.com/capto/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Capto with the following command
   ```
   brew install --cask capto
   ```
4. Capto is ready to use now!