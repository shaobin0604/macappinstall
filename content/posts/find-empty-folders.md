---
title: "Install Find Empty Folders on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Finds empty folders"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Find Empty Folders on MacOS using homebrew

- App Name: Find Empty Folders
- App description: Finds empty folders
- App Version: 1.3
- App Website: https://www.tempel.org/FindEmptyFolders

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Find Empty Folders with the following command
   ```
   brew install --cask find-empty-folders
   ```
4. Find Empty Folders is ready to use now!