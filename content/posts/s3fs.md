---
title: "Install s3fs on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "FUSE-based file system backed by Amazon S3"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install s3fs on MacOS using homebrew

- App Name: s3fs
- App description: FUSE-based file system backed by Amazon S3
- App Version: 1.87
- App Website: https://github.com/s3fs-fuse/s3fs-fuse/wiki

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install s3fs with the following command
   ```
   brew install s3fs
   ```
4. s3fs is ready to use now!