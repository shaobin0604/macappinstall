---
title: "Install Betelguese on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Odysseyra1n installer GUI for jailbroken devices"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Betelguese on MacOS using homebrew

- App Name: Betelguese
- App description: Odysseyra1n installer GUI for jailbroken devices
- App Version: 1.1
- App Website: https://github.com/23Aaron/Betelguese

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Betelguese with the following command
   ```
   brew install --cask betelguese
   ```
4. Betelguese is ready to use now!