---
title: "Install id3v2 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line editor"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install id3v2 on MacOS using homebrew

- App Name: id3v2
- App description: Command-line editor
- App Version: 0.1.12
- App Website: https://id3v2.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install id3v2 with the following command
   ```
   brew install id3v2
   ```
4. id3v2 is ready to use now!