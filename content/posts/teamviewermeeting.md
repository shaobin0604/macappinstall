---
title: "Install TeamViewerMeeting on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Videoconferencing and communication software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install TeamViewerMeeting on MacOS using homebrew

- App Name: TeamViewerMeeting
- App description: Videoconferencing and communication software
- App Version: 15.24.2
- App Website: https://www.teamviewer.com/meeting/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install TeamViewerMeeting with the following command
   ```
   brew install --cask teamviewermeeting
   ```
4. TeamViewerMeeting is ready to use now!