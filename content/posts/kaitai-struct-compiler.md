---
title: "Install kaitai-struct-compiler on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Compiler for generating binary data parsers"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install kaitai-struct-compiler on MacOS using homebrew

- App Name: kaitai-struct-compiler
- App description: Compiler for generating binary data parsers
- App Version: 0.9
- App Website: https://kaitai.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install kaitai-struct-compiler with the following command
   ```
   brew install kaitai-struct-compiler
   ```
4. kaitai-struct-compiler is ready to use now!