---
title: "Install OpenPHT on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Community-driven fork of Plex Home Theater"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install OpenPHT on MacOS using homebrew

- App Name: OpenPHT
- App description: Community-driven fork of Plex Home Theater
- App Version: 1.8.0.148-573b6d73
- App Website: https://github.com/RasPlex/OpenPHT

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install OpenPHT with the following command
   ```
   brew install --cask openpht
   ```
4. OpenPHT is ready to use now!