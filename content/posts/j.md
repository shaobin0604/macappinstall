---
title: "Install J on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Programming language for mathematical, statistical and logical analysis of data"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install J on MacOS using homebrew

- App Name: J
- App description: Programming language for mathematical, statistical and logical analysis of data
- App Version: 903
- App Website: https://www.jsoftware.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install J with the following command
   ```
   brew install --cask j
   ```
4. J is ready to use now!