---
title: "Install Rancher Desktop on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Kubernetes and container management on the desktop"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Rancher Desktop on MacOS using homebrew

- App Name: Rancher Desktop
- App description: Kubernetes and container management on the desktop
- App Version: 1.0.1
- App Website: https://rancherdesktop.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Rancher Desktop with the following command
   ```
   brew install --cask rancher
   ```
4. Rancher Desktop is ready to use now!