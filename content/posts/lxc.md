---
title: "Install lxc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CLI client for interacting with LXD"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install lxc on MacOS using homebrew

- App Name: lxc
- App description: CLI client for interacting with LXD
- App Version: 4.23
- App Website: https://linuxcontainers.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install lxc with the following command
   ```
   brew install lxc
   ```
4. lxc is ready to use now!