---
title: "Install gitwatch on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Watch a file or folder and automatically commit changes to a git repo easily"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gitwatch on MacOS using homebrew

- App Name: gitwatch
- App description: Watch a file or folder and automatically commit changes to a git repo easily
- App Version: 0.1
- App Website: https://github.com/gitwatch/gitwatch

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gitwatch with the following command
   ```
   brew install gitwatch
   ```
4. gitwatch is ready to use now!