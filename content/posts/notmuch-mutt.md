---
title: "Install notmuch-mutt on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Notmuch integration for Mutt"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install notmuch-mutt on MacOS using homebrew

- App Name: notmuch-mutt
- App description: Notmuch integration for Mutt
- App Version: 0.35
- App Website: https://notmuchmail.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install notmuch-mutt with the following command
   ```
   brew install notmuch-mutt
   ```
4. notmuch-mutt is ready to use now!