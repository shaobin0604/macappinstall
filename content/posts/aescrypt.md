---
title: "Install aescrypt on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Program for encryption/decryption"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install aescrypt on MacOS using homebrew

- App Name: aescrypt
- App description: Program for encryption/decryption
- App Version: 0.7
- App Website: https://aescrypt.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install aescrypt with the following command
   ```
   brew install aescrypt
   ```
4. aescrypt is ready to use now!