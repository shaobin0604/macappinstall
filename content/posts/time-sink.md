---
title: "Install Time Sink on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tracks how you spend your time on your computer"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Time Sink on MacOS using homebrew

- App Name: Time Sink
- App description: Tracks how you spend your time on your computer
- App Version: 2.2.3
- App Website: https://manytricks.com/timesink/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Time Sink with the following command
   ```
   brew install --cask time-sink
   ```
4. Time Sink is ready to use now!