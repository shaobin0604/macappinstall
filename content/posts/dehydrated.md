---
title: "Install dehydrated on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "LetsEncrypt/acme client implemented as a shell-script"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dehydrated on MacOS using homebrew

- App Name: dehydrated
- App description: LetsEncrypt/acme client implemented as a shell-script
- App Version: 0.7.0
- App Website: https://dehydrated.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dehydrated with the following command
   ```
   brew install dehydrated
   ```
4. dehydrated is ready to use now!