---
title: "Install DingTalk on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Teamwork app by Alibaba Group"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install DingTalk on MacOS using homebrew

- App Name: DingTalk
- App description: Teamwork app by Alibaba Group
- App Version: 6.3.25.9
- App Website: https://www.dingtalk.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install DingTalk with the following command
   ```
   brew install --cask dingtalk
   ```
4. DingTalk is ready to use now!