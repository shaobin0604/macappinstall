---
title: "Install Pragli on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Virtual office for remote teams"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Pragli on MacOS using homebrew

- App Name: Pragli
- App description: Virtual office for remote teams
- App Version: 21.9.2
- App Website: https://pragli.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Pragli with the following command
   ```
   brew install --cask pragli
   ```
4. Pragli is ready to use now!