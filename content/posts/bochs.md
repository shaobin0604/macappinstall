---
title: "Install bochs on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Open source IA-32 (x86) PC emulator written in C++"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bochs on MacOS using homebrew

- App Name: bochs
- App description: Open source IA-32 (x86) PC emulator written in C++
- App Version: 2.7
- App Website: https://bochs.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bochs with the following command
   ```
   brew install bochs
   ```
4. bochs is ready to use now!