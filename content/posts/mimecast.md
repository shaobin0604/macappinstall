---
title: "Install Mimecast for Mac on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Mimecast for Mac on MacOS using homebrew

- App Name: Mimecast for Mac
- App description: null
- App Version: 2.11
- App Website: https://community.mimecast.com/community/knowledge-base/mimecast-for-mac

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Mimecast for Mac with the following command
   ```
   brew install --cask mimecast
   ```
4. Mimecast for Mac is ready to use now!