---
title: "Install cgrep on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Context-aware grep for source code"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cgrep on MacOS using homebrew

- App Name: cgrep
- App description: Context-aware grep for source code
- App Version: 6.7.1
- App Website: https://github.com/awgn/cgrep

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cgrep with the following command
   ```
   brew install cgrep
   ```
4. cgrep is ready to use now!