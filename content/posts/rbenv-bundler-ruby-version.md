---
title: "Install rbenv-bundler-ruby-version on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Pick a ruby version from bundler's Gemfile"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install rbenv-bundler-ruby-version on MacOS using homebrew

- App Name: rbenv-bundler-ruby-version
- App description: Pick a ruby version from bundler's Gemfile
- App Version: 1.0.0
- App Website: https://github.com/aripollak/rbenv-bundler-ruby-version

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install rbenv-bundler-ruby-version with the following command
   ```
   brew install rbenv-bundler-ruby-version
   ```
4. rbenv-bundler-ruby-version is ready to use now!