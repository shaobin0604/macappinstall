---
title: "Install advancemame on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "MAME with advanced video support"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install advancemame on MacOS using homebrew

- App Name: advancemame
- App description: MAME with advanced video support
- App Version: 3.9
- App Website: https://www.advancemame.it/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install advancemame with the following command
   ```
   brew install advancemame
   ```
4. advancemame is ready to use now!