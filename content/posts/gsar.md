---
title: "Install gsar on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "General Search And Replace on files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gsar on MacOS using homebrew

- App Name: gsar
- App description: General Search And Replace on files
- App Version: 1.51
- App Website: http://tjaberg.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gsar with the following command
   ```
   brew install gsar
   ```
4. gsar is ready to use now!