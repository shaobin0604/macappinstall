---
title: "Install fio on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "I/O benchmark and stress test"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fio on MacOS using homebrew

- App Name: fio
- App description: I/O benchmark and stress test
- App Version: 3.29
- App Website: https://github.com/axboe/fio

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fio with the following command
   ```
   brew install fio
   ```
4. fio is ready to use now!