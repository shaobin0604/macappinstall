---
title: "Install PDFpenPro on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "PDF editing software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install PDFpenPro on MacOS using homebrew

- App Name: PDFpenPro
- App description: PDF editing software
- App Version: 12.2.3,1223.1
- App Website: https://smilesoftware.com/PDFpenPro

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install PDFpenPro with the following command
   ```
   brew install --cask pdfpenpro
   ```
4. PDFpenPro is ready to use now!