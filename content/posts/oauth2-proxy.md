---
title: "Install oauth2_proxy on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Reverse proxy for authenticating users via OAuth 2 providers"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install oauth2_proxy on MacOS using homebrew

- App Name: oauth2_proxy
- App description: Reverse proxy for authenticating users via OAuth 2 providers
- App Version: 7.2.1
- App Website: https://oauth2-proxy.github.io/oauth2-proxy/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install oauth2_proxy with the following command
   ```
   brew install oauth2_proxy
   ```
4. oauth2_proxy is ready to use now!