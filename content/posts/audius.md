---
title: "Install Audius on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Music streaming and sharing platform"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Audius on MacOS using homebrew

- App Name: Audius
- App description: Music streaming and sharing platform
- App Version: 0.24.39
- App Website: https://audius.co/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Audius with the following command
   ```
   brew install --cask audius
   ```
4. Audius is ready to use now!