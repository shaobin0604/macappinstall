---
title: "Install libmp3splt on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utility library to split mp3, ogg, and FLAC files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libmp3splt on MacOS using homebrew

- App Name: libmp3splt
- App description: Utility library to split mp3, ogg, and FLAC files
- App Version: 0.9.2
- App Website: https://mp3splt.sourceforge.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libmp3splt with the following command
   ```
   brew install libmp3splt
   ```
4. libmp3splt is ready to use now!