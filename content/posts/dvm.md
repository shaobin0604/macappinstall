---
title: "Install dvm on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Docker Version Manager"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dvm on MacOS using homebrew

- App Name: dvm
- App description: Docker Version Manager
- App Version: 1.0.2
- App Website: https://github.com/howtowhale/dvm

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dvm with the following command
   ```
   brew install dvm
   ```
4. dvm is ready to use now!