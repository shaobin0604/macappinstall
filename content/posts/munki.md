---
title: "Install Munki on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Software installation manager"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Munki on MacOS using homebrew

- App Name: Munki
- App description: Software installation manager
- App Version: 5.6.3.4401
- App Website: https://www.munki.org/munki/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Munki with the following command
   ```
   brew install --cask munki
   ```
4. Munki is ready to use now!