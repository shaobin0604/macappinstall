---
title: "Install cassandra@2.2 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Eventually consistent, distributed key-value db"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cassandra@2.2 on MacOS using homebrew

- App Name: cassandra@2.2
- App description: Eventually consistent, distributed key-value db
- App Version: 2.2.19
- App Website: https://cassandra.apache.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cassandra@2.2 with the following command
   ```
   brew install cassandra@2.2
   ```
4. cassandra@2.2 is ready to use now!