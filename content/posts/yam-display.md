---
title: "Install Yam Display on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Yet another monitor"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Yam Display on MacOS using homebrew

- App Name: Yam Display
- App description: Yet another monitor
- App Version: 2.2.8
- App Website: https://www.yamdisplay.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Yam Display with the following command
   ```
   brew install --cask yam-display
   ```
4. Yam Display is ready to use now!