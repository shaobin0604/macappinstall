---
title: "Install gitui on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Blazing fast terminal-ui for git written in rust"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gitui on MacOS using homebrew

- App Name: gitui
- App description: Blazing fast terminal-ui for git written in rust
- App Version: 0.20.1
- App Website: https://github.com/extrawurst/gitui

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gitui with the following command
   ```
   brew install gitui
   ```
4. gitui is ready to use now!