---
title: "Install snap-telemetry on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Snap is an opensource telemetry framework"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install snap-telemetry on MacOS using homebrew

- App Name: snap-telemetry
- App description: Snap is an opensource telemetry framework
- App Version: 2.0.0
- App Website: https://snap-telemetry.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install snap-telemetry with the following command
   ```
   brew install snap-telemetry
   ```
4. snap-telemetry is ready to use now!