---
title: "Install derby on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Apache Derby is an embedded relational database running on JVM"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install derby on MacOS using homebrew

- App Name: derby
- App description: Apache Derby is an embedded relational database running on JVM
- App Version: 10.15.2.0
- App Website: https://db.apache.org/derby/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install derby with the following command
   ```
   brew install derby
   ```
4. derby is ready to use now!