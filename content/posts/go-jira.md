---
title: "Install go-jira on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Simple jira command-line client in Go"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install go-jira on MacOS using homebrew

- App Name: go-jira
- App description: Simple jira command-line client in Go
- App Version: 1.0.27
- App Website: https://github.com/go-jira/jira

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install go-jira with the following command
   ```
   brew install go-jira
   ```
4. go-jira is ready to use now!