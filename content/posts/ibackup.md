---
title: "Install iBackup on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Backup utility"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install iBackup on MacOS using homebrew

- App Name: iBackup
- App description: Backup utility
- App Version: 7.6
- App Website: http://www.grapefruit.ch/iBackup/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install iBackup with the following command
   ```
   brew install --cask ibackup
   ```
4. iBackup is ready to use now!