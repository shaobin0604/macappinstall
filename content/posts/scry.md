---
title: "Install scry on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Code analysis server for Crystal programming language"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install scry on MacOS using homebrew

- App Name: scry
- App description: Code analysis server for Crystal programming language
- App Version: 0.9.1
- App Website: https://github.com/crystal-lang-tools/scry/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install scry with the following command
   ```
   brew install scry
   ```
4. scry is ready to use now!