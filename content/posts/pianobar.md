---
title: "Install pianobar on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line player for https://pandora.com"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pianobar on MacOS using homebrew

- App Name: pianobar
- App description: Command-line player for https://pandora.com
- App Version: 2020.11.28
- App Website: https://github.com/PromyLOPh/pianobar/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pianobar with the following command
   ```
   brew install pianobar
   ```
4. pianobar is ready to use now!