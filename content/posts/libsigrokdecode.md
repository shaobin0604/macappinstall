---
title: "Install libsigrokdecode on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Drivers for logic analyzers and other supported devices"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libsigrokdecode on MacOS using homebrew

- App Name: libsigrokdecode
- App description: Drivers for logic analyzers and other supported devices
- App Version: 0.5.3
- App Website: https://sigrok.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libsigrokdecode with the following command
   ```
   brew install libsigrokdecode
   ```
4. libsigrokdecode is ready to use now!