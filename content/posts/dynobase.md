---
title: "Install Dynobase on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GUI Client for DynamoDB"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Dynobase on MacOS using homebrew

- App Name: Dynobase
- App description: GUI Client for DynamoDB
- App Version: 1.8.2
- App Website: https://dynobase.dev/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Dynobase with the following command
   ```
   brew install --cask dynobase
   ```
4. Dynobase is ready to use now!