---
title: "Install Freenet on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Menu bar application to control Freenet"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Freenet on MacOS using homebrew

- App Name: Freenet
- App description: Menu bar application to control Freenet
- App Version: 2.2.0
- App Website: https://freenetproject.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Freenet with the following command
   ```
   brew install --cask freenettray
   ```
4. Freenet is ready to use now!