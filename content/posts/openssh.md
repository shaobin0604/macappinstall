---
title: "Install openssh on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "OpenBSD freely-licensed SSH connectivity tools"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install openssh on MacOS using homebrew

- App Name: openssh
- App description: OpenBSD freely-licensed SSH connectivity tools
- App Version: 8.8p1
- App Website: https://www.openssh.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install openssh with the following command
   ```
   brew install openssh
   ```
4. openssh is ready to use now!