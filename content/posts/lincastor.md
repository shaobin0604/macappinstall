---
title: "Install LinCastor on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Choose a web browser when opening a link"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install LinCastor on MacOS using homebrew

- App Name: LinCastor
- App description: Choose a web browser when opening a link
- App Version: 4.0
- App Website: https://onflapp.github.io/blog/pages/LinCastorBrowser.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install LinCastor with the following command
   ```
   brew install --cask lincastor
   ```
4. LinCastor is ready to use now!