---
title: "Install twarc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line tool and Python library for archiving Twitter JSON"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install twarc on MacOS using homebrew

- App Name: twarc
- App description: Command-line tool and Python library for archiving Twitter JSON
- App Version: 2.9.2
- App Website: https://github.com/DocNow/twarc

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install twarc with the following command
   ```
   brew install twarc
   ```
4. twarc is ready to use now!