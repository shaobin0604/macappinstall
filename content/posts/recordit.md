---
title: "Install RecordIt on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Screencasting software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install RecordIt on MacOS using homebrew

- App Name: RecordIt
- App description: Screencasting software
- App Version: 1.6.10,114
- App Website: https://recordit.co/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install RecordIt with the following command
   ```
   brew install --cask recordit
   ```
4. RecordIt is ready to use now!