---
title: "Install sispmctl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Control Gembird SIS-PM programmable power outlet strips"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sispmctl on MacOS using homebrew

- App Name: sispmctl
- App description: Control Gembird SIS-PM programmable power outlet strips
- App Version: 4.9
- App Website: https://sispmctl.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sispmctl with the following command
   ```
   brew install sispmctl
   ```
4. sispmctl is ready to use now!