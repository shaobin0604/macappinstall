---
title: "Install Advanced REST Client on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "API testing tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Advanced REST Client on MacOS using homebrew

- App Name: Advanced REST Client
- App description: API testing tool
- App Version: 17.0.8
- App Website: https://github.com/advanced-rest-client/arc-electron

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Advanced REST Client with the following command
   ```
   brew install --cask advancedrestclient
   ```
4. Advanced REST Client is ready to use now!