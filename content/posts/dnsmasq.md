---
title: "Install dnsmasq on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Lightweight DNS forwarder and DHCP server"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dnsmasq on MacOS using homebrew

- App Name: dnsmasq
- App description: Lightweight DNS forwarder and DHCP server
- App Version: 2.86
- App Website: https://thekelleys.org.uk/dnsmasq/doc.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dnsmasq with the following command
   ```
   brew install dnsmasq
   ```
4. dnsmasq is ready to use now!