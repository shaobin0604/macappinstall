---
title: "Install MIDITrail on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "MIDI player which provides 3D visualization of MIDI data sets"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install MIDITrail on MacOS using homebrew

- App Name: MIDITrail
- App description: MIDI player which provides 3D visualization of MIDI data sets
- App Version: 1.3.5,76730
- App Website: https://osdn.net/projects/miditrail/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install MIDITrail with the following command
   ```
   brew install --cask miditrail
   ```
4. MIDITrail is ready to use now!