---
title: "Install cromwell on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Workflow Execution Engine using Workflow Description Language"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cromwell on MacOS using homebrew

- App Name: cromwell
- App description: Workflow Execution Engine using Workflow Description Language
- App Version: 76
- App Website: https://github.com/broadinstitute/cromwell

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cromwell with the following command
   ```
   brew install cromwell
   ```
4. cromwell is ready to use now!