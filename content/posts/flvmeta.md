---
title: "Install flvmeta on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Manipulate Adobe flash video files (FLV)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install flvmeta on MacOS using homebrew

- App Name: flvmeta
- App description: Manipulate Adobe flash video files (FLV)
- App Version: 1.2.2
- App Website: https://www.flvmeta.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install flvmeta with the following command
   ```
   brew install flvmeta
   ```
4. flvmeta is ready to use now!