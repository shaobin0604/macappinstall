---
title: "Install minio-mc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Replacement for ls, cp and other commands for object storage"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install minio-mc on MacOS using homebrew

- App Name: minio-mc
- App description: Replacement for ls, cp and other commands for object storage
- App Version: 20220216055401
- App Website: https://github.com/minio/mc

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install minio-mc with the following command
   ```
   brew install minio-mc
   ```
4. minio-mc is ready to use now!