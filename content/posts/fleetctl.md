---
title: "Install fleetctl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Distributed init system"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fleetctl on MacOS using homebrew

- App Name: fleetctl
- App description: Distributed init system
- App Version: 1.0.0
- App Website: https://github.com/coreos/fleet

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fleetctl with the following command
   ```
   brew install fleetctl
   ```
4. fleetctl is ready to use now!