---
title: "Install xScope on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tools for measuring, inspecting & testing on-screen graphics and layouts"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install xScope on MacOS using homebrew

- App Name: xScope
- App description: Tools for measuring, inspecting & testing on-screen graphics and layouts
- App Version: 4.5.1,108
- App Website: https://xscopeapp.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install xScope with the following command
   ```
   brew install --cask xscope
   ```
4. xScope is ready to use now!