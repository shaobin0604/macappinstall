---
title: "Install Clover on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Notes, whiteboarding, todos, and a daily planner in one tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Clover on MacOS using homebrew

- App Name: Clover
- App description: Notes, whiteboarding, todos, and a daily planner in one tool
- App Version: 1.1.0
- App Website: https://cloverapp.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Clover with the following command
   ```
   brew install --cask clover
   ```
4. Clover is ready to use now!