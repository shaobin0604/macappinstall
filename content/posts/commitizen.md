---
title: "Install commitizen on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Defines a standard way of committing rules and communicating it"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install commitizen on MacOS using homebrew

- App Name: commitizen
- App description: Defines a standard way of committing rules and communicating it
- App Version: 2.21.0
- App Website: https://commitizen-tools.github.io/commitizen/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install commitizen with the following command
   ```
   brew install commitizen
   ```
4. commitizen is ready to use now!