---
title: "Install jerryscript on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Ultra-lightweight JavaScript engine for the Internet of Things"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install jerryscript on MacOS using homebrew

- App Name: jerryscript
- App description: Ultra-lightweight JavaScript engine for the Internet of Things
- App Version: 2.4.0
- App Website: https://jerryscript.net

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install jerryscript with the following command
   ```
   brew install jerryscript
   ```
4. jerryscript is ready to use now!