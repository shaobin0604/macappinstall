---
title: "Install Brisk on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "App for submitting radars"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Brisk on MacOS using homebrew

- App Name: Brisk
- App description: App for submitting radars
- App Version: 1.2.0
- App Website: https://github.com/br1sk/brisk

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Brisk with the following command
   ```
   brew install --cask brisk
   ```
4. Brisk is ready to use now!