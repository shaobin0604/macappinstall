---
title: "Install ClickUp on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Productivity platform for tasks, docs, goals, and chat"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ClickUp on MacOS using homebrew

- App Name: ClickUp
- App description: Productivity platform for tasks, docs, goals, and chat
- App Version: 3.0.5
- App Website: https://clickup.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ClickUp with the following command
   ```
   brew install --cask clickup
   ```
4. ClickUp is ready to use now!