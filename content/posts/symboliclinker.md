---
title: "Install SymbolicLinker on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Service that allows users to make symbolic links in the Finder"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install SymbolicLinker on MacOS using homebrew

- App Name: SymbolicLinker
- App description: Service that allows users to make symbolic links in the Finder
- App Version: 2.2
- App Website: https://github.com/nickzman/symboliclinker

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install SymbolicLinker with the following command
   ```
   brew install --cask symboliclinker
   ```
4. SymbolicLinker is ready to use now!