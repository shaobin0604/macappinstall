---
title: "Install gofabric8 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CLI for fabric8 running on Kubernetes or OpenShift"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gofabric8 on MacOS using homebrew

- App Name: gofabric8
- App description: CLI for fabric8 running on Kubernetes or OpenShift
- App Version: 0.4.176
- App Website: https://github.com/fabric8io/gofabric8/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gofabric8 with the following command
   ```
   brew install gofabric8
   ```
4. gofabric8 is ready to use now!