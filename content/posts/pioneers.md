---
title: "Install pioneers on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Settlers of Catan clone"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pioneers on MacOS using homebrew

- App Name: pioneers
- App description: Settlers of Catan clone
- App Version: 15.6
- App Website: https://pio.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pioneers with the following command
   ```
   brew install pioneers
   ```
4. pioneers is ready to use now!