---
title: "Install atk on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GNOME accessibility toolkit"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install atk on MacOS using homebrew

- App Name: atk
- App description: GNOME accessibility toolkit
- App Version: 2.36.0
- App Website: https://library.gnome.org/devel/atk/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install atk with the following command
   ```
   brew install atk
   ```
4. atk is ready to use now!