---
title: "Install unp on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Unpack everything with one command"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install unp on MacOS using homebrew

- App Name: unp
- App description: Unpack everything with one command
- App Version: 2.0-pre7-nmu1
- App Website: https://packages.debian.org/source/stable/unp

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install unp with the following command
   ```
   brew install unp
   ```
4. unp is ready to use now!