---
title: "Install re:AMP on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "WinAMP clone written in SwiftUI"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install re:AMP on MacOS using homebrew

- App Name: re:AMP
- App description: WinAMP clone written in SwiftUI
- App Version: 1.6.3,511
- App Website: https://re-amp.ru/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install re:AMP with the following command
   ```
   brew install --cask reamp
   ```
4. re:AMP is ready to use now!