---
title: "Install spectra on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Header-only C++ library for large scale eigenvalue problems"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install spectra on MacOS using homebrew

- App Name: spectra
- App description: Header-only C++ library for large scale eigenvalue problems
- App Version: 1.0.0
- App Website: https://spectralib.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install spectra with the following command
   ```
   brew install spectra
   ```
4. spectra is ready to use now!