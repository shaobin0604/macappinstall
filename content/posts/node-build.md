---
title: "Install node-build on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Install NodeJS versions"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install node-build on MacOS using homebrew

- App Name: node-build
- App description: Install NodeJS versions
- App Version: 4.9.69
- App Website: https://github.com/nodenv/node-build

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install node-build with the following command
   ```
   brew install node-build
   ```
4. node-build is ready to use now!