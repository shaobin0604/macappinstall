---
title: "Install emacs-clang-complete-async on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Emacs plugin using libclang to complete C/C++ code"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install emacs-clang-complete-async on MacOS using homebrew

- App Name: emacs-clang-complete-async
- App description: Emacs plugin using libclang to complete C/C++ code
- App Version: 0.5
- App Website: https://github.com/Golevka/emacs-clang-complete-async

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install emacs-clang-complete-async with the following command
   ```
   brew install emacs-clang-complete-async
   ```
4. emacs-clang-complete-async is ready to use now!