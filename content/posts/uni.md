---
title: "Install uni on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Unicode database query tool for the command-line"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install uni on MacOS using homebrew

- App Name: uni
- App description: Unicode database query tool for the command-line
- App Version: 2.4.0
- App Website: https://github.com/arp242/uni

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install uni with the following command
   ```
   brew install uni
   ```
4. uni is ready to use now!