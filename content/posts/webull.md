---
title: "Install Webull on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Desktop client for Webull Financial LLC"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Webull on MacOS using homebrew

- App Name: Webull
- App description: Desktop client for Webull Financial LLC
- App Version: 5.6.6
- App Website: https://webull.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Webull with the following command
   ```
   brew install --cask webull
   ```
4. Webull is ready to use now!