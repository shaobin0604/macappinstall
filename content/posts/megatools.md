---
title: "Install megatools on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line client for Mega.co.nz"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install megatools on MacOS using homebrew

- App Name: megatools
- App description: Command-line client for Mega.co.nz
- App Version: 1.10.3
- App Website: https://megatools.megous.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install megatools with the following command
   ```
   brew install megatools
   ```
4. megatools is ready to use now!