---
title: "Install EV3 Classroom on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Companion app for the LEGO MINDSTORMS Education EV3 Core Set"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install EV3 Classroom on MacOS using homebrew

- App Name: EV3 Classroom
- App description: Companion app for the LEGO MINDSTORMS Education EV3 Core Set
- App Version: 1.50
- App Website: https://education.lego.com/en-us/downloads/mindstorms-ev3/software

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install EV3 Classroom with the following command
   ```
   brew install --cask ev3-classroom
   ```
4. EV3 Classroom is ready to use now!