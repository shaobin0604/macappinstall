---
title: "Install SuperSlicer on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Convert 3D models into G-code instructions or PNG layers"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install SuperSlicer on MacOS using homebrew

- App Name: SuperSlicer
- App description: Convert 3D models into G-code instructions or PNG layers
- App Version: 2.3.57.11,220213
- App Website: https://github.com/supermerill/SuperSlicer

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install SuperSlicer with the following command
   ```
   brew install --cask superslicer
   ```
4. SuperSlicer is ready to use now!