---
title: "Install libpano on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Build panoramic images from a set of overlapping images"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libpano on MacOS using homebrew

- App Name: libpano
- App description: Build panoramic images from a set of overlapping images
- App Version: 13-2.9.21
- App Website: https://panotools.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libpano with the following command
   ```
   brew install libpano
   ```
4. libpano is ready to use now!