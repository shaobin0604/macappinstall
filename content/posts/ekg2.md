---
title: "Install ekg2 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Multiplatform, multiprotocol, plugin-based instant messenger"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ekg2 on MacOS using homebrew

- App Name: ekg2
- App description: Multiplatform, multiprotocol, plugin-based instant messenger
- App Version: 0.3.1
- App Website: https://github.com/ekg2/ekg2

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ekg2 with the following command
   ```
   brew install ekg2
   ```
4. ekg2 is ready to use now!