---
title: "Install libsndfile on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C library for files containing sampled sound"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libsndfile on MacOS using homebrew

- App Name: libsndfile
- App description: C library for files containing sampled sound
- App Version: 1.0.31
- App Website: https://libsndfile.github.io/libsndfile/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libsndfile with the following command
   ```
   brew install libsndfile
   ```
4. libsndfile is ready to use now!