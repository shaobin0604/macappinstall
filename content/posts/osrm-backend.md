---
title: "Install osrm-backend on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "High performance routing engine"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install osrm-backend on MacOS using homebrew

- App Name: osrm-backend
- App description: High performance routing engine
- App Version: 5.26.0
- App Website: http://project-osrm.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install osrm-backend with the following command
   ```
   brew install osrm-backend
   ```
4. osrm-backend is ready to use now!