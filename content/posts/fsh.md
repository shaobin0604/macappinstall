---
title: "Install fsh on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Provides remote command execution"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fsh on MacOS using homebrew

- App Name: fsh
- App description: Provides remote command execution
- App Version: 1.2
- App Website: https://www.lysator.liu.se/fsh/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fsh with the following command
   ```
   brew install fsh
   ```
4. fsh is ready to use now!