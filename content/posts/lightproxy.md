---
title: "Install LightProxy on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Proxy & Debug tools based on whistle with Chrome Devtools UI"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install LightProxy on MacOS using homebrew

- App Name: LightProxy
- App description: Proxy & Debug tools based on whistle with Chrome Devtools UI
- App Version: 1.1.41
- App Website: https://alibaba.github.io/lightproxy/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install LightProxy with the following command
   ```
   brew install --cask lightproxy
   ```
4. LightProxy is ready to use now!