---
title: "Install sysdig on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "System-level exploration and troubleshooting tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sysdig on MacOS using homebrew

- App Name: sysdig
- App description: System-level exploration and troubleshooting tool
- App Version: 0.28.0
- App Website: https://sysdig.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sysdig with the following command
   ```
   brew install sysdig
   ```
4. sysdig is ready to use now!