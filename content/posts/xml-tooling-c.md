---
title: "Install xml-tooling-c on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Provides a higher level interface to XML processing"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install xml-tooling-c on MacOS using homebrew

- App Name: xml-tooling-c
- App description: Provides a higher level interface to XML processing
- App Version: 3.2.0
- App Website: https://wiki.shibboleth.net/confluence/display/OpenSAML/XMLTooling-C

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install xml-tooling-c with the following command
   ```
   brew install xml-tooling-c
   ```
4. xml-tooling-c is ready to use now!