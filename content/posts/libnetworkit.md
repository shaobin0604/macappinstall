---
title: "Install libnetworkit on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "NetworKit is an OS-toolkit for large-scale network analysis"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libnetworkit on MacOS using homebrew

- App Name: libnetworkit
- App description: NetworKit is an OS-toolkit for large-scale network analysis
- App Version: 9.1.1
- App Website: https://networkit.github.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libnetworkit with the following command
   ```
   brew install libnetworkit
   ```
4. libnetworkit is ready to use now!