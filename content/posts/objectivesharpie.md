---
title: "Install Objective Sharpie on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool used to generate C# interfaces starting from objective-c code"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Objective Sharpie on MacOS using homebrew

- App Name: Objective Sharpie
- App description: Tool used to generate C# interfaces starting from objective-c code
- App Version: 3.5.45
- App Website: https://docs.microsoft.com/en-au/xamarin/cross-platform/macios/binding/objective-sharpie/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Objective Sharpie with the following command
   ```
   brew install --cask objectivesharpie
   ```
4. Objective Sharpie is ready to use now!