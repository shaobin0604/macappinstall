---
title: "Install VASSAL on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Board game engine"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install VASSAL on MacOS using homebrew

- App Name: VASSAL
- App description: Board game engine
- App Version: 3.6.5
- App Website: https://www.vassalengine.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install VASSAL with the following command
   ```
   brew install --cask vassal
   ```
4. VASSAL is ready to use now!