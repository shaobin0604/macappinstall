---
title: "Install AzireVPN on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Open-source VPN client for AzireVPN"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install AzireVPN on MacOS using homebrew

- App Name: AzireVPN
- App description: Open-source VPN client for AzireVPN
- App Version: 0.5
- App Website: https://www.azirevpn.com/client

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install AzireVPN with the following command
   ```
   brew install --cask azirevpn
   ```
4. AzireVPN is ready to use now!