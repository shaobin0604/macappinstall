---
title: "Install Mighty Mike on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Top-down action game from Pangea Software (a.k.a. Power Pete)"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Mighty Mike on MacOS using homebrew

- App Name: Mighty Mike
- App description: Top-down action game from Pangea Software (a.k.a. Power Pete)
- App Version: 3.0.0
- App Website: https://pangeasoft.net/mightymike/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Mighty Mike with the following command
   ```
   brew install --cask mightymike
   ```
4. Mighty Mike is ready to use now!