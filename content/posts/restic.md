---
title: "Install restic on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Fast, efficient and secure backup program"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install restic on MacOS using homebrew

- App Name: restic
- App description: Fast, efficient and secure backup program
- App Version: 0.12.1
- App Website: https://restic.github.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install restic with the following command
   ```
   brew install restic
   ```
4. restic is ready to use now!