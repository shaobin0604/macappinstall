---
title: "Install grpcui on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Interactive web UI for gRPC, along the lines of postman"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install grpcui on MacOS using homebrew

- App Name: grpcui
- App description: Interactive web UI for gRPC, along the lines of postman
- App Version: 1.3.0
- App Website: https://github.com/fullstorydev/grpcui

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install grpcui with the following command
   ```
   brew install grpcui
   ```
4. grpcui is ready to use now!