---
title: "Install libtrace on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library for trace processing supporting multiple inputs"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libtrace on MacOS using homebrew

- App Name: libtrace
- App description: Library for trace processing supporting multiple inputs
- App Version: 4.0.17
- App Website: https://research.wand.net.nz/software/libtrace.php

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libtrace with the following command
   ```
   brew install libtrace
   ```
4. libtrace is ready to use now!