---
title: "Install whistle on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "HTTP, HTTP2, HTTPS, Websocket debugging proxy"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install whistle on MacOS using homebrew

- App Name: whistle
- App description: HTTP, HTTP2, HTTPS, Websocket debugging proxy
- App Version: 2.9.1
- App Website: https://github.com/avwo/whistle

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install whistle with the following command
   ```
   brew install whistle
   ```
4. whistle is ready to use now!