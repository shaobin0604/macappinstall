---
title: "Install mtools on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tools for manipulating MSDOS files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mtools on MacOS using homebrew

- App Name: mtools
- App description: Tools for manipulating MSDOS files
- App Version: 4.0.37
- App Website: https://www.gnu.org/software/mtools/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mtools with the following command
   ```
   brew install mtools
   ```
4. mtools is ready to use now!