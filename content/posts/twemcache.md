---
title: "Install twemcache on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Twitter fork of memcached"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install twemcache on MacOS using homebrew

- App Name: twemcache
- App description: Twitter fork of memcached
- App Version: 2.6.3
- App Website: https://github.com/twitter/twemcache

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install twemcache with the following command
   ```
   brew install twemcache
   ```
4. twemcache is ready to use now!