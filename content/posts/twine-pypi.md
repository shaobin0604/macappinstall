---
title: "Install twine-pypi on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utilities for interacting with PyPI"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install twine-pypi on MacOS using homebrew

- App Name: twine-pypi
- App description: Utilities for interacting with PyPI
- App Version: 3.8.0
- App Website: https://github.com/pypa/twine

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install twine-pypi with the following command
   ```
   brew install twine-pypi
   ```
4. twine-pypi is ready to use now!