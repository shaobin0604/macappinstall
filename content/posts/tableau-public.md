---
title: "Install Tableau Public on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Explore, create and publicly share data visualizations online"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Tableau Public on MacOS using homebrew

- App Name: Tableau Public
- App description: Explore, create and publicly share data visualizations online
- App Version: 2021.4.3
- App Website: https://public.tableau.com/s/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Tableau Public with the following command
   ```
   brew install --cask tableau-public
   ```
4. Tableau Public is ready to use now!