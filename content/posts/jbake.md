---
title: "Install jbake on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Java based static site/blog generator"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install jbake on MacOS using homebrew

- App Name: jbake
- App description: Java based static site/blog generator
- App Version: 2.6.7
- App Website: https://jbake.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install jbake with the following command
   ```
   brew install jbake
   ```
4. jbake is ready to use now!