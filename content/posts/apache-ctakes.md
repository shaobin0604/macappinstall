---
title: "Install apache-ctakes on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "NLP system for extraction of information from EMR clinical text"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install apache-ctakes on MacOS using homebrew

- App Name: apache-ctakes
- App description: NLP system for extraction of information from EMR clinical text
- App Version: 4.0.0.1
- App Website: https://ctakes.apache.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install apache-ctakes with the following command
   ```
   brew install apache-ctakes
   ```
4. apache-ctakes is ready to use now!