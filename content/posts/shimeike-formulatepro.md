---
title: "Install FormulatePro on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Overlays text and graphics on PDF documents"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install FormulatePro on MacOS using homebrew

- App Name: FormulatePro
- App description: Overlays text and graphics on PDF documents
- App Version: 0.0.6
- App Website: https://github.com/shimeike/formulatepro/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install FormulatePro with the following command
   ```
   brew install --cask shimeike-formulatepro
   ```
4. FormulatePro is ready to use now!