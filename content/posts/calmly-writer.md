---
title: "Install Calmly Writer on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Word processor with markdown formatting and select themes"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Calmly Writer on MacOS using homebrew

- App Name: Calmly Writer
- App description: Word processor with markdown formatting and select themes
- App Version: 2.0.39
- App Website: https://calmlywriter.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Calmly Writer with the following command
   ```
   brew install --cask calmly-writer
   ```
4. Calmly Writer is ready to use now!