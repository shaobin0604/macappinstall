---
title: "Install Mora Downloader on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Online music and video store for the Japanese market"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Mora Downloader on MacOS using homebrew

- App Name: Mora Downloader
- App description: Online music and video store for the Japanese market
- App Version: 2.0.0.5,6
- App Website: https://mora.jp/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Mora Downloader with the following command
   ```
   brew install --cask moradownloader
   ```
4. Mora Downloader is ready to use now!