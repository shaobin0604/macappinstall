---
title: "Install Flip4Mac on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Play back and convert Windows Media"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Flip4Mac on MacOS using homebrew

- App Name: Flip4Mac
- App description: Play back and convert Windows Media
- App Version: 3.3.8
- App Website: https://www.telestream.net/flip4mac/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Flip4Mac with the following command
   ```
   brew install --cask flip4mac
   ```
4. Flip4Mac is ready to use now!