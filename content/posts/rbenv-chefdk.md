---
title: "Install rbenv-chefdk on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Treat ChefDK as another version in rbenv"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install rbenv-chefdk on MacOS using homebrew

- App Name: rbenv-chefdk
- App description: Treat ChefDK as another version in rbenv
- App Version: 1.0.0
- App Website: https://github.com/docwhat/rbenv-chefdk

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install rbenv-chefdk with the following command
   ```
   brew install rbenv-chefdk
   ```
4. rbenv-chefdk is ready to use now!