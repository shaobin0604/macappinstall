---
title: "Install mix-completion on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Elixir Mix completion plus shortcuts/colors"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mix-completion on MacOS using homebrew

- App Name: mix-completion
- App description: Elixir Mix completion plus shortcuts/colors
- App Version: 0.8.2
- App Website: https://github.com/davidhq/mix-power-completion

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mix-completion with the following command
   ```
   brew install mix-completion
   ```
4. mix-completion is ready to use now!