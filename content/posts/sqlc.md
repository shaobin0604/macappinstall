---
title: "Install sqlc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Generate type safe Go from SQL"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sqlc on MacOS using homebrew

- App Name: sqlc
- App description: Generate type safe Go from SQL
- App Version: 1.12.0
- App Website: https://sqlc.dev/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sqlc with the following command
   ```
   brew install sqlc
   ```
4. sqlc is ready to use now!