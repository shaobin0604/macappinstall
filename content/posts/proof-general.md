---
title: "Install proof-general on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Emacs-based generic interface for theorem provers"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install proof-general on MacOS using homebrew

- App Name: proof-general
- App description: Emacs-based generic interface for theorem provers
- App Version: 4.4
- App Website: https://proofgeneral.github.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install proof-general with the following command
   ```
   brew install proof-general
   ```
4. proof-general is ready to use now!