---
title: "Install Endurance on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Battery monitoring and management"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Endurance on MacOS using homebrew

- App Name: Endurance
- App description: Battery monitoring and management
- App Version: 3.1,47
- App Website: https://enduranceapp.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Endurance with the following command
   ```
   brew install --cask endurance
   ```
4. Endurance is ready to use now!