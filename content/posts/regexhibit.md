---
title: "Install RegExhibit on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Perl regex assistant"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install RegExhibit on MacOS using homebrew

- App Name: RegExhibit
- App description: Perl regex assistant
- App Version: 1.2
- App Website: https://roger-jolly.nl/software/#regexhibit

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install RegExhibit with the following command
   ```
   brew install --cask regexhibit
   ```
4. RegExhibit is ready to use now!