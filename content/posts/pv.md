---
title: "Install pv on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Monitor data's progress through a pipe"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pv on MacOS using homebrew

- App Name: pv
- App description: Monitor data's progress through a pipe
- App Version: 1.6.20
- App Website: https://www.ivarch.com/programs/pv.shtml

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pv with the following command
   ```
   brew install pv
   ```
4. pv is ready to use now!