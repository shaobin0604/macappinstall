---
title: "Install pagmo on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Scientific library for massively parallel optimization"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pagmo on MacOS using homebrew

- App Name: pagmo
- App description: Scientific library for massively parallel optimization
- App Version: 2.18.0
- App Website: https://esa.github.io/pagmo2/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pagmo with the following command
   ```
   brew install pagmo
   ```
4. pagmo is ready to use now!