---
title: "Install iso-codes on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Provides lists of various ISO standards"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install iso-codes on MacOS using homebrew

- App Name: iso-codes
- App description: Provides lists of various ISO standards
- App Version: 4.9.0
- App Website: https://salsa.debian.org/iso-codes-team/iso-codes

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install iso-codes with the following command
   ```
   brew install iso-codes
   ```
4. iso-codes is ready to use now!