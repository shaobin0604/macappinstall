---
title: "Install Cisco Jabber on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Jabber client from Cisco"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Cisco Jabber on MacOS using homebrew

- App Name: Cisco Jabber
- App description: Jabber client from Cisco
- App Version: 20210902045804
- App Website: https://www.webex.com/downloads/jabber.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Cisco Jabber with the following command
   ```
   brew install --cask cisco-jabber
   ```
4. Cisco Jabber is ready to use now!