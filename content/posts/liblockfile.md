---
title: "Install liblockfile on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library providing functions to lock standard mailboxes"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install liblockfile on MacOS using homebrew

- App Name: liblockfile
- App description: Library providing functions to lock standard mailboxes
- App Version: 1.17
- App Website: https://tracker.debian.org/pkg/liblockfile

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install liblockfile with the following command
   ```
   brew install liblockfile
   ```
4. liblockfile is ready to use now!