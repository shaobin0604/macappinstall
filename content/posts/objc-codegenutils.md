---
title: "Install objc-codegenutils on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Three small tools to help work with XCode"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install objc-codegenutils on MacOS using homebrew

- App Name: objc-codegenutils
- App description: Three small tools to help work with XCode
- App Version: 1.0
- App Website: https://github.com/square/objc-codegenutils

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install objc-codegenutils with the following command
   ```
   brew install objc-codegenutils
   ```
4. objc-codegenutils is ready to use now!