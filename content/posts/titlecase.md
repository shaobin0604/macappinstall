---
title: "Install titlecase on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Script to convert text to title case"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install titlecase on MacOS using homebrew

- App Name: titlecase
- App description: Script to convert text to title case
- App Version: 1.004
- App Website: http://plasmasturm.org/code/titlecase/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install titlecase with the following command
   ```
   brew install titlecase
   ```
4. titlecase is ready to use now!