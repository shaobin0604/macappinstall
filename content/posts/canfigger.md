---
title: "Install canfigger on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Simple configuration file parser library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install canfigger on MacOS using homebrew

- App Name: canfigger
- App description: Simple configuration file parser library
- App Version: 0.2.0
- App Website: https://github.com/andy5995/canfigger/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install canfigger with the following command
   ```
   brew install canfigger
   ```
4. canfigger is ready to use now!