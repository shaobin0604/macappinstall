---
title: "Install lcdf-typetools on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Manipulate OpenType and multiple-master fonts"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install lcdf-typetools on MacOS using homebrew

- App Name: lcdf-typetools
- App description: Manipulate OpenType and multiple-master fonts
- App Version: 2.108
- App Website: https://www.lcdf.org/type/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install lcdf-typetools with the following command
   ```
   brew install lcdf-typetools
   ```
4. lcdf-typetools is ready to use now!