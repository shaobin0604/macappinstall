---
title: "Install unittest on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C++ Unit Test Framework"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install unittest on MacOS using homebrew

- App Name: unittest
- App description: C++ Unit Test Framework
- App Version: 0.50-62
- App Website: https://unittest.red-bean.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install unittest with the following command
   ```
   brew install unittest
   ```
4. unittest is ready to use now!