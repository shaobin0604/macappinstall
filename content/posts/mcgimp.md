---
title: "Install McGimp on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Recompiled GIMP installation"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install McGimp on MacOS using homebrew

- App Name: McGimp
- App description: Recompiled GIMP installation
- App Version: 2.10.22
- App Website: https://www.partha.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install McGimp with the following command
   ```
   brew install --cask mcgimp
   ```
4. McGimp is ready to use now!