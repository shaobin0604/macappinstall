---
title: "Install c-ares on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Asynchronous DNS library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install c-ares on MacOS using homebrew

- App Name: c-ares
- App description: Asynchronous DNS library
- App Version: 1.18.1
- App Website: https://c-ares.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install c-ares with the following command
   ```
   brew install c-ares
   ```
4. c-ares is ready to use now!