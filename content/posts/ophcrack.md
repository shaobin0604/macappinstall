---
title: "Install ophcrack on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Microsoft Windows password cracker using rainbow tables"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ophcrack on MacOS using homebrew

- App Name: ophcrack
- App description: Microsoft Windows password cracker using rainbow tables
- App Version: 3.8.0
- App Website: https://ophcrack.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ophcrack with the following command
   ```
   brew install ophcrack
   ```
4. ophcrack is ready to use now!