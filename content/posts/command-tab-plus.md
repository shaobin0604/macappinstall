---
title: "Install Command-Tab Plus on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Keyboard-centric application and window switcher"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Command-Tab Plus on MacOS using homebrew

- App Name: Command-Tab Plus
- App description: Keyboard-centric application and window switcher
- App Version: 1.130,380
- App Website: https://noteifyapp.com/command-tab-plus/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Command-Tab Plus with the following command
   ```
   brew install --cask command-tab-plus
   ```
4. Command-Tab Plus is ready to use now!