---
title: "Install MeshLab on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Mesh processing system"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install MeshLab on MacOS using homebrew

- App Name: MeshLab
- App description: Mesh processing system
- App Version: 2021.10
- App Website: https://www.meshlab.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install MeshLab with the following command
   ```
   brew install --cask meshlab
   ```
4. MeshLab is ready to use now!