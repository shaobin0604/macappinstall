---
title: "Install Qv2ray on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "V2Ray GUI client with extensive protocol support"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Qv2ray on MacOS using homebrew

- App Name: Qv2ray
- App description: V2Ray GUI client with extensive protocol support
- App Version: 2.7.0
- App Website: https://qv2ray.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Qv2ray with the following command
   ```
   brew install --cask qv2ray
   ```
4. Qv2ray is ready to use now!