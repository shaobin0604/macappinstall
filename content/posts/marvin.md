---
title: "Install Amazing Marvin on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Personal productivity app"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Amazing Marvin on MacOS using homebrew

- App Name: Amazing Marvin
- App description: Personal productivity app
- App Version: 1.61.0
- App Website: https://www.amazingmarvin.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Amazing Marvin with the following command
   ```
   brew install --cask marvin
   ```
4. Amazing Marvin is ready to use now!