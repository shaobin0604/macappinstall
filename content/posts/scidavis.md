---
title: "Install SciDAVis on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Application for scientific data analysis and visualization"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install SciDAVis on MacOS using homebrew

- App Name: SciDAVis
- App description: Application for scientific data analysis and visualization
- App Version: 2.7.1
- App Website: https://scidavis.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install SciDAVis with the following command
   ```
   brew install --cask scidavis
   ```
4. SciDAVis is ready to use now!