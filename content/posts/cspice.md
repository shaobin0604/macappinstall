---
title: "Install cspice on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Observation geometry system for robotic space science missions"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cspice on MacOS using homebrew

- App Name: cspice
- App description: Observation geometry system for robotic space science missions
- App Version: 67
- App Website: https://naif.jpl.nasa.gov/naif/toolkit.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cspice with the following command
   ```
   brew install cspice
   ```
4. cspice is ready to use now!