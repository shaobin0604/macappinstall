---
title: "Install SpaceId on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Menu bar indicator showing the currently selected space"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install SpaceId on MacOS using homebrew

- App Name: SpaceId
- App description: Menu bar indicator showing the currently selected space
- App Version: 1.4
- App Website: https://github.com/dshnkao/SpaceId/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install SpaceId with the following command
   ```
   brew install --cask spaceid
   ```
4. SpaceId is ready to use now!