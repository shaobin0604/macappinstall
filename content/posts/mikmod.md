---
title: "Install mikmod on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Portable tracked music player"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mikmod on MacOS using homebrew

- App Name: mikmod
- App description: Portable tracked music player
- App Version: 3.2.8
- App Website: https://mikmod.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mikmod with the following command
   ```
   brew install mikmod
   ```
4. mikmod is ready to use now!