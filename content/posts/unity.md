---
title: "Install Unity Editor on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Platform for 3D content"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Unity Editor on MacOS using homebrew

- App Name: Unity Editor
- App description: Platform for 3D content
- App Version: 2021.2.11f1,e50cafbb4399
- App Website: https://unity.com/products

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Unity Editor with the following command
   ```
   brew install --cask unity
   ```
4. Unity Editor is ready to use now!