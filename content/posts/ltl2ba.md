---
title: "Install ltl2ba on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Translate LTL formulae to Buchi automata"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ltl2ba on MacOS using homebrew

- App Name: ltl2ba
- App description: Translate LTL formulae to Buchi automata
- App Version: 1.3
- App Website: https://www.lsv.ens-cachan.fr/~gastin/ltl2ba/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ltl2ba with the following command
   ```
   brew install ltl2ba
   ```
4. ltl2ba is ready to use now!