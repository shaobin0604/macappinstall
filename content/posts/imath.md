---
title: "Install imath on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library of 2D and 3D vector, matrix, and math operations"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install imath on MacOS using homebrew

- App Name: imath
- App description: Library of 2D and 3D vector, matrix, and math operations
- App Version: 3.1.4
- App Website: https://www.openexr.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install imath with the following command
   ```
   brew install imath
   ```
4. imath is ready to use now!