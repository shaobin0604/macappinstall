---
title: "Install python-tabulate on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Pretty-print tabular data in Python"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install python-tabulate on MacOS using homebrew

- App Name: python-tabulate
- App description: Pretty-print tabular data in Python
- App Version: 0.8.9
- App Website: https://pypi.org/project/tabulate/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install python-tabulate with the following command
   ```
   brew install python-tabulate
   ```
4. python-tabulate is ready to use now!