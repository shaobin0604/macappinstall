---
title: "Install libde265 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Open h.265 video codec implementation"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libde265 on MacOS using homebrew

- App Name: libde265
- App description: Open h.265 video codec implementation
- App Version: 1.0.8
- App Website: https://github.com/strukturag/libde265

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libde265 with the following command
   ```
   brew install libde265
   ```
4. libde265 is ready to use now!