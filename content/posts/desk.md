---
title: "Install desk on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Lightweight workspace manager for the shell"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install desk on MacOS using homebrew

- App Name: desk
- App description: Lightweight workspace manager for the shell
- App Version: 0.6.0
- App Website: https://github.com/jamesob/desk

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install desk with the following command
   ```
   brew install desk
   ```
4. desk is ready to use now!