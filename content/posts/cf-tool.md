---
title: "Install cf-tool on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line tool for Codeforces contests"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cf-tool on MacOS using homebrew

- App Name: cf-tool
- App description: Command-line tool for Codeforces contests
- App Version: 1.0.0
- App Website: https://github.com/xalanq/cf-tool

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cf-tool with the following command
   ```
   brew install cf-tool
   ```
4. cf-tool is ready to use now!