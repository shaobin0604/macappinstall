---
title: "Install darkice on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Live audio streamer"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install darkice on MacOS using homebrew

- App Name: darkice
- App description: Live audio streamer
- App Version: 1.4
- App Website: http://www.darkice.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install darkice with the following command
   ```
   brew install darkice
   ```
4. darkice is ready to use now!