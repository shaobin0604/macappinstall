---
title: "Install popt on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library like getopt(3) with a number of enhancements"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install popt on MacOS using homebrew

- App Name: popt
- App description: Library like getopt(3) with a number of enhancements
- App Version: 1.18
- App Website: https://github.com/rpm-software-management/popt

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install popt with the following command
   ```
   brew install popt
   ```
4. popt is ready to use now!