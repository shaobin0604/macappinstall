---
title: "Install wren on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Small, fast, class-based concurrent scripting language"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install wren on MacOS using homebrew

- App Name: wren
- App description: Small, fast, class-based concurrent scripting language
- App Version: 0.4.0
- App Website: https://wren.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install wren with the following command
   ```
   brew install wren
   ```
4. wren is ready to use now!