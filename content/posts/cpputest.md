---
title: "Install cpputest on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C /C++ based unit xUnit test framework"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cpputest on MacOS using homebrew

- App Name: cpputest
- App description: C /C++ based unit xUnit test framework
- App Version: 4.0
- App Website: https://www.cpputest.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cpputest with the following command
   ```
   brew install cpputest
   ```
4. cpputest is ready to use now!