---
title: "Install collectd on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Statistics collection and monitoring daemon"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install collectd on MacOS using homebrew

- App Name: collectd
- App description: Statistics collection and monitoring daemon
- App Version: 5.12.0
- App Website: https://collectd.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install collectd with the following command
   ```
   brew install collectd
   ```
4. collectd is ready to use now!