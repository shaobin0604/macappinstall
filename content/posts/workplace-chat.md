---
title: "Install Facebook Workplace Chat on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Communications portal for your organisation"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Facebook Workplace Chat on MacOS using homebrew

- App Name: Facebook Workplace Chat
- App description: Communications portal for your organisation
- App Version: 103.72.123
- App Website: https://www.facebook.com/workplace/chat-app

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Facebook Workplace Chat with the following command
   ```
   brew install --cask workplace-chat
   ```
4. Facebook Workplace Chat is ready to use now!