---
title: "Install FastScripts on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool for running time-saving scripts"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install FastScripts on MacOS using homebrew

- App Name: FastScripts
- App description: Tool for running time-saving scripts
- App Version: 3.0.6,1589
- App Website: https://redsweater.com/fastscripts/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install FastScripts with the following command
   ```
   brew install --cask fastscripts
   ```
4. FastScripts is ready to use now!