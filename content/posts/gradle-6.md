---
title: "Install gradle@6 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Open-source build automation tool based on the Groovy and Kotlin DSL"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gradle@6 on MacOS using homebrew

- App Name: gradle@6
- App description: Open-source build automation tool based on the Groovy and Kotlin DSL
- App Version: 6.9.1
- App Website: https://www.gradle.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gradle@6 with the following command
   ```
   brew install gradle@6
   ```
4. gradle@6 is ready to use now!