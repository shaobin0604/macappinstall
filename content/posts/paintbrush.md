---
title: "Install Paintbrush on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Image editor"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Paintbrush on MacOS using homebrew

- App Name: Paintbrush
- App description: Image editor
- App Version: 2.6.0,20210402
- App Website: https://paintbrush.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Paintbrush with the following command
   ```
   brew install --cask paintbrush
   ```
4. Paintbrush is ready to use now!