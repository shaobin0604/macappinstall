---
title: "Install libvpx on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "VP8/VP9 video codec"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libvpx on MacOS using homebrew

- App Name: libvpx
- App description: VP8/VP9 video codec
- App Version: 1.11.0
- App Website: https://www.webmproject.org/code/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libvpx with the following command
   ```
   brew install libvpx
   ```
4. libvpx is ready to use now!