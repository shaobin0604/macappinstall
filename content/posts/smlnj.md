---
title: "Install Standard ML of New Jersey on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Compiler for the Standard ML '97 programming language"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Standard ML of New Jersey on MacOS using homebrew

- App Name: Standard ML of New Jersey
- App description: Compiler for the Standard ML '97 programming language
- App Version: 110.99.2
- App Website: https://www.smlnj.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Standard ML of New Jersey with the following command
   ```
   brew install --cask smlnj
   ```
4. Standard ML of New Jersey is ready to use now!