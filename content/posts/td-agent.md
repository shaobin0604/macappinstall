---
title: "Install td-agent on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Fluentd distribution package"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install td-agent on MacOS using homebrew

- App Name: td-agent
- App description: Fluentd distribution package
- App Version: 4.3.0
- App Website: https://www.fluentd.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install td-agent with the following command
   ```
   brew install --cask td-agent
   ```
4. td-agent is ready to use now!