---
title: "Install chrony on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Versatile implementation of the Network Time Protocol (NTP)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install chrony on MacOS using homebrew

- App Name: chrony
- App description: Versatile implementation of the Network Time Protocol (NTP)
- App Version: 4.2
- App Website: https://chrony.tuxfamily.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install chrony with the following command
   ```
   brew install chrony
   ```
4. chrony is ready to use now!