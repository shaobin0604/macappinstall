---
title: "Install github-markdown-toc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Easy TOC creation for GitHub README.md (in go)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install github-markdown-toc on MacOS using homebrew

- App Name: github-markdown-toc
- App description: Easy TOC creation for GitHub README.md (in go)
- App Version: 1.2.0
- App Website: https://github.com/ekalinin/github-markdown-toc.go

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install github-markdown-toc with the following command
   ```
   brew install github-markdown-toc
   ```
4. github-markdown-toc is ready to use now!