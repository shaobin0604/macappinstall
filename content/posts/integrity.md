---
title: "Install Integrity on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to scan a website checking for broken links"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Integrity on MacOS using homebrew

- App Name: Integrity
- App description: Tool to scan a website checking for broken links
- App Version: 10.4.13
- App Website: https://peacockmedia.software/mac/integrity/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Integrity with the following command
   ```
   brew install --cask integrity
   ```
4. Integrity is ready to use now!