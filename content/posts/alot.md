---
title: "Install alot on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Text mode MUA using notmuch mail"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install alot on MacOS using homebrew

- App Name: alot
- App description: Text mode MUA using notmuch mail
- App Version: 0.9.1
- App Website: https://github.com/pazz/alot

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install alot with the following command
   ```
   brew install alot
   ```
4. alot is ready to use now!