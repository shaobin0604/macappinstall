---
title: "Install MediaHuman YouTube to MP3 Converter on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Downloads music from playlists or channels"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install MediaHuman YouTube to MP3 Converter on MacOS using homebrew

- App Name: MediaHuman YouTube to MP3 Converter
- App description: Downloads music from playlists or channels
- App Version: 3.9.9.68
- App Website: https://www.mediahuman.net/youtube-to-mp3/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install MediaHuman YouTube to MP3 Converter with the following command
   ```
   brew install --cask youtube-to-mp3
   ```
4. MediaHuman YouTube to MP3 Converter is ready to use now!