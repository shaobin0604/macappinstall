---
title: "Install mill on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Scala build tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mill on MacOS using homebrew

- App Name: mill
- App description: Scala build tool
- App Version: 0.10.0
- App Website: https://com-lihaoyi.github.io/mill/mill/Intro_to_Mill.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mill with the following command
   ```
   brew install mill
   ```
4. mill is ready to use now!