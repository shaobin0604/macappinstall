---
title: "Install Agisoft Metashape Standard Edition on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Process digital images and generate 3D spatial data"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Agisoft Metashape Standard Edition on MacOS using homebrew

- App Name: Agisoft Metashape Standard Edition
- App description: Process digital images and generate 3D spatial data
- App Version: 1.8.0
- App Website: https://www.agisoft.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Agisoft Metashape Standard Edition with the following command
   ```
   brew install --cask metashape
   ```
4. Agisoft Metashape Standard Edition is ready to use now!