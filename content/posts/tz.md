---
title: "Install tz on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CLI time zone visualizer"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tz on MacOS using homebrew

- App Name: tz
- App description: CLI time zone visualizer
- App Version: 0.6.1
- App Website: https://github.com/oz/tz

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tz with the following command
   ```
   brew install tz
   ```
4. tz is ready to use now!