---
title: "Install ddate on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Converts boring normal dates to fun Discordian Date"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ddate on MacOS using homebrew

- App Name: ddate
- App description: Converts boring normal dates to fun Discordian Date
- App Version: 0.2.2
- App Website: https://github.com/bo0ts/ddate

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ddate with the following command
   ```
   brew install ddate
   ```
4. ddate is ready to use now!