---
title: "Install UninstallPKG on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "PKG software package uninstall tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install UninstallPKG on MacOS using homebrew

- App Name: UninstallPKG
- App description: PKG software package uninstall tool
- App Version: 1.2.0,1516
- App Website: https://www.corecode.io/uninstallpkg/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install UninstallPKG with the following command
   ```
   brew install --cask uninstallpkg
   ```
4. UninstallPKG is ready to use now!