---
title: "Install fpdns on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Fingerprint DNS server versions"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fpdns on MacOS using homebrew

- App Name: fpdns
- App description: Fingerprint DNS server versions
- App Version: 20190131
- App Website: https://github.com/kirei/fpdns

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fpdns with the following command
   ```
   brew install fpdns
   ```
4. fpdns is ready to use now!