---
title: "Install wireguard-go on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Userspace Go implementation of WireGuard"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install wireguard-go on MacOS using homebrew

- App Name: wireguard-go
- App description: Userspace Go implementation of WireGuard
- App Version: 0.0.20220117
- App Website: https://www.wireguard.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install wireguard-go with the following command
   ```
   brew install wireguard-go
   ```
4. wireguard-go is ready to use now!