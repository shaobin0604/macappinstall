---
title: "Install utf8cpp on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "UTF-8 with C++ in a Portable Way"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install utf8cpp on MacOS using homebrew

- App Name: utf8cpp
- App description: UTF-8 with C++ in a Portable Way
- App Version: 3.2.1
- App Website: https://github.com/nemtrif/utfcpp

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install utf8cpp with the following command
   ```
   brew install utf8cpp
   ```
4. utf8cpp is ready to use now!