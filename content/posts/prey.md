---
title: "Install Prey on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Anti-theft, data security, and management platform"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Prey on MacOS using homebrew

- App Name: Prey
- App description: Anti-theft, data security, and management platform
- App Version: 1.9.16
- App Website: https://www.preyproject.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Prey with the following command
   ```
   brew install --cask prey
   ```
4. Prey is ready to use now!