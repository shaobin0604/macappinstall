---
title: "Install pari-galpol on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Galois polynomial database for PARI/GP"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pari-galpol on MacOS using homebrew

- App Name: pari-galpol
- App description: Galois polynomial database for PARI/GP
- App Version: 20180625
- App Website: https://pari.math.u-bordeaux.fr/packages.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pari-galpol with the following command
   ```
   brew install pari-galpol
   ```
4. pari-galpol is ready to use now!