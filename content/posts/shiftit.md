---
title: "Install ShiftIt on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to manage the size and position of windows"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ShiftIt on MacOS using homebrew

- App Name: ShiftIt
- App description: Tool to manage the size and position of windows
- App Version: 1.6.6
- App Website: https://github.com/fikovnik/ShiftIt/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ShiftIt with the following command
   ```
   brew install --cask shiftit
   ```
4. ShiftIt is ready to use now!