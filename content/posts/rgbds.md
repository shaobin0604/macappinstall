---
title: "Install rgbds on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Rednex GameBoy Development System"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install rgbds on MacOS using homebrew

- App Name: rgbds
- App description: Rednex GameBoy Development System
- App Version: 0.5.2
- App Website: https://rgbds.gbdev.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install rgbds with the following command
   ```
   brew install rgbds
   ```
4. rgbds is ready to use now!