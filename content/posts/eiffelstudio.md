---
title: "Install eiffelstudio on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Development environment for the Eiffel language"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install eiffelstudio on MacOS using homebrew

- App Name: eiffelstudio
- App description: Development environment for the Eiffel language
- App Version: 19.05.10.3187
- App Website: https://www.eiffel.com

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install eiffelstudio with the following command
   ```
   brew install eiffelstudio
   ```
4. eiffelstudio is ready to use now!