---
title: "Install PPDuck on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Integrates several image compression algorithms"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install PPDuck on MacOS using homebrew

- App Name: PPDuck
- App description: Integrates several image compression algorithms
- App Version: 3.10.13
- App Website: https://ppduck.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install PPDuck with the following command
   ```
   brew install --cask ppduck
   ```
4. PPDuck is ready to use now!