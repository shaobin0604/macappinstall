---
title: "Install DEVONagent Pro on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to help searching the web more efficiently"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install DEVONagent Pro on MacOS using homebrew

- App Name: DEVONagent Pro
- App description: Tool to help searching the web more efficiently
- App Version: 3.11.5
- App Website: https://www.devontechnologies.com/apps/devonagent

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install DEVONagent Pro with the following command
   ```
   brew install --cask devonagent
   ```
4. DEVONagent Pro is ready to use now!