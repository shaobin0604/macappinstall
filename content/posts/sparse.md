---
title: "Install sparse on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Static C code analysis tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sparse on MacOS using homebrew

- App Name: sparse
- App description: Static C code analysis tool
- App Version: 0.6.4
- App Website: https://sparse.wiki.kernel.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sparse with the following command
   ```
   brew install sparse
   ```
4. sparse is ready to use now!