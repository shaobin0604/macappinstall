---
title: "Install Tentacle Sync Studio on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Automatically synchronize video and audio via timecode"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Tentacle Sync Studio on MacOS using homebrew

- App Name: Tentacle Sync Studio
- App description: Automatically synchronize video and audio via timecode
- App Version: 1.30
- App Website: https://tentaclesync.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Tentacle Sync Studio with the following command
   ```
   brew install --cask tentacle-sync-studio
   ```
4. Tentacle Sync Studio is ready to use now!