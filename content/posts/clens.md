---
title: "Install clens on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library to help port code from OpenBSD to other operating systems"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install clens on MacOS using homebrew

- App Name: clens
- App description: Library to help port code from OpenBSD to other operating systems
- App Version: 0.7.0
- App Website: https://github.com/conformal/clens

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install clens with the following command
   ```
   brew install clens
   ```
4. clens is ready to use now!