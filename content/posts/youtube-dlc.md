---
title: "Install youtube-dlc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Media downloader supporting various sites such as youtube"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install youtube-dlc on MacOS using homebrew

- App Name: youtube-dlc
- App description: Media downloader supporting various sites such as youtube
- App Version: 2020.11.11-3
- App Website: https://github.com/blackjack4494/yt-dlc

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install youtube-dlc with the following command
   ```
   brew install youtube-dlc
   ```
4. youtube-dlc is ready to use now!