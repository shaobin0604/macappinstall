---
title: "Install kdoctor on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Environment diagnostics for Kotlin Multiplatform Mobile app development"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install kdoctor on MacOS using homebrew

- App Name: kdoctor
- App description: Environment diagnostics for Kotlin Multiplatform Mobile app development
- App Version: 0.0.2
- App Website: https://github.com/kotlin/kdoctor

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install kdoctor with the following command
   ```
   brew install kdoctor
   ```
4. kdoctor is ready to use now!