---
title: "Install cdk on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Curses development kit provides predefined curses widget for apps"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cdk on MacOS using homebrew

- App Name: cdk
- App description: Curses development kit provides predefined curses widget for apps
- App Version: 5.0-20211216
- App Website: https://invisible-island.net/cdk/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cdk with the following command
   ```
   brew install cdk
   ```
4. cdk is ready to use now!