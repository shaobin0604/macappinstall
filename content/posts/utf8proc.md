---
title: "Install utf8proc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Clean C library for processing UTF-8 Unicode data"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install utf8proc on MacOS using homebrew

- App Name: utf8proc
- App description: Clean C library for processing UTF-8 Unicode data
- App Version: 2.7.0
- App Website: https://juliastrings.github.io/utf8proc/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install utf8proc with the following command
   ```
   brew install utf8proc
   ```
4. utf8proc is ready to use now!