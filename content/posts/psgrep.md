---
title: "Install psgrep on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Shortcut for the 'ps aux | grep' idiom"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install psgrep on MacOS using homebrew

- App Name: psgrep
- App description: Shortcut for the 'ps aux | grep' idiom
- App Version: 1.0.9
- App Website: https://github.com/jvz/psgrep

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install psgrep with the following command
   ```
   brew install psgrep
   ```
4. psgrep is ready to use now!