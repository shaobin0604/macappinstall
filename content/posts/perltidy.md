---
title: "Install perltidy on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Indents and reformats Perl scripts to make them easier to read"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install perltidy on MacOS using homebrew

- App Name: perltidy
- App description: Indents and reformats Perl scripts to make them easier to read
- App Version: 20220217
- App Website: https://perltidy.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install perltidy with the following command
   ```
   brew install perltidy
   ```
4. perltidy is ready to use now!