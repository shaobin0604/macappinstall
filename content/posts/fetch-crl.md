---
title: "Install fetch-crl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Retrieve certificate revocation lists (CRLs)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fetch-crl on MacOS using homebrew

- App Name: fetch-crl
- App description: Retrieve certificate revocation lists (CRLs)
- App Version: 3.0.22
- App Website: https://wiki.nikhef.nl/grid/FetchCRL3

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fetch-crl with the following command
   ```
   brew install fetch-crl
   ```
4. fetch-crl is ready to use now!