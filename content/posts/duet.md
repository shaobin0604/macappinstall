---
title: "Install Duet on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool for using an iPad as a second display"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Duet on MacOS using homebrew

- App Name: Duet
- App description: Tool for using an iPad as a second display
- App Version: 2.4.2.1
- App Website: https://www.duetdisplay.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Duet with the following command
   ```
   brew install --cask duet
   ```
4. Duet is ready to use now!