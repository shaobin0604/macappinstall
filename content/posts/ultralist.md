---
title: "Install ultralist on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Simple GTD-style task management for the command-line"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ultralist on MacOS using homebrew

- App Name: ultralist
- App description: Simple GTD-style task management for the command-line
- App Version: 1.7.0
- App Website: https://ultralist.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ultralist with the following command
   ```
   brew install ultralist
   ```
4. ultralist is ready to use now!