---
title: "Install cyrus-sasl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Simple Authentication and Security Layer"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cyrus-sasl on MacOS using homebrew

- App Name: cyrus-sasl
- App description: Simple Authentication and Security Layer
- App Version: 2.1.27
- App Website: https://www.cyrusimap.org/sasl/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cyrus-sasl with the following command
   ```
   brew install cyrus-sasl
   ```
4. cyrus-sasl is ready to use now!