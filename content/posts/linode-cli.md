---
title: "Install linode-cli on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CLI for the Linode API"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install linode-cli on MacOS using homebrew

- App Name: linode-cli
- App description: CLI for the Linode API
- App Version: 5.16.0
- App Website: https://www.linode.com/products/cli/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install linode-cli with the following command
   ```
   brew install linode-cli
   ```
4. linode-cli is ready to use now!