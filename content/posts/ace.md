---
title: "Install ace on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "ADAPTIVE Communication Environment: OO network programming in C++"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ace on MacOS using homebrew

- App Name: ace
- App description: ADAPTIVE Communication Environment: OO network programming in C++
- App Version: 7.0.6
- App Website: https://www.dre.vanderbilt.edu/~schmidt/ACE.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ace with the following command
   ```
   brew install ace
   ```
4. ace is ready to use now!