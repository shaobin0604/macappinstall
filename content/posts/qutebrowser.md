---
title: "Install qutebrowser on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Keyboard-driven, vim-like browser based on PyQt5"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install qutebrowser on MacOS using homebrew

- App Name: qutebrowser
- App description: Keyboard-driven, vim-like browser based on PyQt5
- App Version: 2.4.0
- App Website: https://www.qutebrowser.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install qutebrowser with the following command
   ```
   brew install --cask qutebrowser
   ```
4. qutebrowser is ready to use now!