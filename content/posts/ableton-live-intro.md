---
title: "Install Ableton Live Intro on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Sound and music editor"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Ableton Live Intro on MacOS using homebrew

- App Name: Ableton Live Intro
- App description: Sound and music editor
- App Version: 11.1
- App Website: https://www.ableton.com/en/live/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Ableton Live Intro with the following command
   ```
   brew install --cask ableton-live-intro
   ```
4. Ableton Live Intro is ready to use now!