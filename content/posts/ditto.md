---
title: "Install Ditto on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Screen mirroring and digital signage"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Ditto on MacOS using homebrew

- App Name: Ditto
- App description: Screen mirroring and digital signage
- App Version: 1.8.0,1641
- App Website: https://www.airsquirrels.com/ditto

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Ditto with the following command
   ```
   brew install --cask ditto
   ```
4. Ditto is ready to use now!