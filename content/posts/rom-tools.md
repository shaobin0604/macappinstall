---
title: "Install rom-tools on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tools for Multiple Arcade Machine Emulator"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install rom-tools on MacOS using homebrew

- App Name: rom-tools
- App description: Tools for Multiple Arcade Machine Emulator
- App Version: 0.240
- App Website: https://mamedev.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install rom-tools with the following command
   ```
   brew install rom-tools
   ```
4. rom-tools is ready to use now!