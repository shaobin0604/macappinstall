---
title: "Install phpunit on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Programmer-oriented testing framework for PHP"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install phpunit on MacOS using homebrew

- App Name: phpunit
- App description: Programmer-oriented testing framework for PHP
- App Version: 9.5.13
- App Website: https://phpunit.de

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install phpunit with the following command
   ```
   brew install phpunit
   ```
4. phpunit is ready to use now!