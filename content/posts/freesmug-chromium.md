---
title: "Install Chromium on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Google Chromium built to solve Chrome incompatibility issue"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Chromium on MacOS using homebrew

- App Name: Chromium
- App description: Google Chromium built to solve Chrome incompatibility issue
- App Version: 85.0.4183.102
- App Website: http://www.freesmug.org/chromium

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Chromium with the following command
   ```
   brew install --cask freesmug-chromium
   ```
4. Chromium is ready to use now!