---
title: "Install neovim on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Ambitious Vim-fork focused on extensibility and agility"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install neovim on MacOS using homebrew

- App Name: neovim
- App description: Ambitious Vim-fork focused on extensibility and agility
- App Version: 0.6.1
- App Website: https://neovim.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install neovim with the following command
   ```
   brew install neovim
   ```
4. neovim is ready to use now!