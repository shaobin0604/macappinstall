---
title: "Install Jamulus on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Play music online with friends"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Jamulus on MacOS using homebrew

- App Name: Jamulus
- App description: Play music online with friends
- App Version: 3.8.1
- App Website: https://jamulus.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Jamulus with the following command
   ```
   brew install --cask jamulus
   ```
4. Jamulus is ready to use now!