---
title: "Install Mathcha Notebook on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Mathematics editor"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Mathcha Notebook on MacOS using homebrew

- App Name: Mathcha Notebook
- App description: Mathematics editor
- App Version: 1.0.428
- App Website: https://www.mathcha.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Mathcha Notebook with the following command
   ```
   brew install --cask mathcha-notebook
   ```
4. Mathcha Notebook is ready to use now!