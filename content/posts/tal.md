---
title: "Install tal on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Align line endings if they match"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tal on MacOS using homebrew

- App Name: tal
- App description: Align line endings if they match
- App Version: 1.9
- App Website: https://web.archive.org/web/20160406172703/https://thomasjensen.com/software/tal/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tal with the following command
   ```
   brew install tal
   ```
4. tal is ready to use now!