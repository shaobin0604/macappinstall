---
title: "Install linux-headers@4.15 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Header files of the Linux kernel"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install linux-headers@4.15 on MacOS using homebrew

- App Name: linux-headers@4.15
- App description: Header files of the Linux kernel
- App Version: 4.15.18
- App Website: https://kernel.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install linux-headers@4.15 with the following command
   ```
   brew install linux-headers@4.15
   ```
4. linux-headers@4.15 is ready to use now!