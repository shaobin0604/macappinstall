---
title: "Install rofs-filtered on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Filtered read-only filesystem for FUSE"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install rofs-filtered on MacOS using homebrew

- App Name: rofs-filtered
- App description: Filtered read-only filesystem for FUSE
- App Version: 1.7
- App Website: https://github.com/gburca/rofs-filtered/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install rofs-filtered with the following command
   ```
   brew install rofs-filtered
   ```
4. rofs-filtered is ready to use now!