---
title: "Install Tenor on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Send, share and save gifs from the menu bar"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Tenor on MacOS using homebrew

- App Name: Tenor
- App description: Send, share and save gifs from the menu bar
- App Version: 2.0.5,21
- App Website: https://tenor.com/mac

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Tenor with the following command
   ```
   brew install --cask tenor
   ```
4. Tenor is ready to use now!