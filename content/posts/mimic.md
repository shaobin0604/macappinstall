---
title: "Install mimic on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Lightweight text-to-speech engine based on CMU Flite"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mimic on MacOS using homebrew

- App Name: mimic
- App description: Lightweight text-to-speech engine based on CMU Flite
- App Version: 1.3.0.1
- App Website: https://mimic.mycroft.ai

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mimic with the following command
   ```
   brew install mimic
   ```
4. mimic is ready to use now!