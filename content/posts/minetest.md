---
title: "Install minetest on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Free, open source voxel game engine and game"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install minetest on MacOS using homebrew

- App Name: minetest
- App description: Free, open source voxel game engine and game
- App Version: 5.4.1
- App Website: https://www.minetest.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install minetest with the following command
   ```
   brew install minetest
   ```
4. minetest is ready to use now!