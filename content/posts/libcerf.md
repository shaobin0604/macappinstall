---
title: "Install libcerf on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Numeric library for complex error functions"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libcerf on MacOS using homebrew

- App Name: libcerf
- App description: Numeric library for complex error functions
- App Version: 1.17
- App Website: https://jugit.fz-juelich.de/mlz/libcerf

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libcerf with the following command
   ```
   brew install libcerf
   ```
4. libcerf is ready to use now!