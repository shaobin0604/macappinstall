---
title: "Install Quickhash on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Data hashing tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Quickhash on MacOS using homebrew

- App Name: Quickhash
- App description: Data hashing tool
- App Version: 3.3.1,2258
- App Website: https://www.quickhash-gui.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Quickhash with the following command
   ```
   brew install --cask quickhash
   ```
4. Quickhash is ready to use now!