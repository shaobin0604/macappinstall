---
title: "Install Extensis Suitcase Fusion on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Font manager"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Extensis Suitcase Fusion on MacOS using homebrew

- App Name: Extensis Suitcase Fusion
- App description: Font manager
- App Version: 21.4.4
- App Website: https://www.extensis.com/suitcase-fusion/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Extensis Suitcase Fusion with the following command
   ```
   brew install --cask suitcase-fusion
   ```
4. Extensis Suitcase Fusion is ready to use now!