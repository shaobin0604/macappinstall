---
title: "Install Remotix Agent on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Remote desktop and monitoring solution"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Remotix Agent on MacOS using homebrew

- App Name: Remotix Agent
- App description: Remote desktop and monitoring solution
- App Version: 1.5.19,23333
- App Website: https://remotixcloud.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Remotix Agent with the following command
   ```
   brew install --cask remotix-agent
   ```
4. Remotix Agent is ready to use now!