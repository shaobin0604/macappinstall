---
title: "Install bash-snippets on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Collection of small bash scripts for heavy terminal users"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bash-snippets on MacOS using homebrew

- App Name: bash-snippets
- App description: Collection of small bash scripts for heavy terminal users
- App Version: 1.23.0
- App Website: https://github.com/alexanderepstein/Bash-Snippets

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bash-snippets with the following command
   ```
   brew install bash-snippets
   ```
4. bash-snippets is ready to use now!