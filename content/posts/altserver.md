---
title: "Install AltServer on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "iOS App Store alternative"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install AltServer on MacOS using homebrew

- App Name: AltServer
- App description: iOS App Store alternative
- App Version: 1.4.9,62
- App Website: https://altstore.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install AltServer with the following command
   ```
   brew install --cask altserver
   ```
4. AltServer is ready to use now!