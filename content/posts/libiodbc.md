---
title: "Install libiodbc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Database connectivity layer based on ODBC. (alternative to unixodbc)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libiodbc on MacOS using homebrew

- App Name: libiodbc
- App description: Database connectivity layer based on ODBC. (alternative to unixodbc)
- App Version: 3.52.15
- App Website: http://www.iodbc.org/dataspace/iodbc/wiki/iODBC/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libiodbc with the following command
   ```
   brew install libiodbc
   ```
4. libiodbc is ready to use now!