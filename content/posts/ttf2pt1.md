---
title: "Install ttf2pt1 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "True Type Font to Postscript Type 1 converter"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ttf2pt1 on MacOS using homebrew

- App Name: ttf2pt1
- App description: True Type Font to Postscript Type 1 converter
- App Version: 3.4.4
- App Website: https://ttf2pt1.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ttf2pt1 with the following command
   ```
   brew install ttf2pt1
   ```
4. ttf2pt1 is ready to use now!