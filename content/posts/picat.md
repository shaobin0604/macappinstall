---
title: "Install picat on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Simple, and yet powerful, logic-based multi-paradigm programming language"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install picat on MacOS using homebrew

- App Name: picat
- App description: Simple, and yet powerful, logic-based multi-paradigm programming language
- App Version: 3.1#6
- App Website: http://picat-lang.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install picat with the following command
   ```
   brew install picat
   ```
4. picat is ready to use now!