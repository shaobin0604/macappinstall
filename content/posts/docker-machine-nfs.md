---
title: "Install docker-machine-nfs on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Activates NFS on docker-machine"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install docker-machine-nfs on MacOS using homebrew

- App Name: docker-machine-nfs
- App description: Activates NFS on docker-machine
- App Version: 0.5.4
- App Website: https://github.com/adlogix/docker-machine-nfs

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install docker-machine-nfs with the following command
   ```
   brew install docker-machine-nfs
   ```
4. docker-machine-nfs is ready to use now!