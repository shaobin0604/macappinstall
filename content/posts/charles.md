---
title: "Install Charles on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Web debugging Proxy application"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Charles on MacOS using homebrew

- App Name: Charles
- App description: Web debugging Proxy application
- App Version: 4.6.2
- App Website: https://www.charlesproxy.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Charles with the following command
   ```
   brew install --cask charles
   ```
4. Charles is ready to use now!