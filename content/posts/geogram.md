---
title: "Install geogram on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Programming library of geometric algorithms"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install geogram on MacOS using homebrew

- App Name: geogram
- App description: Programming library of geometric algorithms
- App Version: 1.7.7
- App Website: http://alice.loria.fr/software/geogram/doc/html/index.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install geogram with the following command
   ```
   brew install geogram
   ```
4. geogram is ready to use now!