---
title: "Install git-filter-repo on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Quickly rewrite git repository history"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install git-filter-repo on MacOS using homebrew

- App Name: git-filter-repo
- App description: Quickly rewrite git repository history
- App Version: 2.34.0
- App Website: https://github.com/newren/git-filter-repo

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install git-filter-repo with the following command
   ```
   brew install git-filter-repo
   ```
4. git-filter-repo is ready to use now!