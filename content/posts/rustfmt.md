---
title: "Install rustfmt on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Format Rust code"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install rustfmt on MacOS using homebrew

- App Name: rustfmt
- App description: Format Rust code
- App Version: 1.4.38
- App Website: https://rust-lang.github.io/rustfmt/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install rustfmt with the following command
   ```
   brew install rustfmt
   ```
4. rustfmt is ready to use now!