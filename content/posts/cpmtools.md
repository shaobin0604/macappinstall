---
title: "Install cpmtools on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tools to access CP/M file systems"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cpmtools on MacOS using homebrew

- App Name: cpmtools
- App description: Tools to access CP/M file systems
- App Version: 2.21
- App Website: http://www.moria.de/~michael/cpmtools/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cpmtools with the following command
   ```
   brew install cpmtools
   ```
4. cpmtools is ready to use now!