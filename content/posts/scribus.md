---
title: "Install Scribus on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Free and open-source page layout program"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Scribus on MacOS using homebrew

- App Name: Scribus
- App description: Free and open-source page layout program
- App Version: 1.4.8
- App Website: https://www.scribus.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Scribus with the following command
   ```
   brew install --cask scribus
   ```
4. Scribus is ready to use now!