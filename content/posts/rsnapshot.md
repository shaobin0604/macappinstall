---
title: "Install rsnapshot on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "File system snapshot utility (based on rsync)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install rsnapshot on MacOS using homebrew

- App Name: rsnapshot
- App description: File system snapshot utility (based on rsync)
- App Version: 1.4.4
- App Website: https://www.rsnapshot.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install rsnapshot with the following command
   ```
   brew install rsnapshot
   ```
4. rsnapshot is ready to use now!