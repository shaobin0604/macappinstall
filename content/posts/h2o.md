---
title: "Install h2o on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "HTTP server with support for HTTP/1.x and HTTP/2"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install h2o on MacOS using homebrew

- App Name: h2o
- App description: HTTP server with support for HTTP/1.x and HTTP/2
- App Version: 2.2.6
- App Website: https://github.com/h2o/h2o/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install h2o with the following command
   ```
   brew install h2o
   ```
4. h2o is ready to use now!