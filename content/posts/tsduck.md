---
title: "Install tsduck on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "MPEG Transport Stream Toolkit"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tsduck on MacOS using homebrew

- App Name: tsduck
- App description: MPEG Transport Stream Toolkit
- App Version: 3.29-2651
- App Website: https://tsduck.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tsduck with the following command
   ```
   brew install tsduck
   ```
4. tsduck is ready to use now!