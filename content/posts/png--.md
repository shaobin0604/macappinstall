---
title: "Install png++ on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C++ wrapper for libpng library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install png++ on MacOS using homebrew

- App Name: png++
- App description: C++ wrapper for libpng library
- App Version: 0.2.10
- App Website: https://www.nongnu.org/pngpp/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install png++ with the following command
   ```
   brew install png++
   ```
4. png++ is ready to use now!