---
title: "Install odpi on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Oracle Database Programming Interface for Drivers and Applications"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install odpi on MacOS using homebrew

- App Name: odpi
- App description: Oracle Database Programming Interface for Drivers and Applications
- App Version: 4.3.0
- App Website: https://oracle.github.io/odpi/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install odpi with the following command
   ```
   brew install odpi
   ```
4. odpi is ready to use now!