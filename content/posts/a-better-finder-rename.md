---
title: "Install A Better Finder Rename on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Renamer for files, music and photos"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install A Better Finder Rename on MacOS using homebrew

- App Name: A Better Finder Rename
- App description: Renamer for files, music and photos
- App Version: 11.43
- App Website: https://www.publicspace.net/ABetterFinderRename/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install A Better Finder Rename with the following command
   ```
   brew install --cask a-better-finder-rename
   ```
4. A Better Finder Rename is ready to use now!