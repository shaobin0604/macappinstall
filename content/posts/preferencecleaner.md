---
title: "Install PreferenceCleaner on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utility to simplify the task of deleting preference files"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install PreferenceCleaner on MacOS using homebrew

- App Name: PreferenceCleaner
- App description: Utility to simplify the task of deleting preference files
- App Version: 2.0
- App Website: https://echomist.co.uk/software/PreferenceCleaner.php

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install PreferenceCleaner with the following command
   ```
   brew install --cask preferencecleaner
   ```
4. PreferenceCleaner is ready to use now!