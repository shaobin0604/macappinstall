---
title: "Install libmagic on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Implementation of the file(1) command"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libmagic on MacOS using homebrew

- App Name: libmagic
- App description: Implementation of the file(1) command
- App Version: 5.41
- App Website: https://www.darwinsys.com/file/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libmagic with the following command
   ```
   brew install libmagic
   ```
4. libmagic is ready to use now!