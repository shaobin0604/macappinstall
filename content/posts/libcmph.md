---
title: "Install libcmph on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C minimal perfect hashing library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libcmph on MacOS using homebrew

- App Name: libcmph
- App description: C minimal perfect hashing library
- App Version: 2.0.2
- App Website: https://cmph.sourceforge.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libcmph with the following command
   ```
   brew install libcmph
   ```
4. libcmph is ready to use now!