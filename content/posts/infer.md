---
title: "Install infer on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Static analyzer for Java, C, C++, and Objective-C"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install infer on MacOS using homebrew

- App Name: infer
- App description: Static analyzer for Java, C, C++, and Objective-C
- App Version: 1.1.0
- App Website: https://fbinfer.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install infer with the following command
   ```
   brew install infer
   ```
4. infer is ready to use now!