---
title: "Install cfn-format on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line tool for formatting AWS CloudFormation templates"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cfn-format on MacOS using homebrew

- App Name: cfn-format
- App description: Command-line tool for formatting AWS CloudFormation templates
- App Version: 1.2.0
- App Website: https://github.com/aws-cloudformation/rain

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cfn-format with the following command
   ```
   brew install cfn-format
   ```
4. cfn-format is ready to use now!