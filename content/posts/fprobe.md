---
title: "Install fprobe on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Libpcap-based NetFlow probe"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fprobe on MacOS using homebrew

- App Name: fprobe
- App description: Libpcap-based NetFlow probe
- App Version: 1.1
- App Website: https://sourceforge.net/projects/fprobe/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fprobe with the following command
   ```
   brew install fprobe
   ```
4. fprobe is ready to use now!