---
title: "Install griffon on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Application framework for desktop applications in the JVM"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install griffon on MacOS using homebrew

- App Name: griffon
- App description: Application framework for desktop applications in the JVM
- App Version: 1.5.0
- App Website: http://griffon-framework.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install griffon with the following command
   ```
   brew install griffon
   ```
4. griffon is ready to use now!