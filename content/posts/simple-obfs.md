---
title: "Install simple-obfs on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Simple obfusacting plugin of shadowsocks-libev"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install simple-obfs on MacOS using homebrew

- App Name: simple-obfs
- App description: Simple obfusacting plugin of shadowsocks-libev
- App Version: 0.0.5
- App Website: https://github.com/shadowsocks/simple-obfs

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install simple-obfs with the following command
   ```
   brew install simple-obfs
   ```
4. simple-obfs is ready to use now!