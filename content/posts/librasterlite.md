---
title: "Install librasterlite on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library to store and retrieve huge raster coverages"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install librasterlite on MacOS using homebrew

- App Name: librasterlite
- App description: Library to store and retrieve huge raster coverages
- App Version: 1.1g
- App Website: https://www.gaia-gis.it/fossil/librasterlite/index

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install librasterlite with the following command
   ```
   brew install librasterlite
   ```
4. librasterlite is ready to use now!