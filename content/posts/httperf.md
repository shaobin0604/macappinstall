---
title: "Install httperf on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool for measuring webserver performance"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install httperf on MacOS using homebrew

- App Name: httperf
- App description: Tool for measuring webserver performance
- App Version: 0.9.0
- App Website: https://github.com/httperf/httperf

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install httperf with the following command
   ```
   brew install httperf
   ```
4. httperf is ready to use now!