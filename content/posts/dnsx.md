---
title: "Install dnsx on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "DNS query and resolution tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dnsx on MacOS using homebrew

- App Name: dnsx
- App description: DNS query and resolution tool
- App Version: 1.0.8
- App Website: https://github.com/projectdiscovery/dnsx

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dnsx with the following command
   ```
   brew install dnsx
   ```
4. dnsx is ready to use now!