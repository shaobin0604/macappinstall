---
title: "Install net-tools on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Linux networking base tools"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install net-tools on MacOS using homebrew

- App Name: net-tools
- App description: Linux networking base tools
- App Version: 2.10
- App Website: https://sourceforge.net/projects/net-tools

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install net-tools with the following command
   ```
   brew install net-tools
   ```
4. net-tools is ready to use now!