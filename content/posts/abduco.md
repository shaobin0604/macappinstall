---
title: "Install abduco on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Provides session management: i.e. separate programs from terminals"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install abduco on MacOS using homebrew

- App Name: abduco
- App description: Provides session management: i.e. separate programs from terminals
- App Version: 0.6
- App Website: https://www.brain-dump.org/projects/abduco

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install abduco with the following command
   ```
   brew install abduco
   ```
4. abduco is ready to use now!