---
title: "Install swig@3 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Generate scripting interfaces to C/C++ code"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install swig@3 on MacOS using homebrew

- App Name: swig@3
- App description: Generate scripting interfaces to C/C++ code
- App Version: 3.0.12
- App Website: http://www.swig.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install swig@3 with the following command
   ```
   brew install swig@3
   ```
4. swig@3 is ready to use now!