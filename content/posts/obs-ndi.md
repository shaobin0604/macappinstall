---
title: "Install obs-ndi on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "NewTek NDI integration for OBS Studio"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install obs-ndi on MacOS using homebrew

- App Name: obs-ndi
- App description: NewTek NDI integration for OBS Studio
- App Version: 4.9.0
- App Website: https://github.com/Palakis/obs-ndi

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install obs-ndi with the following command
   ```
   brew install --cask obs-ndi
   ```
4. obs-ndi is ready to use now!