---
title: "Install attr on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Manipulate filesystem extended attributes"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install attr on MacOS using homebrew

- App Name: attr
- App description: Manipulate filesystem extended attributes
- App Version: 2.5.1
- App Website: https://savannah.nongnu.org/projects/attr

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install attr with the following command
   ```
   brew install attr
   ```
4. attr is ready to use now!