---
title: "Install oksh on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Portable OpenBSD ksh, based on the public domain Korn shell (pdksh)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install oksh on MacOS using homebrew

- App Name: oksh
- App description: Portable OpenBSD ksh, based on the public domain Korn shell (pdksh)
- App Version: 7.0
- App Website: https://github.com/ibara/oksh

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install oksh with the following command
   ```
   brew install oksh
   ```
4. oksh is ready to use now!