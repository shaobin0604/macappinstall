---
title: "Install libgeotiff on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library and tools for dealing with GeoTIFF"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libgeotiff on MacOS using homebrew

- App Name: libgeotiff
- App description: Library and tools for dealing with GeoTIFF
- App Version: 1.7.0
- App Website: https://github.com/OSGeo/libgeotiff

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libgeotiff with the following command
   ```
   brew install libgeotiff
   ```
4. libgeotiff is ready to use now!