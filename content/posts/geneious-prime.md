---
title: "Install Geneious Prime on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Bioinformatics software platform"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Geneious Prime on MacOS using homebrew

- App Name: Geneious Prime
- App description: Bioinformatics software platform
- App Version: 2022.0.2
- App Website: https://www.geneious.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Geneious Prime with the following command
   ```
   brew install --cask geneious-prime
   ```
4. Geneious Prime is ready to use now!