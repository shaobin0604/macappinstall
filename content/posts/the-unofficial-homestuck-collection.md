---
title: "Install The Unofficial Homestuck Collection on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Offline viewer for the webcomic Homestuck"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install The Unofficial Homestuck Collection on MacOS using homebrew

- App Name: The Unofficial Homestuck Collection
- App description: Offline viewer for the webcomic Homestuck
- App Version: 1.1.0
- App Website: https://bambosh.github.io/unofficial-homestuck-collection/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install The Unofficial Homestuck Collection with the following command
   ```
   brew install --cask the-unofficial-homestuck-collection
   ```
4. The Unofficial Homestuck Collection is ready to use now!