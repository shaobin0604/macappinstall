---
title: "Install sail on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CLI toolkit to provision and deploy WordPress applications to DigitalOcean"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sail on MacOS using homebrew

- App Name: sail
- App description: CLI toolkit to provision and deploy WordPress applications to DigitalOcean
- App Version: 0.10.5
- App Website: https://sailed.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sail with the following command
   ```
   brew install sail
   ```
4. sail is ready to use now!