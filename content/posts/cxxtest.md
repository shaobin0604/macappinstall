---
title: "Install cxxtest on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C++ unit testing framework similar to JUnit, CppUnit and xUnit"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cxxtest on MacOS using homebrew

- App Name: cxxtest
- App description: C++ unit testing framework similar to JUnit, CppUnit and xUnit
- App Version: 4.4
- App Website: https://cxxtest.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cxxtest with the following command
   ```
   brew install cxxtest
   ```
4. cxxtest is ready to use now!