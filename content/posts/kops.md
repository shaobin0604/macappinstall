---
title: "Install kops on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Production Grade K8s Installation, Upgrades, and Management"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install kops on MacOS using homebrew

- App Name: kops
- App description: Production Grade K8s Installation, Upgrades, and Management
- App Version: 1.22.3
- App Website: https://kops.sigs.k8s.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install kops with the following command
   ```
   brew install kops
   ```
4. kops is ready to use now!