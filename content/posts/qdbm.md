---
title: "Install qdbm on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library of routines for managing a database"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install qdbm on MacOS using homebrew

- App Name: qdbm
- App description: Library of routines for managing a database
- App Version: 1.8.78
- App Website: https://dbmx.net/qdbm/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install qdbm with the following command
   ```
   brew install qdbm
   ```
4. qdbm is ready to use now!