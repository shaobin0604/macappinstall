---
title: "Install strace on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Diagnostic, instructional, and debugging tool for the Linux kernel"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install strace on MacOS using homebrew

- App Name: strace
- App description: Diagnostic, instructional, and debugging tool for the Linux kernel
- App Version: 5.16
- App Website: https://strace.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install strace with the following command
   ```
   brew install strace
   ```
4. strace is ready to use now!