---
title: "Install slashem on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Fork/variant of Nethack"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install slashem on MacOS using homebrew

- App Name: slashem
- App description: Fork/variant of Nethack
- App Version: 0.0.8E0F1
- App Website: https://slashem.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install slashem with the following command
   ```
   brew install slashem
   ```
4. slashem is ready to use now!