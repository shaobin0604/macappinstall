---
title: "Install libetonyek on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Interpret and import Apple Keynote presentations"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libetonyek on MacOS using homebrew

- App Name: libetonyek
- App description: Interpret and import Apple Keynote presentations
- App Version: 0.1.10
- App Website: https://wiki.documentfoundation.org/DLP/Libraries/libetonyek

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libetonyek with the following command
   ```
   brew install libetonyek
   ```
4. libetonyek is ready to use now!