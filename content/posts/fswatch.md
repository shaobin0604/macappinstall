---
title: "Install fswatch on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Monitor a directory for changes and run a shell command"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fswatch on MacOS using homebrew

- App Name: fswatch
- App description: Monitor a directory for changes and run a shell command
- App Version: 1.16.0
- App Website: https://github.com/emcrisostomo/fswatch

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fswatch with the following command
   ```
   brew install fswatch
   ```
4. fswatch is ready to use now!