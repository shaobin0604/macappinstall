---
title: "Install Unity iOS Build Support on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "iOS target support for Unity"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Unity iOS Build Support on MacOS using homebrew

- App Name: Unity iOS Build Support
- App description: iOS target support for Unity
- App Version: 2021.2.11f1,e50cafbb4399
- App Website: https://unity.com/products

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Unity iOS Build Support with the following command
   ```
   brew install --cask unity-ios-support-for-editor
   ```
4. Unity iOS Build Support is ready to use now!