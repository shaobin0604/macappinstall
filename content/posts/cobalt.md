---
title: "Install cobalt on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Static site generator written in Rust"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cobalt on MacOS using homebrew

- App Name: cobalt
- App description: Static site generator written in Rust
- App Version: 0.17.5
- App Website: https://cobalt-org.github.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cobalt with the following command
   ```
   brew install cobalt
   ```
4. cobalt is ready to use now!