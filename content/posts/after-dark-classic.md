---
title: "Install After Dark Classic Set on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install After Dark Classic Set on MacOS using homebrew

- App Name: After Dark Classic Set
- App description: null
- App Version: 1.0
- App Website: https://en.infinisys.co.jp/product/afterdarkclassicset/index.shtml

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install After Dark Classic Set with the following command
   ```
   brew install --cask after-dark-classic
   ```
4. After Dark Classic Set is ready to use now!