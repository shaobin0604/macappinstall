---
title: "Install elixir on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Functional metaprogramming aware language built on Erlang VM"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install elixir on MacOS using homebrew

- App Name: elixir
- App description: Functional metaprogramming aware language built on Erlang VM
- App Version: 1.13.3
- App Website: https://elixir-lang.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install elixir with the following command
   ```
   brew install elixir
   ```
4. elixir is ready to use now!