---
title: "Install net-snmp on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Implements SNMP v1, v2c, and v3, using IPv4 and IPv6"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install net-snmp on MacOS using homebrew

- App Name: net-snmp
- App description: Implements SNMP v1, v2c, and v3, using IPv4 and IPv6
- App Version: 5.9.1
- App Website: http://www.net-snmp.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install net-snmp with the following command
   ```
   brew install net-snmp
   ```
4. net-snmp is ready to use now!