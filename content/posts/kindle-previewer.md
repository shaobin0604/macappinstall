---
title: "Install Kindle Previewer on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Preview and audit Kindle eBooks"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Kindle Previewer on MacOS using homebrew

- App Name: Kindle Previewer
- App description: Preview and audit Kindle eBooks
- App Version: 3.61.0
- App Website: https://www.amazon.com/Kindle-Previewer/b?ie=UTF8&node=21381691011

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Kindle Previewer with the following command
   ```
   brew install --cask kindle-previewer
   ```
4. Kindle Previewer is ready to use now!