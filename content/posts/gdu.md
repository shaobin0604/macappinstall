---
title: "Install gdu on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Disk usage analyzer with console interface written in Go"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gdu on MacOS using homebrew

- App Name: gdu
- App description: Disk usage analyzer with console interface written in Go
- App Version: 5.13.1
- App Website: https://github.com/dundee/gdu

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gdu with the following command
   ```
   brew install gdu
   ```
4. gdu is ready to use now!