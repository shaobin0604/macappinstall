---
title: "Install docker-clean on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Clean Docker containers, images, networks, and volumes"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install docker-clean on MacOS using homebrew

- App Name: docker-clean
- App description: Clean Docker containers, images, networks, and volumes
- App Version: 2.0.4
- App Website: https://github.com/ZZROTDesign/docker-clean

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install docker-clean with the following command
   ```
   brew install docker-clean
   ```
4. docker-clean is ready to use now!