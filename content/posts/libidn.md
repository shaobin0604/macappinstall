---
title: "Install libidn on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "International domain name library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libidn on MacOS using homebrew

- App Name: libidn
- App description: International domain name library
- App Version: 1.38
- App Website: https://www.gnu.org/software/libidn/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libidn with the following command
   ```
   brew install libidn
   ```
4. libidn is ready to use now!