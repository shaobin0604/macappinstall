---
title: "Install alure on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Manage common tasks with OpenAL applications"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install alure on MacOS using homebrew

- App Name: alure
- App description: Manage common tasks with OpenAL applications
- App Version: 1.2
- App Website: https://kcat.tomasu.net/alure.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install alure with the following command
   ```
   brew install alure
   ```
4. alure is ready to use now!