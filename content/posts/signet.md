---
title: "Install Signet on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Scans and checks bundle signatures"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Signet on MacOS using homebrew

- App Name: Signet
- App description: Scans and checks bundle signatures
- App Version: 1.3,2020.09
- App Website: https://eclecticlight.co/taccy-signet-precize-alifix-utiutility-alisma/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Signet with the following command
   ```
   brew install --cask signet
   ```
4. Signet is ready to use now!