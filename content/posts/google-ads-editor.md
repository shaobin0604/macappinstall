---
title: "Install Google Ads Editor on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Google Ads Editor on MacOS using homebrew

- App Name: Google Ads Editor
- App description: null
- App Version: latest
- App Website: https://ads.google.com/home/tools/ads-editor/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Google Ads Editor with the following command
   ```
   brew install --cask google-ads-editor
   ```
4. Google Ads Editor is ready to use now!