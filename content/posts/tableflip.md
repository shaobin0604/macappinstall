---
title: "Install TableFlip on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "App to edit markdown files in place"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install TableFlip on MacOS using homebrew

- App Name: TableFlip
- App description: App to edit markdown files in place
- App Version: 1.3.1,61
- App Website: https://tableflipapp.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install TableFlip with the following command
   ```
   brew install --cask tableflip
   ```
4. TableFlip is ready to use now!