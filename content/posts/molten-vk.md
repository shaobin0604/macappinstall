---
title: "Install molten-vk on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Implementation of the Vulkan graphics and compute API on top of Metal"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install molten-vk on MacOS using homebrew

- App Name: molten-vk
- App description: Implementation of the Vulkan graphics and compute API on top of Metal
- App Version: 1.1.7
- App Website: https://github.com/KhronosGroup/MoltenVK

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install molten-vk with the following command
   ```
   brew install molten-vk
   ```
4. molten-vk is ready to use now!