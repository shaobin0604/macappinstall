---
title: "Install pywhat on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "🐸 Identify anything: emails, IP addresses, and more 🧙"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pywhat on MacOS using homebrew

- App Name: pywhat
- App description: 🐸 Identify anything: emails, IP addresses, and more 🧙
- App Version: 5.1.0
- App Website: https://github.com/bee-san/pyWhat

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pywhat with the following command
   ```
   brew install pywhat
   ```
4. pywhat is ready to use now!