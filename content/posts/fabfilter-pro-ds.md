---
title: "Install FabFilter Pro-DS on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "De-esser plug-in"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install FabFilter Pro-DS on MacOS using homebrew

- App Name: FabFilter Pro-DS
- App description: De-esser plug-in
- App Version: 1.18
- App Website: https://www.fabfilter.com/products/pro-ds-de-esser-plug-in

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install FabFilter Pro-DS with the following command
   ```
   brew install --cask fabfilter-pro-ds
   ```
4. FabFilter Pro-DS is ready to use now!