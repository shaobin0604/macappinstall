---
title: "Install distribution on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Create ASCII graphical histograms in the terminal"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install distribution on MacOS using homebrew

- App Name: distribution
- App description: Create ASCII graphical histograms in the terminal
- App Version: 1.3
- App Website: https://github.com/philovivero/distribution

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install distribution with the following command
   ```
   brew install distribution
   ```
4. distribution is ready to use now!