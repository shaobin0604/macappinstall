---
title: "Install Ableton Live Lite on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Sound and music editor"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Ableton Live Lite on MacOS using homebrew

- App Name: Ableton Live Lite
- App description: Sound and music editor
- App Version: 11.1
- App Website: https://www.ableton.com/en/products/live-lite/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Ableton Live Lite with the following command
   ```
   brew install --cask ableton-live-lite
   ```
4. Ableton Live Lite is ready to use now!