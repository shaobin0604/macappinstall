---
title: "Install graphicsmagick on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Image processing tools collection"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install graphicsmagick on MacOS using homebrew

- App Name: graphicsmagick
- App description: Image processing tools collection
- App Version: 1.3.37
- App Website: http://www.graphicsmagick.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install graphicsmagick with the following command
   ```
   brew install graphicsmagick
   ```
4. graphicsmagick is ready to use now!