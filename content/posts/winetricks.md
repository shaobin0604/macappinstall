---
title: "Install winetricks on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Automatic workarounds for problems in Wine"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install winetricks on MacOS using homebrew

- App Name: winetricks
- App description: Automatic workarounds for problems in Wine
- App Version: 20210825
- App Website: https://github.com/Winetricks/winetricks

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install winetricks with the following command
   ```
   brew install winetricks
   ```
4. winetricks is ready to use now!