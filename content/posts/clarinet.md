---
title: "Install clarinet on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line tool and runtime for the Clarity smart contract language"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install clarinet on MacOS using homebrew

- App Name: clarinet
- App description: Command-line tool and runtime for the Clarity smart contract language
- App Version: 0.26.1
- App Website: https://github.com/hirosystems/clarinet

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install clarinet with the following command
   ```
   brew install clarinet
   ```
4. clarinet is ready to use now!