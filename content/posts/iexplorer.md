---
title: "Install iExplorer on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "iOS device backup software and file manager"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install iExplorer on MacOS using homebrew

- App Name: iExplorer
- App description: iOS device backup software and file manager
- App Version: 4.5.0,178
- App Website: https://macroplant.com/iexplorer

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install iExplorer with the following command
   ```
   brew install --cask iexplorer
   ```
4. iExplorer is ready to use now!