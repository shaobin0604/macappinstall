---
title: "Install saml2aws on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Login and retrieve AWS temporary credentials using a SAML IDP"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install saml2aws on MacOS using homebrew

- App Name: saml2aws
- App description: Login and retrieve AWS temporary credentials using a SAML IDP
- App Version: 2.34.0
- App Website: https://github.com/Versent/saml2aws

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install saml2aws with the following command
   ```
   brew install saml2aws
   ```
4. saml2aws is ready to use now!