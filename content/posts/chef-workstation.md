---
title: "Install Chef Workstation on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "All-in-one installer for the tools you need to manage your Chef infrastructure"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Chef Workstation on MacOS using homebrew

- App Name: Chef Workstation
- App description: All-in-one installer for the tools you need to manage your Chef infrastructure
- App Version: 22.1.778
- App Website: https://docs.chef.io/workstation/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Chef Workstation with the following command
   ```
   brew install --cask chef-workstation
   ```
4. Chef Workstation is ready to use now!