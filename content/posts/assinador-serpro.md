---
title: "Install Assinador Serpro on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Validate and sign documents using digital certificates"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Assinador Serpro on MacOS using homebrew

- App Name: Assinador Serpro
- App description: Validate and sign documents using digital certificates
- App Version: 3.0.1
- App Website: https://www.serpro.gov.br/links-fixos-superiores/assinador-digital/assinador-serpro

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Assinador Serpro with the following command
   ```
   brew install --cask assinador-serpro
   ```
4. Assinador Serpro is ready to use now!