---
title: "Install otx on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Mach-O disassembler"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install otx on MacOS using homebrew

- App Name: otx
- App description: Mach-O disassembler
- App Version: 1.7,b566
- App Website: https://github.com/x43x61x69/otx

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install otx with the following command
   ```
   brew install --cask otx
   ```
4. otx is ready to use now!