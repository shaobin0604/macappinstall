---
title: "Install Affinity Designer on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Professional graphic design software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Affinity Designer on MacOS using homebrew

- App Name: Affinity Designer
- App description: Professional graphic design software
- App Version: 1.10.4
- App Website: https://affinity.serif.com/en-us/designer/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Affinity Designer with the following command
   ```
   brew install --cask affinity-designer
   ```
4. Affinity Designer is ready to use now!