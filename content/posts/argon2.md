---
title: "Install argon2 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Password hashing library and CLI utility"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install argon2 on MacOS using homebrew

- App Name: argon2
- App description: Password hashing library and CLI utility
- App Version: 20190702
- App Website: https://github.com/P-H-C/phc-winner-argon2

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install argon2 with the following command
   ```
   brew install argon2
   ```
4. argon2 is ready to use now!