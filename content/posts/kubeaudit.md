---
title: "Install kubeaudit on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Helps audit your Kubernetes clusters against common security controls"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install kubeaudit on MacOS using homebrew

- App Name: kubeaudit
- App description: Helps audit your Kubernetes clusters against common security controls
- App Version: 0.16.0
- App Website: https://github.com/Shopify/kubeaudit

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install kubeaudit with the following command
   ```
   brew install kubeaudit
   ```
4. kubeaudit is ready to use now!