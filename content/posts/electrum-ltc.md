---
title: "Install Electrum-LTC on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Litecoin wallet"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Electrum-LTC on MacOS using homebrew

- App Name: Electrum-LTC
- App description: Litecoin wallet
- App Version: 4.0.9.3
- App Website: https://electrum-ltc.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Electrum-LTC with the following command
   ```
   brew install --cask electrum-ltc
   ```
4. Electrum-LTC is ready to use now!