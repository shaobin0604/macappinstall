---
title: "Install ssdb on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "NoSQL database supporting many data structures: Redis alternative"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ssdb on MacOS using homebrew

- App Name: ssdb
- App description: NoSQL database supporting many data structures: Redis alternative
- App Version: 1.9.9
- App Website: https://ssdb.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ssdb with the following command
   ```
   brew install ssdb
   ```
4. ssdb is ready to use now!