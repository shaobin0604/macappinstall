---
title: "Install PlayMemories Home on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Freeware that manages and edits photos and videos"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install PlayMemories Home on MacOS using homebrew

- App Name: PlayMemories Home
- App description: Freeware that manages and edits photos and videos
- App Version: 3.6.01,zb2OnHfINu
- App Website: https://support.d-imaging.sony.co.jp/www/disoft/int/download/playmemories-home/mac/en/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install PlayMemories Home with the following command
   ```
   brew install --cask playmemories-home
   ```
4. PlayMemories Home is ready to use now!