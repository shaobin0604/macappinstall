---
title: "Install fuse-emulator on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Free Unix Spectrum Emulator"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fuse-emulator on MacOS using homebrew

- App Name: fuse-emulator
- App description: Free Unix Spectrum Emulator
- App Version: 1.6.0
- App Website: https://fuse-emulator.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fuse-emulator with the following command
   ```
   brew install fuse-emulator
   ```
4. fuse-emulator is ready to use now!