---
title: "Install gedit on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GNOME text editor"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gedit on MacOS using homebrew

- App Name: gedit
- App description: GNOME text editor
- App Version: 40.1
- App Website: https://wiki.gnome.org/Apps/Gedit

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gedit with the following command
   ```
   brew install gedit
   ```
4. gedit is ready to use now!