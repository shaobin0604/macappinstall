---
title: "Install ots on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "🔐 Share end-to-end encrypted secrets with others via a one-time URL"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ots on MacOS using homebrew

- App Name: ots
- App description: 🔐 Share end-to-end encrypted secrets with others via a one-time URL
- App Version: 0.1.0
- App Website: https://ots.sniptt.com

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ots with the following command
   ```
   brew install ots
   ```
4. ots is ready to use now!