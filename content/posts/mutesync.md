---
title: "Install mütesync on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Companion app to the mütesync physical button"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mütesync on MacOS using homebrew

- App Name: mütesync
- App description: Companion app to the mütesync physical button
- App Version: 4.12.5
- App Website: https://mutesync.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mütesync with the following command
   ```
   brew install --cask mutesync
   ```
4. mütesync is ready to use now!