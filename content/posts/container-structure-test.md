---
title: "Install container-structure-test on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Validate the structure of your container images"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install container-structure-test on MacOS using homebrew

- App Name: container-structure-test
- App description: Validate the structure of your container images
- App Version: 1.11.0
- App Website: https://github.com/GoogleContainerTools/container-structure-test

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install container-structure-test with the following command
   ```
   brew install container-structure-test
   ```
4. container-structure-test is ready to use now!