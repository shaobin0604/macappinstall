---
title: "Install LICEcap on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Animated screen capture application"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install LICEcap on MacOS using homebrew

- App Name: LICEcap
- App description: Animated screen capture application
- App Version: 1.31
- App Website: https://www.cockos.com/licecap/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install LICEcap with the following command
   ```
   brew install --cask licecap
   ```
4. LICEcap is ready to use now!