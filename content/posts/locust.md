---
title: "Install locust on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Scalable user load testing tool written in Python"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install locust on MacOS using homebrew

- App Name: locust
- App description: Scalable user load testing tool written in Python
- App Version: 2.8.2
- App Website: https://locust.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install locust with the following command
   ```
   brew install locust
   ```
4. locust is ready to use now!