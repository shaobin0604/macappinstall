---
title: "Install Turbo Boost Switcher on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Enable and disable the Intel CPU Turbo Boost feature"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Turbo Boost Switcher on MacOS using homebrew

- App Name: Turbo Boost Switcher
- App description: Enable and disable the Intel CPU Turbo Boost feature
- App Version: 2.11.0
- App Website: https://www.rugarciap.com/turbo-boost-switcher-for-os-x/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Turbo Boost Switcher with the following command
   ```
   brew install --cask turbo-boost-switcher
   ```
4. Turbo Boost Switcher is ready to use now!