---
title: "Install envv on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Shell-independent handling of environment variables"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install envv on MacOS using homebrew

- App Name: envv
- App description: Shell-independent handling of environment variables
- App Version: 1.7
- App Website: https://github.com/jakewendt/envv#readme

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install envv with the following command
   ```
   brew install envv
   ```
4. envv is ready to use now!