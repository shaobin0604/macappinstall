---
title: "Install randomize-lines on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Reads and randomize lines from a file (or STDIN)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install randomize-lines on MacOS using homebrew

- App Name: randomize-lines
- App description: Reads and randomize lines from a file (or STDIN)
- App Version: 0.2.7
- App Website: https://arthurdejong.org/rl/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install randomize-lines with the following command
   ```
   brew install randomize-lines
   ```
4. randomize-lines is ready to use now!