---
title: "Install bootloadhid on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "HID-based USB bootloader for AVR microcontrollers"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bootloadhid on MacOS using homebrew

- App Name: bootloadhid
- App description: HID-based USB bootloader for AVR microcontrollers
- App Version: 2012-12-08
- App Website: https://www.obdev.at/products/vusb/bootloadhid.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bootloadhid with the following command
   ```
   brew install bootloadhid
   ```
4. bootloadhid is ready to use now!