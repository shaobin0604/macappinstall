---
title: "Install ProfileCreator on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Create standard or customized configuration profiles"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ProfileCreator on MacOS using homebrew

- App Name: ProfileCreator
- App description: Create standard or customized configuration profiles
- App Version: 0.3.2,201907171032-beta
- App Website: https://github.com/erikberglund/ProfileCreator

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ProfileCreator with the following command
   ```
   brew install --cask profilecreator
   ```
4. ProfileCreator is ready to use now!