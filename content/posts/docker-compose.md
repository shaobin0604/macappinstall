---
title: "Install docker-compose on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Isolated development environments using Docker"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install docker-compose on MacOS using homebrew

- App Name: docker-compose
- App description: Isolated development environments using Docker
- App Version: 2.2.3
- App Website: https://docs.docker.com/compose/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install docker-compose with the following command
   ```
   brew install docker-compose
   ```
4. docker-compose is ready to use now!