---
title: "Install Plug on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Music player for The Hype Machine"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Plug on MacOS using homebrew

- App Name: Plug
- App description: Music player for The Hype Machine
- App Version: 2.0.19,2067
- App Website: https://www.plugformac.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Plug with the following command
   ```
   brew install --cask plug
   ```
4. Plug is ready to use now!