---
title: "Install sproxy on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "HTTP proxy server collecting URLs in a 'siege-friendly' manner"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sproxy on MacOS using homebrew

- App Name: sproxy
- App description: HTTP proxy server collecting URLs in a 'siege-friendly' manner
- App Version: 1.02
- App Website: https://www.joedog.org/sproxy-home/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sproxy with the following command
   ```
   brew install sproxy
   ```
4. sproxy is ready to use now!