---
title: "Install postgis on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Adds support for geographic objects to PostgreSQL"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install postgis on MacOS using homebrew

- App Name: postgis
- App description: Adds support for geographic objects to PostgreSQL
- App Version: 3.2.1
- App Website: https://postgis.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install postgis with the following command
   ```
   brew install postgis
   ```
4. postgis is ready to use now!