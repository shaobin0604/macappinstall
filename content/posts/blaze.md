---
title: "Install blaze on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "High-performance C++ math library for dense and sparse arithmetic"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install blaze on MacOS using homebrew

- App Name: blaze
- App description: High-performance C++ math library for dense and sparse arithmetic
- App Version: 3.8
- App Website: https://bitbucket.org/blaze-lib/blaze

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install blaze with the following command
   ```
   brew install blaze
   ```
4. blaze is ready to use now!