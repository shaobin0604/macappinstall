---
title: "Install mysqltuner on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Increase performance and stability of a MySQL installation"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mysqltuner on MacOS using homebrew

- App Name: mysqltuner
- App description: Increase performance and stability of a MySQL installation
- App Version: 1.8.3
- App Website: https://raw.github.com/major/MySQLTuner-perl/master/mysqltuner.pl

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mysqltuner with the following command
   ```
   brew install mysqltuner
   ```
4. mysqltuner is ready to use now!