---
title: "Install esptool on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "ESP8266 and ESP32 serial bootloader utility"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install esptool on MacOS using homebrew

- App Name: esptool
- App description: ESP8266 and ESP32 serial bootloader utility
- App Version: 3.2
- App Website: https://github.com/espressif/esptool

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install esptool with the following command
   ```
   brew install esptool
   ```
4. esptool is ready to use now!