---
title: "Install emacs on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GNU Emacs text editor"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install emacs on MacOS using homebrew

- App Name: emacs
- App description: GNU Emacs text editor
- App Version: 27.2
- App Website: https://www.gnu.org/software/emacs/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install emacs with the following command
   ```
   brew install emacs
   ```
4. emacs is ready to use now!