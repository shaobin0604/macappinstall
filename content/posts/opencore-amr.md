---
title: "Install opencore-amr on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Audio codecs extracted from Android open source project"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install opencore-amr on MacOS using homebrew

- App Name: opencore-amr
- App description: Audio codecs extracted from Android open source project
- App Version: 0.1.5
- App Website: https://opencore-amr.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install opencore-amr with the following command
   ```
   brew install opencore-amr
   ```
4. opencore-amr is ready to use now!