---
title: "Install cattle on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Brainfuck language toolkit"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cattle on MacOS using homebrew

- App Name: cattle
- App description: Brainfuck language toolkit
- App Version: 1.4.0
- App Website: https://kiyuko.org/software/cattle

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cattle with the following command
   ```
   brew install cattle
   ```
4. cattle is ready to use now!