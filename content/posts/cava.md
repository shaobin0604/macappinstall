---
title: "Install cava on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Console-based Audio Visualizer for ALSA"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cava on MacOS using homebrew

- App Name: cava
- App description: Console-based Audio Visualizer for ALSA
- App Version: 0.7.5
- App Website: https://github.com/karlstav/cava

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cava with the following command
   ```
   brew install cava
   ```
4. cava is ready to use now!