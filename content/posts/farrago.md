---
title: "Install Farrago on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Audio playback"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Farrago on MacOS using homebrew

- App Name: Farrago
- App description: Audio playback
- App Version: 1.6.5
- App Website: https://rogueamoeba.com/farrago/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Farrago with the following command
   ```
   brew install --cask farrago
   ```
4. Farrago is ready to use now!