---
title: "Install iCloud Control on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "User-controlled selective sync for iCloud Drive"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install iCloud Control on MacOS using homebrew

- App Name: iCloud Control
- App description: User-controlled selective sync for iCloud Drive
- App Version: 1.2.0
- App Website: https://github.com/Obbut/iCloud-Control

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install iCloud Control with the following command
   ```
   brew install --cask icloud-control
   ```
4. iCloud Control is ready to use now!