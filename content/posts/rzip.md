---
title: "Install rzip on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "File compression tool (like gzip or bzip2)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install rzip on MacOS using homebrew

- App Name: rzip
- App description: File compression tool (like gzip or bzip2)
- App Version: 2.1
- App Website: https://rzip.samba.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install rzip with the following command
   ```
   brew install rzip
   ```
4. rzip is ready to use now!