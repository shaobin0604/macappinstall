---
title: "Install sparkey on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Constant key-value store, best for frequent read/infrequent write uses"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sparkey on MacOS using homebrew

- App Name: sparkey
- App description: Constant key-value store, best for frequent read/infrequent write uses
- App Version: 1.0.0
- App Website: https://github.com/spotify/sparkey/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sparkey with the following command
   ```
   brew install sparkey
   ```
4. sparkey is ready to use now!