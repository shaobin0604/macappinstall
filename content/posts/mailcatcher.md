---
title: "Install mailcatcher on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Catches mail and serves it through a dream"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mailcatcher on MacOS using homebrew

- App Name: mailcatcher
- App description: Catches mail and serves it through a dream
- App Version: 0.8.1
- App Website: https://mailcatcher.me

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mailcatcher with the following command
   ```
   brew install mailcatcher
   ```
4. mailcatcher is ready to use now!