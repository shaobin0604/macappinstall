---
title: "Install mysql-connector-c++ on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "MySQL database connector for C++ applications"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mysql-connector-c++ on MacOS using homebrew

- App Name: mysql-connector-c++
- App description: MySQL database connector for C++ applications
- App Version: 8.0.28
- App Website: https://dev.mysql.com/downloads/connector/cpp/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mysql-connector-c++ with the following command
   ```
   brew install mysql-connector-c++
   ```
4. mysql-connector-c++ is ready to use now!