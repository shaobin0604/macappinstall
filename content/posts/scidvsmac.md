---
title: "Install Scid vs. Mac on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Chess toolkit"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Scid vs. Mac on MacOS using homebrew

- App Name: Scid vs. Mac
- App description: Chess toolkit
- App Version: 4.22
- App Website: http://scidvspc.sourceforge.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Scid vs. Mac with the following command
   ```
   brew install --cask scidvsmac
   ```
4. Scid vs. Mac is ready to use now!