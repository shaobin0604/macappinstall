---
title: "Install Yuna on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Anime player and list manager"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Yuna on MacOS using homebrew

- App Name: Yuna
- App description: Anime player and list manager
- App Version: 1.4.23
- App Website: https://yuna.moe/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Yuna with the following command
   ```
   brew install --cask yuna
   ```
4. Yuna is ready to use now!