---
title: "Install xsimd on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Modern, portable C++ wrappers for SIMD intrinsics"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install xsimd on MacOS using homebrew

- App Name: xsimd
- App description: Modern, portable C++ wrappers for SIMD intrinsics
- App Version: 8.0.5
- App Website: https://xsimd.readthedocs.io/en/latest/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install xsimd with the following command
   ```
   brew install xsimd
   ```
4. xsimd is ready to use now!