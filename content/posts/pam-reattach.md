---
title: "Install pam-reattach on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "PAM module for reattaching to the user's GUI (Aqua) session"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pam-reattach on MacOS using homebrew

- App Name: pam-reattach
- App description: PAM module for reattaching to the user's GUI (Aqua) session
- App Version: 1.2
- App Website: https://github.com/fabianishere/pam_reattach

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pam-reattach with the following command
   ```
   brew install pam-reattach
   ```
4. pam-reattach is ready to use now!