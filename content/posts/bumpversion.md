---
title: "Install bumpversion on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Increase version numbers with SemVer terms"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bumpversion on MacOS using homebrew

- App Name: bumpversion
- App description: Increase version numbers with SemVer terms
- App Version: 1.0.1
- App Website: https://pypi.python.org/pypi/bumpversion

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bumpversion with the following command
   ```
   brew install bumpversion
   ```
4. bumpversion is ready to use now!