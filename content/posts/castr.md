---
title: "Install castr on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Desktop application for controlling Castr streaming platform"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install castr on MacOS using homebrew

- App Name: castr
- App description: Desktop application for controlling Castr streaming platform
- App Version: 1.0.0
- App Website: https://castr.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install castr with the following command
   ```
   brew install --cask castr
   ```
4. castr is ready to use now!