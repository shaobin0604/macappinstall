---
title: "Install boot-clj on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Build tooling for Clojure"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install boot-clj on MacOS using homebrew

- App Name: boot-clj
- App description: Build tooling for Clojure
- App Version: 2.8.3
- App Website: https://boot-clj.github.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install boot-clj with the following command
   ```
   brew install boot-clj
   ```
4. boot-clj is ready to use now!