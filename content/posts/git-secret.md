---
title: "Install git-secret on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Bash-tool to store the private data inside a git repo"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install git-secret on MacOS using homebrew

- App Name: git-secret
- App description: Bash-tool to store the private data inside a git repo
- App Version: 0.4.0
- App Website: https://git-secret.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install git-secret with the following command
   ```
   brew install git-secret
   ```
4. git-secret is ready to use now!