---
title: "Install GoldenPassport on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Native implementation of Google Authenticator based on Swift3"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install GoldenPassport on MacOS using homebrew

- App Name: GoldenPassport
- App description: Native implementation of Google Authenticator based on Swift3
- App Version: 0.1.6
- App Website: https://github.com/stanzhai/GoldenPassport

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install GoldenPassport with the following command
   ```
   brew install --cask goldenpassport
   ```
4. GoldenPassport is ready to use now!