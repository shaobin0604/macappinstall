---
title: "Install markdown on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Text-to-HTML conversion tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install markdown on MacOS using homebrew

- App Name: markdown
- App description: Text-to-HTML conversion tool
- App Version: 1.0.1
- App Website: https://daringfireball.net/projects/markdown/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install markdown with the following command
   ```
   brew install markdown
   ```
4. markdown is ready to use now!