---
title: "Install Mobirise on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "No-code website creator"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Mobirise on MacOS using homebrew

- App Name: Mobirise
- App description: No-code website creator
- App Version: 5.4.0
- App Website: https://mobirise.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Mobirise with the following command
   ```
   brew install --cask mobirise
   ```
4. Mobirise is ready to use now!