---
title: "Install Yate on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Media file tag editor"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Yate on MacOS using homebrew

- App Name: Yate
- App description: Media file tag editor
- App Version: 6.8.1.1,10808
- App Website: https://2manyrobots.com/yate/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Yate with the following command
   ```
   brew install --cask yate
   ```
4. Yate is ready to use now!