---
title: "Install clusterctl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Home for the Cluster Management API work, a subproject of sig-cluster-lifecycle"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install clusterctl on MacOS using homebrew

- App Name: clusterctl
- App description: Home for the Cluster Management API work, a subproject of sig-cluster-lifecycle
- App Version: 1.1.1
- App Website: https://cluster-api.sigs.k8s.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install clusterctl with the following command
   ```
   brew install clusterctl
   ```
4. clusterctl is ready to use now!