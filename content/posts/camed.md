---
title: "Install CAM Editor on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "XML editor"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install CAM Editor on MacOS using homebrew

- App Name: CAM Editor
- App description: XML editor
- App Version: 3.2.2
- App Website: https://camprocessor.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install CAM Editor with the following command
   ```
   brew install --cask camed
   ```
4. CAM Editor is ready to use now!