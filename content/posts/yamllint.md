---
title: "Install yamllint on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Linter for YAML files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install yamllint on MacOS using homebrew

- App Name: yamllint
- App description: Linter for YAML files
- App Version: 1.26.3
- App Website: https://github.com/adrienverge/yamllint

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install yamllint with the following command
   ```
   brew install yamllint
   ```
4. yamllint is ready to use now!