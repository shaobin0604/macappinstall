---
title: "Install crystal-icr on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Interactive console for Crystal programming language"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install crystal-icr on MacOS using homebrew

- App Name: crystal-icr
- App description: Interactive console for Crystal programming language
- App Version: 0.9.0
- App Website: https://github.com/crystal-community/icr

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install crystal-icr with the following command
   ```
   brew install crystal-icr
   ```
4. crystal-icr is ready to use now!