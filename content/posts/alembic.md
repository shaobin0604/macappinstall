---
title: "Install alembic on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Open computer graphics interchange framework"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install alembic on MacOS using homebrew

- App Name: alembic
- App description: Open computer graphics interchange framework
- App Version: 1.8.3
- App Website: http://alembic.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install alembic with the following command
   ```
   brew install alembic
   ```
4. alembic is ready to use now!