---
title: "Install Insomnia on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "HTTP and GraphQL Client"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Insomnia on MacOS using homebrew

- App Name: Insomnia
- App description: HTTP and GraphQL Client
- App Version: 2021.7.2
- App Website: https://insomnia.rest/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Insomnia with the following command
   ```
   brew install --cask insomnia
   ```
4. Insomnia is ready to use now!