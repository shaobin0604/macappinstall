---
title: "Install Twobird on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Email client with collaborative notes"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Twobird on MacOS using homebrew

- App Name: Twobird
- App description: Email client with collaborative notes
- App Version: 1.0.46
- App Website: https://www.twobird.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Twobird with the following command
   ```
   brew install --cask twobird
   ```
4. Twobird is ready to use now!