---
title: "Install InVision Sync on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install InVision Sync on MacOS using homebrew

- App Name: InVision Sync
- App description: null
- App Version: 1.9.1,692
- App Website: https://www.invisionapp.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install InVision Sync with the following command
   ```
   brew install --cask invisionsync
   ```
4. InVision Sync is ready to use now!