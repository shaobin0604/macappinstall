---
title: "Install gearman on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Application framework to farm out work to other machines or processes"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gearman on MacOS using homebrew

- App Name: gearman
- App description: Application framework to farm out work to other machines or processes
- App Version: 1.1.19.1
- App Website: http://gearman.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gearman with the following command
   ```
   brew install gearman
   ```
4. gearman is ready to use now!