---
title: "Install Plot Digitizer on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Digitize scanned plots of functional data"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Plot Digitizer on MacOS using homebrew

- App Name: Plot Digitizer
- App description: Digitize scanned plots of functional data
- App Version: 2.6.9
- App Website: https://plotdigitizer.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Plot Digitizer with the following command
   ```
   brew install --cask plotdigitizer
   ```
4. Plot Digitizer is ready to use now!