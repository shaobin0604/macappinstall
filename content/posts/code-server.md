---
title: "Install code-server on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Access VS Code through the browser"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install code-server on MacOS using homebrew

- App Name: code-server
- App description: Access VS Code through the browser
- App Version: 4.0.2
- App Website: https://github.com/cdr/code-server

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install code-server with the following command
   ```
   brew install code-server
   ```
4. code-server is ready to use now!