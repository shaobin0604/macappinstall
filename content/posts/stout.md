---
title: "Install stout on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Reliable static website deploy tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install stout on MacOS using homebrew

- App Name: stout
- App description: Reliable static website deploy tool
- App Version: 1.3.2
- App Website: https://github.com/cloudflare/Stout

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install stout with the following command
   ```
   brew install stout
   ```
4. stout is ready to use now!