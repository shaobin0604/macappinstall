---
title: "Install cppzmq on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Header-only C++ binding for libzmq"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cppzmq on MacOS using homebrew

- App Name: cppzmq
- App description: Header-only C++ binding for libzmq
- App Version: 4.8.1
- App Website: https://www.zeromq.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cppzmq with the following command
   ```
   brew install cppzmq
   ```
4. cppzmq is ready to use now!