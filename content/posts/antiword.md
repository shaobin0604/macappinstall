---
title: "Install antiword on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utility to read Word (.doc) files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install antiword on MacOS using homebrew

- App Name: antiword
- App description: Utility to read Word (.doc) files
- App Version: 0.37
- App Website: http://www.winfield.demon.nl/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install antiword with the following command
   ```
   brew install antiword
   ```
4. antiword is ready to use now!