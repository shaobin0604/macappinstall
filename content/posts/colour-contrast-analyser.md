---
title: "Install Colour Contrast Analyser (CCA) on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Color contrast checker"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Colour Contrast Analyser (CCA) on MacOS using homebrew

- App Name: Colour Contrast Analyser (CCA)
- App description: Color contrast checker
- App Version: 3.1.4
- App Website: https://developer.paciellogroup.com/resources/contrastanalyser/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Colour Contrast Analyser (CCA) with the following command
   ```
   brew install --cask colour-contrast-analyser
   ```
4. Colour Contrast Analyser (CCA) is ready to use now!