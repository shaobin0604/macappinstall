---
title: "Install git-lfs on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Git extension for versioning large files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install git-lfs on MacOS using homebrew

- App Name: git-lfs
- App description: Git extension for versioning large files
- App Version: 3.1.2
- App Website: https://github.com/git-lfs/git-lfs

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install git-lfs with the following command
   ```
   brew install git-lfs
   ```
4. git-lfs is ready to use now!