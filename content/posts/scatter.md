---
title: "Install Scatter on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Desktop wallet for EOS"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Scatter on MacOS using homebrew

- App Name: Scatter
- App description: Desktop wallet for EOS
- App Version: 12.1.1
- App Website: https://get-scatter.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Scatter with the following command
   ```
   brew install --cask scatter
   ```
4. Scatter is ready to use now!