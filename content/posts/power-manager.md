---
title: "Install Power Manager on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utility to automate tasks and improve power management"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Power Manager on MacOS using homebrew

- App Name: Power Manager
- App description: Utility to automate tasks and improve power management
- App Version: 5.9.2
- App Website: https://dssw.co.uk/powermanager/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Power Manager with the following command
   ```
   brew install --cask power-manager
   ```
4. Power Manager is ready to use now!