---
title: "Install FileMaker Pro on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Relational database and rapid application development platform"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install FileMaker Pro on MacOS using homebrew

- App Name: FileMaker Pro
- App description: Relational database and rapid application development platform
- App Version: 19.4.2.204
- App Website: https://www.claris.com/filemaker/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install FileMaker Pro with the following command
   ```
   brew install --cask filemaker-pro
   ```
4. FileMaker Pro is ready to use now!