---
title: "Install emojify on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Emoji on the command-line :scream:"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install emojify on MacOS using homebrew

- App Name: emojify
- App description: Emoji on the command-line :scream:
- App Version: 1.0.2
- App Website: https://github.com/mrowa44/emojify

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install emojify with the following command
   ```
   brew install emojify
   ```
4. emojify is ready to use now!