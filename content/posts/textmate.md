---
title: "Install TextMate on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "General-purpose text editor"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install TextMate on MacOS using homebrew

- App Name: TextMate
- App description: General-purpose text editor
- App Version: 2.0.23
- App Website: https://macromates.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install TextMate with the following command
   ```
   brew install --cask textmate
   ```
4. TextMate is ready to use now!