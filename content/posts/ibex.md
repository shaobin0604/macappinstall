---
title: "Install ibex on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C++ library for constraint processing over real numbers"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ibex on MacOS using homebrew

- App Name: ibex
- App description: C++ library for constraint processing over real numbers
- App Version: 2.8.9
- App Website: https://web.archive.org/web/20190826220512/www.ibex-lib.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ibex with the following command
   ```
   brew install ibex
   ```
4. ibex is ready to use now!