---
title: "Install ghostunnel on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Simple SSL/TLS proxy with mutual authentication"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ghostunnel on MacOS using homebrew

- App Name: ghostunnel
- App description: Simple SSL/TLS proxy with mutual authentication
- App Version: 1.6.0
- App Website: https://github.com/ghostunnel/ghostunnel

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ghostunnel with the following command
   ```
   brew install ghostunnel
   ```
4. ghostunnel is ready to use now!