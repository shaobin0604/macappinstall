---
title: "Install xboard on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Graphical user interface for chess"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install xboard on MacOS using homebrew

- App Name: xboard
- App description: Graphical user interface for chess
- App Version: 4.9.1
- App Website: https://www.gnu.org/software/xboard/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install xboard with the following command
   ```
   brew install xboard
   ```
4. xboard is ready to use now!