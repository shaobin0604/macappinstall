---
title: "Install Binary Ninja on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Reverse engineering platform"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Binary Ninja on MacOS using homebrew

- App Name: Binary Ninja
- App description: Reverse engineering platform
- App Version: 2.2.2487
- App Website: https://binary.ninja/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Binary Ninja with the following command
   ```
   brew install --cask binary-ninja
   ```
4. Binary Ninja is ready to use now!