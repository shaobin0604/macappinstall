---
title: "Install xxhash on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Extremely fast non-cryptographic hash algorithm"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install xxhash on MacOS using homebrew

- App Name: xxhash
- App description: Extremely fast non-cryptographic hash algorithm
- App Version: 0.8.1
- App Website: https://github.com/Cyan4973/xxHash

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install xxhash with the following command
   ```
   brew install xxhash
   ```
4. xxhash is ready to use now!