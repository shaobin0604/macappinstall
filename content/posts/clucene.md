---
title: "Install clucene on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C++ port of Lucene: high-performance, full-featured text search engine"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install clucene on MacOS using homebrew

- App Name: clucene
- App description: C++ port of Lucene: high-performance, full-featured text search engine
- App Version: 2.3.3.4
- App Website: https://clucene.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install clucene with the following command
   ```
   brew install clucene
   ```
4. clucene is ready to use now!