---
title: "Install ccls on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C/C++/ObjC language server"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ccls on MacOS using homebrew

- App Name: ccls
- App description: C/C++/ObjC language server
- App Version: 0.20210330
- App Website: https://github.com/MaskRay/ccls

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ccls with the following command
   ```
   brew install ccls
   ```
4. ccls is ready to use now!