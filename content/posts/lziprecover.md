---
title: "Install lziprecover on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Data recovery tool and decompressor for files in the lzip compressed data format"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install lziprecover on MacOS using homebrew

- App Name: lziprecover
- App description: Data recovery tool and decompressor for files in the lzip compressed data format
- App Version: 1.23
- App Website: https://www.nongnu.org/lzip/lziprecover.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install lziprecover with the following command
   ```
   brew install lziprecover
   ```
4. lziprecover is ready to use now!