---
title: "Install PingPlotter on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Network monitoring tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install PingPlotter on MacOS using homebrew

- App Name: PingPlotter
- App description: Network monitoring tool
- App Version: 5.22.3
- App Website: https://www.pingplotter.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install PingPlotter with the following command
   ```
   brew install --cask pingplotter
   ```
4. PingPlotter is ready to use now!