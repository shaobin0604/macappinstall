---
title: "Install Cốc Cốc on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Cốc Cốc on MacOS using homebrew

- App Name: Cốc Cốc
- App description: null
- App Version: 87.0.4280.148,87.0.148
- App Website: https://coccoc.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Cốc Cốc with the following command
   ```
   brew install --cask coccoc
   ```
4. Cốc Cốc is ready to use now!