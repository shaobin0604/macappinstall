---
title: "Install tvnamer on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Automatic TV episode file renamer that uses data from thetvdb.com"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tvnamer on MacOS using homebrew

- App Name: tvnamer
- App description: Automatic TV episode file renamer that uses data from thetvdb.com
- App Version: 3.0.4
- App Website: https://github.com/dbr/tvnamer

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tvnamer with the following command
   ```
   brew install tvnamer
   ```
4. tvnamer is ready to use now!