---
title: "Install Invisor Lite on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Media file inspector"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Invisor Lite on MacOS using homebrew

- App Name: Invisor Lite
- App description: Media file inspector
- App Version: 3.17,989.211020
- App Website: https://www.invisorapp.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Invisor Lite with the following command
   ```
   brew install --cask invisor-lite
   ```
4. Invisor Lite is ready to use now!