---
title: "Install mockolo on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Efficient Mock Generator for Swift"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mockolo on MacOS using homebrew

- App Name: mockolo
- App description: Efficient Mock Generator for Swift
- App Version: 1.6.3
- App Website: https://github.com/uber/mockolo

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mockolo with the following command
   ```
   brew install mockolo
   ```
4. mockolo is ready to use now!