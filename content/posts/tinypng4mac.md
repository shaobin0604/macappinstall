---
title: "Install TinyPNG4Mac on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "TinyPNG client"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install TinyPNG4Mac on MacOS using homebrew

- App Name: TinyPNG4Mac
- App description: TinyPNG client
- App Version: 1.0.5
- App Website: https://github.com/kyleduo/TinyPNG4Mac

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install TinyPNG4Mac with the following command
   ```
   brew install --cask tinypng4mac
   ```
4. TinyPNG4Mac is ready to use now!