---
title: "Install Rclone Browser on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Rclone Browser on MacOS using homebrew

- App Name: Rclone Browser
- App description: null
- App Version: 1.2,a1156a0
- App Website: https://mmozeiko.github.io/RcloneBrowser/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Rclone Browser with the following command
   ```
   brew install --cask rclone-browser
   ```
4. Rclone Browser is ready to use now!