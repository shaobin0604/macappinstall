---
title: "Install libffi on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Portable Foreign Function Interface library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libffi on MacOS using homebrew

- App Name: libffi
- App description: Portable Foreign Function Interface library
- App Version: 3.4.2
- App Website: https://sourceware.org/libffi/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libffi with the following command
   ```
   brew install libffi
   ```
4. libffi is ready to use now!