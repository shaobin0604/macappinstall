---
title: "Install triton on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Joyent Triton CLI"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install triton on MacOS using homebrew

- App Name: triton
- App description: Joyent Triton CLI
- App Version: 7.15.1
- App Website: https://www.npmjs.com/package/triton

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install triton with the following command
   ```
   brew install triton
   ```
4. triton is ready to use now!