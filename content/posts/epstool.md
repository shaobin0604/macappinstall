---
title: "Install epstool on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Edit preview images and fix bounding boxes in EPS files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install epstool on MacOS using homebrew

- App Name: epstool
- App description: Edit preview images and fix bounding boxes in EPS files
- App Version: 3.09
- App Website: http://www.ghostgum.com.au/software/epstool.htm

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install epstool with the following command
   ```
   brew install epstool
   ```
4. epstool is ready to use now!