---
title: "Install teensy_loader_cli on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line integration for Teensy USB development boards"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install teensy_loader_cli on MacOS using homebrew

- App Name: teensy_loader_cli
- App description: Command-line integration for Teensy USB development boards
- App Version: 2.2
- App Website: https://www.pjrc.com/teensy/loader_cli.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install teensy_loader_cli with the following command
   ```
   brew install teensy_loader_cli
   ```
4. teensy_loader_cli is ready to use now!