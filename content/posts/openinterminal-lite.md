---
title: "Install OpenInTerminal-Lite on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Finder Toolbar app to open the current directory in Terminal"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install OpenInTerminal-Lite on MacOS using homebrew

- App Name: OpenInTerminal-Lite
- App description: Finder Toolbar app to open the current directory in Terminal
- App Version: 1.2.4
- App Website: https://github.com/Ji4n1ng/OpenInTerminal

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install OpenInTerminal-Lite with the following command
   ```
   brew install --cask openinterminal-lite
   ```
4. OpenInTerminal-Lite is ready to use now!