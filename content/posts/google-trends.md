---
title: "Install Google Trends Screensaver on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Google Trends Screensaver on MacOS using homebrew

- App Name: Google Trends Screensaver
- App description: null
- App Version: latest
- App Website: https://trends.google.com/trends/hottrends/visualize

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Google Trends Screensaver with the following command
   ```
   brew install --cask google-trends
   ```
4. Google Trends Screensaver is ready to use now!