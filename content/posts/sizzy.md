---
title: "Install Sizzy on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to simulate responsive designs on multiple devices"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Sizzy on MacOS using homebrew

- App Name: Sizzy
- App description: Tool to simulate responsive designs on multiple devices
- App Version: 55.2.0
- App Website: https://sizzy.co/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Sizzy with the following command
   ```
   brew install --cask sizzy
   ```
4. Sizzy is ready to use now!