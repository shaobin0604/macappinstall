---
title: "Install texlab on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Implementation of the Language Server Protocol for LaTeX"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install texlab on MacOS using homebrew

- App Name: texlab
- App description: Implementation of the Language Server Protocol for LaTeX
- App Version: 3.3.1
- App Website: https://texlab.netlify.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install texlab with the following command
   ```
   brew install texlab
   ```
4. texlab is ready to use now!