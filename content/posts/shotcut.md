---
title: "Install Shotcut on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Video editor"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Shotcut on MacOS using homebrew

- App Name: Shotcut
- App description: Video editor
- App Version: 22.01.30
- App Website: https://www.shotcut.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Shotcut with the following command
   ```
   brew install --cask shotcut
   ```
4. Shotcut is ready to use now!