---
title: "Install ngs on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Powerful programming language and shell designed specifically for Ops"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ngs on MacOS using homebrew

- App Name: ngs
- App description: Powerful programming language and shell designed specifically for Ops
- App Version: 0.2.13
- App Website: https://ngs-lang.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ngs with the following command
   ```
   brew install ngs
   ```
4. ngs is ready to use now!