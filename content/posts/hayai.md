---
title: "Install hayai on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C++ benchmarking framework inspired by the googletest framework"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install hayai on MacOS using homebrew

- App Name: hayai
- App description: C++ benchmarking framework inspired by the googletest framework
- App Version: 1.0.2
- App Website: https://bruun.co/2012/02/07/easy-cpp-benchmarking

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install hayai with the following command
   ```
   brew install hayai
   ```
4. hayai is ready to use now!