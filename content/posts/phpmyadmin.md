---
title: "Install phpmyadmin on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Web interface for MySQL and MariaDB"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install phpmyadmin on MacOS using homebrew

- App Name: phpmyadmin
- App description: Web interface for MySQL and MariaDB
- App Version: 5.1.3
- App Website: https://www.phpmyadmin.net

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install phpmyadmin with the following command
   ```
   brew install phpmyadmin
   ```
4. phpmyadmin is ready to use now!