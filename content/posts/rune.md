---
title: "Install rune on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Embeddable dynamic programming language for Rust"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install rune on MacOS using homebrew

- App Name: rune
- App description: Embeddable dynamic programming language for Rust
- App Version: 0.10.3
- App Website: https://rune-rs.github.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install rune with the following command
   ```
   brew install rune
   ```
4. rune is ready to use now!