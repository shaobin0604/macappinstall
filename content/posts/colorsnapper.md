---
title: "Install ColorSnapper 2 on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Color picking application"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ColorSnapper 2 on MacOS using homebrew

- App Name: ColorSnapper 2
- App description: Color picking application
- App Version: 1.6.4
- App Website: https://colorsnapper.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ColorSnapper 2 with the following command
   ```
   brew install --cask colorsnapper
   ```
4. ColorSnapper 2 is ready to use now!