---
title: "Install lzlib on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Data compression library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install lzlib on MacOS using homebrew

- App Name: lzlib
- App description: Data compression library
- App Version: 1.13
- App Website: https://www.nongnu.org/lzip/lzlib.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install lzlib with the following command
   ```
   brew install lzlib
   ```
4. lzlib is ready to use now!