---
title: "Install passpie on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Manage login credentials from the terminal"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install passpie on MacOS using homebrew

- App Name: passpie
- App description: Manage login credentials from the terminal
- App Version: 1.6.1
- App Website: https://github.com/marcwebbie/passpie

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install passpie with the following command
   ```
   brew install passpie
   ```
4. passpie is ready to use now!