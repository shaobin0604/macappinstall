---
title: "Install libzip on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C library for reading, creating, and modifying zip archives"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libzip on MacOS using homebrew

- App Name: libzip
- App description: C library for reading, creating, and modifying zip archives
- App Version: 1.8.0
- App Website: https://libzip.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libzip with the following command
   ```
   brew install libzip
   ```
4. libzip is ready to use now!