---
title: "Install euler-py on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Project Euler command-line tool written in Python"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install euler-py on MacOS using homebrew

- App Name: euler-py
- App description: Project Euler command-line tool written in Python
- App Version: 1.4.0
- App Website: https://github.com/iKevinY/EulerPy

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install euler-py with the following command
   ```
   brew install euler-py
   ```
4. euler-py is ready to use now!