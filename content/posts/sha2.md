---
title: "Install sha2 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Implementation of SHA-256, SHA-384, and SHA-512 hash algorithms"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sha2 on MacOS using homebrew

- App Name: sha2
- App description: Implementation of SHA-256, SHA-384, and SHA-512 hash algorithms
- App Version: 1.0.1
- App Website: https://aarongifford.com/computers/sha.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sha2 with the following command
   ```
   brew install sha2
   ```
4. sha2 is ready to use now!