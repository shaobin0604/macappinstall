---
title: "Install Memory-Map on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GPS navigation software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Memory-Map on MacOS using homebrew

- App Name: Memory-Map
- App description: GPS navigation software
- App Version: 1.7.1-18
- App Website: https://memory-map.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Memory-Map with the following command
   ```
   brew install --cask memory-map
   ```
4. Memory-Map is ready to use now!