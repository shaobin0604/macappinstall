---
title: "Install openexr@2 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "High dynamic-range image file format"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install openexr@2 on MacOS using homebrew

- App Name: openexr@2
- App description: High dynamic-range image file format
- App Version: 2.5.7
- App Website: https://www.openexr.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install openexr@2 with the following command
   ```
   brew install openexr@2
   ```
4. openexr@2 is ready to use now!