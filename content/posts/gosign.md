---
title: "Install GoSign Desktop on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Digital signature and time stamp app"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install GoSign Desktop on MacOS using homebrew

- App Name: GoSign Desktop
- App description: Digital signature and time stamp app
- App Version: 1.2.0
- App Website: https://www.firma.infocert.it/prodotti/gosign.php

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install GoSign Desktop with the following command
   ```
   brew install --cask gosign
   ```
4. GoSign Desktop is ready to use now!