---
title: "Install monitoring-plugins on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Plugins for nagios compatible monitoring systems"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install monitoring-plugins on MacOS using homebrew

- App Name: monitoring-plugins
- App description: Plugins for nagios compatible monitoring systems
- App Version: 2.3.1
- App Website: https://www.monitoring-plugins.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install monitoring-plugins with the following command
   ```
   brew install monitoring-plugins
   ```
4. monitoring-plugins is ready to use now!