---
title: "Install Startupizer2 on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Login items handler"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Startupizer2 on MacOS using homebrew

- App Name: Startupizer2
- App description: Login items handler
- App Version: 2,2060,1527510167
- App Website: http://gentlebytes.com/startupizer/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Startupizer2 with the following command
   ```
   brew install --cask startupizer
   ```
4. Startupizer2 is ready to use now!