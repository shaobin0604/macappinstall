---
title: "Install Skype Web Plugin on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Skype Web Plugin on MacOS using homebrew

- App Name: Skype Web Plugin
- App description: null
- App Version: 7.32.6.278
- App Website: https://www.skype.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Skype Web Plugin with the following command
   ```
   brew install --cask skypewebplugin
   ```
4. Skype Web Plugin is ready to use now!