---
title: "Install MongoDB on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "App wrapper for MongoDB"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install MongoDB on MacOS using homebrew

- App Name: MongoDB
- App description: App wrapper for MongoDB
- App Version: 4.2.0-build.3
- App Website: https://elweb.co/mongodb-app/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install MongoDB with the following command
   ```
   brew install --cask gcollazo-mongodb
   ```
4. MongoDB is ready to use now!