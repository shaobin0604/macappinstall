---
title: "Install Icons8 App on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "App for browsing icon, photo and music packages"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Icons8 App on MacOS using homebrew

- App Name: Icons8 App
- App description: App for browsing icon, photo and music packages
- App Version: 5.7.4,57400
- App Website: https://icons8.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Icons8 App with the following command
   ```
   brew install --cask icons8
   ```
4. Icons8 App is ready to use now!