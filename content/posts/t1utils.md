---
title: "Install t1utils on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line tools for dealing with Type 1 fonts"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install t1utils on MacOS using homebrew

- App Name: t1utils
- App description: Command-line tools for dealing with Type 1 fonts
- App Version: 1.42
- App Website: https://www.lcdf.org/type/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install t1utils with the following command
   ```
   brew install t1utils
   ```
4. t1utils is ready to use now!