---
title: "Install ccheck on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Check X509 certificate expiration from the command-line, with TAP output"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ccheck on MacOS using homebrew

- App Name: ccheck
- App description: Check X509 certificate expiration from the command-line, with TAP output
- App Version: 1.0.1
- App Website: https://github.com/nerdlem/ccheck

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ccheck with the following command
   ```
   brew install ccheck
   ```
4. ccheck is ready to use now!