---
title: "Install activemq-cpp on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C++ API for message brokers such as Apache ActiveMQ"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install activemq-cpp on MacOS using homebrew

- App Name: activemq-cpp
- App description: C++ API for message brokers such as Apache ActiveMQ
- App Version: 3.9.5
- App Website: https://activemq.apache.org/components/cms/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install activemq-cpp with the following command
   ```
   brew install activemq-cpp
   ```
4. activemq-cpp is ready to use now!