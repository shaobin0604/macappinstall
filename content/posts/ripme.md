---
title: "Install RipMe on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Album ripper for various websites"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install RipMe on MacOS using homebrew

- App Name: RipMe
- App description: Album ripper for various websites
- App Version: 1.7.95
- App Website: https://github.com/RipMeApp/ripme

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install RipMe with the following command
   ```
   brew install --cask ripme
   ```
4. RipMe is ready to use now!