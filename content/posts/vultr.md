---
title: "Install vultr on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line tool for Vultr"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install vultr on MacOS using homebrew

- App Name: vultr
- App description: Command-line tool for Vultr
- App Version: 2.0.3
- App Website: https://jamesclonk.github.io/vultr

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install vultr with the following command
   ```
   brew install vultr
   ```
4. vultr is ready to use now!