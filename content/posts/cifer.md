---
title: "Install cifer on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Work on automating classical cipher cracking in C"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cifer on MacOS using homebrew

- App Name: cifer
- App description: Work on automating classical cipher cracking in C
- App Version: 1.2.0
- App Website: https://code.google.com/p/cifer/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cifer with the following command
   ```
   brew install cifer
   ```
4. cifer is ready to use now!