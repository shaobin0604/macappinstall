---
title: "Install Thor on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utility to switch between applications"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Thor on MacOS using homebrew

- App Name: Thor
- App description: Utility to switch between applications
- App Version: 1.5.6
- App Website: https://github.com/gbammc/Thor/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Thor with the following command
   ```
   brew install --cask thor
   ```
4. Thor is ready to use now!