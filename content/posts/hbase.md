---
title: "Install hbase on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Hadoop database: a distributed, scalable, big data store"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install hbase on MacOS using homebrew

- App Name: hbase
- App description: Hadoop database: a distributed, scalable, big data store
- App Version: 2.4.6
- App Website: https://hbase.apache.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install hbase with the following command
   ```
   brew install hbase
   ```
4. hbase is ready to use now!