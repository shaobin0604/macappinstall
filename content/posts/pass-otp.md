---
title: "Install pass-otp on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Pass extension for managing one-time-password tokens"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pass-otp on MacOS using homebrew

- App Name: pass-otp
- App description: Pass extension for managing one-time-password tokens
- App Version: 1.2.0
- App Website: https://github.com/tadfisher/pass-otp#readme

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pass-otp with the following command
   ```
   brew install pass-otp
   ```
4. pass-otp is ready to use now!