---
title: "Install boost-python on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C++ library for C++/Python2 interoperability"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install boost-python on MacOS using homebrew

- App Name: boost-python
- App description: C++ library for C++/Python2 interoperability
- App Version: 1.74.0
- App Website: https://www.boost.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install boost-python with the following command
   ```
   brew install boost-python
   ```
4. boost-python is ready to use now!