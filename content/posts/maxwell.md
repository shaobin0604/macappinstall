---
title: "Install maxwell on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Reads MySQL binlogs and writes row updates as JSON to Kafka"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install maxwell on MacOS using homebrew

- App Name: maxwell
- App description: Reads MySQL binlogs and writes row updates as JSON to Kafka
- App Version: 1.37.0
- App Website: https://maxwells-daemon.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install maxwell with the following command
   ```
   brew install maxwell
   ```
4. maxwell is ready to use now!