---
title: "Install Lychee Slicer on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Slicer for Resin 3D printers"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Lychee Slicer on MacOS using homebrew

- App Name: Lychee Slicer
- App description: Slicer for Resin 3D printers
- App Version: 3.6.6
- App Website: https://mango3d.io/lychee-slicer-3-for-sla-3d-printers/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Lychee Slicer with the following command
   ```
   brew install --cask lycheeslicer
   ```
4. Lychee Slicer is ready to use now!