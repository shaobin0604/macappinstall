---
title: "Install urh on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Universal Radio Hacker"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install urh on MacOS using homebrew

- App Name: urh
- App description: Universal Radio Hacker
- App Version: 2.9.2
- App Website: https://github.com/jopohl/urh

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install urh with the following command
   ```
   brew install urh
   ```
4. urh is ready to use now!