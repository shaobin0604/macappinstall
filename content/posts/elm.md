---
title: "Install elm on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Functional programming language for building browser-based GUIs"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install elm on MacOS using homebrew

- App Name: elm
- App description: Functional programming language for building browser-based GUIs
- App Version: 0.19.1
- App Website: https://elm-lang.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install elm with the following command
   ```
   brew install elm
   ```
4. elm is ready to use now!