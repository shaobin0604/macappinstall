---
title: "Install Visual Paradigm Community Edition on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "All-in-one UML, SysML, BPMN Modeling Platform for Agile"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Visual Paradigm Community Edition on MacOS using homebrew

- App Name: Visual Paradigm Community Edition
- App description: All-in-one UML, SysML, BPMN Modeling Platform for Agile
- App Version: 16.3,20220215
- App Website: https://www.visual-paradigm.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Visual Paradigm Community Edition with the following command
   ```
   brew install --cask visual-paradigm-ce
   ```
4. Visual Paradigm Community Edition is ready to use now!