---
title: "Install chinadns-c on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Port of ChinaDNS to C: fix irregularities with DNS in China"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install chinadns-c on MacOS using homebrew

- App Name: chinadns-c
- App description: Port of ChinaDNS to C: fix irregularities with DNS in China
- App Version: 1.3.2
- App Website: https://github.com/shadowsocks/ChinaDNS

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install chinadns-c with the following command
   ```
   brew install chinadns-c
   ```
4. chinadns-c is ready to use now!