---
title: "Install cedille on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Language based on the Calculus of Dependent Lambda Eliminations"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cedille on MacOS using homebrew

- App Name: cedille
- App description: Language based on the Calculus of Dependent Lambda Eliminations
- App Version: 1.1.2
- App Website: https://cedille.github.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cedille with the following command
   ```
   brew install cedille
   ```
4. cedille is ready to use now!