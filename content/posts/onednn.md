---
title: "Install onednn on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Basic building blocks for deep learning applications"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install onednn on MacOS using homebrew

- App Name: onednn
- App description: Basic building blocks for deep learning applications
- App Version: 2.5.2
- App Website: https://01.org/oneDNN

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install onednn with the following command
   ```
   brew install onednn
   ```
4. onednn is ready to use now!