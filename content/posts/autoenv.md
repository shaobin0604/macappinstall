---
title: "Install autoenv on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Per-project, per-directory shell environments"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install autoenv on MacOS using homebrew

- App Name: autoenv
- App description: Per-project, per-directory shell environments
- App Version: 0.3.0
- App Website: https://github.com/hyperupcall/autoenv

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install autoenv with the following command
   ```
   brew install autoenv
   ```
4. autoenv is ready to use now!