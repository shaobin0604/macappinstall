---
title: "Install um on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line utility for creating and maintaining personal man pages"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install um on MacOS using homebrew

- App Name: um
- App description: Command-line utility for creating and maintaining personal man pages
- App Version: 4.2.0
- App Website: https://github.com/sinclairtarget/um

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install um with the following command
   ```
   brew install um
   ```
4. um is ready to use now!