---
title: "Install Nocturne on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Adjust display colors to suit low light conditions"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Nocturne on MacOS using homebrew

- App Name: Nocturne
- App description: Adjust display colors to suit low light conditions
- App Version: 3.0
- App Website: https://github.com/Daij-Djan/nocturne

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Nocturne with the following command
   ```
   brew install --cask nocturne
   ```
4. Nocturne is ready to use now!