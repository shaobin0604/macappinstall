---
title: "Install Gather Town on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Virtual video-calling space"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Gather Town on MacOS using homebrew

- App Name: Gather Town
- App description: Virtual video-calling space
- App Version: 0.1.7
- App Website: https://gather.town/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Gather Town with the following command
   ```
   brew install --cask gather
   ```
4. Gather Town is ready to use now!