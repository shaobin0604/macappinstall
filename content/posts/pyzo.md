---
title: "Install Pyzo on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Python IDE focused on interactivity and introspection"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Pyzo on MacOS using homebrew

- App Name: Pyzo
- App description: Python IDE focused on interactivity and introspection
- App Version: 4.11.7
- App Website: https://pyzo.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Pyzo with the following command
   ```
   brew install --cask pyzo
   ```
4. Pyzo is ready to use now!