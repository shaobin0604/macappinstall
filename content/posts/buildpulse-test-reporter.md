---
title: "Install buildpulse-test-reporter on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Connect your CI to BuildPulse to detect, track, and rank flaky tests"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install buildpulse-test-reporter on MacOS using homebrew

- App Name: buildpulse-test-reporter
- App description: Connect your CI to BuildPulse to detect, track, and rank flaky tests
- App Version: 0.22.0
- App Website: https://buildpulse.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install buildpulse-test-reporter with the following command
   ```
   brew install buildpulse-test-reporter
   ```
4. buildpulse-test-reporter is ready to use now!