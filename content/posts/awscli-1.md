---
title: "Install awscli@1 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Official Amazon AWS command-line interface"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install awscli@1 on MacOS using homebrew

- App Name: awscli@1
- App description: Official Amazon AWS command-line interface
- App Version: 1.22.50
- App Website: https://aws.amazon.com/cli/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install awscli@1 with the following command
   ```
   brew install awscli@1
   ```
4. awscli@1 is ready to use now!