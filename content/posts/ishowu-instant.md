---
title: "Install iShowU Instant on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Realtime screen recording"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install iShowU Instant on MacOS using homebrew

- App Name: iShowU Instant
- App description: Realtime screen recording
- App Version: 1.4.8,1383
- App Website: https://www.shinywhitebox.com/ishowu-instant

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install iShowU Instant with the following command
   ```
   brew install --cask ishowu-instant
   ```
4. iShowU Instant is ready to use now!