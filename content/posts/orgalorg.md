---
title: "Install orgalorg on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Parallel SSH commands executioner and file synchronization tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install orgalorg on MacOS using homebrew

- App Name: orgalorg
- App description: Parallel SSH commands executioner and file synchronization tool
- App Version: 1.2.0
- App Website: https://github.com/reconquest/orgalorg

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install orgalorg with the following command
   ```
   brew install orgalorg
   ```
4. orgalorg is ready to use now!