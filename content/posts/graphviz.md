---
title: "Install graphviz on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Graph visualization software from AT&T and Bell Labs"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install graphviz on MacOS using homebrew

- App Name: graphviz
- App description: Graph visualization software from AT&T and Bell Labs
- App Version: 2.50.0
- App Website: https://www.graphviz.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install graphviz with the following command
   ```
   brew install graphviz
   ```
4. graphviz is ready to use now!