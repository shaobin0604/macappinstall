---
title: "Install xa on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "6502 cross assembler"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install xa on MacOS using homebrew

- App Name: xa
- App description: 6502 cross assembler
- App Version: 2.3.12
- App Website: https://www.floodgap.com/retrotech/xa/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install xa with the following command
   ```
   brew install xa
   ```
4. xa is ready to use now!