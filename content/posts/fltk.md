---
title: "Install fltk on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cross-platform C++ GUI toolkit"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fltk on MacOS using homebrew

- App Name: fltk
- App description: Cross-platform C++ GUI toolkit
- App Version: 1.3.8
- App Website: https://www.fltk.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fltk with the following command
   ```
   brew install fltk
   ```
4. fltk is ready to use now!