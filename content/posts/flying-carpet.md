---
title: "Install Flying Carpet on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "File transfer over ad-hoc wifi"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Flying Carpet on MacOS using homebrew

- App Name: Flying Carpet
- App description: File transfer over ad-hoc wifi
- App Version: 4.1
- App Website: https://github.com/spieglt/flyingcarpet

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Flying Carpet with the following command
   ```
   brew install --cask flying-carpet
   ```
4. Flying Carpet is ready to use now!