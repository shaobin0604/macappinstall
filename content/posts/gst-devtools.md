---
title: "Install gst-devtools on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GStreamer development and validation tools"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gst-devtools on MacOS using homebrew

- App Name: gst-devtools
- App description: GStreamer development and validation tools
- App Version: 1.18.5
- App Website: https://gstreamer.freedesktop.org/modules/gstreamer.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gst-devtools with the following command
   ```
   brew install gst-devtools
   ```
4. gst-devtools is ready to use now!