---
title: "Install contentful-cli on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Contentful command-line tools"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install contentful-cli on MacOS using homebrew

- App Name: contentful-cli
- App description: Contentful command-line tools
- App Version: 1.11.0
- App Website: https://github.com/contentful/contentful-cli

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install contentful-cli with the following command
   ```
   brew install contentful-cli
   ```
4. contentful-cli is ready to use now!