---
title: "Install YoudaoNote on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Multi-platform note application"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install YoudaoNote on MacOS using homebrew

- App Name: YoudaoNote
- App description: Multi-platform note application
- App Version: 3.6.3
- App Website: https://note.youdao.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install YoudaoNote with the following command
   ```
   brew install --cask youdaonote
   ```
4. YoudaoNote is ready to use now!