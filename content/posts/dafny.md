---
title: "Install dafny on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Verification-aware programming language"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dafny on MacOS using homebrew

- App Name: dafny
- App description: Verification-aware programming language
- App Version: 3.4.1
- App Website: https://github.com/dafny-lang/dafny/blob/master/README.md

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dafny with the following command
   ```
   brew install dafny
   ```
4. dafny is ready to use now!