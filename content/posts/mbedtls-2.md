---
title: "Install mbedtls@2 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cryptographic & SSL/TLS library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mbedtls@2 on MacOS using homebrew

- App Name: mbedtls@2
- App description: Cryptographic & SSL/TLS library
- App Version: 2.28.0
- App Website: https://tls.mbed.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mbedtls@2 with the following command
   ```
   brew install mbedtls@2
   ```
4. mbedtls@2 is ready to use now!