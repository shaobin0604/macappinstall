---
title: "Install gitter-cli on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Extremely simple Gitter client for terminals"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gitter-cli on MacOS using homebrew

- App Name: gitter-cli
- App description: Extremely simple Gitter client for terminals
- App Version: 0.8.5
- App Website: https://github.com/RodrigoEspinosa/gitter-cli

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gitter-cli with the following command
   ```
   brew install gitter-cli
   ```
4. gitter-cli is ready to use now!