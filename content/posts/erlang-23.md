---
title: "Install erlang@23 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Programming language for highly scalable real-time systems"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install erlang@23 on MacOS using homebrew

- App Name: erlang@23
- App description: Programming language for highly scalable real-time systems
- App Version: 23.3.4.10
- App Website: https://www.erlang.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install erlang@23 with the following command
   ```
   brew install erlang@23
   ```
4. erlang@23 is ready to use now!