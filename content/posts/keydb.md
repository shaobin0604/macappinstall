---
title: "Install keydb on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Multithreaded fork of Redis"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install keydb on MacOS using homebrew

- App Name: keydb
- App description: Multithreaded fork of Redis
- App Version: 6.2.2
- App Website: https://keydb.dev

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install keydb with the following command
   ```
   brew install keydb
   ```
4. keydb is ready to use now!