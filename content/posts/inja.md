---
title: "Install inja on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Template engine for modern C++"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install inja on MacOS using homebrew

- App Name: inja
- App description: Template engine for modern C++
- App Version: 3.3.0
- App Website: https://pantor.github.io/inja/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install inja with the following command
   ```
   brew install inja
   ```
4. inja is ready to use now!