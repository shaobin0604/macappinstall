---
title: "Install avce00 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Make Arc/Info (binary) Vector Coverages appear as E00"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install avce00 on MacOS using homebrew

- App Name: avce00
- App description: Make Arc/Info (binary) Vector Coverages appear as E00
- App Version: 2.0.0
- App Website: http://avce00.maptools.org/avce00/index.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install avce00 with the following command
   ```
   brew install avce00
   ```
4. avce00 is ready to use now!