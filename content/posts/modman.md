---
title: "Install modman on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Module deployment script geared towards Magento development"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install modman on MacOS using homebrew

- App Name: modman
- App description: Module deployment script geared towards Magento development
- App Version: 1.14
- App Website: https://github.com/colinmollenhour/modman

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install modman with the following command
   ```
   brew install modman
   ```
4. modman is ready to use now!