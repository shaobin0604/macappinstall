---
title: "Install Winx HD Video Converter on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "HD video converter"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Winx HD Video Converter on MacOS using homebrew

- App Name: Winx HD Video Converter
- App description: HD video converter
- App Version: 6.5.2,20201202
- App Website: https://www.winxdvd.com/hd-video-converter-for-mac/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Winx HD Video Converter with the following command
   ```
   brew install --cask winx-hd-video-converter
   ```
4. Winx HD Video Converter is ready to use now!