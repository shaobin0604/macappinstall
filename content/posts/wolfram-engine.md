---
title: "Install Wolfram Engine on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Evaluator for the Wolfram Language"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Wolfram Engine on MacOS using homebrew

- App Name: Wolfram Engine
- App description: Evaluator for the Wolfram Language
- App Version: 13.0.0.0
- App Website: https://www.wolfram.com/engine/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Wolfram Engine with the following command
   ```
   brew install --cask wolfram-engine
   ```
4. Wolfram Engine is ready to use now!