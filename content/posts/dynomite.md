---
title: "Install dynomite on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Generic dynamo implementation for different k-v storage engines"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dynomite on MacOS using homebrew

- App Name: dynomite
- App description: Generic dynamo implementation for different k-v storage engines
- App Version: 0.6.22
- App Website: https://github.com/Netflix/dynomite

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dynomite with the following command
   ```
   brew install dynomite
   ```
4. dynomite is ready to use now!