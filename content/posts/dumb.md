---
title: "Install dumb on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "IT, XM, S3M and MOD player library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dumb on MacOS using homebrew

- App Name: dumb
- App description: IT, XM, S3M and MOD player library
- App Version: 0.9.3
- App Website: https://dumb.sourceforge.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dumb with the following command
   ```
   brew install dumb
   ```
4. dumb is ready to use now!