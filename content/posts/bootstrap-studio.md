---
title: "Install Bootstrap Studio on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Design and prototype websites using the Bootstrap framework"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Bootstrap Studio on MacOS using homebrew

- App Name: Bootstrap Studio
- App description: Design and prototype websites using the Bootstrap framework
- App Version: 5.9.1
- App Website: https://bootstrapstudio.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Bootstrap Studio with the following command
   ```
   brew install --cask bootstrap-studio
   ```
4. Bootstrap Studio is ready to use now!