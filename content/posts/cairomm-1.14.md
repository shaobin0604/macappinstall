---
title: "Install cairomm@1.14 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Vector graphics library with cross-device output support"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cairomm@1.14 on MacOS using homebrew

- App Name: cairomm@1.14
- App description: Vector graphics library with cross-device output support
- App Version: 1.14.3
- App Website: https://cairographics.org/cairomm/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cairomm@1.14 with the following command
   ```
   brew install cairomm@1.14
   ```
4. cairomm@1.14 is ready to use now!