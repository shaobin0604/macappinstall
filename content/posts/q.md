---
title: "Install q on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Run SQL directly on CSV or TSV files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install q on MacOS using homebrew

- App Name: q
- App description: Run SQL directly on CSV or TSV files
- App Version: 2.0.20
- App Website: https://harelba.github.io/q/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install q with the following command
   ```
   brew install q
   ```
4. q is ready to use now!