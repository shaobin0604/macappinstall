---
title: "Install Tune•Instructor on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Tune•Instructor on MacOS using homebrew

- App Name: Tune•Instructor
- App description: null
- App Version: 3.7v5,17467
- App Website: https://www.tune-instructor.de/com/start.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Tune•Instructor with the following command
   ```
   brew install --cask tuneinstructor
   ```
4. Tune•Instructor is ready to use now!