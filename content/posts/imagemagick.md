---
title: "Install imagemagick on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tools and libraries to manipulate images in many formats"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install imagemagick on MacOS using homebrew

- App Name: imagemagick
- App description: Tools and libraries to manipulate images in many formats
- App Version: 7.1.0-25
- App Website: https://imagemagick.org/index.php

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install imagemagick with the following command
   ```
   brew install imagemagick
   ```
4. imagemagick is ready to use now!