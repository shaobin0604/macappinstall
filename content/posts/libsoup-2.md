---
title: "Install libsoup@2 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "HTTP client/server library for GNOME"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libsoup@2 on MacOS using homebrew

- App Name: libsoup@2
- App description: HTTP client/server library for GNOME
- App Version: 2.74.1
- App Website: https://wiki.gnome.org/Projects/libsoup

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libsoup@2 with the following command
   ```
   brew install libsoup@2
   ```
4. libsoup@2 is ready to use now!