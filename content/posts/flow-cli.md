---
title: "Install flow-cli on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line interface that provides utilities for building Flow applications"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install flow-cli on MacOS using homebrew

- App Name: flow-cli
- App description: Command-line interface that provides utilities for building Flow applications
- App Version: 0.31.1
- App Website: https://onflow.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install flow-cli with the following command
   ```
   brew install flow-cli
   ```
4. flow-cli is ready to use now!