---
title: "Install zelda-roth-se on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Zelda Return of the Hylian SE"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install zelda-roth-se on MacOS using homebrew

- App Name: zelda-roth-se
- App description: Zelda Return of the Hylian SE
- App Version: 1.2.1
- App Website: https://www.solarus-games.org/en/games/the-legend-of-zelda-return-of-the-hylian-se

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install zelda-roth-se with the following command
   ```
   brew install zelda-roth-se
   ```
4. zelda-roth-se is ready to use now!