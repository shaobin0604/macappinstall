---
title: "Install oci-cli on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Oracle Cloud Infrastructure CLI"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install oci-cli on MacOS using homebrew

- App Name: oci-cli
- App description: Oracle Cloud Infrastructure CLI
- App Version: 3.5.1
- App Website: https://docs.cloud.oracle.com/iaas/Content/API/Concepts/cliconcepts.htm

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install oci-cli with the following command
   ```
   brew install oci-cli
   ```
4. oci-cli is ready to use now!