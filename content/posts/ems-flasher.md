---
title: "Install ems-flasher on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Software for flashing the EMS Gameboy USB cart"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ems-flasher on MacOS using homebrew

- App Name: ems-flasher
- App description: Software for flashing the EMS Gameboy USB cart
- App Version: 0.03
- App Website: https://lacklustre.net/projects/ems-flasher/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ems-flasher with the following command
   ```
   brew install ems-flasher
   ```
4. ems-flasher is ready to use now!