---
title: "Install remotion on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Communication tool for remote teams"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install remotion on MacOS using homebrew

- App Name: remotion
- App description: Communication tool for remote teams
- App Version: 1.69.0
- App Website: https://www.remotion.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install remotion with the following command
   ```
   brew install --cask remotion
   ```
4. remotion is ready to use now!