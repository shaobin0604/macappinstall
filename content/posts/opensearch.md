---
title: "Install opensearch on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Open source distributed and RESTful search engine"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install opensearch on MacOS using homebrew

- App Name: opensearch
- App description: Open source distributed and RESTful search engine
- App Version: 1.2.4
- App Website: https://github.com/opensearch-project/OpenSearch

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install opensearch with the following command
   ```
   brew install opensearch
   ```
4. opensearch is ready to use now!