---
title: "Install tty-solitaire on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Ncurses-based klondike solitaire game"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tty-solitaire on MacOS using homebrew

- App Name: tty-solitaire
- App description: Ncurses-based klondike solitaire game
- App Version: 1.3.1
- App Website: https://github.com/mpereira/tty-solitaire

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tty-solitaire with the following command
   ```
   brew install tty-solitaire
   ```
4. tty-solitaire is ready to use now!