---
title: "Install tclap on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Templatized C++ command-line parser library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tclap on MacOS using homebrew

- App Name: tclap
- App description: Templatized C++ command-line parser library
- App Version: 1.2.5
- App Website: https://tclap.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tclap with the following command
   ```
   brew install tclap
   ```
4. tclap is ready to use now!