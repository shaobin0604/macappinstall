---
title: "Install gabedit on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GUI to computational chemistry packages like Gamess-US, Gaussian, etc."
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gabedit on MacOS using homebrew

- App Name: gabedit
- App description: GUI to computational chemistry packages like Gamess-US, Gaussian, etc.
- App Version: 2.5.0
- App Website: https://gabedit.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gabedit with the following command
   ```
   brew install gabedit
   ```
4. gabedit is ready to use now!