---
title: "Install tesseract-lang on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Enables extra languages support for Tesseract"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tesseract-lang on MacOS using homebrew

- App Name: tesseract-lang
- App description: Enables extra languages support for Tesseract
- App Version: 4.1.0
- App Website: https://github.com/tesseract-ocr/tessdata_fast/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tesseract-lang with the following command
   ```
   brew install tesseract-lang
   ```
4. tesseract-lang is ready to use now!