---
title: "Install uudeview on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Smart multi-file multi-part decoder"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install uudeview on MacOS using homebrew

- App Name: uudeview
- App description: Smart multi-file multi-part decoder
- App Version: 0.5.20
- App Website: http://www.fpx.de/fp/Software/UUDeview/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install uudeview with the following command
   ```
   brew install uudeview
   ```
4. uudeview is ready to use now!