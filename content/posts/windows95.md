---
title: "Install Windows 95 on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Electron Windows 95"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Windows 95 on MacOS using homebrew

- App Name: Windows 95
- App description: Electron Windows 95
- App Version: 2.3.0
- App Website: https://github.com/felixrieseberg/windows95

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Windows 95 with the following command
   ```
   brew install --cask windows95
   ```
4. Windows 95 is ready to use now!