---
title: "Install crash on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Kernel debugging shell for Java that allows gdb-like syntax"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install crash on MacOS using homebrew

- App Name: crash
- App description: Kernel debugging shell for Java that allows gdb-like syntax
- App Version: 1.3.2
- App Website: https://www.crashub.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install crash with the following command
   ```
   brew install crash
   ```
4. crash is ready to use now!