---
title: "Install libart on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library for high-performance 2D graphics"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libart on MacOS using homebrew

- App Name: libart
- App description: Library for high-performance 2D graphics
- App Version: 2.3.21
- App Website: https://people.gnome.org/~mathieu/libart/libart.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libart with the following command
   ```
   brew install libart
   ```
4. libart is ready to use now!