---
title: "Install Vagrant Manager on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Vagrant Manager on MacOS using homebrew

- App Name: Vagrant Manager
- App description: null
- App Version: 2.7.1
- App Website: https://www.vagrantmanager.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Vagrant Manager with the following command
   ```
   brew install --cask vagrant-manager
   ```
4. Vagrant Manager is ready to use now!