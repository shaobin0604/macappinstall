---
title: "Install bar on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Provide progress bars for shell scripts"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bar on MacOS using homebrew

- App Name: bar
- App description: Provide progress bars for shell scripts
- App Version: 1.4
- App Website: http://www.theiling.de/projects/bar.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bar with the following command
   ```
   brew install bar
   ```
4. bar is ready to use now!