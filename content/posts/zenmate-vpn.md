---
title: "Install ZenMate VPN on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ZenMate VPN on MacOS using homebrew

- App Name: ZenMate VPN
- App description: null
- App Version: 5.1.0.21
- App Website: https://zenmate.com/products/vpn-for-osx/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ZenMate VPN with the following command
   ```
   brew install --cask zenmate-vpn
   ```
4. ZenMate VPN is ready to use now!