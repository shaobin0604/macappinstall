---
title: "Install ca-certificates on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Mozilla CA certificate store"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ca-certificates on MacOS using homebrew

- App Name: ca-certificates
- App description: Mozilla CA certificate store
- App Version: 2022-02-01
- App Website: https://curl.se/docs/caextract.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ca-certificates with the following command
   ```
   brew install ca-certificates
   ```
4. ca-certificates is ready to use now!