---
title: "Install SimSim on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to explore iOS application folders in Terminal or Finder"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install SimSim on MacOS using homebrew

- App Name: SimSim
- App description: Tool to explore iOS application folders in Terminal or Finder
- App Version: 1.4.5
- App Website: https://github.com/dsmelov/simsim/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install SimSim with the following command
   ```
   brew install --cask simsim
   ```
4. SimSim is ready to use now!