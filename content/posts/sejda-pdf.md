---
title: "Install Sejda PDF Desktop on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "PDF editor"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Sejda PDF Desktop on MacOS using homebrew

- App Name: Sejda PDF Desktop
- App description: PDF editor
- App Version: 7.4.0
- App Website: https://www.sejda.com/desktop

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Sejda PDF Desktop with the following command
   ```
   brew install --cask sejda-pdf
   ```
4. Sejda PDF Desktop is ready to use now!