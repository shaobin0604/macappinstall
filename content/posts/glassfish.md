---
title: "Install glassfish on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Java EE application server"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install glassfish on MacOS using homebrew

- App Name: glassfish
- App description: Java EE application server
- App Version: 6.2.5
- App Website: https://glassfish.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install glassfish with the following command
   ```
   brew install glassfish
   ```
4. glassfish is ready to use now!