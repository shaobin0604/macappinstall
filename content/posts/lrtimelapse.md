---
title: "Install LRTimelapse on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Time lapse editing, keyframing, grading and rendering"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install LRTimelapse on MacOS using homebrew

- App Name: LRTimelapse
- App description: Time lapse editing, keyframing, grading and rendering
- App Version: 5.8.0
- App Website: https://lrtimelapse.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install LRTimelapse with the following command
   ```
   brew install --cask lrtimelapse
   ```
4. LRTimelapse is ready to use now!