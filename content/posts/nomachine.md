---
title: "Install NoMachine on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Remote desktop software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install NoMachine on MacOS using homebrew

- App Name: NoMachine
- App description: Remote desktop software
- App Version: 7.8.2_4
- App Website: https://www.nomachine.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install NoMachine with the following command
   ```
   brew install --cask nomachine
   ```
4. NoMachine is ready to use now!