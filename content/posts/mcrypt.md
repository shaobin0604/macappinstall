---
title: "Install mcrypt on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Replacement for the old crypt package and crypt(1) command"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mcrypt on MacOS using homebrew

- App Name: mcrypt
- App description: Replacement for the old crypt package and crypt(1) command
- App Version: 2.6.8
- App Website: https://mcrypt.sourceforge.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mcrypt with the following command
   ```
   brew install mcrypt
   ```
4. mcrypt is ready to use now!