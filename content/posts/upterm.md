---
title: "Install Upterm on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Terminal emulator for the 21st century"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Upterm on MacOS using homebrew

- App Name: Upterm
- App description: Terminal emulator for the 21st century
- App Version: 0.4.4
- App Website: https://github.com/railsware/upterm

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Upterm with the following command
   ```
   brew install --cask upterm
   ```
4. Upterm is ready to use now!