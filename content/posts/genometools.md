---
title: "Install genometools on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Versatile open source genome analysis software"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install genometools on MacOS using homebrew

- App Name: genometools
- App description: Versatile open source genome analysis software
- App Version: 1.6.2
- App Website: http://genometools.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install genometools with the following command
   ```
   brew install genometools
   ```
4. genometools is ready to use now!