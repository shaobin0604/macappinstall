---
title: "Install pkg-config-wrapper on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Easier way to include C code in your Go program"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pkg-config-wrapper on MacOS using homebrew

- App Name: pkg-config-wrapper
- App description: Easier way to include C code in your Go program
- App Version: 0.2.12
- App Website: https://github.com/influxdata/pkg-config

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pkg-config-wrapper with the following command
   ```
   brew install pkg-config-wrapper
   ```
4. pkg-config-wrapper is ready to use now!