---
title: "Install clipper on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Share macOS clipboard with tmux and other local and remote apps"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install clipper on MacOS using homebrew

- App Name: clipper
- App description: Share macOS clipboard with tmux and other local and remote apps
- App Version: 2.0.0
- App Website: https://wincent.com/products/clipper

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install clipper with the following command
   ```
   brew install clipper
   ```
4. clipper is ready to use now!