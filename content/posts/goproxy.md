---
title: "Install goproxy on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Global proxy for Go modules"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install goproxy on MacOS using homebrew

- App Name: goproxy
- App description: Global proxy for Go modules
- App Version: 2.0.7
- App Website: https://github.com/goproxyio/goproxy

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install goproxy with the following command
   ```
   brew install goproxy
   ```
4. goproxy is ready to use now!