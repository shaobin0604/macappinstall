---
title: "Install detect-secrets on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Enterprise friendly way of detecting and preventing secrets in code"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install detect-secrets on MacOS using homebrew

- App Name: detect-secrets
- App description: Enterprise friendly way of detecting and preventing secrets in code
- App Version: 1.2.0
- App Website: https://github.com/Yelp/detect-secrets

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install detect-secrets with the following command
   ```
   brew install detect-secrets
   ```
4. detect-secrets is ready to use now!