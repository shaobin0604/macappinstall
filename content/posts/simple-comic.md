---
title: "Install Simple Comic on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Comic viewer/reader"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Simple Comic on MacOS using homebrew

- App Name: Simple Comic
- App description: Comic viewer/reader
- App Version: 1.7_252
- App Website: https://dancingtortoise.github.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Simple Comic with the following command
   ```
   brew install --cask simple-comic
   ```
4. Simple Comic is ready to use now!