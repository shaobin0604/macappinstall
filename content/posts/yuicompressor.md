---
title: "Install yuicompressor on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Yahoo! JavaScript and CSS compressor"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install yuicompressor on MacOS using homebrew

- App Name: yuicompressor
- App description: Yahoo! JavaScript and CSS compressor
- App Version: 2.4.8
- App Website: https://yui.github.io/yuicompressor/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install yuicompressor with the following command
   ```
   brew install yuicompressor
   ```
4. yuicompressor is ready to use now!