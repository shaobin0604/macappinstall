---
title: "Install Font Smoothing Adjuster on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Re-enable the font smoothing controls"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Font Smoothing Adjuster on MacOS using homebrew

- App Name: Font Smoothing Adjuster
- App description: Re-enable the font smoothing controls
- App Version: 1.2.1
- App Website: https://www.fontsmoothingadjuster.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Font Smoothing Adjuster with the following command
   ```
   brew install --cask font-smoothing-adjuster
   ```
4. Font Smoothing Adjuster is ready to use now!