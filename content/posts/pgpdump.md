---
title: "Install pgpdump on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "PGP packet visualizer"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pgpdump on MacOS using homebrew

- App Name: pgpdump
- App description: PGP packet visualizer
- App Version: 0.34
- App Website: https://www.mew.org/~kazu/proj/pgpdump/en/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pgpdump with the following command
   ```
   brew install pgpdump
   ```
4. pgpdump is ready to use now!