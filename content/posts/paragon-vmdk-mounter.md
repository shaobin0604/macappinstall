---
title: "Install Paragon VMDK Mounter on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Mounts a virtual container by double click"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Paragon VMDK Mounter on MacOS using homebrew

- App Name: Paragon VMDK Mounter
- App description: Mounts a virtual container by double click
- App Version: 2.4
- App Website: https://www.paragon-software.com/home/vd-mounter-mac-free/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Paragon VMDK Mounter with the following command
   ```
   brew install --cask paragon-vmdk-mounter
   ```
4. Paragon VMDK Mounter is ready to use now!