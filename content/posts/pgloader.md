---
title: "Install pgloader on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Data loading tool for PostgreSQL"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pgloader on MacOS using homebrew

- App Name: pgloader
- App description: Data loading tool for PostgreSQL
- App Version: 3.6.2
- App Website: https://github.com/dimitri/pgloader

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pgloader with the following command
   ```
   brew install pgloader
   ```
4. pgloader is ready to use now!