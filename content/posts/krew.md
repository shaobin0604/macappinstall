---
title: "Install krew on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Package manager for kubectl plugins"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install krew on MacOS using homebrew

- App Name: krew
- App description: Package manager for kubectl plugins
- App Version: 0.4.3
- App Website: https://sigs.k8s.io/krew/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install krew with the following command
   ```
   brew install krew
   ```
4. krew is ready to use now!