---
title: "Install itex2mml on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Text filter to convert itex equations to MathML"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install itex2mml on MacOS using homebrew

- App Name: itex2mml
- App description: Text filter to convert itex equations to MathML
- App Version: 1.6.1
- App Website: https://golem.ph.utexas.edu/~distler/blog/itex2MML.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install itex2mml with the following command
   ```
   brew install itex2mml
   ```
4. itex2mml is ready to use now!