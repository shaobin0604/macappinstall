---
title: "Install tbox on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Glib-like multi-platform C library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tbox on MacOS using homebrew

- App Name: tbox
- App description: Glib-like multi-platform C library
- App Version: 1.6.7
- App Website: https://tboox.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tbox with the following command
   ```
   brew install tbox
   ```
4. tbox is ready to use now!