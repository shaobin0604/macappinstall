---
title: "Install libgnt on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "NCurses toolkit for creating text-mode graphical user interfaces"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libgnt on MacOS using homebrew

- App Name: libgnt
- App description: NCurses toolkit for creating text-mode graphical user interfaces
- App Version: 2.14.3
- App Website: https://keep.imfreedom.org/libgnt/libgnt

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libgnt with the following command
   ```
   brew install libgnt
   ```
4. libgnt is ready to use now!