---
title: "Install Universal G-code Sender (Platform version) on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "G-code sender for CNC (compatible with GRBL, TinyG, g2core and Smoothieware)"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Universal G-code Sender (Platform version) on MacOS using homebrew

- App Name: Universal G-code Sender (Platform version)
- App description: G-code sender for CNC (compatible with GRBL, TinyG, g2core and Smoothieware)
- App Version: 2.0.10
- App Website: https://winder.github.io/ugs_website/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Universal G-code Sender (Platform version) with the following command
   ```
   brew install --cask universal-gcode-platform
   ```
4. Universal G-code Sender (Platform version) is ready to use now!