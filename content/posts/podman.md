---
title: "Install podman on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool for managing OCI containers and pods"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install podman on MacOS using homebrew

- App Name: podman
- App description: Tool for managing OCI containers and pods
- App Version: 3.4.4
- App Website: https://podman.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install podman with the following command
   ```
   brew install podman
   ```
4. podman is ready to use now!