---
title: "Install libcoap on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Lightweight application-protocol for resource-constrained devices"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libcoap on MacOS using homebrew

- App Name: libcoap
- App description: Lightweight application-protocol for resource-constrained devices
- App Version: 4.3.0
- App Website: https://github.com/obgm/libcoap

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libcoap with the following command
   ```
   brew install libcoap
   ```
4. libcoap is ready to use now!