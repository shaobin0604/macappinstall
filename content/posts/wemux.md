---
title: "Install wemux on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Enhances tmux's to provide multiuser terminal multiplexing"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install wemux on MacOS using homebrew

- App Name: wemux
- App description: Enhances tmux's to provide multiuser terminal multiplexing
- App Version: 3.2.0
- App Website: https://github.com/zolrath/wemux

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install wemux with the following command
   ```
   brew install wemux
   ```
4. wemux is ready to use now!