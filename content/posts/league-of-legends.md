---
title: "Install League of Legends on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Multiplayer online battle arena game"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install League of Legends on MacOS using homebrew

- App Name: League of Legends
- App description: Multiplayer online battle arena game
- App Version: 1.0
- App Website: https://na.leagueoflegends.com/en-us/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install League of Legends with the following command
   ```
   brew install --cask league-of-legends
   ```
4. League of Legends is ready to use now!