---
title: "Install gradio on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GTK3 app for finding and listening to internet radio stations"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gradio on MacOS using homebrew

- App Name: gradio
- App description: GTK3 app for finding and listening to internet radio stations
- App Version: 7.3
- App Website: https://github.com/haecker-felix/Gradio

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gradio with the following command
   ```
   brew install gradio
   ```
4. gradio is ready to use now!