---
title: "Install F-Bar on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Manage Laravel Forge servers from the menubar"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install F-Bar on MacOS using homebrew

- App Name: F-Bar
- App description: Manage Laravel Forge servers from the menubar
- App Version: 5.0.5
- App Website: https://laravel-forge-menubar.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install F-Bar with the following command
   ```
   brew install --cask f-bar
   ```
4. F-Bar is ready to use now!