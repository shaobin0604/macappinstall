---
title: "Install creduce on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Reduce a C/C++ program while keeping a property of interest"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install creduce on MacOS using homebrew

- App Name: creduce
- App description: Reduce a C/C++ program while keeping a property of interest
- App Version: 2.10.0
- App Website: https://embed.cs.utah.edu/creduce/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install creduce with the following command
   ```
   brew install creduce
   ```
4. creduce is ready to use now!