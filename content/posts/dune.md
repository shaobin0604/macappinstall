---
title: "Install dune on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Composable build system for OCaml"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dune on MacOS using homebrew

- App Name: dune
- App description: Composable build system for OCaml
- App Version: 2.9.3
- App Website: https://dune.build/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dune with the following command
   ```
   brew install dune
   ```
4. dune is ready to use now!