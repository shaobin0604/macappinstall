---
title: "Install Paw on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "HTTP client that helps testing and describing APIs"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Paw on MacOS using homebrew

- App Name: Paw
- App description: HTTP client that helps testing and describing APIs
- App Version: 3.3.5,3003005001
- App Website: https://paw.cloud/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Paw with the following command
   ```
   brew install --cask paw
   ```
4. Paw is ready to use now!