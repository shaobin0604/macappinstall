---
title: "Install rxvt-unicode on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Rxvt fork with Unicode support"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install rxvt-unicode on MacOS using homebrew

- App Name: rxvt-unicode
- App description: Rxvt fork with Unicode support
- App Version: 9.26
- App Website: http://software.schmorp.de/pkg/rxvt-unicode.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install rxvt-unicode with the following command
   ```
   brew install rxvt-unicode
   ```
4. rxvt-unicode is ready to use now!