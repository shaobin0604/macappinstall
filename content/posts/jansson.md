---
title: "Install jansson on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C library for encoding, decoding, and manipulating JSON"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install jansson on MacOS using homebrew

- App Name: jansson
- App description: C library for encoding, decoding, and manipulating JSON
- App Version: 2.14
- App Website: https://digip.org/jansson/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install jansson with the following command
   ```
   brew install jansson
   ```
4. jansson is ready to use now!