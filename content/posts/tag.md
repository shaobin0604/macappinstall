---
title: "Install tag on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Manipulate and query tags on macOS files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tag on MacOS using homebrew

- App Name: tag
- App description: Manipulate and query tags on macOS files
- App Version: 0.10
- App Website: https://github.com/jdberry/tag/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tag with the following command
   ```
   brew install tag
   ```
4. tag is ready to use now!