---
title: "Install Pomotroid on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Timer application"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Pomotroid on MacOS using homebrew

- App Name: Pomotroid
- App description: Timer application
- App Version: 0.13.0
- App Website: https://github.com/Splode/pomotroid

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Pomotroid with the following command
   ```
   brew install --cask pomotroid
   ```
4. Pomotroid is ready to use now!