---
title: "Install hapi-fhir-cli on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line interface for the HAPI FHIR library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install hapi-fhir-cli on MacOS using homebrew

- App Name: hapi-fhir-cli
- App description: Command-line interface for the HAPI FHIR library
- App Version: 5.6.0
- App Website: https://hapifhir.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install hapi-fhir-cli with the following command
   ```
   brew install hapi-fhir-cli
   ```
4. hapi-fhir-cli is ready to use now!