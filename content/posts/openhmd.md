---
title: "Install openhmd on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Free and open source API and drivers for immersive technology"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install openhmd on MacOS using homebrew

- App Name: openhmd
- App description: Free and open source API and drivers for immersive technology
- App Version: 0.3.0
- App Website: http://openhmd.net

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install openhmd with the following command
   ```
   brew install openhmd
   ```
4. openhmd is ready to use now!