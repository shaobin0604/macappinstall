---
title: "Install ucl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Data compression library with small memory footprint"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ucl on MacOS using homebrew

- App Name: ucl
- App description: Data compression library with small memory footprint
- App Version: 1.03
- App Website: https://www.oberhumer.com/opensource/ucl/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ucl with the following command
   ```
   brew install ucl
   ```
4. ucl is ready to use now!