---
title: "Install libdc1394 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Provides API for IEEE 1394 cameras"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libdc1394 on MacOS using homebrew

- App Name: libdc1394
- App description: Provides API for IEEE 1394 cameras
- App Version: 2.2.6
- App Website: https://damien.douxchamps.net/ieee1394/libdc1394/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libdc1394 with the following command
   ```
   brew install libdc1394
   ```
4. libdc1394 is ready to use now!