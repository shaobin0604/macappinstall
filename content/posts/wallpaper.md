---
title: "Install wallpaper on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Manage the desktop wallpaper"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install wallpaper on MacOS using homebrew

- App Name: wallpaper
- App description: Manage the desktop wallpaper
- App Version: 2.3.1
- App Website: https://github.com/sindresorhus/macos-wallpaper

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install wallpaper with the following command
   ```
   brew install wallpaper
   ```
4. wallpaper is ready to use now!