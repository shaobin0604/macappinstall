---
title: "Install fdk-aac-encoder on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line encoder frontend for libfdk-aac"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fdk-aac-encoder on MacOS using homebrew

- App Name: fdk-aac-encoder
- App description: Command-line encoder frontend for libfdk-aac
- App Version: 1.0.2
- App Website: https://github.com/nu774/fdkaac

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fdk-aac-encoder with the following command
   ```
   brew install fdk-aac-encoder
   ```
4. fdk-aac-encoder is ready to use now!