---
title: "Install cvs-fast-export on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Export an RCS or CVS history as a fast-import stream"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cvs-fast-export on MacOS using homebrew

- App Name: cvs-fast-export
- App description: Export an RCS or CVS history as a fast-import stream
- App Version: 1.59
- App Website: http://www.catb.org/~esr/cvs-fast-export/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cvs-fast-export with the following command
   ```
   brew install cvs-fast-export
   ```
4. cvs-fast-export is ready to use now!