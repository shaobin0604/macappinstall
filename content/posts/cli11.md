---
title: "Install cli11 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Simple and intuitive command-line parser for C++11"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cli11 on MacOS using homebrew

- App Name: cli11
- App description: Simple and intuitive command-line parser for C++11
- App Version: 2.1.2
- App Website: https://cliutils.github.io/CLI11/book/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cli11 with the following command
   ```
   brew install cli11
   ```
4. cli11 is ready to use now!