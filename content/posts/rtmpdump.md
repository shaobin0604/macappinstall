---
title: "Install rtmpdump on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool for downloading RTMP streaming media"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install rtmpdump on MacOS using homebrew

- App Name: rtmpdump
- App description: Tool for downloading RTMP streaming media
- App Version: 2.4+20151223
- App Website: https://rtmpdump.mplayerhq.hu/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install rtmpdump with the following command
   ```
   brew install rtmpdump
   ```
4. rtmpdump is ready to use now!