---
title: "Install yaws on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Webserver for dynamic content (written in Erlang)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install yaws on MacOS using homebrew

- App Name: yaws
- App description: Webserver for dynamic content (written in Erlang)
- App Version: 2.1.1
- App Website: http://yaws.hyber.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install yaws with the following command
   ```
   brew install yaws
   ```
4. yaws is ready to use now!