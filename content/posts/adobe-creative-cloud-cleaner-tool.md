---
title: "Install Adobe Creative Cloud Cleaner Tool on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utility to clean up corrupted installations of Adobe software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Adobe Creative Cloud Cleaner Tool on MacOS using homebrew

- App Name: Adobe Creative Cloud Cleaner Tool
- App description: Utility to clean up corrupted installations of Adobe software
- App Version: 4.3.0.190
- App Website: https://helpx.adobe.com/creative-cloud/kb/cc-cleaner-tool-installation-problems.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Adobe Creative Cloud Cleaner Tool with the following command
   ```
   brew install --cask adobe-creative-cloud-cleaner-tool
   ```
4. Adobe Creative Cloud Cleaner Tool is ready to use now!