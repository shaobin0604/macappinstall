---
title: "Install jing-trang on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Schema validation and conversion based on RELAX NG"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install jing-trang on MacOS using homebrew

- App Name: jing-trang
- App description: Schema validation and conversion based on RELAX NG
- App Version: 20181222
- App Website: http://www.thaiopensource.com/relaxng/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install jing-trang with the following command
   ```
   brew install jing-trang
   ```
4. jing-trang is ready to use now!