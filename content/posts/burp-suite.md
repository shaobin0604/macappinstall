---
title: "Install Burp Suite Community Edition on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Web security testing toolkit"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Burp Suite Community Edition on MacOS using homebrew

- App Name: Burp Suite Community Edition
- App description: Web security testing toolkit
- App Version: 2022.1.1
- App Website: https://portswigger.net/burp/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Burp Suite Community Edition with the following command
   ```
   brew install --cask burp-suite
   ```
4. Burp Suite Community Edition is ready to use now!