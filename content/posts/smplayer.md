---
title: "Install SMPlayer on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Media player with built-in codecs"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install SMPlayer on MacOS using homebrew

- App Name: SMPlayer
- App description: Media player with built-in codecs
- App Version: 21.10.0
- App Website: https://www.smplayer.info/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install SMPlayer with the following command
   ```
   brew install --cask smplayer
   ```
4. SMPlayer is ready to use now!