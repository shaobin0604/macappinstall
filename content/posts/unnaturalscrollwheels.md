---
title: "Install UnnaturalScrollWheels on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to invert scroll direction for physical scroll wheels"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install UnnaturalScrollWheels on MacOS using homebrew

- App Name: UnnaturalScrollWheels
- App description: Tool to invert scroll direction for physical scroll wheels
- App Version: 1.2.3
- App Website: https://github.com/ther0n/UnnaturalScrollWheels

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install UnnaturalScrollWheels with the following command
   ```
   brew install --cask unnaturalscrollwheels
   ```
4. UnnaturalScrollWheels is ready to use now!