---
title: "Install SoundSource on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Sound and audio controller"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install SoundSource on MacOS using homebrew

- App Name: SoundSource
- App description: Sound and audio controller
- App Version: 5.3.9
- App Website: https://rogueamoeba.com/soundsource/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install SoundSource with the following command
   ```
   brew install --cask soundsource
   ```
4. SoundSource is ready to use now!