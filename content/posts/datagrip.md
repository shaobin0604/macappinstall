---
title: "Install DataGrip on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Databases & SQL IDE"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install DataGrip on MacOS using homebrew

- App Name: DataGrip
- App description: Databases & SQL IDE
- App Version: 2021.3.4,213.6777.22
- App Website: https://www.jetbrains.com/datagrip/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install DataGrip with the following command
   ```
   brew install --cask datagrip
   ```
4. DataGrip is ready to use now!