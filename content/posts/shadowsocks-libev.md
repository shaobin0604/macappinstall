---
title: "Install shadowsocks-libev on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Libev port of shadowsocks"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install shadowsocks-libev on MacOS using homebrew

- App Name: shadowsocks-libev
- App description: Libev port of shadowsocks
- App Version: 3.3.5
- App Website: https://github.com/shadowsocks/shadowsocks-libev

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install shadowsocks-libev with the following command
   ```
   brew install shadowsocks-libev
   ```
4. shadowsocks-libev is ready to use now!