---
title: "Install clinfo on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Print information about OpenCL platforms and devices"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install clinfo on MacOS using homebrew

- App Name: clinfo
- App description: Print information about OpenCL platforms and devices
- App Version: 3.0.21.02.21
- App Website: https://github.com/Oblomov/clinfo

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install clinfo with the following command
   ```
   brew install clinfo
   ```
4. clinfo is ready to use now!