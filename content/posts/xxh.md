---
title: "Install xxh on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Bring your favorite shell wherever you go through the ssh"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install xxh on MacOS using homebrew

- App Name: xxh
- App description: Bring your favorite shell wherever you go through the ssh
- App Version: 0.8.10
- App Website: https://github.com/xxh/xxh

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install xxh with the following command
   ```
   brew install xxh
   ```
4. xxh is ready to use now!