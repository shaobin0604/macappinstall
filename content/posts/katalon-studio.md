---
title: "Install Katalon Studio on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Test automation solution"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Katalon Studio on MacOS using homebrew

- App Name: Katalon Studio
- App description: Test automation solution
- App Version: 8.2.5
- App Website: https://www.katalon.com/download/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Katalon Studio with the following command
   ```
   brew install --cask katalon-studio
   ```
4. Katalon Studio is ready to use now!