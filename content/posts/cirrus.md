---
title: "Install Cirrus on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Inspector for iCloud Drive folders"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Cirrus on MacOS using homebrew

- App Name: Cirrus
- App description: Inspector for iCloud Drive folders
- App Version: 1.12,2021.04
- App Website: https://eclecticlight.co/cirrus-bailiff/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Cirrus with the following command
   ```
   brew install --cask cirrus
   ```
4. Cirrus is ready to use now!