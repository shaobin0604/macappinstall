---
title: "Install Microsoft Teams on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Meet, chat, call, and collaborate in just one place"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Microsoft Teams on MacOS using homebrew

- App Name: Microsoft Teams
- App description: Meet, chat, call, and collaborate in just one place
- App Version: 1.5.00.2570
- App Website: https://teams.microsoft.com/downloads

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Microsoft Teams with the following command
   ```
   brew install --cask microsoft-teams
   ```
4. Microsoft Teams is ready to use now!