---
title: "Install Strawberry Wallpaper on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Strawberry Wallpaper on MacOS using homebrew

- App Name: Strawberry Wallpaper
- App description: null
- App Version: 1.4.2
- App Website: https://aitexiaoy.github.io/Strawberry-Wallpaper/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Strawberry Wallpaper with the following command
   ```
   brew install --cask strawberry-wallpaper
   ```
4. Strawberry Wallpaper is ready to use now!