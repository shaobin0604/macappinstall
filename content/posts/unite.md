---
title: "Install Unite on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Turn websites into apps"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Unite on MacOS using homebrew

- App Name: Unite
- App description: Turn websites into apps
- App Version: 4.1.2
- App Website: https://bzgapps.com/unite

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Unite with the following command
   ```
   brew install --cask unite
   ```
4. Unite is ready to use now!