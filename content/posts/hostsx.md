---
title: "Install HostsX on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Local hosts update tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install HostsX on MacOS using homebrew

- App Name: HostsX
- App description: Local hosts update tool
- App Version: 2.8.0
- App Website: https://github.com/ZzzM/HostsX

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install HostsX with the following command
   ```
   brew install --cask hostsx
   ```
4. HostsX is ready to use now!