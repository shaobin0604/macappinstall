---
title: "Install git-cal on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GitHub-like contributions calendar but on the command-line"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install git-cal on MacOS using homebrew

- App Name: git-cal
- App description: GitHub-like contributions calendar but on the command-line
- App Version: 0.9.1
- App Website: https://github.com/k4rthik/git-cal

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install git-cal with the following command
   ```
   brew install git-cal
   ```
4. git-cal is ready to use now!