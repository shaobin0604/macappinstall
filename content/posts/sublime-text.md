---
title: "Install Sublime Text on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Text editor for code, markup and prose"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Sublime Text on MacOS using homebrew

- App Name: Sublime Text
- App description: Text editor for code, markup and prose
- App Version: 4.126
- App Website: https://www.sublimetext.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Sublime Text with the following command
   ```
   brew install --cask sublime-text
   ```
4. Sublime Text is ready to use now!