---
title: "Install KStars on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Astronomy software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install KStars on MacOS using homebrew

- App Name: KStars
- App description: Astronomy software
- App Version: 3.5.7
- App Website: https://edu.kde.org/kstars/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install KStars with the following command
   ```
   brew install --cask kstars
   ```
4. KStars is ready to use now!