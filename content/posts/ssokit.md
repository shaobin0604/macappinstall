---
title: "Install SSokit on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "TCP and UDP debug tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install SSokit on MacOS using homebrew

- App Name: SSokit
- App description: TCP and UDP debug tool
- App Version: 1.2.0
- App Website: https://github.com/rangaofei/SSokit-qmake

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install SSokit with the following command
   ```
   brew install --cask ssokit
   ```
4. SSokit is ready to use now!