---
title: "Install checkra1n on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Jailbreak for iPhone 5s through iPhone X, iOS 12.0 and up"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install checkra1n on MacOS using homebrew

- App Name: checkra1n
- App description: Jailbreak for iPhone 5s through iPhone X, iOS 12.0 and up
- App Version: 0.12.4
- App Website: https://checkra.in/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install checkra1n with the following command
   ```
   brew install --cask checkra1n
   ```
4. checkra1n is ready to use now!