---
title: "Install komposition on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Video editor built for screencasters"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install komposition on MacOS using homebrew

- App Name: komposition
- App description: Video editor built for screencasters
- App Version: 0.2.0
- App Website: https://github.com/owickstrom/komposition

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install komposition with the following command
   ```
   brew install komposition
   ```
4. komposition is ready to use now!