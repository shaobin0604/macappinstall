---
title: "Install GarageBuy on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "App to assist with finding, tracking, and purchasing items on eBay"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install GarageBuy on MacOS using homebrew

- App Name: GarageBuy
- App description: App to assist with finding, tracking, and purchasing items on eBay
- App Version: 3.6
- App Website: https://www.iwascoding.com/GarageBuy/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install GarageBuy with the following command
   ```
   brew install --cask garagebuy
   ```
4. GarageBuy is ready to use now!