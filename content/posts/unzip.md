---
title: "Install unzip on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Extraction utility for .zip compressed archives"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install unzip on MacOS using homebrew

- App Name: unzip
- App description: Extraction utility for .zip compressed archives
- App Version: 6.0
- App Website: https://infozip.sourceforge.io/UnZip.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install unzip with the following command
   ```
   brew install unzip
   ```
4. unzip is ready to use now!