---
title: "Install dd Utility on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Write and backup operating system IMG and ISO files"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dd Utility on MacOS using homebrew

- App Name: dd Utility
- App description: Write and backup operating system IMG and ISO files
- App Version: 1.11
- App Website: https://github.com/thefanclub/dd-utility

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dd Utility with the following command
   ```
   brew install --cask dd-utility
   ```
4. dd Utility is ready to use now!