---
title: "Install libtrng on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tina's Random Number Generator Library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libtrng on MacOS using homebrew

- App Name: libtrng
- App description: Tina's Random Number Generator Library
- App Version: 4.24
- App Website: https://www.numbercrunch.de/trng/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libtrng with the following command
   ```
   brew install libtrng
   ```
4. libtrng is ready to use now!