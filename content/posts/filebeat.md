---
title: "Install filebeat on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "File harvester to ship log files to Elasticsearch or Logstash"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install filebeat on MacOS using homebrew

- App Name: filebeat
- App description: File harvester to ship log files to Elasticsearch or Logstash
- App Version: 8.0.0
- App Website: https://www.elastic.co/products/beats/filebeat

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install filebeat with the following command
   ```
   brew install filebeat
   ```
4. filebeat is ready to use now!