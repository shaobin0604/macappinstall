---
title: "Install ZOC on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Professional SSH client and terminal emulator"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ZOC on MacOS using homebrew

- App Name: ZOC
- App description: Professional SSH client and terminal emulator
- App Version: 8.03.4
- App Website: https://www.emtec.com/zoc/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ZOC with the following command
   ```
   brew install --cask zoc
   ```
4. ZOC is ready to use now!