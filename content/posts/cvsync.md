---
title: "Install cvsync on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Portable CVS repository synchronization utility"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cvsync on MacOS using homebrew

- App Name: cvsync
- App description: Portable CVS repository synchronization utility
- App Version: 0.24.19
- App Website: https://www.cvsync.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cvsync with the following command
   ```
   brew install cvsync
   ```
4. cvsync is ready to use now!