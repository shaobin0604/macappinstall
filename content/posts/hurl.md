---
title: "Install hurl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Run and Test HTTP Requests with plain text and curl"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install hurl on MacOS using homebrew

- App Name: hurl
- App description: Run and Test HTTP Requests with plain text and curl
- App Version: 1.6.0
- App Website: https://hurl.dev

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install hurl with the following command
   ```
   brew install hurl
   ```
4. hurl is ready to use now!