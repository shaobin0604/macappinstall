---
title: "Install write-good on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Naive linter for English prose"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install write-good on MacOS using homebrew

- App Name: write-good
- App description: Naive linter for English prose
- App Version: 1.0.8
- App Website: https://github.com/btford/write-good

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install write-good with the following command
   ```
   brew install write-good
   ```
4. write-good is ready to use now!