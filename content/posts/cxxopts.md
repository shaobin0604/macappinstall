---
title: "Install cxxopts on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Lightweight C++ command-line option parser"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cxxopts on MacOS using homebrew

- App Name: cxxopts
- App description: Lightweight C++ command-line option parser
- App Version: 3.0.0
- App Website: https://github.com/jarro2783/cxxopts

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cxxopts with the following command
   ```
   brew install cxxopts
   ```
4. cxxopts is ready to use now!