---
title: "Install Jitsi on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Jitsi on MacOS using homebrew

- App Name: Jitsi
- App description: null
- App Version: 2.10.5550
- App Website: https://jitsi.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Jitsi with the following command
   ```
   brew install --cask jitsi
   ```
4. Jitsi is ready to use now!