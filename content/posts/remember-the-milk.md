---
title: "Install Remember The Milk on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "To-do app"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Remember The Milk on MacOS using homebrew

- App Name: Remember The Milk
- App description: To-do app
- App Version: 1.3.11
- App Website: https://www.rememberthemilk.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Remember The Milk with the following command
   ```
   brew install --cask remember-the-milk
   ```
4. Remember The Milk is ready to use now!