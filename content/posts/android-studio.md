---
title: "Install Android Studio on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tools for building Android applications"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Android Studio on MacOS using homebrew

- App Name: Android Studio
- App description: Tools for building Android applications
- App Version: 2021.1.1.21
- App Website: https://developer.android.com/studio/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Android Studio with the following command
   ```
   brew install --cask android-studio
   ```
4. Android Studio is ready to use now!