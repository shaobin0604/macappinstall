---
title: "Install direvent on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Monitors events in the file system directories"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install direvent on MacOS using homebrew

- App Name: direvent
- App description: Monitors events in the file system directories
- App Version: 5.2
- App Website: https://www.gnu.org.ua/software/direvent/direvent.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install direvent with the following command
   ```
   brew install direvent
   ```
4. direvent is ready to use now!