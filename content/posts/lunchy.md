---
title: "Install lunchy on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Friendly wrapper for launchctl"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install lunchy on MacOS using homebrew

- App Name: lunchy
- App description: Friendly wrapper for launchctl
- App Version: 0.10.4
- App Website: https://github.com/eddiezane/lunchy

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install lunchy with the following command
   ```
   brew install lunchy
   ```
4. lunchy is ready to use now!