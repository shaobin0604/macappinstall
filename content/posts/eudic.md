---
title: "Install Eudic on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "European dictionary"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Eudic on MacOS using homebrew

- App Name: Eudic
- App description: European dictionary
- App Version: 4.1.5,1065
- App Website: https://www.eudic.net/v4/en/app/eudic

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Eudic with the following command
   ```
   brew install --cask eudic
   ```
4. Eudic is ready to use now!