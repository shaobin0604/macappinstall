---
title: "Install abcde on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Better CD Encoder"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install abcde on MacOS using homebrew

- App Name: abcde
- App description: Better CD Encoder
- App Version: 2.9.3
- App Website: https://abcde.einval.com

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install abcde with the following command
   ```
   brew install abcde
   ```
4. abcde is ready to use now!