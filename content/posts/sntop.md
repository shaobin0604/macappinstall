---
title: "Install sntop on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Curses-based utility that polls hosts to determine connectivity"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sntop on MacOS using homebrew

- App Name: sntop
- App description: Curses-based utility that polls hosts to determine connectivity
- App Version: 1.4.3
- App Website: https://sntop.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sntop with the following command
   ```
   brew install sntop
   ```
4. sntop is ready to use now!