---
title: "Install tmux-xpanes on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Ultimate terminal divider powered by tmux"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tmux-xpanes on MacOS using homebrew

- App Name: tmux-xpanes
- App description: Ultimate terminal divider powered by tmux
- App Version: 4.1.3
- App Website: https://github.com/greymd/tmux-xpanes

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tmux-xpanes with the following command
   ```
   brew install tmux-xpanes
   ```
4. tmux-xpanes is ready to use now!