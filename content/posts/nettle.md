---
title: "Install nettle on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Low-level cryptographic library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install nettle on MacOS using homebrew

- App Name: nettle
- App description: Low-level cryptographic library
- App Version: 3.7.3
- App Website: https://www.lysator.liu.se/~nisse/nettle/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install nettle with the following command
   ```
   brew install nettle
   ```
4. nettle is ready to use now!