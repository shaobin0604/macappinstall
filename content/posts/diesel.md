---
title: "Install diesel on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line tool for Rust ORM Diesel"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install diesel on MacOS using homebrew

- App Name: diesel
- App description: Command-line tool for Rust ORM Diesel
- App Version: 1.4.8
- App Website: https://diesel.rs

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install diesel with the following command
   ```
   brew install diesel
   ```
4. diesel is ready to use now!