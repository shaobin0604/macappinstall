---
title: "Install link-grammar on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Carnegie Mellon University's link grammar parser"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install link-grammar on MacOS using homebrew

- App Name: link-grammar
- App description: Carnegie Mellon University's link grammar parser
- App Version: 5.10.2
- App Website: https://www.abisource.com/projects/link-grammar/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install link-grammar with the following command
   ```
   brew install link-grammar
   ```
4. link-grammar is ready to use now!