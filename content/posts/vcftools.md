---
title: "Install vcftools on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tools for working with VCF files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install vcftools on MacOS using homebrew

- App Name: vcftools
- App description: Tools for working with VCF files
- App Version: 0.1.16
- App Website: https://vcftools.github.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install vcftools with the following command
   ```
   brew install vcftools
   ```
4. vcftools is ready to use now!