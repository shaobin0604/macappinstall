---
title: "Install digdag on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Workload Automation System"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install digdag on MacOS using homebrew

- App Name: digdag
- App description: Workload Automation System
- App Version: 0.10.4
- App Website: https://www.digdag.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install digdag with the following command
   ```
   brew install digdag
   ```
4. digdag is ready to use now!