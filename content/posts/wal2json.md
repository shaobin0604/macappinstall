---
title: "Install wal2json on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Convert PostgreSQL changesets to JSON format"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install wal2json on MacOS using homebrew

- App Name: wal2json
- App description: Convert PostgreSQL changesets to JSON format
- App Version: 2.4
- App Website: https://github.com/eulerto/wal2json

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install wal2json with the following command
   ```
   brew install wal2json
   ```
4. wal2json is ready to use now!