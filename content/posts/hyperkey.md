---
title: "Install Hyperkey on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Convert your caps lock key or any of your modifier keys to the hyper key"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Hyperkey on MacOS using homebrew

- App Name: Hyperkey
- App description: Convert your caps lock key or any of your modifier keys to the hyper key
- App Version: 0.11
- App Website: https://hyperkey.app/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Hyperkey with the following command
   ```
   brew install --cask hyperkey
   ```
4. Hyperkey is ready to use now!