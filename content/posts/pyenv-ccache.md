---
title: "Install pyenv-ccache on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Make Python build faster, using the leverage of `ccache`"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pyenv-ccache on MacOS using homebrew

- App Name: pyenv-ccache
- App description: Make Python build faster, using the leverage of `ccache`
- App Version: 0.0.2
- App Website: https://github.com/pyenv/pyenv-ccache

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pyenv-ccache with the following command
   ```
   brew install pyenv-ccache
   ```
4. pyenv-ccache is ready to use now!