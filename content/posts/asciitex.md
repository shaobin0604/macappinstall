---
title: "Install asciitex on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Generate ASCII-art representations of mathematical equations"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install asciitex on MacOS using homebrew

- App Name: asciitex
- App description: Generate ASCII-art representations of mathematical equations
- App Version: 0.21
- App Website: https://asciitex.sourceforge.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install asciitex with the following command
   ```
   brew install asciitex
   ```
4. asciitex is ready to use now!