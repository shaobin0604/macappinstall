---
title: "Install apng2gif on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Convert APNG animations into animated GIF format"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install apng2gif on MacOS using homebrew

- App Name: apng2gif
- App description: Convert APNG animations into animated GIF format
- App Version: 1.8
- App Website: https://apng2gif.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install apng2gif with the following command
   ```
   brew install apng2gif
   ```
4. apng2gif is ready to use now!