---
title: "Install VNote on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Note-taking platform"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install VNote on MacOS using homebrew

- App Name: VNote
- App description: Note-taking platform
- App Version: 3.12.888
- App Website: https://vnotex.github.io/vnote/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install VNote with the following command
   ```
   brew install --cask vnote
   ```
4. VNote is ready to use now!