---
title: "Install libheif on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "ISO/IEC 23008-12:2017 HEIF file format decoder and encoder"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libheif on MacOS using homebrew

- App Name: libheif
- App description: ISO/IEC 23008-12:2017 HEIF file format decoder and encoder
- App Version: 1.12.0
- App Website: https://www.libde265.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libheif with the following command
   ```
   brew install libheif
   ```
4. libheif is ready to use now!