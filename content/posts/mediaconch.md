---
title: "Install mediaconch on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Conformance checker and technical metadata reporter"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mediaconch on MacOS using homebrew

- App Name: mediaconch
- App description: Conformance checker and technical metadata reporter
- App Version: 18.03.2
- App Website: https://mediaarea.net/MediaConch

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mediaconch with the following command
   ```
   brew install mediaconch
   ```
4. mediaconch is ready to use now!