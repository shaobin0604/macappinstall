---
title: "Install Spring Tool Suite on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Next generation tooling for Spring Boot"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Spring Tool Suite on MacOS using homebrew

- App Name: Spring Tool Suite
- App description: Next generation tooling for Spring Boot
- App Version: 4.13.1,4.22.0
- App Website: https://spring.io/tools

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Spring Tool Suite with the following command
   ```
   brew install --cask springtoolsuite
   ```
4. Spring Tool Suite is ready to use now!