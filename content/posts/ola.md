---
title: "Install ola on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Open Lighting Architecture for lighting control information"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ola on MacOS using homebrew

- App Name: ola
- App description: Open Lighting Architecture for lighting control information
- App Version: 0.10.8
- App Website: https://www.openlighting.org/ola/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ola with the following command
   ```
   brew install ola
   ```
4. ola is ready to use now!