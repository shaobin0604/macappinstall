---
title: "Install tilt on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Define your dev environment as code. For microservice apps on Kubernetes"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tilt on MacOS using homebrew

- App Name: tilt
- App description: Define your dev environment as code. For microservice apps on Kubernetes
- App Version: 0.25.0
- App Website: https://tilt.dev/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tilt with the following command
   ```
   brew install tilt
   ```
4. tilt is ready to use now!