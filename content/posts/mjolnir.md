---
title: "Install Mjolnir on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Lightweight automation and productivity app"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Mjolnir on MacOS using homebrew

- App Name: Mjolnir
- App description: Lightweight automation and productivity app
- App Version: 1.0.2
- App Website: https://mjolnir.rocks/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Mjolnir with the following command
   ```
   brew install --cask mjolnir
   ```
4. Mjolnir is ready to use now!