---
title: "Install mp3info on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "MP3 technical info viewer and ID3 1.x tag editor"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mp3info on MacOS using homebrew

- App Name: mp3info
- App description: MP3 technical info viewer and ID3 1.x tag editor
- App Version: 0.8.5a
- App Website: https://www.ibiblio.org/mp3info/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mp3info with the following command
   ```
   brew install mp3info
   ```
4. mp3info is ready to use now!