---
title: "Install Logos on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Bible study software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Logos on MacOS using homebrew

- App Name: Logos
- App description: Bible study software
- App Version: 9.11.0.0022
- App Website: https://www.logos.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Logos with the following command
   ```
   brew install --cask logos
   ```
4. Logos is ready to use now!