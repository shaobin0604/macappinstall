---
title: "Install libopusenc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Convenience libraray for creating .opus files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libopusenc on MacOS using homebrew

- App Name: libopusenc
- App description: Convenience libraray for creating .opus files
- App Version: 0.2.1
- App Website: https://opus-codec.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libopusenc with the following command
   ```
   brew install libopusenc
   ```
4. libopusenc is ready to use now!