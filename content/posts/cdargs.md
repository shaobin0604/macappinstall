---
title: "Install cdargs on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Directory bookmarking system - Enhanced cd utilities"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cdargs on MacOS using homebrew

- App Name: cdargs
- App description: Directory bookmarking system - Enhanced cd utilities
- App Version: 2.1
- App Website: https://github.com/cbxbiker61/cdargs

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cdargs with the following command
   ```
   brew install cdargs
   ```
4. cdargs is ready to use now!