---
title: "Install Sage on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Mathematics software system"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Sage on MacOS using homebrew

- App Name: Sage
- App description: Mathematics software system
- App Version: 9.5,1.3
- App Website: https://www.sagemath.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Sage with the following command
   ```
   brew install --cask sage
   ```
4. Sage is ready to use now!