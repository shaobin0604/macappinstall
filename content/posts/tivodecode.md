---
title: "Install tivodecode on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Convert .tivo to .mpeg"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tivodecode on MacOS using homebrew

- App Name: tivodecode
- App description: Convert .tivo to .mpeg
- App Version: 0.2pre4
- App Website: https://tivodecode.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tivodecode with the following command
   ```
   brew install tivodecode
   ```
4. tivodecode is ready to use now!