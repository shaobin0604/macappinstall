---
title: "Install jigdo on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to distribute very large files over the internet"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install jigdo on MacOS using homebrew

- App Name: jigdo
- App description: Tool to distribute very large files over the internet
- App Version: 0.7.3
- App Website: http://atterer.org/jigdo/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install jigdo with the following command
   ```
   brew install jigdo
   ```
4. jigdo is ready to use now!