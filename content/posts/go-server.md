---
title: "Install Go Server on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Server for the Go Continuous Delivery platform"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Go Server on MacOS using homebrew

- App Name: Go Server
- App description: Server for the Go Continuous Delivery platform
- App Version: 21.4.0,13469
- App Website: https://www.gocd.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Go Server with the following command
   ```
   brew install --cask go-server
   ```
4. Go Server is ready to use now!