---
title: "Install Navicat Premium on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Database administration and development tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Navicat Premium on MacOS using homebrew

- App Name: Navicat Premium
- App description: Database administration and development tool
- App Version: 16.0.7
- App Website: https://www.navicat.com/products/navicat-premium

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Navicat Premium with the following command
   ```
   brew install --cask navicat-premium
   ```
4. Navicat Premium is ready to use now!