---
title: "Install Brew Services Menubar on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Menu item for starting and stopping homebrew services"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Brew Services Menubar on MacOS using homebrew

- App Name: Brew Services Menubar
- App description: Menu item for starting and stopping homebrew services
- App Version: 4.1
- App Website: https://github.com/andrewn/brew-services-menubar

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Brew Services Menubar with the following command
   ```
   brew install --cask brewservicesmenubar
   ```
4. Brew Services Menubar is ready to use now!