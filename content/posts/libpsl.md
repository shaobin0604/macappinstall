---
title: "Install libpsl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C library for the Public Suffix List"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libpsl on MacOS using homebrew

- App Name: libpsl
- App description: C library for the Public Suffix List
- App Version: 0.21.1
- App Website: https://rockdaboot.github.io/libpsl

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libpsl with the following command
   ```
   brew install libpsl
   ```
4. libpsl is ready to use now!