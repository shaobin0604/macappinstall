---
title: "Install MKS on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Mechanical keyboard simulator"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install MKS on MacOS using homebrew

- App Name: MKS
- App description: Mechanical keyboard simulator
- App Version: 1.7
- App Website: https://github.com/x0054/MKS

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install MKS with the following command
   ```
   brew install --cask mks
   ```
4. MKS is ready to use now!