---
title: "Install openldap on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Open source suite of directory software"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install openldap on MacOS using homebrew

- App Name: openldap
- App description: Open source suite of directory software
- App Version: 2.6.1
- App Website: https://www.openldap.org/software/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install openldap with the following command
   ```
   brew install openldap
   ```
4. openldap is ready to use now!