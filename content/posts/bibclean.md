---
title: "Install bibclean on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "BibTeX bibliography file pretty printer and syntax checker"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bibclean on MacOS using homebrew

- App Name: bibclean
- App description: BibTeX bibliography file pretty printer and syntax checker
- App Version: 3.06
- App Website: https://www.math.utah.edu/~beebe/software/bibclean/bibclean-03.html#HDR.3

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bibclean with the following command
   ```
   brew install bibclean
   ```
4. bibclean is ready to use now!