---
title: "Install f3d on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Fast and minimalist 3D viewer"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install f3d on MacOS using homebrew

- App Name: f3d
- App description: Fast and minimalist 3D viewer
- App Version: 1.2.1
- App Website: https://f3d-app.github.io/f3d/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install f3d with the following command
   ```
   brew install f3d
   ```
4. f3d is ready to use now!