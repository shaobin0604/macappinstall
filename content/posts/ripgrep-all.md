---
title: "Install ripgrep-all on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Wrapper around ripgrep that adds multiple rich file types"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ripgrep-all on MacOS using homebrew

- App Name: ripgrep-all
- App description: Wrapper around ripgrep that adds multiple rich file types
- App Version: 0.9.6
- App Website: https://github.com/phiresky/ripgrep-all

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ripgrep-all with the following command
   ```
   brew install ripgrep-all
   ```
4. ripgrep-all is ready to use now!