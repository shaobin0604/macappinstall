---
title: "Install The Tagger on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install The Tagger on MacOS using homebrew

- App Name: The Tagger
- App description: null
- App Version: 1.6.2
- App Website: https://deadbeatsw.com/thetagger/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install The Tagger with the following command
   ```
   brew install --cask the-tagger
   ```
4. The Tagger is ready to use now!