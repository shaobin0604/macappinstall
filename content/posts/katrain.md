---
title: "Install KaTrain on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool for analyzing games and playing go with AI feedback from KataGo"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install KaTrain on MacOS using homebrew

- App Name: KaTrain
- App description: Tool for analyzing games and playing go with AI feedback from KataGo
- App Version: 1.10.1
- App Website: https://github.com/sanderland/katrain

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install KaTrain with the following command
   ```
   brew install --cask katrain
   ```
4. KaTrain is ready to use now!