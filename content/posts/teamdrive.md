---
title: "Install TeamDrive on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Secure cloud storage service"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install TeamDrive on MacOS using homebrew

- App Name: TeamDrive
- App description: Secure cloud storage service
- App Version: 4.7.3.3113
- App Website: https://www.teamdrive.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install TeamDrive with the following command
   ```
   brew install --cask teamdrive
   ```
4. TeamDrive is ready to use now!