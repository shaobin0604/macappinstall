---
title: "Install db-vcs on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Version control for MySQL databases"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install db-vcs on MacOS using homebrew

- App Name: db-vcs
- App description: Version control for MySQL databases
- App Version: 1.1
- App Website: https://github.com/infostreams/db

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install db-vcs with the following command
   ```
   brew install db-vcs
   ```
4. db-vcs is ready to use now!