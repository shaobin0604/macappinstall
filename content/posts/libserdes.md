---
title: "Install libserdes on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Schema ser/deserializer lib for Avro + Confluent Schema Registry"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libserdes on MacOS using homebrew

- App Name: libserdes
- App description: Schema ser/deserializer lib for Avro + Confluent Schema Registry
- App Version: 8.0.0
- App Website: https://github.com/confluentinc/libserdes

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libserdes with the following command
   ```
   brew install libserdes
   ```
4. libserdes is ready to use now!