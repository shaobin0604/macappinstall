---
title: "Install gopls on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Language server for the Go language"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gopls on MacOS using homebrew

- App Name: gopls
- App description: Language server for the Go language
- App Version: 0.7.5
- App Website: https://github.com/golang/tools/tree/master/gopls

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gopls with the following command
   ```
   brew install gopls
   ```
4. gopls is ready to use now!