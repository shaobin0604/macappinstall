---
title: "Install OpenSesame on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Graphical experiment builder for the social sciences"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install OpenSesame on MacOS using homebrew

- App Name: OpenSesame
- App description: Graphical experiment builder for the social sciences
- App Version: 3.3.10
- App Website: https://osdoc.cogsci.nl/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install OpenSesame with the following command
   ```
   brew install --cask opensesame
   ```
4. OpenSesame is ready to use now!