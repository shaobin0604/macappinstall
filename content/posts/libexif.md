---
title: "Install libexif on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "EXIF parsing library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libexif on MacOS using homebrew

- App Name: libexif
- App description: EXIF parsing library
- App Version: 0.6.24
- App Website: https://libexif.github.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libexif with the following command
   ```
   brew install libexif
   ```
4. libexif is ready to use now!