---
title: "Install forego on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Foreman in Go for Procfile-based application management"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install forego on MacOS using homebrew

- App Name: forego
- App description: Foreman in Go for Procfile-based application management
- App Version: 20180216151118
- App Website: https://github.com/ddollar/forego

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install forego with the following command
   ```
   brew install forego
   ```
4. forego is ready to use now!