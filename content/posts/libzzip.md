---
title: "Install libzzip on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library providing read access on ZIP-archives"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libzzip on MacOS using homebrew

- App Name: libzzip
- App description: Library providing read access on ZIP-archives
- App Version: 0.13.72
- App Website: https://github.com/gdraheim/zziplib

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libzzip with the following command
   ```
   brew install libzzip
   ```
4. libzzip is ready to use now!