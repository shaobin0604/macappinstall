---
title: "Install s4cmd on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Super S3 command-line tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install s4cmd on MacOS using homebrew

- App Name: s4cmd
- App description: Super S3 command-line tool
- App Version: 2.1.0
- App Website: https://github.com/bloomreach/s4cmd

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install s4cmd with the following command
   ```
   brew install s4cmd
   ```
4. s4cmd is ready to use now!