---
title: "Install mycli on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CLI for MySQL with auto-completion and syntax highlighting"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mycli on MacOS using homebrew

- App Name: mycli
- App description: CLI for MySQL with auto-completion and syntax highlighting
- App Version: 1.24.3
- App Website: https://www.mycli.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mycli with the following command
   ```
   brew install mycli
   ```
4. mycli is ready to use now!