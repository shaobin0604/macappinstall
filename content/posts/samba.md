---
title: "Install samba on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "SMB/CIFS file, print, and login server for UNIX"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install samba on MacOS using homebrew

- App Name: samba
- App description: SMB/CIFS file, print, and login server for UNIX
- App Version: 4.15.5
- App Website: https://www.samba.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install samba with the following command
   ```
   brew install samba
   ```
4. samba is ready to use now!