---
title: "Install Therm on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Fork of iTerm2 that aims to have good defaults and minimal features"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Therm on MacOS using homebrew

- App Name: Therm
- App description: Fork of iTerm2 that aims to have good defaults and minimal features
- App Version: 0.4.2
- App Website: https://github.com/trufae/Therm

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Therm with the following command
   ```
   brew install --cask therm
   ```
4. Therm is ready to use now!