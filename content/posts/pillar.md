---
title: "Install pillar on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Manage migrations for Cassandra data stores"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pillar on MacOS using homebrew

- App Name: pillar
- App description: Manage migrations for Cassandra data stores
- App Version: 2.3.0
- App Website: https://github.com/comeara/pillar

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pillar with the following command
   ```
   brew install pillar
   ```
4. pillar is ready to use now!