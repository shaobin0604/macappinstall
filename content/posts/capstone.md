---
title: "Install capstone on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Multi-platform, multi-architecture disassembly framework"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install capstone on MacOS using homebrew

- App Name: capstone
- App description: Multi-platform, multi-architecture disassembly framework
- App Version: 4.0.2
- App Website: https://www.capstone-engine.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install capstone with the following command
   ```
   brew install capstone
   ```
4. capstone is ready to use now!