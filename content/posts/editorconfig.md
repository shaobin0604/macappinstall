---
title: "Install editorconfig on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Maintain consistent coding style between multiple editors"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install editorconfig on MacOS using homebrew

- App Name: editorconfig
- App description: Maintain consistent coding style between multiple editors
- App Version: 0.12.5
- App Website: https://editorconfig.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install editorconfig with the following command
   ```
   brew install editorconfig
   ```
4. editorconfig is ready to use now!