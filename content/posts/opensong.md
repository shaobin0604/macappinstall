---
title: "Install OpenSong on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Presentation software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install OpenSong on MacOS using homebrew

- App Name: OpenSong
- App description: Presentation software
- App Version: 3.4.8
- App Website: http://www.opensong.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install OpenSong with the following command
   ```
   brew install --cask opensong
   ```
4. OpenSong is ready to use now!