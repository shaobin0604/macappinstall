---
title: "Install cdlabelgen on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CD/DVD inserts and envelopes"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cdlabelgen on MacOS using homebrew

- App Name: cdlabelgen
- App description: CD/DVD inserts and envelopes
- App Version: 4.3.0
- App Website: https://www.aczoom.com/tools/cdinsert/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cdlabelgen with the following command
   ```
   brew install cdlabelgen
   ```
4. cdlabelgen is ready to use now!