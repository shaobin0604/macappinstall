---
title: "Install unbound on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Validating, recursive, caching DNS resolver"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install unbound on MacOS using homebrew

- App Name: unbound
- App description: Validating, recursive, caching DNS resolver
- App Version: 1.15.0
- App Website: https://www.unbound.net

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install unbound with the following command
   ```
   brew install unbound
   ```
4. unbound is ready to use now!