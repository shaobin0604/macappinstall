---
title: "Install brag on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Download and assemble multipart binaries from newsgroups"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install brag on MacOS using homebrew

- App Name: brag
- App description: Download and assemble multipart binaries from newsgroups
- App Version: 1.4.3
- App Website: https://brag.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install brag with the following command
   ```
   brew install brag
   ```
4. brag is ready to use now!