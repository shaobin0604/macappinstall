---
title: "Install Anka Virtualization on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CLI tool for managing and creating virtual machines"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Anka Virtualization on MacOS using homebrew

- App Name: Anka Virtualization
- App description: CLI tool for managing and creating virtual machines
- App Version: 2.5.4.139
- App Website: https://veertu.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Anka Virtualization with the following command
   ```
   brew install --cask anka-virtualization
   ```
4. Anka Virtualization is ready to use now!