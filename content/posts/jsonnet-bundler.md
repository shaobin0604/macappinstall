---
title: "Install jsonnet-bundler on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Package manager for Jsonnet"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install jsonnet-bundler on MacOS using homebrew

- App Name: jsonnet-bundler
- App description: Package manager for Jsonnet
- App Version: 0.4.0
- App Website: https://github.com/jsonnet-bundler/jsonnet-bundler

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install jsonnet-bundler with the following command
   ```
   brew install jsonnet-bundler
   ```
4. jsonnet-bundler is ready to use now!