---
title: "Install flamebearer on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Blazing fast flame graph tool for V8 and Node"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install flamebearer on MacOS using homebrew

- App Name: flamebearer
- App description: Blazing fast flame graph tool for V8 and Node
- App Version: 1.1.3
- App Website: https://github.com/mapbox/flamebearer

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install flamebearer with the following command
   ```
   brew install flamebearer
   ```
4. flamebearer is ready to use now!