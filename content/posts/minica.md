---
title: "Install minica on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Small, simple certificate authority"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install minica on MacOS using homebrew

- App Name: minica
- App description: Small, simple certificate authority
- App Version: 1.0.2
- App Website: https://github.com/jsha/minica

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install minica with the following command
   ```
   brew install minica
   ```
4. minica is ready to use now!