---
title: "Install cntlm on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "NTLM authentication proxy with tunneling"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cntlm on MacOS using homebrew

- App Name: cntlm
- App description: NTLM authentication proxy with tunneling
- App Version: 0.92.3
- App Website: https://cntlm.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cntlm with the following command
   ```
   brew install cntlm
   ```
4. cntlm is ready to use now!