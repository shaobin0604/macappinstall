---
title: "Install OpenEmu on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Retro video game emulation"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install OpenEmu on MacOS using homebrew

- App Name: OpenEmu
- App description: Retro video game emulation
- App Version: 2.3.3
- App Website: https://openemu.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install OpenEmu with the following command
   ```
   brew install --cask openemu
   ```
4. OpenEmu is ready to use now!