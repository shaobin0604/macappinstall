---
title: "Install rav1e on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Fastest and safest AV1 video encoder"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install rav1e on MacOS using homebrew

- App Name: rav1e
- App description: Fastest and safest AV1 video encoder
- App Version: 0.5.1
- App Website: https://github.com/xiph/rav1e

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install rav1e with the following command
   ```
   brew install rav1e
   ```
4. rav1e is ready to use now!