---
title: "Install fsevents-tools on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line utilities for the FSEvents API"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fsevents-tools on MacOS using homebrew

- App Name: fsevents-tools
- App description: Command-line utilities for the FSEvents API
- App Version: 1.0.0
- App Website: https://geoff.greer.fm/fsevents/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fsevents-tools with the following command
   ```
   brew install fsevents-tools
   ```
4. fsevents-tools is ready to use now!