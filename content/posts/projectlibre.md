---
title: "Install ProjectLibre on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Microsoft Project in your browser"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ProjectLibre on MacOS using homebrew

- App Name: ProjectLibre
- App description: Microsoft Project in your browser
- App Version: 1.9.3
- App Website: https://www.projectlibre.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ProjectLibre with the following command
   ```
   brew install --cask projectlibre
   ```
4. ProjectLibre is ready to use now!