---
title: "Install at-spi2-atk on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Accessibility Toolkit GTK+ module"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install at-spi2-atk on MacOS using homebrew

- App Name: at-spi2-atk
- App description: Accessibility Toolkit GTK+ module
- App Version: 2.38.0
- App Website: https://www.freedesktop.org/wiki/Accessibility/AT-SPI2

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install at-spi2-atk with the following command
   ```
   brew install at-spi2-atk
   ```
4. at-spi2-atk is ready to use now!