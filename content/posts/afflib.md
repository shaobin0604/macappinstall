---
title: "Install afflib on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Advanced Forensic Format"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install afflib on MacOS using homebrew

- App Name: afflib
- App description: Advanced Forensic Format
- App Version: 3.7.19
- App Website: https://github.com/sshock/AFFLIBv3

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install afflib with the following command
   ```
   brew install afflib
   ```
4. afflib is ready to use now!