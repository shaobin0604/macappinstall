---
title: "Install BlueWallet on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Bitcoin wallet and Lightning wallet"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install BlueWallet on MacOS using homebrew

- App Name: BlueWallet
- App description: Bitcoin wallet and Lightning wallet
- App Version: 6.2.18
- App Website: https://bluewallet.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install BlueWallet with the following command
   ```
   brew install --cask bluewallet
   ```
4. BlueWallet is ready to use now!