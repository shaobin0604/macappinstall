---
title: "Install pdfsandwich on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Generate sandwich OCR PDFs from scanned file"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pdfsandwich on MacOS using homebrew

- App Name: pdfsandwich
- App description: Generate sandwich OCR PDFs from scanned file
- App Version: 0.1.7
- App Website: http://www.tobias-elze.de/pdfsandwich/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pdfsandwich with the following command
   ```
   brew install pdfsandwich
   ```
4. pdfsandwich is ready to use now!