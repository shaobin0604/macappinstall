---
title: "Install msgpack-tools on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line tools for converting between MessagePack and JSON"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install msgpack-tools on MacOS using homebrew

- App Name: msgpack-tools
- App description: Command-line tools for converting between MessagePack and JSON
- App Version: 0.6
- App Website: https://github.com/ludocode/msgpack-tools

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install msgpack-tools with the following command
   ```
   brew install msgpack-tools
   ```
4. msgpack-tools is ready to use now!