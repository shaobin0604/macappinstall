---
title: "Install glulxe on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Portable VM like the Z-machine"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install glulxe on MacOS using homebrew

- App Name: glulxe
- App description: Portable VM like the Z-machine
- App Version: 0.5.4
- App Website: https://www.eblong.com/zarf/glulx/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install glulxe with the following command
   ```
   brew install glulxe
   ```
4. glulxe is ready to use now!