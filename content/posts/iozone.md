---
title: "Install iozone on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "File system benchmark tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install iozone on MacOS using homebrew

- App Name: iozone
- App description: File system benchmark tool
- App Version: 3.493
- App Website: https://www.iozone.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install iozone with the following command
   ```
   brew install iozone
   ```
4. iozone is ready to use now!