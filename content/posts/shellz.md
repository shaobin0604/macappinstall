---
title: "Install shellz on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Small utility to track and control custom shellz"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install shellz on MacOS using homebrew

- App Name: shellz
- App description: Small utility to track and control custom shellz
- App Version: 1.6.0
- App Website: https://github.com/evilsocket/shellz

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install shellz with the following command
   ```
   brew install shellz
   ```
4. shellz is ready to use now!