---
title: "Install pyright on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Static type checker for Python"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pyright on MacOS using homebrew

- App Name: pyright
- App description: Static type checker for Python
- App Version: 1.1.222
- App Website: https://github.com/microsoft/pyright

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pyright with the following command
   ```
   brew install pyright
   ```
4. pyright is ready to use now!