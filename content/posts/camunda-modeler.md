---
title: "Install Camunda Modeler on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Workflow and Decision Automation Platform"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Camunda Modeler on MacOS using homebrew

- App Name: Camunda Modeler
- App description: Workflow and Decision Automation Platform
- App Version: 4.12.0
- App Website: https://camunda.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Camunda Modeler with the following command
   ```
   brew install --cask camunda-modeler
   ```
4. Camunda Modeler is ready to use now!