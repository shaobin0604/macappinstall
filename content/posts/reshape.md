---
title: "Install reshape on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Easy-to-use, zero-downtime schema migration tool for Postgres"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install reshape on MacOS using homebrew

- App Name: reshape
- App description: Easy-to-use, zero-downtime schema migration tool for Postgres
- App Version: 0.3.1
- App Website: https://github.com/fabianlindfors/reshape

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install reshape with the following command
   ```
   brew install reshape
   ```
4. reshape is ready to use now!