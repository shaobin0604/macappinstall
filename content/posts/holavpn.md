---
title: "Install Hola VPN on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Peer-to-peer VPN"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Hola VPN on MacOS using homebrew

- App Name: Hola VPN
- App description: Peer-to-peer VPN
- App Version: 2.45,1.193.481
- App Website: https://hola.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Hola VPN with the following command
   ```
   brew install --cask holavpn
   ```
4. Hola VPN is ready to use now!