---
title: "Install Mathpix Snipping Tool on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Scanner app for math and science"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Mathpix Snipping Tool on MacOS using homebrew

- App Name: Mathpix Snipping Tool
- App description: Scanner app for math and science
- App Version: 3.4.1,341.5
- App Website: https://mathpix.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Mathpix Snipping Tool with the following command
   ```
   brew install --cask mathpix-snipping-tool
   ```
4. Mathpix Snipping Tool is ready to use now!