---
title: "Install pastel on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line tool to generate, analyze, convert and manipulate colors"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pastel on MacOS using homebrew

- App Name: pastel
- App description: Command-line tool to generate, analyze, convert and manipulate colors
- App Version: 0.8.1
- App Website: https://github.com/sharkdp/pastel

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pastel with the following command
   ```
   brew install pastel
   ```
4. pastel is ready to use now!