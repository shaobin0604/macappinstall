---
title: "Install lgogdownloader on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Unofficial downloader for GOG.com games"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install lgogdownloader on MacOS using homebrew

- App Name: lgogdownloader
- App description: Unofficial downloader for GOG.com games
- App Version: 3.8
- App Website: https://sites.google.com/site/gogdownloader/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install lgogdownloader with the following command
   ```
   brew install lgogdownloader
   ```
4. lgogdownloader is ready to use now!