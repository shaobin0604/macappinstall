---
title: "Install iproute2mac on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CLI wrapper for basic network utilities on macOS - ip command"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install iproute2mac on MacOS using homebrew

- App Name: iproute2mac
- App description: CLI wrapper for basic network utilities on macOS - ip command
- App Version: 1.3.0
- App Website: https://github.com/brona/iproute2mac

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install iproute2mac with the following command
   ```
   brew install iproute2mac
   ```
4. iproute2mac is ready to use now!