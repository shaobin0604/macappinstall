---
title: "Install chrome-export on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Convert Chrome’s bookmarks and history to HTML bookmarks files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install chrome-export on MacOS using homebrew

- App Name: chrome-export
- App description: Convert Chrome’s bookmarks and history to HTML bookmarks files
- App Version: 2.0.2
- App Website: https://github.com/bdesham/chrome-export

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install chrome-export with the following command
   ```
   brew install chrome-export
   ```
4. chrome-export is ready to use now!