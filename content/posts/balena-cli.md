---
title: "Install balena-cli on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line tool for interacting with the balenaCloud and balena API"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install balena-cli on MacOS using homebrew

- App Name: balena-cli
- App description: Command-line tool for interacting with the balenaCloud and balena API
- App Version: 12.25.0
- App Website: https://www.balena.io/docs/reference/cli/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install balena-cli with the following command
   ```
   brew install balena-cli
   ```
4. balena-cli is ready to use now!