---
title: "Install plow on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "High-performance and real-time metrics displaying HTTP benchmarking tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install plow on MacOS using homebrew

- App Name: plow
- App description: High-performance and real-time metrics displaying HTTP benchmarking tool
- App Version: 1.1.0
- App Website: https://github.com/six-ddc/plow

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install plow with the following command
   ```
   brew install plow
   ```
4. plow is ready to use now!