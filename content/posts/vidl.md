---
title: "Install ViDL for Mac on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GUI frontend for youtube-dl"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ViDL for Mac on MacOS using homebrew

- App Name: ViDL for Mac
- App description: GUI frontend for youtube-dl
- App Version: 1.0.2,102000
- App Website: https://omz-software.com/vidl/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ViDL for Mac with the following command
   ```
   brew install --cask vidl
   ```
4. ViDL for Mac is ready to use now!