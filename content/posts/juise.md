---
title: "Install juise on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "JUNOS user interface scripting environment"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install juise on MacOS using homebrew

- App Name: juise
- App description: JUNOS user interface scripting environment
- App Version: 0.9.0
- App Website: https://github.com/Juniper/juise/wiki

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install juise with the following command
   ```
   brew install juise
   ```
4. juise is ready to use now!