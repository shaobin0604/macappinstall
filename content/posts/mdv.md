---
title: "Install mdv on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Styled terminal markdown viewer"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mdv on MacOS using homebrew

- App Name: mdv
- App description: Styled terminal markdown viewer
- App Version: 1.7.4
- App Website: https://github.com/axiros/terminal_markdown_viewer

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mdv with the following command
   ```
   brew install mdv
   ```
4. mdv is ready to use now!