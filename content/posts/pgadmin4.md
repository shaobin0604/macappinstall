---
title: "Install pgAdmin4 on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Administration and development platform for PostgreSQL"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pgAdmin4 on MacOS using homebrew

- App Name: pgAdmin4
- App description: Administration and development platform for PostgreSQL
- App Version: 6.5
- App Website: https://www.pgadmin.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pgAdmin4 with the following command
   ```
   brew install --cask pgadmin4
   ```
4. pgAdmin4 is ready to use now!