---
title: "Install dcm2niix on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "DICOM to NIfTI converter"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dcm2niix on MacOS using homebrew

- App Name: dcm2niix
- App description: DICOM to NIfTI converter
- App Version: 1.0.20211006
- App Website: https://www.nitrc.org/plugins/mwiki/index.php/dcm2nii:MainPage

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dcm2niix with the following command
   ```
   brew install dcm2niix
   ```
4. dcm2niix is ready to use now!