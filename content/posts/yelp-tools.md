---
title: "Install yelp-tools on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tools that help create and edit Mallard or DocBook documentation"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install yelp-tools on MacOS using homebrew

- App Name: yelp-tools
- App description: Tools that help create and edit Mallard or DocBook documentation
- App Version: 41.0
- App Website: https://github.com/GNOME/yelp-tools

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install yelp-tools with the following command
   ```
   brew install yelp-tools
   ```
4. yelp-tools is ready to use now!