---
title: "Install jags on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Just Another Gibbs Sampler for Bayesian MCMC simulation"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install jags on MacOS using homebrew

- App Name: jags
- App description: Just Another Gibbs Sampler for Bayesian MCMC simulation
- App Version: 4.3.0
- App Website: https://mcmc-jags.sourceforge.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install jags with the following command
   ```
   brew install jags
   ```
4. jags is ready to use now!