---
title: "Install mac-robber on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Digital investigation tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mac-robber on MacOS using homebrew

- App Name: mac-robber
- App description: Digital investigation tool
- App Version: 1.02
- App Website: https://www.sleuthkit.org/mac-robber/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mac-robber with the following command
   ```
   brew install mac-robber
   ```
4. mac-robber is ready to use now!