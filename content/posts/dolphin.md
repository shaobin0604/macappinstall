---
title: "Install Dolphin on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Emulator to play GameCube and Wii games"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Dolphin on MacOS using homebrew

- App Name: Dolphin
- App description: Emulator to play GameCube and Wii games
- App Version: 5.0
- App Website: https://dolphin-emu.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Dolphin with the following command
   ```
   brew install --cask dolphin
   ```
4. Dolphin is ready to use now!