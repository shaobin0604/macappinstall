---
title: "Install ex-vi on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "UTF8-friendly version of tradition vi"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ex-vi on MacOS using homebrew

- App Name: ex-vi
- App description: UTF8-friendly version of tradition vi
- App Version: 050325
- App Website: https://ex-vi.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ex-vi with the following command
   ```
   brew install ex-vi
   ```
4. ex-vi is ready to use now!