---
title: "Install MusicBrainz Picard on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Music tagger"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install MusicBrainz Picard on MacOS using homebrew

- App Name: MusicBrainz Picard
- App description: Music tagger
- App Version: 2.7.3
- App Website: https://picard.musicbrainz.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install MusicBrainz Picard with the following command
   ```
   brew install --cask musicbrainz-picard
   ```
4. MusicBrainz Picard is ready to use now!