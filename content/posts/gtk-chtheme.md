---
title: "Install gtk-chtheme on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GTK+ 2.0 theme changer GUI"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gtk-chtheme on MacOS using homebrew

- App Name: gtk-chtheme
- App description: GTK+ 2.0 theme changer GUI
- App Version: 0.3.1
- App Website: http://plasmasturm.org/code/gtk-chtheme/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gtk-chtheme with the following command
   ```
   brew install gtk-chtheme
   ```
4. gtk-chtheme is ready to use now!