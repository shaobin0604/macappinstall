---
title: "Install Praat on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Doing phonetics by computer"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Praat on MacOS using homebrew

- App Name: Praat
- App description: Doing phonetics by computer
- App Version: 6.2.09
- App Website: https://www.fon.hum.uva.nl/praat/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Praat with the following command
   ```
   brew install --cask praat
   ```
4. Praat is ready to use now!