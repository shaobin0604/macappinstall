---
title: "Install kettle on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Pentaho Data Integration software"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install kettle on MacOS using homebrew

- App Name: kettle
- App description: Pentaho Data Integration software
- App Version: 9.2.0.0-290
- App Website: https://www.hitachivantara.com/en-us/products/data-management-analytics.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install kettle with the following command
   ```
   brew install kettle
   ```
4. kettle is ready to use now!