---
title: "Install BatChmod on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utility for manipulating file and folder privileges"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install BatChmod on MacOS using homebrew

- App Name: BatChmod
- App description: Utility for manipulating file and folder privileges
- App Version: 1.7b5,175
- App Website: https://www.lagentesoft.com/batchmod/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install BatChmod with the following command
   ```
   brew install --cask batchmod
   ```
4. BatChmod is ready to use now!