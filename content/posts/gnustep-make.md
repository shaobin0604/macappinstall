---
title: "Install gnustep-make on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Basic GNUstep Makefiles"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gnustep-make on MacOS using homebrew

- App Name: gnustep-make
- App description: Basic GNUstep Makefiles
- App Version: 2.9.0
- App Website: http://gnustep.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gnustep-make with the following command
   ```
   brew install gnustep-make
   ```
4. gnustep-make is ready to use now!