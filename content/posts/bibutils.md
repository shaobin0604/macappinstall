---
title: "Install bibutils on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Bibliography conversion utilities"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bibutils on MacOS using homebrew

- App Name: bibutils
- App description: Bibliography conversion utilities
- App Version: 7.2
- App Website: https://sourceforge.net/p/bibutils/home/Bibutils/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bibutils with the following command
   ```
   brew install bibutils
   ```
4. bibutils is ready to use now!