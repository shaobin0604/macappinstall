---
title: "Install premake on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Write once, build anywhere Lua-based build system"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install premake on MacOS using homebrew

- App Name: premake
- App description: Write once, build anywhere Lua-based build system
- App Version: 4.4-beta5
- App Website: https://premake.github.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install premake with the following command
   ```
   brew install premake
   ```
4. premake is ready to use now!