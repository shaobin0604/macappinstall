---
title: "Install Wireshark-ChmodBPF on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Network protocol analyzer"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Wireshark-ChmodBPF on MacOS using homebrew

- App Name: Wireshark-ChmodBPF
- App description: Network protocol analyzer
- App Version: 3.6.2
- App Website: https://www.wireshark.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Wireshark-ChmodBPF with the following command
   ```
   brew install --cask wireshark-chmodbpf
   ```
4. Wireshark-ChmodBPF is ready to use now!