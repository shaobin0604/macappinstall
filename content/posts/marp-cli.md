---
title: "Install marp-cli on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Easily convert Marp Markdown files into static HTML/CSS, PDF, PPT and images"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install marp-cli on MacOS using homebrew

- App Name: marp-cli
- App description: Easily convert Marp Markdown files into static HTML/CSS, PDF, PPT and images
- App Version: 1.6.0
- App Website: https://github.com/marp-team/marp-cli

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install marp-cli with the following command
   ```
   brew install marp-cli
   ```
4. marp-cli is ready to use now!