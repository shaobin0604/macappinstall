---
title: "Install psftools on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tools for fixed-width bitmap fonts"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install psftools on MacOS using homebrew

- App Name: psftools
- App description: Tools for fixed-width bitmap fonts
- App Version: 1.0.14
- App Website: https://www.seasip.info/Unix/PSF/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install psftools with the following command
   ```
   brew install psftools
   ```
4. psftools is ready to use now!