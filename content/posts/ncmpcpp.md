---
title: "Install ncmpcpp on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Ncurses-based client for the Music Player Daemon"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ncmpcpp on MacOS using homebrew

- App Name: ncmpcpp
- App description: Ncurses-based client for the Music Player Daemon
- App Version: 0.9.2
- App Website: https://rybczak.net/ncmpcpp/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ncmpcpp with the following command
   ```
   brew install ncmpcpp
   ```
4. ncmpcpp is ready to use now!