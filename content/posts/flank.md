---
title: "Install flank on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Massively parallel Android and iOS test runner for Firebase Test Lab"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install flank on MacOS using homebrew

- App Name: flank
- App description: Massively parallel Android and iOS test runner for Firebase Test Lab
- App Version: 21.11.0
- App Website: https://github.com/Flank/flank

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install flank with the following command
   ```
   brew install flank
   ```
4. flank is ready to use now!