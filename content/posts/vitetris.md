---
title: "Install vitetris on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Terminal-based Tetris clone"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install vitetris on MacOS using homebrew

- App Name: vitetris
- App description: Terminal-based Tetris clone
- App Version: 0.59.1
- App Website: https://www.victornils.net/tetris/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install vitetris with the following command
   ```
   brew install vitetris
   ```
4. vitetris is ready to use now!