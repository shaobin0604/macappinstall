---
title: "Install opensearch-dashboards on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Open source visualization dashboards for OpenSearch"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install opensearch-dashboards on MacOS using homebrew

- App Name: opensearch-dashboards
- App description: Open source visualization dashboards for OpenSearch
- App Version: 1.2.0
- App Website: https://opensearch.org/docs/dashboards/index/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install opensearch-dashboards with the following command
   ```
   brew install opensearch-dashboards
   ```
4. opensearch-dashboards is ready to use now!