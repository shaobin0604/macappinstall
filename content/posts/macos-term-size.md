---
title: "Install macos-term-size on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Get the terminal window size on macOS"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install macos-term-size on MacOS using homebrew

- App Name: macos-term-size
- App description: Get the terminal window size on macOS
- App Version: 1.0.0
- App Website: https://github.com/sindresorhus/macos-term-size

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install macos-term-size with the following command
   ```
   brew install macos-term-size
   ```
4. macos-term-size is ready to use now!