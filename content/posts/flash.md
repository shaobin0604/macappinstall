---
title: "Install flash on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line script to flash SD card images of any kind"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install flash on MacOS using homebrew

- App Name: flash
- App description: Command-line script to flash SD card images of any kind
- App Version: 2.7.2
- App Website: https://github.com/hypriot/flash

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install flash with the following command
   ```
   brew install flash
   ```
4. flash is ready to use now!