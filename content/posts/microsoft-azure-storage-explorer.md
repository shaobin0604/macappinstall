---
title: "Install Microsoft Azure Storage Explorer on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Explorer for Azure Storage"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Microsoft Azure Storage Explorer on MacOS using homebrew

- App Name: Microsoft Azure Storage Explorer
- App description: Explorer for Azure Storage
- App Version: 1.22.1
- App Website: https://azure.microsoft.com/en-us/features/storage-explorer/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Microsoft Azure Storage Explorer with the following command
   ```
   brew install --cask microsoft-azure-storage-explorer
   ```
4. Microsoft Azure Storage Explorer is ready to use now!