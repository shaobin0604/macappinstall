---
title: "Install psutils on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Collection of PostScript document handling utilities"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install psutils on MacOS using homebrew

- App Name: psutils
- App description: Collection of PostScript document handling utilities
- App Version: p17
- App Website: http://knackered.org/angus/psutils/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install psutils with the following command
   ```
   brew install psutils
   ```
4. psutils is ready to use now!