---
title: "Install Bria on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Softphone application"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Bria on MacOS using homebrew

- App Name: Bria
- App description: Softphone application
- App Version: 6.5.2,109387
- App Website: https://www.counterpath.com/bria-solo/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Bria with the following command
   ```
   brew install --cask bria
   ```
4. Bria is ready to use now!