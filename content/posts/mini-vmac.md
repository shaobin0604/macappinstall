---
title: "Install Mini vMac on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Allows modern computers to run software made for early Apple computers"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Mini vMac on MacOS using homebrew

- App Name: Mini vMac
- App description: Allows modern computers to run software made for early Apple computers
- App Version: 36.04
- App Website: https://www.gryphel.com/c/minivmac/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Mini vMac with the following command
   ```
   brew install --cask mini-vmac
   ```
4. Mini vMac is ready to use now!