---
title: "Install velero on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Disaster recovery for Kubernetes resources and persistent volumes"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install velero on MacOS using homebrew

- App Name: velero
- App description: Disaster recovery for Kubernetes resources and persistent volumes
- App Version: 1.8.0
- App Website: https://github.com/vmware-tanzu/velero

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install velero with the following command
   ```
   brew install velero
   ```
4. velero is ready to use now!