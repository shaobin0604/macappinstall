---
title: "Install nuxeo on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Enterprise Content Management"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install nuxeo on MacOS using homebrew

- App Name: nuxeo
- App description: Enterprise Content Management
- App Version: 10.10
- App Website: https://nuxeo.github.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install nuxeo with the following command
   ```
   brew install nuxeo
   ```
4. nuxeo is ready to use now!