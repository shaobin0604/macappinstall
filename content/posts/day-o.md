---
title: "Install Day-O on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Day-O on MacOS using homebrew

- App Name: Day-O
- App description: null
- App Version: 3.0.1
- App Website: https://shauninman.com/archive/2020/04/08/day_o_mac_menu_bar_clock_for_catalina

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Day-O with the following command
   ```
   brew install --cask day-o
   ```
4. Day-O is ready to use now!