---
title: "Install jsonschema2pojo on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Generates Java types from JSON Schema (or example JSON)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install jsonschema2pojo on MacOS using homebrew

- App Name: jsonschema2pojo
- App description: Generates Java types from JSON Schema (or example JSON)
- App Version: 1.1.1
- App Website: https://www.jsonschema2pojo.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install jsonschema2pojo with the following command
   ```
   brew install jsonschema2pojo
   ```
4. jsonschema2pojo is ready to use now!