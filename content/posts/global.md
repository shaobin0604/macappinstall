---
title: "Install global on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Source code tag system"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install global on MacOS using homebrew

- App Name: global
- App description: Source code tag system
- App Version: 6.6.8
- App Website: https://www.gnu.org/software/global/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install global with the following command
   ```
   brew install global
   ```
4. global is ready to use now!