---
title: "Install nifi on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Easy to use, powerful, and reliable system to process and distribute data"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install nifi on MacOS using homebrew

- App Name: nifi
- App description: Easy to use, powerful, and reliable system to process and distribute data
- App Version: 1.15.3
- App Website: https://nifi.apache.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install nifi with the following command
   ```
   brew install nifi
   ```
4. nifi is ready to use now!