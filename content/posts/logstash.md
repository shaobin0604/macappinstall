---
title: "Install logstash on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool for managing events and logs"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install logstash on MacOS using homebrew

- App Name: logstash
- App description: Tool for managing events and logs
- App Version: 7.15.2
- App Website: https://www.elastic.co/products/logstash

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install logstash with the following command
   ```
   brew install logstash
   ```
4. logstash is ready to use now!