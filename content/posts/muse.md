---
title: "Install Muse on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Open-source Spotify controller with TouchBar support"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Muse on MacOS using homebrew

- App Name: Muse
- App description: Open-source Spotify controller with TouchBar support
- App Version: 4.1.1
- App Website: https://github.com/xzzz9097/Muse

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Muse with the following command
   ```
   brew install --cask muse
   ```
4. Muse is ready to use now!