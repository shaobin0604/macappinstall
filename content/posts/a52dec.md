---
title: "Install a52dec on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library for decoding ATSC A/52 streams (AKA 'AC-3')"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install a52dec on MacOS using homebrew

- App Name: a52dec
- App description: Library for decoding ATSC A/52 streams (AKA 'AC-3')
- App Version: 0.7.4
- App Website: https://liba52.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install a52dec with the following command
   ```
   brew install a52dec
   ```
4. a52dec is ready to use now!