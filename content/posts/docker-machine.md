---
title: "Install docker-machine on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Create Docker hosts locally and on cloud providers"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install docker-machine on MacOS using homebrew

- App Name: docker-machine
- App description: Create Docker hosts locally and on cloud providers
- App Version: 0.16.2
- App Website: https://docs.docker.com/machine

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install docker-machine with the following command
   ```
   brew install docker-machine
   ```
4. docker-machine is ready to use now!