---
title: "Install up on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool for writing command-line pipes with instant live preview"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install up on MacOS using homebrew

- App Name: up
- App description: Tool for writing command-line pipes with instant live preview
- App Version: 0.4
- App Website: https://github.com/akavel/up

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install up with the following command
   ```
   brew install up
   ```
4. up is ready to use now!