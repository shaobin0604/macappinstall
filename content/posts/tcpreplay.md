---
title: "Install tcpreplay on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Replay saved tcpdump files at arbitrary speeds"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tcpreplay on MacOS using homebrew

- App Name: tcpreplay
- App description: Replay saved tcpdump files at arbitrary speeds
- App Version: 4.4.1
- App Website: https://tcpreplay.appneta.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tcpreplay with the following command
   ```
   brew install tcpreplay
   ```
4. tcpreplay is ready to use now!