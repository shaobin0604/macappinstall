---
title: "Install amazon-ecs-cli on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CLI for Amazon ECS to manage clusters and tasks for development"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install amazon-ecs-cli on MacOS using homebrew

- App Name: amazon-ecs-cli
- App description: CLI for Amazon ECS to manage clusters and tasks for development
- App Version: 1.21.0
- App Website: https://aws.amazon.com/ecs

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install amazon-ecs-cli with the following command
   ```
   brew install amazon-ecs-cli
   ```
4. amazon-ecs-cli is ready to use now!