---
title: "Install Cloudflare WARP on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Free app that makes your Internet safer"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Cloudflare WARP on MacOS using homebrew

- App Name: Cloudflare WARP
- App description: Free app that makes your Internet safer
- App Version: 2021.12.1.0,20211210.10
- App Website: https://cloudflarewarp.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Cloudflare WARP with the following command
   ```
   brew install --cask cloudflare-warp
   ```
4. Cloudflare WARP is ready to use now!