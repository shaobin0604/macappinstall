---
title: "Install fplll on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Lattice algorithms using floating-point arithmetic"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fplll on MacOS using homebrew

- App Name: fplll
- App description: Lattice algorithms using floating-point arithmetic
- App Version: 5.4.1
- App Website: https://github.com/fplll/fplll

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fplll with the following command
   ```
   brew install fplll
   ```
4. fplll is ready to use now!