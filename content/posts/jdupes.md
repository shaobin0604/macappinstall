---
title: "Install jdupes on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Duplicate file finder and an enhanced fork of 'fdupes'"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install jdupes on MacOS using homebrew

- App Name: jdupes
- App description: Duplicate file finder and an enhanced fork of 'fdupes'
- App Version: 1.20.2
- App Website: https://github.com/jbruchon/jdupes

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install jdupes with the following command
   ```
   brew install jdupes
   ```
4. jdupes is ready to use now!