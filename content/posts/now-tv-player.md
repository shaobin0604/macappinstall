---
title: "Install NOW TV Player on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Video streaming service player"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install NOW TV Player on MacOS using homebrew

- App Name: NOW TV Player
- App description: Video streaming service player
- App Version: 8.8.0
- App Website: https://www.nowtv.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install NOW TV Player with the following command
   ```
   brew install --cask now-tv-player
   ```
4. NOW TV Player is ready to use now!