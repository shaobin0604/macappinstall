---
title: "Install nfcutils on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Near Field Communication (NFC) tools under POSIX systems"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install nfcutils on MacOS using homebrew

- App Name: nfcutils
- App description: Near Field Communication (NFC) tools under POSIX systems
- App Version: 0.3.2
- App Website: https://github.com/nfc-tools/nfcutils

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install nfcutils with the following command
   ```
   brew install nfcutils
   ```
4. nfcutils is ready to use now!