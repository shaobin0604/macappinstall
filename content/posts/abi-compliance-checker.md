---
title: "Install abi-compliance-checker on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool for checking backward API/ABI compatibility of a C/C++ library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install abi-compliance-checker on MacOS using homebrew

- App Name: abi-compliance-checker
- App description: Tool for checking backward API/ABI compatibility of a C/C++ library
- App Version: 2.3
- App Website: https://lvc.github.io/abi-compliance-checker/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install abi-compliance-checker with the following command
   ```
   brew install abi-compliance-checker
   ```
4. abi-compliance-checker is ready to use now!