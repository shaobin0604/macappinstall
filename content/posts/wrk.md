---
title: "Install wrk on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "HTTP benchmarking tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install wrk on MacOS using homebrew

- App Name: wrk
- App description: HTTP benchmarking tool
- App Version: 4.2.0
- App Website: https://github.com/wg/wrk

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install wrk with the following command
   ```
   brew install wrk
   ```
4. wrk is ready to use now!