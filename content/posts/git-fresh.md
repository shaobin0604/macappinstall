---
title: "Install git-fresh on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utility to keep git repos fresh"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install git-fresh on MacOS using homebrew

- App Name: git-fresh
- App description: Utility to keep git repos fresh
- App Version: 1.13.0
- App Website: https://github.com/imsky/git-fresh

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install git-fresh with the following command
   ```
   brew install git-fresh
   ```
4. git-fresh is ready to use now!