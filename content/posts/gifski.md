---
title: "Install gifski on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Highest-quality GIF encoder based on pngquant"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gifski on MacOS using homebrew

- App Name: gifski
- App description: Highest-quality GIF encoder based on pngquant
- App Version: 1.6.4
- App Website: https://gif.ski/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gifski with the following command
   ```
   brew install gifski
   ```
4. gifski is ready to use now!