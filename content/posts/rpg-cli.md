---
title: "Install rpg-cli on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Your filesystem as a dungeon!"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install rpg-cli on MacOS using homebrew

- App Name: rpg-cli
- App description: Your filesystem as a dungeon!
- App Version: 1.0.1
- App Website: https://github.com/facundoolano/rpg-cli

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install rpg-cli with the following command
   ```
   brew install rpg-cli
   ```
4. rpg-cli is ready to use now!