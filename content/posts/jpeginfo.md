---
title: "Install jpeginfo on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Prints information and tests integrity of JPEG/JFIF files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install jpeginfo on MacOS using homebrew

- App Name: jpeginfo
- App description: Prints information and tests integrity of JPEG/JFIF files
- App Version: 1.6.1
- App Website: https://www.kokkonen.net/tjko/projects.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install jpeginfo with the following command
   ```
   brew install jpeginfo
   ```
4. jpeginfo is ready to use now!