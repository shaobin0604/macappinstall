---
title: "Install Citrix Workspace on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Managed desktop virtualization solution"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Citrix Workspace on MacOS using homebrew

- App Name: Citrix Workspace
- App description: Managed desktop virtualization solution
- App Version: 22.01.0.23,2201
- App Website: https://www.citrix.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Citrix Workspace with the following command
   ```
   brew install --cask citrix-workspace
   ```
4. Citrix Workspace is ready to use now!