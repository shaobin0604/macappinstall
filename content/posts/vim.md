---
title: "Install vim on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Vi 'workalike' with many additional features"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install vim on MacOS using homebrew

- App Name: vim
- App description: Vi 'workalike' with many additional features
- App Version: 8.2.4400
- App Website: https://www.vim.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install vim with the following command
   ```
   brew install vim
   ```
4. vim is ready to use now!