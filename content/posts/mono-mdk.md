---
title: "Install Mono on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Open source implementation of Microsoft's .NET Framework"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Mono on MacOS using homebrew

- App Name: Mono
- App description: Open source implementation of Microsoft's .NET Framework
- App Version: 6.12.0.122
- App Website: https://www.mono-project.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Mono with the following command
   ```
   brew install --cask mono-mdk
   ```
4. Mono is ready to use now!