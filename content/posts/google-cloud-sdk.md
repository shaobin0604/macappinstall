---
title: "Install Google Cloud SDK on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Set of tools to manage resources and applications hosted on Google Cloud"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Google Cloud SDK on MacOS using homebrew

- App Name: Google Cloud SDK
- App description: Set of tools to manage resources and applications hosted on Google Cloud
- App Version: latest
- App Website: https://cloud.google.com/sdk/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Google Cloud SDK with the following command
   ```
   brew install --cask google-cloud-sdk
   ```
4. Google Cloud SDK is ready to use now!