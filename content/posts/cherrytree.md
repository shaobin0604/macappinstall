---
title: "Install cherrytree on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Hierarchical note taking application featuring rich text and syntax highlighting"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cherrytree on MacOS using homebrew

- App Name: cherrytree
- App description: Hierarchical note taking application featuring rich text and syntax highlighting
- App Version: 0.99.45
- App Website: https://www.giuspen.com/cherrytree/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cherrytree with the following command
   ```
   brew install cherrytree
   ```
4. cherrytree is ready to use now!