---
title: "Install markdownlint-cli on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CLI for Node.js style checker and lint tool for Markdown files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install markdownlint-cli on MacOS using homebrew

- App Name: markdownlint-cli
- App description: CLI for Node.js style checker and lint tool for Markdown files
- App Version: 0.31.1
- App Website: https://github.com/igorshubovych/markdownlint-cli

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install markdownlint-cli with the following command
   ```
   brew install markdownlint-cli
   ```
4. markdownlint-cli is ready to use now!