---
title: "Install DigitalOcean Droplets Manager on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Digital Ocean droplet manager"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install DigitalOcean Droplets Manager on MacOS using homebrew

- App Name: DigitalOcean Droplets Manager
- App description: Digital Ocean droplet manager
- App Version: 0.5.0
- App Website: https://github.com/deivuh/DODropletManager-OSX

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install DigitalOcean Droplets Manager with the following command
   ```
   brew install --cask dropletmanager
   ```
4. DigitalOcean Droplets Manager is ready to use now!