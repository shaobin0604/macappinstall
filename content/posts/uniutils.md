---
title: "Install uniutils on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Manipulate and analyze Unicode text"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install uniutils on MacOS using homebrew

- App Name: uniutils
- App description: Manipulate and analyze Unicode text
- App Version: 2.27
- App Website: https://billposer.org/Software/unidesc.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install uniutils with the following command
   ```
   brew install uniutils
   ```
4. uniutils is ready to use now!