---
title: "Install tpl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Store and retrieve binary data in C"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tpl on MacOS using homebrew

- App Name: tpl
- App description: Store and retrieve binary data in C
- App Version: 1.6.1
- App Website: https://troydhanson.github.io/tpl/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tpl with the following command
   ```
   brew install tpl
   ```
4. tpl is ready to use now!