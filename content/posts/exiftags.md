---
title: "Install exiftags on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utility to read EXIF tags from a digital camera JPEG file"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install exiftags on MacOS using homebrew

- App Name: exiftags
- App description: Utility to read EXIF tags from a digital camera JPEG file
- App Version: 1.01
- App Website: https://johnst.org/sw/exiftags/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install exiftags with the following command
   ```
   brew install exiftags
   ```
4. exiftags is ready to use now!