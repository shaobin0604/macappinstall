---
title: "Install Bitcoin Core on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Bitcoin client and wallet"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Bitcoin Core on MacOS using homebrew

- App Name: Bitcoin Core
- App description: Bitcoin client and wallet
- App Version: 22.0
- App Website: https://bitcoincore.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Bitcoin Core with the following command
   ```
   brew install --cask bitcoin-core
   ```
4. Bitcoin Core is ready to use now!