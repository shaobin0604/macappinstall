---
title: "Install mpdecimal on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library for decimal floating point arithmetic"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mpdecimal on MacOS using homebrew

- App Name: mpdecimal
- App description: Library for decimal floating point arithmetic
- App Version: 2.5.1
- App Website: https://www.bytereef.org/mpdecimal/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mpdecimal with the following command
   ```
   brew install mpdecimal
   ```
4. mpdecimal is ready to use now!