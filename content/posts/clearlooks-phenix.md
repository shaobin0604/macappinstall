---
title: "Install clearlooks-phenix on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GTK+3 port of the Clearlooks Theme"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install clearlooks-phenix on MacOS using homebrew

- App Name: clearlooks-phenix
- App description: GTK+3 port of the Clearlooks Theme
- App Version: 7.0.1
- App Website: https://github.com/jpfleury/clearlooks-phenix

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install clearlooks-phenix with the following command
   ```
   brew install clearlooks-phenix
   ```
4. clearlooks-phenix is ready to use now!