---
title: "Install zmqpp on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "High-level C++ binding for zeromq"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install zmqpp on MacOS using homebrew

- App Name: zmqpp
- App description: High-level C++ binding for zeromq
- App Version: 4.2.0
- App Website: https://zeromq.github.io/zmqpp/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install zmqpp with the following command
   ```
   brew install zmqpp
   ```
4. zmqpp is ready to use now!