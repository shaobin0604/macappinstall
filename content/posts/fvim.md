---
title: "Install FVim on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GUI for the Neovim text editor"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install FVim on MacOS using homebrew

- App Name: FVim
- App description: GUI for the Neovim text editor
- App Version: 0.3.489,g98c4036
- App Website: https://github.com/yatli/fvim

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install FVim with the following command
   ```
   brew install --cask fvim
   ```
4. FVim is ready to use now!