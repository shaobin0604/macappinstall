---
title: "Install Beekeeper Studio on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cross platform SQL editor and database management app"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Beekeeper Studio on MacOS using homebrew

- App Name: Beekeeper Studio
- App description: Cross platform SQL editor and database management app
- App Version: 3.1.0
- App Website: https://www.beekeeperstudio.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Beekeeper Studio with the following command
   ```
   brew install --cask beekeeper-studio
   ```
4. Beekeeper Studio is ready to use now!