---
title: "Install pypy on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Highly performant implementation of Python 2 in Python"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pypy on MacOS using homebrew

- App Name: pypy
- App description: Highly performant implementation of Python 2 in Python
- App Version: 7.3.6
- App Website: https://pypy.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pypy with the following command
   ```
   brew install pypy
   ```
4. pypy is ready to use now!