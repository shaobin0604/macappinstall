---
title: "Install rsstail on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Monitors an RSS feed and emits new entries when detected"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install rsstail on MacOS using homebrew

- App Name: rsstail
- App description: Monitors an RSS feed and emits new entries when detected
- App Version: 2.1
- App Website: https://www.vanheusden.com/rsstail/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install rsstail with the following command
   ```
   brew install rsstail
   ```
4. rsstail is ready to use now!