---
title: "Install libpinyin on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library to deal with pinyin"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libpinyin on MacOS using homebrew

- App Name: libpinyin
- App description: Library to deal with pinyin
- App Version: 2.6.1
- App Website: https://github.com/libpinyin/libpinyin

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libpinyin with the following command
   ```
   brew install libpinyin
   ```
4. libpinyin is ready to use now!