---
title: "Install libspatialite on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Adds spatial SQL capabilities to SQLite"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libspatialite on MacOS using homebrew

- App Name: libspatialite
- App description: Adds spatial SQL capabilities to SQLite
- App Version: 5.0.1
- App Website: https://www.gaia-gis.it/fossil/libspatialite/index

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libspatialite with the following command
   ```
   brew install libspatialite
   ```
4. libspatialite is ready to use now!