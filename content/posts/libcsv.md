---
title: "Install libcsv on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CSV library in ANSI C89"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libcsv on MacOS using homebrew

- App Name: libcsv
- App description: CSV library in ANSI C89
- App Version: 3.0.3
- App Website: https://sourceforge.net/projects/libcsv/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libcsv with the following command
   ```
   brew install libcsv
   ```
4. libcsv is ready to use now!