---
title: "Install gnupg on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GNU Pretty Good Privacy (PGP) package"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gnupg on MacOS using homebrew

- App Name: gnupg
- App description: GNU Pretty Good Privacy (PGP) package
- App Version: 2.3.4
- App Website: https://gnupg.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gnupg with the following command
   ```
   brew install gnupg
   ```
4. gnupg is ready to use now!