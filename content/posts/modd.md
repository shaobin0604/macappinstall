---
title: "Install modd on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Flexible tool for responding to filesystem changes"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install modd on MacOS using homebrew

- App Name: modd
- App description: Flexible tool for responding to filesystem changes
- App Version: 0.8
- App Website: https://github.com/cortesi/modd

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install modd with the following command
   ```
   brew install modd
   ```
4. modd is ready to use now!