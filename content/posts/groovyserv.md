---
title: "Install groovyserv on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Speed up Groovy startup time"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install groovyserv on MacOS using homebrew

- App Name: groovyserv
- App description: Speed up Groovy startup time
- App Version: 1.2.0
- App Website: https://kobo.github.io/groovyserv/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install groovyserv with the following command
   ```
   brew install groovyserv
   ```
4. groovyserv is ready to use now!