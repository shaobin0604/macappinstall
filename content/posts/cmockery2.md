---
title: "Install cmockery2 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Reviving cmockery unit test framework from Google"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cmockery2 on MacOS using homebrew

- App Name: cmockery2
- App description: Reviving cmockery unit test framework from Google
- App Version: 1.3.9
- App Website: https://github.com/lpabon/cmockery2

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cmockery2 with the following command
   ```
   brew install cmockery2
   ```
4. cmockery2 is ready to use now!