---
title: "Install OSMC on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Free and open source media center"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install OSMC on MacOS using homebrew

- App Name: OSMC
- App description: Free and open source media center
- App Version: latest
- App Website: https://osmc.tv/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install OSMC with the following command
   ```
   brew install --cask osmc
   ```
4. OSMC is ready to use now!