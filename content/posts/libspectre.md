---
title: "Install libspectre on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Small library for rendering Postscript documents"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libspectre on MacOS using homebrew

- App Name: libspectre
- App description: Small library for rendering Postscript documents
- App Version: 0.2.10
- App Website: https://wiki.freedesktop.org/www/Software/libspectre/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libspectre with the following command
   ```
   brew install libspectre
   ```
4. libspectre is ready to use now!