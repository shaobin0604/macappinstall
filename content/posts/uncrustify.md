---
title: "Install uncrustify on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Source code beautifier"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install uncrustify on MacOS using homebrew

- App Name: uncrustify
- App description: Source code beautifier
- App Version: 0.74.0
- App Website: https://uncrustify.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install uncrustify with the following command
   ```
   brew install uncrustify
   ```
4. uncrustify is ready to use now!