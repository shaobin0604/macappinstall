---
title: "Install alac on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Basic decoder for Apple Lossless Audio Codec files (ALAC)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install alac on MacOS using homebrew

- App Name: alac
- App description: Basic decoder for Apple Lossless Audio Codec files (ALAC)
- App Version: 0.2.0
- App Website: https://web.archive.org/web/20150319040222/craz.net/programs/itunes/alac.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install alac with the following command
   ```
   brew install alac
   ```
4. alac is ready to use now!