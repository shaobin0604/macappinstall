---
title: "Install regula on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Checks infrastructure as code templates using Open Policy Agent/Rego"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install regula on MacOS using homebrew

- App Name: regula
- App description: Checks infrastructure as code templates using Open Policy Agent/Rego
- App Version: 2.5.0
- App Website: https://regula.dev/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install regula with the following command
   ```
   brew install regula
   ```
4. regula is ready to use now!