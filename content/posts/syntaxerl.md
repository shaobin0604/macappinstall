---
title: "Install syntaxerl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Syntax checker for Erlang code and config files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install syntaxerl on MacOS using homebrew

- App Name: syntaxerl
- App description: Syntax checker for Erlang code and config files
- App Version: 0.15.0
- App Website: https://github.com/ten0s/syntaxerl

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install syntaxerl with the following command
   ```
   brew install syntaxerl
   ```
4. syntaxerl is ready to use now!