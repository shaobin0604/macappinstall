---
title: "Install rbenv-bundle-exec on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Integrate rbenv and bundler"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install rbenv-bundle-exec on MacOS using homebrew

- App Name: rbenv-bundle-exec
- App description: Integrate rbenv and bundler
- App Version: 1.0.0
- App Website: https://github.com/maljub01/rbenv-bundle-exec

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install rbenv-bundle-exec with the following command
   ```
   brew install rbenv-bundle-exec
   ```
4. rbenv-bundle-exec is ready to use now!