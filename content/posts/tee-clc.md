---
title: "Install tee-clc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Microsoft Team Explorer Everywhere command-line Client"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tee-clc on MacOS using homebrew

- App Name: tee-clc
- App description: Microsoft Team Explorer Everywhere command-line Client
- App Version: 14.135.0
- App Website: https://github.com/Microsoft/team-explorer-everywhere

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tee-clc with the following command
   ```
   brew install tee-clc
   ```
4. tee-clc is ready to use now!