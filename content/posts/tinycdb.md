---
title: "Install tinycdb on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Create and read constant databases"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tinycdb on MacOS using homebrew

- App Name: tinycdb
- App description: Create and read constant databases
- App Version: 0.78
- App Website: https://www.corpit.ru/mjt/tinycdb.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tinycdb with the following command
   ```
   brew install tinycdb
   ```
4. tinycdb is ready to use now!