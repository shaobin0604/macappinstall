---
title: "Install Spotify on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Music streaming service"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Spotify on MacOS using homebrew

- App Name: Spotify
- App description: Music streaming service
- App Version: 1.1.78.765,5ea20b00,16
- App Website: https://www.spotify.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Spotify with the following command
   ```
   brew install --cask spotify
   ```
4. Spotify is ready to use now!