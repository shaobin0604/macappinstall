---
title: "Install libmaa on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Low-level data structures including hash tables, sets, lists"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libmaa on MacOS using homebrew

- App Name: libmaa
- App description: Low-level data structures including hash tables, sets, lists
- App Version: 1.4.7
- App Website: http://www.dict.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libmaa with the following command
   ```
   brew install libmaa
   ```
4. libmaa is ready to use now!