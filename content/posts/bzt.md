---
title: "Install bzt on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "BlazeMeter Taurus"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bzt on MacOS using homebrew

- App Name: bzt
- App description: BlazeMeter Taurus
- App Version: 1.16.3
- App Website: https://gettaurus.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bzt with the following command
   ```
   brew install bzt
   ```
4. bzt is ready to use now!