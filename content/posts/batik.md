---
title: "Install batik on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Java-based toolkit for SVG images"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install batik on MacOS using homebrew

- App Name: batik
- App description: Java-based toolkit for SVG images
- App Version: 1.14
- App Website: https://xmlgraphics.apache.org/batik/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install batik with the following command
   ```
   brew install batik
   ```
4. batik is ready to use now!