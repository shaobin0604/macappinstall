---
title: "Install devspace on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CLI helps develop/deploy/debug apps with Docker and k8s"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install devspace on MacOS using homebrew

- App Name: devspace
- App description: CLI helps develop/deploy/debug apps with Docker and k8s
- App Version: 5.18.3
- App Website: https://devspace.sh/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install devspace with the following command
   ```
   brew install devspace
   ```
4. devspace is ready to use now!