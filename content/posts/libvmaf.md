---
title: "Install libvmaf on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Perceptual video quality assessment based on multi-method fusion"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libvmaf on MacOS using homebrew

- App Name: libvmaf
- App description: Perceptual video quality assessment based on multi-method fusion
- App Version: 2.3.0
- App Website: https://github.com/Netflix/vmaf

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libvmaf with the following command
   ```
   brew install libvmaf
   ```
4. libvmaf is ready to use now!