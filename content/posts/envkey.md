---
title: "Install EnvKey on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Protects credentials and syncs configurations"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install EnvKey on MacOS using homebrew

- App Name: EnvKey
- App description: Protects credentials and syncs configurations
- App Version: 1.4.20
- App Website: https://www.envkey.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install EnvKey with the following command
   ```
   brew install --cask envkey
   ```
4. EnvKey is ready to use now!