---
title: "Install breezy on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Version control system implemented in Python with multi-format support"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install breezy on MacOS using homebrew

- App Name: breezy
- App description: Version control system implemented in Python with multi-format support
- App Version: 3.2.1
- App Website: https://www.breezy-vcs.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install breezy with the following command
   ```
   brew install breezy
   ```
4. breezy is ready to use now!