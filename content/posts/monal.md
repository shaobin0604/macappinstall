---
title: "Install Monal on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to securely connect to chat servers"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Monal on MacOS using homebrew

- App Name: Monal
- App description: Tool to securely connect to chat servers
- App Version: 5.1.0,767
- App Website: https://monal.im/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Monal with the following command
   ```
   brew install --cask monal
   ```
4. Monal is ready to use now!