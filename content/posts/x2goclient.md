---
title: "Install X2Go Client on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Remote desktop software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install X2Go Client on MacOS using homebrew

- App Name: X2Go Client
- App description: Remote desktop software
- App Version: 4.1.2.2
- App Website: https://wiki.x2go.org/doku.php

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install X2Go Client with the following command
   ```
   brew install --cask x2goclient
   ```
4. X2Go Client is ready to use now!