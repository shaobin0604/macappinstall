---
title: "Install DisplayCAL on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install DisplayCAL on MacOS using homebrew

- App Name: DisplayCAL
- App description: null
- App Version: 3.8.9.3
- App Website: https://displaycal.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install DisplayCAL with the following command
   ```
   brew install --cask displaycal
   ```
4. DisplayCAL is ready to use now!