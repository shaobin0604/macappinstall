---
title: "Install muffet on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Fast website link checker in Go"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install muffet on MacOS using homebrew

- App Name: muffet
- App description: Fast website link checker in Go
- App Version: 2.4.8
- App Website: https://github.com/raviqqe/muffet

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install muffet with the following command
   ```
   brew install muffet
   ```
4. muffet is ready to use now!