---
title: "Install fabric-completion on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Bash completion for Fabric"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fabric-completion on MacOS using homebrew

- App Name: fabric-completion
- App description: Bash completion for Fabric
- App Version: 1
- App Website: https://github.com/st3ldz/fabric-completion

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fabric-completion with the following command
   ```
   brew install fabric-completion
   ```
4. fabric-completion is ready to use now!