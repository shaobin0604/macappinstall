---
title: "Install NEGU Soft Ultimate Control on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Take control of your computer wirelessly"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install NEGU Soft Ultimate Control on MacOS using homebrew

- App Name: NEGU Soft Ultimate Control
- App description: Take control of your computer wirelessly
- App Version: 1.2
- App Website: https://www.negusoft.com/ucontrol/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install NEGU Soft Ultimate Control with the following command
   ```
   brew install --cask ultimate-control
   ```
4. NEGU Soft Ultimate Control is ready to use now!