---
title: "Install libhttpserver on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C++ library of embedded Rest HTTP server"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libhttpserver on MacOS using homebrew

- App Name: libhttpserver
- App description: C++ library of embedded Rest HTTP server
- App Version: 0.18.2
- App Website: https://github.com/etr/libhttpserver

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libhttpserver with the following command
   ```
   brew install libhttpserver
   ```
4. libhttpserver is ready to use now!