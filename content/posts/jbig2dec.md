---
title: "Install jbig2dec on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "JBIG2 decoder and library (for monochrome documents)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install jbig2dec on MacOS using homebrew

- App Name: jbig2dec
- App description: JBIG2 decoder and library (for monochrome documents)
- App Version: 0.19
- App Website: https://jbig2dec.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install jbig2dec with the following command
   ```
   brew install jbig2dec
   ```
4. jbig2dec is ready to use now!