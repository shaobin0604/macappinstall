---
title: "Install git-annex on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Manage files with git without checking in file contents"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install git-annex on MacOS using homebrew

- App Name: git-annex
- App description: Manage files with git without checking in file contents
- App Version: 10.20220127
- App Website: https://git-annex.branchable.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install git-annex with the following command
   ```
   brew install git-annex
   ```
4. git-annex is ready to use now!