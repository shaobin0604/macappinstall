---
title: "Install jenkins on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Extendable open source continuous integration server"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install jenkins on MacOS using homebrew

- App Name: jenkins
- App description: Extendable open source continuous integration server
- App Version: 2.335
- App Website: https://www.jenkins.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install jenkins with the following command
   ```
   brew install jenkins
   ```
4. jenkins is ready to use now!