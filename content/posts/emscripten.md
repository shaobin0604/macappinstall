---
title: "Install emscripten on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "LLVM bytecode to JavaScript compiler"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install emscripten on MacOS using homebrew

- App Name: emscripten
- App description: LLVM bytecode to JavaScript compiler
- App Version: 3.1.4
- App Website: https://emscripten.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install emscripten with the following command
   ```
   brew install emscripten
   ```
4. emscripten is ready to use now!