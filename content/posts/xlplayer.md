---
title: "Install XLPlayer for Mac on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Video player"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install XLPlayer for Mac on MacOS using homebrew

- App Name: XLPlayer for Mac
- App description: Video player
- App Version: 3.0.1.12449
- App Website: https://video.xunlei.com/mac.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install XLPlayer for Mac with the following command
   ```
   brew install --cask xlplayer
   ```
4. XLPlayer for Mac is ready to use now!