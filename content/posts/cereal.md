---
title: "Install cereal on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C++11 library for serialization"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cereal on MacOS using homebrew

- App Name: cereal
- App description: C++11 library for serialization
- App Version: 1.3.1
- App Website: https://uscilab.github.io/cereal/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cereal with the following command
   ```
   brew install cereal
   ```
4. cereal is ready to use now!