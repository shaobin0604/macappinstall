---
title: "Install lcrack on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Generic password cracker"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install lcrack on MacOS using homebrew

- App Name: lcrack
- App description: Generic password cracker
- App Version: 20040914
- App Website: https://packages.debian.org/sid/lcrack

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install lcrack with the following command
   ```
   brew install lcrack
   ```
4. lcrack is ready to use now!