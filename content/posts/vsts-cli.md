---
title: "Install vsts-cli on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Manage and work with VSTS/TFS resources from the command-line"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install vsts-cli on MacOS using homebrew

- App Name: vsts-cli
- App description: Manage and work with VSTS/TFS resources from the command-line
- App Version: 0.1.4
- App Website: https://docs.microsoft.com/en-us/cli/vsts

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install vsts-cli with the following command
   ```
   brew install vsts-cli
   ```
4. vsts-cli is ready to use now!