---
title: "Install CLion on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C and C++ IDE"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install CLion on MacOS using homebrew

- App Name: CLion
- App description: C and C++ IDE
- App Version: 2021.3.3,213.6777.58
- App Website: https://www.jetbrains.com/clion/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install CLion with the following command
   ```
   brew install --cask clion
   ```
4. CLion is ready to use now!