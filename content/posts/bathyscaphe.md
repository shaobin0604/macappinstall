---
title: "Install BathyScaphe on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "2-channel browser"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install BathyScaphe on MacOS using homebrew

- App Name: BathyScaphe
- App description: 2-channel browser
- App Version: 3.1.0,1089
- App Website: https://bathyscaphe.bitbucket.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install BathyScaphe with the following command
   ```
   brew install --cask bathyscaphe
   ```
4. BathyScaphe is ready to use now!