---
title: "Install RDM on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utility to set a Retina display to custom resolutions"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install RDM on MacOS using homebrew

- App Name: RDM
- App description: Utility to set a Retina display to custom resolutions
- App Version: 2.2
- App Website: https://github.com/avibrazil/RDM

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install RDM with the following command
   ```
   brew install --cask avibrazil-rdm
   ```
4. RDM is ready to use now!