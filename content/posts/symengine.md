---
title: "Install symengine on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Fast symbolic manipulation library written in C++"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install symengine on MacOS using homebrew

- App Name: symengine
- App description: Fast symbolic manipulation library written in C++
- App Version: 0.8.1
- App Website: https://sympy.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install symengine with the following command
   ```
   brew install symengine
   ```
4. symengine is ready to use now!