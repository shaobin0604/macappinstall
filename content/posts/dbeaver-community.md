---
title: "Install DBeaver Community Edition on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Universal database tool and SQL client"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install DBeaver Community Edition on MacOS using homebrew

- App Name: DBeaver Community Edition
- App description: Universal database tool and SQL client
- App Version: 21.3.4
- App Website: https://dbeaver.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install DBeaver Community Edition with the following command
   ```
   brew install --cask dbeaver-community
   ```
4. DBeaver Community Edition is ready to use now!