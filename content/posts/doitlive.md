---
title: "Install doitlive on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Replay stored shell commands for live presentations"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install doitlive on MacOS using homebrew

- App Name: doitlive
- App description: Replay stored shell commands for live presentations
- App Version: 4.3.0
- App Website: https://doitlive.readthedocs.io/en/latest/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install doitlive with the following command
   ```
   brew install doitlive
   ```
4. doitlive is ready to use now!