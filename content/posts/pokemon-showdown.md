---
title: "Install Pokémon Showdown on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Pokémon Showdown on MacOS using homebrew

- App Name: Pokémon Showdown
- App description: null
- App Version: 0.11,bf46c879d0
- App Website: https://pokemonshowdown.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Pokémon Showdown with the following command
   ```
   brew install --cask pokemon-showdown
   ```
4. Pokémon Showdown is ready to use now!