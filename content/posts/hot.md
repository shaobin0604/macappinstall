---
title: "Install Hot on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Menu bar application that displays the CPU speed limit due to thermal issues"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Hot on MacOS using homebrew

- App Name: Hot
- App description: Menu bar application that displays the CPU speed limit due to thermal issues
- App Version: 1.6.1
- App Website: https://github.com/macmade/Hot

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Hot with the following command
   ```
   brew install --cask hot
   ```
4. Hot is ready to use now!