---
title: "Install nuget on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Package manager for Microsoft development platform including .NET"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install nuget on MacOS using homebrew

- App Name: nuget
- App description: Package manager for Microsoft development platform including .NET
- App Version: 6.0.0
- App Website: https://www.nuget.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install nuget with the following command
   ```
   brew install nuget
   ```
4. nuget is ready to use now!