---
title: "Install RaiderIO Client on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "World of Warcraft rankings site client"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install RaiderIO Client on MacOS using homebrew

- App Name: RaiderIO Client
- App description: World of Warcraft rankings site client
- App Version: 2.1.8
- App Website: https://raider.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install RaiderIO Client with the following command
   ```
   brew install --cask raiderio
   ```
4. RaiderIO Client is ready to use now!