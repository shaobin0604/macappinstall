---
title: "Install cppcheck on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Static analysis of C and C++ code"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cppcheck on MacOS using homebrew

- App Name: cppcheck
- App description: Static analysis of C and C++ code
- App Version: 2.7
- App Website: https://sourceforge.net/projects/cppcheck/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cppcheck with the following command
   ```
   brew install cppcheck
   ```
4. cppcheck is ready to use now!