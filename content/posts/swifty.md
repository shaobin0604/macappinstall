---
title: "Install Swifty on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Offline password manager tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Swifty on MacOS using homebrew

- App Name: Swifty
- App description: Offline password manager tool
- App Version: 0.6.6
- App Website: https://getswifty.pro/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Swifty with the following command
   ```
   brew install --cask swifty
   ```
4. Swifty is ready to use now!