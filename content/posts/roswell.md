---
title: "Install roswell on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Lisp installer and launcher for major environments"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install roswell on MacOS using homebrew

- App Name: roswell
- App description: Lisp installer and launcher for major environments
- App Version: 21.10.14.111
- App Website: https://github.com/roswell/roswell

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install roswell with the following command
   ```
   brew install roswell
   ```
4. roswell is ready to use now!