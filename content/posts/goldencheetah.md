---
title: "Install GoldenCheetah on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Performance software for cyclists, runners and triathletes"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install GoldenCheetah on MacOS using homebrew

- App Name: GoldenCheetah
- App description: Performance software for cyclists, runners and triathletes
- App Version: 3.5
- App Website: https://www.goldencheetah.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install GoldenCheetah with the following command
   ```
   brew install --cask goldencheetah
   ```
4. GoldenCheetah is ready to use now!