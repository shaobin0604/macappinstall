---
title: "Install libvisio on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Interpret and import Visio diagrams"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libvisio on MacOS using homebrew

- App Name: libvisio
- App description: Interpret and import Visio diagrams
- App Version: 0.1.7
- App Website: https://wiki.documentfoundation.org/DLP/Libraries/libvisio

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libvisio with the following command
   ```
   brew install libvisio
   ```
4. libvisio is ready to use now!