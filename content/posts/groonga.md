---
title: "Install groonga on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Fulltext search engine and column store"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install groonga on MacOS using homebrew

- App Name: groonga
- App description: Fulltext search engine and column store
- App Version: 12.0.0
- App Website: https://groonga.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install groonga with the following command
   ```
   brew install groonga
   ```
4. groonga is ready to use now!