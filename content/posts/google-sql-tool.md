---
title: "Install google-sql-tool on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line tool for executing common SQL statements"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install google-sql-tool on MacOS using homebrew

- App Name: google-sql-tool
- App description: Command-line tool for executing common SQL statements
- App Version: r10
- App Website: https://cloud.google.com/sql/docs/mysql-client

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install google-sql-tool with the following command
   ```
   brew install google-sql-tool
   ```
4. google-sql-tool is ready to use now!