---
title: "Install bgpstream on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "For live and historical BGP data analysis"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bgpstream on MacOS using homebrew

- App Name: bgpstream
- App description: For live and historical BGP data analysis
- App Version: 2.2.0
- App Website: https://bgpstream.caida.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bgpstream with the following command
   ```
   brew install bgpstream
   ```
4. bgpstream is ready to use now!