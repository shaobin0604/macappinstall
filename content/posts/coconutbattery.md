---
title: "Install coconutBattery on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to show live information about the batteries in various devices"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install coconutBattery on MacOS using homebrew

- App Name: coconutBattery
- App description: Tool to show live information about the batteries in various devices
- App Version: 3.9.7,1b4b96e3
- App Website: https://www.coconut-flavour.com/coconutbattery/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install coconutBattery with the following command
   ```
   brew install --cask coconutbattery
   ```
4. coconutBattery is ready to use now!