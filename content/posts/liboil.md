---
title: "Install liboil on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C library of simple functions optimized for various CPUs"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install liboil on MacOS using homebrew

- App Name: liboil
- App description: C library of simple functions optimized for various CPUs
- App Version: 0.3.17
- App Website: https://wiki.freedesktop.org/liboil/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install liboil with the following command
   ```
   brew install liboil
   ```
4. liboil is ready to use now!