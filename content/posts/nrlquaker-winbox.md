---
title: "Install Winbox-mac on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "MikroTik Winbox"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Winbox-mac on MacOS using homebrew

- App Name: Winbox-mac
- App description: MikroTik Winbox
- App Version: 3.32.0
- App Website: https://github.com/nrlquaker/winbox-mac/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Winbox-mac with the following command
   ```
   brew install --cask nrlquaker-winbox
   ```
4. Winbox-mac is ready to use now!