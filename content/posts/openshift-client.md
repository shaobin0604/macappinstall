---
title: "Install Openshift Client on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Red Hat OpenShift Container Platform command-line client"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Openshift Client on MacOS using homebrew

- App Name: Openshift Client
- App description: Red Hat OpenShift Container Platform command-line client
- App Version: 4.9.21
- App Website: https://www.openshift.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Openshift Client with the following command
   ```
   brew install --cask openshift-client
   ```
4. Openshift Client is ready to use now!