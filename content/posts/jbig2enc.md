---
title: "Install jbig2enc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "JBIG2 encoder (for monochrome documents)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install jbig2enc on MacOS using homebrew

- App Name: jbig2enc
- App description: JBIG2 encoder (for monochrome documents)
- App Version: 0.29
- App Website: https://github.com/agl/jbig2enc

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install jbig2enc with the following command
   ```
   brew install jbig2enc
   ```
4. jbig2enc is ready to use now!