---
title: "Install gtksourceview4 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Text view with syntax, undo/redo, and text marks"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gtksourceview4 on MacOS using homebrew

- App Name: gtksourceview4
- App description: Text view with syntax, undo/redo, and text marks
- App Version: 4.8.2
- App Website: https://projects.gnome.org/gtksourceview/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gtksourceview4 with the following command
   ```
   brew install gtksourceview4
   ```
4. gtksourceview4 is ready to use now!