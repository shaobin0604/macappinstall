---
title: "Install rfcmarkup on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Add HTML markup and links to internet-drafts and RFCs"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install rfcmarkup on MacOS using homebrew

- App Name: rfcmarkup
- App description: Add HTML markup and links to internet-drafts and RFCs
- App Version: 1.129
- App Website: https://tools.ietf.org/tools/rfcmarkup/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install rfcmarkup with the following command
   ```
   brew install rfcmarkup
   ```
4. rfcmarkup is ready to use now!