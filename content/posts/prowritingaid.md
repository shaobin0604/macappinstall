---
title: "Install ProWritingAid on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Grammar checker, style editor, and writing mentor"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ProWritingAid on MacOS using homebrew

- App Name: ProWritingAid
- App description: Grammar checker, style editor, and writing mentor
- App Version: 2.0.56
- App Website: https://prowritingaid.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ProWritingAid with the following command
   ```
   brew install --cask prowritingaid
   ```
4. ProWritingAid is ready to use now!