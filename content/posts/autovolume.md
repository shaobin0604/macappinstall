---
title: "Install AutoVolume on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool that automatically sets the volume to a specified volume"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install AutoVolume on MacOS using homebrew

- App Name: AutoVolume
- App description: Tool that automatically sets the volume to a specified volume
- App Version: 1.0.1
- App Website: https://github.com/jesse-c/AutoVolume

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install AutoVolume with the following command
   ```
   brew install --cask autovolume
   ```
4. AutoVolume is ready to use now!