---
title: "Install nmh on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "New version of the MH mail handler"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install nmh on MacOS using homebrew

- App Name: nmh
- App description: New version of the MH mail handler
- App Version: 1.7.1
- App Website: https://www.nongnu.org/nmh/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install nmh with the following command
   ```
   brew install nmh
   ```
4. nmh is ready to use now!