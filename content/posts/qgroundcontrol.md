---
title: "Install QGroundControl on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Ground control station for drones"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install QGroundControl on MacOS using homebrew

- App Name: QGroundControl
- App description: Ground control station for drones
- App Version: 4.1.4
- App Website: http://qgroundcontrol.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install QGroundControl with the following command
   ```
   brew install --cask qgroundcontrol
   ```
4. QGroundControl is ready to use now!