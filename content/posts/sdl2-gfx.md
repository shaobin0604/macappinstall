---
title: "Install sdl2_gfx on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "SDL2 graphics drawing primitives and other support functions"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sdl2_gfx on MacOS using homebrew

- App Name: sdl2_gfx
- App description: SDL2 graphics drawing primitives and other support functions
- App Version: 1.0.4
- App Website: https://www.ferzkopp.net/wordpress/2016/01/02/sdl_gfx-sdl2_gfx/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sdl2_gfx with the following command
   ```
   brew install sdl2_gfx
   ```
4. sdl2_gfx is ready to use now!