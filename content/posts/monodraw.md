---
title: "Install Monodraw on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to create text-based art"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Monodraw on MacOS using homebrew

- App Name: Monodraw
- App description: Tool to create text-based art
- App Version: 1.5,107
- App Website: https://monodraw.helftone.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Monodraw with the following command
   ```
   brew install --cask monodraw
   ```
4. Monodraw is ready to use now!