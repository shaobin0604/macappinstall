---
title: "Install vncsnapshot on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line utility for taking VNC snapshots"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install vncsnapshot on MacOS using homebrew

- App Name: vncsnapshot
- App description: Command-line utility for taking VNC snapshots
- App Version: 1.2a
- App Website: https://sourceforge.net/projects/vncsnapshot/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install vncsnapshot with the following command
   ```
   brew install vncsnapshot
   ```
4. vncsnapshot is ready to use now!