---
title: "Install libfreenect on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Drivers and libraries for the Xbox Kinect device"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libfreenect on MacOS using homebrew

- App Name: libfreenect
- App description: Drivers and libraries for the Xbox Kinect device
- App Version: 0.6.2
- App Website: https://openkinect.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libfreenect with the following command
   ```
   brew install libfreenect
   ```
4. libfreenect is ready to use now!