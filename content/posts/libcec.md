---
title: "Install libcec on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Control devices with TV remote control and HDMI cabling"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libcec on MacOS using homebrew

- App Name: libcec
- App description: Control devices with TV remote control and HDMI cabling
- App Version: 6.0.2
- App Website: http://libcec.pulse-eight.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libcec with the following command
   ```
   brew install libcec
   ```
4. libcec is ready to use now!