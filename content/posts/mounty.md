---
title: "Install Mounty for NTFS on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Re-mounts write-protected NTFS volumes"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Mounty for NTFS on MacOS using homebrew

- App Name: Mounty for NTFS
- App description: Re-mounts write-protected NTFS volumes
- App Version: 1.13
- App Website: https://mounty.app/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Mounty for NTFS with the following command
   ```
   brew install --cask mounty
   ```
4. Mounty for NTFS is ready to use now!