---
title: "Install pkger on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Embed static files in Go binaries (replacement for gobuffalo/packr)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pkger on MacOS using homebrew

- App Name: pkger
- App description: Embed static files in Go binaries (replacement for gobuffalo/packr)
- App Version: 0.17.1
- App Website: https://github.com/markbates/pkger

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pkger with the following command
   ```
   brew install pkger
   ```
4. pkger is ready to use now!