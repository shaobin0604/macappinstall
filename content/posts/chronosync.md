---
title: "Install ChronoSync on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Synchronization and backup tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ChronoSync on MacOS using homebrew

- App Name: ChronoSync
- App description: Synchronization and backup tool
- App Version: 4.9.13
- App Website: https://www.econtechnologies.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ChronoSync with the following command
   ```
   brew install --cask chronosync
   ```
4. ChronoSync is ready to use now!