---
title: "Install bonnie++ on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Benchmark suite for file systems and hard drives"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bonnie++ on MacOS using homebrew

- App Name: bonnie++
- App description: Benchmark suite for file systems and hard drives
- App Version: 2.00a
- App Website: https://www.coker.com.au/bonnie++/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bonnie++ with the following command
   ```
   brew install bonnie++
   ```
4. bonnie++ is ready to use now!