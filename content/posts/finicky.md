---
title: "Install Finicky on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utility for customizing which browser to start"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Finicky on MacOS using homebrew

- App Name: Finicky
- App description: Utility for customizing which browser to start
- App Version: 3.4.0
- App Website: https://github.com/johnste/finicky

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Finicky with the following command
   ```
   brew install --cask finicky
   ```
4. Finicky is ready to use now!