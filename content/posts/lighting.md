---
title: "Install Lighting on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to control LIFX lights via a Notification Center widget"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Lighting on MacOS using homebrew

- App Name: Lighting
- App description: Tool to control LIFX lights via a Notification Center widget
- App Version: 1.1.7
- App Website: https://github.com/tatey/Lighting

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Lighting with the following command
   ```
   brew install --cask lighting
   ```
4. Lighting is ready to use now!