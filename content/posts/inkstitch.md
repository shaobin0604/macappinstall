---
title: "Install Inkstitch on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Inkscape extension for machine embroidery design"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Inkstitch on MacOS using homebrew

- App Name: Inkstitch
- App description: Inkscape extension for machine embroidery design
- App Version: 2.1.2
- App Website: https://inkstitch.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Inkstitch with the following command
   ```
   brew install --cask inkstitch
   ```
4. Inkstitch is ready to use now!