---
title: "Install SpechtLite on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Rule-based proxy"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install SpechtLite on MacOS using homebrew

- App Name: SpechtLite
- App description: Rule-based proxy
- App Version: 0.10.7
- App Website: https://github.com/zhuhaow/SpechtLite

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install SpechtLite with the following command
   ```
   brew install --cask spechtlite
   ```
4. SpechtLite is ready to use now!