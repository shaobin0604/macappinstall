---
title: "Install webdis on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Redis HTTP interface with JSON output"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install webdis on MacOS using homebrew

- App Name: webdis
- App description: Redis HTTP interface with JSON output
- App Version: 0.1.19
- App Website: https://webd.is/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install webdis with the following command
   ```
   brew install webdis
   ```
4. webdis is ready to use now!