---
title: "Install gexiv2 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GObject wrapper around the Exiv2 photo metadata library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gexiv2 on MacOS using homebrew

- App Name: gexiv2
- App description: GObject wrapper around the Exiv2 photo metadata library
- App Version: 0.14.0
- App Website: https://wiki.gnome.org/Projects/gexiv2

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gexiv2 with the following command
   ```
   brew install gexiv2
   ```
4. gexiv2 is ready to use now!