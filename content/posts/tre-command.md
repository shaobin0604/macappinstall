---
title: "Install tre-command on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tree command, improved"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tre-command on MacOS using homebrew

- App Name: tre-command
- App description: Tree command, improved
- App Version: 0.3.6
- App Website: https://github.com/dduan/tre

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tre-command with the following command
   ```
   brew install tre-command
   ```
4. tre-command is ready to use now!