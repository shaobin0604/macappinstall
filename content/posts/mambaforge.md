---
title: "Install mambaforge on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Minimal installer for conda with preinstalled support for Mamba"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mambaforge on MacOS using homebrew

- App Name: mambaforge
- App description: Minimal installer for conda with preinstalled support for Mamba
- App Version: 4.11.0-0
- App Website: https://github.com/conda-forge/miniforge

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mambaforge with the following command
   ```
   brew install --cask mambaforge
   ```
4. mambaforge is ready to use now!