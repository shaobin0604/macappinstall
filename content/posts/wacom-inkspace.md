---
title: "Install Wacom Inkspace on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Wacom Inkspace on MacOS using homebrew

- App Name: Wacom Inkspace
- App description: null
- App Version: 2.7.4
- App Website: https://www.wacom.com/en-us/products/apps-services/inkspace

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Wacom Inkspace with the following command
   ```
   brew install --cask wacom-inkspace
   ```
4. Wacom Inkspace is ready to use now!