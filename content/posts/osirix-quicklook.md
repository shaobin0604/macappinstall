---
title: "Install OsiriX DICOM QuickLook on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "QuickLook plugin for OsiriX DICOM files"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install OsiriX DICOM QuickLook on MacOS using homebrew

- App Name: OsiriX DICOM QuickLook
- App description: QuickLook plugin for OsiriX DICOM files
- App Version: 6.0
- App Website: https://www.osirix-viewer.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install OsiriX DICOM QuickLook with the following command
   ```
   brew install --cask osirix-quicklook
   ```
4. OsiriX DICOM QuickLook is ready to use now!