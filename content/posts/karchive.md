---
title: "Install karchive on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Reading, creating, and manipulating file archives"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install karchive on MacOS using homebrew

- App Name: karchive
- App description: Reading, creating, and manipulating file archives
- App Version: 5.91.0
- App Website: https://api.kde.org/frameworks/karchive/html/index.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install karchive with the following command
   ```
   brew install karchive
   ```
4. karchive is ready to use now!