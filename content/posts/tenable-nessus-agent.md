---
title: "Install Tenable Nessus Agent on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Agent for Nessus vulnerability scanner"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Tenable Nessus Agent on MacOS using homebrew

- App Name: Tenable Nessus Agent
- App description: Agent for Nessus vulnerability scanner
- App Version: 10.0.1,15494
- App Website: https://www.tenable.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Tenable Nessus Agent with the following command
   ```
   brew install --cask tenable-nessus-agent
   ```
4. Tenable Nessus Agent is ready to use now!