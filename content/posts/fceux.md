---
title: "Install fceux on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "All-in-one NES/Famicom Emulator"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fceux on MacOS using homebrew

- App Name: fceux
- App description: All-in-one NES/Famicom Emulator
- App Version: 2.6.2
- App Website: https://fceux.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fceux with the following command
   ```
   brew install fceux
   ```
4. fceux is ready to use now!