---
title: "Install RingCentral Phone on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Phone system manager"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install RingCentral Phone on MacOS using homebrew

- App Name: RingCentral Phone
- App description: Phone system manager
- App Version: 21.4.0
- App Website: https://www.ringcentral.com/apps/rc-phone

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install RingCentral Phone with the following command
   ```
   brew install --cask ringcentral-phone
   ```
4. RingCentral Phone is ready to use now!