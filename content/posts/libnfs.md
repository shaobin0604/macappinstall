---
title: "Install libnfs on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C client library for NFS"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libnfs on MacOS using homebrew

- App Name: libnfs
- App description: C client library for NFS
- App Version: 5.0.1
- App Website: https://github.com/sahlberg/libnfs

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libnfs with the following command
   ```
   brew install libnfs
   ```
4. libnfs is ready to use now!