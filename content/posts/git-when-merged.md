---
title: "Install git-when-merged on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Find where a commit was merged in git"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install git-when-merged on MacOS using homebrew

- App Name: git-when-merged
- App description: Find where a commit was merged in git
- App Version: 1.2.0
- App Website: https://github.com/mhagger/git-when-merged

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install git-when-merged with the following command
   ```
   brew install git-when-merged
   ```
4. git-when-merged is ready to use now!