---
title: "Install xvid on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "High-performance, high-quality MPEG-4 video library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install xvid on MacOS using homebrew

- App Name: xvid
- App description: High-performance, high-quality MPEG-4 video library
- App Version: 1.3.7
- App Website: https://labs.xvid.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install xvid with the following command
   ```
   brew install xvid
   ```
4. xvid is ready to use now!