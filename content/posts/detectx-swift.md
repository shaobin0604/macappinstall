---
title: "Install DetectX Swift on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Searching and troubleshooting tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install DetectX Swift on MacOS using homebrew

- App Name: DetectX Swift
- App description: Searching and troubleshooting tool
- App Version: 1.0982
- App Website: https://sqwarq.com/detectx/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install DetectX Swift with the following command
   ```
   brew install --cask detectx-swift
   ```
4. DetectX Swift is ready to use now!