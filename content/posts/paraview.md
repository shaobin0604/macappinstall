---
title: "Install ParaView on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Data analysis and visualization application"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ParaView on MacOS using homebrew

- App Name: ParaView
- App description: Data analysis and visualization application
- App Version: 5.10.0
- App Website: https://www.paraview.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ParaView with the following command
   ```
   brew install --cask paraview
   ```
4. ParaView is ready to use now!