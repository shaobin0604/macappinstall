---
title: "Install git-multipush on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Push a branch to multiple remotes in one command"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install git-multipush on MacOS using homebrew

- App Name: git-multipush
- App description: Push a branch to multiple remotes in one command
- App Version: 2.3
- App Website: https://github.com/gavinbeatty/git-multipush

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install git-multipush with the following command
   ```
   brew install git-multipush
   ```
4. git-multipush is ready to use now!