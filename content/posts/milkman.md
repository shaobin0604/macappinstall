---
title: "Install Milkman on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Extensible request and response workbench"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Milkman on MacOS using homebrew

- App Name: Milkman
- App description: Extensible request and response workbench
- App Version: 5.3.0
- App Website: https://github.com/warmuuh/milkman

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Milkman with the following command
   ```
   brew install --cask milkman
   ```
4. Milkman is ready to use now!