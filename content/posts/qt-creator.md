---
title: "Install Qt Creator on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "IDE for application development"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Qt Creator on MacOS using homebrew

- App Name: Qt Creator
- App description: IDE for application development
- App Version: 6.0.2
- App Website: https://www.qt.io/developers/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Qt Creator with the following command
   ```
   brew install --cask qt-creator
   ```
4. Qt Creator is ready to use now!