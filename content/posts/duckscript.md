---
title: "Install duckscript on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Simple, extendable and embeddable scripting language"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install duckscript on MacOS using homebrew

- App Name: duckscript
- App description: Simple, extendable and embeddable scripting language
- App Version: 0.8.10
- App Website: https://sagiegurari.github.io/duckscript

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install duckscript with the following command
   ```
   brew install duckscript
   ```
4. duckscript is ready to use now!