---
title: "Install bdw-gc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Garbage collector for C and C++"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bdw-gc on MacOS using homebrew

- App Name: bdw-gc
- App description: Garbage collector for C and C++
- App Version: 8.0.6
- App Website: https://www.hboehm.info/gc/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bdw-gc with the following command
   ```
   brew install bdw-gc
   ```
4. bdw-gc is ready to use now!