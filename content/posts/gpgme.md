---
title: "Install gpgme on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library access to GnuPG"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gpgme on MacOS using homebrew

- App Name: gpgme
- App description: Library access to GnuPG
- App Version: 1.17.0
- App Website: https://www.gnupg.org/related_software/gpgme/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gpgme with the following command
   ```
   brew install gpgme
   ```
4. gpgme is ready to use now!