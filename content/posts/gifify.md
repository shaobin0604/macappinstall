---
title: "Install gifify on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Turn movies into GIFs"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gifify on MacOS using homebrew

- App Name: gifify
- App description: Turn movies into GIFs
- App Version: 4.0
- App Website: https://github.com/jclem/gifify

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gifify with the following command
   ```
   brew install gifify
   ```
4. gifify is ready to use now!