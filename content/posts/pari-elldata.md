---
title: "Install pari-elldata on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "J.E. Cremona elliptic curve data for PARI/GP"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pari-elldata on MacOS using homebrew

- App Name: pari-elldata
- App description: J.E. Cremona elliptic curve data for PARI/GP
- App Version: 20210301
- App Website: https://pari.math.u-bordeaux.fr/packages.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pari-elldata with the following command
   ```
   brew install pari-elldata
   ```
4. pari-elldata is ready to use now!