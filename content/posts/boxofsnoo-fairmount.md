---
title: "Install Fairmount on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "DVD mounting software, originally from Metakine"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Fairmount on MacOS using homebrew

- App Name: Fairmount
- App description: DVD mounting software, originally from Metakine
- App Version: 1.1.3
- App Website: https://github.com/BoxOfSnoo/Fairmount

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Fairmount with the following command
   ```
   brew install --cask boxofsnoo-fairmount
   ```
4. Fairmount is ready to use now!