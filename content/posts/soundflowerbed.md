---
title: "Install SoundflowerBed on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Taps into Soundflower channels and route them to an audio device"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install SoundflowerBed on MacOS using homebrew

- App Name: SoundflowerBed
- App description: Taps into Soundflower channels and route them to an audio device
- App Version: 2.0.0
- App Website: https://github.com/mLupine/SoundflowerBed

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install SoundflowerBed with the following command
   ```
   brew install --cask soundflowerbed
   ```
4. SoundflowerBed is ready to use now!