---
title: "Install plotutils on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C/C++ function library for exporting 2-D vector graphics"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install plotutils on MacOS using homebrew

- App Name: plotutils
- App description: C/C++ function library for exporting 2-D vector graphics
- App Version: 2.6
- App Website: https://www.gnu.org/software/plotutils/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install plotutils with the following command
   ```
   brew install plotutils
   ```
4. plotutils is ready to use now!