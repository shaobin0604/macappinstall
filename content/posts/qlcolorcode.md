---
title: "Install QLColorCode on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "QuickLook plug-in that renders source code with syntax highlighting"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install QLColorCode on MacOS using homebrew

- App Name: QLColorCode
- App description: QuickLook plug-in that renders source code with syntax highlighting
- App Version: 4.1.0
- App Website: https://github.com/anthonygelibert/QLColorCode

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install QLColorCode with the following command
   ```
   brew install --cask qlcolorcode
   ```
4. QLColorCode is ready to use now!