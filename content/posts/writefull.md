---
title: "Install Writefull on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Provides feedback on your writing"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Writefull on MacOS using homebrew

- App Name: Writefull
- App description: Provides feedback on your writing
- App Version: 3.0.0-beta19
- App Website: https://writefullapp.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Writefull with the following command
   ```
   brew install --cask writefull
   ```
4. Writefull is ready to use now!