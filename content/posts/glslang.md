---
title: "Install glslang on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "OpenGL and OpenGL ES reference compiler for shading languages"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install glslang on MacOS using homebrew

- App Name: glslang
- App description: OpenGL and OpenGL ES reference compiler for shading languages
- App Version: 11.8.0
- App Website: https://www.khronos.org/opengles/sdk/tools/Reference-Compiler/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install glslang with the following command
   ```
   brew install glslang
   ```
4. glslang is ready to use now!