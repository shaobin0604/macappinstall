---
title: "Install julia on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Fast, Dynamic Programming Language"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install julia on MacOS using homebrew

- App Name: julia
- App description: Fast, Dynamic Programming Language
- App Version: 1.7.1
- App Website: https://julialang.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install julia with the following command
   ```
   brew install julia
   ```
4. julia is ready to use now!