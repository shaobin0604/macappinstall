---
title: "Install BookMacster on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Bookmarks manager"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install BookMacster on MacOS using homebrew

- App Name: BookMacster
- App description: Bookmarks manager
- App Version: 2.12
- App Website: https://sheepsystems.com/products/bookmacster.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install BookMacster with the following command
   ```
   brew install --cask bookmacster
   ```
4. BookMacster is ready to use now!