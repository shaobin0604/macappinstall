---
title: "Install moon-buggy on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Drive some car across the moon"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install moon-buggy on MacOS using homebrew

- App Name: moon-buggy
- App description: Drive some car across the moon
- App Version: 1.0
- App Website: https://www.seehuhn.de/pages/moon-buggy.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install moon-buggy with the following command
   ```
   brew install moon-buggy
   ```
4. moon-buggy is ready to use now!