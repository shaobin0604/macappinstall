---
title: "Install argtable on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "ANSI C library for parsing GNU-style command-line options"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install argtable on MacOS using homebrew

- App Name: argtable
- App description: ANSI C library for parsing GNU-style command-line options
- App Version: 2.13
- App Website: https://argtable.sourceforge.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install argtable with the following command
   ```
   brew install argtable
   ```
4. argtable is ready to use now!