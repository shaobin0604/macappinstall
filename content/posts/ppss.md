---
title: "Install ppss on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Shell script to execute commands in parallel"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ppss on MacOS using homebrew

- App Name: ppss
- App description: Shell script to execute commands in parallel
- App Version: 2.97
- App Website: https://github.com/louwrentius/PPSS

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ppss with the following command
   ```
   brew install ppss
   ```
4. ppss is ready to use now!