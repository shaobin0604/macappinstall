---
title: "Install i2p on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Anonymous overlay network - a network within a network"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install i2p on MacOS using homebrew

- App Name: i2p
- App description: Anonymous overlay network - a network within a network
- App Version: 0.9.50
- App Website: https://geti2p.net

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install i2p with the following command
   ```
   brew install i2p
   ```
4. i2p is ready to use now!