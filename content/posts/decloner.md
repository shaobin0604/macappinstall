---
title: "Install Decloner on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Fast and reliable duplicate files finder"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Decloner on MacOS using homebrew

- App Name: Decloner
- App description: Fast and reliable duplicate files finder
- App Version: 1.6.3,23
- App Website: https://www.pixelespressoapps.com/decloner/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Decloner with the following command
   ```
   brew install --cask decloner
   ```
4. Decloner is ready to use now!