---
title: "Install YoudaoDict on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Youdao Dictionary"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install YoudaoDict on MacOS using homebrew

- App Name: YoudaoDict
- App description: Youdao Dictionary
- App Version: 2.9.2,191
- App Website: https://cidian.youdao.com/index-mac.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install YoudaoDict with the following command
   ```
   brew install --cask youdaodict
   ```
4. YoudaoDict is ready to use now!