---
title: "Install glances on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Alternative to top/htop"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install glances on MacOS using homebrew

- App Name: glances
- App description: Alternative to top/htop
- App Version: 3.2.4.2
- App Website: https://nicolargo.github.io/glances/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install glances with the following command
   ```
   brew install glances
   ```
4. glances is ready to use now!