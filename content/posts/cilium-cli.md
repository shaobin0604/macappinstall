---
title: "Install cilium-cli on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CLI to install, manage & troubleshoot Kubernetes clusters running Cilium"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cilium-cli on MacOS using homebrew

- App Name: cilium-cli
- App description: CLI to install, manage & troubleshoot Kubernetes clusters running Cilium
- App Version: 0.10.3
- App Website: https://cilium.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cilium-cli with the following command
   ```
   brew install cilium-cli
   ```
4. cilium-cli is ready to use now!