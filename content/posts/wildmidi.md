---
title: "Install wildmidi on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Simple software midi player"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install wildmidi on MacOS using homebrew

- App Name: wildmidi
- App description: Simple software midi player
- App Version: 0.4.4
- App Website: https://www.mindwerks.net/projects/wildmidi/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install wildmidi with the following command
   ```
   brew install wildmidi
   ```
4. wildmidi is ready to use now!