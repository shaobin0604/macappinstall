---
title: "Install OpenArena on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install OpenArena on MacOS using homebrew

- App Name: OpenArena
- App description: null
- App Version: 0.8.8,r28
- App Website: https://openarena.ws/smfnews.php

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install OpenArena with the following command
   ```
   brew install --cask openarena
   ```
4. OpenArena is ready to use now!