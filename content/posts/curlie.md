---
title: "Install curlie on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Power of curl, ease of use of httpie"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install curlie on MacOS using homebrew

- App Name: curlie
- App description: Power of curl, ease of use of httpie
- App Version: 1.6.7
- App Website: https://curlie.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install curlie with the following command
   ```
   brew install curlie
   ```
4. curlie is ready to use now!