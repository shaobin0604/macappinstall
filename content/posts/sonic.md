---
title: "Install sonic on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Fast, lightweight & schema-less search backend"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sonic on MacOS using homebrew

- App Name: sonic
- App description: Fast, lightweight & schema-less search backend
- App Version: 1.3.2
- App Website: https://github.com/valeriansaliou/sonic

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sonic with the following command
   ```
   brew install sonic
   ```
4. sonic is ready to use now!