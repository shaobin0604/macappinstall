---
title: "Install maven-completion on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Bash completion for Maven"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install maven-completion on MacOS using homebrew

- App Name: maven-completion
- App description: Bash completion for Maven
- App Version: 20200420
- App Website: https://github.com/juven/maven-bash-completion

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install maven-completion with the following command
   ```
   brew install maven-completion
   ```
4. maven-completion is ready to use now!