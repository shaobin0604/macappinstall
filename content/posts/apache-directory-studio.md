---
title: "Install Apache Directory Studio on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Eclipse-based LDAP browser and directory client"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Apache Directory Studio on MacOS using homebrew

- App Name: Apache Directory Studio
- App description: Eclipse-based LDAP browser and directory client
- App Version: 2.0.0.v20210717-M17
- App Website: https://directory.apache.org/studio/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Apache Directory Studio with the following command
   ```
   brew install --cask apache-directory-studio
   ```
4. Apache Directory Studio is ready to use now!