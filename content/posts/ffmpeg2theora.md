---
title: "Install ffmpeg2theora on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Convert video files to Ogg Theora format"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ffmpeg2theora on MacOS using homebrew

- App Name: ffmpeg2theora
- App description: Convert video files to Ogg Theora format
- App Version: 0.30
- App Website: https://v2v.cc/~j/ffmpeg2theora/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ffmpeg2theora with the following command
   ```
   brew install ffmpeg2theora
   ```
4. ffmpeg2theora is ready to use now!