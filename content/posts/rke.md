---
title: "Install rke on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Rancher Kubernetes Engine, a Kubernetes installer that works everywhere"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install rke on MacOS using homebrew

- App Name: rke
- App description: Rancher Kubernetes Engine, a Kubernetes installer that works everywhere
- App Version: 1.3.7
- App Website: https://rancher.com/docs/rke/latest/en/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install rke with the following command
   ```
   brew install rke
   ```
4. rke is ready to use now!