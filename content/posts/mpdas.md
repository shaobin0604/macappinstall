---
title: "Install mpdas on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C++ client to submit tracks to audioscrobbler"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mpdas on MacOS using homebrew

- App Name: mpdas
- App description: C++ client to submit tracks to audioscrobbler
- App Version: 0.4.5
- App Website: https://www.50hz.ws/mpdas/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mpdas with the following command
   ```
   brew install mpdas
   ```
4. mpdas is ready to use now!