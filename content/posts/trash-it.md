---
title: "Install Trash It! on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to delete stuck trash"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Trash It! on MacOS using homebrew

- App Name: Trash It!
- App description: Tool to delete stuck trash
- App Version: 7.5
- App Website: https://nonamescriptware.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Trash It! with the following command
   ```
   brew install --cask trash-it
   ```
4. Trash It! is ready to use now!