---
title: "Install tealdeer on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Very fast implementation of tldr in Rust"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tealdeer on MacOS using homebrew

- App Name: tealdeer
- App description: Very fast implementation of tldr in Rust
- App Version: 1.5.0
- App Website: https://github.com/dbrgn/tealdeer

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tealdeer with the following command
   ```
   brew install tealdeer
   ```
4. tealdeer is ready to use now!