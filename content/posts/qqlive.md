---
title: "Install QQLive on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tencent video streaming and sharing platform"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install QQLive on MacOS using homebrew

- App Name: QQLive
- App description: Tencent video streaming and sharing platform
- App Version: 2.31.0.53087
- App Website: https://v.qq.com/download.html#mac

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install QQLive with the following command
   ```
   brew install --cask qqlive
   ```
4. QQLive is ready to use now!