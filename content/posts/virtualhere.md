---
title: "Install VirtualHere on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Use USB devices remotely over a network"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install VirtualHere on MacOS using homebrew

- App Name: VirtualHere
- App description: Use USB devices remotely over a network
- App Version: 5.2.7
- App Website: https://www.virtualhere.com/usb_client_software

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install VirtualHere with the following command
   ```
   brew install --cask virtualhere
   ```
4. VirtualHere is ready to use now!