---
title: "Install PingID on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cloud-based, multi-factor authentication"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install PingID on MacOS using homebrew

- App Name: PingID
- App description: Cloud-based, multi-factor authentication
- App Version: 1.7.2
- App Website: https://www.pingidentity.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install PingID with the following command
   ```
   brew install --cask pingid
   ```
4. PingID is ready to use now!