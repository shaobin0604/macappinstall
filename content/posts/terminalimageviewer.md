---
title: "Install terminalimageviewer on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Display images in a terminal using block graphic characters"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install terminalimageviewer on MacOS using homebrew

- App Name: terminalimageviewer
- App description: Display images in a terminal using block graphic characters
- App Version: 1.1.1
- App Website: https://github.com/stefanhaustein/TerminalImageViewer

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install terminalimageviewer with the following command
   ```
   brew install terminalimageviewer
   ```
4. terminalimageviewer is ready to use now!