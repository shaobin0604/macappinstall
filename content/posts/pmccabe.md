---
title: "Install pmccabe on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Calculate McCabe-style cyclomatic complexity for C/C++ code"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pmccabe on MacOS using homebrew

- App Name: pmccabe
- App description: Calculate McCabe-style cyclomatic complexity for C/C++ code
- App Version: 2.8
- App Website: https://gitlab.com/pmccabe/pmccabe

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pmccabe with the following command
   ```
   brew install pmccabe
   ```
4. pmccabe is ready to use now!