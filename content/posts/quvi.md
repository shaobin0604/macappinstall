---
title: "Install quvi on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Parse video download URLs"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install quvi on MacOS using homebrew

- App Name: quvi
- App description: Parse video download URLs
- App Version: 0.4.2
- App Website: https://quvi.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install quvi with the following command
   ```
   brew install quvi
   ```
4. quvi is ready to use now!