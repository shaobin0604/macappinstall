---
title: "Install BitShares on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GUI / Reference wallet for the BitShares blockchain"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install BitShares on MacOS using homebrew

- App Name: BitShares
- App description: GUI / Reference wallet for the BitShares blockchain
- App Version: 5.0.210216
- App Website: https://bitshares.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install BitShares with the following command
   ```
   brew install --cask bitshares
   ```
4. BitShares is ready to use now!