---
title: "Install elixir-build on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Elixir version of ruby-build"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install elixir-build on MacOS using homebrew

- App Name: elixir-build
- App description: Elixir version of ruby-build
- App Version: 20141001
- App Website: https://github.com/mururu/elixir-build

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install elixir-build with the following command
   ```
   brew install elixir-build
   ```
4. elixir-build is ready to use now!