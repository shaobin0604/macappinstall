---
title: "Install Gqrx on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Software-defined radio receiver powered by GNU Radio and Qt"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Gqrx on MacOS using homebrew

- App Name: Gqrx
- App description: Software-defined radio receiver powered by GNU Radio and Qt
- App Version: 2.15.8
- App Website: https://gqrx.dk/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Gqrx with the following command
   ```
   brew install --cask gqrx
   ```
4. Gqrx is ready to use now!