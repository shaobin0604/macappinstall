---
title: "Install Kext Updater on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Automatic updater for kernel extensions required by 'Hackintoshes'"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Kext Updater on MacOS using homebrew

- App Name: Kext Updater
- App description: Automatic updater for kernel extensions required by 'Hackintoshes'
- App Version: 4.0.1,401
- App Website: https://kextupdater.de/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Kext Updater with the following command
   ```
   brew install --cask kext-updater
   ```
4. Kext Updater is ready to use now!