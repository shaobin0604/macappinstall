---
title: "Install iputils on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Set of small useful utilities for Linux networking"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install iputils on MacOS using homebrew

- App Name: iputils
- App description: Set of small useful utilities for Linux networking
- App Version: 20211215
- App Website: https://github.com/iputils/iputils

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install iputils with the following command
   ```
   brew install iputils
   ```
4. iputils is ready to use now!