---
title: "Install Studio on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Build, render, and create LEGO instructions"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Studio on MacOS using homebrew

- App Name: Studio
- App description: Build, render, and create LEGO instructions
- App Version: 2.2.11_1
- App Website: https://www.bricklink.com/v3/studio/download.page

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Studio with the following command
   ```
   brew install --cask bricklink-studio
   ```
4. Studio is ready to use now!