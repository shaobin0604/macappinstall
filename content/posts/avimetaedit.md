---
title: "Install avimetaedit on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool for embedding, validating, and exporting of AVI files metadata"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install avimetaedit on MacOS using homebrew

- App Name: avimetaedit
- App description: Tool for embedding, validating, and exporting of AVI files metadata
- App Version: 1.0.2
- App Website: https://mediaarea.net/AVIMetaEdit

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install avimetaedit with the following command
   ```
   brew install avimetaedit
   ```
4. avimetaedit is ready to use now!