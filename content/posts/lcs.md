---
title: "Install lcs on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Satirical console-based political role-playing/strategy game"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install lcs on MacOS using homebrew

- App Name: lcs
- App description: Satirical console-based political role-playing/strategy game
- App Version: 4.07.4b
- App Website: https://sourceforge.net/projects/lcsgame/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install lcs with the following command
   ```
   brew install lcs
   ```
4. lcs is ready to use now!