---
title: "Install Cog on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Audio player"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Cog on MacOS using homebrew

- App Name: Cog
- App description: Audio player
- App Version: 1784,8eb2b4c4
- App Website: https://cog.losno.co/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Cog with the following command
   ```
   brew install --cask cog
   ```
4. Cog is ready to use now!