---
title: "Install tiff2png on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "TIFF to PNG converter"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tiff2png on MacOS using homebrew

- App Name: tiff2png
- App description: TIFF to PNG converter
- App Version: 0.92
- App Website: http://www.libpng.org/pub/png/apps/tiff2png.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tiff2png with the following command
   ```
   brew install tiff2png
   ```
4. tiff2png is ready to use now!