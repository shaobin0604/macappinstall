---
title: "Install Fastmarks on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Search and open web browser bookmarks"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Fastmarks on MacOS using homebrew

- App Name: Fastmarks
- App description: Search and open web browser bookmarks
- App Version: 1.1.0,5
- App Website: https://retina.studio/fastmarks/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Fastmarks with the following command
   ```
   brew install --cask fastmarks
   ```
4. Fastmarks is ready to use now!