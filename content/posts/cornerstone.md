---
title: "Install Cornerstone on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Subversion client"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Cornerstone on MacOS using homebrew

- App Name: Cornerstone
- App description: Subversion client
- App Version: 4.2
- App Website: https://cornerstone.assembla.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Cornerstone with the following command
   ```
   brew install --cask cornerstone
   ```
4. Cornerstone is ready to use now!