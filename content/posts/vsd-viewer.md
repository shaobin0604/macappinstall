---
title: "Install Nektony VSD Viewer on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Preview .VSD, .VDX, .VSDX file formats of Visio drawings"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Nektony VSD Viewer on MacOS using homebrew

- App Name: Nektony VSD Viewer
- App description: Preview .VSD, .VDX, .VSDX file formats of Visio drawings
- App Version: 6.15.1,556
- App Website: https://nektony.com/free-visio-viewer-mac

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Nektony VSD Viewer with the following command
   ```
   brew install --cask vsd-viewer
   ```
4. Nektony VSD Viewer is ready to use now!