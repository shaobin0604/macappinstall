---
title: "Install jpdfbookmarks on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Create and edit bookmarks on existing PDF files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install jpdfbookmarks on MacOS using homebrew

- App Name: jpdfbookmarks
- App description: Create and edit bookmarks on existing PDF files
- App Version: 2.5.2
- App Website: https://sourceforge.net/projects/jpdfbookmarks/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install jpdfbookmarks with the following command
   ```
   brew install jpdfbookmarks
   ```
4. jpdfbookmarks is ready to use now!