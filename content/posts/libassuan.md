---
title: "Install libassuan on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Assuan IPC Library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libassuan on MacOS using homebrew

- App Name: libassuan
- App description: Assuan IPC Library
- App Version: 2.5.5
- App Website: https://www.gnupg.org/related_software/libassuan/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libassuan with the following command
   ```
   brew install libassuan
   ```
4. libassuan is ready to use now!