---
title: "Install Noto on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Simple plain text editor"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Noto on MacOS using homebrew

- App Name: Noto
- App description: Simple plain text editor
- App Version: 1.2,828
- App Website: https://www.brunophilipe.com/software/noto/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Noto with the following command
   ```
   brew install --cask noto
   ```
4. Noto is ready to use now!