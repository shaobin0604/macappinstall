---
title: "Install privoxy on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Advanced filtering web proxy"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install privoxy on MacOS using homebrew

- App Name: privoxy
- App description: Advanced filtering web proxy
- App Version: 3.0.33
- App Website: https://www.privoxy.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install privoxy with the following command
   ```
   brew install privoxy
   ```
4. privoxy is ready to use now!