---
title: "Install stormssh on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line tool to manage your ssh connections"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install stormssh on MacOS using homebrew

- App Name: stormssh
- App description: Command-line tool to manage your ssh connections
- App Version: 0.7.0
- App Website: https://github.com/emre/storm

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install stormssh with the following command
   ```
   brew install stormssh
   ```
4. stormssh is ready to use now!