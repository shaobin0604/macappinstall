---
title: "Install SwiftPM Catalog on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Browse and search for Swift Package Manager packages"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install SwiftPM Catalog on MacOS using homebrew

- App Name: SwiftPM Catalog
- App description: Browse and search for Swift Package Manager packages
- App Version: 1.1.10,30
- App Website: https://zeezide.com/en/products/swiftpmcatalog/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install SwiftPM Catalog with the following command
   ```
   brew install --cask swiftpm-catalog
   ```
4. SwiftPM Catalog is ready to use now!