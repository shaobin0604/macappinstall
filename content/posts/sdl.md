---
title: "Install sdl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Low-level access to audio, keyboard, mouse, joystick and graphics"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sdl on MacOS using homebrew

- App Name: sdl
- App description: Low-level access to audio, keyboard, mouse, joystick and graphics
- App Version: 1.2.15
- App Website: https://www.libsdl.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sdl with the following command
   ```
   brew install sdl
   ```
4. sdl is ready to use now!