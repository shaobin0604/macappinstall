---
title: "Install rbenv-bundler on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Makes shims aware of bundle install paths"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install rbenv-bundler on MacOS using homebrew

- App Name: rbenv-bundler
- App description: Makes shims aware of bundle install paths
- App Version: 1.0.1
- App Website: https://github.com/carsomyr/rbenv-bundler

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install rbenv-bundler with the following command
   ```
   brew install rbenv-bundler
   ```
4. rbenv-bundler is ready to use now!