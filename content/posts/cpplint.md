---
title: "Install cpplint on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Static code checker for C++"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cpplint on MacOS using homebrew

- App Name: cpplint
- App description: Static code checker for C++
- App Version: 1.5.5
- App Website: https://pypi.org/project/cpplint/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cpplint with the following command
   ```
   brew install cpplint
   ```
4. cpplint is ready to use now!