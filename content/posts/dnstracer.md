---
title: "Install dnstracer on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Trace a chain of DNS servers to the source"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dnstracer on MacOS using homebrew

- App Name: dnstracer
- App description: Trace a chain of DNS servers to the source
- App Version: 1.9
- App Website: https://www.mavetju.org/unix/dnstracer.php

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dnstracer with the following command
   ```
   brew install dnstracer
   ```
4. dnstracer is ready to use now!