---
title: "Install kcat on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Generic command-line non-JVM Apache Kafka producer and consumer"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install kcat on MacOS using homebrew

- App Name: kcat
- App description: Generic command-line non-JVM Apache Kafka producer and consumer
- App Version: 1.7.0
- App Website: https://github.com/edenhill/kcat

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install kcat with the following command
   ```
   brew install kcat
   ```
4. kcat is ready to use now!