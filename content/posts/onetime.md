---
title: "Install onetime on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Encryption with one-time pads"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install onetime on MacOS using homebrew

- App Name: onetime
- App description: Encryption with one-time pads
- App Version: 1.81
- App Website: https://www.red-bean.com/onetime/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install onetime with the following command
   ```
   brew install onetime
   ```
4. onetime is ready to use now!