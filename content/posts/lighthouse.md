---
title: "Install lighthouse on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Rust Ethereum 2.0 Client"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install lighthouse on MacOS using homebrew

- App Name: lighthouse
- App description: Rust Ethereum 2.0 Client
- App Version: 2.1.3
- App Website: https://github.com/sigp/lighthouse

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install lighthouse with the following command
   ```
   brew install lighthouse
   ```
4. lighthouse is ready to use now!