---
title: "Install pugixml on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Light-weight C++ XML processing library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pugixml on MacOS using homebrew

- App Name: pugixml
- App description: Light-weight C++ XML processing library
- App Version: 1.12.1
- App Website: https://pugixml.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pugixml with the following command
   ```
   brew install pugixml
   ```
4. pugixml is ready to use now!