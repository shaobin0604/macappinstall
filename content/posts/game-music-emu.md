---
title: "Install game-music-emu on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Videogame music file emulator collection"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install game-music-emu on MacOS using homebrew

- App Name: game-music-emu
- App description: Videogame music file emulator collection
- App Version: 0.6.3
- App Website: https://bitbucket.org/mpyne/game-music-emu

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install game-music-emu with the following command
   ```
   brew install game-music-emu
   ```
4. game-music-emu is ready to use now!