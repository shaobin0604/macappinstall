---
title: "Install jnettop on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "View hosts/ports taking up the most network traffic"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install jnettop on MacOS using homebrew

- App Name: jnettop
- App description: View hosts/ports taking up the most network traffic
- App Version: 0.13.0
- App Website: https://web.archive.org/web/20161127214942/jnettop.kubs.info/wiki/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install jnettop with the following command
   ```
   brew install jnettop
   ```
4. jnettop is ready to use now!