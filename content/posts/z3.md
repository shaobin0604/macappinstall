---
title: "Install z3 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "High-performance theorem prover"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install z3 on MacOS using homebrew

- App Name: z3
- App description: High-performance theorem prover
- App Version: 4.8.14
- App Website: https://github.com/Z3Prover/z3

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install z3 with the following command
   ```
   brew install z3
   ```
4. z3 is ready to use now!