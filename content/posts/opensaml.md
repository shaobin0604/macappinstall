---
title: "Install opensaml on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library for Security Assertion Markup Language"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install opensaml on MacOS using homebrew

- App Name: opensaml
- App description: Library for Security Assertion Markup Language
- App Version: 3.2.1
- App Website: https://wiki.shibboleth.net/confluence/display/OpenSAML/Home

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install opensaml with the following command
   ```
   brew install opensaml
   ```
4. opensaml is ready to use now!