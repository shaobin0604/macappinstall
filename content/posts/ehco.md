---
title: "Install ehco on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Network relay tool and a typo :)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ehco on MacOS using homebrew

- App Name: ehco
- App description: Network relay tool and a typo :)
- App Version: 1.1.1
- App Website: https://github.com/Ehco1996/ehco

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ehco with the following command
   ```
   brew install ehco
   ```
4. ehco is ready to use now!