---
title: "Install Spark AR Studio on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Create and share augmented reality experiences using the Facebook family of apps"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Spark AR Studio on MacOS using homebrew

- App Name: Spark AR Studio
- App description: Create and share augmented reality experiences using the Facebook family of apps
- App Version: 129
- App Website: https://sparkar.facebook.com/ar-studio/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Spark AR Studio with the following command
   ```
   brew install --cask spark-ar-studio
   ```
4. Spark AR Studio is ready to use now!