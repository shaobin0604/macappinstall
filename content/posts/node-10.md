---
title: "Install node@10 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Platform built on V8 to build network applications"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install node@10 on MacOS using homebrew

- App Name: node@10
- App description: Platform built on V8 to build network applications
- App Version: 10.24.1
- App Website: https://nodejs.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install node@10 with the following command
   ```
   brew install node@10
   ```
4. node@10 is ready to use now!