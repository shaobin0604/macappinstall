---
title: "Install Krita on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Free and open-source painting and sketching program"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Krita on MacOS using homebrew

- App Name: Krita
- App description: Free and open-source painting and sketching program
- App Version: 5.0.2
- App Website: https://krita.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Krita with the following command
   ```
   brew install --cask krita
   ```
4. Krita is ready to use now!