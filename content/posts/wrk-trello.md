---
title: "Install wrk-trello on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line interface to Trello"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install wrk-trello on MacOS using homebrew

- App Name: wrk-trello
- App description: Command-line interface to Trello
- App Version: 1.0.1
- App Website: https://github.com/blangel/wrk

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install wrk-trello with the following command
   ```
   brew install wrk-trello
   ```
4. wrk-trello is ready to use now!