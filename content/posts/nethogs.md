---
title: "Install nethogs on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Net top tool grouping bandwidth per process"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install nethogs on MacOS using homebrew

- App Name: nethogs
- App description: Net top tool grouping bandwidth per process
- App Version: 0.8.6
- App Website: https://raboof.github.io/nethogs/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install nethogs with the following command
   ```
   brew install nethogs
   ```
4. nethogs is ready to use now!