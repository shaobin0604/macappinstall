---
title: "Install vivid on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Generator for LS_COLORS with support for multiple color themes"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install vivid on MacOS using homebrew

- App Name: vivid
- App description: Generator for LS_COLORS with support for multiple color themes
- App Version: 0.8.0
- App Website: https://github.com/sharkdp/vivid

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install vivid with the following command
   ```
   brew install vivid
   ```
4. vivid is ready to use now!