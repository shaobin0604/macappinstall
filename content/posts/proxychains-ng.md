---
title: "Install proxychains-ng on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Hook preloader"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install proxychains-ng on MacOS using homebrew

- App Name: proxychains-ng
- App description: Hook preloader
- App Version: 4.16
- App Website: https://sourceforge.net/projects/proxychains-ng/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install proxychains-ng with the following command
   ```
   brew install proxychains-ng
   ```
4. proxychains-ng is ready to use now!