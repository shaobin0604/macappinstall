---
title: "Install whalebrew on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Homebrew, but with Docker images"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install whalebrew on MacOS using homebrew

- App Name: whalebrew
- App description: Homebrew, but with Docker images
- App Version: 0.3.1
- App Website: https://github.com/whalebrew/whalebrew

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install whalebrew with the following command
   ```
   brew install whalebrew
   ```
4. whalebrew is ready to use now!