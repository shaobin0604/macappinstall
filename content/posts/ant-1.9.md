---
title: "Install ant@1.9 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Java build tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ant@1.9 on MacOS using homebrew

- App Name: ant@1.9
- App description: Java build tool
- App Version: 1.9.16
- App Website: https://ant.apache.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ant@1.9 with the following command
   ```
   brew install ant@1.9
   ```
4. ant@1.9 is ready to use now!