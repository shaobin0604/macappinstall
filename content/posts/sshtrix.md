---
title: "Install sshtrix on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "SSH login cracker"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sshtrix on MacOS using homebrew

- App Name: sshtrix
- App description: SSH login cracker
- App Version: 0.0.3
- App Website: https://nullsecurity.net/tools/cracker.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sshtrix with the following command
   ```
   brew install sshtrix
   ```
4. sshtrix is ready to use now!