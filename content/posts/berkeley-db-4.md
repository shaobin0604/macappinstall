---
title: "Install berkeley-db@4 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "High performance key/value database"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install berkeley-db@4 on MacOS using homebrew

- App Name: berkeley-db@4
- App description: High performance key/value database
- App Version: 4.8.30
- App Website: https://www.oracle.com/database/technologies/related/berkeleydb.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install berkeley-db@4 with the following command
   ```
   brew install berkeley-db@4
   ```
4. berkeley-db@4 is ready to use now!