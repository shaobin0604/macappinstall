---
title: "Install libbitcoin-blockchain on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Bitcoin Blockchain Library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libbitcoin-blockchain on MacOS using homebrew

- App Name: libbitcoin-blockchain
- App description: Bitcoin Blockchain Library
- App Version: 3.6.0
- App Website: https://github.com/libbitcoin/libbitcoin-blockchain

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libbitcoin-blockchain with the following command
   ```
   brew install libbitcoin-blockchain
   ```
4. libbitcoin-blockchain is ready to use now!