---
title: "Install cargo-watch on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Watches over your Cargo project's source"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cargo-watch on MacOS using homebrew

- App Name: cargo-watch
- App description: Watches over your Cargo project's source
- App Version: 8.1.2
- App Website: https://github.com/passcod/cargo-watch

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cargo-watch with the following command
   ```
   brew install cargo-watch
   ```
4. cargo-watch is ready to use now!