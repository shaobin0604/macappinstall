---
title: "Install ansible-lint on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Checks ansible playbooks for practices and behaviour"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ansible-lint on MacOS using homebrew

- App Name: ansible-lint
- App description: Checks ansible playbooks for practices and behaviour
- App Version: 5.4.0
- App Website: https://github.com/ansible/ansible-lint/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ansible-lint with the following command
   ```
   brew install ansible-lint
   ```
4. ansible-lint is ready to use now!