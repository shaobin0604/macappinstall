---
title: "Install gitbucket on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Git platform powered by Scala offering"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gitbucket on MacOS using homebrew

- App Name: gitbucket
- App description: Git platform powered by Scala offering
- App Version: 4.37.2
- App Website: https://github.com/gitbucket/gitbucket

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gitbucket with the following command
   ```
   brew install gitbucket
   ```
4. gitbucket is ready to use now!