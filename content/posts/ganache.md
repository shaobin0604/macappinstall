---
title: "Install Ganache on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Personal blockchain for Ethereum development"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Ganache on MacOS using homebrew

- App Name: Ganache
- App description: Personal blockchain for Ethereum development
- App Version: 2.5.4
- App Website: https://truffleframework.com/ganache/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Ganache with the following command
   ```
   brew install --cask ganache
   ```
4. Ganache is ready to use now!