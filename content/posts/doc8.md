---
title: "Install doc8 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Style checker for Sphinx documentation"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install doc8 on MacOS using homebrew

- App Name: doc8
- App description: Style checker for Sphinx documentation
- App Version: 0.10.1
- App Website: https://github.com/PyCQA/doc8

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install doc8 with the following command
   ```
   brew install doc8
   ```
4. doc8 is ready to use now!