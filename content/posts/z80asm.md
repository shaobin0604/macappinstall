---
title: "Install z80asm on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Assembler for the Zilog Z80 microprcessor and compatibles"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install z80asm on MacOS using homebrew

- App Name: z80asm
- App description: Assembler for the Zilog Z80 microprcessor and compatibles
- App Version: 1.8
- App Website: https://www.nongnu.org/z80asm/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install z80asm with the following command
   ```
   brew install z80asm
   ```
4. z80asm is ready to use now!