---
title: "Install zpaq on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Incremental, journaling command-line archiver"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install zpaq on MacOS using homebrew

- App Name: zpaq
- App description: Incremental, journaling command-line archiver
- App Version: 7.15
- App Website: http://mattmahoney.net/dc/zpaq.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install zpaq with the following command
   ```
   brew install zpaq
   ```
4. zpaq is ready to use now!