---
title: "Install z80dasm on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Disassembler for the Zilog Z80 microprocessor and compatibles"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install z80dasm on MacOS using homebrew

- App Name: z80dasm
- App description: Disassembler for the Zilog Z80 microprocessor and compatibles
- App Version: 1.1.6
- App Website: https://www.tablix.org/~avian/blog/articles/z80dasm/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install z80dasm with the following command
   ```
   brew install z80dasm
   ```
4. z80dasm is ready to use now!