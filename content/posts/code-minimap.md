---
title: "Install code-minimap on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "High performance code minimap generator"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install code-minimap on MacOS using homebrew

- App Name: code-minimap
- App description: High performance code minimap generator
- App Version: 0.6.4
- App Website: https://github.com/wfxr/code-minimap

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install code-minimap with the following command
   ```
   brew install code-minimap
   ```
4. code-minimap is ready to use now!