---
title: "Install c-kermit on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Scriptable network and serial communication for UNIX and VMS"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install c-kermit on MacOS using homebrew

- App Name: c-kermit
- App description: Scriptable network and serial communication for UNIX and VMS
- App Version: 9.0.302
- App Website: https://www.kermitproject.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install c-kermit with the following command
   ```
   brew install c-kermit
   ```
4. c-kermit is ready to use now!