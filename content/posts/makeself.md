---
title: "Install makeself on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Generates a self-extracting compressed tar archive"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install makeself on MacOS using homebrew

- App Name: makeself
- App description: Generates a self-extracting compressed tar archive
- App Version: 2.4.5
- App Website: https://makeself.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install makeself with the following command
   ```
   brew install makeself
   ```
4. makeself is ready to use now!