---
title: "Install Safe Exam Browser on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Web browser environment to carry out e-assessments safely"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Safe Exam Browser on MacOS using homebrew

- App Name: Safe Exam Browser
- App description: Web browser environment to carry out e-assessments safely
- App Version: 3.0
- App Website: https://safeexambrowser.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Safe Exam Browser with the following command
   ```
   brew install --cask safe-exam-browser
   ```
4. Safe Exam Browser is ready to use now!