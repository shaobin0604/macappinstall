---
title: "Install aldo on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Morse code learning tool released under GPL"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install aldo on MacOS using homebrew

- App Name: aldo
- App description: Morse code learning tool released under GPL
- App Version: 0.7.7
- App Website: https://www.nongnu.org/aldo/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install aldo with the following command
   ```
   brew install aldo
   ```
4. aldo is ready to use now!