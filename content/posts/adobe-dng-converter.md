---
title: "Install Adobe DNG Converter on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "DNG file converter"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Adobe DNG Converter on MacOS using homebrew

- App Name: Adobe DNG Converter
- App description: DNG file converter
- App Version: 14.1
- App Website: https://helpx.adobe.com/camera-raw/using/adobe-dng-converter.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Adobe DNG Converter with the following command
   ```
   brew install --cask adobe-dng-converter
   ```
4. Adobe DNG Converter is ready to use now!