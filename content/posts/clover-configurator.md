---
title: "Install Clover Configurator on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Clover EFI bootloader configuration helper"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Clover Configurator on MacOS using homebrew

- App Name: Clover Configurator
- App description: Clover EFI bootloader configuration helper
- App Version: 5.18.3.0
- App Website: https://mackie100projects.altervista.org/clover-configurator/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Clover Configurator with the following command
   ```
   brew install --cask clover-configurator
   ```
4. Clover Configurator is ready to use now!