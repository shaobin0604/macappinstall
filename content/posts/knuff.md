---
title: "Install Knuff on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Debug application for Apple Push Notification Service (APNs)"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Knuff on MacOS using homebrew

- App Name: Knuff
- App description: Debug application for Apple Push Notification Service (APNs)
- App Version: 1.3
- App Website: https://github.com/KnuffApp/Knuff

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Knuff with the following command
   ```
   brew install --cask knuff
   ```
4. Knuff is ready to use now!