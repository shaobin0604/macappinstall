---
title: "Install proftpd on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Highly configurable GPL-licensed FTP server software"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install proftpd on MacOS using homebrew

- App Name: proftpd
- App description: Highly configurable GPL-licensed FTP server software
- App Version: 1.3.7c
- App Website: http://www.proftpd.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install proftpd with the following command
   ```
   brew install proftpd
   ```
4. proftpd is ready to use now!