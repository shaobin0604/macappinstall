---
title: "Install Fenix on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Simple and visual static web server with collaboration features"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Fenix on MacOS using homebrew

- App Name: Fenix
- App description: Simple and visual static web server with collaboration features
- App Version: 2.0.0
- App Website: http://fenixwebserver.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Fenix with the following command
   ```
   brew install --cask fenix
   ```
4. Fenix is ready to use now!