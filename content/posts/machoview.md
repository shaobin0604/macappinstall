---
title: "Install MachOView on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Visual Mach-O file browser"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install MachOView on MacOS using homebrew

- App Name: MachOView
- App description: Visual Mach-O file browser
- App Version: 2.4.9200
- App Website: https://sourceforge.net/projects/machoview/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install MachOView with the following command
   ```
   brew install --cask machoview
   ```
4. MachOView is ready to use now!