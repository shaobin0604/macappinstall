---
title: "Install postgresql on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Object-relational database system"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install postgresql on MacOS using homebrew

- App Name: postgresql
- App description: Object-relational database system
- App Version: 14.2
- App Website: https://www.postgresql.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install postgresql with the following command
   ```
   brew install postgresql
   ```
4. postgresql is ready to use now!