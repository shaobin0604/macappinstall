---
title: "Install ry on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Ruby virtual env tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ry on MacOS using homebrew

- App Name: ry
- App description: Ruby virtual env tool
- App Version: 0.5.2
- App Website: https://github.com/jayferd/ry

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ry with the following command
   ```
   brew install ry
   ```
4. ry is ready to use now!