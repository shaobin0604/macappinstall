---
title: "Install ttygif on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Converts a ttyrec file into gif files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ttygif on MacOS using homebrew

- App Name: ttygif
- App description: Converts a ttyrec file into gif files
- App Version: 1.6.0
- App Website: https://github.com/icholy/ttygif

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ttygif with the following command
   ```
   brew install ttygif
   ```
4. ttygif is ready to use now!