---
title: "Install MongoDB Compass on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Explore and manipulate your MongoDB data"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install MongoDB Compass on MacOS using homebrew

- App Name: MongoDB Compass
- App description: Explore and manipulate your MongoDB data
- App Version: 1.30.1
- App Website: https://www.mongodb.com/products/compass

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install MongoDB Compass with the following command
   ```
   brew install --cask mongodb-compass
   ```
4. MongoDB Compass is ready to use now!