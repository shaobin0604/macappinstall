---
title: "Install openh264 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "H.264 codec from Cisco"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install openh264 on MacOS using homebrew

- App Name: openh264
- App description: H.264 codec from Cisco
- App Version: 2.2.0
- App Website: https://www.openh264.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install openh264 with the following command
   ```
   brew install openh264
   ```
4. openh264 is ready to use now!