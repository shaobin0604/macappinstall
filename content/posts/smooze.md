---
title: "Install Smooze on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Rediscover your scroll wheel mouse"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Smooze on MacOS using homebrew

- App Name: Smooze
- App description: Rediscover your scroll wheel mouse
- App Version: 1.9.22,492
- App Website: https://smooze.co/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Smooze with the following command
   ```
   brew install --cask smooze
   ```
4. Smooze is ready to use now!