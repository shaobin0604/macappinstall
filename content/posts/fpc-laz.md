---
title: "Install Pascal compiler for Lazarus on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Pascal compiler for Lazarus"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Pascal compiler for Lazarus on MacOS using homebrew

- App Name: Pascal compiler for Lazarus
- App description: Pascal compiler for Lazarus
- App Version: 3.2.2,2.2.0
- App Website: https://www.lazarus-ide.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Pascal compiler for Lazarus with the following command
   ```
   brew install --cask fpc-laz
   ```
4. Pascal compiler for Lazarus is ready to use now!