---
title: "Install hostess on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Idempotent command-line utility for managing your /etc/hosts file"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install hostess on MacOS using homebrew

- App Name: hostess
- App description: Idempotent command-line utility for managing your /etc/hosts file
- App Version: 0.5.2
- App Website: https://github.com/cbednarski/hostess

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install hostess with the following command
   ```
   brew install hostess
   ```
4. hostess is ready to use now!