---
title: "Install Home Assistant on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Companion app for Home Assistant home automation software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Home Assistant on MacOS using homebrew

- App Name: Home Assistant
- App description: Companion app for Home Assistant home automation software
- App Version: 2022.2,2022.345
- App Website: https://companion.home-assistant.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Home Assistant with the following command
   ```
   brew install --cask home-assistant
   ```
4. Home Assistant is ready to use now!