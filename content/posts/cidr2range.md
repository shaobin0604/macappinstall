---
title: "Install cidr2range on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Converts CIDRs to IP ranges"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cidr2range on MacOS using homebrew

- App Name: cidr2range
- App description: Converts CIDRs to IP ranges
- App Version: 1.2.0
- App Website: https://ipinfo.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cidr2range with the following command
   ```
   brew install cidr2range
   ```
4. cidr2range is ready to use now!