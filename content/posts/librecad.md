---
title: "Install LibreCAD on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install LibreCAD on MacOS using homebrew

- App Name: LibreCAD
- App description: null
- App Version: 2.1.3
- App Website: https://librecad.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install LibreCAD with the following command
   ```
   brew install --cask librecad
   ```
4. LibreCAD is ready to use now!