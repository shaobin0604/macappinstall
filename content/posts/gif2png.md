---
title: "Install gif2png on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Convert GIFs to PNGs"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gif2png on MacOS using homebrew

- App Name: gif2png
- App description: Convert GIFs to PNGs
- App Version: 2.5.13
- App Website: http://www.catb.org/~esr/gif2png/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gif2png with the following command
   ```
   brew install gif2png
   ```
4. gif2png is ready to use now!