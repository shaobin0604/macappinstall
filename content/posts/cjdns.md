---
title: "Install cjdns on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Advanced mesh routing system with cryptographic addressing"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cjdns on MacOS using homebrew

- App Name: cjdns
- App description: Advanced mesh routing system with cryptographic addressing
- App Version: 21.2
- App Website: https://github.com/cjdelisle/cjdns/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cjdns with the following command
   ```
   brew install cjdns
   ```
4. cjdns is ready to use now!