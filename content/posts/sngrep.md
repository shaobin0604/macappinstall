---
title: "Install sngrep on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line tool for displaying SIP calls message flows"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sngrep on MacOS using homebrew

- App Name: sngrep
- App description: Command-line tool for displaying SIP calls message flows
- App Version: 1.4.10
- App Website: https://github.com/irontec/sngrep

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sngrep with the following command
   ```
   brew install sngrep
   ```
4. sngrep is ready to use now!