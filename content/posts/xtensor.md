---
title: "Install xtensor on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Multi-dimensional arrays with broadcasting and lazy computing"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install xtensor on MacOS using homebrew

- App Name: xtensor
- App description: Multi-dimensional arrays with broadcasting and lazy computing
- App Version: 0.24.0
- App Website: https://xtensor.readthedocs.io/en/latest/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install xtensor with the following command
   ```
   brew install xtensor
   ```
4. xtensor is ready to use now!