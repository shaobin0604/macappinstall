---
title: "Install minimodem on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "General-purpose software audio FSK modem"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install minimodem on MacOS using homebrew

- App Name: minimodem
- App description: General-purpose software audio FSK modem
- App Version: 0.24
- App Website: http://www.whence.com/minimodem/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install minimodem with the following command
   ```
   brew install minimodem
   ```
4. minimodem is ready to use now!