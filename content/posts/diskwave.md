---
title: "Install DiskWave on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utility to determine what files and folders consume the most disk space"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install DiskWave on MacOS using homebrew

- App Name: DiskWave
- App description: Utility to determine what files and folders consume the most disk space
- App Version: 0.4-3
- App Website: https://diskwave.barthe.ph/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install DiskWave with the following command
   ```
   brew install --cask diskwave
   ```
4. DiskWave is ready to use now!