---
title: "Install vaulted on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Allows the secure storage and execution of environments"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install vaulted on MacOS using homebrew

- App Name: vaulted
- App description: Allows the secure storage and execution of environments
- App Version: 3.0.0
- App Website: https://github.com/miquella/vaulted

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install vaulted with the following command
   ```
   brew install vaulted
   ```
4. vaulted is ready to use now!