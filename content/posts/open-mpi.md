---
title: "Install open-mpi on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "High performance message passing library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install open-mpi on MacOS using homebrew

- App Name: open-mpi
- App description: High performance message passing library
- App Version: 4.1.2
- App Website: https://www.open-mpi.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install open-mpi with the following command
   ```
   brew install open-mpi
   ```
4. open-mpi is ready to use now!