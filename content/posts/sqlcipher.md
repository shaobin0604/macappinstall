---
title: "Install sqlcipher on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "SQLite extension providing 256-bit AES encryption"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sqlcipher on MacOS using homebrew

- App Name: sqlcipher
- App description: SQLite extension providing 256-bit AES encryption
- App Version: 4.5.0
- App Website: https://www.zetetic.net/sqlcipher/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sqlcipher with the following command
   ```
   brew install sqlcipher
   ```
4. sqlcipher is ready to use now!