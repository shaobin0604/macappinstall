---
title: "Install xmlsh on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "XML shell"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install xmlsh on MacOS using homebrew

- App Name: xmlsh
- App description: XML shell
- App Version: 1.2.5
- App Website: http://www.xmlsh.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install xmlsh with the following command
   ```
   brew install xmlsh
   ```
4. xmlsh is ready to use now!