---
title: "Install rosa-cli on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "RedHat OpenShift Service on AWS (ROSA) command-line interface"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install rosa-cli on MacOS using homebrew

- App Name: rosa-cli
- App description: RedHat OpenShift Service on AWS (ROSA) command-line interface
- App Version: 1.1.9
- App Website: https://www.openshift.com/products/amazon-openshift

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install rosa-cli with the following command
   ```
   brew install rosa-cli
   ```
4. rosa-cli is ready to use now!