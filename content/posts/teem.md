---
title: "Install teem on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Libraries for scientific raster data"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install teem on MacOS using homebrew

- App Name: teem
- App description: Libraries for scientific raster data
- App Version: 1.11.0
- App Website: https://teem.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install teem with the following command
   ```
   brew install teem
   ```
4. teem is ready to use now!