---
title: "Install Boxy Suite on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Gmail, Calendar, Keep and Contacts apps"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Boxy Suite on MacOS using homebrew

- App Name: Boxy Suite
- App description: Gmail, Calendar, Keep and Contacts apps
- App Version: latest
- App Website: https://www.boxysuite.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Boxy Suite with the following command
   ```
   brew install --cask boxy-suite
   ```
4. Boxy Suite is ready to use now!