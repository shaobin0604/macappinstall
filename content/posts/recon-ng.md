---
title: "Install recon-ng on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Web Reconnaissance Framework"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install recon-ng on MacOS using homebrew

- App Name: recon-ng
- App description: Web Reconnaissance Framework
- App Version: 5.1.2
- App Website: https://github.com/lanmaster53/recon-ng

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install recon-ng with the following command
   ```
   brew install recon-ng
   ```
4. recon-ng is ready to use now!