---
title: "Install metis on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Programs that partition graphs and order matrices"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install metis on MacOS using homebrew

- App Name: metis
- App description: Programs that partition graphs and order matrices
- App Version: 5.1.0
- App Website: http://glaros.dtc.umn.edu/gkhome/views/metis

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install metis with the following command
   ```
   brew install metis
   ```
4. metis is ready to use now!