---
title: "Install json5 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "JSON enhanced with usability features"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install json5 on MacOS using homebrew

- App Name: json5
- App description: JSON enhanced with usability features
- App Version: 2.2.0
- App Website: https://json5.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install json5 with the following command
   ```
   brew install json5
   ```
4. json5 is ready to use now!