---
title: "Install sdl_sound on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library to decode several popular sound file formats"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sdl_sound on MacOS using homebrew

- App Name: sdl_sound
- App description: Library to decode several popular sound file formats
- App Version: 1.0.3
- App Website: https://icculus.org/SDL_sound/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sdl_sound with the following command
   ```
   brew install sdl_sound
   ```
4. sdl_sound is ready to use now!