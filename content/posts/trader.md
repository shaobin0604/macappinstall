---
title: "Install trader on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Star Traders"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install trader on MacOS using homebrew

- App Name: trader
- App description: Star Traders
- App Version: 7.16
- App Website: https://www.zap.org.au/projects/trader/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install trader with the following command
   ```
   brew install trader
   ```
4. trader is ready to use now!