---
title: "Install spotbugs on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool for Java static analysis (FindBugs's successor)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install spotbugs on MacOS using homebrew

- App Name: spotbugs
- App description: Tool for Java static analysis (FindBugs's successor)
- App Version: 4.5.3
- App Website: https://spotbugs.github.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install spotbugs with the following command
   ```
   brew install spotbugs
   ```
4. spotbugs is ready to use now!