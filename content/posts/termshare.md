---
title: "Install termshare on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Interactive or view-only terminal sharing via client or web"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install termshare on MacOS using homebrew

- App Name: termshare
- App description: Interactive or view-only terminal sharing via client or web
- App Version: 0.2.0
- App Website: https://github.com/progrium/termshare

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install termshare with the following command
   ```
   brew install termshare
   ```
4. termshare is ready to use now!