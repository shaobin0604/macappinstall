---
title: "Install bgpdump on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C library for analyzing MRT/Zebra/Quagga dump files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bgpdump on MacOS using homebrew

- App Name: bgpdump
- App description: C library for analyzing MRT/Zebra/Quagga dump files
- App Version: 1.6.2
- App Website: https://github.com/RIPE-NCC/bgpdump/wiki

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bgpdump with the following command
   ```
   brew install bgpdump
   ```
4. bgpdump is ready to use now!