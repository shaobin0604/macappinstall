---
title: "Install harbour on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Portable, xBase-compatible programming language and environment"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install harbour on MacOS using homebrew

- App Name: harbour
- App description: Portable, xBase-compatible programming language and environment
- App Version: 3.0.0
- App Website: https://harbour.github.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install harbour with the following command
   ```
   brew install harbour
   ```
4. harbour is ready to use now!