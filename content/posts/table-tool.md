---
title: "Install Table Tool on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CSV file editor"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Table Tool on MacOS using homebrew

- App Name: Table Tool
- App description: CSV file editor
- App Version: 1.2.1
- App Website: https://github.com/jakob/TableTool

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Table Tool with the following command
   ```
   brew install --cask table-tool
   ```
4. Table Tool is ready to use now!