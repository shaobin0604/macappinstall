---
title: "Install schroedinger on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "High-speed implementation of the Dirac codec"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install schroedinger on MacOS using homebrew

- App Name: schroedinger
- App description: High-speed implementation of the Dirac codec
- App Version: 1.0.11
- App Website: https://launchpad.net/schroedinger

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install schroedinger with the following command
   ```
   brew install schroedinger
   ```
4. schroedinger is ready to use now!