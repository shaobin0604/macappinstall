---
title: "Install Prezi Next on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Presentation software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Prezi Next on MacOS using homebrew

- App Name: Prezi Next
- App description: Presentation software
- App Version: 1.53.2,27109
- App Website: https://prezi.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Prezi Next with the following command
   ```
   brew install --cask prezi-next
   ```
4. Prezi Next is ready to use now!