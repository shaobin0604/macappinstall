---
title: "Install whatmp3 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Small script to create mp3 torrents out of FLACs"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install whatmp3 on MacOS using homebrew

- App Name: whatmp3
- App description: Small script to create mp3 torrents out of FLACs
- App Version: 3.8
- App Website: https://github.com/RecursiveForest/whatmp3

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install whatmp3 with the following command
   ```
   brew install whatmp3
   ```
4. whatmp3 is ready to use now!