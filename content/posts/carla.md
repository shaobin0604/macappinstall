---
title: "Install carla on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Audio plugin host supporting LADSPA, LV2, VST2/3, SF2 and more"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install carla on MacOS using homebrew

- App Name: carla
- App description: Audio plugin host supporting LADSPA, LV2, VST2/3, SF2 and more
- App Version: 2.4.1
- App Website: https://kxstudio.linuxaudio.org/Applications:Carla

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install carla with the following command
   ```
   brew install carla
   ```
4. carla is ready to use now!