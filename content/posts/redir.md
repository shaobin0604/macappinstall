---
title: "Install redir on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Port redirector"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install redir on MacOS using homebrew

- App Name: redir
- App description: Port redirector
- App Version: 2.2.1-9
- App Website: https://web.archive.org/web/20190817033513/sammy.net/~sammy/hacks/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install redir with the following command
   ```
   brew install redir
   ```
4. redir is ready to use now!