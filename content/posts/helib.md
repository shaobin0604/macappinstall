---
title: "Install helib on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Implementation of homomorphic encryption"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install helib on MacOS using homebrew

- App Name: helib
- App description: Implementation of homomorphic encryption
- App Version: 2.2.1
- App Website: https://github.com/homenc/HElib

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install helib with the following command
   ```
   brew install helib
   ```
4. helib is ready to use now!