---
title: "Install nginx on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "HTTP(S) server and reverse proxy, and IMAP/POP3 proxy server"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install nginx on MacOS using homebrew

- App Name: nginx
- App description: HTTP(S) server and reverse proxy, and IMAP/POP3 proxy server
- App Version: 1.21.6
- App Website: https://nginx.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install nginx with the following command
   ```
   brew install nginx
   ```
4. nginx is ready to use now!