---
title: "Install terraform_landscape on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Improve Terraform's plan output"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install terraform_landscape on MacOS using homebrew

- App Name: terraform_landscape
- App description: Improve Terraform's plan output
- App Version: 0.3.4
- App Website: https://github.com/coinbase/terraform-landscape

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install terraform_landscape with the following command
   ```
   brew install terraform_landscape
   ```
4. terraform_landscape is ready to use now!