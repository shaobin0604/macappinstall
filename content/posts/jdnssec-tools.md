---
title: "Install jdnssec-tools on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Java command-line tools for DNSSEC"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install jdnssec-tools on MacOS using homebrew

- App Name: jdnssec-tools
- App description: Java command-line tools for DNSSEC
- App Version: 0.15
- App Website: https://github.com/dblacka/jdnssec-tools

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install jdnssec-tools with the following command
   ```
   brew install jdnssec-tools
   ```
4. jdnssec-tools is ready to use now!