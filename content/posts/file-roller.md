---
title: "Install file-roller on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GNOME archive manager"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install file-roller on MacOS using homebrew

- App Name: file-roller
- App description: GNOME archive manager
- App Version: 3.40.0
- App Website: https://wiki.gnome.org/Apps/FileRoller

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install file-roller with the following command
   ```
   brew install file-roller
   ```
4. file-roller is ready to use now!