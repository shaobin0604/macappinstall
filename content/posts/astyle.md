---
title: "Install astyle on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Source code beautifier for C, C++, C#, and Java"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install astyle on MacOS using homebrew

- App Name: astyle
- App description: Source code beautifier for C, C++, C#, and Java
- App Version: 3.1
- App Website: https://astyle.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install astyle with the following command
   ```
   brew install astyle
   ```
4. astyle is ready to use now!