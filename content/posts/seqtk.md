---
title: "Install seqtk on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Toolkit for processing sequences in FASTA/Q formats"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install seqtk on MacOS using homebrew

- App Name: seqtk
- App description: Toolkit for processing sequences in FASTA/Q formats
- App Version: 1.3
- App Website: https://github.com/lh3/seqtk

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install seqtk with the following command
   ```
   brew install seqtk
   ```
4. seqtk is ready to use now!