---
title: "Install libxrandr on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "X.Org: X Resize, Rotate and Reflection extension library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libxrandr on MacOS using homebrew

- App Name: libxrandr
- App description: X.Org: X Resize, Rotate and Reflection extension library
- App Version: 1.5.2
- App Website: https://www.x.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libxrandr with the following command
   ```
   brew install libxrandr
   ```
4. libxrandr is ready to use now!