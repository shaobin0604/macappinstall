---
title: "Install gnu-which on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GNU implementation of which utility"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gnu-which on MacOS using homebrew

- App Name: gnu-which
- App description: GNU implementation of which utility
- App Version: 2.21
- App Website: https://savannah.gnu.org/projects/which/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gnu-which with the following command
   ```
   brew install gnu-which
   ```
4. gnu-which is ready to use now!