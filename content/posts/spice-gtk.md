---
title: "Install spice-gtk on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GTK client/libraries for SPICE"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install spice-gtk on MacOS using homebrew

- App Name: spice-gtk
- App description: GTK client/libraries for SPICE
- App Version: 0.39
- App Website: https://www.spice-space.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install spice-gtk with the following command
   ```
   brew install spice-gtk
   ```
4. spice-gtk is ready to use now!