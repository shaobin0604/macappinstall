---
title: "Install Carbon Copy Cloner on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Hard disk backup and cloning utility"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Carbon Copy Cloner on MacOS using homebrew

- App Name: Carbon Copy Cloner
- App description: Hard disk backup and cloning utility
- App Version: 6.0.5.7252
- App Website: https://bombich.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Carbon Copy Cloner with the following command
   ```
   brew install --cask carbon-copy-cloner
   ```
4. Carbon Copy Cloner is ready to use now!