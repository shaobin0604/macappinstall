---
title: "Install pod2man on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Perl documentation generator"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pod2man on MacOS using homebrew

- App Name: pod2man
- App description: Perl documentation generator
- App Version: 4.14
- App Website: https://www.eyrie.org/~eagle/software/podlators/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pod2man with the following command
   ```
   brew install pod2man
   ```
4. pod2man is ready to use now!