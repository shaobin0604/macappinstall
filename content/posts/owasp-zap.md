---
title: "Install OWASP Zed Attack Proxy on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Free and open source web app scanner"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install OWASP Zed Attack Proxy on MacOS using homebrew

- App Name: OWASP Zed Attack Proxy
- App description: Free and open source web app scanner
- App Version: 2.11.1
- App Website: https://www.zaproxy.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install OWASP Zed Attack Proxy with the following command
   ```
   brew install --cask owasp-zap
   ```
4. OWASP Zed Attack Proxy is ready to use now!