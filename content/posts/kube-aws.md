---
title: "Install kube-aws on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line tool to declaratively manage Kubernetes clusters on AWS"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install kube-aws on MacOS using homebrew

- App Name: kube-aws
- App description: Command-line tool to declaratively manage Kubernetes clusters on AWS
- App Version: 0.16.4
- App Website: https://kubernetes-retired.github.io/kube-aws/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install kube-aws with the following command
   ```
   brew install kube-aws
   ```
4. kube-aws is ready to use now!