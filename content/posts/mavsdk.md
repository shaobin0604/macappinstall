---
title: "Install mavsdk on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "API and library for MAVLink compatible systems written in C++17"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mavsdk on MacOS using homebrew

- App Name: mavsdk
- App description: API and library for MAVLink compatible systems written in C++17
- App Version: 1.0.8
- App Website: https://mavsdk.mavlink.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mavsdk with the following command
   ```
   brew install mavsdk
   ```
4. mavsdk is ready to use now!