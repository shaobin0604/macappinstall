---
title: "Install cogl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Low level OpenGL abstraction library developed for Clutter"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cogl on MacOS using homebrew

- App Name: cogl
- App description: Low level OpenGL abstraction library developed for Clutter
- App Version: 1.22.8
- App Website: https://gitlab.gnome.org/GNOME/cogl

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cogl with the following command
   ```
   brew install cogl
   ```
4. cogl is ready to use now!