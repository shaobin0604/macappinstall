---
title: "Install textsniper on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Extract text from images and other digital documents in seconds"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install textsniper on MacOS using homebrew

- App Name: textsniper
- App description: Extract text from images and other digital documents in seconds
- App Version: 1.7.0
- App Website: https://textsniper.app/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install textsniper with the following command
   ```
   brew install --cask textsniper
   ```
4. textsniper is ready to use now!