---
title: "Install xorgproto on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "X.Org: Protocol Headers"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install xorgproto on MacOS using homebrew

- App Name: xorgproto
- App description: X.Org: Protocol Headers
- App Version: 2021.5
- App Website: https://www.x.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install xorgproto with the following command
   ```
   brew install xorgproto
   ```
4. xorgproto is ready to use now!