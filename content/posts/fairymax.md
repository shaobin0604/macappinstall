---
title: "Install fairymax on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "AI for playing Chess variants"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fairymax on MacOS using homebrew

- App Name: fairymax
- App description: AI for playing Chess variants
- App Version: 5.0b
- App Website: https://www.chessvariants.com/index/msdisplay.php?itemid=MSfairy-max

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fairymax with the following command
   ```
   brew install fairymax
   ```
4. fairymax is ready to use now!