---
title: "Install NAVER Whale on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Web browser"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install NAVER Whale on MacOS using homebrew

- App Name: NAVER Whale
- App description: Web browser
- App Version: 2.11.126.23
- App Website: https://whale.naver.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install NAVER Whale with the following command
   ```
   brew install --cask naver-whale
   ```
4. NAVER Whale is ready to use now!