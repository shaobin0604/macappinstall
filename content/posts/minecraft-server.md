---
title: "Install Minecraft Server on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Run a Minecraft multiplayer server"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Minecraft Server on MacOS using homebrew

- App Name: Minecraft Server
- App description: Run a Minecraft multiplayer server
- App Version: 1.18.1,125e5adf40c659fd3bce3e66e67a16bb49ecc1b9
- App Website: https://www.minecraft.net/en-us/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Minecraft Server with the following command
   ```
   brew install --cask minecraft-server
   ```
4. Minecraft Server is ready to use now!