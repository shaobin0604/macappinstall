---
title: "Install James on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Web Debugging Proxy Application"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install James on MacOS using homebrew

- App Name: James
- App description: Web Debugging Proxy Application
- App Version: 2.1.2
- App Website: https://github.com/james-proxy/james

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install James with the following command
   ```
   brew install --cask james
   ```
4. James is ready to use now!