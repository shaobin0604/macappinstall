---
title: "Install libcello on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Higher-level programming in C"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libcello on MacOS using homebrew

- App Name: libcello
- App description: Higher-level programming in C
- App Version: 2.1.0
- App Website: https://libcello.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libcello with the following command
   ```
   brew install libcello
   ```
4. libcello is ready to use now!