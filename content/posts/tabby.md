---
title: "Install Tabby on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Terminal emulator, SSH and serial client"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Tabby on MacOS using homebrew

- App Name: Tabby
- App description: Terminal emulator, SSH and serial client
- App Version: 1.0.172
- App Website: https://eugeny.github.io/tabby/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Tabby with the following command
   ```
   brew install --cask tabby
   ```
4. Tabby is ready to use now!