---
title: "Install vcsh on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Config manager based on git"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install vcsh on MacOS using homebrew

- App Name: vcsh
- App description: Config manager based on git
- App Version: 2.0.4
- App Website: https://github.com/RichiH/vcsh

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install vcsh with the following command
   ```
   brew install vcsh
   ```
4. vcsh is ready to use now!