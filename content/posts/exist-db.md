---
title: "Install eXist-db on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Native XML database and application platform"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install eXist-db on MacOS using homebrew

- App Name: eXist-db
- App description: Native XML database and application platform
- App Version: 6.0.1
- App Website: https://exist-db.org/exist/apps/homepage/index.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install eXist-db with the following command
   ```
   brew install --cask exist-db
   ```
4. eXist-db is ready to use now!