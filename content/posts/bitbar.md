---
title: "Install BitBar on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utility to display the output from any script or program in the menu bar"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install BitBar on MacOS using homebrew

- App Name: BitBar
- App description: Utility to display the output from any script or program in the menu bar
- App Version: 1.10.1
- App Website: https://github.com/matryer/bitbar/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install BitBar with the following command
   ```
   brew install --cask bitbar
   ```
4. BitBar is ready to use now!