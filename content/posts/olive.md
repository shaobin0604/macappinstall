---
title: "Install Olive on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Non-linear video editor"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Olive on MacOS using homebrew

- App Name: Olive
- App description: Non-linear video editor
- App Version: 1e3cf53
- App Website: https://olivevideoeditor.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Olive with the following command
   ```
   brew install --cask olive
   ```
4. Olive is ready to use now!