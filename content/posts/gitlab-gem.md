---
title: "Install gitlab-gem on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Ruby client and CLI for GitLab API"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gitlab-gem on MacOS using homebrew

- App Name: gitlab-gem
- App description: Ruby client and CLI for GitLab API
- App Version: 4.18.0
- App Website: https://github.com/NARKOZ/gitlab

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gitlab-gem with the following command
   ```
   brew install gitlab-gem
   ```
4. gitlab-gem is ready to use now!