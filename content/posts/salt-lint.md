---
title: "Install salt-lint on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Check for best practices in SaltStack"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install salt-lint on MacOS using homebrew

- App Name: salt-lint
- App description: Check for best practices in SaltStack
- App Version: 0.8.0
- App Website: https://github.com/warpnet/salt-lint

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install salt-lint with the following command
   ```
   brew install salt-lint
   ```
4. salt-lint is ready to use now!