---
title: "Install VimR on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GUI for the Neovim text editor"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install VimR on MacOS using homebrew

- App Name: VimR
- App description: GUI for the Neovim text editor
- App Version: 0.36.1,20220212.162106
- App Website: http://vimr.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install VimR with the following command
   ```
   brew install --cask vimr
   ```
4. VimR is ready to use now!