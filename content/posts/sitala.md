---
title: "Install Sitala on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Drum sampler plugin and standalone app"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Sitala on MacOS using homebrew

- App Name: Sitala
- App description: Drum sampler plugin and standalone app
- App Version: 1.0
- App Website: https://decomposer.de/sitala/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Sitala with the following command
   ```
   brew install --cask sitala
   ```
4. Sitala is ready to use now!