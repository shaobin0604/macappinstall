---
title: "Install libb64 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Base64 encoding/decoding library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libb64 on MacOS using homebrew

- App Name: libb64
- App description: Base64 encoding/decoding library
- App Version: 1.2.1
- App Website: https://libb64.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libb64 with the following command
   ```
   brew install libb64
   ```
4. libb64 is ready to use now!