---
title: "Install Keyman on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Reconfigures keyboard to type in another language"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Keyman on MacOS using homebrew

- App Name: Keyman
- App description: Reconfigures keyboard to type in another language
- App Version: 14.0.286
- App Website: https://keyman.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Keyman with the following command
   ```
   brew install --cask keyman
   ```
4. Keyman is ready to use now!