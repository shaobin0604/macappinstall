---
title: "Install CheatSheet on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to list all active shortcuts of the current application"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install CheatSheet on MacOS using homebrew

- App Name: CheatSheet
- App description: Tool to list all active shortcuts of the current application
- App Version: 1.6.2
- App Website: https://www.mediaatelier.com/CheatSheet/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install CheatSheet with the following command
   ```
   brew install --cask cheatsheet
   ```
4. CheatSheet is ready to use now!