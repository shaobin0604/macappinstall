---
title: "Install libmodbus on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Portable modbus library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libmodbus on MacOS using homebrew

- App Name: libmodbus
- App description: Portable modbus library
- App Version: 3.1.7
- App Website: https://libmodbus.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libmodbus with the following command
   ```
   brew install libmodbus
   ```
4. libmodbus is ready to use now!