---
title: "Install Android SDK Platform-Tools on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Android SDK component"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Android SDK Platform-Tools on MacOS using homebrew

- App Name: Android SDK Platform-Tools
- App description: Android SDK component
- App Version: 32.0.0
- App Website: https://developer.android.com/studio/releases/platform-tools

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Android SDK Platform-Tools with the following command
   ```
   brew install --cask android-platform-tools
   ```
4. Android SDK Platform-Tools is ready to use now!