---
title: "Install easyrpg-player on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "RPG Maker 2000/2003 games interpreter"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install easyrpg-player on MacOS using homebrew

- App Name: easyrpg-player
- App description: RPG Maker 2000/2003 games interpreter
- App Version: 0.7.0
- App Website: https://easyrpg.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install easyrpg-player with the following command
   ```
   brew install easyrpg-player
   ```
4. easyrpg-player is ready to use now!