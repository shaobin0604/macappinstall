---
title: "Install gitlab-ci-local on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Run gitlab pipelines locally as shell executor or docker executor"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gitlab-ci-local on MacOS using homebrew

- App Name: gitlab-ci-local
- App description: Run gitlab pipelines locally as shell executor or docker executor
- App Version: 4.28.2
- App Website: https://github.com/firecow/gitlab-ci-local

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gitlab-ci-local with the following command
   ```
   brew install gitlab-ci-local
   ```
4. gitlab-ci-local is ready to use now!