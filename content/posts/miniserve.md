---
title: "Install miniserve on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "High performance static file server"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install miniserve on MacOS using homebrew

- App Name: miniserve
- App description: High performance static file server
- App Version: 0.19.1
- App Website: https://github.com/svenstaro/miniserve

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install miniserve with the following command
   ```
   brew install miniserve
   ```
4. miniserve is ready to use now!