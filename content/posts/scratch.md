---
title: "Install Scratch on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Programs interactive stories, games, and animations"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Scratch on MacOS using homebrew

- App Name: Scratch
- App description: Programs interactive stories, games, and animations
- App Version: 3.28.0
- App Website: https://scratch.mit.edu/download

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Scratch with the following command
   ```
   brew install --cask scratch
   ```
4. Scratch is ready to use now!