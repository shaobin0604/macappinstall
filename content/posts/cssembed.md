---
title: "Install cssembed on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Automatic data URI embedding in CSS files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cssembed on MacOS using homebrew

- App Name: cssembed
- App description: Automatic data URI embedding in CSS files
- App Version: 0.4.5
- App Website: https://www.nczonline.net/blog/2009/11/03/automatic-data-uri-embedding-in-css-files/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cssembed with the following command
   ```
   brew install cssembed
   ```
4. cssembed is ready to use now!