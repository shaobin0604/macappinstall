---
title: "Install libewf on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library for support of the Expert Witness Compression Format"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libewf on MacOS using homebrew

- App Name: libewf
- App description: Library for support of the Expert Witness Compression Format
- App Version: 20140812
- App Website: https://github.com/libyal/libewf

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libewf with the following command
   ```
   brew install libewf
   ```
4. libewf is ready to use now!