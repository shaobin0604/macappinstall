---
title: "Install unrtf on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "RTF to other formats converter"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install unrtf on MacOS using homebrew

- App Name: unrtf
- App description: RTF to other formats converter
- App Version: 0.21.10
- App Website: https://www.gnu.org/software/unrtf/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install unrtf with the following command
   ```
   brew install unrtf
   ```
4. unrtf is ready to use now!