---
title: "Install xpdf on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "PDF viewer"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install xpdf on MacOS using homebrew

- App Name: xpdf
- App description: PDF viewer
- App Version: 4.03
- App Website: https://www.xpdfreader.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install xpdf with the following command
   ```
   brew install xpdf
   ```
4. xpdf is ready to use now!