---
title: "Install libtensorflow on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C interface for Google's OS library for Machine Intelligence"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libtensorflow on MacOS using homebrew

- App Name: libtensorflow
- App description: C interface for Google's OS library for Machine Intelligence
- App Version: 2.7.0
- App Website: https://www.tensorflow.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libtensorflow with the following command
   ```
   brew install libtensorflow
   ```
4. libtensorflow is ready to use now!