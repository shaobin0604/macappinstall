---
title: "Install curaengine on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C++ 3D printing GCode generator"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install curaengine on MacOS using homebrew

- App Name: curaengine
- App description: C++ 3D printing GCode generator
- App Version: 4.13.1
- App Website: https://github.com/Ultimaker/CuraEngine

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install curaengine with the following command
   ```
   brew install curaengine
   ```
4. curaengine is ready to use now!