---
title: "Install iBackup Viewer on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Extract Data from iPhone Backups"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install iBackup Viewer on MacOS using homebrew

- App Name: iBackup Viewer
- App description: Extract Data from iPhone Backups
- App Version: 4.2550
- App Website: https://www.imactools.com/iphonebackupviewer/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install iBackup Viewer with the following command
   ```
   brew install --cask ibackup-viewer
   ```
4. iBackup Viewer is ready to use now!