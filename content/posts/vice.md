---
title: "Install vice on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Versatile Commodore Emulator"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install vice on MacOS using homebrew

- App Name: vice
- App description: Versatile Commodore Emulator
- App Version: 3.6.1
- App Website: https://sourceforge.net/projects/vice-emu/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install vice with the following command
   ```
   brew install vice
   ```
4. vice is ready to use now!