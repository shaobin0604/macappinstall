---
title: "Install libatomic_ops on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Implementations for atomic memory update operations"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libatomic_ops on MacOS using homebrew

- App Name: libatomic_ops
- App description: Implementations for atomic memory update operations
- App Version: 7.6.12
- App Website: https://github.com/ivmai/libatomic_ops/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libatomic_ops with the following command
   ```
   brew install libatomic_ops
   ```
4. libatomic_ops is ready to use now!