---
title: "Install apollo on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Multi-protocol messaging broker based on ActiveMQ"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install apollo on MacOS using homebrew

- App Name: apollo
- App description: Multi-protocol messaging broker based on ActiveMQ
- App Version: 1.7.1
- App Website: https://activemq.apache.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install apollo with the following command
   ```
   brew install apollo
   ```
4. apollo is ready to use now!