---
title: "Install Menu Bar Splitter on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utility that adds dividers to your menu bar"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Menu Bar Splitter on MacOS using homebrew

- App Name: Menu Bar Splitter
- App description: Utility that adds dividers to your menu bar
- App Version: 2.0.1
- App Website: https://github.com/jwhamilton99/menu-bar-splitter

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Menu Bar Splitter with the following command
   ```
   brew install --cask menu-bar-splitter
   ```
4. Menu Bar Splitter is ready to use now!