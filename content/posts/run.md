---
title: "Install run on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Easily manage and invoke small scripts and wrappers"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install run on MacOS using homebrew

- App Name: run
- App description: Easily manage and invoke small scripts and wrappers
- App Version: 0.8.0
- App Website: https://github.com/TekWizely/run

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install run with the following command
   ```
   brew install run
   ```
4. run is ready to use now!