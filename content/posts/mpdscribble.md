---
title: "Install mpdscribble on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Last.fm reporting client for mpd"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mpdscribble on MacOS using homebrew

- App Name: mpdscribble
- App description: Last.fm reporting client for mpd
- App Version: 0.23
- App Website: https://www.musicpd.org/clients/mpdscribble/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mpdscribble with the following command
   ```
   brew install mpdscribble
   ```
4. mpdscribble is ready to use now!