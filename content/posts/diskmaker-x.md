---
title: "Install DiskMaker X on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to build a system install disk"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install DiskMaker X on MacOS using homebrew

- App Name: DiskMaker X
- App description: Tool to build a system install disk
- App Version: 9.0
- App Website: https://diskmakerx.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install DiskMaker X with the following command
   ```
   brew install --cask diskmaker-x
   ```
4. DiskMaker X is ready to use now!