---
title: "Install empty on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Lightweight Expect-like PTY tool for shell scripts"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install empty on MacOS using homebrew

- App Name: empty
- App description: Lightweight Expect-like PTY tool for shell scripts
- App Version: 0.6.21b
- App Website: https://empty.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install empty with the following command
   ```
   brew install empty
   ```
4. empty is ready to use now!