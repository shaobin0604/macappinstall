---
title: "Install doxymacs on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Elisp package for using doxygen under Emacs"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install doxymacs on MacOS using homebrew

- App Name: doxymacs
- App description: Elisp package for using doxygen under Emacs
- App Version: 1.8.0
- App Website: https://doxymacs.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install doxymacs with the following command
   ```
   brew install doxymacs
   ```
4. doxymacs is ready to use now!