---
title: "Install pgbadger on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Log analyzer for PostgreSQL"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pgbadger on MacOS using homebrew

- App Name: pgbadger
- App description: Log analyzer for PostgreSQL
- App Version: 11.7
- App Website: https://pgbadger.darold.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pgbadger with the following command
   ```
   brew install pgbadger
   ```
4. pgbadger is ready to use now!