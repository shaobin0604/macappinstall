---
title: "Install node@16 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Platform built on V8 to build network applications"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install node@16 on MacOS using homebrew

- App Name: node@16
- App description: Platform built on V8 to build network applications
- App Version: 16.14.0
- App Website: https://nodejs.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install node@16 with the following command
   ```
   brew install node@16
   ```
4. node@16 is ready to use now!