---
title: "Install topgit on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Git patch queue manager"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install topgit on MacOS using homebrew

- App Name: topgit
- App description: Git patch queue manager
- App Version: 0.19.13
- App Website: https://github.com/mackyle/topgit

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install topgit with the following command
   ```
   brew install topgit
   ```
4. topgit is ready to use now!