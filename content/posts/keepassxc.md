---
title: "Install KeePassXC on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Password manager app"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install KeePassXC on MacOS using homebrew

- App Name: KeePassXC
- App description: Password manager app
- App Version: 2.6.6
- App Website: https://keepassxc.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install KeePassXC with the following command
   ```
   brew install --cask keepassxc
   ```
4. KeePassXC is ready to use now!