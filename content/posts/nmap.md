---
title: "Install nmap on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Port scanning utility for large networks"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install nmap on MacOS using homebrew

- App Name: nmap
- App description: Port scanning utility for large networks
- App Version: 7.92
- App Website: https://nmap.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install nmap with the following command
   ```
   brew install nmap
   ```
4. nmap is ready to use now!