---
title: "Install jerm on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Communication terminal through serial and TCP/IP interfaces"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install jerm on MacOS using homebrew

- App Name: jerm
- App description: Communication terminal through serial and TCP/IP interfaces
- App Version: 0.8096
- App Website: https://web.archive.org/web/20160719014241/bsddiary.net/jerm/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install jerm with the following command
   ```
   brew install jerm
   ```
4. jerm is ready to use now!