---
title: "Install Shuttle on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Simple shortcut menu"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Shuttle on MacOS using homebrew

- App Name: Shuttle
- App description: Simple shortcut menu
- App Version: 1.2.9
- App Website: https://fitztrev.github.io/shuttle/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Shuttle with the following command
   ```
   brew install --cask shuttle
   ```
4. Shuttle is ready to use now!