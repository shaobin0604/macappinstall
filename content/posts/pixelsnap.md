---
title: "Install PixelSnap on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Screen measuring tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install PixelSnap on MacOS using homebrew

- App Name: PixelSnap
- App description: Screen measuring tool
- App Version: 2.5
- App Website: https://getpixelsnap.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install PixelSnap with the following command
   ```
   brew install --cask pixelsnap
   ```
4. PixelSnap is ready to use now!