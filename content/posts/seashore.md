---
title: "Install Seashore on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Image editing application similar to GIMP"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Seashore on MacOS using homebrew

- App Name: Seashore
- App description: Image editing application similar to GIMP
- App Version: 2.5.10
- App Website: https://github.com/robaho/seashore

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Seashore with the following command
   ```
   brew install --cask seashore
   ```
4. Seashore is ready to use now!