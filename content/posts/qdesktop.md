---
title: "Install Qdesktop on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "App to set a website as the desktop background"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Qdesktop on MacOS using homebrew

- App Name: Qdesktop
- App description: App to set a website as the desktop background
- App Version: 0.1.2,6
- App Website: https://github.com/qvacua/qdesktop

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Qdesktop with the following command
   ```
   brew install --cask qdesktop
   ```
4. Qdesktop is ready to use now!