---
title: "Install efl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Enlightenment Foundation Libraries"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install efl on MacOS using homebrew

- App Name: efl
- App description: Enlightenment Foundation Libraries
- App Version: 1.25.1
- App Website: https://www.enlightenment.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install efl with the following command
   ```
   brew install efl
   ```
4. efl is ready to use now!