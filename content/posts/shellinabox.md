---
title: "Install shellinabox on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Export command-line tools to web based terminal emulator"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install shellinabox on MacOS using homebrew

- App Name: shellinabox
- App description: Export command-line tools to web based terminal emulator
- App Version: 2.20
- App Website: https://github.com/shellinabox/shellinabox

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install shellinabox with the following command
   ```
   brew install shellinabox
   ```
4. shellinabox is ready to use now!