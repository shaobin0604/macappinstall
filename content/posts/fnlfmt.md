---
title: "Install fnlfmt on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Formatter for Fennel code"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fnlfmt on MacOS using homebrew

- App Name: fnlfmt
- App description: Formatter for Fennel code
- App Version: 0.2.2
- App Website: https://git.sr.ht/~technomancy/fnlfmt

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fnlfmt with the following command
   ```
   brew install fnlfmt
   ```
4. fnlfmt is ready to use now!