---
title: "Install folderify on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Generate pixel-perfect macOS folder icons in the native style"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install folderify on MacOS using homebrew

- App Name: folderify
- App description: Generate pixel-perfect macOS folder icons in the native style
- App Version: 2.3.0
- App Website: https://github.com/lgarron/folderify

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install folderify with the following command
   ```
   brew install folderify
   ```
4. folderify is ready to use now!