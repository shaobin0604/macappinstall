---
title: "Install golo on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Lightweight dynamic language for the JVM"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install golo on MacOS using homebrew

- App Name: golo
- App description: Lightweight dynamic language for the JVM
- App Version: 3.4.0
- App Website: https://golo-lang.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install golo with the following command
   ```
   brew install golo
   ```
4. golo is ready to use now!