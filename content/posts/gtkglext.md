---
title: "Install gtkglext on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "OpenGL extension to GTK+"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gtkglext on MacOS using homebrew

- App Name: gtkglext
- App description: OpenGL extension to GTK+
- App Version: 1.2.0
- App Website: https://developer.gnome.org/gtkglext/stable/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gtkglext with the following command
   ```
   brew install gtkglext
   ```
4. gtkglext is ready to use now!