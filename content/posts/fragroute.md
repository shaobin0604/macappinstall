---
title: "Install fragroute on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Intercepts, modifies and rewrites egress traffic for a specified host"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fragroute on MacOS using homebrew

- App Name: fragroute
- App description: Intercepts, modifies and rewrites egress traffic for a specified host
- App Version: 1.2
- App Website: https://www.monkey.org/~dugsong/fragroute/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fragroute with the following command
   ```
   brew install fragroute
   ```
4. fragroute is ready to use now!