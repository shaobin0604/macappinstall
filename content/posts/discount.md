---
title: "Install discount on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C implementation of Markdown"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install discount on MacOS using homebrew

- App Name: discount
- App description: C implementation of Markdown
- App Version: 2.2.7
- App Website: https://www.pell.portland.or.us/~orc/Code/discount/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install discount with the following command
   ```
   brew install discount
   ```
4. discount is ready to use now!