---
title: "Install YouTube Downloader on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Simple menu bar app to download YouTube movies"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install YouTube Downloader on MacOS using homebrew

- App Name: YouTube Downloader
- App description: Simple menu bar app to download YouTube movies
- App Version: 0.9
- App Website: https://github.com/DenBeke/YouTube-Downloader-for-macOS

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install YouTube Downloader with the following command
   ```
   brew install --cask youtube-downloader
   ```
4. YouTube Downloader is ready to use now!