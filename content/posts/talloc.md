---
title: "Install talloc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Hierarchical, reference-counted memory pool with destructors"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install talloc on MacOS using homebrew

- App Name: talloc
- App description: Hierarchical, reference-counted memory pool with destructors
- App Version: 2.3.3
- App Website: https://talloc.samba.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install talloc with the following command
   ```
   brew install talloc
   ```
4. talloc is ready to use now!