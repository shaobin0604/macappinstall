---
title: "Install yamale on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Schema and validator for YAML"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install yamale on MacOS using homebrew

- App Name: yamale
- App description: Schema and validator for YAML
- App Version: 4.0.3
- App Website: https://github.com/23andMe/Yamale

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install yamale with the following command
   ```
   brew install yamale
   ```
4. yamale is ready to use now!