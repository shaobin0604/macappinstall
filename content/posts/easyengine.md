---
title: "Install easyengine on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line control panel to manage WordPress sites"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install easyengine on MacOS using homebrew

- App Name: easyengine
- App description: Command-line control panel to manage WordPress sites
- App Version: 4.5.3
- App Website: https://easyengine.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install easyengine with the following command
   ```
   brew install easyengine
   ```
4. easyengine is ready to use now!