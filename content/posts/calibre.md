---
title: "Install calibre on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "E-books management software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install calibre on MacOS using homebrew

- App Name: calibre
- App description: E-books management software
- App Version: 5.36.0
- App Website: https://calibre-ebook.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install calibre with the following command
   ```
   brew install --cask calibre
   ```
4. calibre is ready to use now!