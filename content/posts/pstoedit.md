---
title: "Install pstoedit on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Convert PostScript and PDF files to editable vector graphics"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pstoedit on MacOS using homebrew

- App Name: pstoedit
- App description: Convert PostScript and PDF files to editable vector graphics
- App Version: 3.78
- App Website: http://www.pstoedit.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pstoedit with the following command
   ```
   brew install pstoedit
   ```
4. pstoedit is ready to use now!