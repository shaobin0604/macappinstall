---
title: "Install yaf on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Yet another flowmeter: processes packet data from pcap(3)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install yaf on MacOS using homebrew

- App Name: yaf
- App description: Yet another flowmeter: processes packet data from pcap(3)
- App Version: 2.12.2
- App Website: https://tools.netsa.cert.org/yaf/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install yaf with the following command
   ```
   brew install yaf
   ```
4. yaf is ready to use now!