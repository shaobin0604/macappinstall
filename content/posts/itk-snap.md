---
title: "Install ITK-SNAP on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Segment structures in 3D medical images"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ITK-SNAP on MacOS using homebrew

- App Name: ITK-SNAP
- App description: Segment structures in 3D medical images
- App Version: 3.8.0,20190612
- App Website: http://www.itksnap.org/pmwiki/pmwiki.php

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ITK-SNAP with the following command
   ```
   brew install --cask itk-snap
   ```
4. ITK-SNAP is ready to use now!