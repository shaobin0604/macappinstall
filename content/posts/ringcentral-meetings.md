---
title: "Install RingCentral Meetings for Mac on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Video conferencing, screen sharing, and team messaging platform"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install RingCentral Meetings for Mac on MacOS using homebrew

- App Name: RingCentral Meetings for Mac
- App description: Video conferencing, screen sharing, and team messaging platform
- App Version: latest
- App Website: https://www.ringcentral.com/online-meetings/overview.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install RingCentral Meetings for Mac with the following command
   ```
   brew install --cask ringcentral-meetings
   ```
4. RingCentral Meetings for Mac is ready to use now!