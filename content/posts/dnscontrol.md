---
title: "Install dnscontrol on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "It is system for maintaining DNS zones"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dnscontrol on MacOS using homebrew

- App Name: dnscontrol
- App description: It is system for maintaining DNS zones
- App Version: 3.14.0
- App Website: https://github.com/StackExchange/dnscontrol

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dnscontrol with the following command
   ```
   brew install dnscontrol
   ```
4. dnscontrol is ready to use now!