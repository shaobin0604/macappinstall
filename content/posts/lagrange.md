---
title: "Install Lagrange on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Desktop GUI client for browsing Geminispace"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Lagrange on MacOS using homebrew

- App Name: Lagrange
- App description: Desktop GUI client for browsing Geminispace
- App Version: 1.10.5
- App Website: https://gmi.skyjake.fi/lagrange/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Lagrange with the following command
   ```
   brew install --cask lagrange
   ```
4. Lagrange is ready to use now!