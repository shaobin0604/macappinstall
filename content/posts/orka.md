---
title: "Install Orka CLI on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Orchestration with Kubernetes on Apple"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Orka CLI on MacOS using homebrew

- App Name: Orka CLI
- App description: Orchestration with Kubernetes on Apple
- App Version: 2.0.0
- App Website: https://orkadocs.macstadium.com/docs

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Orka CLI with the following command
   ```
   brew install --cask orka
   ```
4. Orka CLI is ready to use now!