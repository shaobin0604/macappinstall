---
title: "Install TNEF's Enough on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Read and extract files from Microsoft TNEF files"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install TNEF's Enough on MacOS using homebrew

- App Name: TNEF's Enough
- App description: Read and extract files from Microsoft TNEF files
- App Version: 3.8
- App Website: https://www.joshjacob.com/mac-development/tnef.php

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install TNEF's Enough with the following command
   ```
   brew install --cask tnefs-enough
   ```
4. TNEF's Enough is ready to use now!