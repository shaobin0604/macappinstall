---
title: "Install libgda on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Provides unified data access to the GNOME project"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libgda on MacOS using homebrew

- App Name: libgda
- App description: Provides unified data access to the GNOME project
- App Version: 5.2.10
- App Website: https://www.gnome-db.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libgda with the following command
   ```
   brew install libgda
   ```
4. libgda is ready to use now!