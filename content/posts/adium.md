---
title: "Install Adium on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Instant messaging application"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Adium on MacOS using homebrew

- App Name: Adium
- App description: Instant messaging application
- App Version: 1.5.10.4
- App Website: https://www.adium.im/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Adium with the following command
   ```
   brew install --cask adium
   ```
4. Adium is ready to use now!