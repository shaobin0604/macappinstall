---
title: "Install jp2a on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Convert JPG images to ASCII"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install jp2a on MacOS using homebrew

- App Name: jp2a
- App description: Convert JPG images to ASCII
- App Version: 1.0.6
- App Website: https://csl.name/jp2a/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install jp2a with the following command
   ```
   brew install jp2a
   ```
4. jp2a is ready to use now!