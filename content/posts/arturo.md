---
title: "Install arturo on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Simple, modern and portable programming language for efficient scripting"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install arturo on MacOS using homebrew

- App Name: arturo
- App description: Simple, modern and portable programming language for efficient scripting
- App Version: 0.9.78
- App Website: https://github.com/arturo-lang/arturo

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install arturo with the following command
   ```
   brew install arturo
   ```
4. arturo is ready to use now!