---
title: "Install ratfor on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Rational Fortran"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ratfor on MacOS using homebrew

- App Name: ratfor
- App description: Rational Fortran
- App Version: 1.05
- App Website: http://www.dgate.org/ratfor/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ratfor with the following command
   ```
   brew install ratfor
   ```
4. ratfor is ready to use now!