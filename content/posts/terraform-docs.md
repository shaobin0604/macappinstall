---
title: "Install terraform-docs on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to generate documentation from Terraform modules"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install terraform-docs on MacOS using homebrew

- App Name: terraform-docs
- App description: Tool to generate documentation from Terraform modules
- App Version: 0.16.0
- App Website: https://github.com/terraform-docs/terraform-docs

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install terraform-docs with the following command
   ```
   brew install terraform-docs
   ```
4. terraform-docs is ready to use now!