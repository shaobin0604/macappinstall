---
title: "Install Blockbench on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "3D model editor for boxy models and pixel art textures"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Blockbench on MacOS using homebrew

- App Name: Blockbench
- App description: 3D model editor for boxy models and pixel art textures
- App Version: 4.1.4
- App Website: https://www.blockbench.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Blockbench with the following command
   ```
   brew install --cask blockbench
   ```
4. Blockbench is ready to use now!