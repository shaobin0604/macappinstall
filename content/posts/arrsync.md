---
title: "Install arRsync on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Graphical front end for the utility rsync"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install arRsync on MacOS using homebrew

- App Name: arRsync
- App description: Graphical front end for the utility rsync
- App Version: 0.4.1
- App Website: http://arrsync.sourceforge.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install arRsync with the following command
   ```
   brew install --cask arrsync
   ```
4. arRsync is ready to use now!