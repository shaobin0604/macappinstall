---
title: "Install FileBot on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool for organizing and renaming movies, TV shows, anime or music"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install FileBot on MacOS using homebrew

- App Name: FileBot
- App description: Tool for organizing and renaming movies, TV shows, anime or music
- App Version: 4.9.4
- App Website: https://www.filebot.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install FileBot with the following command
   ```
   brew install --cask filebot
   ```
4. FileBot is ready to use now!