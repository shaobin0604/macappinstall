---
title: "Install shfmt on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Autoformat shell script source code"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install shfmt on MacOS using homebrew

- App Name: shfmt
- App description: Autoformat shell script source code
- App Version: 3.4.2
- App Website: https://github.com/mvdan/sh

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install shfmt with the following command
   ```
   brew install shfmt
   ```
4. shfmt is ready to use now!