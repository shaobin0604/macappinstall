---
title: "Install semgrep on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Easily detect and prevent bugs and anti-patterns in your codebase"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install semgrep on MacOS using homebrew

- App Name: semgrep
- App description: Easily detect and prevent bugs and anti-patterns in your codebase
- App Version: 0.82.0
- App Website: https://semgrep.dev

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install semgrep with the following command
   ```
   brew install semgrep
   ```
4. semgrep is ready to use now!