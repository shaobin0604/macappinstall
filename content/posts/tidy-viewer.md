---
title: "Install tidy-viewer on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CLI csv pretty printer"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tidy-viewer on MacOS using homebrew

- App Name: tidy-viewer
- App description: CLI csv pretty printer
- App Version: 1.4.3
- App Website: https://github.com/alexhallam/tv

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tidy-viewer with the following command
   ```
   brew install tidy-viewer
   ```
4. tidy-viewer is ready to use now!