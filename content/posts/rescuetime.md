---
title: "Install RescueTime on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Time optimising application"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install RescueTime on MacOS using homebrew

- App Name: RescueTime
- App description: Time optimising application
- App Version: 2.16.6.1
- App Website: https://www.rescuetime.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install RescueTime with the following command
   ```
   brew install --cask rescuetime
   ```
4. RescueTime is ready to use now!