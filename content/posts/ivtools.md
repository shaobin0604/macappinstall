---
title: "Install ivtools on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "X11 vector graphic servers"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ivtools on MacOS using homebrew

- App Name: ivtools
- App description: X11 vector graphic servers
- App Version: 2.0.11d
- App Website: https://github.com/vectaport/ivtools

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ivtools with the following command
   ```
   brew install ivtools
   ```
4. ivtools is ready to use now!