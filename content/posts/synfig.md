---
title: "Install synfig on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line renderer"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install synfig on MacOS using homebrew

- App Name: synfig
- App description: Command-line renderer
- App Version: 1.4.2
- App Website: https://synfig.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install synfig with the following command
   ```
   brew install synfig
   ```
4. synfig is ready to use now!