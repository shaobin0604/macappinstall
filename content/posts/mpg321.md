---
title: "Install mpg321 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line MP3 player"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mpg321 on MacOS using homebrew

- App Name: mpg321
- App description: Command-line MP3 player
- App Version: 0.3.2
- App Website: https://mpg321.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mpg321 with the following command
   ```
   brew install mpg321
   ```
4. mpg321 is ready to use now!