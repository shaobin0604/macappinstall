---
title: "Install Ansible DK on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Omnibus-based toolkit for working on Ansible-based infrastructure code"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Ansible DK on MacOS using homebrew

- App Name: Ansible DK
- App description: Omnibus-based toolkit for working on Ansible-based infrastructure code
- App Version: 1.2.0,3
- App Website: https://github.com/omniti-labs/ansible-dk

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Ansible DK with the following command
   ```
   brew install --cask ansible-dk
   ```
4. Ansible DK is ready to use now!