---
title: "Install kind on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Run local Kubernetes cluster in Docker"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install kind on MacOS using homebrew

- App Name: kind
- App description: Run local Kubernetes cluster in Docker
- App Version: 0.11.1
- App Website: https://kind.sigs.k8s.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install kind with the following command
   ```
   brew install kind
   ```
4. kind is ready to use now!