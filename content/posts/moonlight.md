---
title: "Install Moonlight on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GameStream client"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Moonlight on MacOS using homebrew

- App Name: Moonlight
- App description: GameStream client
- App Version: 3.2.0
- App Website: https://moonlight-stream.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Moonlight with the following command
   ```
   brew install --cask moonlight
   ```
4. Moonlight is ready to use now!