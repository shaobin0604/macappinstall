---
title: "Install valgrind on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Dynamic analysis tools (memory, debug, profiling)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install valgrind on MacOS using homebrew

- App Name: valgrind
- App description: Dynamic analysis tools (memory, debug, profiling)
- App Version: 3.18.1
- App Website: https://www.valgrind.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install valgrind with the following command
   ```
   brew install valgrind
   ```
4. valgrind is ready to use now!