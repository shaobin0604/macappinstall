---
title: "Install stress on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to impose load on and stress test a computer system"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install stress on MacOS using homebrew

- App Name: stress
- App description: Tool to impose load on and stress test a computer system
- App Version: 1.0.5
- App Website: https://web.archive.org/web/20190702093856/https://people.seas.harvard.edu/~apw/stress/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install stress with the following command
   ```
   brew install stress
   ```
4. stress is ready to use now!