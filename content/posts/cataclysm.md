---
title: "Install cataclysm on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Fork/variant of Cataclysm Roguelike"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cataclysm on MacOS using homebrew

- App Name: cataclysm
- App description: Fork/variant of Cataclysm Roguelike
- App Version: 0.F-3
- App Website: https://github.com/CleverRaven/Cataclysm-DDA

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cataclysm with the following command
   ```
   brew install cataclysm
   ```
4. cataclysm is ready to use now!