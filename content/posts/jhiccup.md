---
title: "Install jhiccup on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Measure pauses and stalls of an app's Java runtime platform"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install jhiccup on MacOS using homebrew

- App Name: jhiccup
- App description: Measure pauses and stalls of an app's Java runtime platform
- App Version: 2.0.10
- App Website: https://www.azul.com/products/components/jhiccup/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install jhiccup with the following command
   ```
   brew install jhiccup
   ```
4. jhiccup is ready to use now!