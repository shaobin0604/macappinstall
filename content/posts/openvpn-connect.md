---
title: "Install OpenVPN Connect client on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Client program for the OpenVPN Access Server"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install OpenVPN Connect client on MacOS using homebrew

- App Name: OpenVPN Connect client
- App description: Client program for the OpenVPN Access Server
- App Version: 3.3.3,4163
- App Website: https://openvpn.net/client-connect-vpn-for-mac-os/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install OpenVPN Connect client with the following command
   ```
   brew install --cask openvpn-connect
   ```
4. OpenVPN Connect client is ready to use now!