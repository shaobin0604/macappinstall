---
title: "Install docutils on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Text processing system for reStructuredText"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install docutils on MacOS using homebrew

- App Name: docutils
- App description: Text processing system for reStructuredText
- App Version: 0.18.1
- App Website: https://docutils.sourceforge.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install docutils with the following command
   ```
   brew install docutils
   ```
4. docutils is ready to use now!