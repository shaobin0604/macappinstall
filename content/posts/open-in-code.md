---
title: "Install OpenInCode on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Finder toolbar app to open current folder in Visual Studio Code"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install OpenInCode on MacOS using homebrew

- App Name: OpenInCode
- App description: Finder toolbar app to open current folder in Visual Studio Code
- App Version: 1.0
- App Website: https://github.com/sozercan/OpenInCode

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install OpenInCode with the following command
   ```
   brew install --cask open-in-code
   ```
4. OpenInCode is ready to use now!