---
title: "Install dependency-check on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "OWASP dependency-check"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dependency-check on MacOS using homebrew

- App Name: dependency-check
- App description: OWASP dependency-check
- App Version: 6.5.3
- App Website: https://owasp.org/www-project-dependency-check/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dependency-check with the following command
   ```
   brew install dependency-check
   ```
4. dependency-check is ready to use now!