---
title: "Install server-go on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Server for OpenIoTHub"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install server-go on MacOS using homebrew

- App Name: server-go
- App description: Server for OpenIoTHub
- App Version: 1.1.77
- App Website: https://github.com/OpenIoTHub/server-go

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install server-go with the following command
   ```
   brew install server-go
   ```
4. server-go is ready to use now!