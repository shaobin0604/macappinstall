---
title: "Install git-interactive-rebase-tool on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Native sequence editor for Git interactive rebase"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install git-interactive-rebase-tool on MacOS using homebrew

- App Name: git-interactive-rebase-tool
- App description: Native sequence editor for Git interactive rebase
- App Version: 2.1.0
- App Website: https://gitrebasetool.mitmaro.ca/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install git-interactive-rebase-tool with the following command
   ```
   brew install git-interactive-rebase-tool
   ```
4. git-interactive-rebase-tool is ready to use now!