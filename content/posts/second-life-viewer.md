---
title: "Install Linden Lab Second Life Viewer on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "3D browsing software for Second Life online virtual world"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Linden Lab Second Life Viewer on MacOS using homebrew

- App Name: Linden Lab Second Life Viewer
- App description: 3D browsing software for Second Life online virtual world
- App Version: 6.5.2.567427
- App Website: https://secondlife.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Linden Lab Second Life Viewer with the following command
   ```
   brew install --cask second-life-viewer
   ```
4. Linden Lab Second Life Viewer is ready to use now!