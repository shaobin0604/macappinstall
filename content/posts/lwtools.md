---
title: "Install lwtools on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cross-development tools for Motorola 6809 and Hitachi 6309"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install lwtools on MacOS using homebrew

- App Name: lwtools
- App description: Cross-development tools for Motorola 6809 and Hitachi 6309
- App Version: 4.19
- App Website: http://www.lwtools.ca/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install lwtools with the following command
   ```
   brew install lwtools
   ```
4. lwtools is ready to use now!