---
title: "Install yq on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Process YAML documents from the CLI"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install yq on MacOS using homebrew

- App Name: yq
- App description: Process YAML documents from the CLI
- App Version: 4.20.1
- App Website: https://github.com/mikefarah/yq

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install yq with the following command
   ```
   brew install yq
   ```
4. yq is ready to use now!