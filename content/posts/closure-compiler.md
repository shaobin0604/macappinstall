---
title: "Install closure-compiler on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "JavaScript optimizing compiler"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install closure-compiler on MacOS using homebrew

- App Name: closure-compiler
- App description: JavaScript optimizing compiler
- App Version: 20220202
- App Website: https://developers.google.com/closure/compiler

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install closure-compiler with the following command
   ```
   brew install closure-compiler
   ```
4. closure-compiler is ready to use now!