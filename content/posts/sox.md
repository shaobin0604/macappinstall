---
title: "Install sox on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "SOund eXchange: universal sound sample translator"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sox on MacOS using homebrew

- App Name: sox
- App description: SOund eXchange: universal sound sample translator
- App Version: 14.4.2
- App Website: https://sox.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sox with the following command
   ```
   brew install sox
   ```
4. sox is ready to use now!