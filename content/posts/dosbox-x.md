---
title: "Install dosbox-x on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "DOSBox with accurate emulation and wide testing"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dosbox-x on MacOS using homebrew

- App Name: dosbox-x
- App description: DOSBox with accurate emulation and wide testing
- App Version: 0.83.22
- App Website: https://dosbox-x.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dosbox-x with the following command
   ```
   brew install dosbox-x
   ```
4. dosbox-x is ready to use now!