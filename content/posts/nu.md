---
title: "Install nu on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Object-oriented, Lisp-like programming language"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install nu on MacOS using homebrew

- App Name: nu
- App description: Object-oriented, Lisp-like programming language
- App Version: 2.3.0
- App Website: https://programming.nu/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install nu with the following command
   ```
   brew install nu
   ```
4. nu is ready to use now!