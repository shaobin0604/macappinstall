---
title: "Install Telegram for macOS on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Messaging app with a focus on speed and security"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Telegram for macOS on MacOS using homebrew

- App Name: Telegram for macOS
- App description: Messaging app with a focus on speed and security
- App Version: 8.5,226936
- App Website: https://macos.telegram.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Telegram for macOS with the following command
   ```
   brew install --cask telegram
   ```
4. Telegram for macOS is ready to use now!