---
title: "Install lft on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Layer Four Traceroute (LFT), an advanced traceroute tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install lft on MacOS using homebrew

- App Name: lft
- App description: Layer Four Traceroute (LFT), an advanced traceroute tool
- App Version: 3.91
- App Website: https://pwhois.org/lft/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install lft with the following command
   ```
   brew install lft
   ```
4. lft is ready to use now!