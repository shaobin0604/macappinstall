---
title: "Install openssl@3 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cryptography and SSL/TLS Toolkit"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install openssl@3 on MacOS using homebrew

- App Name: openssl@3
- App description: Cryptography and SSL/TLS Toolkit
- App Version: 3.0.1
- App Website: https://openssl.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install openssl@3 with the following command
   ```
   brew install openssl@3
   ```
4. openssl@3 is ready to use now!