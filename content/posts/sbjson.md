---
title: "Install sbjson on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "JSON CLI parser & reformatter based on SBJson v5"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sbjson on MacOS using homebrew

- App Name: sbjson
- App description: JSON CLI parser & reformatter based on SBJson v5
- App Version: 5.0.3
- App Website: https://github.com/stig/json-framework

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sbjson with the following command
   ```
   brew install sbjson
   ```
4. sbjson is ready to use now!