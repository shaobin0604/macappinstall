---
title: "Install Oracle VirtualBox Extension Pack on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Extend the functionality of VirtualBox"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Oracle VirtualBox Extension Pack on MacOS using homebrew

- App Name: Oracle VirtualBox Extension Pack
- App description: Extend the functionality of VirtualBox
- App Version: 6.1.32
- App Website: https://www.virtualbox.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Oracle VirtualBox Extension Pack with the following command
   ```
   brew install --cask virtualbox-extension-pack
   ```
4. Oracle VirtualBox Extension Pack is ready to use now!