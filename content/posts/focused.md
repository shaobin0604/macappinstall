---
title: "Install Focused on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Markdown writing app"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Focused on MacOS using homebrew

- App Name: Focused
- App description: Markdown writing app
- App Version: 3.2,1825
- App Website: https://codebots.co.uk/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Focused with the following command
   ```
   brew install --cask focused
   ```
4. Focused is ready to use now!