---
title: "Install kotlin on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Statically typed programming language for the JVM"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install kotlin on MacOS using homebrew

- App Name: kotlin
- App description: Statically typed programming language for the JVM
- App Version: 1.6.10
- App Website: https://kotlinlang.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install kotlin with the following command
   ```
   brew install kotlin
   ```
4. kotlin is ready to use now!