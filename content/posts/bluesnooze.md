---
title: "Install Bluesnooze on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Prevents your sleeping computer from connecting to Bluetooth accessories"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Bluesnooze on MacOS using homebrew

- App Name: Bluesnooze
- App description: Prevents your sleeping computer from connecting to Bluetooth accessories
- App Version: 1.1
- App Website: https://github.com/odlp/bluesnooze

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Bluesnooze with the following command
   ```
   brew install --cask bluesnooze
   ```
4. Bluesnooze is ready to use now!