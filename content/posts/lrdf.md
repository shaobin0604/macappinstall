---
title: "Install lrdf on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "RDF library for accessing plugin metadata in the LADSPA plugin system"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install lrdf on MacOS using homebrew

- App Name: lrdf
- App description: RDF library for accessing plugin metadata in the LADSPA plugin system
- App Version: 0.6.1
- App Website: https://github.com/swh/LRDF

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install lrdf with the following command
   ```
   brew install lrdf
   ```
4. lrdf is ready to use now!