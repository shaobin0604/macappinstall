---
title: "Install greed on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Game of consumption"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install greed on MacOS using homebrew

- App Name: greed
- App description: Game of consumption
- App Version: 4.2
- App Website: http://www.catb.org/~esr/greed/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install greed with the following command
   ```
   brew install greed
   ```
4. greed is ready to use now!