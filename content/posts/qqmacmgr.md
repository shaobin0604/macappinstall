---
title: "Install 腾讯电脑管家 on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install 腾讯电脑管家 on MacOS using homebrew

- App Name: 腾讯电脑管家
- App description: null
- App Version: 2.4.16
- App Website: https://lemon.qq.com/index_o.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install 腾讯电脑管家 with the following command
   ```
   brew install --cask qqmacmgr
   ```
4. 腾讯电脑管家 is ready to use now!