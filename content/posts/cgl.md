---
title: "Install cgl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cut Generation Library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cgl on MacOS using homebrew

- App Name: cgl
- App description: Cut Generation Library
- App Version: 0.60.5
- App Website: https://github.com/coin-or/Cgl

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cgl with the following command
   ```
   brew install cgl
   ```
4. cgl is ready to use now!