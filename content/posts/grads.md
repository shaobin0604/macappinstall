---
title: "Install Grid Analysis and Display System on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Access, manipulate, and visualize earth science data"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Grid Analysis and Display System on MacOS using homebrew

- App Name: Grid Analysis and Display System
- App description: Access, manipulate, and visualize earth science data
- App Version: 2.2.1
- App Website: http://cola.gmu.edu/grads/grads.php

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Grid Analysis and Display System with the following command
   ```
   brew install --cask grads
   ```
4. Grid Analysis and Display System is ready to use now!