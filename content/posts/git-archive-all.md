---
title: "Install git-archive-all on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Archive a project and its submodules"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install git-archive-all on MacOS using homebrew

- App Name: git-archive-all
- App description: Archive a project and its submodules
- App Version: 1.23.0
- App Website: https://github.com/Kentzo/git-archive-all

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install git-archive-all with the following command
   ```
   brew install git-archive-all
   ```
4. git-archive-all is ready to use now!