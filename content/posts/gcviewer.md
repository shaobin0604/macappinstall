---
title: "Install gcviewer on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Java garbage collection visualization tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gcviewer on MacOS using homebrew

- App Name: gcviewer
- App description: Java garbage collection visualization tool
- App Version: 1.36
- App Website: https://github.com/chewiebug/GCViewer

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gcviewer with the following command
   ```
   brew install gcviewer
   ```
4. gcviewer is ready to use now!