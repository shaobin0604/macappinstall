---
title: "Install mmsrip on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Client for the MMS:// protocol"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mmsrip on MacOS using homebrew

- App Name: mmsrip
- App description: Client for the MMS:// protocol
- App Version: 0.7.0
- App Website: https://nbenoit.tuxfamily.org/index.php?page=MMSRIP

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mmsrip with the following command
   ```
   brew install mmsrip
   ```
4. mmsrip is ready to use now!