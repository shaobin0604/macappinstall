---
title: "Install smug on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Automate your tmux workflow"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install smug on MacOS using homebrew

- App Name: smug
- App description: Automate your tmux workflow
- App Version: 0.2.7
- App Website: https://github.com/ivaaaan/smug

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install smug with the following command
   ```
   brew install smug
   ```
4. smug is ready to use now!