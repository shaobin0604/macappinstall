---
title: "Install dotnet on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: ".NET Core"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dotnet on MacOS using homebrew

- App Name: dotnet
- App description: .NET Core
- App Version: 6.0.100
- App Website: https://dotnet.microsoft.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dotnet with the following command
   ```
   brew install dotnet
   ```
4. dotnet is ready to use now!