---
title: "Install sn0int on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Semi-automatic OSINT framework and package manager"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sn0int on MacOS using homebrew

- App Name: sn0int
- App description: Semi-automatic OSINT framework and package manager
- App Version: 0.24.1
- App Website: https://github.com/kpcyrd/sn0int

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sn0int with the following command
   ```
   brew install sn0int
   ```
4. sn0int is ready to use now!