---
title: "Install Splice on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cloud-based music creation and collaboration platform"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Splice on MacOS using homebrew

- App Name: Splice
- App description: Cloud-based music creation and collaboration platform
- App Version: 3.6.5,20201015.a23ba6253
- App Website: https://splice.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Splice with the following command
   ```
   brew install --cask splice
   ```
4. Splice is ready to use now!