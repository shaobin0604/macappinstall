---
title: "Install gotags on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tag generator for Go, compatible with ctags"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gotags on MacOS using homebrew

- App Name: gotags
- App description: Tag generator for Go, compatible with ctags
- App Version: 1.4.1
- App Website: https://github.com/jstemmer/gotags

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gotags with the following command
   ```
   brew install gotags
   ```
4. gotags is ready to use now!