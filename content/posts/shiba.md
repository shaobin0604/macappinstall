---
title: "Install Shiba on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Rich markdown live preview app with linter"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Shiba on MacOS using homebrew

- App Name: Shiba
- App description: Rich markdown live preview app with linter
- App Version: 1.2.1
- App Website: https://github.com/rhysd/Shiba/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Shiba with the following command
   ```
   brew install --cask shiba
   ```
4. Shiba is ready to use now!