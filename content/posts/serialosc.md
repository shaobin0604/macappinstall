---
title: "Install serialosc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Opensound control server for monome devices"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install serialosc on MacOS using homebrew

- App Name: serialosc
- App description: Opensound control server for monome devices
- App Version: 1.4.3
- App Website: https://github.com/monome/docs/blob/gh-pages/serialosc/osc.md

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install serialosc with the following command
   ```
   brew install serialosc
   ```
4. serialosc is ready to use now!