---
title: "Install Cocktail on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cleans, repairs and optimizes computer systems"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Cocktail on MacOS using homebrew

- App Name: Cocktail
- App description: Cleans, repairs and optimizes computer systems
- App Version: 14.4.1
- App Website: https://www.maintain.se/cocktail/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Cocktail with the following command
   ```
   brew install --cask cocktail
   ```
4. Cocktail is ready to use now!