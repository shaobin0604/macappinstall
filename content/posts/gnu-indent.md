---
title: "Install gnu-indent on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C code prettifier"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gnu-indent on MacOS using homebrew

- App Name: gnu-indent
- App description: C code prettifier
- App Version: 2.2.12
- App Website: https://www.gnu.org/software/indent/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gnu-indent with the following command
   ```
   brew install gnu-indent
   ```
4. gnu-indent is ready to use now!