---
title: "Install mask on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CLI task runner defined by a simple markdown file"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mask on MacOS using homebrew

- App Name: mask
- App description: CLI task runner defined by a simple markdown file
- App Version: 0.11.1
- App Website: https://github.com/jakedeichert/mask/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mask with the following command
   ```
   brew install mask
   ```
4. mask is ready to use now!