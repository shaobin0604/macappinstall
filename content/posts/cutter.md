---
title: "Install cutter on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Unit Testing Framework for C and C++"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cutter on MacOS using homebrew

- App Name: cutter
- App description: Unit Testing Framework for C and C++
- App Version: 1.2.8
- App Website: https://cutter.osdn.jp/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cutter with the following command
   ```
   brew install cutter
   ```
4. cutter is ready to use now!