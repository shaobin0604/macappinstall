---
title: "Install EurKEY keyboard layout on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Keyboard Layout for Europeans, Coders and Translators"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install EurKEY keyboard layout on MacOS using homebrew

- App Name: EurKEY keyboard layout
- App description: Keyboard Layout for Europeans, Coders and Translators
- App Version: latest
- App Website: https://eurkey.steffen.bruentjen.eu/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install EurKEY keyboard layout with the following command
   ```
   brew install --cask eurkey
   ```
4. EurKEY keyboard layout is ready to use now!