---
title: "Install advancescan on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Rom manager for AdvanceMAME/MESS"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install advancescan on MacOS using homebrew

- App Name: advancescan
- App description: Rom manager for AdvanceMAME/MESS
- App Version: 1.18
- App Website: https://www.advancemame.it/scan-readme.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install advancescan with the following command
   ```
   brew install advancescan
   ```
4. advancescan is ready to use now!