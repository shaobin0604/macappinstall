---
title: "Install PokerTH on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Free Texas hold'em poker"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install PokerTH on MacOS using homebrew

- App Name: PokerTH
- App description: Free Texas hold'em poker
- App Version: 1.1.2
- App Website: https://www.pokerth.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install PokerTH with the following command
   ```
   brew install --cask pokerth
   ```
4. PokerTH is ready to use now!