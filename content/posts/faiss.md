---
title: "Install faiss on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Efficient similarity search and clustering of dense vectors"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install faiss on MacOS using homebrew

- App Name: faiss
- App description: Efficient similarity search and clustering of dense vectors
- App Version: 1.7.2
- App Website: https://github.com/facebookresearch/faiss

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install faiss with the following command
   ```
   brew install faiss
   ```
4. faiss is ready to use now!