---
title: "Install arp-scan on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "ARP scanning and fingerprinting tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install arp-scan on MacOS using homebrew

- App Name: arp-scan
- App description: ARP scanning and fingerprinting tool
- App Version: 1.9.7
- App Website: https://github.com/royhills/arp-scan

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install arp-scan with the following command
   ```
   brew install arp-scan
   ```
4. arp-scan is ready to use now!