---
title: "Install mmv on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Move, copy, append, and link multiple files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mmv on MacOS using homebrew

- App Name: mmv
- App description: Move, copy, append, and link multiple files
- App Version: 1.01b
- App Website: https://packages.debian.org/unstable/utils/mmv

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mmv with the following command
   ```
   brew install mmv
   ```
4. mmv is ready to use now!