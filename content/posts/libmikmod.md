---
title: "Install libmikmod on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Portable sound library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libmikmod on MacOS using homebrew

- App Name: libmikmod
- App description: Portable sound library
- App Version: 3.3.11.1
- App Website: https://mikmod.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libmikmod with the following command
   ```
   brew install libmikmod
   ```
4. libmikmod is ready to use now!