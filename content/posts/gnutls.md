---
title: "Install gnutls on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GNU Transport Layer Security (TLS) Library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gnutls on MacOS using homebrew

- App Name: gnutls
- App description: GNU Transport Layer Security (TLS) Library
- App Version: 3.7.3
- App Website: https://gnutls.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gnutls with the following command
   ```
   brew install gnutls
   ```
4. gnutls is ready to use now!