---
title: "Install antigen on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Plugin manager for zsh, inspired by oh-my-zsh and vundle"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install antigen on MacOS using homebrew

- App Name: antigen
- App description: Plugin manager for zsh, inspired by oh-my-zsh and vundle
- App Version: 2.2.3
- App Website: https://antigen.sharats.me/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install antigen with the following command
   ```
   brew install antigen
   ```
4. antigen is ready to use now!