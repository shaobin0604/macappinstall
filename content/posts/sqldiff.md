---
title: "Install sqldiff on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Displays the differences between SQLite databases"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sqldiff on MacOS using homebrew

- App Name: sqldiff
- App description: Displays the differences between SQLite databases
- App Version: 3.37.2
- App Website: https://www.sqlite.org/sqldiff.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sqldiff with the following command
   ```
   brew install sqldiff
   ```
4. sqldiff is ready to use now!