---
title: "Install icdiff on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Improved colored diff"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install icdiff on MacOS using homebrew

- App Name: icdiff
- App description: Improved colored diff
- App Version: 2.0.4
- App Website: https://github.com/jeffkaufman/icdiff

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install icdiff with the following command
   ```
   brew install icdiff
   ```
4. icdiff is ready to use now!