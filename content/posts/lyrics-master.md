---
title: "Install Lyrics Master on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Find and download lyrics"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Lyrics Master on MacOS using homebrew

- App Name: Lyrics Master
- App description: Find and download lyrics
- App Version: 2.5.4.0
- App Website: http://www.kenichimaehashi.com/lyricsmaster/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Lyrics Master with the following command
   ```
   brew install --cask lyrics-master
   ```
4. Lyrics Master is ready to use now!