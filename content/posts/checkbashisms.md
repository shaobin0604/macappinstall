---
title: "Install checkbashisms on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Checks for bashisms in shell scripts"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install checkbashisms on MacOS using homebrew

- App Name: checkbashisms
- App description: Checks for bashisms in shell scripts
- App Version: 2.22.1
- App Website: https://launchpad.net/ubuntu/+source/devscripts/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install checkbashisms with the following command
   ```
   brew install checkbashisms
   ```
4. checkbashisms is ready to use now!