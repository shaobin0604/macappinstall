---
title: "Install Flacon on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Open source audio file encoder"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Flacon on MacOS using homebrew

- App Name: Flacon
- App description: Open source audio file encoder
- App Version: 8.3.0
- App Website: https://flacon.github.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Flacon with the following command
   ```
   brew install --cask flacon
   ```
4. Flacon is ready to use now!