---
title: "Install bareos-client on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Client for Bareos (Backup Archiving REcovery Open Sourced)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bareos-client on MacOS using homebrew

- App Name: bareos-client
- App description: Client for Bareos (Backup Archiving REcovery Open Sourced)
- App Version: 19.2.9
- App Website: https://www.bareos.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bareos-client with the following command
   ```
   brew install bareos-client
   ```
4. bareos-client is ready to use now!