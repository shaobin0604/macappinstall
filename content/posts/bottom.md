---
title: "Install bottom on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Yet another cross-platform graphical process/system monitor"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bottom on MacOS using homebrew

- App Name: bottom
- App description: Yet another cross-platform graphical process/system monitor
- App Version: 0.6.8
- App Website: https://clementtsang.github.io/bottom/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bottom with the following command
   ```
   brew install bottom
   ```
4. bottom is ready to use now!