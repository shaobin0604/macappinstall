---
title: "Install libmxml on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Mini-XML library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libmxml on MacOS using homebrew

- App Name: libmxml
- App description: Mini-XML library
- App Version: 3.3
- App Website: https://michaelrsweet.github.io/mxml/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libmxml with the following command
   ```
   brew install libmxml
   ```
4. libmxml is ready to use now!