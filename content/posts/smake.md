---
title: "Install smake on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Portable make program with automake features"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install smake on MacOS using homebrew

- App Name: smake
- App description: Portable make program with automake features
- App Version: 1.2.5
- App Website: https://s-make.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install smake with the following command
   ```
   brew install smake
   ```
4. smake is ready to use now!