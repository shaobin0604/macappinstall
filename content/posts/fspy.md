---
title: "Install fSpy on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Still image camera matching"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fSpy on MacOS using homebrew

- App Name: fSpy
- App description: Still image camera matching
- App Version: 1.0.3
- App Website: https://fspy.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fSpy with the following command
   ```
   brew install --cask fspy
   ```
4. fSpy is ready to use now!