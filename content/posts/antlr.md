---
title: "Install antlr on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "ANother Tool for Language Recognition"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install antlr on MacOS using homebrew

- App Name: antlr
- App description: ANother Tool for Language Recognition
- App Version: 4.9.3
- App Website: https://www.antlr.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install antlr with the following command
   ```
   brew install antlr
   ```
4. antlr is ready to use now!