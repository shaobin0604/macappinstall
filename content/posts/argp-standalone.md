---
title: "Install argp-standalone on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Standalone version of arguments parsing functions from GLIBC"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install argp-standalone on MacOS using homebrew

- App Name: argp-standalone
- App description: Standalone version of arguments parsing functions from GLIBC
- App Version: 1.3
- App Website: https://www.lysator.liu.se/~nisse/misc/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install argp-standalone with the following command
   ```
   brew install argp-standalone
   ```
4. argp-standalone is ready to use now!