---
title: "Install docker on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Pack, ship and run any application as a lightweight container"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install docker on MacOS using homebrew

- App Name: docker
- App description: Pack, ship and run any application as a lightweight container
- App Version: 20.10.12
- App Website: https://www.docker.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install docker with the following command
   ```
   brew install docker
   ```
4. docker is ready to use now!