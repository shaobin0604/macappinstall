---
title: "Install Send to Kindle on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool for sending personal documents to Kindles from Macs"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Send to Kindle on MacOS using homebrew

- App Name: Send to Kindle
- App description: Tool for sending personal documents to Kindles from Macs
- App Version: 1.1.1.253
- App Website: https://www.amazon.com/gp/sendtokindle/mac

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Send to Kindle with the following command
   ```
   brew install --cask send-to-kindle
   ```
4. Send to Kindle is ready to use now!