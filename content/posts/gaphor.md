---
title: "Install Gaphor on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "UML/SysML modeling tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Gaphor on MacOS using homebrew

- App Name: Gaphor
- App description: UML/SysML modeling tool
- App Version: 2.8.2
- App Website: https://gaphor.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Gaphor with the following command
   ```
   brew install --cask gaphor
   ```
4. Gaphor is ready to use now!