---
title: "Install cri-tools on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CLI and validation tools for Kubelet Container Runtime Interface (CRI)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cri-tools on MacOS using homebrew

- App Name: cri-tools
- App description: CLI and validation tools for Kubelet Container Runtime Interface (CRI)
- App Version: 1.23.0
- App Website: https://github.com/kubernetes-sigs/cri-tools

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cri-tools with the following command
   ```
   brew install cri-tools
   ```
4. cri-tools is ready to use now!