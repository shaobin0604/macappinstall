---
title: "Install midnight-commander on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Terminal-based visual file manager"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install midnight-commander on MacOS using homebrew

- App Name: midnight-commander
- App description: Terminal-based visual file manager
- App Version: 4.8.27
- App Website: https://www.midnight-commander.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install midnight-commander with the following command
   ```
   brew install midnight-commander
   ```
4. midnight-commander is ready to use now!