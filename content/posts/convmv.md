---
title: "Install convmv on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Filename encoding conversion tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install convmv on MacOS using homebrew

- App Name: convmv
- App description: Filename encoding conversion tool
- App Version: 2.05
- App Website: https://www.j3e.de/linux/convmv/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install convmv with the following command
   ```
   brew install convmv
   ```
4. convmv is ready to use now!