---
title: "Install ntfs-3g on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Read-write NTFS driver for FUSE"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ntfs-3g on MacOS using homebrew

- App Name: ntfs-3g
- App description: Read-write NTFS driver for FUSE
- App Version: 2021.8.22
- App Website: https://www.tuxera.com/community/open-source-ntfs-3g/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ntfs-3g with the following command
   ```
   brew install ntfs-3g
   ```
4. ntfs-3g is ready to use now!