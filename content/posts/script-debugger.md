---
title: "Install Script Debugger on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Integrated development environment focused entirely on AppleScript"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Script Debugger on MacOS using homebrew

- App Name: Script Debugger
- App description: Integrated development environment focused entirely on AppleScript
- App Version: 8.0.3-8A49
- App Website: https://latenightsw.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Script Debugger with the following command
   ```
   brew install --cask script-debugger
   ```
4. Script Debugger is ready to use now!