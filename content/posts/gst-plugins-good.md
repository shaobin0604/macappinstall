---
title: "Install gst-plugins-good on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GStreamer plugins (well-supported, under the LGPL)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gst-plugins-good on MacOS using homebrew

- App Name: gst-plugins-good
- App description: GStreamer plugins (well-supported, under the LGPL)
- App Version: 1.18.5
- App Website: https://gstreamer.freedesktop.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gst-plugins-good with the following command
   ```
   brew install gst-plugins-good
   ```
4. gst-plugins-good is ready to use now!