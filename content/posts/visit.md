---
title: "Install VisIt on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Visualization and data analysis for mesh-based scientific data"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install VisIt on MacOS using homebrew

- App Name: VisIt
- App description: Visualization and data analysis for mesh-based scientific data
- App Version: 3.2.1
- App Website: https://wci.llnl.gov/simulation/computer-codes/visit

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install VisIt with the following command
   ```
   brew install --cask visit
   ```
4. VisIt is ready to use now!