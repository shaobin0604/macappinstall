---
title: "Install Loopback on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cable-free audio router"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Loopback on MacOS using homebrew

- App Name: Loopback
- App description: Cable-free audio router
- App Version: 2.2.7
- App Website: https://rogueamoeba.com/loopback/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Loopback with the following command
   ```
   brew install --cask loopback
   ```
4. Loopback is ready to use now!