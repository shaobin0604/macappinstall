---
title: "Install easy-tag on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Application for viewing and editing audio file tags"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install easy-tag on MacOS using homebrew

- App Name: easy-tag
- App description: Application for viewing and editing audio file tags
- App Version: 2.4.3
- App Website: https://projects.gnome.org/easytag

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install easy-tag with the following command
   ```
   brew install easy-tag
   ```
4. easy-tag is ready to use now!