---
title: "Install Disk Inventory X on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Disk usage utility"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Disk Inventory X on MacOS using homebrew

- App Name: Disk Inventory X
- App description: Disk usage utility
- App Version: 1.3
- App Website: http://www.derlien.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Disk Inventory X with the following command
   ```
   brew install --cask disk-inventory-x
   ```
4. Disk Inventory X is ready to use now!