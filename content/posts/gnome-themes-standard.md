---
title: "Install gnome-themes-standard on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Default themes for the GNOME desktop environment"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gnome-themes-standard on MacOS using homebrew

- App Name: gnome-themes-standard
- App description: Default themes for the GNOME desktop environment
- App Version: 3.22.3
- App Website: https://gitlab.gnome.org/GNOME/gnome-themes-extra

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gnome-themes-standard with the following command
   ```
   brew install gnome-themes-standard
   ```
4. gnome-themes-standard is ready to use now!