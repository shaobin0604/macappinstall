---
title: "Install Mouseposé on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Highlight your mouse pointer and cursor position"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Mouseposé on MacOS using homebrew

- App Name: Mouseposé
- App description: Highlight your mouse pointer and cursor position
- App Version: 4.2,10276
- App Website: https://boinx.com/mousepose/overview/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Mouseposé with the following command
   ```
   brew install --cask mousepose
   ```
4. Mouseposé is ready to use now!