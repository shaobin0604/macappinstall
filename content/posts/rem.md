---
title: "Install rem on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line tool to access OSX Reminders.app database"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install rem on MacOS using homebrew

- App Name: rem
- App description: Command-line tool to access OSX Reminders.app database
- App Version: 20150618
- App Website: https://github.com/kykim/rem

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install rem with the following command
   ```
   brew install rem
   ```
4. rem is ready to use now!