---
title: "Install Bespoke Synth on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Software modular synth"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Bespoke Synth on MacOS using homebrew

- App Name: Bespoke Synth
- App description: Software modular synth
- App Version: 1.1.0
- App Website: https://www.bespokesynth.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Bespoke Synth with the following command
   ```
   brew install --cask bespoke
   ```
4. Bespoke Synth is ready to use now!