---
title: "Install amtterm on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Serial-over-LAN (sol) client for Intel AMT"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install amtterm on MacOS using homebrew

- App Name: amtterm
- App description: Serial-over-LAN (sol) client for Intel AMT
- App Version: 1.6
- App Website: https://www.kraxel.org/blog/linux/amtterm/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install amtterm with the following command
   ```
   brew install amtterm
   ```
4. amtterm is ready to use now!