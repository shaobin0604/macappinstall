---
title: "Install xerces-c on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Validating XML parser"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install xerces-c on MacOS using homebrew

- App Name: xerces-c
- App description: Validating XML parser
- App Version: 3.2.3
- App Website: https://xerces.apache.org/xerces-c/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install xerces-c with the following command
   ```
   brew install xerces-c
   ```
4. xerces-c is ready to use now!