---
title: "Install dfu-util on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "USB programmer"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dfu-util on MacOS using homebrew

- App Name: dfu-util
- App description: USB programmer
- App Version: 0.11
- App Website: https://dfu-util.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dfu-util with the following command
   ```
   brew install dfu-util
   ```
4. dfu-util is ready to use now!