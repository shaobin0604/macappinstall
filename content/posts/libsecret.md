---
title: "Install libsecret on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library for storing/retrieving passwords and other secrets"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libsecret on MacOS using homebrew

- App Name: libsecret
- App description: Library for storing/retrieving passwords and other secrets
- App Version: 0.20.4
- App Website: https://wiki.gnome.org/Projects/Libsecret

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libsecret with the following command
   ```
   brew install libsecret
   ```
4. libsecret is ready to use now!