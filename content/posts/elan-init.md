---
title: "Install elan-init on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Lean Theorem Prover installer and version manager"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install elan-init on MacOS using homebrew

- App Name: elan-init
- App description: Lean Theorem Prover installer and version manager
- App Version: 1.3.1
- App Website: https://github.com/leanprover/elan

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install elan-init with the following command
   ```
   brew install elan-init
   ```
4. elan-init is ready to use now!