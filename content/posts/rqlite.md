---
title: "Install rqlite on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Lightweight, distributed relational database built on SQLite"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install rqlite on MacOS using homebrew

- App Name: rqlite
- App description: Lightweight, distributed relational database built on SQLite
- App Version: 7.3.1
- App Website: http://www.rqlite.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install rqlite with the following command
   ```
   brew install rqlite
   ```
4. rqlite is ready to use now!