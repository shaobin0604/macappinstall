---
title: "Install groovysdk on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "SDK for Groovy: a Java-based scripting language"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install groovysdk on MacOS using homebrew

- App Name: groovysdk
- App description: SDK for Groovy: a Java-based scripting language
- App Version: 4.0.0
- App Website: https://www.groovy-lang.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install groovysdk with the following command
   ```
   brew install groovysdk
   ```
4. groovysdk is ready to use now!