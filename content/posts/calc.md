---
title: "Install calc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Arbitrary precision calculator"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install calc on MacOS using homebrew

- App Name: calc
- App description: Arbitrary precision calculator
- App Version: 2.14.0.14
- App Website: http://www.isthe.com/chongo/tech/comp/calc/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install calc with the following command
   ```
   brew install calc
   ```
4. calc is ready to use now!