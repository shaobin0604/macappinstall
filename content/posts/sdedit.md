---
title: "Install sdedit on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool for generating sequence diagrams very quickly"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sdedit on MacOS using homebrew

- App Name: sdedit
- App description: Tool for generating sequence diagrams very quickly
- App Version: 4.2.1
- App Website: https://sdedit.sourceforge.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sdedit with the following command
   ```
   brew install sdedit
   ```
4. sdedit is ready to use now!