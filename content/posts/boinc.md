---
title: "Install Berkeley Open Infrastructure for Network Computing on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Downloads scientific computing jobs and runs them invisibly in the background"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Berkeley Open Infrastructure for Network Computing on MacOS using homebrew

- App Name: Berkeley Open Infrastructure for Network Computing
- App description: Downloads scientific computing jobs and runs them invisibly in the background
- App Version: 7.16.19
- App Website: https://boinc.berkeley.edu/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Berkeley Open Infrastructure for Network Computing with the following command
   ```
   brew install --cask boinc
   ```
4. Berkeley Open Infrastructure for Network Computing is ready to use now!