---
title: "Install dcraw on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Digital camera RAW photo decoding software"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dcraw on MacOS using homebrew

- App Name: dcraw
- App description: Digital camera RAW photo decoding software
- App Version: 9.28.0
- App Website: https://www.dechifro.org/dcraw/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dcraw with the following command
   ```
   brew install dcraw
   ```
4. dcraw is ready to use now!