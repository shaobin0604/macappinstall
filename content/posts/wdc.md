---
title: "Install wdc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "WebDAV Client provides easy and convenient to work with WebDAV-servers"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install wdc on MacOS using homebrew

- App Name: wdc
- App description: WebDAV Client provides easy and convenient to work with WebDAV-servers
- App Version: 1.1.5
- App Website: https://cloudpolis.github.io/webdav-client-cpp

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install wdc with the following command
   ```
   brew install wdc
   ```
4. wdc is ready to use now!