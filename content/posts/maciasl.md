---
title: "Install MaciASL on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "ACPI Machine Language (AML) compiler and IDE"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install MaciASL on MacOS using homebrew

- App Name: MaciASL
- App description: ACPI Machine Language (AML) compiler and IDE
- App Version: 1.6.2
- App Website: https://github.com/acidanthera/MaciASL

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install MaciASL with the following command
   ```
   brew install --cask maciasl
   ```
4. MaciASL is ready to use now!