---
title: "Install Mbed Studio on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "IDE for Mbed OS application and library development"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Mbed Studio on MacOS using homebrew

- App Name: Mbed Studio
- App description: IDE for Mbed OS application and library development
- App Version: 1.4.3
- App Website: https://os.mbed.com/studio/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Mbed Studio with the following command
   ```
   brew install --cask mbed-studio
   ```
4. Mbed Studio is ready to use now!