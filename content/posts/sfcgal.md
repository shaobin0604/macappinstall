---
title: "Install sfcgal on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C++ wrapper library around CGAL"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sfcgal on MacOS using homebrew

- App Name: sfcgal
- App description: C++ wrapper library around CGAL
- App Version: 1.4.1
- App Website: http://sfcgal.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sfcgal with the following command
   ```
   brew install sfcgal
   ```
4. sfcgal is ready to use now!