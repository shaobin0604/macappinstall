---
title: "Install hebcal on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Perpetual Jewish calendar for the command-line"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install hebcal on MacOS using homebrew

- App Name: hebcal
- App description: Perpetual Jewish calendar for the command-line
- App Version: 4.27
- App Website: https://github.com/hebcal/hebcal

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install hebcal with the following command
   ```
   brew install hebcal
   ```
4. hebcal is ready to use now!