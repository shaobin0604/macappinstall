---
title: "Install xcv on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cut, copy and paste files with Bash"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install xcv on MacOS using homebrew

- App Name: xcv
- App description: Cut, copy and paste files with Bash
- App Version: 1.0.1
- App Website: https://github.com/busterc/xcv

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install xcv with the following command
   ```
   brew install xcv
   ```
4. xcv is ready to use now!