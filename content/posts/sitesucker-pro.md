---
title: "Install SiteSucker Pro on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Website downloader tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install SiteSucker Pro on MacOS using homebrew

- App Name: SiteSucker Pro
- App description: Website downloader tool
- App Version: 4.3.3
- App Website: https://ricks-apps.com/osx/sitesucker/index.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install SiteSucker Pro with the following command
   ```
   brew install --cask sitesucker-pro
   ```
4. SiteSucker Pro is ready to use now!