---
title: "Install WatchGuard Mobile VPN with SSL on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install WatchGuard Mobile VPN with SSL on MacOS using homebrew

- App Name: WatchGuard Mobile VPN with SSL
- App description: null
- App Version: 12.5.3,615421
- App Website: https://www.watchguard.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install WatchGuard Mobile VPN with SSL with the following command
   ```
   brew install --cask watchguard-mobile-vpn-with-ssl
   ```
4. WatchGuard Mobile VPN with SSL is ready to use now!