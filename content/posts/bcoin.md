---
title: "Install bcoin on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Javascript bitcoin library for node.js and browsers"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bcoin on MacOS using homebrew

- App Name: bcoin
- App description: Javascript bitcoin library for node.js and browsers
- App Version: 2.2.0
- App Website: https://bcoin.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bcoin with the following command
   ```
   brew install bcoin
   ```
4. bcoin is ready to use now!