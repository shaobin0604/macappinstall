---
title: "Install Recut on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Remove silence from videos and automatically generate a cut list"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Recut on MacOS using homebrew

- App Name: Recut
- App description: Remove silence from videos and automatically generate a cut list
- App Version: 2.0.5,571
- App Website: https://getrecut.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Recut with the following command
   ```
   brew install --cask recut
   ```
4. Recut is ready to use now!