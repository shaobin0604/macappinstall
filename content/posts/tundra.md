---
title: "Install tundra on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Code build system that tries to be fast for incremental builds"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tundra on MacOS using homebrew

- App Name: tundra
- App description: Code build system that tries to be fast for incremental builds
- App Version: 2.16.3
- App Website: https://github.com/deplinenoise/tundra

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tundra with the following command
   ```
   brew install tundra
   ```
4. tundra is ready to use now!