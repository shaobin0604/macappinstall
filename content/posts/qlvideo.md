---
title: "Install QuickLook Video on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install QuickLook Video on MacOS using homebrew

- App Name: QuickLook Video
- App description: null
- App Version: 1.94
- App Website: https://github.com/Marginal/QLVideo

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install QuickLook Video with the following command
   ```
   brew install --cask qlvideo
   ```
4. QuickLook Video is ready to use now!