---
title: "Install msc-generator on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Draws signalling charts from textual description"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install msc-generator on MacOS using homebrew

- App Name: msc-generator
- App description: Draws signalling charts from textual description
- App Version: 7.1
- App Website: https://sourceforge.net/p/msc-generator

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install msc-generator with the following command
   ```
   brew install msc-generator
   ```
4. msc-generator is ready to use now!