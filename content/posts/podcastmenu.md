---
title: "Install PodcastMenu on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to display Overcast on the menu bar"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install PodcastMenu on MacOS using homebrew

- App Name: PodcastMenu
- App description: Tool to display Overcast on the menu bar
- App Version: 1.3,10
- App Website: https://github.com/insidegui/PodcastMenu

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install PodcastMenu with the following command
   ```
   brew install --cask podcastmenu
   ```
4. PodcastMenu is ready to use now!