---
title: "Install Reveal on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Powerful runtime view debugging for iOS developers"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Reveal on MacOS using homebrew

- App Name: Reveal
- App description: Powerful runtime view debugging for iOS developers
- App Version: 33,15756
- App Website: https://revealapp.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Reveal with the following command
   ```
   brew install --cask reveal
   ```
4. Reveal is ready to use now!