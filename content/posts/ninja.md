---
title: "Install ninja on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Small build system for use with gyp or CMake"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ninja on MacOS using homebrew

- App Name: ninja
- App description: Small build system for use with gyp or CMake
- App Version: 1.10.2
- App Website: https://ninja-build.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ninja with the following command
   ```
   brew install ninja
   ```
4. ninja is ready to use now!