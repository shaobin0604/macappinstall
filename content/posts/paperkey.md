---
title: "Install paperkey on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Extract just secret information out of OpenPGP secret keys"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install paperkey on MacOS using homebrew

- App Name: paperkey
- App description: Extract just secret information out of OpenPGP secret keys
- App Version: 1.6
- App Website: https://www.jabberwocky.com/software/paperkey/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install paperkey with the following command
   ```
   brew install paperkey
   ```
4. paperkey is ready to use now!