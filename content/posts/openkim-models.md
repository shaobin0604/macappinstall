---
title: "Install openkim-models on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "All OpenKIM Models compatible with kim-api"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install openkim-models on MacOS using homebrew

- App Name: openkim-models
- App description: All OpenKIM Models compatible with kim-api
- App Version: 2021-08-11
- App Website: https://openkim.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install openkim-models with the following command
   ```
   brew install openkim-models
   ```
4. openkim-models is ready to use now!