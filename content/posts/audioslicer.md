---
title: "Install AudioSlicer on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Finds all silences in an audio file"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install AudioSlicer on MacOS using homebrew

- App Name: AudioSlicer
- App description: Finds all silences in an audio file
- App Version: 1.1.1
- App Website: https://audioslicer.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install AudioSlicer with the following command
   ```
   brew install --cask audioslicer
   ```
4. AudioSlicer is ready to use now!