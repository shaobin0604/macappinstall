---
title: "Install arangodb on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Multi-Model NoSQL Database"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install arangodb on MacOS using homebrew

- App Name: arangodb
- App description: Multi-Model NoSQL Database
- App Version: 3.9.0
- App Website: https://www.arangodb.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install arangodb with the following command
   ```
   brew install arangodb
   ```
4. arangodb is ready to use now!