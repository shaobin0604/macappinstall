---
title: "Install almighty on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Settings and tweaks configurator"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install almighty on MacOS using homebrew

- App Name: almighty
- App description: Settings and tweaks configurator
- App Version: 2.2.1,32
- App Website: https://onmyway133.com/almighty/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install almighty with the following command
   ```
   brew install --cask almighty
   ```
4. almighty is ready to use now!