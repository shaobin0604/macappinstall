---
title: "Install Buckets on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Budgeting tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Buckets on MacOS using homebrew

- App Name: Buckets
- App description: Budgeting tool
- App Version: 0.63.2
- App Website: https://www.budgetwithbuckets.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Buckets with the following command
   ```
   brew install --cask buckets
   ```
4. Buckets is ready to use now!