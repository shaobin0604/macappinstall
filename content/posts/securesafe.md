---
title: "Install SecureSafe on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Highly secure online storage with password manager"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install SecureSafe on MacOS using homebrew

- App Name: SecureSafe
- App description: Highly secure online storage with password manager
- App Version: 2.13.0
- App Website: https://www.securesafe.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install SecureSafe with the following command
   ```
   brew install --cask securesafe
   ```
4. SecureSafe is ready to use now!