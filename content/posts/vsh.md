---
title: "Install vsh on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "HashiCorp Vault interactive shell"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install vsh on MacOS using homebrew

- App Name: vsh
- App description: HashiCorp Vault interactive shell
- App Version: 0.12.1
- App Website: https://github.com/fishi0x01/vsh

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install vsh with the following command
   ```
   brew install vsh
   ```
4. vsh is ready to use now!