---
title: "Install latex2rtf on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Translate LaTeX to RTF"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install latex2rtf on MacOS using homebrew

- App Name: latex2rtf
- App description: Translate LaTeX to RTF
- App Version: 2.3.18a
- App Website: https://latex2rtf.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install latex2rtf with the following command
   ```
   brew install latex2rtf
   ```
4. latex2rtf is ready to use now!