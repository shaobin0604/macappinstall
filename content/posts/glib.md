---
title: "Install glib on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Core application library for C"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install glib on MacOS using homebrew

- App Name: glib
- App description: Core application library for C
- App Version: 2.70.4
- App Website: https://developer.gnome.org/glib/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install glib with the following command
   ```
   brew install glib
   ```
4. glib is ready to use now!