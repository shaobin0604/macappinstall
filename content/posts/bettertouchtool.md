---
title: "Install BetterTouchTool on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to customize input devices and automate computer systems"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install BetterTouchTool on MacOS using homebrew

- App Name: BetterTouchTool
- App description: Tool to customize input devices and automate computer systems
- App Version: 3.743,1885
- App Website: https://folivora.ai/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install BetterTouchTool with the following command
   ```
   brew install --cask bettertouchtool
   ```
4. BetterTouchTool is ready to use now!