---
title: "Install legit on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line interface for Git, optimized for workflow simplicity"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install legit on MacOS using homebrew

- App Name: legit
- App description: Command-line interface for Git, optimized for workflow simplicity
- App Version: 1.2.0.post0
- App Website: https://frostming.github.io/legit/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install legit with the following command
   ```
   brew install legit
   ```
4. legit is ready to use now!