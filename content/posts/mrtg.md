---
title: "Install mrtg on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Multi router traffic grapher"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mrtg on MacOS using homebrew

- App Name: mrtg
- App description: Multi router traffic grapher
- App Version: 2.17.10
- App Website: https://oss.oetiker.ch/mrtg/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mrtg with the following command
   ```
   brew install mrtg
   ```
4. mrtg is ready to use now!