---
title: "Install Launchpad Manager on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to manage the launchpad"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Launchpad Manager on MacOS using homebrew

- App Name: Launchpad Manager
- App description: Tool to manage the launchpad
- App Version: 1.0.10
- App Website: http://launchpadmanager.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Launchpad Manager with the following command
   ```
   brew install --cask launchpad-manager
   ```
4. Launchpad Manager is ready to use now!