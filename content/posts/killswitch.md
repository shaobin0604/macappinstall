---
title: "Install killswitch on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "VPN kill switch for macOS"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install killswitch on MacOS using homebrew

- App Name: killswitch
- App description: VPN kill switch for macOS
- App Version: 0.7.2
- App Website: https://vpn-kill-switch.com

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install killswitch with the following command
   ```
   brew install killswitch
   ```
4. killswitch is ready to use now!