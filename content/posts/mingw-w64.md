---
title: "Install mingw-w64 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Minimalist GNU for Windows and GCC cross-compilers"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mingw-w64 on MacOS using homebrew

- App Name: mingw-w64
- App description: Minimalist GNU for Windows and GCC cross-compilers
- App Version: 9.0.0
- App Website: https://sourceforge.net/projects/mingw-w64/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mingw-w64 with the following command
   ```
   brew install mingw-w64
   ```
4. mingw-w64 is ready to use now!