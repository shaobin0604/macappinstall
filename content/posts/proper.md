---
title: "Install proper on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "QuickCheck-inspired property-based testing tool for Erlang"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install proper on MacOS using homebrew

- App Name: proper
- App description: QuickCheck-inspired property-based testing tool for Erlang
- App Version: 1.4
- App Website: https://proper-testing.github.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install proper with the following command
   ```
   brew install proper
   ```
4. proper is ready to use now!