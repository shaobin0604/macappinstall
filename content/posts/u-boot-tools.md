---
title: "Install u-boot-tools on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Universal boot loader"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install u-boot-tools on MacOS using homebrew

- App Name: u-boot-tools
- App description: Universal boot loader
- App Version: 2022.01
- App Website: https://www.denx.de/wiki/U-Boot/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install u-boot-tools with the following command
   ```
   brew install u-boot-tools
   ```
4. u-boot-tools is ready to use now!