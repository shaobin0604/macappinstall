---
title: "Install Bugdom on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Bug-themed 3D action/adventure game from Pangea Software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Bugdom on MacOS using homebrew

- App Name: Bugdom
- App description: Bug-themed 3D action/adventure game from Pangea Software
- App Version: 1.3.1
- App Website: https://pangeasoft.net/bug/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Bugdom with the following command
   ```
   brew install --cask bugdom
   ```
4. Bugdom is ready to use now!