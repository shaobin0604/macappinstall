---
title: "Install bibtexconv on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "BibTeX file converter"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bibtexconv on MacOS using homebrew

- App Name: bibtexconv
- App description: BibTeX file converter
- App Version: 1.3.2
- App Website: https://www.uni-due.de/~be0001/bibtexconv/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bibtexconv with the following command
   ```
   brew install bibtexconv
   ```
4. bibtexconv is ready to use now!