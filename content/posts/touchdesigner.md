---
title: "Install Derivative TouchDesigner on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool for creating dynamic digital art"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Derivative TouchDesigner on MacOS using homebrew

- App Name: Derivative TouchDesigner
- App description: Tool for creating dynamic digital art
- App Version: 2021.16270
- App Website: https://derivative.ca/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Derivative TouchDesigner with the following command
   ```
   brew install --cask touchdesigner
   ```
4. Derivative TouchDesigner is ready to use now!