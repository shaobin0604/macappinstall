---
title: "Install apachetop on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Top-like display of Apache log"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install apachetop on MacOS using homebrew

- App Name: apachetop
- App description: Top-like display of Apache log
- App Version: 0.19.7
- App Website: https://web.archive.org/web/20170809160553/freecode.com/projects/apachetop

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install apachetop with the following command
   ```
   brew install apachetop
   ```
4. apachetop is ready to use now!