---
title: "Install simutrans on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Transport simulator"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install simutrans on MacOS using homebrew

- App Name: simutrans
- App description: Transport simulator
- App Version: 123.0.1
- App Website: https://www.simutrans.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install simutrans with the following command
   ```
   brew install simutrans
   ```
4. simutrans is ready to use now!