---
title: "Install coccinelle on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Program matching and transformation engine for C code"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install coccinelle on MacOS using homebrew

- App Name: coccinelle
- App description: Program matching and transformation engine for C code
- App Version: 1.1.1
- App Website: http://coccinelle.lip6.fr/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install coccinelle with the following command
   ```
   brew install coccinelle
   ```
4. coccinelle is ready to use now!