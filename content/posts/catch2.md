---
title: "Install catch2 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Modern, C++-native, header-only, test framework"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install catch2 on MacOS using homebrew

- App Name: catch2
- App description: Modern, C++-native, header-only, test framework
- App Version: 2.13.8
- App Website: https://github.com/catchorg/Catch2

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install catch2 with the following command
   ```
   brew install catch2
   ```
4. catch2 is ready to use now!