---
title: "Install libvdpau on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Open source Video Decode and Presentation API library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libvdpau on MacOS using homebrew

- App Name: libvdpau
- App description: Open source Video Decode and Presentation API library
- App Version: 1.4
- App Website: https://www.freedesktop.org/wiki/Software/VDPAU/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libvdpau with the following command
   ```
   brew install libvdpau
   ```
4. libvdpau is ready to use now!