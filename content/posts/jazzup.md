---
title: "Install JazzUp on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Plays sound effects as you type"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install JazzUp on MacOS using homebrew

- App Name: JazzUp
- App description: Plays sound effects as you type
- App Version: 1.0b3,3
- App Website: https://www.irradiatedsoftware.com/labs/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install JazzUp with the following command
   ```
   brew install --cask jazzup
   ```
4. JazzUp is ready to use now!