---
title: "Install lastpass-cli on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "LastPass command-line interface tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install lastpass-cli on MacOS using homebrew

- App Name: lastpass-cli
- App description: LastPass command-line interface tool
- App Version: 1.3.3
- App Website: https://github.com/lastpass/lastpass-cli

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install lastpass-cli with the following command
   ```
   brew install lastpass-cli
   ```
4. lastpass-cli is ready to use now!