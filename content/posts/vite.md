---
title: "Install vite on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Next generation frontend tooling. It's fast!"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install vite on MacOS using homebrew

- App Name: vite
- App description: Next generation frontend tooling. It's fast!
- App Version: 2.8.3
- App Website: https://vitejs.dev/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install vite with the following command
   ```
   brew install vite
   ```
4. vite is ready to use now!