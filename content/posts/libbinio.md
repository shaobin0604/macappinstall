---
title: "Install libbinio on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Binary I/O stream class library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libbinio on MacOS using homebrew

- App Name: libbinio
- App description: Binary I/O stream class library
- App Version: 1.5
- App Website: https://adplug.github.io/libbinio/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libbinio with the following command
   ```
   brew install libbinio
   ```
4. libbinio is ready to use now!