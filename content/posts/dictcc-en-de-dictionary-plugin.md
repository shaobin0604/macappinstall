---
title: "Install dict.cc English-German dictionary plugin on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dict.cc English-German dictionary plugin on MacOS using homebrew

- App Name: dict.cc English-German dictionary plugin
- App description: null
- App Version: 2011-05-26
- App Website: https://www.dict.cc/?s=about%3Awordlist

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dict.cc English-German dictionary plugin with the following command
   ```
   brew install --cask dictcc-en-de-dictionary-plugin
   ```
4. dict.cc English-German dictionary plugin is ready to use now!