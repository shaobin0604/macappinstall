---
title: "Install WiFi Explorer on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Scan, monitor, and troubleshoot wireless networks"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install WiFi Explorer on MacOS using homebrew

- App Name: WiFi Explorer
- App description: Scan, monitor, and troubleshoot wireless networks
- App Version: 3.3.3,48
- App Website: https://www.intuitibits.com/products/wifi-explorer/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install WiFi Explorer with the following command
   ```
   brew install --cask wifi-explorer
   ```
4. WiFi Explorer is ready to use now!