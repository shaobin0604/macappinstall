---
title: "Install ephemeralpg on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Run tests on an isolated, temporary Postgres database"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ephemeralpg on MacOS using homebrew

- App Name: ephemeralpg
- App description: Run tests on an isolated, temporary Postgres database
- App Version: 3.1
- App Website: https://eradman.com/ephemeralpg/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ephemeralpg with the following command
   ```
   brew install ephemeralpg
   ```
4. ephemeralpg is ready to use now!