---
title: "Install blackbox on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Safely store secrets in Git/Mercurial/Subversion"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install blackbox on MacOS using homebrew

- App Name: blackbox
- App description: Safely store secrets in Git/Mercurial/Subversion
- App Version: 2.0.0
- App Website: https://github.com/StackExchange/blackbox

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install blackbox with the following command
   ```
   brew install blackbox
   ```
4. blackbox is ready to use now!