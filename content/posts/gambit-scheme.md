---
title: "Install gambit-scheme on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Implementation of the Scheme Language"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gambit-scheme on MacOS using homebrew

- App Name: gambit-scheme
- App description: Implementation of the Scheme Language
- App Version: 4.9.3
- App Website: https://github.com/gambit/gambit

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gambit-scheme with the following command
   ```
   brew install gambit-scheme
   ```
4. gambit-scheme is ready to use now!