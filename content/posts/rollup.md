---
title: "Install rollup on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Next-generation ES module bundler"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install rollup on MacOS using homebrew

- App Name: rollup
- App description: Next-generation ES module bundler
- App Version: 2.67.2
- App Website: https://rollupjs.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install rollup with the following command
   ```
   brew install rollup
   ```
4. rollup is ready to use now!