---
title: "Install uptoc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Convenient static file deployment tool that supports multiple platforms"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install uptoc on MacOS using homebrew

- App Name: uptoc
- App description: Convenient static file deployment tool that supports multiple platforms
- App Version: 1.4.3
- App Website: https://github.com/saltbo/uptoc

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install uptoc with the following command
   ```
   brew install uptoc
   ```
4. uptoc is ready to use now!