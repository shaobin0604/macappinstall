---
title: "Install SmoothScroll on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Smooth mouse scrolling utility"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install SmoothScroll on MacOS using homebrew

- App Name: SmoothScroll
- App description: Smooth mouse scrolling utility
- App Version: 1.5.7,10507.1
- App Website: https://www.smoothscroll.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install SmoothScroll with the following command
   ```
   brew install --cask smoothscroll
   ```
4. SmoothScroll is ready to use now!