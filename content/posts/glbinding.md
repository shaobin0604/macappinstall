---
title: "Install glbinding on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C++ binding for the OpenGL API"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install glbinding on MacOS using homebrew

- App Name: glbinding
- App description: C++ binding for the OpenGL API
- App Version: 2.1.4
- App Website: https://github.com/cginternals/glbinding

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install glbinding with the following command
   ```
   brew install glbinding
   ```
4. glbinding is ready to use now!