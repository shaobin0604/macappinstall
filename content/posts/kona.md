---
title: "Install kona on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Open-source implementation of the K programming language"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install kona on MacOS using homebrew

- App Name: kona
- App description: Open-source implementation of the K programming language
- App Version: 20211225
- App Website: https://github.com/kevinlawler/kona

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install kona with the following command
   ```
   brew install kona
   ```
4. kona is ready to use now!