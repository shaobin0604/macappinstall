---
title: "Install iStumbler on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Wireless discovery tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install iStumbler on MacOS using homebrew

- App Name: iStumbler
- App description: Wireless discovery tool
- App Version: 103.43
- App Website: https://istumbler.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install iStumbler with the following command
   ```
   brew install --cask istumbler
   ```
4. iStumbler is ready to use now!