---
title: "Install ColorWell on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Color picker and color palette generator"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ColorWell on MacOS using homebrew

- App Name: ColorWell
- App description: Color picker and color palette generator
- App Version: 7.3.4
- App Website: https://colorwell.sweetpproductions.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ColorWell with the following command
   ```
   brew install --cask colorwell
   ```
4. ColorWell is ready to use now!