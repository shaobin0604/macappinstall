---
title: "Install openfast on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "NREL-supported OpenFAST whole-turbine simulation code"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install openfast on MacOS using homebrew

- App Name: openfast
- App description: NREL-supported OpenFAST whole-turbine simulation code
- App Version: 3.0.0
- App Website: https://openfast.readthedocs.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install openfast with the following command
   ```
   brew install openfast
   ```
4. openfast is ready to use now!