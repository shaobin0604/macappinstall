---
title: "Install fsevent_watch on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "macOS FSEvents client"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fsevent_watch on MacOS using homebrew

- App Name: fsevent_watch
- App description: macOS FSEvents client
- App Version: 0.2
- App Website: https://github.com/proger/fsevent_watch

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fsevent_watch with the following command
   ```
   brew install fsevent_watch
   ```
4. fsevent_watch is ready to use now!