---
title: "Install noTunes on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Simple application that will prevent iTunes or Apple Music from launching"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install noTunes on MacOS using homebrew

- App Name: noTunes
- App description: Simple application that will prevent iTunes or Apple Music from launching
- App Version: 3.2
- App Website: https://github.com/tombonez/noTunes

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install noTunes with the following command
   ```
   brew install --cask notunes
   ```
4. noTunes is ready to use now!