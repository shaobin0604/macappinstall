---
title: "Install sylpheed on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Simple, lightweight email-client"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sylpheed on MacOS using homebrew

- App Name: sylpheed
- App description: Simple, lightweight email-client
- App Version: 3.7.0
- App Website: https://sylpheed.sraoss.jp/en/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sylpheed with the following command
   ```
   brew install sylpheed
   ```
4. sylpheed is ready to use now!