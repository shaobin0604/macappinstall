---
title: "Install Rocks'n'Diamonds on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Arcade-style game"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Rocks'n'Diamonds on MacOS using homebrew

- App Name: Rocks'n'Diamonds
- App description: Arcade-style game
- App Version: 4.3.1.1
- App Website: https://www.artsoft.org/rocksndiamonds/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Rocks'n'Diamonds with the following command
   ```
   brew install --cask rocks-n-diamonds
   ```
4. Rocks'n'Diamonds is ready to use now!