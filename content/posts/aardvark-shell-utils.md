---
title: "Install aardvark_shell_utils on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utilities to aid shell scripts or command-line users"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install aardvark_shell_utils on MacOS using homebrew

- App Name: aardvark_shell_utils
- App description: Utilities to aid shell scripts or command-line users
- App Version: 1.0
- App Website: http://www.laffeycomputer.com/shellutils.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install aardvark_shell_utils with the following command
   ```
   brew install aardvark_shell_utils
   ```
4. aardvark_shell_utils is ready to use now!