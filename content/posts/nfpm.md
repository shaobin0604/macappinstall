---
title: "Install nfpm on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Simple deb and rpm packager"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install nfpm on MacOS using homebrew

- App Name: nfpm
- App description: Simple deb and rpm packager
- App Version: 2.13.0
- App Website: https://nfpm.goreleaser.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install nfpm with the following command
   ```
   brew install nfpm
   ```
4. nfpm is ready to use now!