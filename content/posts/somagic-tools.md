---
title: "Install somagic-tools on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tools to extract firmware from EasyCAP"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install somagic-tools on MacOS using homebrew

- App Name: somagic-tools
- App description: Tools to extract firmware from EasyCAP
- App Version: 1.1
- App Website: https://code.google.com/archive/p/easycap-somagic-linux/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install somagic-tools with the following command
   ```
   brew install somagic-tools
   ```
4. somagic-tools is ready to use now!