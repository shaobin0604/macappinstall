---
title: "Install admesh on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Processes triangulated solid meshes"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install admesh on MacOS using homebrew

- App Name: admesh
- App description: Processes triangulated solid meshes
- App Version: 0.98.4
- App Website: https://github.com/admesh/admesh

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install admesh with the following command
   ```
   brew install admesh
   ```
4. admesh is ready to use now!