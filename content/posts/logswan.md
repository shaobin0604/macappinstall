---
title: "Install logswan on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Fast Web log analyzer using probabilistic data structures"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install logswan on MacOS using homebrew

- App Name: logswan
- App description: Fast Web log analyzer using probabilistic data structures
- App Version: 2.1.12
- App Website: https://www.logswan.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install logswan with the following command
   ```
   brew install logswan
   ```
4. logswan is ready to use now!