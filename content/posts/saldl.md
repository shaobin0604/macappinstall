---
title: "Install saldl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CLI downloader optimized for speed and early preview"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install saldl on MacOS using homebrew

- App Name: saldl
- App description: CLI downloader optimized for speed and early preview
- App Version: 41
- App Website: https://saldl.github.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install saldl with the following command
   ```
   brew install saldl
   ```
4. saldl is ready to use now!