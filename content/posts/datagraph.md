---
title: "Install DataGraph on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Scientific/statistical graphing software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install DataGraph on MacOS using homebrew

- App Name: DataGraph
- App description: Scientific/statistical graphing software
- App Version: 5.0,59
- App Website: https://www.visualdatatools.com/DataGraph/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install DataGraph with the following command
   ```
   brew install --cask datagraph
   ```
4. DataGraph is ready to use now!