---
title: "Install libxxf86vm on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "X.Org: XFree86-VidMode X extension"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libxxf86vm on MacOS using homebrew

- App Name: libxxf86vm
- App description: X.Org: XFree86-VidMode X extension
- App Version: 1.1.4
- App Website: https://www.x.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libxxf86vm with the following command
   ```
   brew install libxxf86vm
   ```
4. libxxf86vm is ready to use now!