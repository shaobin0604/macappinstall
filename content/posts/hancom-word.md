---
title: "Install Hacom Word Processor 2014 VP on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Word processor"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Hacom Word Processor 2014 VP on MacOS using homebrew

- App Name: Hacom Word Processor 2014 VP
- App description: Word processor
- App Version: latest
- App Website: https://office.hancom.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Hacom Word Processor 2014 VP with the following command
   ```
   brew install --cask hancom-word
   ```
4. Hacom Word Processor 2014 VP is ready to use now!