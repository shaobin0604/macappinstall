---
title: "Install ncview on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Visual browser for netCDF format files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ncview on MacOS using homebrew

- App Name: ncview
- App description: Visual browser for netCDF format files
- App Version: 2.1.8
- App Website: https://cirrus.ucsd.edu/ncview/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ncview with the following command
   ```
   brew install ncview
   ```
4. ncview is ready to use now!