---
title: "Install cfssl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CloudFlare's PKI toolkit"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cfssl on MacOS using homebrew

- App Name: cfssl
- App description: CloudFlare's PKI toolkit
- App Version: 1.6.1
- App Website: https://cfssl.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cfssl with the following command
   ```
   brew install cfssl
   ```
4. cfssl is ready to use now!