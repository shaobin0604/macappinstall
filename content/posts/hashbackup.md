---
title: "Install HashBackup on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line backup program"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install HashBackup on MacOS using homebrew

- App Name: HashBackup
- App description: Command-line backup program
- App Version: 2677
- App Website: https://www.hashbackup.com/hashbackup/index.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install HashBackup with the following command
   ```
   brew install --cask hashbackup
   ```
4. HashBackup is ready to use now!