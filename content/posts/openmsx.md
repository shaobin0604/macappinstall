---
title: "Install openmsx on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "MSX emulator"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install openmsx on MacOS using homebrew

- App Name: openmsx
- App description: MSX emulator
- App Version: 17.0
- App Website: https://openmsx.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install openmsx with the following command
   ```
   brew install openmsx
   ```
4. openmsx is ready to use now!