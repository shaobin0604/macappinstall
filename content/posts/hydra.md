---
title: "Install hydra on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Network logon cracker which supports many services"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install hydra on MacOS using homebrew

- App Name: hydra
- App description: Network logon cracker which supports many services
- App Version: 9.2
- App Website: https://github.com/vanhauser-thc/thc-hydra

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install hydra with the following command
   ```
   brew install hydra
   ```
4. hydra is ready to use now!