---
title: "Install Joplin on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Note taking and to-do application with synchronization capabilities"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Joplin on MacOS using homebrew

- App Name: Joplin
- App description: Note taking and to-do application with synchronization capabilities
- App Version: 2.6.10
- App Website: https://joplinapp.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Joplin with the following command
   ```
   brew install --cask joplin
   ```
4. Joplin is ready to use now!