---
title: "Install Xcodes on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Install and switch between multiple versions of Xcode"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Xcodes on MacOS using homebrew

- App Name: Xcodes
- App description: Install and switch between multiple versions of Xcode
- App Version: 1.2.0,8
- App Website: https://github.com/RobotsAndPencils/XcodesApp

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Xcodes with the following command
   ```
   brew install --cask xcodes
   ```
4. Xcodes is ready to use now!