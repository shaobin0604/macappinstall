---
title: "Install VYM (View Your Mind) on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Generate and manipulate maps which show your thoughts"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install VYM (View Your Mind) on MacOS using homebrew

- App Name: VYM (View Your Mind)
- App description: Generate and manipulate maps which show your thoughts
- App Version: 2.8.8
- App Website: https://sourceforge.net/projects/vym/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install VYM (View Your Mind) with the following command
   ```
   brew install --cask vym
   ```
4. VYM (View Your Mind) is ready to use now!