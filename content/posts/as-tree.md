---
title: "Install as-tree on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Print a list of paths as a tree of paths 🌳"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install as-tree on MacOS using homebrew

- App Name: as-tree
- App description: Print a list of paths as a tree of paths 🌳
- App Version: 0.12.0
- App Website: https://github.com/jez/as-tree

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install as-tree with the following command
   ```
   brew install as-tree
   ```
4. as-tree is ready to use now!