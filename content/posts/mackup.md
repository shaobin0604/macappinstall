---
title: "Install mackup on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Keep your Mac's application settings in sync"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mackup on MacOS using homebrew

- App Name: mackup
- App description: Keep your Mac's application settings in sync
- App Version: 0.8.33
- App Website: https://github.com/lra/mackup

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mackup with the following command
   ```
   brew install mackup
   ```
4. mackup is ready to use now!