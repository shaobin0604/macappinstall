---
title: "Install Cinco on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Generator-driven Eclipse IDE for domain-specific graphical modeling tools"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Cinco on MacOS using homebrew

- App Name: Cinco
- App description: Generator-driven Eclipse IDE for domain-specific graphical modeling tools
- App Version: 2.0.1
- App Website: https://cinco.scce.info/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Cinco with the following command
   ```
   brew install --cask cinco
   ```
4. Cinco is ready to use now!