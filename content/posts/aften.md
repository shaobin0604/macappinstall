---
title: "Install aften on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Audio encoder which generates ATSC A/52 compressed audio streams"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install aften on MacOS using homebrew

- App Name: aften
- App description: Audio encoder which generates ATSC A/52 compressed audio streams
- App Version: 0.0.8
- App Website: https://aften.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install aften with the following command
   ```
   brew install aften
   ```
4. aften is ready to use now!