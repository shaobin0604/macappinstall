---
title: "Install avro-cpp on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Data serialization system"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install avro-cpp on MacOS using homebrew

- App Name: avro-cpp
- App description: Data serialization system
- App Version: 1.11.0
- App Website: https://avro.apache.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install avro-cpp with the following command
   ```
   brew install avro-cpp
   ```
4. avro-cpp is ready to use now!