---
title: "Install antlr4-cpp-runtime on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "ANother Tool for Language Recognition C++ Runtime Library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install antlr4-cpp-runtime on MacOS using homebrew

- App Name: antlr4-cpp-runtime
- App description: ANother Tool for Language Recognition C++ Runtime Library
- App Version: 4.9.3
- App Website: https://www.antlr.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install antlr4-cpp-runtime with the following command
   ```
   brew install antlr4-cpp-runtime
   ```
4. antlr4-cpp-runtime is ready to use now!