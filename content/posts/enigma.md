---
title: "Install enigma on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Puzzle game inspired by Oxyd and Rock'n'Roll"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install enigma on MacOS using homebrew

- App Name: enigma
- App description: Puzzle game inspired by Oxyd and Rock'n'Roll
- App Version: 1.30
- App Website: https://www.nongnu.org/enigma/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install enigma with the following command
   ```
   brew install enigma
   ```
4. enigma is ready to use now!