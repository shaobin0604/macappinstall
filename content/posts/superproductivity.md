---
title: "Install Super Productivity on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "To-do list and time tracker"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Super Productivity on MacOS using homebrew

- App Name: Super Productivity
- App description: To-do list and time tracker
- App Version: 7.10.1
- App Website: https://super-productivity.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Super Productivity with the following command
   ```
   brew install --cask superproductivity
   ```
4. Super Productivity is ready to use now!