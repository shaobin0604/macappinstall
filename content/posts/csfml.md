---
title: "Install csfml on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "SMFL bindings for C"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install csfml on MacOS using homebrew

- App Name: csfml
- App description: SMFL bindings for C
- App Version: 2.5.1
- App Website: https://www.sfml-dev.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install csfml with the following command
   ```
   brew install csfml
   ```
4. csfml is ready to use now!