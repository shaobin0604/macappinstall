---
title: "Install zbctl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Zeebe CLI client"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install zbctl on MacOS using homebrew

- App Name: zbctl
- App description: Zeebe CLI client
- App Version: 1.3.4
- App Website: https://docs.camunda.io/docs/apis-clients/cli-client/index/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install zbctl with the following command
   ```
   brew install zbctl
   ```
4. zbctl is ready to use now!