---
title: "Install Navicat Data Modeler on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Database design tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Navicat Data Modeler on MacOS using homebrew

- App Name: Navicat Data Modeler
- App description: Database design tool
- App Version: 3.1.4
- App Website: https://www.navicat.com/products/navicat-data-modeler

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Navicat Data Modeler with the following command
   ```
   brew install --cask navicat-data-modeler
   ```
4. Navicat Data Modeler is ready to use now!