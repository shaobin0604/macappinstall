---
title: "Install bpytop on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Linux/OSX/FreeBSD resource monitor"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bpytop on MacOS using homebrew

- App Name: bpytop
- App description: Linux/OSX/FreeBSD resource monitor
- App Version: 1.0.68
- App Website: https://github.com/aristocratos/bpytop

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bpytop with the following command
   ```
   brew install bpytop
   ```
4. bpytop is ready to use now!