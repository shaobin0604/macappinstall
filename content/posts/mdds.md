---
title: "Install mdds on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Multi-dimensional data structure and indexing algorithm"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mdds on MacOS using homebrew

- App Name: mdds
- App description: Multi-dimensional data structure and indexing algorithm
- App Version: 2.0.1
- App Website: https://gitlab.com/mdds/mdds

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mdds with the following command
   ```
   brew install mdds
   ```
4. mdds is ready to use now!