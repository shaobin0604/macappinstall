---
title: "Install weighttp on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Webserver benchmarking tool that supports multithreading"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install weighttp on MacOS using homebrew

- App Name: weighttp
- App description: Webserver benchmarking tool that supports multithreading
- App Version: 0.4
- App Website: https://redmine.lighttpd.net/projects/weighttp/wiki

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install weighttp with the following command
   ```
   brew install weighttp
   ```
4. weighttp is ready to use now!