---
title: "Install fontconfig on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "XML-based font configuration API for X Windows"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fontconfig on MacOS using homebrew

- App Name: fontconfig
- App description: XML-based font configuration API for X Windows
- App Version: 2.13.1
- App Website: https://wiki.freedesktop.org/www/Software/fontconfig/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fontconfig with the following command
   ```
   brew install fontconfig
   ```
4. fontconfig is ready to use now!