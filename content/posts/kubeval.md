---
title: "Install kubeval on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Validate Kubernetes configuration files, supports multiple Kubernetes versions"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install kubeval on MacOS using homebrew

- App Name: kubeval
- App description: Validate Kubernetes configuration files, supports multiple Kubernetes versions
- App Version: 0.16.1
- App Website: https://www.kubeval.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install kubeval with the following command
   ```
   brew install kubeval
   ```
4. kubeval is ready to use now!