---
title: "Install woof on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Ad-hoc single-file webserver"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install woof on MacOS using homebrew

- App Name: woof
- App description: Ad-hoc single-file webserver
- App Version: 20220202
- App Website: http://www.home.unix-ag.org/simon/woof.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install woof with the following command
   ```
   brew install woof
   ```
4. woof is ready to use now!