---
title: "Install Screens Connect on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Remote desktop software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Screens Connect on MacOS using homebrew

- App Name: Screens Connect
- App description: Remote desktop software
- App Version: 4.11.1,21658
- App Website: https://edovia.com/en/screens-connect/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Screens Connect with the following command
   ```
   brew install --cask screens-connect
   ```
4. Screens Connect is ready to use now!