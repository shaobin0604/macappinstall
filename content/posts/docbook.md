---
title: "Install docbook on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Standard SGML representation system for technical documents"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install docbook on MacOS using homebrew

- App Name: docbook
- App description: Standard SGML representation system for technical documents
- App Version: 5.1
- App Website: https://docbook.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install docbook with the following command
   ```
   brew install docbook
   ```
4. docbook is ready to use now!