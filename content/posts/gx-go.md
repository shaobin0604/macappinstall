---
title: "Install gx-go on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to use with the gx package manager for packages written in go"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gx-go on MacOS using homebrew

- App Name: gx-go
- App description: Tool to use with the gx package manager for packages written in go
- App Version: 1.9.0
- App Website: https://github.com/whyrusleeping/gx-go

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gx-go with the following command
   ```
   brew install gx-go
   ```
4. gx-go is ready to use now!