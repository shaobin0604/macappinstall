---
title: "Install pyenv-pip-migrate on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Migrate pip packages from one Python version to another"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pyenv-pip-migrate on MacOS using homebrew

- App Name: pyenv-pip-migrate
- App description: Migrate pip packages from one Python version to another
- App Version: 20181205
- App Website: https://github.com/pyenv/pyenv-pip-migrate

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pyenv-pip-migrate with the following command
   ```
   brew install pyenv-pip-migrate
   ```
4. pyenv-pip-migrate is ready to use now!