---
title: "Install osinfo-db on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Osinfo database of operating systems for virtualization provisioning tools"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install osinfo-db on MacOS using homebrew

- App Name: osinfo-db
- App description: Osinfo database of operating systems for virtualization provisioning tools
- App Version: 20220214
- App Website: https://libosinfo.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install osinfo-db with the following command
   ```
   brew install osinfo-db
   ```
4. osinfo-db is ready to use now!