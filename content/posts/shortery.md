---
title: "Install Shortery on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Shortcut automation"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Shortery on MacOS using homebrew

- App Name: Shortery
- App description: Shortcut automation
- App Version: 1.2,11
- App Website: https://lgerckens.de/shortery/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Shortery with the following command
   ```
   brew install --cask shortery
   ```
4. Shortery is ready to use now!