---
title: "Install faudio on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Accuracy-focused XAudio reimplementation for open platforms"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install faudio on MacOS using homebrew

- App Name: faudio
- App description: Accuracy-focused XAudio reimplementation for open platforms
- App Version: 22.02
- App Website: https://fna-xna.github.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install faudio with the following command
   ```
   brew install faudio
   ```
4. faudio is ready to use now!