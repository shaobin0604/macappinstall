---
title: "Install qlImageSize on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Display image info and preview unsupported formats in QuickLook"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install qlImageSize on MacOS using homebrew

- App Name: qlImageSize
- App description: Display image info and preview unsupported formats in QuickLook
- App Version: 2.6.1
- App Website: https://github.com/Nyx0uf/qlImageSize

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install qlImageSize with the following command
   ```
   brew install --cask qlimagesize
   ```
4. qlImageSize is ready to use now!