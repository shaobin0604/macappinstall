---
title: "Install libcap-ng on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library for Linux that makes using posix capabilities easy"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libcap-ng on MacOS using homebrew

- App Name: libcap-ng
- App description: Library for Linux that makes using posix capabilities easy
- App Version: 0.8.2
- App Website: https://people.redhat.com/sgrubb/libcap-ng

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libcap-ng with the following command
   ```
   brew install libcap-ng
   ```
4. libcap-ng is ready to use now!