---
title: "Install SubGit on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Convert SVN repositories to Git"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install SubGit on MacOS using homebrew

- App Name: SubGit
- App description: Convert SVN repositories to Git
- App Version: 3.3.12
- App Website: https://subgit.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install SubGit with the following command
   ```
   brew install --cask subgit
   ```
4. SubGit is ready to use now!