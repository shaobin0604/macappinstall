---
title: "Install Enpass on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Password and credentials mananger"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Enpass on MacOS using homebrew

- App Name: Enpass
- App description: Password and credentials mananger
- App Version: 6.7.4.949
- App Website: https://www.enpass.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Enpass with the following command
   ```
   brew install --cask enpass
   ```
4. Enpass is ready to use now!