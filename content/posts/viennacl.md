---
title: "Install viennacl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Linear algebra library for many-core architectures and multi-core CPUs"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install viennacl on MacOS using homebrew

- App Name: viennacl
- App description: Linear algebra library for many-core architectures and multi-core CPUs
- App Version: 1.7.1
- App Website: https://viennacl.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install viennacl with the following command
   ```
   brew install viennacl
   ```
4. viennacl is ready to use now!