---
title: "Install Ukelele on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Unicode keyboard layout editor"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Ukelele on MacOS using homebrew

- App Name: Ukelele
- App description: Unicode keyboard layout editor
- App Version: 3.5.5,351
- App Website: https://software.sil.org/ukelele/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Ukelele with the following command
   ```
   brew install --cask ukelele
   ```
4. Ukelele is ready to use now!