---
title: "Install dbus-glib on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GLib bindings for the D-Bus message bus system"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dbus-glib on MacOS using homebrew

- App Name: dbus-glib
- App description: GLib bindings for the D-Bus message bus system
- App Version: 0.112
- App Website: https://wiki.freedesktop.org/www/Software/DBusBindings/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dbus-glib with the following command
   ```
   brew install dbus-glib
   ```
4. dbus-glib is ready to use now!