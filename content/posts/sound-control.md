---
title: "Install Sound Control on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Per-app audio controls"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Sound Control on MacOS using homebrew

- App Name: Sound Control
- App description: Per-app audio controls
- App Version: 2.6.6,5156
- App Website: https://staticz.com/soundcontrol/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Sound Control with the following command
   ```
   brew install --cask sound-control
   ```
4. Sound Control is ready to use now!