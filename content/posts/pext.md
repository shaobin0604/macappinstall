---
title: "Install Pext on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Python-based extendable tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Pext on MacOS using homebrew

- App Name: Pext
- App description: Python-based extendable tool
- App Version: 0.32
- App Website: https://pext.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Pext with the following command
   ```
   brew install --cask pext
   ```
4. Pext is ready to use now!