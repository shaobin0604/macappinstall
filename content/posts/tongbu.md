---
title: "Install Tongbu on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Mobile phone management tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Tongbu on MacOS using homebrew

- App Name: Tongbu
- App description: Mobile phone management tool
- App Version: 3.1.5.0
- App Website: https://zs.tongbu.com/mac/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Tongbu with the following command
   ```
   brew install --cask tongbu
   ```
4. Tongbu is ready to use now!