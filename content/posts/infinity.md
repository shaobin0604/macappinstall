---
title: "Install Infinity on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Customizable work management platform"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Infinity on MacOS using homebrew

- App Name: Infinity
- App description: Customizable work management platform
- App Version: 1.0.0
- App Website: https://startinfinity.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Infinity with the following command
   ```
   brew install --cask infinity
   ```
4. Infinity is ready to use now!