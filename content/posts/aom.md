---
title: "Install aom on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Codec library for encoding and decoding AV1 video streams"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install aom on MacOS using homebrew

- App Name: aom
- App description: Codec library for encoding and decoding AV1 video streams
- App Version: 3.2.0
- App Website: https://aomedia.googlesource.com/aom

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install aom with the following command
   ```
   brew install aom
   ```
4. aom is ready to use now!