---
title: "Install Doomsday Engine on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Enhanced source port of Doom, Heretic, and Hexen"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Doomsday Engine on MacOS using homebrew

- App Name: Doomsday Engine
- App description: Enhanced source port of Doom, Heretic, and Hexen
- App Version: 2.3.1
- App Website: https://dengine.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Doomsday Engine with the following command
   ```
   brew install --cask doomsday-engine
   ```
4. Doomsday Engine is ready to use now!