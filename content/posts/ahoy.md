---
title: "Install ahoy on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Creates self documenting CLI programs from commands in YAML files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ahoy on MacOS using homebrew

- App Name: ahoy
- App description: Creates self documenting CLI programs from commands in YAML files
- App Version: 2.0.0
- App Website: https://ahoy-cli.readthedocs.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ahoy with the following command
   ```
   brew install ahoy
   ```
4. ahoy is ready to use now!