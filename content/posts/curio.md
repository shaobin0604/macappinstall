---
title: "Install Curio on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Note-taking and organizing app"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Curio on MacOS using homebrew

- App Name: Curio
- App description: Note-taking and organizing app
- App Version: 20,20009
- App Website: https://zengobi.com/curio/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Curio with the following command
   ```
   brew install --cask curio
   ```
4. Curio is ready to use now!