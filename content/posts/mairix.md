---
title: "Install mairix on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Email index and search tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mairix on MacOS using homebrew

- App Name: mairix
- App description: Email index and search tool
- App Version: 0.24
- App Website: http://www.rpcurnow.force9.co.uk/mairix/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mairix with the following command
   ```
   brew install mairix
   ```
4. mairix is ready to use now!