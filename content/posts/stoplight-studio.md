---
title: "Install Stoplight Studio on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Editor for designing and documenting APIs"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Stoplight Studio on MacOS using homebrew

- App Name: Stoplight Studio
- App description: Editor for designing and documenting APIs
- App Version: 2.8.0,7145.git-d6b6ed0
- App Website: https://stoplight.io/studio/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Stoplight Studio with the following command
   ```
   brew install --cask stoplight-studio
   ```
4. Stoplight Studio is ready to use now!