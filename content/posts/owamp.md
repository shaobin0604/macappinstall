---
title: "Install owamp on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Implementation of the One-Way Active Measurement Protocol"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install owamp on MacOS using homebrew

- App Name: owamp
- App description: Implementation of the One-Way Active Measurement Protocol
- App Version: 3.4-10
- App Website: https://www.internet2.edu/products-services/performance-analytics/performance-tools/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install owamp with the following command
   ```
   brew install owamp
   ```
4. owamp is ready to use now!