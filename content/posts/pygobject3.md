---
title: "Install pygobject3 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GNOME Python bindings (based on GObject Introspection)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pygobject3 on MacOS using homebrew

- App Name: pygobject3
- App description: GNOME Python bindings (based on GObject Introspection)
- App Version: 3.42.0
- App Website: https://wiki.gnome.org/Projects/PyGObject

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pygobject3 with the following command
   ```
   brew install pygobject3
   ```
4. pygobject3 is ready to use now!