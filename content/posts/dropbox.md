---
title: "Install Dropbox on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Client for the Dropbox cloud storage service"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Dropbox on MacOS using homebrew

- App Name: Dropbox
- App description: Client for the Dropbox cloud storage service
- App Version: 142.4.4197
- App Website: https://www.dropbox.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Dropbox with the following command
   ```
   brew install --cask dropbox
   ```
4. Dropbox is ready to use now!