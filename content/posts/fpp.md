---
title: "Install fpp on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CLI program that accepts piped input and presents files for selection"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fpp on MacOS using homebrew

- App Name: fpp
- App description: CLI program that accepts piped input and presents files for selection
- App Version: 0.9.2
- App Website: https://facebook.github.io/PathPicker/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fpp with the following command
   ```
   brew install fpp
   ```
4. fpp is ready to use now!