---
title: "Install Nektony VSDX Annotator on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Preview, edit and convert Visio drawings"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Nektony VSDX Annotator on MacOS using homebrew

- App Name: Nektony VSDX Annotator
- App description: Preview, edit and convert Visio drawings
- App Version: 1.15.1,556
- App Website: https://nektony.com/products/vsdx-annotator-mac

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Nektony VSDX Annotator with the following command
   ```
   brew install --cask vsdx-annotator
   ```
4. Nektony VSDX Annotator is ready to use now!