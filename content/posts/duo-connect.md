---
title: "Install DuoConnect on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Access your organization’s SSH servers"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install DuoConnect on MacOS using homebrew

- App Name: DuoConnect
- App description: Access your organization’s SSH servers
- App Version: 1.1.1
- App Website: https://guide.duo.com/duoconnect

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install DuoConnect with the following command
   ```
   brew install --cask duo-connect
   ```
4. DuoConnect is ready to use now!