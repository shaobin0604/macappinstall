---
title: "Install KeeWeb on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Password manager compatible with KeePass"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install KeeWeb on MacOS using homebrew

- App Name: KeeWeb
- App description: Password manager compatible with KeePass
- App Version: 1.18.7
- App Website: https://keeweb.info/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install KeeWeb with the following command
   ```
   brew install --cask keeweb
   ```
4. KeeWeb is ready to use now!