---
title: "Install Synfig Studio on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "2D animation software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Synfig Studio on MacOS using homebrew

- App Name: Synfig Studio
- App description: 2D animation software
- App Version: 1.5.1,2021.10.21,2cb6c
- App Website: https://synfig.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Synfig Studio with the following command
   ```
   brew install --cask synfigstudio
   ```
4. Synfig Studio is ready to use now!