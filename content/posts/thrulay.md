---
title: "Install thrulay on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Measure performance of a network"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install thrulay on MacOS using homebrew

- App Name: thrulay
- App description: Measure performance of a network
- App Version: 0.9
- App Website: https://sourceforge.net/projects/thrulay/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install thrulay with the following command
   ```
   brew install thrulay
   ```
4. thrulay is ready to use now!