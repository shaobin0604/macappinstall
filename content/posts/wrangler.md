---
title: "Install wrangler on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Refactoring tool for Erlang with emacs and Eclipse integration"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install wrangler on MacOS using homebrew

- App Name: wrangler
- App description: Refactoring tool for Erlang with emacs and Eclipse integration
- App Version: 1.2
- App Website: https://www.cs.kent.ac.uk/projects/wrangler/Wrangler/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install wrangler with the following command
   ```
   brew install wrangler
   ```
4. wrangler is ready to use now!