---
title: "Install Ears on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Instant audio switcher"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Ears on MacOS using homebrew

- App Name: Ears
- App description: Instant audio switcher
- App Version: 1.3.1,18
- App Website: https://clickontyler.com/ears/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Ears with the following command
   ```
   brew install --cask ears
   ```
4. Ears is ready to use now!