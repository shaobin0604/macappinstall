---
title: "Install stgit on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Manage Git commits as a stack of patches"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install stgit on MacOS using homebrew

- App Name: stgit
- App description: Manage Git commits as a stack of patches
- App Version: 1.5
- App Website: https://stacked-git.github.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install stgit with the following command
   ```
   brew install stgit
   ```
4. stgit is ready to use now!