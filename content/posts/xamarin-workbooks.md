---
title: "Install Xamarin Workbooks on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C# documentation and coding materials"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Xamarin Workbooks on MacOS using homebrew

- App Name: Xamarin Workbooks
- App description: C# documentation and coding materials
- App Version: 1.5.0
- App Website: https://docs.microsoft.com/en-us/xamarin/tools/workbooks/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Xamarin Workbooks with the following command
   ```
   brew install --cask xamarin-workbooks
   ```
4. Xamarin Workbooks is ready to use now!