---
title: "Install docx2txt on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Converts Microsoft Office docx documents to equivalent text documents"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install docx2txt on MacOS using homebrew

- App Name: docx2txt
- App description: Converts Microsoft Office docx documents to equivalent text documents
- App Version: 1.4
- App Website: https://docx2txt.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install docx2txt with the following command
   ```
   brew install docx2txt
   ```
4. docx2txt is ready to use now!