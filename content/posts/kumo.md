---
title: "Install kumo on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Word Clouds in Java"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install kumo on MacOS using homebrew

- App Name: kumo
- App description: Word Clouds in Java
- App Version: 1.28
- App Website: https://github.com/kennycason/kumo

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install kumo with the following command
   ```
   brew install kumo
   ```
4. kumo is ready to use now!