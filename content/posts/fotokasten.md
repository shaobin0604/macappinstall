---
title: "Install Fotokasten on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Create and buy photo products"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Fotokasten on MacOS using homebrew

- App Name: Fotokasten
- App description: Create and buy photo products
- App Version: 3.46.0,210630.1140
- App Website: https://www.fotokasten.de/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Fotokasten with the following command
   ```
   brew install --cask fotokasten
   ```
4. Fotokasten is ready to use now!