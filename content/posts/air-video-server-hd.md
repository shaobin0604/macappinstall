---
title: "Install Air Video Server HD on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to stream videos to Apple devices"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Air Video Server HD on MacOS using homebrew

- App Name: Air Video Server HD
- App description: Tool to stream videos to Apple devices
- App Version: 2.3.0-beta1u1,202.0902
- App Website: https://airvideo.app/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Air Video Server HD with the following command
   ```
   brew install --cask air-video-server-hd
   ```
4. Air Video Server HD is ready to use now!