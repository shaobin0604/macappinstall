---
title: "Install aws-okta on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Authenticate with AWS using your Okta credentials"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install aws-okta on MacOS using homebrew

- App Name: aws-okta
- App description: Authenticate with AWS using your Okta credentials
- App Version: 1.0.11
- App Website: https://github.com/segmentio/aws-okta

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install aws-okta with the following command
   ```
   brew install aws-okta
   ```
4. aws-okta is ready to use now!