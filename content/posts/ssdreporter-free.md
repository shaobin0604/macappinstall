---
title: "Install SSDReporter Free on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "SSD health monitoring tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install SSDReporter Free on MacOS using homebrew

- App Name: SSDReporter Free
- App description: SSD health monitoring tool
- App Version: 1.6.0,1575
- App Website: https://www.corecode.io/ssdreporter/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install SSDReporter Free with the following command
   ```
   brew install --cask ssdreporter-free
   ```
4. SSDReporter Free is ready to use now!