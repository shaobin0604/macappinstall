---
title: "Install AccessMenuBarApps on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Instant access for menubar apps"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install AccessMenuBarApps on MacOS using homebrew

- App Name: AccessMenuBarApps
- App description: Instant access for menubar apps
- App Version: 2.6.1,15
- App Website: https://www.ortisoft.de/accessmenubarapps/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install AccessMenuBarApps with the following command
   ```
   brew install --cask accessmenubarapps
   ```
4. AccessMenuBarApps is ready to use now!