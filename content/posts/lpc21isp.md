---
title: "Install lpc21isp on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "In-circuit programming (ISP) tool for several NXP microcontrollers"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install lpc21isp on MacOS using homebrew

- App Name: lpc21isp
- App description: In-circuit programming (ISP) tool for several NXP microcontrollers
- App Version: 1.97
- App Website: https://lpc21isp.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install lpc21isp with the following command
   ```
   brew install lpc21isp
   ```
4. lpc21isp is ready to use now!