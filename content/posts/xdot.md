---
title: "Install xdot on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Interactive viewer for graphs written in Graphviz's dot language"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install xdot on MacOS using homebrew

- App Name: xdot
- App description: Interactive viewer for graphs written in Graphviz's dot language
- App Version: 1.2
- App Website: https://github.com/jrfonseca/xdot.py

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install xdot with the following command
   ```
   brew install xdot
   ```
4. xdot is ready to use now!