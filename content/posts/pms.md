---
title: "Install pms on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Practical Music Search, an ncurses-based MPD client"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pms on MacOS using homebrew

- App Name: pms
- App description: Practical Music Search, an ncurses-based MPD client
- App Version: 0.42
- App Website: https://pms.sourceforge.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pms with the following command
   ```
   brew install pms
   ```
4. pms is ready to use now!