---
title: "Install libofx on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library to support OFX command responses"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libofx on MacOS using homebrew

- App Name: libofx
- App description: Library to support OFX command responses
- App Version: 0.10.3
- App Website: https://libofx.sourceforge.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libofx with the following command
   ```
   brew install libofx
   ```
4. libofx is ready to use now!