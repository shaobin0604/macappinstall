---
title: "Install midgard2 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Generic content repository for web and desktop applications"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install midgard2 on MacOS using homebrew

- App Name: midgard2
- App description: Generic content repository for web and desktop applications
- App Version: 12.09
- App Website: http://www.midgard-project.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install midgard2 with the following command
   ```
   brew install midgard2
   ```
4. midgard2 is ready to use now!