---
title: "Install webhook on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Lightweight, configurable incoming webhook server"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install webhook on MacOS using homebrew

- App Name: webhook
- App description: Lightweight, configurable incoming webhook server
- App Version: 2.8.0
- App Website: https://github.com/adnanh/webhook

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install webhook with the following command
   ```
   brew install webhook
   ```
4. webhook is ready to use now!