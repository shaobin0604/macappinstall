---
title: "Install datafusion on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Apache Arrow DataFusion and Ballista query engines"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install datafusion on MacOS using homebrew

- App Name: datafusion
- App description: Apache Arrow DataFusion and Ballista query engines
- App Version: 7.0.0
- App Website: https://arrow.apache.org/datafusion

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install datafusion with the following command
   ```
   brew install datafusion
   ```
4. datafusion is ready to use now!