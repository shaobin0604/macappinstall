---
title: "Install TunnelBear on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "VPN client for secure internet access and private browsing"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install TunnelBear on MacOS using homebrew

- App Name: TunnelBear
- App description: VPN client for secure internet access and private browsing
- App Version: 4.1.8,1629989300
- App Website: https://www.tunnelbear.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install TunnelBear with the following command
   ```
   brew install --cask tunnelbear
   ```
4. TunnelBear is ready to use now!