---
title: "Install Hammerspoon on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Desktop automation application"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Hammerspoon on MacOS using homebrew

- App Name: Hammerspoon
- App description: Desktop automation application
- App Version: 0.9.93
- App Website: https://www.hammerspoon.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Hammerspoon with the following command
   ```
   brew install --cask hammerspoon
   ```
4. Hammerspoon is ready to use now!