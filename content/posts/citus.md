---
title: "Install citus on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "PostgreSQL-based distributed RDBMS"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install citus on MacOS using homebrew

- App Name: citus
- App description: PostgreSQL-based distributed RDBMS
- App Version: 10.2.4
- App Website: https://www.citusdata.com

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install citus with the following command
   ```
   brew install citus
   ```
4. citus is ready to use now!