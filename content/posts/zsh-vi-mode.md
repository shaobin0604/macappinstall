---
title: "Install zsh-vi-mode on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Better and friendly vi(vim) mode plugin for ZSH"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install zsh-vi-mode on MacOS using homebrew

- App Name: zsh-vi-mode
- App description: Better and friendly vi(vim) mode plugin for ZSH
- App Version: 0.8.5
- App Website: https://github.com/jeffreytse/zsh-vi-mode

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install zsh-vi-mode with the following command
   ```
   brew install zsh-vi-mode
   ```
4. zsh-vi-mode is ready to use now!