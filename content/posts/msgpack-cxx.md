---
title: "Install msgpack-cxx on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "MessagePack implementation for C++ / msgpack.org[C++]"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install msgpack-cxx on MacOS using homebrew

- App Name: msgpack-cxx
- App description: MessagePack implementation for C++ / msgpack.org[C++]
- App Version: 4.0.3
- App Website: https://msgpack.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install msgpack-cxx with the following command
   ```
   brew install msgpack-cxx
   ```
4. msgpack-cxx is ready to use now!