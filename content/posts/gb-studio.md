---
title: "Install GB Studio on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Drag and drop retro game creator"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install GB Studio on MacOS using homebrew

- App Name: GB Studio
- App description: Drag and drop retro game creator
- App Version: 3.0.3
- App Website: https://www.gbstudio.dev/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install GB Studio with the following command
   ```
   brew install --cask gb-studio
   ```
4. GB Studio is ready to use now!