---
title: "Install Alchemy on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Open drawing project"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Alchemy on MacOS using homebrew

- App Name: Alchemy
- App description: Open drawing project
- App Version: 008
- App Website: https://al.chemy.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Alchemy with the following command
   ```
   brew install --cask alchemy
   ```
4. Alchemy is ready to use now!