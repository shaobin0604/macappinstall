---
title: "Install bib-tool on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Manipulates BibTeX databases"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bib-tool on MacOS using homebrew

- App Name: bib-tool
- App description: Manipulates BibTeX databases
- App Version: 2.68
- App Website: http://www.gerd-neugebauer.de/software/TeX/BibTool/en/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bib-tool with the following command
   ```
   brew install bib-tool
   ```
4. bib-tool is ready to use now!