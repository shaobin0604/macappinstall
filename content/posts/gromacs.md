---
title: "Install gromacs on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Versatile package for molecular dynamics calculations"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gromacs on MacOS using homebrew

- App Name: gromacs
- App description: Versatile package for molecular dynamics calculations
- App Version: 2021.5
- App Website: https://www.gromacs.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gromacs with the following command
   ```
   brew install gromacs
   ```
4. gromacs is ready to use now!