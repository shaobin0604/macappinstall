---
title: "Install jmxterm on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Open source, command-line based interactive JMX client"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install jmxterm on MacOS using homebrew

- App Name: jmxterm
- App description: Open source, command-line based interactive JMX client
- App Version: 1.0.2
- App Website: https://docs.cyclopsgroup.org/jmxterm

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install jmxterm with the following command
   ```
   brew install jmxterm
   ```
4. jmxterm is ready to use now!