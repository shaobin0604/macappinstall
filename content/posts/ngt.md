---
title: "Install ngt on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Neighborhood graph and tree for indexing high-dimensional data"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ngt on MacOS using homebrew

- App Name: ngt
- App description: Neighborhood graph and tree for indexing high-dimensional data
- App Version: 1.14.1
- App Website: https://github.com/yahoojapan/NGT

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ngt with the following command
   ```
   brew install ngt
   ```
4. ngt is ready to use now!