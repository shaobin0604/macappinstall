---
title: "Install mac2imgur on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mac2imgur on MacOS using homebrew

- App Name: mac2imgur
- App description: null
- App Version: 226
- App Website: https://github.com/mileswd/mac2imgur

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mac2imgur with the following command
   ```
   brew install --cask mac2imgur
   ```
4. mac2imgur is ready to use now!