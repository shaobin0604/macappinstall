---
title: "Install zsh-async on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Perform tasks asynchronously without external tools"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install zsh-async on MacOS using homebrew

- App Name: zsh-async
- App description: Perform tasks asynchronously without external tools
- App Version: 1.8.5
- App Website: https://github.com/mafredri/zsh-async

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install zsh-async with the following command
   ```
   brew install zsh-async
   ```
4. zsh-async is ready to use now!