---
title: "Install pcb on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Interactive printed circuit board editor"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pcb on MacOS using homebrew

- App Name: pcb
- App description: Interactive printed circuit board editor
- App Version: 4.3.0
- App Website: http://pcb.geda-project.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pcb with the following command
   ```
   brew install pcb
   ```
4. pcb is ready to use now!