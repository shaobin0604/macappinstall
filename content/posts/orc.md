---
title: "Install orc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Oil Runtime Compiler (ORC)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install orc on MacOS using homebrew

- App Name: orc
- App description: Oil Runtime Compiler (ORC)
- App Version: 0.4.32
- App Website: https://gstreamer.freedesktop.org/projects/orc.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install orc with the following command
   ```
   brew install orc
   ```
4. orc is ready to use now!