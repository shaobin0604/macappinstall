---
title: "Install litecli on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CLI for SQLite Databases with auto-completion and syntax highlighting"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install litecli on MacOS using homebrew

- App Name: litecli
- App description: CLI for SQLite Databases with auto-completion and syntax highlighting
- App Version: 1.7.0
- App Website: https://github.com/dbcli/litecli

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install litecli with the following command
   ```
   brew install litecli
   ```
4. litecli is ready to use now!