---
title: "Install gammaray on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Examine and manipulate Qt application internals at runtime"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gammaray on MacOS using homebrew

- App Name: gammaray
- App description: Examine and manipulate Qt application internals at runtime
- App Version: 2.11.3
- App Website: https://www.kdab.com/gammaray

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gammaray with the following command
   ```
   brew install gammaray
   ```
4. gammaray is ready to use now!