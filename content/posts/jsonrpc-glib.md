---
title: "Install jsonrpc-glib on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GNOME library to communicate with JSON-RPC based peers"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install jsonrpc-glib on MacOS using homebrew

- App Name: jsonrpc-glib
- App description: GNOME library to communicate with JSON-RPC based peers
- App Version: 3.40.0
- App Website: https://gitlab.gnome.org/GNOME/jsonrpc-glib

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install jsonrpc-glib with the following command
   ```
   brew install jsonrpc-glib
   ```
4. jsonrpc-glib is ready to use now!