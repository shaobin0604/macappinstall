---
title: "Install Debookee on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Network traffic analyzer"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Debookee on MacOS using homebrew

- App Name: Debookee
- App description: Network traffic analyzer
- App Version: 8.1.1,3238
- App Website: https://debookee.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Debookee with the following command
   ```
   brew install --cask debookee
   ```
4. Debookee is ready to use now!