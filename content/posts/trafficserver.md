---
title: "Install trafficserver on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "HTTP/1.1 compliant caching proxy server"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install trafficserver on MacOS using homebrew

- App Name: trafficserver
- App description: HTTP/1.1 compliant caching proxy server
- App Version: 9.1.1
- App Website: https://trafficserver.apache.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install trafficserver with the following command
   ```
   brew install trafficserver
   ```
4. trafficserver is ready to use now!