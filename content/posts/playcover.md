---
title: "Install PlayCover on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Sideload iOS apps and games"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install PlayCover on MacOS using homebrew

- App Name: PlayCover
- App description: Sideload iOS apps and games
- App Version: 0.9.2
- App Website: https://www.playcover.me/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install PlayCover with the following command
   ```
   brew install --cask playcover
   ```
4. PlayCover is ready to use now!