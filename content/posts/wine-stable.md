---
title: "Install WineHQ-stable on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Compatibility layer to run Windows applications"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install WineHQ-stable on MacOS using homebrew

- App Name: WineHQ-stable
- App description: Compatibility layer to run Windows applications
- App Version: 7.0
- App Website: https://wiki.winehq.org/MacOS

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install WineHQ-stable with the following command
   ```
   brew install --cask wine-stable
   ```
4. WineHQ-stable is ready to use now!