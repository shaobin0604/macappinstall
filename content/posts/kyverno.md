---
title: "Install kyverno on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Kubernetes Native Policy Management"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install kyverno on MacOS using homebrew

- App Name: kyverno
- App description: Kubernetes Native Policy Management
- App Version: 1.6.0
- App Website: https://kyverno.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install kyverno with the following command
   ```
   brew install kyverno
   ```
4. kyverno is ready to use now!