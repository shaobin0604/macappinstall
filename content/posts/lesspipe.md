---
title: "Install lesspipe on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Input filter for the pager less"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install lesspipe on MacOS using homebrew

- App Name: lesspipe
- App description: Input filter for the pager less
- App Version: 2.02
- App Website: https://www-zeuthen.desy.de/~friebel/unix/lesspipe.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install lesspipe with the following command
   ```
   brew install lesspipe
   ```
4. lesspipe is ready to use now!