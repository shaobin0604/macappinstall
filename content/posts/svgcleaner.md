---
title: "Install svgcleaner on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cleans your SVG files by removing unnecessary data"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install svgcleaner on MacOS using homebrew

- App Name: svgcleaner
- App description: Cleans your SVG files by removing unnecessary data
- App Version: 0.9.5
- App Website: https://github.com/RazrFalcon/svgcleaner

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install svgcleaner with the following command
   ```
   brew install svgcleaner
   ```
4. svgcleaner is ready to use now!