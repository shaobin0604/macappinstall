---
title: "Install generate-json-schema on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Generate a JSON Schema from Sample JSON"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install generate-json-schema on MacOS using homebrew

- App Name: generate-json-schema
- App description: Generate a JSON Schema from Sample JSON
- App Version: 2.6.0
- App Website: https://github.com/Nijikokun/generate-schema

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install generate-json-schema with the following command
   ```
   brew install generate-json-schema
   ```
4. generate-json-schema is ready to use now!