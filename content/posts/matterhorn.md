---
title: "Install Matterhorn on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Unix terminal client for Mattermost"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Matterhorn on MacOS using homebrew

- App Name: Matterhorn
- App description: Unix terminal client for Mattermost
- App Version: 50200.15.0
- App Website: https://github.com/matterhorn-chat/matterhorn

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Matterhorn with the following command
   ```
   brew install --cask matterhorn
   ```
4. Matterhorn is ready to use now!