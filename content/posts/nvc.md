---
title: "Install nvc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "VHDL compiler and simulator"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install nvc on MacOS using homebrew

- App Name: nvc
- App description: VHDL compiler and simulator
- App Version: 1.6.1
- App Website: https://github.com/nickg/nvc

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install nvc with the following command
   ```
   brew install nvc
   ```
4. nvc is ready to use now!