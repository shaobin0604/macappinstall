---
title: "Install MP3Gain Express on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install MP3Gain Express on MacOS using homebrew

- App Name: MP3Gain Express
- App description: null
- App Version: 2.5
- App Website: https://projects.sappharad.com/mp3gain/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install MP3Gain Express with the following command
   ```
   brew install --cask mp3gain-express
   ```
4. MP3Gain Express is ready to use now!