---
title: "Install git-svn on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Bidirectional operation between a Subversion repository and Git"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install git-svn on MacOS using homebrew

- App Name: git-svn
- App description: Bidirectional operation between a Subversion repository and Git
- App Version: 2.35.1
- App Website: https://git-scm.com

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install git-svn with the following command
   ```
   brew install git-svn
   ```
4. git-svn is ready to use now!