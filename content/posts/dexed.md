---
title: "Install Dexed on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "DX7 FM synthesizer"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Dexed on MacOS using homebrew

- App Name: Dexed
- App description: DX7 FM synthesizer
- App Version: 0.9.6
- App Website: https://asb2m10.github.io/dexed/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Dexed with the following command
   ```
   brew install --cask dexed
   ```
4. Dexed is ready to use now!