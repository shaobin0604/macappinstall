---
title: "Install points2grid on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Generate digital elevation models using local griding"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install points2grid on MacOS using homebrew

- App Name: points2grid
- App description: Generate digital elevation models using local griding
- App Version: 1.3.1
- App Website: https://github.com/CRREL/points2grid

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install points2grid with the following command
   ```
   brew install points2grid
   ```
4. points2grid is ready to use now!