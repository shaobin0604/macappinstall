---
title: "Install pulumi on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cloud native development platform"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pulumi on MacOS using homebrew

- App Name: pulumi
- App description: Cloud native development platform
- App Version: 3.24.1
- App Website: https://pulumi.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pulumi with the following command
   ```
   brew install pulumi
   ```
4. pulumi is ready to use now!