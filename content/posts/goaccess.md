---
title: "Install goaccess on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Log analyzer and interactive viewer for the Apache Webserver"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install goaccess on MacOS using homebrew

- App Name: goaccess
- App description: Log analyzer and interactive viewer for the Apache Webserver
- App Version: 1.5.5
- App Website: https://goaccess.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install goaccess with the following command
   ```
   brew install goaccess
   ```
4. goaccess is ready to use now!