---
title: "Install swiftgen on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Swift code generator for assets, storyboards, Localizable.strings, …"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install swiftgen on MacOS using homebrew

- App Name: swiftgen
- App description: Swift code generator for assets, storyboards, Localizable.strings, …
- App Version: 6.5.1
- App Website: https://github.com/SwiftGen/SwiftGen

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install swiftgen with the following command
   ```
   brew install swiftgen
   ```
4. swiftgen is ready to use now!