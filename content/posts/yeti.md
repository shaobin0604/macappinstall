---
title: "Install yeti on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "ML-style functional programming language that runs on the JVM"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install yeti on MacOS using homebrew

- App Name: yeti
- App description: ML-style functional programming language that runs on the JVM
- App Version: 1.0
- App Website: https://mth.github.io/yeti/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install yeti with the following command
   ```
   brew install yeti
   ```
4. yeti is ready to use now!