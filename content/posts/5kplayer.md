---
title: "Install 5KPlayer on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Play 4K/1080p/360° video, MP3 AAC APE FLAC music without quality loss"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install 5KPlayer on MacOS using homebrew

- App Name: 5KPlayer
- App description: Play 4K/1080p/360° video, MP3 AAC APE FLAC music without quality loss
- App Version: 6.9.0,5000
- App Website: https://www.5kplayer.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install 5KPlayer with the following command
   ```
   brew install --cask 5kplayer
   ```
4. 5KPlayer is ready to use now!