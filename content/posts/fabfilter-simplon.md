---
title: "Install FabFilter Simplon on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Filter plug-in"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install FabFilter Simplon on MacOS using homebrew

- App Name: FabFilter Simplon
- App description: Filter plug-in
- App Version: 1.33
- App Website: https://www.fabfilter.com/products/simplon-basic-filter-plug-in

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install FabFilter Simplon with the following command
   ```
   brew install --cask fabfilter-simplon
   ```
4. FabFilter Simplon is ready to use now!