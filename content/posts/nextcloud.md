---
title: "Install Nextcloud on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Desktop sync client for Nextcloud software products"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Nextcloud on MacOS using homebrew

- App Name: Nextcloud
- App description: Desktop sync client for Nextcloud software products
- App Version: 3.4.2
- App Website: https://nextcloud.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Nextcloud with the following command
   ```
   brew install --cask nextcloud
   ```
4. Nextcloud is ready to use now!