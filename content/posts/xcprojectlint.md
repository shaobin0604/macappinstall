---
title: "Install xcprojectlint on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Xcode project linter"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install xcprojectlint on MacOS using homebrew

- App Name: xcprojectlint
- App description: Xcode project linter
- App Version: 0.0.6
- App Website: https://github.com/americanexpress/xcprojectlint

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install xcprojectlint with the following command
   ```
   brew install xcprojectlint
   ```
4. xcprojectlint is ready to use now!