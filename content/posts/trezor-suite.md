---
title: "Install TREZOR Suite on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Companion app for the Trezor hardware wallet"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install TREZOR Suite on MacOS using homebrew

- App Name: TREZOR Suite
- App description: Companion app for the Trezor hardware wallet
- App Version: 22.1.1
- App Website: https://suite.trezor.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install TREZOR Suite with the following command
   ```
   brew install --cask trezor-suite
   ```
4. TREZOR Suite is ready to use now!