---
title: "Install libxau on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "X.Org: A Sample Authorization Protocol for X"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libxau on MacOS using homebrew

- App Name: libxau
- App description: X.Org: A Sample Authorization Protocol for X
- App Version: 1.0.9
- App Website: https://www.x.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libxau with the following command
   ```
   brew install libxau
   ```
4. libxau is ready to use now!