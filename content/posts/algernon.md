---
title: "Install algernon on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Pure Go web server with Lua, Markdown, HTTP/2 and template support"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install algernon on MacOS using homebrew

- App Name: algernon
- App description: Pure Go web server with Lua, Markdown, HTTP/2 and template support
- App Version: 1.12.14
- App Website: https://github.com/xyproto/algernon

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install algernon with the following command
   ```
   brew install algernon
   ```
4. algernon is ready to use now!