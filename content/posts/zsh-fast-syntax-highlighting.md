---
title: "Install zsh-fast-syntax-highlighting on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Feature-rich syntax highlighting for Zsh"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install zsh-fast-syntax-highlighting on MacOS using homebrew

- App Name: zsh-fast-syntax-highlighting
- App description: Feature-rich syntax highlighting for Zsh
- App Version: 1.55
- App Website: https://github.com/zdharma-continuum/fast-syntax-highlighting

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install zsh-fast-syntax-highlighting with the following command
   ```
   brew install zsh-fast-syntax-highlighting
   ```
4. zsh-fast-syntax-highlighting is ready to use now!