---
title: "Install jvm-mon on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Console-based JVM monitoring"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install jvm-mon on MacOS using homebrew

- App Name: jvm-mon
- App description: Console-based JVM monitoring
- App Version: 0.3
- App Website: https://github.com/ajermakovics/jvm-mon

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install jvm-mon with the following command
   ```
   brew install jvm-mon
   ```
4. jvm-mon is ready to use now!