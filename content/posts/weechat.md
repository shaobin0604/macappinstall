---
title: "Install weechat on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Extensible IRC client"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install weechat on MacOS using homebrew

- App Name: weechat
- App description: Extensible IRC client
- App Version: 3.4
- App Website: https://www.weechat.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install weechat with the following command
   ```
   brew install weechat
   ```
4. weechat is ready to use now!