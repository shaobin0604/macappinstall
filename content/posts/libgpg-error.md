---
title: "Install libgpg-error on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Common error values for all GnuPG components"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libgpg-error on MacOS using homebrew

- App Name: libgpg-error
- App description: Common error values for all GnuPG components
- App Version: 1.44
- App Website: https://www.gnupg.org/related_software/libgpg-error/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libgpg-error with the following command
   ```
   brew install libgpg-error
   ```
4. libgpg-error is ready to use now!