---
title: "Install zero-install on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Decentralised cross-platform software installation system"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install zero-install on MacOS using homebrew

- App Name: zero-install
- App description: Decentralised cross-platform software installation system
- App Version: 2.17
- App Website: https://0install.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install zero-install with the following command
   ```
   brew install zero-install
   ```
4. zero-install is ready to use now!