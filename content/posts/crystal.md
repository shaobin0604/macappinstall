---
title: "Install crystal on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Fast and statically typed, compiled language with Ruby-like syntax"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install crystal on MacOS using homebrew

- App Name: crystal
- App description: Fast and statically typed, compiled language with Ruby-like syntax
- App Version: 1.3.2
- App Website: https://crystal-lang.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install crystal with the following command
   ```
   brew install crystal
   ```
4. crystal is ready to use now!