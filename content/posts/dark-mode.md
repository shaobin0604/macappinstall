---
title: "Install dark-mode on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Control the macOS dark mode from the command-line"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dark-mode on MacOS using homebrew

- App Name: dark-mode
- App description: Control the macOS dark mode from the command-line
- App Version: 3.0.2
- App Website: https://github.com/sindresorhus/dark-mode

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dark-mode with the following command
   ```
   brew install dark-mode
   ```
4. dark-mode is ready to use now!