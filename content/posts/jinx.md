---
title: "Install jinx on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Embeddable scripting language for real-time applications"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install jinx on MacOS using homebrew

- App Name: jinx
- App description: Embeddable scripting language for real-time applications
- App Version: 1.2.0
- App Website: https://github.com/JamesBoer/Jinx

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install jinx with the following command
   ```
   brew install jinx
   ```
4. jinx is ready to use now!