---
title: "Install VirtualHereServer on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Remotely access your connected USB devices over the network"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install VirtualHereServer on MacOS using homebrew

- App Name: VirtualHereServer
- App description: Remotely access your connected USB devices over the network
- App Version: 4.3.3
- App Website: https://www.virtualhere.com/osx_server_software

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install VirtualHereServer with the following command
   ```
   brew install --cask virtualhereserver
   ```
4. VirtualHereServer is ready to use now!