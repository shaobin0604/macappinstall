---
title: "Install tfsec on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Static analysis security scanner for your terraform code"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tfsec on MacOS using homebrew

- App Name: tfsec
- App description: Static analysis security scanner for your terraform code
- App Version: 1.2.1
- App Website: https://tfsec.dev/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tfsec with the following command
   ```
   brew install tfsec
   ```
4. tfsec is ready to use now!