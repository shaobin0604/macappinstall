---
title: "Install yash on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Yet another shell: a POSIX-compliant command-line shell"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install yash on MacOS using homebrew

- App Name: yash
- App description: Yet another shell: a POSIX-compliant command-line shell
- App Version: 2.52
- App Website: https://yash.osdn.jp/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install yash with the following command
   ```
   brew install yash
   ```
4. yash is ready to use now!