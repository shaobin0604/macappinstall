---
title: "Install urdfdom_headers on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Headers for Unified Robot Description Format (URDF) parsers"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install urdfdom_headers on MacOS using homebrew

- App Name: urdfdom_headers
- App description: Headers for Unified Robot Description Format (URDF) parsers
- App Version: 1.0.5
- App Website: https://wiki.ros.org/urdfdom_headers/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install urdfdom_headers with the following command
   ```
   brew install urdfdom_headers
   ```
4. urdfdom_headers is ready to use now!