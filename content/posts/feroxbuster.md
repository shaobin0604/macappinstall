---
title: "Install feroxbuster on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Fast, simple, recursive content discovery tool written in Rust"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install feroxbuster on MacOS using homebrew

- App Name: feroxbuster
- App description: Fast, simple, recursive content discovery tool written in Rust
- App Version: 2.5.0
- App Website: https://epi052.github.io/feroxbuster

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install feroxbuster with the following command
   ```
   brew install feroxbuster
   ```
4. feroxbuster is ready to use now!