---
title: "Install SameBoy on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Game Boy and Game Boy Color emulator written in C"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install SameBoy on MacOS using homebrew

- App Name: SameBoy
- App description: Game Boy and Game Boy Color emulator written in C
- App Version: 0.14.7
- App Website: https://sameboy.github.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install SameBoy with the following command
   ```
   brew install --cask sameboy
   ```
4. SameBoy is ready to use now!