---
title: "Install git-annex-remote-rclone on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Use rclone supported cloud storage with git-annex"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install git-annex-remote-rclone on MacOS using homebrew

- App Name: git-annex-remote-rclone
- App description: Use rclone supported cloud storage with git-annex
- App Version: 0.6
- App Website: https://github.com/DanielDent/git-annex-remote-rclone

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install git-annex-remote-rclone with the following command
   ```
   brew install git-annex-remote-rclone
   ```
4. git-annex-remote-rclone is ready to use now!