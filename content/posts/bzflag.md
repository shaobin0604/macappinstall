---
title: "Install BZFlag on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "3D multi-player tank battle game"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install BZFlag on MacOS using homebrew

- App Name: BZFlag
- App description: 3D multi-player tank battle game
- App Version: 2.4.22
- App Website: https://www.bzflag.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install BZFlag with the following command
   ```
   brew install --cask bzflag
   ```
4. BZFlag is ready to use now!