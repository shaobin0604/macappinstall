---
title: "Install wren-cli on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Simple REPL and CLI tool for running Wren scripts"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install wren-cli on MacOS using homebrew

- App Name: wren-cli
- App description: Simple REPL and CLI tool for running Wren scripts
- App Version: 0.4.0
- App Website: https://github.com/wren-lang/wren-cli

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install wren-cli with the following command
   ```
   brew install wren-cli
   ```
4. wren-cli is ready to use now!