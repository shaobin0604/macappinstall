---
title: "Install PD Runner on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "VM launcher for Parallels Desktop"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install PD Runner on MacOS using homebrew

- App Name: PD Runner
- App description: VM launcher for Parallels Desktop
- App Version: 0.3.6
- App Website: https://github.com/lihaoyun6/PD-Runner/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install PD Runner with the following command
   ```
   brew install --cask pd-runner
   ```
4. PD Runner is ready to use now!