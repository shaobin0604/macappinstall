---
title: "Install Publii on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Static website generator"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Publii on MacOS using homebrew

- App Name: Publii
- App description: Static website generator
- App Version: 0.38.3
- App Website: https://getpublii.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Publii with the following command
   ```
   brew install --cask publii
   ```
4. Publii is ready to use now!