---
title: "Install sdl2_net on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Small sample cross-platform networking library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sdl2_net on MacOS using homebrew

- App Name: sdl2_net
- App description: Small sample cross-platform networking library
- App Version: 2.0.1
- App Website: https://www.libsdl.org/projects/SDL_net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sdl2_net with the following command
   ```
   brew install sdl2_net
   ```
4. sdl2_net is ready to use now!