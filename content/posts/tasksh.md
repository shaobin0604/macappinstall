---
title: "Install tasksh on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Shell wrapper for Taskwarrior commands"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tasksh on MacOS using homebrew

- App Name: tasksh
- App description: Shell wrapper for Taskwarrior commands
- App Version: 1.2.0
- App Website: https://gothenburgbitfactory.org/projects/tasksh.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tasksh with the following command
   ```
   brew install tasksh
   ```
4. tasksh is ready to use now!