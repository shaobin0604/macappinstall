---
title: "Install flarectl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CLI application for interacting with a Cloudflare account"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install flarectl on MacOS using homebrew

- App Name: flarectl
- App description: CLI application for interacting with a Cloudflare account
- App Version: 0.32.0
- App Website: https://github.com/cloudflare/cloudflare-go/tree/master/cmd/flarectl

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install flarectl with the following command
   ```
   brew install flarectl
   ```
4. flarectl is ready to use now!