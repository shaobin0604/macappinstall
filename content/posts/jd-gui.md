---
title: "Install JD-GUI on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Standalone Java Decompiler GUI"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install JD-GUI on MacOS using homebrew

- App Name: JD-GUI
- App description: Standalone Java Decompiler GUI
- App Version: 1.6.6
- App Website: http://jd.benow.ca/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install JD-GUI with the following command
   ```
   brew install --cask jd-gui
   ```
4. JD-GUI is ready to use now!