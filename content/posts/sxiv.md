---
title: "Install sxiv on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Simple X Image Viewer"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sxiv on MacOS using homebrew

- App Name: sxiv
- App description: Simple X Image Viewer
- App Version: 26
- App Website: https://github.com/muennich/sxiv

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sxiv with the following command
   ```
   brew install sxiv
   ```
4. sxiv is ready to use now!