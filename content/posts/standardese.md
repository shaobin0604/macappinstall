---
title: "Install standardese on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Next-gen documentation generator for C++"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install standardese on MacOS using homebrew

- App Name: standardese
- App description: Next-gen documentation generator for C++
- App Version: 0.5.2
- App Website: https://standardese.github.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install standardese with the following command
   ```
   brew install standardese
   ```
4. standardese is ready to use now!