---
title: "Install biber on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Backend processor for BibLaTeX"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install biber on MacOS using homebrew

- App Name: biber
- App description: Backend processor for BibLaTeX
- App Version: 2.17
- App Website: https://sourceforge.net/projects/biblatex-biber/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install biber with the following command
   ```
   brew install biber
   ```
4. biber is ready to use now!