---
title: "Install clingo on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "ASP system to ground and solve logic programs"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install clingo on MacOS using homebrew

- App Name: clingo
- App description: ASP system to ground and solve logic programs
- App Version: 5.5.1
- App Website: https://potassco.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install clingo with the following command
   ```
   brew install clingo
   ```
4. clingo is ready to use now!