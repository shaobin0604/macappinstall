---
title: "Install hyperkit on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Toolkit for embedding hypervisor capabilities in your application"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install hyperkit on MacOS using homebrew

- App Name: hyperkit
- App description: Toolkit for embedding hypervisor capabilities in your application
- App Version: 0.20200908
- App Website: https://github.com/moby/hyperkit

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install hyperkit with the following command
   ```
   brew install hyperkit
   ```
4. hyperkit is ready to use now!