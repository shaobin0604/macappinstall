---
title: "Install density on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Superfast compression library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install density on MacOS using homebrew

- App Name: density
- App description: Superfast compression library
- App Version: 0.14.2
- App Website: https://github.com/centaurean/density

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install density with the following command
   ```
   brew install density
   ```
4. density is ready to use now!