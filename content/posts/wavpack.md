---
title: "Install wavpack on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Hybrid lossless audio compression"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install wavpack on MacOS using homebrew

- App Name: wavpack
- App description: Hybrid lossless audio compression
- App Version: 5.4.0
- App Website: https://www.wavpack.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install wavpack with the following command
   ```
   brew install wavpack
   ```
4. wavpack is ready to use now!