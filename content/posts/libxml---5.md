---
title: "Install libxml++@5 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C++ wrapper for libxml"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libxml++@5 on MacOS using homebrew

- App Name: libxml++@5
- App description: C++ wrapper for libxml
- App Version: 5.0.1
- App Website: https://libxmlplusplus.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libxml++@5 with the following command
   ```
   brew install libxml++@5
   ```
4. libxml++@5 is ready to use now!