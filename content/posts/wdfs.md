---
title: "Install wdfs on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Webdav file system"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install wdfs on MacOS using homebrew

- App Name: wdfs
- App description: Webdav file system
- App Version: 1.4.2
- App Website: http://noedler.de/projekte/wdfs/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install wdfs with the following command
   ```
   brew install wdfs
   ```
4. wdfs is ready to use now!