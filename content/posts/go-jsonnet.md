---
title: "Install go-jsonnet on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Go implementation of configuration language for defining JSON data"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install go-jsonnet on MacOS using homebrew

- App Name: go-jsonnet
- App description: Go implementation of configuration language for defining JSON data
- App Version: 0.18.0
- App Website: https://jsonnet.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install go-jsonnet with the following command
   ```
   brew install go-jsonnet
   ```
4. go-jsonnet is ready to use now!