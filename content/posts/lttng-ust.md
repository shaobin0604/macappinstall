---
title: "Install lttng-ust on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Linux Trace Toolkit Next Generation Userspace Tracer"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install lttng-ust on MacOS using homebrew

- App Name: lttng-ust
- App description: Linux Trace Toolkit Next Generation Userspace Tracer
- App Version: 2.12.2
- App Website: https://lttng.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install lttng-ust with the following command
   ```
   brew install lttng-ust
   ```
4. lttng-ust is ready to use now!