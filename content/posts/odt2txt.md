---
title: "Install odt2txt on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Convert OpenDocument files to plain text"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install odt2txt on MacOS using homebrew

- App Name: odt2txt
- App description: Convert OpenDocument files to plain text
- App Version: 0.5
- App Website: https://github.com/dstosberg/odt2txt/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install odt2txt with the following command
   ```
   brew install odt2txt
   ```
4. odt2txt is ready to use now!