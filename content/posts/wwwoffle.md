---
title: "Install wwwoffle on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Better browsing for computers with intermittent connections"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install wwwoffle on MacOS using homebrew

- App Name: wwwoffle
- App description: Better browsing for computers with intermittent connections
- App Version: 2.9j
- App Website: https://www.gedanken.org.uk/software/wwwoffle/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install wwwoffle with the following command
   ```
   brew install wwwoffle
   ```
4. wwwoffle is ready to use now!