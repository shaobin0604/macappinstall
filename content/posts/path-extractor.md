---
title: "Install path-extractor on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "UNIX filter which outputs the filepaths found in stdin"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install path-extractor on MacOS using homebrew

- App Name: path-extractor
- App description: UNIX filter which outputs the filepaths found in stdin
- App Version: 0.2.0
- App Website: https://github.com/edi9999/path-extractor

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install path-extractor with the following command
   ```
   brew install path-extractor
   ```
4. path-extractor is ready to use now!