---
title: "Install cloud-watch on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Amazon CloudWatch command-line Tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cloud-watch on MacOS using homebrew

- App Name: cloud-watch
- App description: Amazon CloudWatch command-line Tool
- App Version: 1.0.20.0
- App Website: https://aws.amazon.com/developertools/2534

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cloud-watch with the following command
   ```
   brew install cloud-watch
   ```
4. cloud-watch is ready to use now!