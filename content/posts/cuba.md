---
title: "Install cuba on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library for multidimensional numerical integration"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cuba on MacOS using homebrew

- App Name: cuba
- App description: Library for multidimensional numerical integration
- App Version: 4.2.1
- App Website: http://www.feynarts.de/cuba/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cuba with the following command
   ```
   brew install cuba
   ```
4. cuba is ready to use now!