---
title: "Install sdl_ttf on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library for using TrueType fonts in SDL applications"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sdl_ttf on MacOS using homebrew

- App Name: sdl_ttf
- App description: Library for using TrueType fonts in SDL applications
- App Version: 2.0.11
- App Website: https://www.libsdl.org/projects/SDL_ttf/release-1.2.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sdl_ttf with the following command
   ```
   brew install sdl_ttf
   ```
4. sdl_ttf is ready to use now!