---
title: "Install NeoFinder on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Digital media asset manager"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install NeoFinder on MacOS using homebrew

- App Name: NeoFinder
- App description: Digital media asset manager
- App Version: 8.0
- App Website: https://www.cdfinder.de/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install NeoFinder with the following command
   ```
   brew install --cask neofinder
   ```
4. NeoFinder is ready to use now!