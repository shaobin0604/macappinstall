---
title: "Install pdnsd on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Proxy DNS server with permanent caching"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pdnsd on MacOS using homebrew

- App Name: pdnsd
- App description: Proxy DNS server with permanent caching
- App Version: 1.2.9a-par
- App Website: https://web.archive.org/web/20201203080556/members.home.nl/p.a.rombouts/pdnsd/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pdnsd with the following command
   ```
   brew install pdnsd
   ```
4. pdnsd is ready to use now!