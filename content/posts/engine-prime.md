---
title: "Install Engine Prime on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Music Management Software for Denon's Engine OS Hardware"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Engine Prime on MacOS using homebrew

- App Name: Engine Prime
- App description: Music Management Software for Denon's Engine OS Hardware
- App Version: 1.6.1,5f4b42a70b
- App Website: https://www.denondj.com/engineprime

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Engine Prime with the following command
   ```
   brew install --cask engine-prime
   ```
4. Engine Prime is ready to use now!