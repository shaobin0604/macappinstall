---
title: "Install pinot on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Realtime distributed OLAP datastore"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pinot on MacOS using homebrew

- App Name: pinot
- App description: Realtime distributed OLAP datastore
- App Version: 0.9.3
- App Website: https://pinot.apache.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pinot with the following command
   ```
   brew install pinot
   ```
4. pinot is ready to use now!