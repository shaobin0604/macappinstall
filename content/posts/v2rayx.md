---
title: "Install V2RayX on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GUI for v2ray-core"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install V2RayX on MacOS using homebrew

- App Name: V2RayX
- App description: GUI for v2ray-core
- App Version: 1.5.1
- App Website: https://github.com/Cenmrev/V2RayX

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install V2RayX with the following command
   ```
   brew install --cask v2rayx
   ```
4. V2RayX is ready to use now!