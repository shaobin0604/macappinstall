---
title: "Install braid on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Simple tool to help track vendor branches in a Git repository"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install braid on MacOS using homebrew

- App Name: braid
- App description: Simple tool to help track vendor branches in a Git repository
- App Version: 1.1.3
- App Website: https://cristibalan.github.io/braid/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install braid with the following command
   ```
   brew install braid
   ```
4. braid is ready to use now!