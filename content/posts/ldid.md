---
title: "Install ldid on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Lets you manipulate the signature block in a Mach-O binary"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ldid on MacOS using homebrew

- App Name: ldid
- App description: Lets you manipulate the signature block in a Mach-O binary
- App Version: 2.1.5
- App Website: https://cydia.saurik.com/info/ldid/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ldid with the following command
   ```
   brew install ldid
   ```
4. ldid is ready to use now!