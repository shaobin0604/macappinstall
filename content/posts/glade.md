---
title: "Install glade on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "RAD tool for the GTK+ and GNOME environment"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install glade on MacOS using homebrew

- App Name: glade
- App description: RAD tool for the GTK+ and GNOME environment
- App Version: 3.38.2
- App Website: https://glade.gnome.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install glade with the following command
   ```
   brew install glade
   ```
4. glade is ready to use now!