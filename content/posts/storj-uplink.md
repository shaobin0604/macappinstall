---
title: "Install storj-uplink on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Uplink CLI for the Storj network"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install storj-uplink on MacOS using homebrew

- App Name: storj-uplink
- App description: Uplink CLI for the Storj network
- App Version: 1.35.3
- App Website: https://storj.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install storj-uplink with the following command
   ```
   brew install storj-uplink
   ```
4. storj-uplink is ready to use now!