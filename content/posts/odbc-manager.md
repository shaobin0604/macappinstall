---
title: "Install ODBC Manager on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "ODBC administrator"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ODBC Manager on MacOS using homebrew

- App Name: ODBC Manager
- App description: ODBC administrator
- App Version: 1.0.22
- App Website: http://www.odbcmanager.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ODBC Manager with the following command
   ```
   brew install --cask odbc-manager
   ```
4. ODBC Manager is ready to use now!