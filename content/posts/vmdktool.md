---
title: "Install vmdktool on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Converts raw filesystems to VMDK files and vice versa"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install vmdktool on MacOS using homebrew

- App Name: vmdktool
- App description: Converts raw filesystems to VMDK files and vice versa
- App Version: 1.4
- App Website: https://manned.org/vmdktool

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install vmdktool with the following command
   ```
   brew install vmdktool
   ```
4. vmdktool is ready to use now!