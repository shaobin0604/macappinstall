---
title: "Install Ledger Live on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Wallet desktop application to maintain multiple cryptocurrencies"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Ledger Live on MacOS using homebrew

- App Name: Ledger Live
- App description: Wallet desktop application to maintain multiple cryptocurrencies
- App Version: 2.38.3
- App Website: https://www.ledgerwallet.com/live

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Ledger Live with the following command
   ```
   brew install --cask ledger-live
   ```
4. Ledger Live is ready to use now!