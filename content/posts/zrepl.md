---
title: "Install zrepl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "One-stop ZFS backup & replication solution"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install zrepl on MacOS using homebrew

- App Name: zrepl
- App description: One-stop ZFS backup & replication solution
- App Version: 0.5.0
- App Website: https://zrepl.github.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install zrepl with the following command
   ```
   brew install zrepl
   ```
4. zrepl is ready to use now!