---
title: "Install Mactracker on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Detailed information on every Apple product ever made"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Mactracker on MacOS using homebrew

- App Name: Mactracker
- App description: Detailed information on every Apple product ever made
- App Version: 7.11.1
- App Website: https://mactracker.ca/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Mactracker with the following command
   ```
   brew install --cask mactracker
   ```
4. Mactracker is ready to use now!