---
title: "Install operator-sdk on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "SDK for building Kubernetes applications"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install operator-sdk on MacOS using homebrew

- App Name: operator-sdk
- App description: SDK for building Kubernetes applications
- App Version: 1.17.0
- App Website: https://coreos.com/operators/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install operator-sdk with the following command
   ```
   brew install operator-sdk
   ```
4. operator-sdk is ready to use now!