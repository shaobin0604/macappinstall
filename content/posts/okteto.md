---
title: "Install okteto on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Build better apps by developing and testing code directly in Kubernetes"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install okteto on MacOS using homebrew

- App Name: okteto
- App description: Build better apps by developing and testing code directly in Kubernetes
- App Version: 1.15.4
- App Website: https://okteto.com

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install okteto with the following command
   ```
   brew install okteto
   ```
4. okteto is ready to use now!