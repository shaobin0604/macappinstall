---
title: "Install maven@3.3 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Java-based project management"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install maven@3.3 on MacOS using homebrew

- App Name: maven@3.3
- App description: Java-based project management
- App Version: 3.3.9
- App Website: https://maven.apache.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install maven@3.3 with the following command
   ```
   brew install maven@3.3
   ```
4. maven@3.3 is ready to use now!