---
title: "Install elixir-ls on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Language Server and Debugger for Elixir"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install elixir-ls on MacOS using homebrew

- App Name: elixir-ls
- App description: Language Server and Debugger for Elixir
- App Version: 0.9.0
- App Website: https://elixir-lsp.github.io/elixir-ls

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install elixir-ls with the following command
   ```
   brew install elixir-ls
   ```
4. elixir-ls is ready to use now!