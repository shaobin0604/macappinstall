---
title: "Install getxbook on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tools to download ebooks from various sources"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install getxbook on MacOS using homebrew

- App Name: getxbook
- App description: Tools to download ebooks from various sources
- App Version: 1.2
- App Website: https://njw.name/getxbook/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install getxbook with the following command
   ```
   brew install getxbook
   ```
4. getxbook is ready to use now!