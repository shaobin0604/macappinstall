---
title: "Install dua-cli on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "View disk space usage and delete unwanted data, fast"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dua-cli on MacOS using homebrew

- App Name: dua-cli
- App description: View disk space usage and delete unwanted data, fast
- App Version: 2.17.0
- App Website: https://lib.rs/crates/dua-cli

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dua-cli with the following command
   ```
   brew install dua-cli
   ```
4. dua-cli is ready to use now!