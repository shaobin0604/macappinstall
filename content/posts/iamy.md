---
title: "Install iamy on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "AWS IAM import and export tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install iamy on MacOS using homebrew

- App Name: iamy
- App description: AWS IAM import and export tool
- App Version: 2.4.0
- App Website: https://github.com/99designs/iamy

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install iamy with the following command
   ```
   brew install iamy
   ```
4. iamy is ready to use now!