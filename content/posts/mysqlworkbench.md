---
title: "Install MySQL Workbench on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Visual tool to design, develop and administer MySQL servers"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install MySQL Workbench on MacOS using homebrew

- App Name: MySQL Workbench
- App description: Visual tool to design, develop and administer MySQL servers
- App Version: 8.0.28
- App Website: https://www.mysql.com/products/workbench/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install MySQL Workbench with the following command
   ```
   brew install --cask mysqlworkbench
   ```
4. MySQL Workbench is ready to use now!