---
title: "Install Tencent Meeting on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cloud video conferencing"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Tencent Meeting on MacOS using homebrew

- App Name: Tencent Meeting
- App description: Cloud video conferencing
- App Version: 3.4.3.478,359b6c1a219330c1de533bcc5c24af88
- App Website: https://meeting.tencent.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Tencent Meeting with the following command
   ```
   brew install --cask tencent-meeting
   ```
4. Tencent Meeting is ready to use now!