---
title: "Install HA Menu on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Menu Bar app to perform common Home Assistant functions"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install HA Menu on MacOS using homebrew

- App Name: HA Menu
- App description: Menu Bar app to perform common Home Assistant functions
- App Version: 2.5.1
- App Website: https://github.com/codechimp-org/ha-menu

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install HA Menu with the following command
   ```
   brew install --cask ha-menu
   ```
4. HA Menu is ready to use now!