---
title: "Install fruit on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Dependency injection framework for C++"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fruit on MacOS using homebrew

- App Name: fruit
- App description: Dependency injection framework for C++
- App Version: 3.6.0
- App Website: https://github.com/google/fruit/wiki

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fruit with the following command
   ```
   brew install fruit
   ```
4. fruit is ready to use now!