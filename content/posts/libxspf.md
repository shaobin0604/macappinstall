---
title: "Install libxspf on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C++ library for XSPF playlist reading and writing"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libxspf on MacOS using homebrew

- App Name: libxspf
- App description: C++ library for XSPF playlist reading and writing
- App Version: 1.2.1
- App Website: https://libspiff.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libxspf with the following command
   ```
   brew install libxspf
   ```
4. libxspf is ready to use now!