---
title: "Install wdiff on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Display word differences between text files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install wdiff on MacOS using homebrew

- App Name: wdiff
- App description: Display word differences between text files
- App Version: 1.2.2
- App Website: https://www.gnu.org/software/wdiff/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install wdiff with the following command
   ```
   brew install wdiff
   ```
4. wdiff is ready to use now!