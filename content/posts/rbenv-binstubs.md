---
title: "Install rbenv-binstubs on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Make rbenv aware of bundler binstubs"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install rbenv-binstubs on MacOS using homebrew

- App Name: rbenv-binstubs
- App description: Make rbenv aware of bundler binstubs
- App Version: 1.5
- App Website: https://github.com/ianheggie/rbenv-binstubs

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install rbenv-binstubs with the following command
   ```
   brew install rbenv-binstubs
   ```
4. rbenv-binstubs is ready to use now!