---
title: "Install Qt Design Studio on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "User Interface Design Tools for Applications"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Qt Design Studio on MacOS using homebrew

- App Name: Qt Design Studio
- App description: User Interface Design Tools for Applications
- App Version: 2.3.1
- App Website: https://www.qt.io/product/ui-design-tools

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Qt Design Studio with the following command
   ```
   brew install --cask qt-design-studio
   ```
4. Qt Design Studio is ready to use now!