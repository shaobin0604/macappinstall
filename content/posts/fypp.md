---
title: "Install fypp on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Python powered Fortran preprocessor"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fypp on MacOS using homebrew

- App Name: fypp
- App description: Python powered Fortran preprocessor
- App Version: 3.1
- App Website: https://fypp.readthedocs.io/en/stable/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fypp with the following command
   ```
   brew install fypp
   ```
4. fypp is ready to use now!