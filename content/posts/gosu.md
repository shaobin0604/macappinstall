---
title: "Install gosu on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Pragmatic language for the JVM"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gosu on MacOS using homebrew

- App Name: gosu
- App description: Pragmatic language for the JVM
- App Version: 1.14.20
- App Website: https://gosu-lang.github.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gosu with the following command
   ```
   brew install gosu
   ```
4. gosu is ready to use now!