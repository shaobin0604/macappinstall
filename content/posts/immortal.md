---
title: "Install immortal on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "OS agnostic (*nix) cross-platform supervisor"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install immortal on MacOS using homebrew

- App Name: immortal
- App description: OS agnostic (*nix) cross-platform supervisor
- App Version: 0.24.4
- App Website: https://immortal.run/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install immortal with the following command
   ```
   brew install immortal
   ```
4. immortal is ready to use now!