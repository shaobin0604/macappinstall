---
title: "Install docker-machine-driver-vmware on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "VMware Fusion & Workstation docker-machine driver"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install docker-machine-driver-vmware on MacOS using homebrew

- App Name: docker-machine-driver-vmware
- App description: VMware Fusion & Workstation docker-machine driver
- App Version: 0.1.5
- App Website: https://www.vmware.com/products/personal-desktop-virtualization.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install docker-machine-driver-vmware with the following command
   ```
   brew install docker-machine-driver-vmware
   ```
4. docker-machine-driver-vmware is ready to use now!