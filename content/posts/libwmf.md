---
title: "Install libwmf on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library for converting WMF (Window Metafile Format) files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libwmf on MacOS using homebrew

- App Name: libwmf
- App description: Library for converting WMF (Window Metafile Format) files
- App Version: 0.2.8.4
- App Website: https://wvware.sourceforge.io/libwmf.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libwmf with the following command
   ```
   brew install libwmf
   ```
4. libwmf is ready to use now!