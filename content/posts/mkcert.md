---
title: "Install mkcert on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Simple tool to make locally trusted development certificates"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mkcert on MacOS using homebrew

- App Name: mkcert
- App description: Simple tool to make locally trusted development certificates
- App Version: 1.4.3
- App Website: https://github.com/FiloSottile/mkcert

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mkcert with the following command
   ```
   brew install mkcert
   ```
4. mkcert is ready to use now!