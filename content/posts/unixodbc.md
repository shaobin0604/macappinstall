---
title: "Install unixodbc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "ODBC 3 connectivity for UNIX"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install unixodbc on MacOS using homebrew

- App Name: unixodbc
- App description: ODBC 3 connectivity for UNIX
- App Version: 2.3.9
- App Website: http://www.unixodbc.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install unixodbc with the following command
   ```
   brew install unixodbc
   ```
4. unixodbc is ready to use now!