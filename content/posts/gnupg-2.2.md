---
title: "Install gnupg@2.2 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GNU Pretty Good Privacy (PGP) package"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gnupg@2.2 on MacOS using homebrew

- App Name: gnupg@2.2
- App description: GNU Pretty Good Privacy (PGP) package
- App Version: 2.2.34
- App Website: https://gnupg.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gnupg@2.2 with the following command
   ```
   brew install gnupg@2.2
   ```
4. gnupg@2.2 is ready to use now!