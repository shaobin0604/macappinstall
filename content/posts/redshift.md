---
title: "Install redshift on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Adjust color temperature of your screen according to your surroundings"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install redshift on MacOS using homebrew

- App Name: redshift
- App description: Adjust color temperature of your screen according to your surroundings
- App Version: 1.12
- App Website: http://jonls.dk/redshift/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install redshift with the following command
   ```
   brew install redshift
   ```
4. redshift is ready to use now!