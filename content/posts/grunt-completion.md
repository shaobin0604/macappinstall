---
title: "Install grunt-completion on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Bash and Zsh completion for Grunt"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install grunt-completion on MacOS using homebrew

- App Name: grunt-completion
- App description: Bash and Zsh completion for Grunt
- App Version: 1.4.3
- App Website: https://gruntjs.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install grunt-completion with the following command
   ```
   brew install grunt-completion
   ```
4. grunt-completion is ready to use now!