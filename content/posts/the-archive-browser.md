---
title: "Install The Archive Browser on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Browse the contents of archives"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install The Archive Browser on MacOS using homebrew

- App Name: The Archive Browser
- App description: Browse the contents of archives
- App Version: 1.11.2,111,1504018288
- App Website: https://theunarchiver.com/archive-browser

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install The Archive Browser with the following command
   ```
   brew install --cask the-archive-browser
   ```
4. The Archive Browser is ready to use now!