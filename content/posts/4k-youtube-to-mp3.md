---
title: "Install 4K YouTube to MP3 on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Turn YouTube links into MP3 files"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install 4K YouTube to MP3 on MacOS using homebrew

- App Name: 4K YouTube to MP3
- App description: Turn YouTube links into MP3 files
- App Version: 4.4.3
- App Website: https://www.4kdownload.com/products/youtubetomp3/1

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install 4K YouTube to MP3 with the following command
   ```
   brew install --cask 4k-youtube-to-mp3
   ```
4. 4K YouTube to MP3 is ready to use now!