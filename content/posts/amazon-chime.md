---
title: "Install Amazon Chime on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Communications service"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Amazon Chime on MacOS using homebrew

- App Name: Amazon Chime
- App description: Communications service
- App Version: 4.39.8605
- App Website: https://chime.aws/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Amazon Chime with the following command
   ```
   brew install --cask amazon-chime
   ```
4. Amazon Chime is ready to use now!