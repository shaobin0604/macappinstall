---
title: "Install fsql on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Search through your filesystem with SQL-esque queries"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fsql on MacOS using homebrew

- App Name: fsql
- App description: Search through your filesystem with SQL-esque queries
- App Version: 0.4.0
- App Website: https://github.com/kashav/fsql

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fsql with the following command
   ```
   brew install fsql
   ```
4. fsql is ready to use now!