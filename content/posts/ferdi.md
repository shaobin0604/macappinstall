---
title: "Install Ferdi on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Messaging browser which combines several services"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Ferdi on MacOS using homebrew

- App Name: Ferdi
- App description: Messaging browser which combines several services
- App Version: 5.7.0
- App Website: https://getferdi.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Ferdi with the following command
   ```
   brew install --cask ferdi
   ```
4. Ferdi is ready to use now!