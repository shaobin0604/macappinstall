---
title: "Install What's Your Sign? on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Shows a files cryptographic signing information"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install What's Your Sign? on MacOS using homebrew

- App Name: What's Your Sign?
- App description: Shows a files cryptographic signing information
- App Version: 2.0.1
- App Website: https://objective-see.com/products/whatsyoursign.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install What's Your Sign? with the following command
   ```
   brew install --cask whatsyoursign
   ```
4. What's Your Sign? is ready to use now!