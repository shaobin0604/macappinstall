---
title: "Install rsync on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utility that provides fast incremental file transfer"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install rsync on MacOS using homebrew

- App Name: rsync
- App description: Utility that provides fast incremental file transfer
- App Version: 3.2.3
- App Website: https://rsync.samba.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install rsync with the following command
   ```
   brew install rsync
   ```
4. rsync is ready to use now!