---
title: "Install GOG Galaxy on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Game client"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install GOG Galaxy on MacOS using homebrew

- App Name: GOG Galaxy
- App description: Game client
- App Version: 2.0.44.200
- App Website: https://www.gog.com/galaxy

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install GOG Galaxy with the following command
   ```
   brew install --cask gog-galaxy
   ```
4. GOG Galaxy is ready to use now!