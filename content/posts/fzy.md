---
title: "Install fzy on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Fast, simple fuzzy text selector with an advanced scoring algorithm"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fzy on MacOS using homebrew

- App Name: fzy
- App description: Fast, simple fuzzy text selector with an advanced scoring algorithm
- App Version: 1.0
- App Website: https://github.com/jhawthorn/fzy

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fzy with the following command
   ```
   brew install fzy
   ```
4. fzy is ready to use now!