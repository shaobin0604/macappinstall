---
title: "Install openrct2 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Open source re-implementation of RollerCoaster Tycoon 2"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install openrct2 on MacOS using homebrew

- App Name: openrct2
- App description: Open source re-implementation of RollerCoaster Tycoon 2
- App Version: 0.3.5.1
- App Website: https://openrct2.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install openrct2 with the following command
   ```
   brew install openrct2
   ```
4. openrct2 is ready to use now!