---
title: "Install MacPilot on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Graphical user interface for the command terminal"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install MacPilot on MacOS using homebrew

- App Name: MacPilot
- App description: Graphical user interface for the command terminal
- App Version: 13.0.5
- App Website: https://www.koingosw.com/products/macpilot/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install MacPilot with the following command
   ```
   brew install --cask macpilot
   ```
4. MacPilot is ready to use now!