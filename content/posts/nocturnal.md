---
title: "Install Nocturnal on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Simple app to toggle dark mode with one click"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Nocturnal on MacOS using homebrew

- App Name: Nocturnal
- App description: Simple app to toggle dark mode with one click
- App Version: 0.3
- App Website: https://github.com/HarshilShah/Nocturnal

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Nocturnal with the following command
   ```
   brew install --cask nocturnal
   ```
4. Nocturnal is ready to use now!