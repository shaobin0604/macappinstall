---
title: "Install tailor on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cross-platform static analyzer and linter for Swift"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tailor on MacOS using homebrew

- App Name: tailor
- App description: Cross-platform static analyzer and linter for Swift
- App Version: 0.12.0
- App Website: https://sleekbyte.github.io/tailor/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tailor with the following command
   ```
   brew install tailor
   ```
4. tailor is ready to use now!