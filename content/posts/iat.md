---
title: "Install iat on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Converts many CD-ROM image formats to ISO9660"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install iat on MacOS using homebrew

- App Name: iat
- App description: Converts many CD-ROM image formats to ISO9660
- App Version: 0.1.7
- App Website: https://sourceforge.net/projects/iat.berlios/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install iat with the following command
   ```
   brew install iat
   ```
4. iat is ready to use now!