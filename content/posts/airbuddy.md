---
title: "Install AirBuddy on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "AirPods companion app"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install AirBuddy on MacOS using homebrew

- App Name: AirBuddy
- App description: AirPods companion app
- App Version: 2.5.1,570
- App Website: https://airbuddy.app/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install AirBuddy with the following command
   ```
   brew install --cask airbuddy
   ```
4. AirBuddy is ready to use now!