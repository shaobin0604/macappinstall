---
title: "Install swift-format on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Formatting technology for Swift source code"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install swift-format on MacOS using homebrew

- App Name: swift-format
- App description: Formatting technology for Swift source code
- App Version: 0.50500.0
- App Website: https://github.com/apple/swift-format

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install swift-format with the following command
   ```
   brew install swift-format
   ```
4. swift-format is ready to use now!