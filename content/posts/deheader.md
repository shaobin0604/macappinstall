---
title: "Install deheader on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Analyze C/C++ files for unnecessary headers"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install deheader on MacOS using homebrew

- App Name: deheader
- App description: Analyze C/C++ files for unnecessary headers
- App Version: 1.8
- App Website: http://www.catb.org/~esr/deheader/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install deheader with the following command
   ```
   brew install deheader
   ```
4. deheader is ready to use now!