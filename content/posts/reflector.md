---
title: "Install Reflector on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Wireless screen-mirroring application"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Reflector on MacOS using homebrew

- App Name: Reflector
- App description: Wireless screen-mirroring application
- App Version: 4.0.3,249
- App Website: https://www.airsquirrels.com/reflector/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Reflector with the following command
   ```
   brew install --cask reflector
   ```
4. Reflector is ready to use now!