---
title: "Install yamcha on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "NLP text chunker using Support Vector Machines"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install yamcha on MacOS using homebrew

- App Name: yamcha
- App description: NLP text chunker using Support Vector Machines
- App Version: 0.33
- App Website: http://chasen.org/~taku/software/yamcha/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install yamcha with the following command
   ```
   brew install yamcha
   ```
4. yamcha is ready to use now!