---
title: "Install ksync on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Sync files between your local system and a kubernetes cluster"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ksync on MacOS using homebrew

- App Name: ksync
- App description: Sync files between your local system and a kubernetes cluster
- App Version: 0.4.7
- App Website: https://ksync.github.io/ksync/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ksync with the following command
   ```
   brew install ksync
   ```
4. ksync is ready to use now!