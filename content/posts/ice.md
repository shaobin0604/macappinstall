---
title: "Install ice on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Comprehensive RPC framework"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ice on MacOS using homebrew

- App Name: ice
- App description: Comprehensive RPC framework
- App Version: 3.7.7
- App Website: https://zeroc.com

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ice with the following command
   ```
   brew install ice
   ```
4. ice is ready to use now!