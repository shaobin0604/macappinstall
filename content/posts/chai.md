---
title: "Install Chai on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utility to prevent the system from going to sleep"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Chai on MacOS using homebrew

- App Name: Chai
- App description: Utility to prevent the system from going to sleep
- App Version: 3.2.0
- App Website: https://github.com/lvillani/chai

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Chai with the following command
   ```
   brew install --cask chai
   ```
4. Chai is ready to use now!