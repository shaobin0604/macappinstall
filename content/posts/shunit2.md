---
title: "Install shunit2 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Unit testing framework for Bourne-based shell scripts"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install shunit2 on MacOS using homebrew

- App Name: shunit2
- App description: Unit testing framework for Bourne-based shell scripts
- App Version: 2.1.8
- App Website: https://github.com/kward/shunit2

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install shunit2 with the following command
   ```
   brew install shunit2
   ```
4. shunit2 is ready to use now!