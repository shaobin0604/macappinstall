---
title: "Install Xamarin Mac on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Gives C# and .NET developers access to Objective-C and Swift API's"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Xamarin Mac on MacOS using homebrew

- App Name: Xamarin Mac
- App description: Gives C# and .NET developers access to Objective-C and Swift API's
- App Version: 8.4.0.0
- App Website: https://www.xamarin.com/platform

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Xamarin Mac with the following command
   ```
   brew install --cask xamarin-mac
   ```
4. Xamarin Mac is ready to use now!