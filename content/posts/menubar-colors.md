---
title: "Install Menubar Colors on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Menu bar app for convenient access to the system color panel"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Menubar Colors on MacOS using homebrew

- App Name: Menubar Colors
- App description: Menu bar app for convenient access to the system color panel
- App Version: 2.3.0
- App Website: https://github.com/nvzqz/Menubar-Colors

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Menubar Colors with the following command
   ```
   brew install --cask menubar-colors
   ```
4. Menubar Colors is ready to use now!