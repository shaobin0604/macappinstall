---
title: "Install Typinator on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to automate the insertion of frequently used text and graphics"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Typinator on MacOS using homebrew

- App Name: Typinator
- App description: Tool to automate the insertion of frequently used text and graphics
- App Version: 8.11
- App Website: https://www.ergonis.com/products/typinator/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Typinator with the following command
   ```
   brew install --cask typinator
   ```
4. Typinator is ready to use now!