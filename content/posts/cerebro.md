---
title: "Install Cerebro on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Open-source productivity booster with a brain"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Cerebro on MacOS using homebrew

- App Name: Cerebro
- App description: Open-source productivity booster with a brain
- App Version: 0.3.2
- App Website: https://cerebroapp.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Cerebro with the following command
   ```
   brew install --cask cerebro
   ```
4. Cerebro is ready to use now!