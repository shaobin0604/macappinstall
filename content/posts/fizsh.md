---
title: "Install fizsh on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Fish-like front end for ZSH"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fizsh on MacOS using homebrew

- App Name: fizsh
- App description: Fish-like front end for ZSH
- App Version: 1.0.9
- App Website: https://github.com/zsh-users/fizsh

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fizsh with the following command
   ```
   brew install fizsh
   ```
4. fizsh is ready to use now!