---
title: "Install diffoscope on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "In-depth comparison of files, archives, and directories"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install diffoscope on MacOS using homebrew

- App Name: diffoscope
- App description: In-depth comparison of files, archives, and directories
- App Version: 204
- App Website: https://diffoscope.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install diffoscope with the following command
   ```
   brew install diffoscope
   ```
4. diffoscope is ready to use now!