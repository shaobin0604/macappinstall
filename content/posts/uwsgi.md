---
title: "Install uwsgi on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Full stack for building hosting services"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install uwsgi on MacOS using homebrew

- App Name: uwsgi
- App description: Full stack for building hosting services
- App Version: 2.0.19.1
- App Website: https://uwsgi-docs.readthedocs.io/en/latest/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install uwsgi with the following command
   ```
   brew install uwsgi
   ```
4. uwsgi is ready to use now!