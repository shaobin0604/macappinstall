---
title: "Install libtasn1 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "ASN.1 structure parser library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libtasn1 on MacOS using homebrew

- App Name: libtasn1
- App description: ASN.1 structure parser library
- App Version: 4.18.0
- App Website: https://www.gnu.org/software/libtasn1/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libtasn1 with the following command
   ```
   brew install libtasn1
   ```
4. libtasn1 is ready to use now!