---
title: "Install riff on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Function As A Service on top of Kubernetes, riff is for functions"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install riff on MacOS using homebrew

- App Name: riff
- App description: Function As A Service on top of Kubernetes, riff is for functions
- App Version: 0.5.0
- App Website: https://www.projectriff.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install riff with the following command
   ```
   brew install riff
   ```
4. riff is ready to use now!