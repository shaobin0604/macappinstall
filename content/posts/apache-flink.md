---
title: "Install apache-flink on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Scalable batch and stream data processing"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install apache-flink on MacOS using homebrew

- App Name: apache-flink
- App description: Scalable batch and stream data processing
- App Version: 1.14.3
- App Website: https://flink.apache.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install apache-flink with the following command
   ```
   brew install apache-flink
   ```
4. apache-flink is ready to use now!