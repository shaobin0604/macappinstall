---
title: "Install libass on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Subtitle renderer for the ASS/SSA subtitle format"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libass on MacOS using homebrew

- App Name: libass
- App description: Subtitle renderer for the ASS/SSA subtitle format
- App Version: 0.15.2
- App Website: https://github.com/libass/libass

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libass with the following command
   ```
   brew install libass
   ```
4. libass is ready to use now!