---
title: "Install cpanminus on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Get, unpack, build, and install modules from CPAN"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cpanminus on MacOS using homebrew

- App Name: cpanminus
- App description: Get, unpack, build, and install modules from CPAN
- App Version: 1.9019
- App Website: https://github.com/miyagawa/cpanminus

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cpanminus with the following command
   ```
   brew install cpanminus
   ```
4. cpanminus is ready to use now!