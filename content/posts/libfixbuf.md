---
title: "Install libfixbuf on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Implements the IPFIX Protocol as a C library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libfixbuf on MacOS using homebrew

- App Name: libfixbuf
- App description: Implements the IPFIX Protocol as a C library
- App Version: 2.4.1
- App Website: https://tools.netsa.cert.org/fixbuf/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libfixbuf with the following command
   ```
   brew install libfixbuf
   ```
4. libfixbuf is ready to use now!