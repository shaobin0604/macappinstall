---
title: "Install QQ on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Instant messaging tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install QQ on MacOS using homebrew

- App Name: QQ
- App description: Instant messaging tool
- App Version: 6.7.5.20630_EXP
- App Website: https://im.qq.com/macqq/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install QQ with the following command
   ```
   brew install --cask qq
   ```
4. QQ is ready to use now!