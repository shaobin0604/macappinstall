---
title: "Install links on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Lynx-like WWW browser that supports tables, menus, etc."
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install links on MacOS using homebrew

- App Name: links
- App description: Lynx-like WWW browser that supports tables, menus, etc.
- App Version: 2.25
- App Website: http://links.twibright.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install links with the following command
   ```
   brew install links
   ```
4. links is ready to use now!