---
title: "Install pwncat on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Netcat with FW/IDS/IPS evasion, self-inject-, bind- and reverse shell"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pwncat on MacOS using homebrew

- App Name: pwncat
- App description: Netcat with FW/IDS/IPS evasion, self-inject-, bind- and reverse shell
- App Version: 0.1.2
- App Website: https://pwncat.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pwncat with the following command
   ```
   brew install pwncat
   ```
4. pwncat is ready to use now!