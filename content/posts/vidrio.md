---
title: "Install Vidrio on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Presentation design tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Vidrio on MacOS using homebrew

- App Name: Vidrio
- App description: Presentation design tool
- App Version: 1.23
- App Website: https://vidr.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Vidrio with the following command
   ```
   brew install --cask vidrio
   ```
4. Vidrio is ready to use now!