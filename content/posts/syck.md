---
title: "Install syck on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Extension for reading and writing YAML"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install syck on MacOS using homebrew

- App Name: syck
- App description: Extension for reading and writing YAML
- App Version: 0.70
- App Website: https://github.com/indeyets/syck

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install syck with the following command
   ```
   brew install syck
   ```
4. syck is ready to use now!