---
title: "Install mdxmini on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Plays music in X68000 MDX chiptune format"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mdxmini on MacOS using homebrew

- App Name: mdxmini
- App description: Plays music in X68000 MDX chiptune format
- App Version: 1.0.0
- App Website: https://github.com/mistydemeo/mdxmini/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mdxmini with the following command
   ```
   brew install mdxmini
   ```
4. mdxmini is ready to use now!