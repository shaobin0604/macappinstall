---
title: "Install libwpg on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library for reading and parsing Word Perfect Graphics format"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libwpg on MacOS using homebrew

- App Name: libwpg
- App description: Library for reading and parsing Word Perfect Graphics format
- App Version: 0.3.3
- App Website: https://libwpg.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libwpg with the following command
   ```
   brew install libwpg
   ```
4. libwpg is ready to use now!