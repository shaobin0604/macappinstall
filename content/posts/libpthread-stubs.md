---
title: "Install libpthread-stubs on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "X.Org: pthread-stubs.pc"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libpthread-stubs on MacOS using homebrew

- App Name: libpthread-stubs
- App description: X.Org: pthread-stubs.pc
- App Version: 0.4
- App Website: https://www.x.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libpthread-stubs with the following command
   ```
   brew install libpthread-stubs
   ```
4. libpthread-stubs is ready to use now!