---
title: "Install Warzone 2100 on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Free and open-source real time strategy game"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Warzone 2100 on MacOS using homebrew

- App Name: Warzone 2100
- App description: Free and open-source real time strategy game
- App Version: 4.2.4
- App Website: https://wz2100.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Warzone 2100 with the following command
   ```
   brew install --cask warzone-2100
   ```
4. Warzone 2100 is ready to use now!