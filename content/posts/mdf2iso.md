---
title: "Install mdf2iso on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to convert MDF (Alcohol 120% images) images to ISO images"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mdf2iso on MacOS using homebrew

- App Name: mdf2iso
- App description: Tool to convert MDF (Alcohol 120% images) images to ISO images
- App Version: 0.3.1
- App Website: https://packages.debian.org/sid/mdf2iso

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mdf2iso with the following command
   ```
   brew install mdf2iso
   ```
4. mdf2iso is ready to use now!