---
title: "Install uBar on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Window manager and productivity tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install uBar on MacOS using homebrew

- App Name: uBar
- App description: Window manager and productivity tool
- App Version: 4.2.0,420
- App Website: https://brawersoftware.com/products/ubar

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install uBar with the following command
   ```
   brew install --cask ubar
   ```
4. uBar is ready to use now!