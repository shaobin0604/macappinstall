---
title: "Install runit on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Collection of tools for managing UNIX services"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install runit on MacOS using homebrew

- App Name: runit
- App description: Collection of tools for managing UNIX services
- App Version: 2.1.2
- App Website: http://smarden.org/runit

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install runit with the following command
   ```
   brew install runit
   ```
4. runit is ready to use now!