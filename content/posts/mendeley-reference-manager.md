---
title: "Install Mendeley Reference Manager on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Research management tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Mendeley Reference Manager on MacOS using homebrew

- App Name: Mendeley Reference Manager
- App description: Research management tool
- App Version: 2.64.0
- App Website: https://www.mendeley.com/download-reference-manager/macOS/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Mendeley Reference Manager with the following command
   ```
   brew install --cask mendeley-reference-manager
   ```
4. Mendeley Reference Manager is ready to use now!