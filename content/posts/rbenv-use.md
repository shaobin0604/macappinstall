---
title: "Install rbenv-use on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Switch between rubies without reference to patch levels"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install rbenv-use on MacOS using homebrew

- App Name: rbenv-use
- App description: Switch between rubies without reference to patch levels
- App Version: 1.0.0
- App Website: https://github.com/rkh/rbenv-use

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install rbenv-use with the following command
   ```
   brew install rbenv-use
   ```
4. rbenv-use is ready to use now!