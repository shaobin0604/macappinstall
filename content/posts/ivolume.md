---
title: "Install iVolume on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "App to ensures that all songs are played at the same volume level"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install iVolume on MacOS using homebrew

- App Name: iVolume
- App description: App to ensures that all songs are played at the same volume level
- App Version: 3.9.0,2260
- App Website: https://www.mani.de/en/ivolume/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install iVolume with the following command
   ```
   brew install --cask ivolume
   ```
4. iVolume is ready to use now!