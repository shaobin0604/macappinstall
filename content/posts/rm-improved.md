---
title: "Install rm-improved on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line deletion tool focused on safety, ergonomics, and performance"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install rm-improved on MacOS using homebrew

- App Name: rm-improved
- App description: Command-line deletion tool focused on safety, ergonomics, and performance
- App Version: 0.13.1
- App Website: https://github.com/nivekuil/rip

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install rm-improved with the following command
   ```
   brew install rm-improved
   ```
4. rm-improved is ready to use now!