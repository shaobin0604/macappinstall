---
title: "Install rename on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Perl-powered file rename script with many helpful built-ins"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install rename on MacOS using homebrew

- App Name: rename
- App description: Perl-powered file rename script with many helpful built-ins
- App Version: 1.601
- App Website: http://plasmasturm.org/code/rename

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install rename with the following command
   ```
   brew install rename
   ```
4. rename is ready to use now!