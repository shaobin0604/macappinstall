---
title: "Install lazygit on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Simple terminal UI for git commands"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install lazygit on MacOS using homebrew

- App Name: lazygit
- App description: Simple terminal UI for git commands
- App Version: 0.32.2
- App Website: https://github.com/jesseduffield/lazygit/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install lazygit with the following command
   ```
   brew install lazygit
   ```
4. lazygit is ready to use now!