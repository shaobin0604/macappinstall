---
title: "Install Papyrus on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Model-Based Engineering tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Papyrus on MacOS using homebrew

- App Name: Papyrus
- App description: Model-Based Engineering tool
- App Version: 6.0.0,2021-12
- App Website: https://eclipse.org/papyrus/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Papyrus with the following command
   ```
   brew install --cask papyrus
   ```
4. Papyrus is ready to use now!