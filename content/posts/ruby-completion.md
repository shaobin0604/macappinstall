---
title: "Install ruby-completion on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Bash completion for Ruby"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ruby-completion on MacOS using homebrew

- App Name: ruby-completion
- App description: Bash completion for Ruby
- App Version: 2
- App Website: https://github.com/mernen/completion-ruby

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ruby-completion with the following command
   ```
   brew install ruby-completion
   ```
4. ruby-completion is ready to use now!