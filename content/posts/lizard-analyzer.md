---
title: "Install lizard-analyzer on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Extensible Cyclomatic Complexity Analyzer"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install lizard-analyzer on MacOS using homebrew

- App Name: lizard-analyzer
- App description: Extensible Cyclomatic Complexity Analyzer
- App Version: 1.17.9
- App Website: http://www.lizard.ws

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install lizard-analyzer with the following command
   ```
   brew install lizard-analyzer
   ```
4. lizard-analyzer is ready to use now!