---
title: "Install ccat on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Like cat but displays content with syntax highlighting"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ccat on MacOS using homebrew

- App Name: ccat
- App description: Like cat but displays content with syntax highlighting
- App Version: 1.1.0
- App Website: https://github.com/jingweno/ccat

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ccat with the following command
   ```
   brew install ccat
   ```
4. ccat is ready to use now!