---
title: "Install saxon on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "XSLT and XQuery processor"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install saxon on MacOS using homebrew

- App Name: saxon
- App description: XSLT and XQuery processor
- App Version: 10.6
- App Website: https://saxon.sourceforge.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install saxon with the following command
   ```
   brew install saxon
   ```
4. saxon is ready to use now!