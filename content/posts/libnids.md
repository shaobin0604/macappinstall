---
title: "Install libnids on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Implements E-component of network intrusion detection system"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libnids on MacOS using homebrew

- App Name: libnids
- App description: Implements E-component of network intrusion detection system
- App Version: 1.24
- App Website: https://libnids.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libnids with the following command
   ```
   brew install libnids
   ```
4. libnids is ready to use now!