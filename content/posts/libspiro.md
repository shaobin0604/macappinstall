---
title: "Install libspiro on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library to simplify the drawing of curves"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libspiro on MacOS using homebrew

- App Name: libspiro
- App description: Library to simplify the drawing of curves
- App Version: 20200505
- App Website: https://github.com/fontforge/libspiro

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libspiro with the following command
   ```
   brew install libspiro
   ```
4. libspiro is ready to use now!