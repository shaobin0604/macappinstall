---
title: "Install gammu on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line utility to control a phone"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gammu on MacOS using homebrew

- App Name: gammu
- App description: Command-line utility to control a phone
- App Version: 1.42.0
- App Website: https://wammu.eu/gammu/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gammu with the following command
   ```
   brew install gammu
   ```
4. gammu is ready to use now!