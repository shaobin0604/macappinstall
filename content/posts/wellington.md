---
title: "Install wellington on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Project-focused tool to manage Sass and spriting"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install wellington on MacOS using homebrew

- App Name: wellington
- App description: Project-focused tool to manage Sass and spriting
- App Version: 1.0.5
- App Website: https://github.com/wellington/wellington

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install wellington with the following command
   ```
   brew install wellington
   ```
4. wellington is ready to use now!