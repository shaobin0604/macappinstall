---
title: "Install Rocket Typist on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Rocket Typist on MacOS using homebrew

- App Name: Rocket Typist
- App description: null
- App Version: 2.3,150b
- App Website: https://witt-software.com/rockettypist/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Rocket Typist with the following command
   ```
   brew install --cask rocket-typist
   ```
4. Rocket Typist is ready to use now!