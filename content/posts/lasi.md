---
title: "Install lasi on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C++ stream output interface for creating Postscript documents"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install lasi on MacOS using homebrew

- App Name: lasi
- App description: C++ stream output interface for creating Postscript documents
- App Version: 1.1.3
- App Website: https://www.unifont.org/lasi/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install lasi with the following command
   ```
   brew install lasi
   ```
4. lasi is ready to use now!