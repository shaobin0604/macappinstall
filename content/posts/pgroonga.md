---
title: "Install pgroonga on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "PostgreSQL plugin to use Groonga as index"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pgroonga on MacOS using homebrew

- App Name: pgroonga
- App description: PostgreSQL plugin to use Groonga as index
- App Version: 2.3.4
- App Website: https://pgroonga.github.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pgroonga with the following command
   ```
   brew install pgroonga
   ```
4. pgroonga is ready to use now!