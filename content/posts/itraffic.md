---
title: "Install itraffic on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Monitor for displaying process traffic on status bar"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install itraffic on MacOS using homebrew

- App Name: itraffic
- App description: Monitor for displaying process traffic on status bar
- App Version: 0.1.4
- App Website: https://github.com/foamzou/ITraffic-monitor-for-mac

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install itraffic with the following command
   ```
   brew install --cask itraffic
   ```
4. itraffic is ready to use now!