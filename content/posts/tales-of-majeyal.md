---
title: "Install Tales of Maj'Eyal on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Topdown tactical RPG roguelike game and game engine"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Tales of Maj'Eyal on MacOS using homebrew

- App Name: Tales of Maj'Eyal
- App description: Topdown tactical RPG roguelike game and game engine
- App Version: 1.7.4
- App Website: https://te4.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Tales of Maj'Eyal with the following command
   ```
   brew install --cask tales-of-majeyal
   ```
4. Tales of Maj'Eyal is ready to use now!