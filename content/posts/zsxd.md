---
title: "Install zsxd on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Zelda Mystery of Solarus XD"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install zsxd on MacOS using homebrew

- App Name: zsxd
- App description: Zelda Mystery of Solarus XD
- App Version: 1.12.2
- App Website: https://www.solarus-games.org/en/games/the-legend-of-zelda-mystery-of-solarus-xd

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install zsxd with the following command
   ```
   brew install zsxd
   ```
4. zsxd is ready to use now!