---
title: "Install docuum on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Perform least recently used (LRU) eviction of Docker images"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install docuum on MacOS using homebrew

- App Name: docuum
- App description: Perform least recently used (LRU) eviction of Docker images
- App Version: 0.20.4
- App Website: https://github.com/stepchowfun/docuum

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install docuum with the following command
   ```
   brew install docuum
   ```
4. docuum is ready to use now!