---
title: "Install Pocket Casts on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Podcast platform"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Pocket Casts on MacOS using homebrew

- App Name: Pocket Casts
- App description: Podcast platform
- App Version: 1.5.1,45
- App Website: https://play.pocketcasts.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Pocket Casts with the following command
   ```
   brew install --cask pocket-casts
   ```
4. Pocket Casts is ready to use now!