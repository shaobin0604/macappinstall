---
title: "Install freexl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library to extract data from Excel .xls files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install freexl on MacOS using homebrew

- App Name: freexl
- App description: Library to extract data from Excel .xls files
- App Version: 1.0.6
- App Website: https://www.gaia-gis.it/fossil/freexl/index

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install freexl with the following command
   ```
   brew install freexl
   ```
4. freexl is ready to use now!