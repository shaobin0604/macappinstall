---
title: "Install picard-tools on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tools for manipulating HTS data and formats"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install picard-tools on MacOS using homebrew

- App Name: picard-tools
- App description: Tools for manipulating HTS data and formats
- App Version: 2.26.10
- App Website: https://broadinstitute.github.io/picard/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install picard-tools with the following command
   ```
   brew install picard-tools
   ```
4. picard-tools is ready to use now!