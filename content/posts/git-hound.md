---
title: "Install git-hound on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Git plugin that prevents sensitive data from being committed"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install git-hound on MacOS using homebrew

- App Name: git-hound
- App description: Git plugin that prevents sensitive data from being committed
- App Version: 1.0.0
- App Website: https://github.com/ezekg/git-hound

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install git-hound with the following command
   ```
   brew install git-hound
   ```
4. git-hound is ready to use now!