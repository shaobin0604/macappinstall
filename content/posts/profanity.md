---
title: "Install profanity on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Console based XMPP client"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install profanity on MacOS using homebrew

- App Name: profanity
- App description: Console based XMPP client
- App Version: 0.11.1
- App Website: https://profanity-im.github.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install profanity with the following command
   ```
   brew install profanity
   ```
4. profanity is ready to use now!