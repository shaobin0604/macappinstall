---
title: "Install libmwaw on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library for converting legacy Mac document formats"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libmwaw on MacOS using homebrew

- App Name: libmwaw
- App description: Library for converting legacy Mac document formats
- App Version: 0.3.21
- App Website: https://sourceforge.net/p/libmwaw/wiki/Home/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libmwaw with the following command
   ```
   brew install libmwaw
   ```
4. libmwaw is ready to use now!