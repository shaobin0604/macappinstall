---
title: "Install himalaya on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CLI email client written in Rust"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install himalaya on MacOS using homebrew

- App Name: himalaya
- App description: CLI email client written in Rust
- App Version: 0.5.5
- App Website: https://github.com/soywod/himalaya

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install himalaya with the following command
   ```
   brew install himalaya
   ```
4. himalaya is ready to use now!