---
title: "Install INAV Configurator on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Configuration tool for the INAV flight control system"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install INAV Configurator on MacOS using homebrew

- App Name: INAV Configurator
- App description: Configuration tool for the INAV flight control system
- App Version: 4.0.0
- App Website: https://github.com/iNavFlight/inav-configurator/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install INAV Configurator with the following command
   ```
   brew install --cask inav-configurator
   ```
4. INAV Configurator is ready to use now!