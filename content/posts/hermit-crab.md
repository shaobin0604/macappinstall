---
title: "Install Hermit Crab on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Run shell commands without leaving your current app"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Hermit Crab on MacOS using homebrew

- App Name: Hermit Crab
- App description: Run shell commands without leaving your current app
- App Version: 1.0.3
- App Website: https://belkadan.com/hermitcrab/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Hermit Crab with the following command
   ```
   brew install --cask hermit-crab
   ```
4. Hermit Crab is ready to use now!