---
title: "Install objfw on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Portable, lightweight framework for the Objective-C language"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install objfw on MacOS using homebrew

- App Name: objfw
- App description: Portable, lightweight framework for the Objective-C language
- App Version: 0.90.2
- App Website: https://objfw.nil.im/doc/trunk/README.md

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install objfw with the following command
   ```
   brew install objfw
   ```
4. objfw is ready to use now!