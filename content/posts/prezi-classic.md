---
title: "Install Prezi Classic on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Desktop client for the Prezi presentation SaaS"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Prezi Classic on MacOS using homebrew

- App Name: Prezi Classic
- App description: Desktop client for the Prezi presentation SaaS
- App Version: 6.14.0,24153
- App Website: https://prezi.com/desktop

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Prezi Classic with the following command
   ```
   brew install --cask prezi-classic
   ```
4. Prezi Classic is ready to use now!