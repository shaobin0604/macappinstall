---
title: "Install Billings Pro on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Invoices, estimates, quotes and time-tracking"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Billings Pro on MacOS using homebrew

- App Name: Billings Pro
- App description: Invoices, estimates, quotes and time-tracking
- App Version: 1.7.18,37803
- App Website: https://www.marketcircle.com/billingspro/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Billings Pro with the following command
   ```
   brew install --cask billings-pro
   ```
4. Billings Pro is ready to use now!