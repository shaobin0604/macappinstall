---
title: "Install cabal-install on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line interface for Cabal and Hackage"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cabal-install on MacOS using homebrew

- App Name: cabal-install
- App description: Command-line interface for Cabal and Hackage
- App Version: 3.6.2.0
- App Website: https://www.haskell.org/cabal/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cabal-install with the following command
   ```
   brew install cabal-install
   ```
4. cabal-install is ready to use now!