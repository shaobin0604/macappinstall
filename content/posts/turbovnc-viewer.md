---
title: "Install TurboVNC on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Remote display system"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install TurboVNC on MacOS using homebrew

- App Name: TurboVNC
- App description: Remote display system
- App Version: 2.2.90
- App Website: https://www.turbovnc.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install TurboVNC with the following command
   ```
   brew install --cask turbovnc-viewer
   ```
4. TurboVNC is ready to use now!