---
title: "Install vamp-plugin-sdk on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Audio processing plugin system sdk"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install vamp-plugin-sdk on MacOS using homebrew

- App Name: vamp-plugin-sdk
- App description: Audio processing plugin system sdk
- App Version: 2.10.0
- App Website: https://www.vamp-plugins.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install vamp-plugin-sdk with the following command
   ```
   brew install vamp-plugin-sdk
   ```
4. vamp-plugin-sdk is ready to use now!