---
title: "Install handbrake on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Open-source video transcoder available for Linux, Mac, and Windows"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install handbrake on MacOS using homebrew

- App Name: handbrake
- App description: Open-source video transcoder available for Linux, Mac, and Windows
- App Version: 1.5.1
- App Website: https://handbrake.fr/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install handbrake with the following command
   ```
   brew install handbrake
   ```
4. handbrake is ready to use now!