---
title: "Install git-cinnabar on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Git remote helper to interact with mercurial repositories"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install git-cinnabar on MacOS using homebrew

- App Name: git-cinnabar
- App description: Git remote helper to interact with mercurial repositories
- App Version: 0.5.8
- App Website: https://github.com/glandium/git-cinnabar

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install git-cinnabar with the following command
   ```
   brew install git-cinnabar
   ```
4. git-cinnabar is ready to use now!