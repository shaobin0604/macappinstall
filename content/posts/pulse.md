---
title: "Install Pulse on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Logger and network inspector"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Pulse on MacOS using homebrew

- App Name: Pulse
- App description: Logger and network inspector
- App Version: 0.20.0
- App Website: https://kean.blog/pulse/home

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Pulse with the following command
   ```
   brew install --cask pulse
   ```
4. Pulse is ready to use now!