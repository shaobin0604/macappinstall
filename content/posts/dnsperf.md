---
title: "Install dnsperf on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Measure DNS performance by simulating network conditions"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dnsperf on MacOS using homebrew

- App Name: dnsperf
- App description: Measure DNS performance by simulating network conditions
- App Version: 2.9.0
- App Website: https://www.dns-oarc.net/tools/dnsperf

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dnsperf with the following command
   ```
   brew install dnsperf
   ```
4. dnsperf is ready to use now!