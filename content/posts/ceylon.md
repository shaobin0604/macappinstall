---
title: "Install ceylon on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Programming language for writing large programs in teams"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ceylon on MacOS using homebrew

- App Name: ceylon
- App description: Programming language for writing large programs in teams
- App Version: 1.3.3
- App Website: https://ceylon-lang.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ceylon with the following command
   ```
   brew install ceylon
   ```
4. ceylon is ready to use now!