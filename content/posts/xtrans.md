---
title: "Install xtrans on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "X.Org: X Network Transport layer shared code"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install xtrans on MacOS using homebrew

- App Name: xtrans
- App description: X.Org: X Network Transport layer shared code
- App Version: 1.4.0
- App Website: https://www.x.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install xtrans with the following command
   ```
   brew install xtrans
   ```
4. xtrans is ready to use now!