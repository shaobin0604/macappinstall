---
title: "Install wechat web devtools on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Weixin DevTools for Official Account and Mini Program development"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install wechat web devtools on MacOS using homebrew

- App Name: wechat web devtools
- App description: Weixin DevTools for Official Account and Mini Program development
- App Version: 1.05.2111300
- App Website: https://mp.weixin.qq.com/debug/wxadoc/dev/devtools/download.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install wechat web devtools with the following command
   ```
   brew install --cask wechatwebdevtools
   ```
4. wechat web devtools is ready to use now!