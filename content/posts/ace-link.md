---
title: "Install Ace Link on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Menu bar app that allows playing Ace Stream video streams in the VLC player"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Ace Link on MacOS using homebrew

- App Name: Ace Link
- App description: Menu bar app that allows playing Ace Stream video streams in the VLC player
- App Version: 2.0.1
- App Website: https://github.com/blaise-io/acelink

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Ace Link with the following command
   ```
   brew install --cask ace-link
   ```
4. Ace Link is ready to use now!