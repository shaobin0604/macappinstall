---
title: "Install ccrypt on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Encrypt and decrypt files and streams"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ccrypt on MacOS using homebrew

- App Name: ccrypt
- App description: Encrypt and decrypt files and streams
- App Version: 1.11
- App Website: https://ccrypt.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ccrypt with the following command
   ```
   brew install ccrypt
   ```
4. ccrypt is ready to use now!