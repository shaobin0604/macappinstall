---
title: "Install ptpython on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Advanced Python REPL"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ptpython on MacOS using homebrew

- App Name: ptpython
- App description: Advanced Python REPL
- App Version: 3.0.20
- App Website: https://github.com/prompt-toolkit/ptpython

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ptpython with the following command
   ```
   brew install ptpython
   ```
4. ptpython is ready to use now!