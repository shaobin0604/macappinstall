---
title: "Install Epic Privacy Browser on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Private, secure web browser"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Epic Privacy Browser on MacOS using homebrew

- App Name: Epic Privacy Browser
- App description: Private, secure web browser
- App Version: 91.0.4472.114
- App Website: https://www.epicbrowser.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Epic Privacy Browser with the following command
   ```
   brew install --cask epic
   ```
4. Epic Privacy Browser is ready to use now!