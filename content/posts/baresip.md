---
title: "Install baresip on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Modular SIP useragent"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install baresip on MacOS using homebrew

- App Name: baresip
- App description: Modular SIP useragent
- App Version: 1.1.0
- App Website: https://github.com/baresip/baresip

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install baresip with the following command
   ```
   brew install baresip
   ```
4. baresip is ready to use now!