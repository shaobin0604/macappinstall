---
title: "Install Maltego on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Open source intelligence and graphical link analysis tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Maltego on MacOS using homebrew

- App Name: Maltego
- App description: Open source intelligence and graphical link analysis tool
- App Version: 4.3.0
- App Website: https://www.maltego.com/pricing-plans/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Maltego with the following command
   ```
   brew install --cask maltego
   ```
4. Maltego is ready to use now!