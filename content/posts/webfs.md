---
title: "Install webfs on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "HTTP server for purely static content"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install webfs on MacOS using homebrew

- App Name: webfs
- App description: HTTP server for purely static content
- App Version: 1.21
- App Website: https://linux.bytesex.org/misc/webfs.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install webfs with the following command
   ```
   brew install webfs
   ```
4. webfs is ready to use now!