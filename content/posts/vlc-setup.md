---
title: "Install VLC Remote Setup Helper on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Set up VLC for VLC Remote"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install VLC Remote Setup Helper on MacOS using homebrew

- App Name: VLC Remote Setup Helper
- App description: Set up VLC for VLC Remote
- App Version: 3.70
- App Website: https://hobbyistsoftware.com/VLC

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install VLC Remote Setup Helper with the following command
   ```
   brew install --cask vlc-setup
   ```
4. VLC Remote Setup Helper is ready to use now!