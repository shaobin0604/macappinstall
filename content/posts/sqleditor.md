---
title: "Install SQLEditor on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "SQL database design tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install SQLEditor on MacOS using homebrew

- App Name: SQLEditor
- App description: SQL database design tool
- App Version: 3.7.7,10868
- App Website: https://www.malcolmhardie.com/sqleditor/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install SQLEditor with the following command
   ```
   brew install --cask sqleditor
   ```
4. SQLEditor is ready to use now!