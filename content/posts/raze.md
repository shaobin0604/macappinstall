---
title: "Install Raze on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Build engine port backed by GZDoom tech"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Raze on MacOS using homebrew

- App Name: Raze
- App description: Build engine port backed by GZDoom tech
- App Version: 1.4.1
- App Website: https://github.com/coelckers/Raze

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Raze with the following command
   ```
   brew install --cask raze
   ```
4. Raze is ready to use now!