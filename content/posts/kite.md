---
title: "Install kite on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Programming language designed to minimize programmer experience"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install kite on MacOS using homebrew

- App Name: kite
- App description: Programming language designed to minimize programmer experience
- App Version: 1.0.4
- App Website: https://web.archive.org/web/20200217045713/http://www.kite-language.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install kite with the following command
   ```
   brew install kite
   ```
4. kite is ready to use now!