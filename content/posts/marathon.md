---
title: "Install Marathon on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "First-person shooter, first in a trilogy"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Marathon on MacOS using homebrew

- App Name: Marathon
- App description: First-person shooter, first in a trilogy
- App Version: 20220115
- App Website: https://alephone.lhowon.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Marathon with the following command
   ```
   brew install --cask marathon
   ```
4. Marathon is ready to use now!