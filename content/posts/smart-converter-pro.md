---
title: "Install Smart Converter Pro on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Video converter"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Smart Converter Pro on MacOS using homebrew

- App Name: Smart Converter Pro
- App description: Video converter
- App Version: 3.0.2
- App Website: https://shedworx.com/smart-converter-pro

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Smart Converter Pro with the following command
   ```
   brew install --cask smart-converter-pro
   ```
4. Smart Converter Pro is ready to use now!