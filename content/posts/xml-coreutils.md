---
title: "Install xml-coreutils on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Powerful interactive system for text processing"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install xml-coreutils on MacOS using homebrew

- App Name: xml-coreutils
- App description: Powerful interactive system for text processing
- App Version: 0.8.1
- App Website: https://www.lbreyer.com/xml-coreutils.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install xml-coreutils with the following command
   ```
   brew install xml-coreutils
   ```
4. xml-coreutils is ready to use now!