---
title: "Install exult on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Recreation of Ultima 7"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install exult on MacOS using homebrew

- App Name: exult
- App description: Recreation of Ultima 7
- App Version: 1.6
- App Website: https://exult.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install exult with the following command
   ```
   brew install exult
   ```
4. exult is ready to use now!