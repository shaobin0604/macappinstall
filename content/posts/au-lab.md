---
title: "Install AU Lab on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Digital audio mixing application"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install AU Lab on MacOS using homebrew

- App Name: AU Lab
- App description: Digital audio mixing application
- App Version: 2.3
- App Website: https://www.apple.com/apple-music/apple-digital-masters/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install AU Lab with the following command
   ```
   brew install --cask au-lab
   ```
4. AU Lab is ready to use now!