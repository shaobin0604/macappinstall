---
title: "Install Piezo on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Audio recording application"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Piezo on MacOS using homebrew

- App Name: Piezo
- App description: Audio recording application
- App Version: 1.7.7
- App Website: https://rogueamoeba.com/piezo/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Piezo with the following command
   ```
   brew install --cask piezo
   ```
4. Piezo is ready to use now!