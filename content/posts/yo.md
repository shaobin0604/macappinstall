---
title: "Install Yo Scheduler on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utility to emit Notification Center messages from the command-line"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Yo Scheduler on MacOS using homebrew

- App Name: Yo Scheduler
- App description: Utility to emit Notification Center messages from the command-line
- App Version: 2.0.1
- App Website: https://github.com/sheagcraig/yo

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Yo Scheduler with the following command
   ```
   brew install --cask yo
   ```
4. Yo Scheduler is ready to use now!