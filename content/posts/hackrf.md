---
title: "Install hackrf on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Low cost software radio platform"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install hackrf on MacOS using homebrew

- App Name: hackrf
- App description: Low cost software radio platform
- App Version: 2021.03.1
- App Website: https://github.com/mossmann/hackrf

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install hackrf with the following command
   ```
   brew install hackrf
   ```
4. hackrf is ready to use now!