---
title: "Install cargo-audit on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Audit Cargo.lock files for crates with security vulnerabilities"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cargo-audit on MacOS using homebrew

- App Name: cargo-audit
- App description: Audit Cargo.lock files for crates with security vulnerabilities
- App Version: 0.16.0
- App Website: https://rustsec.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cargo-audit with the following command
   ```
   brew install cargo-audit
   ```
4. cargo-audit is ready to use now!