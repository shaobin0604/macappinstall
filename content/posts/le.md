---
title: "Install le on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Text editor with block and binary operations"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install le on MacOS using homebrew

- App Name: le
- App description: Text editor with block and binary operations
- App Version: 1.16.7
- App Website: https://github.com/lavv17/le

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install le with the following command
   ```
   brew install le
   ```
4. le is ready to use now!