---
title: "Install iCollections on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "App to help keep the desktop organized"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install iCollections on MacOS using homebrew

- App Name: iCollections
- App description: App to help keep the desktop organized
- App Version: 6.8.3,68301
- App Website: https://naarakstudio.com/icollections/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install iCollections with the following command
   ```
   brew install --cask icollections
   ```
4. iCollections is ready to use now!