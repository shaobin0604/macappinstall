---
title: "Install glib-openssl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "OpenSSL GIO module for glib"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install glib-openssl on MacOS using homebrew

- App Name: glib-openssl
- App description: OpenSSL GIO module for glib
- App Version: 2.50.8
- App Website: https://launchpad.net/glib-networking

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install glib-openssl with the following command
   ```
   brew install glib-openssl
   ```
4. glib-openssl is ready to use now!