---
title: "Install OnyX on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Verify system files structure, run miscellaneous maintenance and more"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install OnyX on MacOS using homebrew

- App Name: OnyX
- App description: Verify system files structure, run miscellaneous maintenance and more
- App Version: 4.0.1
- App Website: https://www.titanium-software.fr/en/onyx.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install OnyX with the following command
   ```
   brew install --cask onyx
   ```
4. OnyX is ready to use now!