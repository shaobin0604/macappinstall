---
title: "Install Hacker Menu on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Hacker News Delivered to Desktop :dancers:"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Hacker Menu on MacOS using homebrew

- App Name: Hacker Menu
- App description: Hacker News Delivered to Desktop :dancers:
- App Version: 1.1.5
- App Website: https://hackermenu.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Hacker Menu with the following command
   ```
   brew install --cask hacker-menu
   ```
4. Hacker Menu is ready to use now!