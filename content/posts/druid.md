---
title: "Install druid on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "High-performance, column-oriented, distributed data store"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install druid on MacOS using homebrew

- App Name: druid
- App description: High-performance, column-oriented, distributed data store
- App Version: 0.22.1
- App Website: https://druid.apache.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install druid with the following command
   ```
   brew install druid
   ```
4. druid is ready to use now!