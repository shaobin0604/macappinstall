---
title: "Install enscript on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Convert text to Postscript, HTML, or RTF, with syntax highlighting"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install enscript on MacOS using homebrew

- App Name: enscript
- App description: Convert text to Postscript, HTML, or RTF, with syntax highlighting
- App Version: 1.6.6
- App Website: https://www.gnu.org/software/enscript/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install enscript with the following command
   ```
   brew install enscript
   ```
4. enscript is ready to use now!