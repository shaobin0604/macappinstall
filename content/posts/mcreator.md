---
title: "Install MCreator on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Software used to make Minecraft Java Edition mods"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install MCreator on MacOS using homebrew

- App Name: MCreator
- App description: Software used to make Minecraft Java Edition mods
- App Version: 2021.3.54000
- App Website: https://mcreator.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install MCreator with the following command
   ```
   brew install --cask mcreator
   ```
4. MCreator is ready to use now!