---
title: "Install Vanilla on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to hide menu bar icons"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Vanilla on MacOS using homebrew

- App Name: Vanilla
- App description: Tool to hide menu bar icons
- App Version: 2.0.3,49
- App Website: https://matthewpalmer.net/vanilla/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Vanilla with the following command
   ```
   brew install --cask vanilla
   ```
4. Vanilla is ready to use now!