---
title: "Install sound-touch on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Audio processing library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sound-touch on MacOS using homebrew

- App Name: sound-touch
- App description: Audio processing library
- App Version: 2.3.1
- App Website: https://www.surina.net/soundtouch/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sound-touch with the following command
   ```
   brew install sound-touch
   ```
4. sound-touch is ready to use now!