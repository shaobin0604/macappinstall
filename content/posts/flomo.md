---
title: "Install flomo on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Memo note taking and management app"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install flomo on MacOS using homebrew

- App Name: flomo
- App description: Memo note taking and management app
- App Version: 0.1.4
- App Website: https://flomoapp.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install flomo with the following command
   ```
   brew install --cask flomo
   ```
4. flomo is ready to use now!