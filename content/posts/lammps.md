---
title: "Install lammps on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Molecular Dynamics Simulator"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install lammps on MacOS using homebrew

- App Name: lammps
- App description: Molecular Dynamics Simulator
- App Version: 20210929-update2
- App Website: https://lammps.sandia.gov/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install lammps with the following command
   ```
   brew install lammps
   ```
4. lammps is ready to use now!