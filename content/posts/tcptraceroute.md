---
title: "Install tcptraceroute on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Traceroute implementation using TCP packets"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tcptraceroute on MacOS using homebrew

- App Name: tcptraceroute
- App description: Traceroute implementation using TCP packets
- App Version: 1.5beta7
- App Website: https://github.com/mct/tcptraceroute

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tcptraceroute with the following command
   ```
   brew install tcptraceroute
   ```
4. tcptraceroute is ready to use now!