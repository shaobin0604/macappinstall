---
title: "Install ahcpd on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Autoconfiguration protocol for IPv6 and IPv6/IPv4 networks"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ahcpd on MacOS using homebrew

- App Name: ahcpd
- App description: Autoconfiguration protocol for IPv6 and IPv6/IPv4 networks
- App Version: 0.53
- App Website: https://www.irif.univ-paris-diderot.fr/~jch/software/ahcp/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ahcpd with the following command
   ```
   brew install ahcpd
   ```
4. ahcpd is ready to use now!