---
title: "Install mailutils on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Swiss Army knife of email handling"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mailutils on MacOS using homebrew

- App Name: mailutils
- App description: Swiss Army knife of email handling
- App Version: 3.14
- App Website: https://mailutils.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mailutils with the following command
   ```
   brew install mailutils
   ```
4. mailutils is ready to use now!