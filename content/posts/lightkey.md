---
title: "Install Lightkey on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "DMX lighting control"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Lightkey on MacOS using homebrew

- App Name: Lightkey
- App description: DMX lighting control
- App Version: 3.9,cd7e7cdfcd-1643103796
- App Website: https://lightkeyapp.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Lightkey with the following command
   ```
   brew install --cask lightkey
   ```
4. Lightkey is ready to use now!