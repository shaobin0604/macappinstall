---
title: "Install Glyphfinder on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Character search for designers, developers and writers"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Glyphfinder on MacOS using homebrew

- App Name: Glyphfinder
- App description: Character search for designers, developers and writers
- App Version: 1.4.1
- App Website: https://www.glyphfinder.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Glyphfinder with the following command
   ```
   brew install --cask glyphfinder
   ```
4. Glyphfinder is ready to use now!