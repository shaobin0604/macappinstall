---
title: "Install ripmime on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Extract attachments out of MIME encoded email packages"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ripmime on MacOS using homebrew

- App Name: ripmime
- App description: Extract attachments out of MIME encoded email packages
- App Version: 1.4.0.10
- App Website: https://pldaniels.com/ripmime/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ripmime with the following command
   ```
   brew install ripmime
   ```
4. ripmime is ready to use now!