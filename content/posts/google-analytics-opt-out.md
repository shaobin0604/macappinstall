---
title: "Install Google Analytics Opt Out on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Prevent website visitor's data from being used by Google Analytics JavaScript"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Google Analytics Opt Out on MacOS using homebrew

- App Name: Google Analytics Opt Out
- App description: Prevent website visitor's data from being used by Google Analytics JavaScript
- App Version: 1.0.1
- App Website: https://tools.google.com/dlpage/gaoptout

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Google Analytics Opt Out with the following command
   ```
   brew install --cask google-analytics-opt-out
   ```
4. Google Analytics Opt Out is ready to use now!