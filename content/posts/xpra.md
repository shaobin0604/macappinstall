---
title: "Install Xpra on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Screen and application forwarding system"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Xpra on MacOS using homebrew

- App Name: Xpra
- App description: Screen and application forwarding system
- App Version: 4.3.2,0
- App Website: https://www.xpra.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Xpra with the following command
   ```
   brew install --cask xpra
   ```
4. Xpra is ready to use now!