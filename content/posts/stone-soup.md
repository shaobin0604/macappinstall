---
title: "Install stone-soup on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Dungeon Crawl Stone Soup: a roguelike game"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install stone-soup on MacOS using homebrew

- App Name: stone-soup
- App description: Dungeon Crawl Stone Soup: a roguelike game
- App Version: 0.28.0
- App Website: https://crawl.develz.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install stone-soup with the following command
   ```
   brew install stone-soup
   ```
4. stone-soup is ready to use now!