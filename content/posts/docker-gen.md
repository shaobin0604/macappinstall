---
title: "Install docker-gen on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Generate files from docker container metadata"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install docker-gen on MacOS using homebrew

- App Name: docker-gen
- App description: Generate files from docker container metadata
- App Version: 0.8.2
- App Website: https://github.com/jwilder/docker-gen

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install docker-gen with the following command
   ```
   brew install docker-gen
   ```
4. docker-gen is ready to use now!