---
title: "Install ipbt on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Program for recording a UNIX terminal session"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ipbt on MacOS using homebrew

- App Name: ipbt
- App description: Program for recording a UNIX terminal session
- App Version: 20211203
- App Website: https://www.chiark.greenend.org.uk/~sgtatham/ipbt/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ipbt with the following command
   ```
   brew install ipbt
   ```
4. ipbt is ready to use now!