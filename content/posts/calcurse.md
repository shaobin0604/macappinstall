---
title: "Install calcurse on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Text-based personal organizer"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install calcurse on MacOS using homebrew

- App Name: calcurse
- App description: Text-based personal organizer
- App Version: 4.7.1
- App Website: https://calcurse.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install calcurse with the following command
   ```
   brew install calcurse
   ```
4. calcurse is ready to use now!