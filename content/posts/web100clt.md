---
title: "Install web100clt on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line version of NDT diagnostic client"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install web100clt on MacOS using homebrew

- App Name: web100clt
- App description: Command-line version of NDT diagnostic client
- App Version: 3.7.0.2
- App Website: https://software.internet2.edu/ndt/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install web100clt with the following command
   ```
   brew install web100clt
   ```
4. web100clt is ready to use now!