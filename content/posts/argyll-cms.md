---
title: "Install argyll-cms on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "ICC compatible color management system"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install argyll-cms on MacOS using homebrew

- App Name: argyll-cms
- App description: ICC compatible color management system
- App Version: 2.3.0
- App Website: https://www.argyllcms.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install argyll-cms with the following command
   ```
   brew install argyll-cms
   ```
4. argyll-cms is ready to use now!