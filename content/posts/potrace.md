---
title: "Install potrace on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Convert bitmaps to vector graphics"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install potrace on MacOS using homebrew

- App Name: potrace
- App description: Convert bitmaps to vector graphics
- App Version: 1.16
- App Website: https://potrace.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install potrace with the following command
   ```
   brew install potrace
   ```
4. potrace is ready to use now!