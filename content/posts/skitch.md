---
title: "Install Skitch on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Screen capture tool with mark up and sharing features"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Skitch on MacOS using homebrew

- App Name: Skitch
- App description: Screen capture tool with mark up and sharing features
- App Version: 2.9,265157
- App Website: https://evernote.com/products/skitch

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Skitch with the following command
   ```
   brew install --cask skitch
   ```
4. Skitch is ready to use now!