---
title: "Install gimme on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Shell script to install any Go version"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gimme on MacOS using homebrew

- App Name: gimme
- App description: Shell script to install any Go version
- App Version: 1.5.4
- App Website: https://github.com/travis-ci/gimme

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gimme with the following command
   ```
   brew install gimme
   ```
4. gimme is ready to use now!