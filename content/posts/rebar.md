---
title: "Install rebar on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Erlang build tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install rebar on MacOS using homebrew

- App Name: rebar
- App description: Erlang build tool
- App Version: 2.6.4
- App Website: https://github.com/rebar/rebar

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install rebar with the following command
   ```
   brew install rebar
   ```
4. rebar is ready to use now!