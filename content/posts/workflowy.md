---
title: "Install WorkFlowy on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Notetaking tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install WorkFlowy on MacOS using homebrew

- App Name: WorkFlowy
- App description: Notetaking tool
- App Version: 1.3.7-3060
- App Website: https://workflowy.com/downloads/mac/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install WorkFlowy with the following command
   ```
   brew install --cask workflowy
   ```
4. WorkFlowy is ready to use now!