---
title: "Install Amazon Music on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Desktop client for Amazon Music"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Amazon Music on MacOS using homebrew

- App Name: Amazon Music
- App description: Desktop client for Amazon Music
- App Version: 9.0.1.2313,2313210_06ff83521a054808062972e6c857ce4c
- App Website: https://www.amazon.com/musicapps

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Amazon Music with the following command
   ```
   brew install --cask amazon-music
   ```
4. Amazon Music is ready to use now!