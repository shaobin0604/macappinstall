---
title: "Install Polar FlowSync Software on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Syncing software for Polar Flow products"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Polar FlowSync Software on MacOS using homebrew

- App Name: Polar FlowSync Software
- App description: Syncing software for Polar Flow products
- App Version: 4.0.6
- App Website: https://support.polar.com/uk-en/support/flowsync

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Polar FlowSync Software with the following command
   ```
   brew install --cask flowsync
   ```
4. Polar FlowSync Software is ready to use now!