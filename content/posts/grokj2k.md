---
title: "Install grokj2k on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "JPEG 2000 Library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install grokj2k on MacOS using homebrew

- App Name: grokj2k
- App description: JPEG 2000 Library
- App Version: 9.2.0
- App Website: https://github.com/GrokImageCompression/grok

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install grokj2k with the following command
   ```
   brew install grokj2k
   ```
4. grokj2k is ready to use now!