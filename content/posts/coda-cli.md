---
title: "Install coda-cli on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Shell integration for Panic's Coda"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install coda-cli on MacOS using homebrew

- App Name: coda-cli
- App description: Shell integration for Panic's Coda
- App Version: 1.0.5
- App Website: http://justinhileman.info/coda-cli/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install coda-cli with the following command
   ```
   brew install coda-cli
   ```
4. coda-cli is ready to use now!