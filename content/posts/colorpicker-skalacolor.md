---
title: "Install Skala Color on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Color picker"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Skala Color on MacOS using homebrew

- App Name: Skala Color
- App description: Color picker
- App Version: 2.10
- App Website: https://bjango.com/help/skalacolor/gettingstarted/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Skala Color with the following command
   ```
   brew install --cask colorpicker-skalacolor
   ```
4. Skala Color is ready to use now!