---
title: "Install G Desktop Suite on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install G Desktop Suite on MacOS using homebrew

- App Name: G Desktop Suite
- App description: null
- App Version: 0.3.1
- App Website: https://github.com/alexkim205/g-desktop-suite

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install G Desktop Suite with the following command
   ```
   brew install --cask g-desktop-suite
   ```
4. G Desktop Suite is ready to use now!