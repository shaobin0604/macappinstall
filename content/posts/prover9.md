---
title: "Install prover9 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Automated theorem prover for first-order and equational logic"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install prover9 on MacOS using homebrew

- App Name: prover9
- App description: Automated theorem prover for first-order and equational logic
- App Version: 2009-11A
- App Website: https://www.cs.unm.edu/~mccune/prover9/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install prover9 with the following command
   ```
   brew install prover9
   ```
4. prover9 is ready to use now!