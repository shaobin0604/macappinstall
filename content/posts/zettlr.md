---
title: "Install Zettlr on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Open-source markdown editor"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Zettlr on MacOS using homebrew

- App Name: Zettlr
- App description: Open-source markdown editor
- App Version: 2.2.2
- App Website: https://github.com/Zettlr/Zettlr/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Zettlr with the following command
   ```
   brew install --cask zettlr
   ```
4. Zettlr is ready to use now!