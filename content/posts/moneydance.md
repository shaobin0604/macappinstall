---
title: "Install Moneydance on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Personal financial management application focused on privacy"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Moneydance on MacOS using homebrew

- App Name: Moneydance
- App description: Personal financial management application focused on privacy
- App Version: 2022.2,4060
- App Website: https://infinitekind.com/moneydance

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Moneydance with the following command
   ```
   brew install --cask moneydance
   ```
4. Moneydance is ready to use now!