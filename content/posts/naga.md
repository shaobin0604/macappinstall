---
title: "Install naga on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Terminal implementation of the Snake game"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install naga on MacOS using homebrew

- App Name: naga
- App description: Terminal implementation of the Snake game
- App Version: 1.0
- App Website: https://github.com/anayjoshi/naga/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install naga with the following command
   ```
   brew install naga
   ```
4. naga is ready to use now!