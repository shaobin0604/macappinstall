---
title: "Install StepMania on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Advanced rhythm game designed for both home and arcade use"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install StepMania on MacOS using homebrew

- App Name: StepMania
- App description: Advanced rhythm game designed for both home and arcade use
- App Version: 5.0.12
- App Website: https://www.stepmania.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install StepMania with the following command
   ```
   brew install --cask stepmania
   ```
4. StepMania is ready to use now!