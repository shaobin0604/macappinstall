---
title: "Install osm2pgsql on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "OpenStreetMap data to PostgreSQL converter"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install osm2pgsql on MacOS using homebrew

- App Name: osm2pgsql
- App description: OpenStreetMap data to PostgreSQL converter
- App Version: 1.6.0
- App Website: https://osm2pgsql.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install osm2pgsql with the following command
   ```
   brew install osm2pgsql
   ```
4. osm2pgsql is ready to use now!