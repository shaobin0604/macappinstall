---
title: "Install Shearwater Cloud on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Review, edit and share dive log data"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Shearwater Cloud on MacOS using homebrew

- App Name: Shearwater Cloud
- App description: Review, edit and share dive log data
- App Version: 2.8.2
- App Website: https://www.shearwater.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Shearwater Cloud with the following command
   ```
   brew install --cask shearwater-cloud
   ```
4. Shearwater Cloud is ready to use now!