---
title: "Install clac on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line, stack-based calculator with postfix notation"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install clac on MacOS using homebrew

- App Name: clac
- App description: Command-line, stack-based calculator with postfix notation
- App Version: 0.3.3
- App Website: https://github.com/soveran/clac

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install clac with the following command
   ```
   brew install clac
   ```
4. clac is ready to use now!