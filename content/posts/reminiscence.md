---
title: "Install reminiscence on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Flashback engine reimplementation"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install reminiscence on MacOS using homebrew

- App Name: reminiscence
- App description: Flashback engine reimplementation
- App Version: 0.4.9
- App Website: http://cyxdown.free.fr/reminiscence/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install reminiscence with the following command
   ```
   brew install reminiscence
   ```
4. reminiscence is ready to use now!