---
title: "Install passenger on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Server for Ruby, Python, and Node.js apps via Apache/NGINX"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install passenger on MacOS using homebrew

- App Name: passenger
- App description: Server for Ruby, Python, and Node.js apps via Apache/NGINX
- App Version: 6.0.12
- App Website: https://www.phusionpassenger.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install passenger with the following command
   ```
   brew install passenger
   ```
4. passenger is ready to use now!