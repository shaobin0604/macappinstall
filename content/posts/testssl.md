---
title: "Install testssl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool which checks for the support of TLS/SSL ciphers and flaws"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install testssl on MacOS using homebrew

- App Name: testssl
- App description: Tool which checks for the support of TLS/SSL ciphers and flaws
- App Version: 3.0.6
- App Website: https://testssl.sh/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install testssl with the following command
   ```
   brew install testssl
   ```
4. testssl is ready to use now!