---
title: "Install libseccomp on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Interface to the Linux Kernel's syscall filtering mechanism"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libseccomp on MacOS using homebrew

- App Name: libseccomp
- App description: Interface to the Linux Kernel's syscall filtering mechanism
- App Version: 2.5.3
- App Website: https://github.com/seccomp/libseccomp

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libseccomp with the following command
   ```
   brew install libseccomp
   ```
4. libseccomp is ready to use now!