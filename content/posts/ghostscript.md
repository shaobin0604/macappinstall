---
title: "Install ghostscript on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Interpreter for PostScript and PDF"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ghostscript on MacOS using homebrew

- App Name: ghostscript
- App description: Interpreter for PostScript and PDF
- App Version: 9.55.0
- App Website: https://www.ghostscript.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ghostscript with the following command
   ```
   brew install ghostscript
   ```
4. ghostscript is ready to use now!