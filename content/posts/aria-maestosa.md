---
title: "Install Aria Maestosa on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Midi sequencer and editor"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Aria Maestosa on MacOS using homebrew

- App Name: Aria Maestosa
- App description: Midi sequencer and editor
- App Version: 1.4.13
- App Website: https://ariamaestosa.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Aria Maestosa with the following command
   ```
   brew install --cask aria-maestosa
   ```
4. Aria Maestosa is ready to use now!