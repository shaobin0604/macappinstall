---
title: "Install moz-git-tools on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tools for working with Git at Mozilla"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install moz-git-tools on MacOS using homebrew

- App Name: moz-git-tools
- App description: Tools for working with Git at Mozilla
- App Version: 0.1
- App Website: https://github.com/mozilla/moz-git-tools

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install moz-git-tools with the following command
   ```
   brew install moz-git-tools
   ```
4. moz-git-tools is ready to use now!