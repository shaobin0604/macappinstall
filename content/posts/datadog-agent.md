---
title: "Install Datadog Agent on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Monitoring and security across systems, apps, and services"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Datadog Agent on MacOS using homebrew

- App Name: Datadog Agent
- App description: Monitoring and security across systems, apps, and services
- App Version: 7.33.0-1
- App Website: https://www.datadoghq.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Datadog Agent with the following command
   ```
   brew install --cask datadog-agent
   ```
4. Datadog Agent is ready to use now!