---
title: "Install mailhog on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Web and API based SMTP testing tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mailhog on MacOS using homebrew

- App Name: mailhog
- App description: Web and API based SMTP testing tool
- App Version: 1.0.1
- App Website: https://github.com/mailhog/MailHog

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mailhog with the following command
   ```
   brew install mailhog
   ```
4. mailhog is ready to use now!