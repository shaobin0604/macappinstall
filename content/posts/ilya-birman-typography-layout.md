---
title: "Install Ilya Birman Typography Layout on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Typography keyboard layout"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Ilya Birman Typography Layout on MacOS using homebrew

- App Name: Ilya Birman Typography Layout
- App description: Typography keyboard layout
- App Version: 3.8
- App Website: https://ilyabirman.ru/typography-layout/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Ilya Birman Typography Layout with the following command
   ```
   brew install --cask ilya-birman-typography-layout
   ```
4. Ilya Birman Typography Layout is ready to use now!