---
title: "Install stdman on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Formatted C++ stdlib man pages from cppreference.com"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install stdman on MacOS using homebrew

- App Name: stdman
- App description: Formatted C++ stdlib man pages from cppreference.com
- App Version: 2021.12.21
- App Website: https://github.com/jeaye/stdman

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install stdman with the following command
   ```
   brew install stdman
   ```
4. stdman is ready to use now!