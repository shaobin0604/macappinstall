---
title: "Install MTGA Pro Tracker on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Advanced Magic: The Gathering Arena tracking tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install MTGA Pro Tracker on MacOS using homebrew

- App Name: MTGA Pro Tracker
- App description: Advanced Magic: The Gathering Arena tracking tool
- App Version: 2.1.35
- App Website: https://mtgarena.pro/mtga-pro-tracker/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install MTGA Pro Tracker with the following command
   ```
   brew install --cask mtgaprotracker
   ```
4. MTGA Pro Tracker is ready to use now!