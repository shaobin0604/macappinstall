---
title: "Install flake8 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Lint your Python code for style and logical errors"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install flake8 on MacOS using homebrew

- App Name: flake8
- App description: Lint your Python code for style and logical errors
- App Version: 4.0.1
- App Website: https://flake8.pycqa.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install flake8 with the following command
   ```
   brew install flake8
   ```
4. flake8 is ready to use now!