---
title: "Install Typeface on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Font manager application"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Typeface on MacOS using homebrew

- App Name: Typeface
- App description: Font manager application
- App Version: 3.5.0,3122
- App Website: https://typefaceapp.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Typeface with the following command
   ```
   brew install --cask typeface
   ```
4. Typeface is ready to use now!