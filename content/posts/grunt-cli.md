---
title: "Install grunt-cli on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "JavaScript Task Runner"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install grunt-cli on MacOS using homebrew

- App Name: grunt-cli
- App description: JavaScript Task Runner
- App Version: 1.4.3
- App Website: https://gruntjs.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install grunt-cli with the following command
   ```
   brew install grunt-cli
   ```
4. grunt-cli is ready to use now!