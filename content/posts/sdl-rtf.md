---
title: "Install sdl_rtf on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Sample library to display Rich Text Format (RTF) documents"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sdl_rtf on MacOS using homebrew

- App Name: sdl_rtf
- App description: Sample library to display Rich Text Format (RTF) documents
- App Version: 0.1.0
- App Website: https://www.libsdl.org/projects/SDL_rtf/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sdl_rtf with the following command
   ```
   brew install sdl_rtf
   ```
4. sdl_rtf is ready to use now!