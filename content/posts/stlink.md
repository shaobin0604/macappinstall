---
title: "Install stlink on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "STM32 discovery line Linux programmer"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install stlink on MacOS using homebrew

- App Name: stlink
- App description: STM32 discovery line Linux programmer
- App Version: 1.7.0
- App Website: https://github.com/texane/stlink

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install stlink with the following command
   ```
   brew install stlink
   ```
4. stlink is ready to use now!