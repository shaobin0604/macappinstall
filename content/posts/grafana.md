---
title: "Install grafana on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Gorgeous metric visualizations and dashboards for timeseries databases"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install grafana on MacOS using homebrew

- App Name: grafana
- App description: Gorgeous metric visualizations and dashboards for timeseries databases
- App Version: 8.3.6
- App Website: https://grafana.com

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install grafana with the following command
   ```
   brew install grafana
   ```
4. grafana is ready to use now!