---
title: "Install osquery on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "SQL powered operating system instrumentation and analytics"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install osquery on MacOS using homebrew

- App Name: osquery
- App description: SQL powered operating system instrumentation and analytics
- App Version: 5.1.0
- App Website: https://osquery.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install osquery with the following command
   ```
   brew install --cask osquery
   ```
4. osquery is ready to use now!