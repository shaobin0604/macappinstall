---
title: "Install perl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Highly capable, feature-rich programming language"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install perl on MacOS using homebrew

- App Name: perl
- App description: Highly capable, feature-rich programming language
- App Version: 5.34.0
- App Website: https://www.perl.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install perl with the following command
   ```
   brew install perl
   ```
4. perl is ready to use now!