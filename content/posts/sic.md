---
title: "Install sic on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Minimal multiplexing IRC client"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sic on MacOS using homebrew

- App Name: sic
- App description: Minimal multiplexing IRC client
- App Version: 1.2
- App Website: https://tools.suckless.org/sic/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sic with the following command
   ```
   brew install sic
   ```
4. sic is ready to use now!