---
title: "Install Netron on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Visualizer for neural network, deep learning, and machine learning models"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Netron on MacOS using homebrew

- App Name: Netron
- App description: Visualizer for neural network, deep learning, and machine learning models
- App Version: 5.5.6
- App Website: https://github.com/lutzroeder/netron

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Netron with the following command
   ```
   brew install --cask netron
   ```
4. Netron is ready to use now!