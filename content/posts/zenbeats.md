---
title: "Install Zenbeats on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Music creation app"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Zenbeats on MacOS using homebrew

- App Name: Zenbeats
- App description: Music creation app
- App Version: 2.2.2,8612
- App Website: https://www.roland.com/us/products/zenbeats/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Zenbeats with the following command
   ```
   brew install --cask zenbeats
   ```
4. Zenbeats is ready to use now!