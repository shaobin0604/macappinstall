---
title: "Install pk on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Field extractor command-line utility"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pk on MacOS using homebrew

- App Name: pk
- App description: Field extractor command-line utility
- App Version: 1.0.2
- App Website: https://github.com/johnmorrow/pk

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pk with the following command
   ```
   brew install pk
   ```
4. pk is ready to use now!