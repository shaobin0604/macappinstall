---
title: "Install Eve Online on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "EVE Online client"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Eve Online on MacOS using homebrew

- App Name: Eve Online
- App description: EVE Online client
- App Version: 1990751
- App Website: https://www.eveonline.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Eve Online with the following command
   ```
   brew install --cask eve-launcher
   ```
4. Eve Online is ready to use now!