---
title: "Install VOFA+ on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Serial port console that is driven by the plugin"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install VOFA+ on MacOS using homebrew

- App Name: VOFA+
- App description: Serial port console that is driven by the plugin
- App Version: 1.3.10
- App Website: https://www.vofa.plus/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install VOFA+ with the following command
   ```
   brew install --cask vofa-plus
   ```
4. VOFA+ is ready to use now!