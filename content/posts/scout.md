---
title: "Install Scout-App on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Simple Sass processor"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Scout-App on MacOS using homebrew

- App Name: Scout-App
- App description: Simple Sass processor
- App Version: 2.18.16
- App Website: https://scout-app.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Scout-App with the following command
   ```
   brew install --cask scout
   ```
4. Scout-App is ready to use now!