---
title: "Install pth on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GNU Portable THreads"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pth on MacOS using homebrew

- App Name: pth
- App description: GNU Portable THreads
- App Version: 2.0.7
- App Website: https://www.gnu.org/software/pth/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pth with the following command
   ```
   brew install pth
   ```
4. pth is ready to use now!