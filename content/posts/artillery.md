---
title: "Install artillery on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cloud-native performance & reliability testing for developers and SREs"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install artillery on MacOS using homebrew

- App Name: artillery
- App description: Cloud-native performance & reliability testing for developers and SREs
- App Version: 1.7.9
- App Website: https://artillery.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install artillery with the following command
   ```
   brew install artillery
   ```
4. artillery is ready to use now!