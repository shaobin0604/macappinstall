---
title: "Install cython on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Compiler for writing C extensions for the Python language"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cython on MacOS using homebrew

- App Name: cython
- App description: Compiler for writing C extensions for the Python language
- App Version: 0.29.27
- App Website: https://cython.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cython with the following command
   ```
   brew install cython
   ```
4. cython is ready to use now!