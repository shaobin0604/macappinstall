---
title: "Install ClashX on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Rule-based custom proxy with GUI based on clash"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ClashX on MacOS using homebrew

- App Name: ClashX
- App description: Rule-based custom proxy with GUI based on clash
- App Version: 1.90.0
- App Website: https://github.com/yichengchen/clashX

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ClashX with the following command
   ```
   brew install --cask clashx
   ```
4. ClashX is ready to use now!