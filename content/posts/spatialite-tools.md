---
title: "Install spatialite-tools on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CLI tools supporting SpatiaLite"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install spatialite-tools on MacOS using homebrew

- App Name: spatialite-tools
- App description: CLI tools supporting SpatiaLite
- App Version: 5.0.1
- App Website: https://www.gaia-gis.it/fossil/spatialite-tools/index

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install spatialite-tools with the following command
   ```
   brew install spatialite-tools
   ```
4. spatialite-tools is ready to use now!