---
title: "Install Chrome Remote Desktop on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Remotely access another computer through the Google Chrome browser"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Chrome Remote Desktop on MacOS using homebrew

- App Name: Chrome Remote Desktop
- App description: Remotely access another computer through the Google Chrome browser
- App Version: 89.0.4389.25
- App Website: https://chrome.google.com/webstore/detail/chrome-remote-desktop/inomeogfingihgjfjlpeplalcfajhgai

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Chrome Remote Desktop with the following command
   ```
   brew install --cask chrome-remote-desktop-host
   ```
4. Chrome Remote Desktop is ready to use now!