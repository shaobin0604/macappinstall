---
title: "Install VitalSource Bookshelf on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Access eTextbooks"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install VitalSource Bookshelf on MacOS using homebrew

- App Name: VitalSource Bookshelf
- App description: Access eTextbooks
- App Version: 10.0.2.1377
- App Website: https://www.vitalsource.com/bookshelf-features

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install VitalSource Bookshelf with the following command
   ```
   brew install --cask vitalsource-bookshelf
   ```
4. VitalSource Bookshelf is ready to use now!