---
title: "Install choose-rust on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Human-friendly and fast alternative to cut and (sometimes) awk"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install choose-rust on MacOS using homebrew

- App Name: choose-rust
- App description: Human-friendly and fast alternative to cut and (sometimes) awk
- App Version: 1.3.3
- App Website: https://github.com/theryangeary/choose

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install choose-rust with the following command
   ```
   brew install choose-rust
   ```
4. choose-rust is ready to use now!