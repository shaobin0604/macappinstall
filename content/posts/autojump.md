---
title: "Install autojump on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Shell extension to jump to frequently used directories"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install autojump on MacOS using homebrew

- App Name: autojump
- App description: Shell extension to jump to frequently used directories
- App Version: 22.5.3
- App Website: https://github.com/wting/autojump

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install autojump with the following command
   ```
   brew install autojump
   ```
4. autojump is ready to use now!