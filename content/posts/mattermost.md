---
title: "Install Mattermost on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Open-source, self-hosted Slack-alternative"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Mattermost on MacOS using homebrew

- App Name: Mattermost
- App description: Open-source, self-hosted Slack-alternative
- App Version: 5.0.4
- App Website: https://about.mattermost.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Mattermost with the following command
   ```
   brew install --cask mattermost
   ```
4. Mattermost is ready to use now!