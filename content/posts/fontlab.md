---
title: "Install Fontlab on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Professional font editor"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Fontlab on MacOS using homebrew

- App Name: Fontlab
- App description: Professional font editor
- App Version: 7.2.0.7644
- App Website: https://www.fontlab.com/font-editor/fontlab/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Fontlab with the following command
   ```
   brew install --cask fontlab
   ```
4. Fontlab is ready to use now!