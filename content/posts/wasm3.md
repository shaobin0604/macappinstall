---
title: "Install wasm3 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "High performance WebAssembly interpreter"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install wasm3 on MacOS using homebrew

- App Name: wasm3
- App description: High performance WebAssembly interpreter
- App Version: 0.5.0
- App Website: https://github.com/wasm3/wasm3

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install wasm3 with the following command
   ```
   brew install wasm3
   ```
4. wasm3 is ready to use now!