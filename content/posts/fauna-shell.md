---
title: "Install fauna-shell on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Interactive shell for FaunaDB"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fauna-shell on MacOS using homebrew

- App Name: fauna-shell
- App description: Interactive shell for FaunaDB
- App Version: 0.14.0
- App Website: https://fauna.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fauna-shell with the following command
   ```
   brew install fauna-shell
   ```
4. fauna-shell is ready to use now!