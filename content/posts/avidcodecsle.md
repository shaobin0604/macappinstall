---
title: "Install Avid Codecs LE on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Use QuickTime movies using Avid codecs on systems without Media Composer"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Avid Codecs LE on MacOS using homebrew

- App Name: Avid Codecs LE
- App description: Use QuickTime movies using Avid codecs on systems without Media Composer
- App Version: 2.7.6,3B39AE16
- App Website: https://avid.secure.force.com/pkb/articles/en_US/download/Avid-QuickTime-Codecs-LE

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Avid Codecs LE with the following command
   ```
   brew install --cask avidcodecsle
   ```
4. Avid Codecs LE is ready to use now!