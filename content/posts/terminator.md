---
title: "Install terminator on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Multiple GNOME terminals in one window"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install terminator on MacOS using homebrew

- App Name: terminator
- App description: Multiple GNOME terminals in one window
- App Version: 2.1.1
- App Website: https://gnome-terminator.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install terminator with the following command
   ```
   brew install terminator
   ```
4. terminator is ready to use now!