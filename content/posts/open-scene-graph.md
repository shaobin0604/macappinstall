---
title: "Install open-scene-graph on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "3D graphics toolkit"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install open-scene-graph on MacOS using homebrew

- App Name: open-scene-graph
- App description: 3D graphics toolkit
- App Version: 3.6.5
- App Website: https://github.com/openscenegraph/OpenSceneGraph

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install open-scene-graph with the following command
   ```
   brew install open-scene-graph
   ```
4. open-scene-graph is ready to use now!