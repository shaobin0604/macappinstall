---
title: "Install solr@7.7 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Enterprise search platform from the Apache Lucene project"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install solr@7.7 on MacOS using homebrew

- App Name: solr@7.7
- App description: Enterprise search platform from the Apache Lucene project
- App Version: 7.7.3
- App Website: https://solr.apache.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install solr@7.7 with the following command
   ```
   brew install solr@7.7
   ```
4. solr@7.7 is ready to use now!