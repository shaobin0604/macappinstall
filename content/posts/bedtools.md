---
title: "Install bedtools on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tools for genome arithmetic (set theory on the genome)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bedtools on MacOS using homebrew

- App Name: bedtools
- App description: Tools for genome arithmetic (set theory on the genome)
- App Version: 2.30.0
- App Website: https://github.com/arq5x/bedtools2

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bedtools with the following command
   ```
   brew install bedtools
   ```
4. bedtools is ready to use now!