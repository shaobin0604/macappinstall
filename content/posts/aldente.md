---
title: "Install AlDente on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Menu bar tool to limit maximum charging percentage"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install AlDente on MacOS using homebrew

- App Name: AlDente
- App description: Menu bar tool to limit maximum charging percentage
- App Version: 1.14
- App Website: https://github.com/davidwernhart/AlDente

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install AlDente with the following command
   ```
   brew install --cask aldente
   ```
4. AlDente is ready to use now!