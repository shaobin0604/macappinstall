---
title: "Install LibreELEC USB-SD Creator on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install LibreELEC USB-SD Creator on MacOS using homebrew

- App Name: LibreELEC USB-SD Creator
- App description: null
- App Version: latest
- App Website: https://libreelec.tv/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install LibreELEC USB-SD Creator with the following command
   ```
   brew install --cask libreelec-usb-sd-creator
   ```
4. LibreELEC USB-SD Creator is ready to use now!