---
title: "Install Plain Clip on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Removes formatting from copied text"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Plain Clip on MacOS using homebrew

- App Name: Plain Clip
- App description: Removes formatting from copied text
- App Version: 2.5.2
- App Website: https://www.bluem.net/en/mac/plain-clip/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Plain Clip with the following command
   ```
   brew install --cask plain-clip
   ```
4. Plain Clip is ready to use now!