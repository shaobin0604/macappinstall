---
title: "Install ToolReleases on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utility to notify about the latest Apple tool releases (including Beta releases)"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ToolReleases on MacOS using homebrew

- App Name: ToolReleases
- App description: Utility to notify about the latest Apple tool releases (including Beta releases)
- App Version: 1.4.2,45
- App Website: https://github.com/DeveloperMaris/ToolReleases

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ToolReleases with the following command
   ```
   brew install --cask toolreleases
   ```
4. ToolReleases is ready to use now!