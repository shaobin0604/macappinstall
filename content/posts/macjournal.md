---
title: "Install MacJournal on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Journaling and blogging software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install MacJournal on MacOS using homebrew

- App Name: MacJournal
- App description: Journaling and blogging software
- App Version: 7.3,341
- App Website: https://danschimpf.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install MacJournal with the following command
   ```
   brew install --cask macjournal
   ```
4. MacJournal is ready to use now!