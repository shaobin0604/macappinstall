---
title: "Install SideNotes on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Note-taking application"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install SideNotes on MacOS using homebrew

- App Name: SideNotes
- App description: Note-taking application
- App Version: 1.4.5,179
- App Website: https://www.apptorium.com/sidenotes

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install SideNotes with the following command
   ```
   brew install --cask sidenotes
   ```
4. SideNotes is ready to use now!