---
title: "Install PokerStars on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Free-to-play online poker"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install PokerStars on MacOS using homebrew

- App Name: PokerStars
- App description: Free-to-play online poker
- App Version: 46.372
- App Website: https://www.pokerstars.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install PokerStars with the following command
   ```
   brew install --cask pokerstars
   ```
4. PokerStars is ready to use now!