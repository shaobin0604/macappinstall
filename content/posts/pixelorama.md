---
title: "Install Pixelorama on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "2D sprite editor made with the Godot Engine"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Pixelorama on MacOS using homebrew

- App Name: Pixelorama
- App description: 2D sprite editor made with the Godot Engine
- App Version: 0.9.2
- App Website: https://orama-interactive.itch.io/pixelorama

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Pixelorama with the following command
   ```
   brew install --cask pixelorama
   ```
4. Pixelorama is ready to use now!