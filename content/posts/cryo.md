---
title: "Install Cryo on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Visual file manager"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Cryo on MacOS using homebrew

- App Name: Cryo
- App description: Visual file manager
- App Version: 0.5.22
- App Website: https://cryonet.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Cryo with the following command
   ```
   brew install --cask cryo
   ```
4. Cryo is ready to use now!