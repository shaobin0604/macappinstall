---
title: "Install SmartSVN on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Subversion client"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install SmartSVN on MacOS using homebrew

- App Name: SmartSVN
- App description: Subversion client
- App Version: 14.1.2
- App Website: https://www.smartsvn.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install SmartSVN with the following command
   ```
   brew install --cask smartsvn
   ```
4. SmartSVN is ready to use now!