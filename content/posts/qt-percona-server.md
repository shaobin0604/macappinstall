---
title: "Install qt-percona-server on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Qt SQL Database Driver"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install qt-percona-server on MacOS using homebrew

- App Name: qt-percona-server
- App description: Qt SQL Database Driver
- App Version: 6.2.2
- App Website: https://www.qt.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install qt-percona-server with the following command
   ```
   brew install qt-percona-server
   ```
4. qt-percona-server is ready to use now!