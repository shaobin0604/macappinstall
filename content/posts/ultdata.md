---
title: "Install UltData on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "iPhone data recovery software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install UltData on MacOS using homebrew

- App Name: UltData
- App description: iPhone data recovery software
- App Version: 9.6.2.1
- App Website: https://www.tenorshare.com/products/iphone-data-recovery.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install UltData with the following command
   ```
   brew install --cask ultdata
   ```
4. UltData is ready to use now!