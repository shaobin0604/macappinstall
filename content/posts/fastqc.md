---
title: "Install fastqc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Quality control tool for high throughput sequence data"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fastqc on MacOS using homebrew

- App Name: fastqc
- App description: Quality control tool for high throughput sequence data
- App Version: 0.11.9
- App Website: https://www.bioinformatics.babraham.ac.uk/projects/fastqc/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fastqc with the following command
   ```
   brew install fastqc
   ```
4. fastqc is ready to use now!