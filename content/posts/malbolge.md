---
title: "Install malbolge on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Deliberately difficult to program esoteric programming language"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install malbolge on MacOS using homebrew

- App Name: malbolge
- App description: Deliberately difficult to program esoteric programming language
- App Version: 0.1.0
- App Website: https://esoteric.sange.fi/orphaned/malbolge/README.txt

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install malbolge with the following command
   ```
   brew install malbolge
   ```
4. malbolge is ready to use now!