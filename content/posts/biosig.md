---
title: "Install biosig on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tools for biomedical signal processing and data conversion"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install biosig on MacOS using homebrew

- App Name: biosig
- App description: Tools for biomedical signal processing and data conversion
- App Version: 2.3.3
- App Website: https://biosig.sourceforge.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install biosig with the following command
   ```
   brew install biosig
   ```
4. biosig is ready to use now!