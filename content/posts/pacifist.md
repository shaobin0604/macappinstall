---
title: "Install Pacifist on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Extract files and folders from package files, disk images, and archives"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Pacifist on MacOS using homebrew

- App Name: Pacifist
- App description: Extract files and folders from package files, disk images, and archives
- App Version: 4.0.2,65727
- App Website: https://www.charlessoft.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Pacifist with the following command
   ```
   brew install --cask pacifist
   ```
4. Pacifist is ready to use now!