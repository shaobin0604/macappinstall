---
title: "Install SUBtools on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Helper-application for MP4tools, MKVtools, and AVItools"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install SUBtools on MacOS using homebrew

- App Name: SUBtools
- App description: Helper-application for MP4tools, MKVtools, and AVItools
- App Version: 1.0.1
- App Website: https://www.emmgunn.com/subtools-home/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install SUBtools with the following command
   ```
   brew install --cask subtools
   ```
4. SUBtools is ready to use now!