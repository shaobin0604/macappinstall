---
title: "Install depqbf on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Solver for quantified boolean formulae (QBF)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install depqbf on MacOS using homebrew

- App Name: depqbf
- App description: Solver for quantified boolean formulae (QBF)
- App Version: 6.03
- App Website: https://lonsing.github.io/depqbf/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install depqbf with the following command
   ```
   brew install depqbf
   ```
4. depqbf is ready to use now!