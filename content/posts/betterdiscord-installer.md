---
title: "Install betterdiscord on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Installer for BetterDiscord"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install betterdiscord on MacOS using homebrew

- App Name: betterdiscord
- App description: Installer for BetterDiscord
- App Version: 1.1.1
- App Website: https://betterdiscord.app/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install betterdiscord with the following command
   ```
   brew install --cask betterdiscord-installer
   ```
4. betterdiscord is ready to use now!