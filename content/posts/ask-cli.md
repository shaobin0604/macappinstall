---
title: "Install ask-cli on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CLI tool for Alexa Skill Kit"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ask-cli on MacOS using homebrew

- App Name: ask-cli
- App description: CLI tool for Alexa Skill Kit
- App Version: 2.26.0
- App Website: https://www.npmjs.com/package/ask-cli

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ask-cli with the following command
   ```
   brew install ask-cli
   ```
4. ask-cli is ready to use now!