---
title: "Install menhir on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "LR(1) parser generator for the OCaml programming language"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install menhir on MacOS using homebrew

- App Name: menhir
- App description: LR(1) parser generator for the OCaml programming language
- App Version: 20210929
- App Website: http://cristal.inria.fr/~fpottier/menhir

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install menhir with the following command
   ```
   brew install menhir
   ```
4. menhir is ready to use now!