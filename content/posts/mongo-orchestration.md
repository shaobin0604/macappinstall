---
title: "Install mongo-orchestration on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "REST API to manage MongoDB configurations on a single host"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mongo-orchestration on MacOS using homebrew

- App Name: mongo-orchestration
- App description: REST API to manage MongoDB configurations on a single host
- App Version: 0.7.0
- App Website: https://github.com/10gen/mongo-orchestration

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mongo-orchestration with the following command
   ```
   brew install mongo-orchestration
   ```
4. mongo-orchestration is ready to use now!