---
title: "Install libid3tag on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "ID3 tag manipulation library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libid3tag on MacOS using homebrew

- App Name: libid3tag
- App description: ID3 tag manipulation library
- App Version: 0.16.1
- App Website: https://www.underbit.com/products/mad/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libid3tag with the following command
   ```
   brew install libid3tag
   ```
4. libid3tag is ready to use now!