---
title: "Install wirouter_keyrec on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Recover the default WPA passphrases from supported routers"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install wirouter_keyrec on MacOS using homebrew

- App Name: wirouter_keyrec
- App description: Recover the default WPA passphrases from supported routers
- App Version: 1.1.2
- App Website: https://www.salvatorefresta.net/tools/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install wirouter_keyrec with the following command
   ```
   brew install wirouter_keyrec
   ```
4. wirouter_keyrec is ready to use now!