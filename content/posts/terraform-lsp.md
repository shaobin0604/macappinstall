---
title: "Install terraform-lsp on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Language Server Protocol for Terraform"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install terraform-lsp on MacOS using homebrew

- App Name: terraform-lsp
- App description: Language Server Protocol for Terraform
- App Version: 0.0.12
- App Website: https://github.com/juliosueiras/terraform-lsp

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install terraform-lsp with the following command
   ```
   brew install terraform-lsp
   ```
4. terraform-lsp is ready to use now!