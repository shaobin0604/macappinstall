---
title: "Install anycable-go on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "WebSocket server with action cable protocol"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install anycable-go on MacOS using homebrew

- App Name: anycable-go
- App description: WebSocket server with action cable protocol
- App Version: 1.2.0
- App Website: https://github.com/anycable/anycable-go

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install anycable-go with the following command
   ```
   brew install anycable-go
   ```
4. anycable-go is ready to use now!