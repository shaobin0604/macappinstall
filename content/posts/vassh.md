---
title: "Install vassh on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Vagrant Host-Guest SSH Command Wrapper/Proxy/Forwarder"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install vassh on MacOS using homebrew

- App Name: vassh
- App description: Vagrant Host-Guest SSH Command Wrapper/Proxy/Forwarder
- App Version: 0.2
- App Website: https://github.com/x-team/vassh

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install vassh with the following command
   ```
   brew install vassh
   ```
4. vassh is ready to use now!