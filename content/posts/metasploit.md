---
title: "Install Metasploit Framework on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Penetration testing framework"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Metasploit Framework on MacOS using homebrew

- App Name: Metasploit Framework
- App description: Penetration testing framework
- App Version: 6.1.29,20220210113018
- App Website: https://www.metasploit.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Metasploit Framework with the following command
   ```
   brew install --cask metasploit
   ```
4. Metasploit Framework is ready to use now!