---
title: "Install Evernote on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Note taking app"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Evernote on MacOS using homebrew

- App Name: Evernote
- App description: Note taking app
- App Version: 9.5.18_466205
- App Website: https://www.yinxiang.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Evernote with the following command
   ```
   brew install --cask yinxiangbiji
   ```
4. Evernote is ready to use now!