---
title: "Install py-spy on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Sampling profiler for Python programs"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install py-spy on MacOS using homebrew

- App Name: py-spy
- App description: Sampling profiler for Python programs
- App Version: 0.3.11
- App Website: https://github.com/benfred/py-spy

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install py-spy with the following command
   ```
   brew install py-spy
   ```
4. py-spy is ready to use now!