---
title: "Install func-e on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Easily run Envoy"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install func-e on MacOS using homebrew

- App Name: func-e
- App description: Easily run Envoy
- App Version: 1.1.2
- App Website: https://func-e.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install func-e with the following command
   ```
   brew install func-e
   ```
4. func-e is ready to use now!