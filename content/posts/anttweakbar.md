---
title: "Install anttweakbar on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C/C++ library for adding GUIs to OpenGL apps"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install anttweakbar on MacOS using homebrew

- App Name: anttweakbar
- App description: C/C++ library for adding GUIs to OpenGL apps
- App Version: 1.16
- App Website: https://anttweakbar.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install anttweakbar with the following command
   ```
   brew install anttweakbar
   ```
4. anttweakbar is ready to use now!