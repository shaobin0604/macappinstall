---
title: "Install genstats on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Generate statistics about stdin or textfiles"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install genstats on MacOS using homebrew

- App Name: genstats
- App description: Generate statistics about stdin or textfiles
- App Version: 1.2
- App Website: https://www.vanheusden.com/genstats/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install genstats with the following command
   ```
   brew install genstats
   ```
4. genstats is ready to use now!