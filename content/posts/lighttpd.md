---
title: "Install lighttpd on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Small memory footprint, flexible web-server"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install lighttpd on MacOS using homebrew

- App Name: lighttpd
- App description: Small memory footprint, flexible web-server
- App Version: 1.4.64
- App Website: https://www.lighttpd.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install lighttpd with the following command
   ```
   brew install lighttpd
   ```
4. lighttpd is ready to use now!