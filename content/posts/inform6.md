---
title: "Install inform6 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Design system for interactive fiction"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install inform6 on MacOS using homebrew

- App Name: inform6
- App description: Design system for interactive fiction
- App Version: 6.35-r8
- App Website: https://inform-fiction.org/inform6.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install inform6 with the following command
   ```
   brew install inform6
   ```
4. inform6 is ready to use now!