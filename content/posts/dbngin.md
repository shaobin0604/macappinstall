---
title: "Install DBngin on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Database version management tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install DBngin on MacOS using homebrew

- App Name: DBngin
- App description: Database version management tool
- App Version: 4.0,42
- App Website: https://dbngin.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install DBngin with the following command
   ```
   brew install --cask dbngin
   ```
4. DBngin is ready to use now!