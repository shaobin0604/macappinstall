---
title: "Install Displays on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Monitor resolution and settings manager"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Displays on MacOS using homebrew

- App Name: Displays
- App description: Monitor resolution and settings manager
- App Version: 1.9.10,120
- App Website: https://www.jibapps.com/apps/displays/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Displays with the following command
   ```
   brew install --cask displays
   ```
4. Displays is ready to use now!