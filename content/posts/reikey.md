---
title: "Install ReiKey on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Scans, detects, and monitors keyboard taps"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ReiKey on MacOS using homebrew

- App Name: ReiKey
- App description: Scans, detects, and monitors keyboard taps
- App Version: 1.4.2
- App Website: https://objective-see.com/products/reikey.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ReiKey with the following command
   ```
   brew install --cask reikey
   ```
4. ReiKey is ready to use now!