---
title: "Install bat-extras on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Bash scripts that integrate bat with various command-line tools"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bat-extras on MacOS using homebrew

- App Name: bat-extras
- App description: Bash scripts that integrate bat with various command-line tools
- App Version: 2021.08.21
- App Website: https://github.com/eth-p/bat-extras

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bat-extras with the following command
   ```
   brew install bat-extras
   ```
4. bat-extras is ready to use now!