---
title: "Install Softorino YouTube Converter on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "YouTube downloader and converter"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Softorino YouTube Converter on MacOS using homebrew

- App Name: Softorino YouTube Converter
- App description: YouTube downloader and converter
- App Version: 4.0.21
- App Website: https://softorino.com/youtube-converter/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Softorino YouTube Converter with the following command
   ```
   brew install --cask softorino-youtube-converter
   ```
4. Softorino YouTube Converter is ready to use now!