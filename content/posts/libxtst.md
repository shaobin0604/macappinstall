---
title: "Install libxtst on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "X.Org: Client API for the XTEST & RECORD extensions"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libxtst on MacOS using homebrew

- App Name: libxtst
- App description: X.Org: Client API for the XTEST & RECORD extensions
- App Version: 1.2.3
- App Website: https://www.x.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libxtst with the following command
   ```
   brew install libxtst
   ```
4. libxtst is ready to use now!