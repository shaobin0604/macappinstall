---
title: "Install libtool on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Generic library support script"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libtool on MacOS using homebrew

- App Name: libtool
- App description: Generic library support script
- App Version: 2.4.6
- App Website: https://www.gnu.org/software/libtool/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libtool with the following command
   ```
   brew install libtool
   ```
4. libtool is ready to use now!