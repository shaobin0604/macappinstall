---
title: "Install fava on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Web interface for the double-entry bookkeeping software Beancount"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fava on MacOS using homebrew

- App Name: fava
- App description: Web interface for the double-entry bookkeeping software Beancount
- App Version: 1.21
- App Website: https://beancount.github.io/fava/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fava with the following command
   ```
   brew install fava
   ```
4. fava is ready to use now!