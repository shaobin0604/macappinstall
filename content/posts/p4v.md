---
title: "Install Perforce Helix Visual Client on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Visual client for Helix Core"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Perforce Helix Visual Client on MacOS using homebrew

- App Name: Perforce Helix Visual Client
- App description: Visual client for Helix Core
- App Version: 2021.4,2227050
- App Website: https://www.perforce.com/products/helix-core-apps/helix-visual-client-p4v

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Perforce Helix Visual Client with the following command
   ```
   brew install --cask p4v
   ```
4. Perforce Helix Visual Client is ready to use now!