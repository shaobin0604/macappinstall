---
title: "Install beagle on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Evaluate the likelihood of sequence evolution on trees"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install beagle on MacOS using homebrew

- App Name: beagle
- App description: Evaluate the likelihood of sequence evolution on trees
- App Version: 3.1.2
- App Website: https://github.com/beagle-dev/beagle-lib

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install beagle with the following command
   ```
   brew install beagle
   ```
4. beagle is ready to use now!