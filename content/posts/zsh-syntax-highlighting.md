---
title: "Install zsh-syntax-highlighting on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Fish shell like syntax highlighting for zsh"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install zsh-syntax-highlighting on MacOS using homebrew

- App Name: zsh-syntax-highlighting
- App description: Fish shell like syntax highlighting for zsh
- App Version: 0.7.1
- App Website: https://github.com/zsh-users/zsh-syntax-highlighting

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install zsh-syntax-highlighting with the following command
   ```
   brew install zsh-syntax-highlighting
   ```
4. zsh-syntax-highlighting is ready to use now!