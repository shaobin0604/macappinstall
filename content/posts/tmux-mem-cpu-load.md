---
title: "Install tmux-mem-cpu-load on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CPU, RAM memory, and load monitor for use with tmux"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tmux-mem-cpu-load on MacOS using homebrew

- App Name: tmux-mem-cpu-load
- App description: CPU, RAM memory, and load monitor for use with tmux
- App Version: 3.5.1
- App Website: https://github.com/thewtex/tmux-mem-cpu-load

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tmux-mem-cpu-load with the following command
   ```
   brew install tmux-mem-cpu-load
   ```
4. tmux-mem-cpu-load is ready to use now!