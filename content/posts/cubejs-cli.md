---
title: "Install cubejs-cli on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cube.js command-line interface"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cubejs-cli on MacOS using homebrew

- App Name: cubejs-cli
- App description: Cube.js command-line interface
- App Version: 0.29.28
- App Website: https://cube.dev/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cubejs-cli with the following command
   ```
   brew install cubejs-cli
   ```
4. cubejs-cli is ready to use now!