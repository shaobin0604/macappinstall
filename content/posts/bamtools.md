---
title: "Install bamtools on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C++ API and command-line toolkit for BAM data"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bamtools on MacOS using homebrew

- App Name: bamtools
- App description: C++ API and command-line toolkit for BAM data
- App Version: 2.5.2
- App Website: https://github.com/pezmaster31/bamtools

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bamtools with the following command
   ```
   brew install bamtools
   ```
4. bamtools is ready to use now!