---
title: "Install telnet on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "User interface to the TELNET protocol"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install telnet on MacOS using homebrew

- App Name: telnet
- App description: User interface to the TELNET protocol
- App Version: 63
- App Website: https://opensource.apple.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install telnet with the following command
   ```
   brew install telnet
   ```
4. telnet is ready to use now!