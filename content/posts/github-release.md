---
title: "Install github-release on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Create and edit releases on Github (and upload artifacts)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install github-release on MacOS using homebrew

- App Name: github-release
- App description: Create and edit releases on Github (and upload artifacts)
- App Version: 0.10.0
- App Website: https://github.com/github-release/github-release

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install github-release with the following command
   ```
   brew install github-release
   ```
4. github-release is ready to use now!