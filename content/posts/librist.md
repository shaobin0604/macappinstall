---
title: "Install librist on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Reliable Internet Stream Transport (RIST)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install librist on MacOS using homebrew

- App Name: librist
- App description: Reliable Internet Stream Transport (RIST)
- App Version: 0.2.6
- App Website: https://code.videolan.org/rist/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install librist with the following command
   ```
   brew install librist
   ```
4. librist is ready to use now!