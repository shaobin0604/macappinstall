---
title: "Install Swish on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Control windows and applications right from your trackpad"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Swish on MacOS using homebrew

- App Name: Swish
- App description: Control windows and applications right from your trackpad
- App Version: 1.8.1,50
- App Website: https://highlyopinionated.co/swish/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Swish with the following command
   ```
   brew install --cask swish
   ```
4. Swish is ready to use now!