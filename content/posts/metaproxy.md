---
title: "Install metaproxy on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Z39.50 proxy and router utilizing Yaz toolkit"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install metaproxy on MacOS using homebrew

- App Name: metaproxy
- App description: Z39.50 proxy and router utilizing Yaz toolkit
- App Version: 1.20.0
- App Website: https://www.indexdata.com/resources/software/metaproxy/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install metaproxy with the following command
   ```
   brew install metaproxy
   ```
4. metaproxy is ready to use now!