---
title: "Install matterbridge on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Protocol bridge for multiple chat platforms"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install matterbridge on MacOS using homebrew

- App Name: matterbridge
- App description: Protocol bridge for multiple chat platforms
- App Version: 1.24.0
- App Website: https://github.com/42wim/matterbridge

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install matterbridge with the following command
   ```
   brew install matterbridge
   ```
4. matterbridge is ready to use now!