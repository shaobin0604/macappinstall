---
title: "Install wv on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Programs for accessing Microsoft Word documents"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install wv on MacOS using homebrew

- App Name: wv
- App description: Programs for accessing Microsoft Word documents
- App Version: 1.2.9
- App Website: https://wvware.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install wv with the following command
   ```
   brew install wv
   ```
4. wv is ready to use now!