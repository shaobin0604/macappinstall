---
title: "Install Webex Teams on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Video communication and virtual meeting platform"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Webex Teams on MacOS using homebrew

- App Name: Webex Teams
- App description: Video communication and virtual meeting platform
- App Version: 42.2.0.21338
- App Website: https://www.webex.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Webex Teams with the following command
   ```
   brew install --cask webex
   ```
4. Webex Teams is ready to use now!