---
title: "Install skinny on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Full-stack web app framework in Scala"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install skinny on MacOS using homebrew

- App Name: skinny
- App description: Full-stack web app framework in Scala
- App Version: 3.1.0
- App Website: http://skinny-framework.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install skinny with the following command
   ```
   brew install skinny
   ```
4. skinny is ready to use now!