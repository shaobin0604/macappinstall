---
title: "Install at-spi2-core on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Protocol definitions and daemon for D-Bus at-spi"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install at-spi2-core on MacOS using homebrew

- App Name: at-spi2-core
- App description: Protocol definitions and daemon for D-Bus at-spi
- App Version: 2.42.0
- App Website: https://www.freedesktop.org/wiki/Accessibility/AT-SPI2

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install at-spi2-core with the following command
   ```
   brew install at-spi2-core
   ```
4. at-spi2-core is ready to use now!