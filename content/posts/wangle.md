---
title: "Install wangle on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Modular, composable client/server abstractions framework"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install wangle on MacOS using homebrew

- App Name: wangle
- App description: Modular, composable client/server abstractions framework
- App Version: 2022.02.14.00
- App Website: https://github.com/facebook/wangle

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install wangle with the following command
   ```
   brew install wangle
   ```
4. wangle is ready to use now!