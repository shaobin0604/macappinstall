---
title: "Install AltTab on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Enable Windows-like alt-tab"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install AltTab on MacOS using homebrew

- App Name: AltTab
- App description: Enable Windows-like alt-tab
- App Version: 6.31.0
- App Website: https://github.com/lwouis/alt-tab-macos

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install AltTab with the following command
   ```
   brew install --cask alt-tab
   ```
4. AltTab is ready to use now!