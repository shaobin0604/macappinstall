---
title: "Install parallel on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Shell command parallelization utility"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install parallel on MacOS using homebrew

- App Name: parallel
- App description: Shell command parallelization utility
- App Version: 20220122
- App Website: https://savannah.gnu.org/projects/parallel/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install parallel with the following command
   ```
   brew install parallel
   ```
4. parallel is ready to use now!