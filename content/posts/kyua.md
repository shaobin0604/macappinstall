---
title: "Install kyua on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Testing framework for infrastructure software"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install kyua on MacOS using homebrew

- App Name: kyua
- App description: Testing framework for infrastructure software
- App Version: 0.13
- App Website: https://github.com/jmmv/kyua

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install kyua with the following command
   ```
   brew install kyua
   ```
4. kyua is ready to use now!