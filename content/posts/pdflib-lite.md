---
title: "Install pdflib-lite on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Subset of the functionality of PDFlib 7"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pdflib-lite on MacOS using homebrew

- App Name: pdflib-lite
- App description: Subset of the functionality of PDFlib 7
- App Version: 7.0.5p3
- App Website: https://www.pdflib.com/download/free-software/pdflib-lite-7/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pdflib-lite with the following command
   ```
   brew install pdflib-lite
   ```
4. pdflib-lite is ready to use now!