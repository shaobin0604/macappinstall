---
title: "Install Universal Battle on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Realtime multiplayer tabletop war game"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Universal Battle on MacOS using homebrew

- App Name: Universal Battle
- App description: Realtime multiplayer tabletop war game
- App Version: 1.4.1,52
- App Website: https://universalbattle2.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Universal Battle with the following command
   ```
   brew install --cask universal-battle
   ```
4. Universal Battle is ready to use now!