---
title: "Install boxes on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Draw boxes around text"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install boxes on MacOS using homebrew

- App Name: boxes
- App description: Draw boxes around text
- App Version: 2.1.1
- App Website: https://boxes.thomasjensen.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install boxes with the following command
   ```
   brew install boxes
   ```
4. boxes is ready to use now!