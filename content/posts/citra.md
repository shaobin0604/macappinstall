---
title: "Install Citra on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Nintendo 3DS emulator"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Citra on MacOS using homebrew

- App Name: Citra
- App description: Nintendo 3DS emulator
- App Version: latest
- App Website: https://citra-emu.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Citra with the following command
   ```
   brew install --cask citra
   ```
4. Citra is ready to use now!