---
title: "Install qca on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Qt Cryptographic Architecture (QCA)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install qca on MacOS using homebrew

- App Name: qca
- App description: Qt Cryptographic Architecture (QCA)
- App Version: 2.3.4
- App Website: https://userbase.kde.org/QCA

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install qca with the following command
   ```
   brew install qca
   ```
4. qca is ready to use now!