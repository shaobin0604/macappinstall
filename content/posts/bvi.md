---
title: "Install bvi on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Vi-like binary file (hex) editor"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bvi on MacOS using homebrew

- App Name: bvi
- App description: Vi-like binary file (hex) editor
- App Version: 1.4.1
- App Website: https://bvi.sourceforge.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bvi with the following command
   ```
   brew install bvi
   ```
4. bvi is ready to use now!