---
title: "Install vault-cli on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Subversion-like utility to work with Jackrabbit FileVault"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install vault-cli on MacOS using homebrew

- App Name: vault-cli
- App description: Subversion-like utility to work with Jackrabbit FileVault
- App Version: 3.5.8
- App Website: https://jackrabbit.apache.org/filevault/index.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install vault-cli with the following command
   ```
   brew install vault-cli
   ```
4. vault-cli is ready to use now!