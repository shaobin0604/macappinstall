---
title: "Install erofs-utils on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utilities for Enhanced Read-Only File System"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install erofs-utils on MacOS using homebrew

- App Name: erofs-utils
- App description: Utilities for Enhanced Read-Only File System
- App Version: 1.4
- App Website: https://git.kernel.org/pub/scm/linux/kernel/git/xiang/erofs-utils.git

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install erofs-utils with the following command
   ```
   brew install erofs-utils
   ```
4. erofs-utils is ready to use now!