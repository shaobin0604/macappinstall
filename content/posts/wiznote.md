---
title: "Install WizNote on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Note-taking application"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install WizNote on MacOS using homebrew

- App Name: WizNote
- App description: Note-taking application
- App Version: 0.1.66
- App Website: https://www.wiz.cn/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install WizNote with the following command
   ```
   brew install --cask wiznote
   ```
4. WizNote is ready to use now!