---
title: "Install zsh-completions on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Additional completion definitions for zsh"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install zsh-completions on MacOS using homebrew

- App Name: zsh-completions
- App description: Additional completion definitions for zsh
- App Version: 0.33.0
- App Website: https://github.com/zsh-users/zsh-completions

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install zsh-completions with the following command
   ```
   brew install zsh-completions
   ```
4. zsh-completions is ready to use now!