---
title: "Install TI UniFlash on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Flash tool for microcontrollers"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install TI UniFlash on MacOS using homebrew

- App Name: TI UniFlash
- App description: Flash tool for microcontrollers
- App Version: 7.0.0.3615
- App Website: https://www.ti.com/tool/UNIFLASH

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install TI UniFlash with the following command
   ```
   brew install --cask uniflash
   ```
4. TI UniFlash is ready to use now!