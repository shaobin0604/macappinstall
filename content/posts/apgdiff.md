---
title: "Install apgdiff on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Another PostgreSQL diff tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install apgdiff on MacOS using homebrew

- App Name: apgdiff
- App description: Another PostgreSQL diff tool
- App Version: 2.4
- App Website: https://www.apgdiff.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install apgdiff with the following command
   ```
   brew install apgdiff
   ```
4. apgdiff is ready to use now!