---
title: "Install valabind on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Vala bindings for radare, reverse engineering framework"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install valabind on MacOS using homebrew

- App Name: valabind
- App description: Vala bindings for radare, reverse engineering framework
- App Version: 1.8.0
- App Website: https://github.com/radare/valabind

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install valabind with the following command
   ```
   brew install valabind
   ```
4. valabind is ready to use now!