---
title: "Install mas on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Mac App Store command-line interface"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mas on MacOS using homebrew

- App Name: mas
- App description: Mac App Store command-line interface
- App Version: 1.8.6
- App Website: https://github.com/mas-cli/mas

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mas with the following command
   ```
   brew install mas
   ```
4. mas is ready to use now!