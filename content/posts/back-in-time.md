---
title: "Install Back-In-Time on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Access content of Time Machine backups"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Back-In-Time on MacOS using homebrew

- App Name: Back-In-Time
- App description: Access content of Time Machine backups
- App Version: 5.1.8
- App Website: https://www.tri-edre.com/english/backintime.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Back-In-Time with the following command
   ```
   brew install --cask back-in-time
   ```
4. Back-In-Time is ready to use now!