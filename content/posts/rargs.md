---
title: "Install rargs on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Util like xargs + awk with pattern matching support"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install rargs on MacOS using homebrew

- App Name: rargs
- App description: Util like xargs + awk with pattern matching support
- App Version: 0.3.0
- App Website: https://github.com/lotabout/rargs

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install rargs with the following command
   ```
   brew install rargs
   ```
4. rargs is ready to use now!