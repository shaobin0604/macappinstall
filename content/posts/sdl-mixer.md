---
title: "Install sdl_mixer on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Sample multi-channel audio mixer library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sdl_mixer on MacOS using homebrew

- App Name: sdl_mixer
- App description: Sample multi-channel audio mixer library
- App Version: 1.2.12
- App Website: https://www.libsdl.org/projects/SDL_mixer/release-1.2.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sdl_mixer with the following command
   ```
   brew install sdl_mixer
   ```
4. sdl_mixer is ready to use now!