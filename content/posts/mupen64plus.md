---
title: "Install mupen64plus on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cross-platform plugin-based N64 emulator"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mupen64plus on MacOS using homebrew

- App Name: mupen64plus
- App description: Cross-platform plugin-based N64 emulator
- App Version: 2.5
- App Website: https://www.mupen64plus.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mupen64plus with the following command
   ```
   brew install mupen64plus
   ```
4. mupen64plus is ready to use now!