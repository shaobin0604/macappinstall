---
title: "Install libgadu on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library for ICQ instant messenger protocol"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libgadu on MacOS using homebrew

- App Name: libgadu
- App description: Library for ICQ instant messenger protocol
- App Version: 1.12.2
- App Website: https://libgadu.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libgadu with the following command
   ```
   brew install libgadu
   ```
4. libgadu is ready to use now!