---
title: "Install open-mesh on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Generic data structure to represent and manipulate polygonal meshes"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install open-mesh on MacOS using homebrew

- App Name: open-mesh
- App description: Generic data structure to represent and manipulate polygonal meshes
- App Version: 9.0
- App Website: https://openmesh.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install open-mesh with the following command
   ```
   brew install open-mesh
   ```
4. open-mesh is ready to use now!