---
title: "Install btpd on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "BitTorrent Protocol Daemon"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install btpd on MacOS using homebrew

- App Name: btpd
- App description: BitTorrent Protocol Daemon
- App Version: 0.16
- App Website: https://github.com/btpd/btpd

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install btpd with the following command
   ```
   brew install btpd
   ```
4. btpd is ready to use now!