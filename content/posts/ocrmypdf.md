---
title: "Install ocrmypdf on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Adds an OCR text layer to scanned PDF files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ocrmypdf on MacOS using homebrew

- App Name: ocrmypdf
- App description: Adds an OCR text layer to scanned PDF files
- App Version: 13.3.0
- App Website: https://github.com/jbarlow83/OCRmyPDF

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ocrmypdf with the following command
   ```
   brew install ocrmypdf
   ```
4. ocrmypdf is ready to use now!