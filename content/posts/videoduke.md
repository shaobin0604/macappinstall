---
title: "Install VideoDuke on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Video downloader"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install VideoDuke on MacOS using homebrew

- App Name: VideoDuke
- App description: Video downloader
- App Version: 2.3,485
- App Website: https://mac.eltima.com/video-downloader.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install VideoDuke with the following command
   ```
   brew install --cask videoduke
   ```
4. VideoDuke is ready to use now!