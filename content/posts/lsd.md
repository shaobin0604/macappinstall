---
title: "Install lsd on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Clone of ls with colorful output, file type icons, and more"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install lsd on MacOS using homebrew

- App Name: lsd
- App description: Clone of ls with colorful output, file type icons, and more
- App Version: 0.21.0
- App Website: https://github.com/Peltoche/lsd

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install lsd with the following command
   ```
   brew install lsd
   ```
4. lsd is ready to use now!