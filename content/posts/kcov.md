---
title: "Install kcov on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Code coverage tester for compiled programs, Python, and shell scripts"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install kcov on MacOS using homebrew

- App Name: kcov
- App description: Code coverage tester for compiled programs, Python, and shell scripts
- App Version: 40
- App Website: https://simonkagstrom.github.io/kcov/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install kcov with the following command
   ```
   brew install kcov
   ```
4. kcov is ready to use now!