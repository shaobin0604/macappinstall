---
title: "Install zsh on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "UNIX shell (command interpreter)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install zsh on MacOS using homebrew

- App Name: zsh
- App description: UNIX shell (command interpreter)
- App Version: 5.8.1
- App Website: https://www.zsh.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install zsh with the following command
   ```
   brew install zsh
   ```
4. zsh is ready to use now!