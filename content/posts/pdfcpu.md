---
title: "Install pdfcpu on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "PDF processor written in Go"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pdfcpu on MacOS using homebrew

- App Name: pdfcpu
- App description: PDF processor written in Go
- App Version: 0.3.13
- App Website: https://pdfcpu.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pdfcpu with the following command
   ```
   brew install pdfcpu
   ```
4. pdfcpu is ready to use now!