---
title: "Install btop on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Resource monitor. C++ version and continuation of bashtop and bpytop"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install btop on MacOS using homebrew

- App Name: btop
- App description: Resource monitor. C++ version and continuation of bashtop and bpytop
- App Version: 1.2.3
- App Website: https://github.com/aristocratos/btop

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install btop with the following command
   ```
   brew install btop
   ```
4. btop is ready to use now!