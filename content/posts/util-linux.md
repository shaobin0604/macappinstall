---
title: "Install util-linux on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Collection of Linux utilities"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install util-linux on MacOS using homebrew

- App Name: util-linux
- App description: Collection of Linux utilities
- App Version: 2.37.4
- App Website: https://github.com/karelzak/util-linux

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install util-linux with the following command
   ```
   brew install util-linux
   ```
4. util-linux is ready to use now!