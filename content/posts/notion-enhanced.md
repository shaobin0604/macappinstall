---
title: "Install Notion Enhanced on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Enhancer/customiser for the all-in-one productivity workspace notion.so"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Notion Enhanced on MacOS using homebrew

- App Name: Notion Enhanced
- App description: Enhancer/customiser for the all-in-one productivity workspace notion.so
- App Version: 2.0.18-1
- App Website: https://notion-enhancer.github.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Notion Enhanced with the following command
   ```
   brew install --cask notion-enhanced
   ```
4. Notion Enhanced is ready to use now!