---
title: "Install azure-cli on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Microsoft Azure CLI 2.0"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install azure-cli on MacOS using homebrew

- App Name: azure-cli
- App description: Microsoft Azure CLI 2.0
- App Version: 2.33.1
- App Website: https://docs.microsoft.com/cli/azure/overview

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install azure-cli with the following command
   ```
   brew install azure-cli
   ```
4. azure-cli is ready to use now!