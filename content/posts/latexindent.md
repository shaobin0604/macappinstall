---
title: "Install latexindent on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Add indentation to LaTeX files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install latexindent on MacOS using homebrew

- App Name: latexindent
- App description: Add indentation to LaTeX files
- App Version: 3.15
- App Website: https://ctan.org/pkg/latexindent

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install latexindent with the following command
   ```
   brew install latexindent
   ```
4. latexindent is ready to use now!