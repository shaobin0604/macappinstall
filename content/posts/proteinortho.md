---
title: "Install proteinortho on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Detecting orthologous genes within different species"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install proteinortho on MacOS using homebrew

- App Name: proteinortho
- App description: Detecting orthologous genes within different species
- App Version: 6.0.33
- App Website: https://gitlab.com/paulklemm_PHD/proteinortho

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install proteinortho with the following command
   ```
   brew install proteinortho
   ```
4. proteinortho is ready to use now!