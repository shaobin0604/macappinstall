---
title: "Install imagemagick@6 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tools and libraries to manipulate images in many formats"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install imagemagick@6 on MacOS using homebrew

- App Name: imagemagick@6
- App description: Tools and libraries to manipulate images in many formats
- App Version: 6.9.12-40
- App Website: https://legacy.imagemagick.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install imagemagick@6 with the following command
   ```
   brew install imagemagick@6
   ```
4. imagemagick@6 is ready to use now!