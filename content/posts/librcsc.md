---
title: "Install librcsc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "RoboCup Soccer Simulator library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install librcsc on MacOS using homebrew

- App Name: librcsc
- App description: RoboCup Soccer Simulator library
- App Version: 4.1.0
- App Website: https://osdn.net/projects/rctools/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install librcsc with the following command
   ```
   brew install librcsc
   ```
4. librcsc is ready to use now!