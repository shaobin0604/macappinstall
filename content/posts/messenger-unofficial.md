---
title: "Install Messenger on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Messenger on MacOS using homebrew

- App Name: Messenger
- App description: null
- App Version: 0.1.13.1491443794,7b1777b58fef0bf9
- App Website: https://fbmacmessenger.rsms.me/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Messenger with the following command
   ```
   brew install --cask messenger-unofficial
   ```
4. Messenger is ready to use now!