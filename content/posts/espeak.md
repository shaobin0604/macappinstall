---
title: "Install espeak on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Text to speech, software speech synthesizer"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install espeak on MacOS using homebrew

- App Name: espeak
- App description: Text to speech, software speech synthesizer
- App Version: 1.48.04
- App Website: https://espeak.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install espeak with the following command
   ```
   brew install espeak
   ```
4. espeak is ready to use now!