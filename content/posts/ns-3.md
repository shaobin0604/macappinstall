---
title: "Install ns-3 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Discrete-event network simulator"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ns-3 on MacOS using homebrew

- App Name: ns-3
- App description: Discrete-event network simulator
- App Version: 3.35
- App Website: https://www.nsnam.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ns-3 with the following command
   ```
   brew install ns-3
   ```
4. ns-3 is ready to use now!