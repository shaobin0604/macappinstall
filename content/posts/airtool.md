---
title: "Install Airtool on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Capture Wi-Fi packets"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Airtool on MacOS using homebrew

- App Name: Airtool
- App description: Capture Wi-Fi packets
- App Version: 2.3.6,14
- App Website: https://www.intuitibits.com/products/airtool/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Airtool with the following command
   ```
   brew install --cask airtool
   ```
4. Airtool is ready to use now!