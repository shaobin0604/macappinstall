---
title: "Install mapnik on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Toolkit for developing mapping applications"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mapnik on MacOS using homebrew

- App Name: mapnik
- App description: Toolkit for developing mapping applications
- App Version: 3.1.0
- App Website: https://mapnik.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mapnik with the following command
   ```
   brew install mapnik
   ```
4. mapnik is ready to use now!