---
title: "Install QtSpim on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Simulator that runs MIPS32 assembly language programs"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install QtSpim on MacOS using homebrew

- App Name: QtSpim
- App description: Simulator that runs MIPS32 assembly language programs
- App Version: 9.1.23
- App Website: https://spimsimulator.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install QtSpim with the following command
   ```
   brew install --cask qtspim
   ```
4. QtSpim is ready to use now!