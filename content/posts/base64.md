---
title: "Install base64 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Encode and decode base64 files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install base64 on MacOS using homebrew

- App Name: base64
- App description: Encode and decode base64 files
- App Version: 1.5
- App Website: https://www.fourmilab.ch/webtools/base64/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install base64 with the following command
   ```
   brew install base64
   ```
4. base64 is ready to use now!