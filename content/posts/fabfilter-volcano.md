---
title: "Install FabFilter Volcano on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Filter plug-in"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install FabFilter Volcano on MacOS using homebrew

- App Name: FabFilter Volcano
- App description: Filter plug-in
- App Version: 3.01
- App Website: https://www.fabfilter.com/products/volcano-2-powerful-filter-plug-in

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install FabFilter Volcano with the following command
   ```
   brew install --cask fabfilter-volcano
   ```
4. FabFilter Volcano is ready to use now!