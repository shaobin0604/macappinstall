---
title: "Install dvorak7min on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Dvorak (simplified keyboard layout) typing tutor"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dvorak7min on MacOS using homebrew

- App Name: dvorak7min
- App description: Dvorak (simplified keyboard layout) typing tutor
- App Version: 1.6.1
- App Website: https://web.archive.org/web/dvorak7min.sourcearchive.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dvorak7min with the following command
   ```
   brew install dvorak7min
   ```
4. dvorak7min is ready to use now!