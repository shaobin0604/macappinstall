---
title: "Install SessionRestore on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Helps to keep numerous Safari tabs open for reading them later"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install SessionRestore on MacOS using homebrew

- App Name: SessionRestore
- App description: Helps to keep numerous Safari tabs open for reading them later
- App Version: 2.6.5
- App Website: https://sessionrestore.sweetpproductions.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install SessionRestore with the following command
   ```
   brew install --cask sessionrestore
   ```
4. SessionRestore is ready to use now!