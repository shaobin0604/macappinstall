---
title: "Install testdisk on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Powerful free data recovery utility"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install testdisk on MacOS using homebrew

- App Name: testdisk
- App description: Powerful free data recovery utility
- App Version: 7.1
- App Website: https://www.cgsecurity.org/wiki/TestDisk

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install testdisk with the following command
   ```
   brew install testdisk
   ```
4. testdisk is ready to use now!