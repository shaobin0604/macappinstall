---
title: "Install libxmp-lite on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Lite libxmp"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libxmp-lite on MacOS using homebrew

- App Name: libxmp-lite
- App description: Lite libxmp
- App Version: 4.5.0
- App Website: https://xmp.sourceforge.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libxmp-lite with the following command
   ```
   brew install libxmp-lite
   ```
4. libxmp-lite is ready to use now!