---
title: "Install Soothe 2 on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Dynamic resonance suppressor"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Soothe 2 on MacOS using homebrew

- App Name: Soothe 2
- App description: Dynamic resonance suppressor
- App Version: 1.2.2
- App Website: https://oeksound.com/plugins/soothe2/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Soothe 2 with the following command
   ```
   brew install --cask soothe2
   ```
4. Soothe 2 is ready to use now!