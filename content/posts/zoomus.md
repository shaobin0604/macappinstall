---
title: "Install Zoom.us makeshift alias on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Temporary makeshift alias for the video communication tool Zoom"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Zoom.us makeshift alias on MacOS using homebrew

- App Name: Zoom.us makeshift alias
- App description: Temporary makeshift alias for the video communication tool Zoom
- App Version: 5.4.58903.1122.1
- App Website: https://www.zoom.us/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Zoom.us makeshift alias with the following command
   ```
   brew install --cask zoomus
   ```
4. Zoom.us makeshift alias is ready to use now!