---
title: "Install czmq on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "High-level C binding for ZeroMQ"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install czmq on MacOS using homebrew

- App Name: czmq
- App description: High-level C binding for ZeroMQ
- App Version: 4.2.1
- App Website: http://czmq.zeromq.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install czmq with the following command
   ```
   brew install czmq
   ```
4. czmq is ready to use now!