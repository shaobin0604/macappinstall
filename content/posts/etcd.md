---
title: "Install etcd on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Key value store for shared configuration and service discovery"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install etcd on MacOS using homebrew

- App Name: etcd
- App description: Key value store for shared configuration and service discovery
- App Version: 3.5.2
- App Website: https://github.com/etcd-io/etcd

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install etcd with the following command
   ```
   brew install etcd
   ```
4. etcd is ready to use now!