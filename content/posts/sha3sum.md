---
title: "Install sha3sum on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Keccak, SHA-3, SHAKE, and RawSHAKE checksum utilities"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sha3sum on MacOS using homebrew

- App Name: sha3sum
- App description: Keccak, SHA-3, SHAKE, and RawSHAKE checksum utilities
- App Version: 1.2.1
- App Website: https://github.com/maandree/sha3sum

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sha3sum with the following command
   ```
   brew install sha3sum
   ```
4. sha3sum is ready to use now!