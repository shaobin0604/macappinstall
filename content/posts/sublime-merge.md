---
title: "Install Sublime Merge on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Git client"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Sublime Merge on MacOS using homebrew

- App Name: Sublime Merge
- App description: Git client
- App Version: 2068
- App Website: https://www.sublimemerge.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Sublime Merge with the following command
   ```
   brew install --cask sublime-merge
   ```
4. Sublime Merge is ready to use now!