---
title: "Install gopass on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Slightly more awesome Standard Unix Password Manager for Teams"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gopass on MacOS using homebrew

- App Name: gopass
- App description: Slightly more awesome Standard Unix Password Manager for Teams
- App Version: 1.13.1
- App Website: https://github.com/gopasspw/gopass

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gopass with the following command
   ```
   brew install gopass
   ```
4. gopass is ready to use now!