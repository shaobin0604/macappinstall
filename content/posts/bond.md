---
title: "Install bond on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cross-platform framework for working with schematized data"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bond on MacOS using homebrew

- App Name: bond
- App description: Cross-platform framework for working with schematized data
- App Version: 9.0.5
- App Website: https://github.com/microsoft/bond

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bond with the following command
   ```
   brew install bond
   ```
4. bond is ready to use now!