---
title: "Install Memory Tracker by Timely on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Time tracking software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Memory Tracker by Timely on MacOS using homebrew

- App Name: Memory Tracker by Timely
- App description: Time tracking software
- App Version: 2021.12,347
- App Website: https://memory.ai/timely/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Memory Tracker by Timely with the following command
   ```
   brew install --cask memory
   ```
4. Memory Tracker by Timely is ready to use now!