---
title: "Install Anonym on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Network access anonymizer"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Anonym on MacOS using homebrew

- App Name: Anonym
- App description: Network access anonymizer
- App Version: 2.3
- App Website: https://www.hanynet.com/anonym/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Anonym with the following command
   ```
   brew install --cask anonym
   ```
4. Anonym is ready to use now!