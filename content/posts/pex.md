---
title: "Install pex on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Package manager for PostgreSQL"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pex on MacOS using homebrew

- App Name: pex
- App description: Package manager for PostgreSQL
- App Version: 1.20140409
- App Website: https://github.com/petere/pex

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pex with the following command
   ```
   brew install pex
   ```
4. pex is ready to use now!