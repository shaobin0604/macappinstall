---
title: "Install sipsak on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "SIP Swiss army knife"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sipsak on MacOS using homebrew

- App Name: sipsak
- App description: SIP Swiss army knife
- App Version: 0.9.8.1
- App Website: https://github.com/nils-ohlmeier/sipsak/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sipsak with the following command
   ```
   brew install sipsak
   ```
4. sipsak is ready to use now!