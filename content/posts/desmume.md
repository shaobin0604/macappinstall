---
title: "Install DeSmuME on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Nintendo DS emulator"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install DeSmuME on MacOS using homebrew

- App Name: DeSmuME
- App description: Nintendo DS emulator
- App Version: 0.9.11
- App Website: https://sourceforge.net/projects/desmume/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install DeSmuME with the following command
   ```
   brew install --cask desmume
   ```
4. DeSmuME is ready to use now!