---
title: "Install wumpus on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Exact clone of the ancient BASIC Hunt the Wumpus game"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install wumpus on MacOS using homebrew

- App Name: wumpus
- App description: Exact clone of the ancient BASIC Hunt the Wumpus game
- App Version: 1.7
- App Website: http://www.catb.org/~esr/wumpus/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install wumpus with the following command
   ```
   brew install wumpus
   ```
4. wumpus is ready to use now!