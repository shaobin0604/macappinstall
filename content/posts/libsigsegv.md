---
title: "Install libsigsegv on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library for handling page faults in user mode"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libsigsegv on MacOS using homebrew

- App Name: libsigsegv
- App description: Library for handling page faults in user mode
- App Version: 2.14
- App Website: https://www.gnu.org/software/libsigsegv/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libsigsegv with the following command
   ```
   brew install libsigsegv
   ```
4. libsigsegv is ready to use now!