---
title: "Install golangci-lint on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Fast linters runner for Go"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install golangci-lint on MacOS using homebrew

- App Name: golangci-lint
- App description: Fast linters runner for Go
- App Version: 1.44.2
- App Website: https://golangci-lint.run/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install golangci-lint with the following command
   ```
   brew install golangci-lint
   ```
4. golangci-lint is ready to use now!