---
title: "Install Sunlogin 11 on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Remote desktop control and monitoring tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Sunlogin 11 on MacOS using homebrew

- App Name: Sunlogin 11
- App description: Remote desktop control and monitoring tool
- App Version: 11.0.2.41962
- App Website: https://sunlogin.oray.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Sunlogin 11 with the following command
   ```
   brew install --cask sunloginclient
   ```
4. Sunlogin 11 is ready to use now!