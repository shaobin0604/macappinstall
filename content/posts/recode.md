---
title: "Install recode on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Convert character set (charsets)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install recode on MacOS using homebrew

- App Name: recode
- App description: Convert character set (charsets)
- App Version: 3.7.12
- App Website: https://github.com/rrthomas/recode

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install recode with the following command
   ```
   brew install recode
   ```
4. recode is ready to use now!