---
title: "Install rarian on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Documentation metadata library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install rarian on MacOS using homebrew

- App Name: rarian
- App description: Documentation metadata library
- App Version: 0.8.1
- App Website: https://rarian.freedesktop.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install rarian with the following command
   ```
   brew install rarian
   ```
4. rarian is ready to use now!