---
title: "Install Pokemon Trading Card Game Online on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "PLay the Pokemon TCG online"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Pokemon Trading Card Game Online on MacOS using homebrew

- App Name: Pokemon Trading Card Game Online
- App description: PLay the Pokemon TCG online
- App Version: 2.78.0.5176
- App Website: https://www.pokemon.com/us/pokemon-tcg/play-online/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Pokemon Trading Card Game Online with the following command
   ```
   brew install --cask pokemon-trading-card-game-online
   ```
4. Pokemon Trading Card Game Online is ready to use now!