---
title: "Install Banking 4 on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "German accounting software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Banking 4 on MacOS using homebrew

- App Name: Banking 4
- App description: German accounting software
- App Version: 7.8.0,8000
- App Website: https://subsembly.com/banking4.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Banking 4 with the following command
   ```
   brew install --cask banking-4
   ```
4. Banking 4 is ready to use now!