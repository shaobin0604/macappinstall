---
title: "Install git-xargs on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CLI for making updates across multiple Github repositories with a single command"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install git-xargs on MacOS using homebrew

- App Name: git-xargs
- App description: CLI for making updates across multiple Github repositories with a single command
- App Version: 0.0.13
- App Website: https://github.com/gruntwork-io/git-xargs

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install git-xargs with the following command
   ```
   brew install git-xargs
   ```
4. git-xargs is ready to use now!