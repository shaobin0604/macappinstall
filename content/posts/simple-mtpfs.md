---
title: "Install simple-mtpfs on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Simple MTP fuse filesystem driver"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install simple-mtpfs on MacOS using homebrew

- App Name: simple-mtpfs
- App description: Simple MTP fuse filesystem driver
- App Version: 0.4.0
- App Website: https://github.com/phatina/simple-mtpfs

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install simple-mtpfs with the following command
   ```
   brew install simple-mtpfs
   ```
4. simple-mtpfs is ready to use now!