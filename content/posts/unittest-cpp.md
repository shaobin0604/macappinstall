---
title: "Install unittest-cpp on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Unit testing framework for C++"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install unittest-cpp on MacOS using homebrew

- App Name: unittest-cpp
- App description: Unit testing framework for C++
- App Version: 2.0.0
- App Website: https://github.com/unittest-cpp/unittest-cpp

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install unittest-cpp with the following command
   ```
   brew install unittest-cpp
   ```
4. unittest-cpp is ready to use now!