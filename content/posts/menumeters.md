---
title: "Install MenuMeters for El Capitan (and later) on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Set of CPU, memory, disk, and network monitoring tools"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install MenuMeters for El Capitan (and later) on MacOS using homebrew

- App Name: MenuMeters for El Capitan (and later)
- App description: Set of CPU, memory, disk, and network monitoring tools
- App Version: 2.1.6.1
- App Website: https://member.ipmu.jp/yuji.tachikawa/MenuMetersElCapitan/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install MenuMeters for El Capitan (and later) with the following command
   ```
   brew install --cask menumeters
   ```
4. MenuMeters for El Capitan (and later) is ready to use now!