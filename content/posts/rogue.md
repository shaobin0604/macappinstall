---
title: "Install rogue on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Dungeon crawling video game"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install rogue on MacOS using homebrew

- App Name: rogue
- App description: Dungeon crawling video game
- App Version: 5.4.4
- App Website: https://sourceforge.net/projects/roguelike/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install rogue with the following command
   ```
   brew install rogue
   ```
4. rogue is ready to use now!