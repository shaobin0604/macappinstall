---
title: "Install Amadeus Pro on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Multi-purpose audio recorder, editor and converter"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Amadeus Pro on MacOS using homebrew

- App Name: Amadeus Pro
- App description: Multi-purpose audio recorder, editor and converter
- App Version: 2.8.8
- App Website: https://www.hairersoft.com/pro.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Amadeus Pro with the following command
   ```
   brew install --cask amadeus-pro
   ```
4. Amadeus Pro is ready to use now!