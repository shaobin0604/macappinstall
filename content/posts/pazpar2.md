---
title: "Install pazpar2 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Metasearching middleware webservice"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pazpar2 on MacOS using homebrew

- App Name: pazpar2
- App description: Metasearching middleware webservice
- App Version: 1.14.1
- App Website: https://www.indexdata.com/resources/software/pazpar2/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pazpar2 with the following command
   ```
   brew install pazpar2
   ```
4. pazpar2 is ready to use now!