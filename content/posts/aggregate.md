---
title: "Install aggregate on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Optimizes lists of prefixes to reduce list lengths"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install aggregate on MacOS using homebrew

- App Name: aggregate
- App description: Optimizes lists of prefixes to reduce list lengths
- App Version: 1.6
- App Website: https://web.archive.org/web/20160716192438/freecode.com/projects/aggregate/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install aggregate with the following command
   ```
   brew install aggregate
   ```
4. aggregate is ready to use now!