---
title: "Install oggz on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line tool for manipulating Ogg files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install oggz on MacOS using homebrew

- App Name: oggz
- App description: Command-line tool for manipulating Ogg files
- App Version: 1.1.1
- App Website: https://www.xiph.org/oggz/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install oggz with the following command
   ```
   brew install oggz
   ```
4. oggz is ready to use now!