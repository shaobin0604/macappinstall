---
title: "Install Noisy Typer on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Noisy Typer on MacOS using homebrew

- App Name: Noisy Typer
- App description: null
- App Version: 0.0.1
- App Website: http://fffff.at/noisy-typer-a-typewriter-for-your-laptop/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Noisy Typer with the following command
   ```
   brew install --cask noisytyper
   ```
4. Noisy Typer is ready to use now!