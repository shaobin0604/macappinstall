---
title: "Install DropDMG on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Create DMGs and other archives"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install DropDMG on MacOS using homebrew

- App Name: DropDMG
- App description: Create DMGs and other archives
- App Version: 3.6.3
- App Website: https://c-command.com/dropdmg/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install DropDMG with the following command
   ```
   brew install --cask dropdmg
   ```
4. DropDMG is ready to use now!