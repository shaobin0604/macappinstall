---
title: "Install MediaInfo on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Display technical and tag data for video and audio files"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install MediaInfo on MacOS using homebrew

- App Name: MediaInfo
- App description: Display technical and tag data for video and audio files
- App Version: 21.09
- App Website: https://mediaarea.net/en/MediaInfo

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install MediaInfo with the following command
   ```
   brew install --cask mediainfo
   ```
4. MediaInfo is ready to use now!