---
title: "Install Kindle Comic Creator on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Kindle Comic Creator on MacOS using homebrew

- App Name: Kindle Comic Creator
- App description: null
- App Version: 1.160
- App Website: https://www.amazon.com/gp/feature.html?ie=UTF8&docId=1001103761

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Kindle Comic Creator with the following command
   ```
   brew install --cask kindle-comic-creator
   ```
4. Kindle Comic Creator is ready to use now!