---
title: "Install openapi-generator on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Generate clients, server & docs from an OpenAPI spec (v2, v3)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install openapi-generator on MacOS using homebrew

- App Name: openapi-generator
- App description: Generate clients, server & docs from an OpenAPI spec (v2, v3)
- App Version: 5.4.0
- App Website: https://openapi-generator.tech/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install openapi-generator with the following command
   ```
   brew install openapi-generator
   ```
4. openapi-generator is ready to use now!