---
title: "Install dps8m on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Simulator for the Multics dps-8/m mainframe"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dps8m on MacOS using homebrew

- App Name: dps8m
- App description: Simulator for the Multics dps-8/m mainframe
- App Version: 2.0
- App Website: https://ringzero.wikidot.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dps8m with the following command
   ```
   brew install dps8m
   ```
4. dps8m is ready to use now!