---
title: "Install runcocoa on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tools to run Cocoa/Objective-C and C code from the command-line"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install runcocoa on MacOS using homebrew

- App Name: runcocoa
- App description: Tools to run Cocoa/Objective-C and C code from the command-line
- App Version: 20120108
- App Website: https://github.com/michaeltyson/Commandline-Cocoa

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install runcocoa with the following command
   ```
   brew install runcocoa
   ```
4. runcocoa is ready to use now!