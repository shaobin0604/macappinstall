---
title: "Install (Deep) HIARCS Chess Explorer on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Chess database, analysis and game playing program"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install (Deep) HIARCS Chess Explorer on MacOS using homebrew

- App Name: (Deep) HIARCS Chess Explorer
- App description: Chess database, analysis and game playing program
- App Version: 1.11.1
- App Website: https://www.hiarcs.com/mac-chess-explorer.htm

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install (Deep) HIARCS Chess Explorer with the following command
   ```
   brew install --cask hiarcs-chess-explorer
   ```
4. (Deep) HIARCS Chess Explorer is ready to use now!