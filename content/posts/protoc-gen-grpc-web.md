---
title: "Install protoc-gen-grpc-web on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Protoc plugin that generates code for gRPC-Web clients"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install protoc-gen-grpc-web on MacOS using homebrew

- App Name: protoc-gen-grpc-web
- App description: Protoc plugin that generates code for gRPC-Web clients
- App Version: 1.3.1
- App Website: https://github.com/grpc/grpc-web

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install protoc-gen-grpc-web with the following command
   ```
   brew install protoc-gen-grpc-web
   ```
4. protoc-gen-grpc-web is ready to use now!