---
title: "Install Haptic Touch Bar on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Haptic Touch Bar on MacOS using homebrew

- App Name: Haptic Touch Bar
- App description: null
- App Version: 2.4.0,1540815050
- App Website: https://www.haptictouchbar.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Haptic Touch Bar with the following command
   ```
   brew install --cask haptic-touch-bar
   ```
4. Haptic Touch Bar is ready to use now!