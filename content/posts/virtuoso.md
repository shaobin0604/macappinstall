---
title: "Install virtuoso on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "High-performance object-relational SQL database"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install virtuoso on MacOS using homebrew

- App Name: virtuoso
- App description: High-performance object-relational SQL database
- App Version: 7.2.6
- App Website: https://virtuoso.openlinksw.com/wiki/main/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install virtuoso with the following command
   ```
   brew install virtuoso
   ```
4. virtuoso is ready to use now!