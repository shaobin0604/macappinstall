---
title: "Install serf on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Service orchestration and management tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install serf on MacOS using homebrew

- App Name: serf
- App description: Service orchestration and management tool
- App Version: 0.9.7
- App Website: https://serfdom.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install serf with the following command
   ```
   brew install serf
   ```
4. serf is ready to use now!