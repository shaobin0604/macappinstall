---
title: "Install ncurses on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Text-based UI library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ncurses on MacOS using homebrew

- App Name: ncurses
- App description: Text-based UI library
- App Version: 6.3
- App Website: https://invisible-island.net/ncurses/announce.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ncurses with the following command
   ```
   brew install ncurses
   ```
4. ncurses is ready to use now!