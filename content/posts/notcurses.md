---
title: "Install notcurses on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Blingful character graphics/TUI library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install notcurses on MacOS using homebrew

- App Name: notcurses
- App description: Blingful character graphics/TUI library
- App Version: 3.0.6
- App Website: https://nick-black.com/dankwiki/index.php/Notcurses

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install notcurses with the following command
   ```
   brew install notcurses
   ```
4. notcurses is ready to use now!