---
title: "Install OpenZFS on OS X on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "ZFS driver and utilities"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install OpenZFS on OS X on MacOS using homebrew

- App Name: OpenZFS on OS X
- App description: ZFS driver and utilities
- App Version: 2.1.0,343
- App Website: https://openzfsonosx.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install OpenZFS on OS X with the following command
   ```
   brew install --cask openzfs
   ```
4. OpenZFS on OS X is ready to use now!