---
title: "Install Jitsi Meet on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Secure video conferencing app"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Jitsi Meet on MacOS using homebrew

- App Name: Jitsi Meet
- App description: Secure video conferencing app
- App Version: 2022.1.1
- App Website: https://github.com/jitsi/jitsi-meet-electron/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Jitsi Meet with the following command
   ```
   brew install --cask jitsi-meet
   ```
4. Jitsi Meet is ready to use now!