---
title: "Install Cardhop on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Contacts manager"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Cardhop on MacOS using homebrew

- App Name: Cardhop
- App description: Contacts manager
- App Version: 2.0.8,1072
- App Website: https://flexibits.com/cardhop

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Cardhop with the following command
   ```
   brew install --cask cardhop
   ```
4. Cardhop is ready to use now!