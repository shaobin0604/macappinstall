---
title: "Install OP.GG Desktop on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Game records and champion analysis"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install OP.GG Desktop on MacOS using homebrew

- App Name: OP.GG Desktop
- App description: Game records and champion analysis
- App Version: 1.0.9
- App Website: https://op.gg/desktop/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install OP.GG Desktop with the following command
   ```
   brew install --cask opgg
   ```
4. OP.GG Desktop is ready to use now!