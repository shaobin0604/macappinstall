---
title: "Install Printrun on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Control your 3D printer from your PC"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Printrun on MacOS using homebrew

- App Name: Printrun
- App description: Control your 3D printer from your PC
- App Version: 2.0.0rc8
- App Website: https://github.com/kliment/Printrun

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Printrun with the following command
   ```
   brew install --cask pronterface
   ```
4. Printrun is ready to use now!