---
title: "Install promtail on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Log agent for Loki"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install promtail on MacOS using homebrew

- App Name: promtail
- App description: Log agent for Loki
- App Version: 2.4.2
- App Website: https://grafana.com/loki

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install promtail with the following command
   ```
   brew install promtail
   ```
4. promtail is ready to use now!