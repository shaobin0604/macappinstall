---
title: "Install Blisk Browser on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Developer-oriented browser"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Blisk Browser on MacOS using homebrew

- App Name: Blisk Browser
- App description: Developer-oriented browser
- App Version: 17.0.158.186
- App Website: https://blisk.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Blisk Browser with the following command
   ```
   brew install --cask blisk
   ```
4. Blisk Browser is ready to use now!