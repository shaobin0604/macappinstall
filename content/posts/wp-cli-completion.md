---
title: "Install wp-cli-completion on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Bash completion for Wpcli"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install wp-cli-completion on MacOS using homebrew

- App Name: wp-cli-completion
- App description: Bash completion for Wpcli
- App Version: 2.6.0
- App Website: https://github.com/wp-cli/wp-cli

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install wp-cli-completion with the following command
   ```
   brew install wp-cli-completion
   ```
4. wp-cli-completion is ready to use now!