---
title: "Install Local on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "WordPress local development tool by Flywheel"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Local on MacOS using homebrew

- App Name: Local
- App description: WordPress local development tool by Flywheel
- App Version: 6.3.0,5756
- App Website: https://localwp.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Local with the following command
   ```
   brew install --cask local
   ```
4. Local is ready to use now!