---
title: "Install Bandage on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Bioinformatics Application for Navigating De novo Assembly Graphs"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Bandage on MacOS using homebrew

- App Name: Bandage
- App description: Bioinformatics Application for Navigating De novo Assembly Graphs
- App Version: 0.8.1
- App Website: https://rrwick.github.io/Bandage/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Bandage with the following command
   ```
   brew install --cask bandage
   ```
4. Bandage is ready to use now!