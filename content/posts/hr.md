---
title: "Install hr on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "<hr />, for your terminal window"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install hr on MacOS using homebrew

- App Name: hr
- App description: <hr />, for your terminal window
- App Version: 1.3
- App Website: https://github.com/LuRsT/hr

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install hr with the following command
   ```
   brew install hr
   ```
4. hr is ready to use now!