---
title: "Install terraform@0.13 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to build, change, and version infrastructure"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install terraform@0.13 on MacOS using homebrew

- App Name: terraform@0.13
- App description: Tool to build, change, and version infrastructure
- App Version: 0.13.7
- App Website: https://www.terraform.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install terraform@0.13 with the following command
   ```
   brew install terraform@0.13
   ```
4. terraform@0.13 is ready to use now!