---
title: "Install PDF-Images on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install PDF-Images on MacOS using homebrew

- App Name: PDF-Images
- App description: null
- App Version: 1.1
- App Website: https://sourceforge.net/projects/pdf-images/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install PDF-Images with the following command
   ```
   brew install --cask pdf-images
   ```
4. PDF-Images is ready to use now!