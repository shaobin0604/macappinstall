---
title: "Install Kap on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Open-source screen recorder built with web technology"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Kap on MacOS using homebrew

- App Name: Kap
- App description: Open-source screen recorder built with web technology
- App Version: 3.5.3
- App Website: https://getkap.co/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Kap with the following command
   ```
   brew install --cask kap
   ```
4. Kap is ready to use now!