---
title: "Install hpack on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Modern format for Haskell packages"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install hpack on MacOS using homebrew

- App Name: hpack
- App description: Modern format for Haskell packages
- App Version: 0.34.6
- App Website: https://github.com/sol/hpack

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install hpack with the following command
   ```
   brew install hpack
   ```
4. hpack is ready to use now!