---
title: "Install jpeg-xl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "New file format for still image compression"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install jpeg-xl on MacOS using homebrew

- App Name: jpeg-xl
- App description: New file format for still image compression
- App Version: 0.6.1
- App Website: https://jpeg.org/jpegxl/index.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install jpeg-xl with the following command
   ```
   brew install jpeg-xl
   ```
4. jpeg-xl is ready to use now!