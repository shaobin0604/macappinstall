---
title: "Install servus on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library and Utilities for zeroconf networking"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install servus on MacOS using homebrew

- App Name: servus
- App description: Library and Utilities for zeroconf networking
- App Version: 1.5.2
- App Website: https://github.com/HBPVIS/Servus

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install servus with the following command
   ```
   brew install servus
   ```
4. servus is ready to use now!