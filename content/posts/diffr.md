---
title: "Install diffr on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "LCS based diff highlighting tool to ease code review from your terminal"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install diffr on MacOS using homebrew

- App Name: diffr
- App description: LCS based diff highlighting tool to ease code review from your terminal
- App Version: 0.1.4
- App Website: https://github.com/mookid/diffr

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install diffr with the following command
   ```
   brew install diffr
   ```
4. diffr is ready to use now!