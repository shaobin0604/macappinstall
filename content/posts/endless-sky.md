---
title: "Install Endless Sky on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Space exploration, trading, and combat game"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Endless Sky on MacOS using homebrew

- App Name: Endless Sky
- App description: Space exploration, trading, and combat game
- App Version: 0.9.14
- App Website: https://endless-sky.github.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Endless Sky with the following command
   ```
   brew install --cask endless-sky
   ```
4. Endless Sky is ready to use now!