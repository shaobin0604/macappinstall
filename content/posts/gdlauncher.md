---
title: "Install GDLauncher on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Custom Minecraft Launcher"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install GDLauncher on MacOS using homebrew

- App Name: GDLauncher
- App description: Custom Minecraft Launcher
- App Version: 1.1.21
- App Website: https://gdevs.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install GDLauncher with the following command
   ```
   brew install --cask gdlauncher
   ```
4. GDLauncher is ready to use now!