---
title: "Install DeathToDSStore on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install DeathToDSStore on MacOS using homebrew

- App Name: DeathToDSStore
- App description: null
- App Version: 1.0.5
- App Website: https://www.aorensoftware.com/blog/2011/12/24/death-to-ds_store/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install DeathToDSStore with the following command
   ```
   brew install --cask deathtodsstore
   ```
4. DeathToDSStore is ready to use now!