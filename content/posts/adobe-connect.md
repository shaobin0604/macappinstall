---
title: "Install Adobe Connect on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Virtual meeting client"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Adobe Connect on MacOS using homebrew

- App Name: Adobe Connect
- App description: Virtual meeting client
- App Version: 11,2021.11.22
- App Website: https://www.adobe.com/products/adobeconnect.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Adobe Connect with the following command
   ```
   brew install --cask adobe-connect
   ```
4. Adobe Connect is ready to use now!