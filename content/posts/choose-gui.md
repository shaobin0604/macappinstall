---
title: "Install choose-gui on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Fuzzy matcher that uses std{in,out} and a native GUI"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install choose-gui on MacOS using homebrew

- App Name: choose-gui
- App description: Fuzzy matcher that uses std{in,out} and a native GUI
- App Version: 1.2.1
- App Website: https://github.com/chipsenkbeil/choose

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install choose-gui with the following command
   ```
   brew install choose-gui
   ```
4. choose-gui is ready to use now!