---
title: "Install Surge on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Hybrid synthesizer"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Surge on MacOS using homebrew

- App Name: Surge
- App description: Hybrid synthesizer
- App Version: 1.9.0
- App Website: https://surge-synthesizer.github.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Surge with the following command
   ```
   brew install --cask surge-synthesizer
   ```
4. Surge is ready to use now!