---
title: "Install tomcat-native on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Lets Tomcat use some native resources for performance"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tomcat-native on MacOS using homebrew

- App Name: tomcat-native
- App description: Lets Tomcat use some native resources for performance
- App Version: 1.2.31
- App Website: https://tomcat.apache.org/native-doc/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tomcat-native with the following command
   ```
   brew install tomcat-native
   ```
4. tomcat-native is ready to use now!