---
title: "Install zebra on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Information management system"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install zebra on MacOS using homebrew

- App Name: zebra
- App description: Information management system
- App Version: 2.2.3
- App Website: https://www.indexdata.com/resources/software/zebra/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install zebra with the following command
   ```
   brew install zebra
   ```
4. zebra is ready to use now!