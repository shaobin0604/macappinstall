---
title: "Install touchosc on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "MIDI and OSC Controller Software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install touchosc on MacOS using homebrew

- App Name: touchosc
- App description: MIDI and OSC Controller Software
- App Version: 1.1.0,132
- App Website: https://hexler.net/touchosc

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install touchosc with the following command
   ```
   brew install --cask touchosc
   ```
4. touchosc is ready to use now!