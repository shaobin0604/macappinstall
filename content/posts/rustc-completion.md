---
title: "Install rustc-completion on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Bash completion for rustc"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install rustc-completion on MacOS using homebrew

- App Name: rustc-completion
- App description: Bash completion for rustc
- App Version: 0.12.1
- App Website: https://github.com/roshan/rust-bash-completion

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install rustc-completion with the following command
   ```
   brew install rustc-completion
   ```
4. rustc-completion is ready to use now!