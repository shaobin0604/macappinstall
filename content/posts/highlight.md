---
title: "Install highlight on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Convert source code to formatted text with syntax highlighting"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install highlight on MacOS using homebrew

- App Name: highlight
- App description: Convert source code to formatted text with syntax highlighting
- App Version: 4.1
- App Website: http://www.andre-simon.de/doku/highlight/en/highlight.php

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install highlight with the following command
   ```
   brew install highlight
   ```
4. highlight is ready to use now!