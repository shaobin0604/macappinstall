---
title: "Install TheDesk on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Mastodon/Misskey Client for PC"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install TheDesk on MacOS using homebrew

- App Name: TheDesk
- App description: Mastodon/Misskey Client for PC
- App Version: 22.3.1
- App Website: https://thedesk.top/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install TheDesk with the following command
   ```
   brew install --cask thedesk
   ```
4. TheDesk is ready to use now!