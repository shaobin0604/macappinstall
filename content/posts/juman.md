---
title: "Install juman on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Japanese morphological analysis system"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install juman on MacOS using homebrew

- App Name: juman
- App description: Japanese morphological analysis system
- App Version: 7.01
- App Website: https://nlp.ist.i.kyoto-u.ac.jp/index.php?JUMAN

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install juman with the following command
   ```
   brew install juman
   ```
4. juman is ready to use now!