---
title: "Install PixelStick on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool for measuring distances, angles and colors on the screen"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install PixelStick on MacOS using homebrew

- App Name: PixelStick
- App description: Tool for measuring distances, angles and colors on the screen
- App Version: 2.16.2
- App Website: https://plumamazing.com/product/pixelstick

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install PixelStick with the following command
   ```
   brew install --cask pixelstick
   ```
4. PixelStick is ready to use now!