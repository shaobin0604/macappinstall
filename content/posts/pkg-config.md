---
title: "Install pkg-config on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Manage compile and link flags for libraries"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pkg-config on MacOS using homebrew

- App Name: pkg-config
- App description: Manage compile and link flags for libraries
- App Version: 0.29.2
- App Website: https://freedesktop.org/wiki/Software/pkg-config/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pkg-config with the following command
   ```
   brew install pkg-config
   ```
4. pkg-config is ready to use now!