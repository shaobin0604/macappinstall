---
title: "Install tnftpd on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "NetBSD's FTP server"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tnftpd on MacOS using homebrew

- App Name: tnftpd
- App description: NetBSD's FTP server
- App Version: 20200704
- App Website: https://cdn.netbsd.org/pub/NetBSD/misc/tnftp/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tnftpd with the following command
   ```
   brew install tnftpd
   ```
4. tnftpd is ready to use now!