---
title: "Install bullet on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Physics SDK"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bullet on MacOS using homebrew

- App Name: bullet
- App description: Physics SDK
- App Version: 3.21
- App Website: https://bulletphysics.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bullet with the following command
   ```
   brew install bullet
   ```
4. bullet is ready to use now!