---
title: "Install libspnav on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Client library for connecting to 3Dconnexion's 3D input devices"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libspnav on MacOS using homebrew

- App Name: libspnav
- App description: Client library for connecting to 3Dconnexion's 3D input devices
- App Version: 0.3
- App Website: https://spacenav.sourceforge.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libspnav with the following command
   ```
   brew install libspnav
   ```
4. libspnav is ready to use now!