---
title: "Install mighttpd2 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "HTTP server"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mighttpd2 on MacOS using homebrew

- App Name: mighttpd2
- App description: HTTP server
- App Version: 4.0.2
- App Website: https://kazu-yamamoto.github.io/mighttpd2/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mighttpd2 with the following command
   ```
   brew install mighttpd2
   ```
4. mighttpd2 is ready to use now!