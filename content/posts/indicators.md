---
title: "Install indicators on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Activity indicators for modern C++"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install indicators on MacOS using homebrew

- App Name: indicators
- App description: Activity indicators for modern C++
- App Version: 2.2
- App Website: https://github.com/p-ranav/indicators

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install indicators with the following command
   ```
   brew install indicators
   ```
4. indicators is ready to use now!