---
title: "Install terraforming on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Export existing AWS resources to Terraform style (tf, tfstate)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install terraforming on MacOS using homebrew

- App Name: terraforming
- App description: Export existing AWS resources to Terraform style (tf, tfstate)
- App Version: 0.18.0
- App Website: https://terraforming.dtan4.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install terraforming with the following command
   ```
   brew install terraforming
   ```
4. terraforming is ready to use now!