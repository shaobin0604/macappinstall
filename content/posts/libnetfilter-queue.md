---
title: "Install libnetfilter-queue on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Userspace API to packets queued by the kernel packet filter"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libnetfilter-queue on MacOS using homebrew

- App Name: libnetfilter-queue
- App description: Userspace API to packets queued by the kernel packet filter
- App Version: 1.0.5
- App Website: https://www.netfilter.org/projects/libnetfilter_queue

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libnetfilter-queue with the following command
   ```
   brew install libnetfilter-queue
   ```
4. libnetfilter-queue is ready to use now!