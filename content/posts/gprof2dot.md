---
title: "Install gprof2dot on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Convert the output from many profilers into a Graphviz dot graph"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gprof2dot on MacOS using homebrew

- App Name: gprof2dot
- App description: Convert the output from many profilers into a Graphviz dot graph
- App Version: 2021.2.21
- App Website: https://github.com/jrfonseca/gprof2dot

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gprof2dot with the following command
   ```
   brew install gprof2dot
   ```
4. gprof2dot is ready to use now!