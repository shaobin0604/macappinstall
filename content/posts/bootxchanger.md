---
title: "Install BootXChanger on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utility to change the boot logo on old Macs"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install BootXChanger on MacOS using homebrew

- App Name: BootXChanger
- App description: Utility to change the boot logo on old Macs
- App Version: 2.0
- App Website: https://namedfork.net/bootxchanger/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install BootXChanger with the following command
   ```
   brew install --cask bootxchanger
   ```
4. BootXChanger is ready to use now!