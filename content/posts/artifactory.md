---
title: "Install artifactory on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Manages binaries"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install artifactory on MacOS using homebrew

- App Name: artifactory
- App description: Manages binaries
- App Version: 6.23.38
- App Website: https://www.jfrog.com/artifactory/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install artifactory with the following command
   ```
   brew install artifactory
   ```
4. artifactory is ready to use now!