---
title: "Install regldg on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Regular expression grammar language dictionary generator"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install regldg on MacOS using homebrew

- App Name: regldg
- App description: Regular expression grammar language dictionary generator
- App Version: 1.0.0
- App Website: https://regldg.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install regldg with the following command
   ```
   brew install regldg
   ```
4. regldg is ready to use now!