---
title: "Install webkit2png on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Create screenshots of webpages from the terminal"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install webkit2png on MacOS using homebrew

- App Name: webkit2png
- App description: Create screenshots of webpages from the terminal
- App Version: 0.7
- App Website: https://www.paulhammond.org/webkit2png/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install webkit2png with the following command
   ```
   brew install webkit2png
   ```
4. webkit2png is ready to use now!