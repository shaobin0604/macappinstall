---
title: "Install dhcping on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Perform a dhcp-request to check whether a dhcp-server is running"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dhcping on MacOS using homebrew

- App Name: dhcping
- App description: Perform a dhcp-request to check whether a dhcp-server is running
- App Version: 1.2
- App Website: https://www.mavetju.org/unix/general.php

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dhcping with the following command
   ```
   brew install dhcping
   ```
4. dhcping is ready to use now!