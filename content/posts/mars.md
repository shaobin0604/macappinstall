---
title: "Install MARS on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Mips Assembly and Runtime Simulator"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install MARS on MacOS using homebrew

- App Name: MARS
- App description: Mips Assembly and Runtime Simulator
- App Version: 4.5,Aug2014
- App Website: https://courses.missouristate.edu/KenVollmar/mars/index.htm

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install MARS with the following command
   ```
   brew install --cask mars
   ```
4. MARS is ready to use now!