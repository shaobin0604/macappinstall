---
title: "Install fon-flash-cli on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Flash La Fonera and Atheros chipset compatible devices"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fon-flash-cli on MacOS using homebrew

- App Name: fon-flash-cli
- App description: Flash La Fonera and Atheros chipset compatible devices
- App Version: 1.12.0
- App Website: https://www.gargoyle-router.com/wiki/doku.php?id=fon_flash

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fon-flash-cli with the following command
   ```
   brew install fon-flash-cli
   ```
4. fon-flash-cli is ready to use now!