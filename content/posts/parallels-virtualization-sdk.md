---
title: "Install Parallels Virtualization SDK on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Desktop virtualization development kit"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Parallels Virtualization SDK on MacOS using homebrew

- App Name: Parallels Virtualization SDK
- App description: Desktop virtualization development kit
- App Version: 17.1.1-51537
- App Website: https://www.parallels.com/products/desktop/download/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Parallels Virtualization SDK with the following command
   ```
   brew install --cask parallels-virtualization-sdk
   ```
4. Parallels Virtualization SDK is ready to use now!