---
title: "Install mdp on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line based markdown presentation tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mdp on MacOS using homebrew

- App Name: mdp
- App description: Command-line based markdown presentation tool
- App Version: 1.0.15
- App Website: https://github.com/visit1985/mdp

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mdp with the following command
   ```
   brew install mdp
   ```
4. mdp is ready to use now!