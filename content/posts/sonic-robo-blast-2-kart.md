---
title: "Install Sonic Robo Blast 2 Kart on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Classic styled kart racer, complete with beautiful courses, and wacky items"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Sonic Robo Blast 2 Kart on MacOS using homebrew

- App Name: Sonic Robo Blast 2 Kart
- App description: Classic styled kart racer, complete with beautiful courses, and wacky items
- App Version: 1.3
- App Website: https://mb.srb2.org/addons/srb2kart.2435/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Sonic Robo Blast 2 Kart with the following command
   ```
   brew install --cask sonic-robo-blast-2-kart
   ```
4. Sonic Robo Blast 2 Kart is ready to use now!