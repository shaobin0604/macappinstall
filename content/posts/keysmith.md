---
title: "Install Keysmith on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Create custom keyboard shortcuts for anything"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Keysmith on MacOS using homebrew

- App Name: Keysmith
- App description: Create custom keyboard shortcuts for anything
- App Version: 1.17.1,92
- App Website: https://www.keysmith.app/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Keysmith with the following command
   ```
   brew install --cask keysmith
   ```
4. Keysmith is ready to use now!