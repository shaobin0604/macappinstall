---
title: "Install webp-pixbuf-loader on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "WebP Image format GdkPixbuf loader"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install webp-pixbuf-loader on MacOS using homebrew

- App Name: webp-pixbuf-loader
- App description: WebP Image format GdkPixbuf loader
- App Version: 0.0.4
- App Website: https://github.com/aruiz/webp-pixbuf-loader

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install webp-pixbuf-loader with the following command
   ```
   brew install webp-pixbuf-loader
   ```
4. webp-pixbuf-loader is ready to use now!