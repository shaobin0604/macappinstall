---
title: "Install One Switch on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "All system and utility switches in one place"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install One Switch on MacOS using homebrew

- App Name: One Switch
- App description: All system and utility switches in one place
- App Version: 1.24.1,354
- App Website: https://fireball.studio/oneswitch

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install One Switch with the following command
   ```
   brew install --cask one-switch
   ```
4. One Switch is ready to use now!