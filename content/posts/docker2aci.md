---
title: "Install docker2aci on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library and CLI tool to convert Docker images to ACIs"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install docker2aci on MacOS using homebrew

- App Name: docker2aci
- App description: Library and CLI tool to convert Docker images to ACIs
- App Version: 0.17.2
- App Website: https://github.com/appc/docker2aci

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install docker2aci with the following command
   ```
   brew install docker2aci
   ```
4. docker2aci is ready to use now!