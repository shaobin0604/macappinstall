---
title: "Install grails on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Web application framework for the Groovy language"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install grails on MacOS using homebrew

- App Name: grails
- App description: Web application framework for the Groovy language
- App Version: 5.1.2
- App Website: https://grails.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install grails with the following command
   ```
   brew install grails
   ```
4. grails is ready to use now!