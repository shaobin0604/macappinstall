---
title: "Install gwenhywfar on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utility library required by aqbanking and related software"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gwenhywfar on MacOS using homebrew

- App Name: gwenhywfar
- App description: Utility library required by aqbanking and related software
- App Version: 5.8.2
- App Website: https://www.aquamaniac.de/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gwenhywfar with the following command
   ```
   brew install gwenhywfar
   ```
4. gwenhywfar is ready to use now!