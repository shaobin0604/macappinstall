---
title: "Install aescrypt-packetizer on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Encrypt and decrypt using 256-bit AES encryption"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install aescrypt-packetizer on MacOS using homebrew

- App Name: aescrypt-packetizer
- App description: Encrypt and decrypt using 256-bit AES encryption
- App Version: 3.16
- App Website: https://www.aescrypt.com

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install aescrypt-packetizer with the following command
   ```
   brew install aescrypt-packetizer
   ```
4. aescrypt-packetizer is ready to use now!