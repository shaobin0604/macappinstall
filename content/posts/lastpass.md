---
title: "Install LastPass on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Password manager"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install LastPass on MacOS using homebrew

- App Name: LastPass
- App description: Password manager
- App Version: 4.89.0,1225
- App Website: https://www.lastpass.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install LastPass with the following command
   ```
   brew install --cask lastpass
   ```
4. LastPass is ready to use now!