---
title: "Install zk on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Plain text note-taking assistant"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install zk on MacOS using homebrew

- App Name: zk
- App description: Plain text note-taking assistant
- App Version: 0.9.0
- App Website: https://github.com/mickael-menu/zk

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install zk with the following command
   ```
   brew install zk
   ```
4. zk is ready to use now!