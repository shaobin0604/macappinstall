---
title: "Install ssh-permit-a38 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Central management and deployment for SSH keys"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ssh-permit-a38 on MacOS using homebrew

- App Name: ssh-permit-a38
- App description: Central management and deployment for SSH keys
- App Version: 0.2.0
- App Website: https://github.com/ierror/ssh-permit-a38

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ssh-permit-a38 with the following command
   ```
   brew install ssh-permit-a38
   ```
4. ssh-permit-a38 is ready to use now!