---
title: "Install lesstif on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Open source implementation of OSF/Motif"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install lesstif on MacOS using homebrew

- App Name: lesstif
- App description: Open source implementation of OSF/Motif
- App Version: 0.95.2
- App Website: https://lesstif.sourceforge.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install lesstif with the following command
   ```
   brew install lesstif
   ```
4. lesstif is ready to use now!