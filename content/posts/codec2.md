---
title: "Install codec2 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Open source speech codec"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install codec2 on MacOS using homebrew

- App Name: codec2
- App description: Open source speech codec
- App Version: 1.0.3
- App Website: https://www.rowetel.com/?page_id=452

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install codec2 with the following command
   ```
   brew install codec2
   ```
4. codec2 is ready to use now!