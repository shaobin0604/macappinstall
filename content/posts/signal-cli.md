---
title: "Install signal-cli on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CLI and dbus interface for WhisperSystems/libsignal-service-java"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install signal-cli on MacOS using homebrew

- App Name: signal-cli
- App description: CLI and dbus interface for WhisperSystems/libsignal-service-java
- App Version: 0.9.0
- App Website: https://github.com/AsamK/signal-cli

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install signal-cli with the following command
   ```
   brew install signal-cli
   ```
4. signal-cli is ready to use now!