---
title: "Install FS-UAE Launcher on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Amiga emulator launcher"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install FS-UAE Launcher on MacOS using homebrew

- App Name: FS-UAE Launcher
- App description: Amiga emulator launcher
- App Version: 3.1.66
- App Website: https://fs-uae.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install FS-UAE Launcher with the following command
   ```
   brew install --cask fs-uae-launcher
   ```
4. FS-UAE Launcher is ready to use now!