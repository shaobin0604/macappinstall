---
title: "Install helmsman on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Helm Charts as Code tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install helmsman on MacOS using homebrew

- App Name: helmsman
- App description: Helm Charts as Code tool
- App Version: 3.8.1
- App Website: https://github.com/Praqma/helmsman

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install helmsman with the following command
   ```
   brew install helmsman
   ```
4. helmsman is ready to use now!