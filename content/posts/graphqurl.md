---
title: "Install graphqurl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Curl for GraphQL with autocomplete, subscriptions and GraphiQL"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install graphqurl on MacOS using homebrew

- App Name: graphqurl
- App description: Curl for GraphQL with autocomplete, subscriptions and GraphiQL
- App Version: 1.0.1
- App Website: https://github.com/hasura/graphqurl

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install graphqurl with the following command
   ```
   brew install graphqurl
   ```
4. graphqurl is ready to use now!