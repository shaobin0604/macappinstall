---
title: "Install bloaty on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Size profiler for binaries"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bloaty on MacOS using homebrew

- App Name: bloaty
- App description: Size profiler for binaries
- App Version: 1.1
- App Website: https://github.com/google/bloaty

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bloaty with the following command
   ```
   brew install bloaty
   ```
4. bloaty is ready to use now!