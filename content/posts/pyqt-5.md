---
title: "Install pyqt@5 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Python bindings for v5 of Qt"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pyqt@5 on MacOS using homebrew

- App Name: pyqt@5
- App description: Python bindings for v5 of Qt
- App Version: 5.15.6
- App Website: https://www.riverbankcomputing.com/software/pyqt/intro

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pyqt@5 with the following command
   ```
   brew install pyqt@5
   ```
4. pyqt@5 is ready to use now!