---
title: "Install inso on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CLI HTTP and GraphQL Client"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install inso on MacOS using homebrew

- App Name: inso
- App description: CLI HTTP and GraphQL Client
- App Version: 2.4.1
- App Website: https://insomnia.rest/products/inso

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install inso with the following command
   ```
   brew install --cask inso
   ```
4. inso is ready to use now!