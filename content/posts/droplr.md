---
title: "Install Droplr on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Screenshot and screen recorder"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Droplr on MacOS using homebrew

- App Name: Droplr
- App description: Screenshot and screen recorder
- App Version: 5.9.16,475
- App Website: https://droplr.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Droplr with the following command
   ```
   brew install --cask droplr
   ```
4. Droplr is ready to use now!