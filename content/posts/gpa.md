---
title: "Install gpa on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Graphical user interface for the GnuPG"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gpa on MacOS using homebrew

- App Name: gpa
- App description: Graphical user interface for the GnuPG
- App Version: 0.10.0
- App Website: https://www.gnupg.org/related_software/gpa/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gpa with the following command
   ```
   brew install gpa
   ```
4. gpa is ready to use now!