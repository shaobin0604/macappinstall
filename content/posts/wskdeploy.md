---
title: "Install wskdeploy on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Apache OpenWhisk project deployment utility"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install wskdeploy on MacOS using homebrew

- App Name: wskdeploy
- App description: Apache OpenWhisk project deployment utility
- App Version: 1.2.0
- App Website: https://openwhisk.apache.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install wskdeploy with the following command
   ```
   brew install wskdeploy
   ```
4. wskdeploy is ready to use now!