---
title: "Install libltc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "POSIX-C Library for handling Linear/Logitudinal Time Code (LTC)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libltc on MacOS using homebrew

- App Name: libltc
- App description: POSIX-C Library for handling Linear/Logitudinal Time Code (LTC)
- App Version: 1.3.1
- App Website: https://x42.github.io/libltc/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libltc with the following command
   ```
   brew install libltc
   ```
4. libltc is ready to use now!