---
title: "Install swift on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "High-performance system programming language"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install swift on MacOS using homebrew

- App Name: swift
- App description: High-performance system programming language
- App Version: 5.5.2
- App Website: https://www.swift.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install swift with the following command
   ```
   brew install swift
   ```
4. swift is ready to use now!