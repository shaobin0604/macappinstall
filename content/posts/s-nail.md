---
title: "Install s-nail on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Fork of Heirloom mailx"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install s-nail on MacOS using homebrew

- App Name: s-nail
- App description: Fork of Heirloom mailx
- App Version: 14.9.23
- App Website: https://www.sdaoden.eu/code.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install s-nail with the following command
   ```
   brew install s-nail
   ```
4. s-nail is ready to use now!