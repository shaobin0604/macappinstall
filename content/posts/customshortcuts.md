---
title: "Install CustomShortcuts on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Customize menu item keyboard shortcuts"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install CustomShortcuts on MacOS using homebrew

- App Name: CustomShortcuts
- App description: Customize menu item keyboard shortcuts
- App Version: 1.1,102
- App Website: https://www.houdah.com/customShortcuts/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install CustomShortcuts with the following command
   ```
   brew install --cask customshortcuts
   ```
4. CustomShortcuts is ready to use now!