---
title: "Install jose on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C-language implementation of Javascript Object Signing and Encryption"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install jose on MacOS using homebrew

- App Name: jose
- App description: C-language implementation of Javascript Object Signing and Encryption
- App Version: 11
- App Website: https://github.com/latchset/jose

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install jose with the following command
   ```
   brew install jose
   ```
4. jose is ready to use now!