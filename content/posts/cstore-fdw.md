---
title: "Install cstore_fdw on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Columnar store for analytics with Postgres"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cstore_fdw on MacOS using homebrew

- App Name: cstore_fdw
- App description: Columnar store for analytics with Postgres
- App Version: 1.7.0
- App Website: https://github.com/citusdata/cstore_fdw

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cstore_fdw with the following command
   ```
   brew install cstore_fdw
   ```
4. cstore_fdw is ready to use now!