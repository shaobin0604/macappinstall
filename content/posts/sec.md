---
title: "Install sec on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Event correlation tool for event processing of various kinds"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sec on MacOS using homebrew

- App Name: sec
- App description: Event correlation tool for event processing of various kinds
- App Version: 2.9.0
- App Website: https://simple-evcorr.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sec with the following command
   ```
   brew install sec
   ```
4. sec is ready to use now!