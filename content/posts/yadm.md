---
title: "Install yadm on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Yet Another Dotfiles Manager"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install yadm on MacOS using homebrew

- App Name: yadm
- App description: Yet Another Dotfiles Manager
- App Version: 3.1.1
- App Website: https://yadm.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install yadm with the following command
   ```
   brew install yadm
   ```
4. yadm is ready to use now!