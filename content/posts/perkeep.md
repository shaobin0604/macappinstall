---
title: "Install perkeep on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Lets you permanently keep your stuff, for life"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install perkeep on MacOS using homebrew

- App Name: perkeep
- App description: Lets you permanently keep your stuff, for life
- App Version: 0.11
- App Website: https://perkeep.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install perkeep with the following command
   ```
   brew install perkeep
   ```
4. perkeep is ready to use now!