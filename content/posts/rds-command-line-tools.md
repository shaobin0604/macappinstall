---
title: "Install rds-command-line-tools on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Amazon RDS command-line toolkit"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install rds-command-line-tools on MacOS using homebrew

- App Name: rds-command-line-tools
- App description: Amazon RDS command-line toolkit
- App Version: 1.19.004
- App Website: https://aws.amazon.com/developertools/2928

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install rds-command-line-tools with the following command
   ```
   brew install rds-command-line-tools
   ```
4. rds-command-line-tools is ready to use now!