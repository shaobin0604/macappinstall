---
title: "Install IPA Palette on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install IPA Palette on MacOS using homebrew

- App Name: IPA Palette
- App description: null
- App Version: 2.3.2,2020.01.26
- App Website: https://www.blugs.com/IPA/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install IPA Palette with the following command
   ```
   brew install --cask ipa-manager
   ```
4. IPA Palette is ready to use now!