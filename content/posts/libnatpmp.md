---
title: "Install libnatpmp on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "NAT port mapping protocol library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libnatpmp on MacOS using homebrew

- App Name: libnatpmp
- App description: NAT port mapping protocol library
- App Version: 20150609
- App Website: http://miniupnp.free.fr/libnatpmp.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libnatpmp with the following command
   ```
   brew install libnatpmp
   ```
4. libnatpmp is ready to use now!