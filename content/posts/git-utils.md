---
title: "Install git-utils on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Various Git helper utilities"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install git-utils on MacOS using homebrew

- App Name: git-utils
- App description: Various Git helper utilities
- App Version: 1.0
- App Website: https://github.com/ddollar/git-utils

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install git-utils with the following command
   ```
   brew install git-utils
   ```
4. git-utils is ready to use now!