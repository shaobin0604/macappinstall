---
title: "Install gucharmap on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GNOME Character Map, based on the Unicode Character Database"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gucharmap on MacOS using homebrew

- App Name: gucharmap
- App description: GNOME Character Map, based on the Unicode Character Database
- App Version: 12.0.1
- App Website: https://wiki.gnome.org/Apps/Gucharmap

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gucharmap with the following command
   ```
   brew install gucharmap
   ```
4. gucharmap is ready to use now!