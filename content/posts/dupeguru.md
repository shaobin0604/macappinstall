---
title: "Install dupeGuru on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Finds duplicate files in a computer system"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dupeGuru on MacOS using homebrew

- App Name: dupeGuru
- App description: Finds duplicate files in a computer system
- App Version: 4.1.1
- App Website: https://dupeguru.voltaicideas.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dupeGuru with the following command
   ```
   brew install --cask dupeguru
   ```
4. dupeGuru is ready to use now!