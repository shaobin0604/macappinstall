---
title: "Install cdo on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Climate Data Operators"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cdo on MacOS using homebrew

- App Name: cdo
- App description: Climate Data Operators
- App Version: 2.0.3
- App Website: https://code.mpimet.mpg.de/projects/cdo

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cdo with the following command
   ```
   brew install cdo
   ```
4. cdo is ready to use now!