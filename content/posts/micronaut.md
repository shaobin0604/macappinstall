---
title: "Install micronaut on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Modern JVM-based framework for building modular microservices"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install micronaut on MacOS using homebrew

- App Name: micronaut
- App description: Modern JVM-based framework for building modular microservices
- App Version: 3.3.3
- App Website: https://micronaut.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install micronaut with the following command
   ```
   brew install micronaut
   ```
4. micronaut is ready to use now!