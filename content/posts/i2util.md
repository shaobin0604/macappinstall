---
title: "Install i2util on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Internet2 utility tools"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install i2util on MacOS using homebrew

- App Name: i2util
- App description: Internet2 utility tools
- App Version: 1.2
- App Website: https://software.internet2.edu/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install i2util with the following command
   ```
   brew install i2util
   ```
4. i2util is ready to use now!