---
title: "Install QBlocker on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Stops you from accidentally quitting an app"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install QBlocker on MacOS using homebrew

- App Name: QBlocker
- App description: Stops you from accidentally quitting an app
- App Version: 1.2,20,1464612307
- App Website: https://qblocker.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install QBlocker with the following command
   ```
   brew install --cask qblocker
   ```
4. QBlocker is ready to use now!