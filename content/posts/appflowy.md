---
title: "Install appflowy on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Open-source project and knowledge management tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install appflowy on MacOS using homebrew

- App Name: appflowy
- App description: Open-source project and knowledge management tool
- App Version: 0.0.2
- App Website: https://www.appflowy.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install appflowy with the following command
   ```
   brew install --cask appflowy
   ```
4. appflowy is ready to use now!