---
title: "Install sbcl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Steel Bank Common Lisp system"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sbcl on MacOS using homebrew

- App Name: sbcl
- App description: Steel Bank Common Lisp system
- App Version: 2.1.9
- App Website: http://www.sbcl.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sbcl with the following command
   ```
   brew install sbcl
   ```
4. sbcl is ready to use now!