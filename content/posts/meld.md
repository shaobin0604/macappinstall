---
title: "Install Meld for OSX on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Visual diff and merge tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Meld for OSX on MacOS using homebrew

- App Name: Meld for OSX
- App description: Visual diff and merge tool
- App Version: 3.21.0-r3,19
- App Website: https://yousseb.github.io/meld/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Meld for OSX with the following command
   ```
   brew install --cask meld
   ```
4. Meld for OSX is ready to use now!