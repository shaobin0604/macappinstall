---
title: "Install bfg on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Remove large files or passwords from Git history like git-filter-branch"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bfg on MacOS using homebrew

- App Name: bfg
- App description: Remove large files or passwords from Git history like git-filter-branch
- App Version: 1.14.0
- App Website: https://rtyley.github.io/bfg-repo-cleaner/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bfg with the following command
   ```
   brew install bfg
   ```
4. bfg is ready to use now!