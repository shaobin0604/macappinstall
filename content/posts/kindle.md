---
title: "Install Kindle for Mac on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Interface for reading and syncing eBooks"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Kindle for Mac on MacOS using homebrew

- App Name: Kindle for Mac
- App description: Interface for reading and syncing eBooks
- App Version: 1.34.63102
- App Website: https://www.amazon.com/gp/digital/fiona/kcp-landing-page

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Kindle for Mac with the following command
   ```
   brew install --cask kindle
   ```
4. Kindle for Mac is ready to use now!