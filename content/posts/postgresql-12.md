---
title: "Install postgresql@12 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Object-relational database system"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install postgresql@12 on MacOS using homebrew

- App Name: postgresql@12
- App description: Object-relational database system
- App Version: 12.9
- App Website: https://www.postgresql.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install postgresql@12 with the following command
   ```
   brew install postgresql@12
   ```
4. postgresql@12 is ready to use now!