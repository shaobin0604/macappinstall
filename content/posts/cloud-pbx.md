---
title: "Install Cloud PBX on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cloud-based telephone system"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Cloud PBX on MacOS using homebrew

- App Name: Cloud PBX
- App description: Cloud-based telephone system
- App Version: 22.9.20.159
- App Website: https://geschaeftskunden.telekom.de/internet-dsl/tarife/festnetz-internet-dsl/companyflex/cloud-pbx

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Cloud PBX with the following command
   ```
   brew install --cask cloud-pbx
   ```
4. Cloud PBX is ready to use now!