---
title: "Install pdf-redact-tools on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Securely redacting and stripping metadata"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pdf-redact-tools on MacOS using homebrew

- App Name: pdf-redact-tools
- App description: Securely redacting and stripping metadata
- App Version: 0.1.2
- App Website: https://github.com/firstlookmedia/pdf-redact-tools

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pdf-redact-tools with the following command
   ```
   brew install pdf-redact-tools
   ```
4. pdf-redact-tools is ready to use now!