---
title: "Install ipfs on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Peer-to-peer hypermedia protocol"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ipfs on MacOS using homebrew

- App Name: ipfs
- App description: Peer-to-peer hypermedia protocol
- App Version: 0.11.0
- App Website: https://ipfs.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ipfs with the following command
   ```
   brew install ipfs
   ```
4. ipfs is ready to use now!