---
title: "Install shairport-sync on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "AirTunes emulator that adds multi-room capability"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install shairport-sync on MacOS using homebrew

- App Name: shairport-sync
- App description: AirTunes emulator that adds multi-room capability
- App Version: 3.3.9
- App Website: https://github.com/mikebrady/shairport-sync

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install shairport-sync with the following command
   ```
   brew install shairport-sync
   ```
4. shairport-sync is ready to use now!