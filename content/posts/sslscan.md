---
title: "Install sslscan on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Test SSL/TLS enabled services to discover supported cipher suites"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sslscan on MacOS using homebrew

- App Name: sslscan
- App description: Test SSL/TLS enabled services to discover supported cipher suites
- App Version: 2.0.11
- App Website: https://github.com/rbsec/sslscan

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sslscan with the following command
   ```
   brew install sslscan
   ```
4. sslscan is ready to use now!