---
title: "Install Discord on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Voice and text chat software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Discord on MacOS using homebrew

- App Name: Discord
- App description: Voice and text chat software
- App Version: 0.0.264
- App Website: https://discord.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Discord with the following command
   ```
   brew install --cask discord
   ```
4. Discord is ready to use now!