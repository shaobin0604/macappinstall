---
title: "Install go-statik on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Embed files into a Go executable"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install go-statik on MacOS using homebrew

- App Name: go-statik
- App description: Embed files into a Go executable
- App Version: 0.1.7
- App Website: https://github.com/rakyll/statik

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install go-statik with the following command
   ```
   brew install go-statik
   ```
4. go-statik is ready to use now!