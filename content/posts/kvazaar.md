---
title: "Install kvazaar on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Ultravideo HEVC encoder"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install kvazaar on MacOS using homebrew

- App Name: kvazaar
- App description: Ultravideo HEVC encoder
- App Version: 2.1.0
- App Website: https://github.com/ultravideo/kvazaar

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install kvazaar with the following command
   ```
   brew install kvazaar
   ```
4. kvazaar is ready to use now!