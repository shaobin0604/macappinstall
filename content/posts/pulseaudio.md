---
title: "Install pulseaudio on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Sound system for POSIX OSes"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pulseaudio on MacOS using homebrew

- App Name: pulseaudio
- App description: Sound system for POSIX OSes
- App Version: 14.2
- App Website: https://wiki.freedesktop.org/www/Software/PulseAudio/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pulseaudio with the following command
   ```
   brew install pulseaudio
   ```
4. pulseaudio is ready to use now!