---
title: "Install dolt on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Git for Data"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dolt on MacOS using homebrew

- App Name: dolt
- App description: Git for Data
- App Version: 0.37.1
- App Website: https://github.com/dolthub/dolt

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dolt with the following command
   ```
   brew install dolt
   ```
4. dolt is ready to use now!