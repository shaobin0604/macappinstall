---
title: "Install modgit on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool for git repo deploy with filters. Used for magento development"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install modgit on MacOS using homebrew

- App Name: modgit
- App description: Tool for git repo deploy with filters. Used for magento development
- App Version: 1.1.0
- App Website: https://github.com/jreinke/modgit

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install modgit with the following command
   ```
   brew install modgit
   ```
4. modgit is ready to use now!