---
title: "Install trash-cli on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line interface to the freedesktop.org trashcan"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install trash-cli on MacOS using homebrew

- App Name: trash-cli
- App description: Command-line interface to the freedesktop.org trashcan
- App Version: 0.21.10.24
- App Website: https://github.com/andreafrancia/trash-cli

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install trash-cli with the following command
   ```
   brew install trash-cli
   ```
4. trash-cli is ready to use now!