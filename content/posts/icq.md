---
title: "Install ICQ on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Messenger application"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ICQ on MacOS using homebrew

- App Name: ICQ
- App description: Messenger application
- App Version: 3.0.32393
- App Website: https://icq.com/desktop

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ICQ with the following command
   ```
   brew install --cask icq
   ```
4. ICQ is ready to use now!