---
title: "Install zsh-lovers on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tips, tricks, and examples for zsh"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install zsh-lovers on MacOS using homebrew

- App Name: zsh-lovers
- App description: Tips, tricks, and examples for zsh
- App Version: 0.9.1
- App Website: https://grml.org/zsh/#zshlovers

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install zsh-lovers with the following command
   ```
   brew install zsh-lovers
   ```
4. zsh-lovers is ready to use now!