---
title: "Install k3sup on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utility to create k3s clusters on any local or remote VM"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install k3sup on MacOS using homebrew

- App Name: k3sup
- App description: Utility to create k3s clusters on any local or remote VM
- App Version: 0.11.3
- App Website: https://k3sup.dev

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install k3sup with the following command
   ```
   brew install k3sup
   ```
4. k3sup is ready to use now!