---
title: "Install grepcidr on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Filter IP addresses matching IPv4 CIDR/network specification"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install grepcidr on MacOS using homebrew

- App Name: grepcidr
- App description: Filter IP addresses matching IPv4 CIDR/network specification
- App Version: 2.0
- App Website: http://www.pc-tools.net/unix/grepcidr/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install grepcidr with the following command
   ```
   brew install grepcidr
   ```
4. grepcidr is ready to use now!