---
title: "Install Sonic 3 A.I.R. on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Reimplementation of Sonic 3 & Knuckles (requires original game)"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Sonic 3 A.I.R. on MacOS using homebrew

- App Name: Sonic 3 A.I.R.
- App description: Reimplementation of Sonic 3 & Knuckles (requires original game)
- App Version: 21.09.28.0
- App Website: https://sonic3air.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Sonic 3 A.I.R. with the following command
   ```
   brew install --cask sonic3air
   ```
4. Sonic 3 A.I.R. is ready to use now!