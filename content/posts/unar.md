---
title: "Install unar on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line unarchiving tools supporting multiple formats"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install unar on MacOS using homebrew

- App Name: unar
- App description: Command-line unarchiving tools supporting multiple formats
- App Version: 1.10.7
- App Website: https://theunarchiver.com/command-line

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install unar with the following command
   ```
   brew install unar
   ```
4. unar is ready to use now!