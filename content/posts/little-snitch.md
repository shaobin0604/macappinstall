---
title: "Install Little Snitch on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Host-based application firewall"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Little Snitch on MacOS using homebrew

- App Name: Little Snitch
- App description: Host-based application firewall
- App Version: 5.3.2
- App Website: https://www.obdev.at/products/littlesnitch/index.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Little Snitch with the following command
   ```
   brew install --cask little-snitch
   ```
4. Little Snitch is ready to use now!