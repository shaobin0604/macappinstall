---
title: "Install oath-toolkit on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tools for one-time password authentication systems"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install oath-toolkit on MacOS using homebrew

- App Name: oath-toolkit
- App description: Tools for one-time password authentication systems
- App Version: 2.6.6
- App Website: https://www.nongnu.org/oath-toolkit/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install oath-toolkit with the following command
   ```
   brew install oath-toolkit
   ```
4. oath-toolkit is ready to use now!