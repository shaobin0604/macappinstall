---
title: "Install deployer on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Deployment tool written in PHP with support for popular frameworks"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install deployer on MacOS using homebrew

- App Name: deployer
- App description: Deployment tool written in PHP with support for popular frameworks
- App Version: 6.8.0
- App Website: https://deployer.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install deployer with the following command
   ```
   brew install deployer
   ```
4. deployer is ready to use now!