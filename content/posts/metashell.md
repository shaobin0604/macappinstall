---
title: "Install metashell on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Metaprogramming shell for C++ templates"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install metashell on MacOS using homebrew

- App Name: metashell
- App description: Metaprogramming shell for C++ templates
- App Version: 4.0.0
- App Website: http://metashell.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install metashell with the following command
   ```
   brew install metashell
   ```
4. metashell is ready to use now!