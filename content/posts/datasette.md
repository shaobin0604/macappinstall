---
title: "Install datasette on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Open source multi-tool for exploring and publishing data"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install datasette on MacOS using homebrew

- App Name: datasette
- App description: Open source multi-tool for exploring and publishing data
- App Version: 0.60.2
- App Website: https://docs.datasette.io/en/stable/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install datasette with the following command
   ```
   brew install datasette
   ```
4. datasette is ready to use now!