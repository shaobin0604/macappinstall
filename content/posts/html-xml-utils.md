---
title: "Install html-xml-utils on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tools for manipulating HTML and XML files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install html-xml-utils on MacOS using homebrew

- App Name: html-xml-utils
- App description: Tools for manipulating HTML and XML files
- App Version: 8.3
- App Website: https://www.w3.org/Tools/HTML-XML-utils/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install html-xml-utils with the following command
   ```
   brew install html-xml-utils
   ```
4. html-xml-utils is ready to use now!