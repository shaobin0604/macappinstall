---
title: "Install stress-ng on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Stress test a computer system in various selectable ways"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install stress-ng on MacOS using homebrew

- App Name: stress-ng
- App description: Stress test a computer system in various selectable ways
- App Version: 0.13.11
- App Website: https://wiki.ubuntu.com/Kernel/Reference/stress-ng

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install stress-ng with the following command
   ```
   brew install stress-ng
   ```
4. stress-ng is ready to use now!