---
title: "Install Fishing Funds on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Display real-time trends of Chinese funds in the menubar"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Fishing Funds on MacOS using homebrew

- App Name: Fishing Funds
- App description: Display real-time trends of Chinese funds in the menubar
- App Version: 5.6.0
- App Website: https://ff.1zilc.top/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Fishing Funds with the following command
   ```
   brew install --cask fishing-funds
   ```
4. Fishing Funds is ready to use now!