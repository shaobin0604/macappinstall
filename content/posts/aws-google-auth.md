---
title: "Install aws-google-auth on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Acquire AWS credentials using Google Apps"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install aws-google-auth on MacOS using homebrew

- App Name: aws-google-auth
- App description: Acquire AWS credentials using Google Apps
- App Version: 0.0.37
- App Website: https://github.com/cevoaustralia/aws-google-auth

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install aws-google-auth with the following command
   ```
   brew install aws-google-auth
   ```
4. aws-google-auth is ready to use now!