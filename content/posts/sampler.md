---
title: "Install sampler on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool for shell commands execution, visualization and alerting"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sampler on MacOS using homebrew

- App Name: sampler
- App description: Tool for shell commands execution, visualization and alerting
- App Version: 1.1.0
- App Website: https://sampler.dev

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sampler with the following command
   ```
   brew install sampler
   ```
4. sampler is ready to use now!