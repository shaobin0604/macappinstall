---
title: "Install terraformer on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CLI tool to generate terraform files from existing infrastructure"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install terraformer on MacOS using homebrew

- App Name: terraformer
- App description: CLI tool to generate terraform files from existing infrastructure
- App Version: 0.8.18
- App Website: https://github.com/GoogleCloudPlatform/terraformer

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install terraformer with the following command
   ```
   brew install terraformer
   ```
4. terraformer is ready to use now!