---
title: "Install linux-pam on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Pluggable Authentication Modules for Linux"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install linux-pam on MacOS using homebrew

- App Name: linux-pam
- App description: Pluggable Authentication Modules for Linux
- App Version: 1.5.2
- App Website: http://www.linux-pam.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install linux-pam with the following command
   ```
   brew install linux-pam
   ```
4. linux-pam is ready to use now!