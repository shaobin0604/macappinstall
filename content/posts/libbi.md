---
title: "Install libbi on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Bayesian state-space modelling on parallel computer hardware"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libbi on MacOS using homebrew

- App Name: libbi
- App description: Bayesian state-space modelling on parallel computer hardware
- App Version: 1.4.5
- App Website: https://libbi.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libbi with the following command
   ```
   brew install libbi
   ```
4. libbi is ready to use now!