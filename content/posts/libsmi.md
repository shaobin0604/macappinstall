---
title: "Install libsmi on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library to Access SMI MIB Information"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libsmi on MacOS using homebrew

- App Name: libsmi
- App description: Library to Access SMI MIB Information
- App Version: 0.5.0
- App Website: https://www.ibr.cs.tu-bs.de/projects/libsmi/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libsmi with the following command
   ```
   brew install libsmi
   ```
4. libsmi is ready to use now!