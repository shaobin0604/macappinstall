---
title: "Install pla on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool for building Gantt charts in PNG, EPS, PDF or SVG format"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pla on MacOS using homebrew

- App Name: pla
- App description: Tool for building Gantt charts in PNG, EPS, PDF or SVG format
- App Version: 1.3
- App Website: https://www.arpalert.org/pla.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pla with the following command
   ```
   brew install pla
   ```
4. pla is ready to use now!