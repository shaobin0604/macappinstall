---
title: "Install cimg on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C++ toolkit for image processing"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cimg on MacOS using homebrew

- App Name: cimg
- App description: C++ toolkit for image processing
- App Version: 3.0.2
- App Website: https://cimg.eu/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cimg with the following command
   ```
   brew install cimg
   ```
4. cimg is ready to use now!