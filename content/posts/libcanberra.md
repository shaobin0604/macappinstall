---
title: "Install libcanberra on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Implementation of XDG Sound Theme and Name Specifications"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libcanberra on MacOS using homebrew

- App Name: libcanberra
- App description: Implementation of XDG Sound Theme and Name Specifications
- App Version: 0.30
- App Website: https://0pointer.de/lennart/projects/libcanberra/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libcanberra with the following command
   ```
   brew install libcanberra
   ```
4. libcanberra is ready to use now!