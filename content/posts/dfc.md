---
title: "Install dfc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Display graphs and colors of file system space/usage"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dfc on MacOS using homebrew

- App Name: dfc
- App description: Display graphs and colors of file system space/usage
- App Version: 3.1.1
- App Website: https://github.com/Rolinh/dfc

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dfc with the following command
   ```
   brew install dfc
   ```
4. dfc is ready to use now!