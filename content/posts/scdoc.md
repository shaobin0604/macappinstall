---
title: "Install scdoc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Small man page generator"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install scdoc on MacOS using homebrew

- App Name: scdoc
- App description: Small man page generator
- App Version: 1.11.2
- App Website: https://sr.ht/~sircmpwn/scdoc/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install scdoc with the following command
   ```
   brew install scdoc
   ```
4. scdoc is ready to use now!