---
title: "Install cpufetch on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CPU architecture fetching tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cpufetch on MacOS using homebrew

- App Name: cpufetch
- App description: CPU architecture fetching tool
- App Version: 1.01
- App Website: https://github.com/Dr-Noob/cpufetch

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cpufetch with the following command
   ```
   brew install cpufetch
   ```
4. cpufetch is ready to use now!