---
title: "Install Adobe Acrobat Pro DC on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "View, create, manipulate, print and manage files in Portable Document Format"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Adobe Acrobat Pro DC on MacOS using homebrew

- App Name: Adobe Acrobat Pro DC
- App description: View, create, manipulate, print and manage files in Portable Document Format
- App Version: 21.011.20039
- App Website: https://acrobat.adobe.com/us/en/acrobat/pdf-reader.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Adobe Acrobat Pro DC with the following command
   ```
   brew install --cask adobe-acrobat-pro
   ```
4. Adobe Acrobat Pro DC is ready to use now!