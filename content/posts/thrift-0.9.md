---
title: "Install thrift@0.9 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Framework for scalable cross-language services development"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install thrift@0.9 on MacOS using homebrew

- App Name: thrift@0.9
- App description: Framework for scalable cross-language services development
- App Version: 0.9.3.1
- App Website: https://thrift.apache.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install thrift@0.9 with the following command
   ```
   brew install thrift@0.9
   ```
4. thrift@0.9 is ready to use now!