---
title: "Install gojq on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Pure Go implementation of jq"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gojq on MacOS using homebrew

- App Name: gojq
- App description: Pure Go implementation of jq
- App Version: 0.12.6
- App Website: https://github.com/itchyny/gojq

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gojq with the following command
   ```
   brew install gojq
   ```
4. gojq is ready to use now!