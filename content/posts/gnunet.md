---
title: "Install gnunet on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Framework for distributed, secure and privacy-preserving applications"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gnunet on MacOS using homebrew

- App Name: gnunet
- App description: Framework for distributed, secure and privacy-preserving applications
- App Version: 0.15.3
- App Website: https://gnunet.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gnunet with the following command
   ```
   brew install gnunet
   ```
4. gnunet is ready to use now!