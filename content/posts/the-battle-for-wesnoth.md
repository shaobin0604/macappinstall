---
title: "Install The Battle for Wesnoth on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Fantasy-themed turn-based strategy game"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install The Battle for Wesnoth on MacOS using homebrew

- App Name: The Battle for Wesnoth
- App description: Fantasy-themed turn-based strategy game
- App Version: 1.17.0
- App Website: https://wesnoth.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install The Battle for Wesnoth with the following command
   ```
   brew install --cask the-battle-for-wesnoth
   ```
4. The Battle for Wesnoth is ready to use now!