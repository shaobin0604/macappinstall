---
title: "Install ocaml-num on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "OCaml legacy Num library for arbitrary-precision arithmetic"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ocaml-num on MacOS using homebrew

- App Name: ocaml-num
- App description: OCaml legacy Num library for arbitrary-precision arithmetic
- App Version: 1.4
- App Website: https://github.com/ocaml/num

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ocaml-num with the following command
   ```
   brew install ocaml-num
   ```
4. ocaml-num is ready to use now!