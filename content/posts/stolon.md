---
title: "Install stolon on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cloud native PostgreSQL manager for high availability"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install stolon on MacOS using homebrew

- App Name: stolon
- App description: Cloud native PostgreSQL manager for high availability
- App Version: 0.17.0
- App Website: https://github.com/sorintlab/stolon

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install stolon with the following command
   ```
   brew install stolon
   ```
4. stolon is ready to use now!