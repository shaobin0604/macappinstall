---
title: "Install dockviz on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Visualizing docker data"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dockviz on MacOS using homebrew

- App Name: dockviz
- App description: Visualizing docker data
- App Version: 0.6.3
- App Website: https://github.com/justone/dockviz

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dockviz with the following command
   ```
   brew install dockviz
   ```
4. dockviz is ready to use now!