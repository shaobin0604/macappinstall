---
title: "Install dwarfutils on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Dump and produce DWARF debug information in ELF objects"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dwarfutils on MacOS using homebrew

- App Name: dwarfutils
- App description: Dump and produce DWARF debug information in ELF objects
- App Version: 0.3.3
- App Website: https://www.prevanders.net/dwarf.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dwarfutils with the following command
   ```
   brew install dwarfutils
   ```
4. dwarfutils is ready to use now!