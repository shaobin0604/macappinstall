---
title: "Install Audioscrobbler on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Minimal, robust iTunes scrobbling"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Audioscrobbler on MacOS using homebrew

- App Name: Audioscrobbler
- App description: Minimal, robust iTunes scrobbling
- App Version: 0.9.15
- App Website: https://github.com/mxcl/Audioscrobbler.app

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Audioscrobbler with the following command
   ```
   brew install --cask audioscrobbler
   ```
4. Audioscrobbler is ready to use now!