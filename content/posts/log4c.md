---
title: "Install log4c on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Logging Framework for C"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install log4c on MacOS using homebrew

- App Name: log4c
- App description: Logging Framework for C
- App Version: 1.2.4
- App Website: https://log4c.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install log4c with the following command
   ```
   brew install log4c
   ```
4. log4c is ready to use now!