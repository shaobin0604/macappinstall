---
title: "Install dav1d on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "AV1 decoder targeted to be small and fast"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dav1d on MacOS using homebrew

- App Name: dav1d
- App description: AV1 decoder targeted to be small and fast
- App Version: 0.9.2
- App Website: https://code.videolan.org/videolan/dav1d

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dav1d with the following command
   ```
   brew install dav1d
   ```
4. dav1d is ready to use now!