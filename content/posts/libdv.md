---
title: "Install libdv on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Codec for DV video encoding format"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libdv on MacOS using homebrew

- App Name: libdv
- App description: Codec for DV video encoding format
- App Version: 1.0.0
- App Website: https://libdv.sourceforge.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libdv with the following command
   ```
   brew install libdv
   ```
4. libdv is ready to use now!