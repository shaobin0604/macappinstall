---
title: "Install modules on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Dynamic modification of a user's environment via modulefiles"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install modules on MacOS using homebrew

- App Name: modules
- App description: Dynamic modification of a user's environment via modulefiles
- App Version: 5.0.1
- App Website: https://modules.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install modules with the following command
   ```
   brew install modules
   ```
4. modules is ready to use now!