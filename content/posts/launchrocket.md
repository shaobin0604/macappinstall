---
title: "Install LaunchRocket on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install LaunchRocket on MacOS using homebrew

- App Name: LaunchRocket
- App description: null
- App Version: 0.7
- App Website: https://github.com/jimbojsb/launchrocket

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install LaunchRocket with the following command
   ```
   brew install --cask launchrocket
   ```
4. LaunchRocket is ready to use now!