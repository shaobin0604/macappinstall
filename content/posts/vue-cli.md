---
title: "Install vue-cli on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Standard Tooling for Vue.js Development"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install vue-cli on MacOS using homebrew

- App Name: vue-cli
- App description: Standard Tooling for Vue.js Development
- App Version: 5.0.1
- App Website: https://cli.vuejs.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install vue-cli with the following command
   ```
   brew install vue-cli
   ```
4. vue-cli is ready to use now!