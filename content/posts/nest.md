---
title: "Install nest on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Neural Simulation Tool (NEST) with Python3 bindings (PyNEST)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install nest on MacOS using homebrew

- App Name: nest
- App description: Neural Simulation Tool (NEST) with Python3 bindings (PyNEST)
- App Version: 3.2
- App Website: https://www.nest-simulator.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install nest with the following command
   ```
   brew install nest
   ```
4. nest is ready to use now!