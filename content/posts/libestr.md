---
title: "Install libestr on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C library for string handling (and a bit more)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libestr on MacOS using homebrew

- App Name: libestr
- App description: C library for string handling (and a bit more)
- App Version: 0.1.11
- App Website: https://libestr.adiscon.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libestr with the following command
   ```
   brew install libestr
   ```
4. libestr is ready to use now!