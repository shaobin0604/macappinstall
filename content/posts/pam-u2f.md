---
title: "Install pam-u2f on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Provides an easy way to use U2F-compliant authenticators with PAM"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pam-u2f on MacOS using homebrew

- App Name: pam-u2f
- App description: Provides an easy way to use U2F-compliant authenticators with PAM
- App Version: 1.2.0
- App Website: https://developers.yubico.com/pam-u2f/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pam-u2f with the following command
   ```
   brew install pam-u2f
   ```
4. pam-u2f is ready to use now!