---
title: "Install TextBar on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Add any text to menu bar"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install TextBar on MacOS using homebrew

- App Name: TextBar
- App description: Add any text to menu bar
- App Version: 3.5.6
- App Website: http://richsomerfield.com/apps/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install TextBar with the following command
   ```
   brew install --cask textbar
   ```
4. TextBar is ready to use now!