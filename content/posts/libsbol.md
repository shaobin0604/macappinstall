---
title: "Install libsbol on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Read and write files in the Synthetic Biology Open Language (SBOL)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libsbol on MacOS using homebrew

- App Name: libsbol
- App description: Read and write files in the Synthetic Biology Open Language (SBOL)
- App Version: 2.3.2
- App Website: https://synbiodex.github.io/libSBOL

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libsbol with the following command
   ```
   brew install libsbol
   ```
4. libsbol is ready to use now!