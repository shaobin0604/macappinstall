---
title: "Install icarus-verilog on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Verilog simulation and synthesis tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install icarus-verilog on MacOS using homebrew

- App Name: icarus-verilog
- App description: Verilog simulation and synthesis tool
- App Version: 11.0
- App Website: http://iverilog.icarus.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install icarus-verilog with the following command
   ```
   brew install icarus-verilog
   ```
4. icarus-verilog is ready to use now!