---
title: "Install socat on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "SOcket CAT: netcat on steroids"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install socat on MacOS using homebrew

- App Name: socat
- App description: SOcket CAT: netcat on steroids
- App Version: 1.7.4.3
- App Website: http://www.dest-unreach.org/socat/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install socat with the following command
   ```
   brew install socat
   ```
4. socat is ready to use now!