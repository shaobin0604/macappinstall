---
title: "Install b43-fwcutter on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Extract firmware from Braodcom 43xx driver files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install b43-fwcutter on MacOS using homebrew

- App Name: b43-fwcutter
- App description: Extract firmware from Braodcom 43xx driver files
- App Version: 019
- App Website: https://wireless.wiki.kernel.org/en/users/drivers/b43

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install b43-fwcutter with the following command
   ```
   brew install b43-fwcutter
   ```
4. b43-fwcutter is ready to use now!