---
title: "Install libsvg-cairo on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "SVG rendering library using Cairo"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libsvg-cairo on MacOS using homebrew

- App Name: libsvg-cairo
- App description: SVG rendering library using Cairo
- App Version: 0.1.6
- App Website: https://cairographics.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libsvg-cairo with the following command
   ```
   brew install libsvg-cairo
   ```
4. libsvg-cairo is ready to use now!