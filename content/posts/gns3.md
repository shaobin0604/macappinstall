---
title: "Install GNS3 on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GUI for the Dynamips Cisco router emulator"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install GNS3 on MacOS using homebrew

- App Name: GNS3
- App description: GUI for the Dynamips Cisco router emulator
- App Version: 2.2.29
- App Website: https://www.gns3.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install GNS3 with the following command
   ```
   brew install --cask gns3
   ```
4. GNS3 is ready to use now!