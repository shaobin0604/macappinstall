---
title: "Install Cura LulzBot Edition on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "3D printing solution"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Cura LulzBot Edition on MacOS using homebrew

- App Name: Cura LulzBot Edition
- App description: 3D printing solution
- App Version: 3.6.21,ce3e47a08065c6687f0a226a4f1b2dc3
- App Website: https://www.lulzbot.com/learn/tutorials/cura-lulzbot-edition-installation-macos

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Cura LulzBot Edition with the following command
   ```
   brew install --cask cura-lulzbot
   ```
4. Cura LulzBot Edition is ready to use now!