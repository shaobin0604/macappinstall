---
title: "Install zinc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Stand-alone version of sbt's Scala incremental compiler"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install zinc on MacOS using homebrew

- App Name: zinc
- App description: Stand-alone version of sbt's Scala incremental compiler
- App Version: 0.3.15
- App Website: https://github.com/typesafehub/zinc

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install zinc with the following command
   ```
   brew install zinc
   ```
4. zinc is ready to use now!