---
title: "Install Ananas Analytics Desktop Edition on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Hackable data integration & analysis tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Ananas Analytics Desktop Edition on MacOS using homebrew

- App Name: Ananas Analytics Desktop Edition
- App description: Hackable data integration & analysis tool
- App Version: 0.9.0
- App Website: https://ananasanalytics.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Ananas Analytics Desktop Edition with the following command
   ```
   brew install --cask ananas-analytics-desktop-edition
   ```
4. Ananas Analytics Desktop Edition is ready to use now!