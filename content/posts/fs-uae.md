---
title: "Install fs-uae on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Amiga emulator"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fs-uae on MacOS using homebrew

- App Name: fs-uae
- App description: Amiga emulator
- App Version: 3.1.66
- App Website: https://fs-uae.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fs-uae with the following command
   ```
   brew install fs-uae
   ```
4. fs-uae is ready to use now!