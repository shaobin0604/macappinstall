---
title: "Install Proxyman on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Modern and intuitive HTTP Debugging Proxy app"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Proxyman on MacOS using homebrew

- App Name: Proxyman
- App description: Modern and intuitive HTTP Debugging Proxy app
- App Version: 3.1.0,30100
- App Website: https://proxyman.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Proxyman with the following command
   ```
   brew install --cask proxyman
   ```
4. Proxyman is ready to use now!