---
title: "Install Real VNC Server on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Remote desktop server application"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Real VNC Server on MacOS using homebrew

- App Name: Real VNC Server
- App description: Remote desktop server application
- App Version: 6.8.0
- App Website: https://www.realvnc.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Real VNC Server with the following command
   ```
   brew install --cask vnc-server
   ```
4. Real VNC Server is ready to use now!