---
title: "Install moar on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Nice to use pager for humans"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install moar on MacOS using homebrew

- App Name: moar
- App description: Nice to use pager for humans
- App Version: 1.8.4
- App Website: https://github.com/walles/moar

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install moar with the following command
   ```
   brew install moar
   ```
4. moar is ready to use now!