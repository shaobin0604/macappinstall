---
title: "Install ExifRenamer on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to rename digital photos, movie- and audio-clips"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ExifRenamer on MacOS using homebrew

- App Name: ExifRenamer
- App description: Tool to rename digital photos, movie- and audio-clips
- App Version: 2.4.0,15
- App Website: https://www.qdev.de/?location=mac/exifrenamer&forcelang=en

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ExifRenamer with the following command
   ```
   brew install --cask exifrenamer
   ```
4. ExifRenamer is ready to use now!