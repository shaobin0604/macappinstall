---
title: "Install ed on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Classic UNIX line editor"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ed on MacOS using homebrew

- App Name: ed
- App description: Classic UNIX line editor
- App Version: 1.18
- App Website: https://www.gnu.org/software/ed/ed.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ed with the following command
   ```
   brew install ed
   ```
4. ed is ready to use now!