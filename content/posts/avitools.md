---
title: "Install AVItools on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Graphical interface for a variety of video file processing tools"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install AVItools on MacOS using homebrew

- App Name: AVItools
- App description: Graphical interface for a variety of video file processing tools
- App Version: 3.7.2
- App Website: https://www.emmgunn.com/avitools-home/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install AVItools with the following command
   ```
   brew install --cask avitools
   ```
4. AVItools is ready to use now!