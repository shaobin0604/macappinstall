---
title: "Install Play! on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "PlayStation 2 emulator"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Play! on MacOS using homebrew

- App Name: Play!
- App description: PlayStation 2 emulator
- App Version: 2022-01-05,5e124ac9
- App Website: https://purei.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Play! with the following command
   ```
   brew install --cask purei-play
   ```
4. Play! is ready to use now!