---
title: "Install libmypaint on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "MyPaint brush engine library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libmypaint on MacOS using homebrew

- App Name: libmypaint
- App description: MyPaint brush engine library
- App Version: 1.6.1
- App Website: https://github.com/mypaint/libmypaint/wiki

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libmypaint with the following command
   ```
   brew install libmypaint
   ```
4. libmypaint is ready to use now!