---
title: "Install lefthook on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Fast and powerful Git hooks manager for any type of projects"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install lefthook on MacOS using homebrew

- App Name: lefthook
- App description: Fast and powerful Git hooks manager for any type of projects
- App Version: 0.7.7
- App Website: https://github.com/evilmartians/lefthook

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install lefthook with the following command
   ```
   brew install lefthook
   ```
4. lefthook is ready to use now!