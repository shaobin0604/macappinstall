---
title: "Install Logic Sniffer on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Software client for the Open Bench Logic Sniffer logic analyser hardware"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Logic Sniffer on MacOS using homebrew

- App Name: Logic Sniffer
- App description: Software client for the Open Bench Logic Sniffer logic analyser hardware
- App Version: 0.9.7.2
- App Website: https://lxtreme.nl/projects/ols/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Logic Sniffer with the following command
   ```
   brew install --cask logicsniffer
   ```
4. Logic Sniffer is ready to use now!