---
title: "Install ansible-cmdb on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Generates static HTML overview page from Ansible facts"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ansible-cmdb on MacOS using homebrew

- App Name: ansible-cmdb
- App description: Generates static HTML overview page from Ansible facts
- App Version: 1.31
- App Website: https://github.com/fboender/ansible-cmdb

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ansible-cmdb with the following command
   ```
   brew install ansible-cmdb
   ```
4. ansible-cmdb is ready to use now!