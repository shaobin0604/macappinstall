---
title: "Install soapysdr on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Vendor and platform neutral SDR support library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install soapysdr on MacOS using homebrew

- App Name: soapysdr
- App description: Vendor and platform neutral SDR support library
- App Version: 0.8.1
- App Website: https://github.com/pothosware/SoapySDR/wiki

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install soapysdr with the following command
   ```
   brew install soapysdr
   ```
4. soapysdr is ready to use now!