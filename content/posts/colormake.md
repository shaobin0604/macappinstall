---
title: "Install colormake on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Wrapper around make to colorize the output"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install colormake on MacOS using homebrew

- App Name: colormake
- App description: Wrapper around make to colorize the output
- App Version: 0.9.20140503
- App Website: https://github.com/pagekite/Colormake

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install colormake with the following command
   ```
   brew install colormake
   ```
4. colormake is ready to use now!