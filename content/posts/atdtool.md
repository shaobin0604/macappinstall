---
title: "Install atdtool on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line interface for After the Deadline language checker"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install atdtool on MacOS using homebrew

- App Name: atdtool
- App description: Command-line interface for After the Deadline language checker
- App Version: 1.3.3
- App Website: https://github.com/lpenz/atdtool

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install atdtool with the following command
   ```
   brew install atdtool
   ```
4. atdtool is ready to use now!