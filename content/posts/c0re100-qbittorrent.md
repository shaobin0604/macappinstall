---
title: "Install qBittorrent Enhanced Edition on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Bittorrent client"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install qBittorrent Enhanced Edition on MacOS using homebrew

- App Name: qBittorrent Enhanced Edition
- App description: Bittorrent client
- App Version: 4.4.1.10
- App Website: https://github.com/c0re100/qBittorrent-Enhanced-Edition

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install qBittorrent Enhanced Edition with the following command
   ```
   brew install --cask c0re100-qbittorrent
   ```
4. qBittorrent Enhanced Edition is ready to use now!