---
title: "Install Fluor on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Change the behavior of the fn keys depending on the active application"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Fluor on MacOS using homebrew

- App Name: Fluor
- App description: Change the behavior of the fn keys depending on the active application
- App Version: 2.5.0
- App Website: https://github.com/Pyroh/Fluor

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Fluor with the following command
   ```
   brew install --cask fluor
   ```
4. Fluor is ready to use now!