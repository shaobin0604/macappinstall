---
title: "Install mp3wrap on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Wrap two or more mp3 files in a single large file"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mp3wrap on MacOS using homebrew

- App Name: mp3wrap
- App description: Wrap two or more mp3 files in a single large file
- App Version: 0.5
- App Website: https://mp3wrap.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mp3wrap with the following command
   ```
   brew install mp3wrap
   ```
4. mp3wrap is ready to use now!