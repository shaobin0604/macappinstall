---
title: "Install luau on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Fast, safe, gradually typed embeddable scripting language derived from Lua"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install luau on MacOS using homebrew

- App Name: luau
- App description: Fast, safe, gradually typed embeddable scripting language derived from Lua
- App Version: 0.514
- App Website: https://luau-lang.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install luau with the following command
   ```
   brew install luau
   ```
4. luau is ready to use now!