---
title: "Install libdeflate on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Heavily optimized DEFLATE/zlib/gzip compression and decompression"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libdeflate on MacOS using homebrew

- App Name: libdeflate
- App description: Heavily optimized DEFLATE/zlib/gzip compression and decompression
- App Version: 1.10
- App Website: https://github.com/ebiggers/libdeflate

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libdeflate with the following command
   ```
   brew install libdeflate
   ```
4. libdeflate is ready to use now!