---
title: "Install ShareMouse on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Share peripherals between computers"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ShareMouse on MacOS using homebrew

- App Name: ShareMouse
- App description: Share peripherals between computers
- App Version: 5.0.51
- App Website: https://www.sharemouse.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ShareMouse with the following command
   ```
   brew install --cask sharemouse
   ```
4. ShareMouse is ready to use now!