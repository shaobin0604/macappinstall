---
title: "Install fcp on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Significantly faster alternative to the classic Unix cp(1) command"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fcp on MacOS using homebrew

- App Name: fcp
- App description: Significantly faster alternative to the classic Unix cp(1) command
- App Version: 0.2.1
- App Website: https://github.com/Svetlitski/fcp/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fcp with the following command
   ```
   brew install fcp
   ```
4. fcp is ready to use now!