---
title: "Install znc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Advanced IRC bouncer"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install znc on MacOS using homebrew

- App Name: znc
- App description: Advanced IRC bouncer
- App Version: 1.8.2
- App Website: https://wiki.znc.in/ZNC

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install znc with the following command
   ```
   brew install znc
   ```
4. znc is ready to use now!