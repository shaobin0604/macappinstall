---
title: "Install Airtame on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Wireless screen sharing platform"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Airtame on MacOS using homebrew

- App Name: Airtame
- App description: Wireless screen sharing platform
- App Version: 4.3.2
- App Website: https://airtame.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Airtame with the following command
   ```
   brew install --cask airtame
   ```
4. Airtame is ready to use now!