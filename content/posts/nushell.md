---
title: "Install nushell on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Modern shell for the GitHub era"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install nushell on MacOS using homebrew

- App Name: nushell
- App description: Modern shell for the GitHub era
- App Version: 0.44.0
- App Website: https://www.nushell.sh

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install nushell with the following command
   ```
   brew install nushell
   ```
4. nushell is ready to use now!