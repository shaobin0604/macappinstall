---
title: "Install innoextract on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to unpack installers created by Inno Setup"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install innoextract on MacOS using homebrew

- App Name: innoextract
- App description: Tool to unpack installers created by Inno Setup
- App Version: 1.9
- App Website: https://constexpr.org/innoextract/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install innoextract with the following command
   ```
   brew install innoextract
   ```
4. innoextract is ready to use now!