---
title: "Install dhall-lsp-server on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Language Server Protocol (LSP) server for Dhall"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dhall-lsp-server on MacOS using homebrew

- App Name: dhall-lsp-server
- App description: Language Server Protocol (LSP) server for Dhall
- App Version: 1.0.17
- App Website: https://github.com/dhall-lang/dhall-haskell/tree/master/dhall-lsp-server

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dhall-lsp-server with the following command
   ```
   brew install dhall-lsp-server
   ```
4. dhall-lsp-server is ready to use now!