---
title: "Install tty-share on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Terminal sharing over the Internet"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tty-share on MacOS using homebrew

- App Name: tty-share
- App description: Terminal sharing over the Internet
- App Version: 2.2.1
- App Website: https://tty-share.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tty-share with the following command
   ```
   brew install tty-share
   ```
4. tty-share is ready to use now!