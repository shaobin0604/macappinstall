---
title: "Install webpack on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Bundler for JavaScript and friends"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install webpack on MacOS using homebrew

- App Name: webpack
- App description: Bundler for JavaScript and friends
- App Version: 5.69.1
- App Website: https://webpack.js.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install webpack with the following command
   ```
   brew install webpack
   ```
4. webpack is ready to use now!