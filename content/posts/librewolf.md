---
title: "Install LibreWolf on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Web browser"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install LibreWolf on MacOS using homebrew

- App Name: LibreWolf
- App description: Web browser
- App Version: 97.0,1,45070518c865ab1af2cd2c40fe9c3143
- App Website: https://librewolf.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install LibreWolf with the following command
   ```
   brew install --cask librewolf
   ```
4. LibreWolf is ready to use now!