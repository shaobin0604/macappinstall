---
title: "Install xmlsectool on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Check schema validity and signature of an XML document"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install xmlsectool on MacOS using homebrew

- App Name: xmlsectool
- App description: Check schema validity and signature of an XML document
- App Version: 3.0.0
- App Website: https://wiki.shibboleth.net/confluence/display/XSTJ3/xmlsectool+V3+Home

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install xmlsectool with the following command
   ```
   brew install xmlsectool
   ```
4. xmlsectool is ready to use now!