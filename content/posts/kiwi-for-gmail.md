---
title: "Install Kiwi for Gmail on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Enhances Gmail like a full-featured desktop office productivity app"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Kiwi for Gmail on MacOS using homebrew

- App Name: Kiwi for Gmail
- App description: Enhances Gmail like a full-featured desktop office productivity app
- App Version: 3.4.2,2411
- App Website: https://www.kiwiforgmail.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Kiwi for Gmail with the following command
   ```
   brew install --cask kiwi-for-gmail
   ```
4. Kiwi for Gmail is ready to use now!