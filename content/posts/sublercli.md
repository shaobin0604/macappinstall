---
title: "Install SublerCLI on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line version of Subler"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install SublerCLI on MacOS using homebrew

- App Name: SublerCLI
- App description: Command-line version of Subler
- App Version: 1.5.1
- App Website: https://bitbucket.org/galad87/sublercli/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install SublerCLI with the following command
   ```
   brew install --cask sublercli
   ```
4. SublerCLI is ready to use now!