---
title: "Install glib-networking on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Network related modules for glib"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install glib-networking on MacOS using homebrew

- App Name: glib-networking
- App description: Network related modules for glib
- App Version: 2.70.1
- App Website: https://gitlab.gnome.org/GNOME/glib-networking

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install glib-networking with the following command
   ```
   brew install glib-networking
   ```
4. glib-networking is ready to use now!