---
title: "Install crush-tools on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line tools for processing delimited text data"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install crush-tools on MacOS using homebrew

- App Name: crush-tools
- App description: Command-line tools for processing delimited text data
- App Version: 20150716
- App Website: https://github.com/google/crush-tools

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install crush-tools with the following command
   ```
   brew install crush-tools
   ```
4. crush-tools is ready to use now!