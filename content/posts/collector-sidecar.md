---
title: "Install collector-sidecar on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Manage log collectors through Graylog"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install collector-sidecar on MacOS using homebrew

- App Name: collector-sidecar
- App description: Manage log collectors through Graylog
- App Version: 1.1.0
- App Website: https://www.graylog.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install collector-sidecar with the following command
   ```
   brew install collector-sidecar
   ```
4. collector-sidecar is ready to use now!