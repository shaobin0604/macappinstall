---
title: "Install Eclipse for Testers on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Eclipse IDE for testing purposes"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Eclipse for Testers on MacOS using homebrew

- App Name: Eclipse for Testers
- App description: Eclipse IDE for testing purposes
- App Version: 4.18.0,2020-12
- App Website: https://eclipse.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Eclipse for Testers with the following command
   ```
   brew install --cask eclipse-testing
   ```
4. Eclipse for Testers is ready to use now!