---
title: "Install Schism Tracker on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Oldschool sample-based music composition tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Schism Tracker on MacOS using homebrew

- App Name: Schism Tracker
- App description: Oldschool sample-based music composition tool
- App Version: 20220125
- App Website: https://github.com/schismtracker/schismtracker

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Schism Tracker with the following command
   ```
   brew install --cask schism-tracker
   ```
4. Schism Tracker is ready to use now!