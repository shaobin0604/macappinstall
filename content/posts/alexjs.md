---
title: "Install alexjs on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Catch insensitive, inconsiderate writing"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install alexjs on MacOS using homebrew

- App Name: alexjs
- App description: Catch insensitive, inconsiderate writing
- App Version: 10.0.0
- App Website: https://alexjs.com

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install alexjs with the following command
   ```
   brew install alexjs
   ```
4. alexjs is ready to use now!