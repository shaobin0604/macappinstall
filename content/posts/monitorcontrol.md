---
title: "Install MonitorControl on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to control external monitor brightness & volume"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install MonitorControl on MacOS using homebrew

- App Name: MonitorControl
- App description: Tool to control external monitor brightness & volume
- App Version: 4.0.2
- App Website: https://github.com/MonitorControl/MonitorControl

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install MonitorControl with the following command
   ```
   brew install --cask monitorcontrol
   ```
4. MonitorControl is ready to use now!