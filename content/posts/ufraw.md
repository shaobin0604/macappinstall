---
title: "Install ufraw on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Unidentified Flying RAW: RAW image processing utility"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ufraw on MacOS using homebrew

- App Name: ufraw
- App description: Unidentified Flying RAW: RAW image processing utility
- App Version: 0.22
- App Website: https://ufraw.sourceforge.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ufraw with the following command
   ```
   brew install ufraw
   ```
4. ufraw is ready to use now!