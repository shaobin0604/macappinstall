---
title: "Install Movist on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Movie Player based on QuickTime & FFmpeg"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Movist on MacOS using homebrew

- App Name: Movist
- App description: Movie Player based on QuickTime & FFmpeg
- App Version: 0.6.9,08e974e3+
- App Website: https://github.com/samiamwork/Movist

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Movist with the following command
   ```
   brew install --cask movist
   ```
4. Movist is ready to use now!