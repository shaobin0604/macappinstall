---
title: "Install open-zwave on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library that interfaces with selected Z-Wave PC controllers"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install open-zwave on MacOS using homebrew

- App Name: open-zwave
- App description: Library that interfaces with selected Z-Wave PC controllers
- App Version: 1.6.1914
- App Website: http://www.openzwave.com

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install open-zwave with the following command
   ```
   brew install open-zwave
   ```
4. open-zwave is ready to use now!