---
title: "Install fantom on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Object oriented, portable programming language"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fantom on MacOS using homebrew

- App Name: fantom
- App description: Object oriented, portable programming language
- App Version: 1.0.77
- App Website: https://fantom.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fantom with the following command
   ```
   brew install fantom
   ```
4. fantom is ready to use now!