---
title: "Install DevUtils on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Offline toolbox for developers"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install DevUtils on MacOS using homebrew

- App Name: DevUtils
- App description: Offline toolbox for developers
- App Version: 1.13.0,108
- App Website: https://devutils.app/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install DevUtils with the following command
   ```
   brew install --cask devutils
   ```
4. DevUtils is ready to use now!