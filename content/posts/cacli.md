---
title: "Install cacli on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Train machine learning models from Cloud Annotations"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cacli on MacOS using homebrew

- App Name: cacli
- App description: Train machine learning models from Cloud Annotations
- App Version: 1.3.2
- App Website: https://cloud.annotations.ai

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cacli with the following command
   ```
   brew install cacli
   ```
4. cacli is ready to use now!