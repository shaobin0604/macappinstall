---
title: "Install pangomm@2.46 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C++ interface to Pango"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pangomm@2.46 on MacOS using homebrew

- App Name: pangomm@2.46
- App description: C++ interface to Pango
- App Version: 2.46.2
- App Website: https://www.pango.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pangomm@2.46 with the following command
   ```
   brew install pangomm@2.46
   ```
4. pangomm@2.46 is ready to use now!