---
title: "Install Aladin Desktop on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Interactive sky atlas"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Aladin Desktop on MacOS using homebrew

- App Name: Aladin Desktop
- App description: Interactive sky atlas
- App Version: 11.024
- App Website: https://aladin.u-strasbg.fr/AladinDesktop/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Aladin Desktop with the following command
   ```
   brew install --cask aladin
   ```
4. Aladin Desktop is ready to use now!