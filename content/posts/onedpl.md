---
title: "Install onedpl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C++ standard library algorithms with support for execution policies"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install onedpl on MacOS using homebrew

- App Name: onedpl
- App description: C++ standard library algorithms with support for execution policies
- App Version: 2021.6.0
- App Website: https://software.intel.com/content/www/us/en/develop/tools/oneapi/components/dpc-library.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install onedpl with the following command
   ```
   brew install onedpl
   ```
4. onedpl is ready to use now!