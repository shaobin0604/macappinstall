---
title: "Install My TouchBar. My rules on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install My TouchBar. My rules on MacOS using homebrew

- App Name: My TouchBar. My rules
- App description: null
- App Version: 0.27,433
- App Website: https://mtmr.app/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install My TouchBar. My rules with the following command
   ```
   brew install --cask mtmr
   ```
4. My TouchBar. My rules is ready to use now!