---
title: "Install libchewing on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Intelligent phonetic input method library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libchewing on MacOS using homebrew

- App Name: libchewing
- App description: Intelligent phonetic input method library
- App Version: 0.5.1
- App Website: http://chewing.im/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libchewing with the following command
   ```
   brew install libchewing
   ```
4. libchewing is ready to use now!