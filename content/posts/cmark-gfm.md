---
title: "Install cmark-gfm on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C implementation of GitHub Flavored Markdown"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cmark-gfm on MacOS using homebrew

- App Name: cmark-gfm
- App description: C implementation of GitHub Flavored Markdown
- App Version: 0.29.0.gfm.2
- App Website: https://github.com/github/cmark-gfm

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cmark-gfm with the following command
   ```
   brew install cmark-gfm
   ```
4. cmark-gfm is ready to use now!