---
title: "Install JamKazam on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Low-latency rehearsing, jamming and performing"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install JamKazam on MacOS using homebrew

- App Name: JamKazam
- App description: Low-latency rehearsing, jamming and performing
- App Version: 1.0.4048
- App Website: https://jamkazam.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install JamKazam with the following command
   ```
   brew install --cask jamkazam
   ```
4. JamKazam is ready to use now!