---
title: "Install pure on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Pretty, minimal and fast ZSH prompt"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pure on MacOS using homebrew

- App Name: pure
- App description: Pretty, minimal and fast ZSH prompt
- App Version: 1.20.0
- App Website: https://github.com/sindresorhus/pure

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pure with the following command
   ```
   brew install pure
   ```
4. pure is ready to use now!