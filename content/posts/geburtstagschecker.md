---
title: "Install GeburtstagsChecker on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install GeburtstagsChecker on MacOS using homebrew

- App Name: GeburtstagsChecker
- App description: null
- App Version: 1.8.2,195
- App Website: https://earthlingsoft.net/GeburtstagsChecker/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install GeburtstagsChecker with the following command
   ```
   brew install --cask geburtstagschecker
   ```
4. GeburtstagsChecker is ready to use now!