---
title: "Install git-tf on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Share changes between TFS and git"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install git-tf on MacOS using homebrew

- App Name: git-tf
- App description: Share changes between TFS and git
- App Version: 2.0.3.20131219
- App Website: https://archive.codeplex.com/?p=gittf

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install git-tf with the following command
   ```
   brew install git-tf
   ```
4. git-tf is ready to use now!