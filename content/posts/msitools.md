---
title: "Install msitools on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Windows installer (.MSI) tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install msitools on MacOS using homebrew

- App Name: msitools
- App description: Windows installer (.MSI) tool
- App Version: 0.101
- App Website: https://wiki.gnome.org/msitools

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install msitools with the following command
   ```
   brew install msitools
   ```
4. msitools is ready to use now!