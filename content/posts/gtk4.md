---
title: "Install gtk4 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Toolkit for creating graphical user interfaces"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gtk4 on MacOS using homebrew

- App Name: gtk4
- App description: Toolkit for creating graphical user interfaces
- App Version: 4.6.1
- App Website: https://gtk.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gtk4 with the following command
   ```
   brew install gtk4
   ```
4. gtk4 is ready to use now!