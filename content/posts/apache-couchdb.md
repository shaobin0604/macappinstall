---
title: "Install Apache CouchDB on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Multi-master syncing database"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Apache CouchDB on MacOS using homebrew

- App Name: Apache CouchDB
- App description: Multi-master syncing database
- App Version: 3.2.0
- App Website: https://couchdb.apache.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Apache CouchDB with the following command
   ```
   brew install --cask apache-couchdb
   ```
4. Apache CouchDB is ready to use now!