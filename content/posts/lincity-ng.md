---
title: "Install lincity-ng on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "City simulation game"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install lincity-ng on MacOS using homebrew

- App Name: lincity-ng
- App description: City simulation game
- App Version: 2.0
- App Website: https://github.com/lincity-ng/lincity-ng/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install lincity-ng with the following command
   ```
   brew install lincity-ng
   ```
4. lincity-ng is ready to use now!