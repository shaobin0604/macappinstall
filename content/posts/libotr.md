---
title: "Install libotr on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Off-The-Record (OTR) messaging library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libotr on MacOS using homebrew

- App Name: libotr
- App description: Off-The-Record (OTR) messaging library
- App Version: 4.1.1
- App Website: https://otr.cypherpunks.ca/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libotr with the following command
   ```
   brew install libotr
   ```
4. libotr is ready to use now!