---
title: "Install sfst on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Toolbox for morphological analysers and other FST-based tools"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sfst on MacOS using homebrew

- App Name: sfst
- App description: Toolbox for morphological analysers and other FST-based tools
- App Version: 1.4.7f
- App Website: https://www.cis.uni-muenchen.de/~schmid/tools/SFST/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sfst with the following command
   ```
   brew install sfst
   ```
4. sfst is ready to use now!