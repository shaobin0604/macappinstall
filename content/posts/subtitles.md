---
title: "Install Subtitles on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Automatically downloads subtitles movies and TV shows"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Subtitles on MacOS using homebrew

- App Name: Subtitles
- App description: Automatically downloads subtitles movies and TV shows
- App Version: 3.2.15
- App Website: https://subtitlesapp.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Subtitles with the following command
   ```
   brew install --cask subtitles
   ```
4. Subtitles is ready to use now!