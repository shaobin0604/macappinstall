---
title: "Install TouchSwitcher on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Use Touch Bar to switch apps"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install TouchSwitcher on MacOS using homebrew

- App Name: TouchSwitcher
- App description: Use Touch Bar to switch apps
- App Version: 1.4.3,145
- App Website: https://hazeover.com/touchswitcher.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install TouchSwitcher with the following command
   ```
   brew install --cask touchswitcher
   ```
4. TouchSwitcher is ready to use now!