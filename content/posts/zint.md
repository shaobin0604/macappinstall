---
title: "Install zint on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Barcode encoding library supporting over 50 symbologies"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install zint on MacOS using homebrew

- App Name: zint
- App description: Barcode encoding library supporting over 50 symbologies
- App Version: 2.10.0
- App Website: https://www.zint.org.uk/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install zint with the following command
   ```
   brew install zint
   ```
4. zint is ready to use now!