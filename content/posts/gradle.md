---
title: "Install gradle on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Open-source build automation tool based on the Groovy and Kotlin DSL"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gradle on MacOS using homebrew

- App Name: gradle
- App description: Open-source build automation tool based on the Groovy and Kotlin DSL
- App Version: 7.4
- App Website: https://www.gradle.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gradle with the following command
   ```
   brew install gradle
   ```
4. gradle is ready to use now!