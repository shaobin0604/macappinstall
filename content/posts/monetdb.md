---
title: "Install monetdb on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Column-store database"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install monetdb on MacOS using homebrew

- App Name: monetdb
- App description: Column-store database
- App Version: 11.43.9
- App Website: https://www.monetdb.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install monetdb with the following command
   ```
   brew install monetdb
   ```
4. monetdb is ready to use now!