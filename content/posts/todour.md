---
title: "Install Todour on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Todo.txt application Todour"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Todour on MacOS using homebrew

- App Name: Todour
- App description: Todo.txt application Todour
- App Version: 2.20
- App Website: https://nerdur.com/todour-pl/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Todour with the following command
   ```
   brew install --cask todour
   ```
4. Todour is ready to use now!