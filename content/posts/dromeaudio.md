---
title: "Install dromeaudio on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Small C++ audio manipulation and playback library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dromeaudio on MacOS using homebrew

- App Name: dromeaudio
- App description: Small C++ audio manipulation and playback library
- App Version: 0.3.0
- App Website: https://github.com/joshb/dromeaudio/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dromeaudio with the following command
   ```
   brew install dromeaudio
   ```
4. dromeaudio is ready to use now!