---
title: "Install dmenu on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Dynamic menu for X11"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dmenu on MacOS using homebrew

- App Name: dmenu
- App description: Dynamic menu for X11
- App Version: 5.1
- App Website: https://tools.suckless.org/dmenu/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dmenu with the following command
   ```
   brew install dmenu
   ```
4. dmenu is ready to use now!