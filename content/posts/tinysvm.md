---
title: "Install tinysvm on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Support vector machine library for pattern recognition"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tinysvm on MacOS using homebrew

- App Name: tinysvm
- App description: Support vector machine library for pattern recognition
- App Version: 0.09
- App Website: http://chasen.org/~taku/software/TinySVM/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tinysvm with the following command
   ```
   brew install tinysvm
   ```
4. tinysvm is ready to use now!