---
title: "Install RStudio on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Data science software focusing on R and Python"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install RStudio on MacOS using homebrew

- App Name: RStudio
- App description: Data science software focusing on R and Python
- App Version: 2021.09.2,382
- App Website: https://www.rstudio.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install RStudio with the following command
   ```
   brew install --cask rstudio
   ```
4. RStudio is ready to use now!