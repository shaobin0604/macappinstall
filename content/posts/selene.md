---
title: "Install selene on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Blazing-fast modern Lua linter"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install selene on MacOS using homebrew

- App Name: selene
- App description: Blazing-fast modern Lua linter
- App Version: 0.16.0
- App Website: https://kampfkarren.github.io/selene

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install selene with the following command
   ```
   brew install selene
   ```
4. selene is ready to use now!