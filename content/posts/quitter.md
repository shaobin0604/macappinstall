---
title: "Install Quitter on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Automatically hides or quits apps after periods of inactivity"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Quitter on MacOS using homebrew

- App Name: Quitter
- App description: Automatically hides or quits apps after periods of inactivity
- App Version: 1.0,108
- App Website: https://marco.org/apps#quitter

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Quitter with the following command
   ```
   brew install --cask quitter
   ```
4. Quitter is ready to use now!