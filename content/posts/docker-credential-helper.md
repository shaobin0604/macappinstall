---
title: "Install docker-credential-helper on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "macOS Credential Helper for Docker"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install docker-credential-helper on MacOS using homebrew

- App Name: docker-credential-helper
- App description: macOS Credential Helper for Docker
- App Version: 0.6.4
- App Website: https://github.com/docker/docker-credential-helpers

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install docker-credential-helper with the following command
   ```
   brew install docker-credential-helper
   ```
4. docker-credential-helper is ready to use now!