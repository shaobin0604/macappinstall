---
title: "Install instead on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Interpreter of simple text adventures"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install instead on MacOS using homebrew

- App Name: instead
- App description: Interpreter of simple text adventures
- App Version: 3.4.1
- App Website: https://instead.hugeping.ru/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install instead with the following command
   ```
   brew install instead
   ```
4. instead is ready to use now!