---
title: "Install Betwixt on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Web Debugging Proxy based on Chrome DevTools Network panel"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Betwixt on MacOS using homebrew

- App Name: Betwixt
- App description: Web Debugging Proxy based on Chrome DevTools Network panel
- App Version: 1.6.1
- App Website: https://github.com/kdzwinel/betwixt

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Betwixt with the following command
   ```
   brew install --cask betwixt
   ```
4. Betwixt is ready to use now!