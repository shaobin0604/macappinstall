---
title: "Install zmap on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Network scanner for Internet-wide network studies"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install zmap on MacOS using homebrew

- App Name: zmap
- App description: Network scanner for Internet-wide network studies
- App Version: 2.1.1
- App Website: https://zmap.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install zmap with the following command
   ```
   brew install zmap
   ```
4. zmap is ready to use now!