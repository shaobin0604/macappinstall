---
title: "Install renameutils on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tools for file renaming"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install renameutils on MacOS using homebrew

- App Name: renameutils
- App description: Tools for file renaming
- App Version: 0.12.0
- App Website: https://www.nongnu.org/renameutils/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install renameutils with the following command
   ```
   brew install renameutils
   ```
4. renameutils is ready to use now!