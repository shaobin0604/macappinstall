---
title: "Install spirv-llvm-translator on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool and a library for bi-directional translation between SPIR-V and LLVM IR"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install spirv-llvm-translator on MacOS using homebrew

- App Name: spirv-llvm-translator
- App description: Tool and a library for bi-directional translation between SPIR-V and LLVM IR
- App Version: 13.0.0
- App Website: https://github.com/KhronosGroup/SPIRV-LLVM-Translator

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install spirv-llvm-translator with the following command
   ```
   brew install spirv-llvm-translator
   ```
4. spirv-llvm-translator is ready to use now!