---
title: "Install libtar on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C library for manipulating POSIX tar files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libtar on MacOS using homebrew

- App Name: libtar
- App description: C library for manipulating POSIX tar files
- App Version: 1.2.20
- App Website: https://repo.or.cz/libtar.git

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libtar with the following command
   ```
   brew install libtar
   ```
4. libtar is ready to use now!