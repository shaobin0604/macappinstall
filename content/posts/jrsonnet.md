---
title: "Install jrsonnet on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Rust implementation of Jsonnet language"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install jrsonnet on MacOS using homebrew

- App Name: jrsonnet
- App description: Rust implementation of Jsonnet language
- App Version: 0.4.2
- App Website: https://github.com/CertainLach/jrsonnet

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install jrsonnet with the following command
   ```
   brew install jrsonnet
   ```
4. jrsonnet is ready to use now!