---
title: "Install mlton on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Whole-program, optimizing compiler for Standard ML"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mlton on MacOS using homebrew

- App Name: mlton
- App description: Whole-program, optimizing compiler for Standard ML
- App Version: 20210117
- App Website: http://mlton.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mlton with the following command
   ```
   brew install mlton
   ```
4. mlton is ready to use now!