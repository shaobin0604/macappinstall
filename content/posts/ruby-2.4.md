---
title: "Install ruby@2.4 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Powerful, clean, object-oriented scripting language"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ruby@2.4 on MacOS using homebrew

- App Name: ruby@2.4
- App description: Powerful, clean, object-oriented scripting language
- App Version: 2.4.10
- App Website: https://www.ruby-lang.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ruby@2.4 with the following command
   ```
   brew install ruby@2.4
   ```
4. ruby@2.4 is ready to use now!