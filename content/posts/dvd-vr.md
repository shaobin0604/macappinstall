---
title: "Install dvd-vr on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utility to identify and extract recordings from DVD-VR files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dvd-vr on MacOS using homebrew

- App Name: dvd-vr
- App description: Utility to identify and extract recordings from DVD-VR files
- App Version: 0.9.7
- App Website: https://www.pixelbeat.org/programs/dvd-vr/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dvd-vr with the following command
   ```
   brew install dvd-vr
   ```
4. dvd-vr is ready to use now!