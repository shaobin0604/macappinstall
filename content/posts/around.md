---
title: "Install Around on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Video calls designed for energy, ideas and action"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Around on MacOS using homebrew

- App Name: Around
- App description: Video calls designed for energy, ideas and action
- App Version: 0.58.27
- App Website: https://www.around.co/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Around with the following command
   ```
   brew install --cask around
   ```
4. Around is ready to use now!