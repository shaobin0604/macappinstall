---
title: "Install gpx on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Gcode to x3g converter for 3D printers running Sailfish"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gpx on MacOS using homebrew

- App Name: gpx
- App description: Gcode to x3g converter for 3D printers running Sailfish
- App Version: 2.6.8
- App Website: https://github.com/markwal/GPX/blob/HEAD/README.md

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gpx with the following command
   ```
   brew install gpx
   ```
4. gpx is ready to use now!