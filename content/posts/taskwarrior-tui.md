---
title: "Install taskwarrior-tui on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Terminal user interface for taskwarrior"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install taskwarrior-tui on MacOS using homebrew

- App Name: taskwarrior-tui
- App description: Terminal user interface for taskwarrior
- App Version: 0.19.4
- App Website: https://github.com/kdheepak/taskwarrior-tui

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install taskwarrior-tui with the following command
   ```
   brew install taskwarrior-tui
   ```
4. taskwarrior-tui is ready to use now!