---
title: "Install Prefs Editor on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Graphical user interface for the 'defaults' command"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Prefs Editor on MacOS using homebrew

- App Name: Prefs Editor
- App description: Graphical user interface for the 'defaults' command
- App Version: 1.3.0.3.0
- App Website: https://apps.tempel.org/PrefsEditor/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Prefs Editor with the following command
   ```
   brew install --cask prefs-editor
   ```
4. Prefs Editor is ready to use now!