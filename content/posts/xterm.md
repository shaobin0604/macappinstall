---
title: "Install xterm on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Terminal emulator for the X Window System"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install xterm on MacOS using homebrew

- App Name: xterm
- App description: Terminal emulator for the X Window System
- App Version: 370
- App Website: https://invisible-island.net/xterm/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install xterm with the following command
   ```
   brew install xterm
   ```
4. xterm is ready to use now!