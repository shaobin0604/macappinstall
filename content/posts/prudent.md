---
title: "Install Prudent on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Integrated environment for your personal and family ledger"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Prudent on MacOS using homebrew

- App Name: Prudent
- App description: Integrated environment for your personal and family ledger
- App Version: 29
- App Website: https://prudent.me/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Prudent with the following command
   ```
   brew install --cask prudent
   ```
4. Prudent is ready to use now!