---
title: "Install FabFilter Saturn on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Multiband distorsion/saturation plug-in"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install FabFilter Saturn on MacOS using homebrew

- App Name: FabFilter Saturn
- App description: Multiband distorsion/saturation plug-in
- App Version: 2.05
- App Website: https://www.fabfilter.com/products/saturn-2-multiband-distortion-saturation-plug-in

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install FabFilter Saturn with the following command
   ```
   brew install --cask fabfilter-saturn
   ```
4. FabFilter Saturn is ready to use now!