---
title: "Install BlueHarvest on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Remove metadata files from external drives"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install BlueHarvest on MacOS using homebrew

- App Name: BlueHarvest
- App description: Remove metadata files from external drives
- App Version: 8.0.12
- App Website: https://zeroonetwenty.com/blueharvest/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install BlueHarvest with the following command
   ```
   brew install --cask blueharvest
   ```
4. BlueHarvest is ready to use now!