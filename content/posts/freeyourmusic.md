---
title: "Install FreeYourMusic on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Move playlists, tracks, and albums between music platforms"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install FreeYourMusic on MacOS using homebrew

- App Name: FreeYourMusic
- App description: Move playlists, tracks, and albums between music platforms
- App Version: 6.2.4
- App Website: https://freeyourmusic.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install FreeYourMusic with the following command
   ```
   brew install --cask freeyourmusic
   ```
4. FreeYourMusic is ready to use now!