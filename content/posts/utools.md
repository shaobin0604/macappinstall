---
title: "Install uTools on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Plug-in productivity tool set"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install uTools on MacOS using homebrew

- App Name: uTools
- App description: Plug-in productivity tool set
- App Version: 2.5.2
- App Website: https://u.tools/index.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install uTools with the following command
   ```
   brew install --cask utools
   ```
4. uTools is ready to use now!