---
title: "Install movgrab on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Downloader for youtube, dailymotion, and other video websites"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install movgrab on MacOS using homebrew

- App Name: movgrab
- App description: Downloader for youtube, dailymotion, and other video websites
- App Version: 3.1.2
- App Website: https://sites.google.com/site/columscode/home/movgrab

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install movgrab with the following command
   ```
   brew install movgrab
   ```
4. movgrab is ready to use now!