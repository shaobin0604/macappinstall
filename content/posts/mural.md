---
title: "Install MURAL on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Visual online collaboration platform"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install MURAL on MacOS using homebrew

- App Name: MURAL
- App description: Visual online collaboration platform
- App Version: 1.0.11
- App Website: https://mural.co/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install MURAL with the following command
   ```
   brew install --cask mural
   ```
4. MURAL is ready to use now!