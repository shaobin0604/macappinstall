---
title: "Install Folder Colorizer on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Folder icon editor & manager"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Folder Colorizer on MacOS using homebrew

- App Name: Folder Colorizer
- App description: Folder icon editor & manager
- App Version: 4.0.0
- App Website: https://softorino.com/folder-colorizer-mac/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Folder Colorizer with the following command
   ```
   brew install --cask folder-colorizer
   ```
4. Folder Colorizer is ready to use now!