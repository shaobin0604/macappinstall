---
title: "Install Fork on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GIT client"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Fork on MacOS using homebrew

- App Name: Fork
- App description: GIT client
- App Version: 2.15.1
- App Website: https://fork.dev/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Fork with the following command
   ```
   brew install --cask fork
   ```
4. Fork is ready to use now!