---
title: "Install libwbxml on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library and tools to parse and encode WBXML documents"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libwbxml on MacOS using homebrew

- App Name: libwbxml
- App description: Library and tools to parse and encode WBXML documents
- App Version: 0.11.7
- App Website: https://github.com/libwbxml/libwbxml

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libwbxml with the following command
   ```
   brew install libwbxml
   ```
4. libwbxml is ready to use now!