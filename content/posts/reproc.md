---
title: "Install reproc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cross-platform (C99/C++11) process library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install reproc on MacOS using homebrew

- App Name: reproc
- App description: Cross-platform (C99/C++11) process library
- App Version: 14.2.4
- App Website: https://github.com/DaanDeMeyer/reproc

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install reproc with the following command
   ```
   brew install reproc
   ```
4. reproc is ready to use now!