---
title: "Install fastme on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Accurate and fast distance-based phylogeny inference program"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fastme on MacOS using homebrew

- App Name: fastme
- App description: Accurate and fast distance-based phylogeny inference program
- App Version: 2.1.6.3
- App Website: http://www.atgc-montpellier.fr/fastme/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fastme with the following command
   ```
   brew install fastme
   ```
4. fastme is ready to use now!