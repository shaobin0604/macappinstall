---
title: "Install usbutils on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "List detailed info about USB devices"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install usbutils on MacOS using homebrew

- App Name: usbutils
- App description: List detailed info about USB devices
- App Version: 014
- App Website: http://www.linux-usb.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install usbutils with the following command
   ```
   brew install usbutils
   ```
4. usbutils is ready to use now!