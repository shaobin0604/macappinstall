---
title: "Install zet on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CLI utility to find the union, intersection, and set difference of files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install zet on MacOS using homebrew

- App Name: zet
- App description: CLI utility to find the union, intersection, and set difference of files
- App Version: 0.2.0
- App Website: https://github.com/yarrow/zet

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install zet with the following command
   ```
   brew install zet
   ```
4. zet is ready to use now!