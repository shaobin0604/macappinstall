---
title: "Install Gitfox on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Git client"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Gitfox on MacOS using homebrew

- App Name: Gitfox
- App description: Git client
- App Version: 2.4.0,6656
- App Website: https://www.gitfox.app/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Gitfox with the following command
   ```
   brew install --cask gitfox
   ```
4. Gitfox is ready to use now!