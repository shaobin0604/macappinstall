---
title: "Install Brain.fm on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Desktop client for brain.fm"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Brain.fm on MacOS using homebrew

- App Name: Brain.fm
- App description: Desktop client for brain.fm
- App Version: 0.1.5
- App Website: https://github.com/Dinius/Brain.fm-Desktop-Client

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Brain.fm with the following command
   ```
   brew install --cask brainfm
   ```
4. Brain.fm is ready to use now!