---
title: "Install GPG Suite (without GPG Mail) on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tools to protect your files"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install GPG Suite (without GPG Mail) on MacOS using homebrew

- App Name: GPG Suite (without GPG Mail)
- App description: Tools to protect your files
- App Version: 2021.3
- App Website: https://gpgtools.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install GPG Suite (without GPG Mail) with the following command
   ```
   brew install --cask gpg-suite-no-mail
   ```
4. GPG Suite (without GPG Mail) is ready to use now!