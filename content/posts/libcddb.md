---
title: "Install libcddb on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CDDB server access library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libcddb on MacOS using homebrew

- App Name: libcddb
- App description: CDDB server access library
- App Version: 1.3.2
- App Website: https://libcddb.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libcddb with the following command
   ```
   brew install libcddb
   ```
4. libcddb is ready to use now!