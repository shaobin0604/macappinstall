---
title: "Install jitouch on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Multi-touch gestures editor"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install jitouch on MacOS using homebrew

- App Name: jitouch
- App description: Multi-touch gestures editor
- App Version: latest
- App Website: https://www.jitouch.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install jitouch with the following command
   ```
   brew install --cask jitouch
   ```
4. jitouch is ready to use now!