---
title: "Install flume on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Hadoop-based distributed log collection and aggregation"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install flume on MacOS using homebrew

- App Name: flume
- App description: Hadoop-based distributed log collection and aggregation
- App Version: 1.9.0
- App Website: https://flume.apache.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install flume with the following command
   ```
   brew install flume
   ```
4. flume is ready to use now!