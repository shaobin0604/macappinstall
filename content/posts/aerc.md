---
title: "Install aerc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Email client that runs in your terminal"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install aerc on MacOS using homebrew

- App Name: aerc
- App description: Email client that runs in your terminal
- App Version: 0.7.1
- App Website: https://aerc-mail.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install aerc with the following command
   ```
   brew install aerc
   ```
4. aerc is ready to use now!