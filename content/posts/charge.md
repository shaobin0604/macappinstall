---
title: "Install charge on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Opinionated, zero-config static site generator"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install charge on MacOS using homebrew

- App Name: charge
- App description: Opinionated, zero-config static site generator
- App Version: 1.7.0
- App Website: https://charge.js.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install charge with the following command
   ```
   brew install charge
   ```
4. charge is ready to use now!