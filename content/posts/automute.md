---
title: "Install AutoMute on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Mute or unmute the system based on the current Wi-Fi network"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install AutoMute on MacOS using homebrew

- App Name: AutoMute
- App description: Mute or unmute the system based on the current Wi-Fi network
- App Version: 1.1
- App Website: https://github.com/Lorenzo45/AutoMute

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install AutoMute with the following command
   ```
   brew install --cask automute
   ```
4. AutoMute is ready to use now!