---
title: "Install juliaup on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Julia installer and version multiplexer"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install juliaup on MacOS using homebrew

- App Name: juliaup
- App description: Julia installer and version multiplexer
- App Version: 1.5.36
- App Website: https://github.com/JuliaLang/juliaup

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install juliaup with the following command
   ```
   brew install juliaup
   ```
4. juliaup is ready to use now!