---
title: "Install sratom on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library for serializing LV2 atoms to/from RDF"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sratom on MacOS using homebrew

- App Name: sratom
- App description: Library for serializing LV2 atoms to/from RDF
- App Version: 0.6.8
- App Website: https://drobilla.net/software/sratom.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sratom with the following command
   ```
   brew install sratom
   ```
4. sratom is ready to use now!