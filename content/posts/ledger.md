---
title: "Install ledger on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line, double-entry accounting tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ledger on MacOS using homebrew

- App Name: ledger
- App description: Command-line, double-entry accounting tool
- App Version: 3.2.1
- App Website: https://ledger-cli.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ledger with the following command
   ```
   brew install ledger
   ```
4. ledger is ready to use now!