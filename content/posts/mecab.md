---
title: "Install mecab on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Yet another part-of-speech and morphological analyzer"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mecab on MacOS using homebrew

- App Name: mecab
- App description: Yet another part-of-speech and morphological analyzer
- App Version: 0.996
- App Website: https://taku910.github.io/mecab/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mecab with the following command
   ```
   brew install mecab
   ```
4. mecab is ready to use now!