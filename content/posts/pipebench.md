---
title: "Install pipebench on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Measure the speed of STDIN/STDOUT communication"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pipebench on MacOS using homebrew

- App Name: pipebench
- App description: Measure the speed of STDIN/STDOUT communication
- App Version: 0.40
- App Website: https://www.habets.pp.se/synscan/programs_pipebench.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pipebench with the following command
   ```
   brew install pipebench
   ```
4. pipebench is ready to use now!