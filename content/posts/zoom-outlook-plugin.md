---
title: "Install Zoom.us Outlook Plugin on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Outlook Plugin for Zoom.us"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Zoom.us Outlook Plugin on MacOS using homebrew

- App Name: Zoom.us Outlook Plugin
- App description: Outlook Plugin for Zoom.us
- App Version: 5.8.6,2021.11.23
- App Website: https://www.zoom.us/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Zoom.us Outlook Plugin with the following command
   ```
   brew install --cask zoom-outlook-plugin
   ```
4. Zoom.us Outlook Plugin is ready to use now!