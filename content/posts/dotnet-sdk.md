---
title: "Install .NET SDK on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Developer platform"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install .NET SDK on MacOS using homebrew

- App Name: .NET SDK
- App description: Developer platform
- App Version: 6.0.200,0f1f23eb-004f-41a2-a4ef-e1a9b533a794,f2d47f0ed6a7be2027166ea1132cc806
- App Website: https://www.microsoft.com/net/core#macos

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install .NET SDK with the following command
   ```
   brew install --cask dotnet-sdk
   ```
4. .NET SDK is ready to use now!