---
title: "Install httrack on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Website copier/offline browser"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install httrack on MacOS using homebrew

- App Name: httrack
- App description: Website copier/offline browser
- App Version: 3.49.2
- App Website: https://www.httrack.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install httrack with the following command
   ```
   brew install httrack
   ```
4. httrack is ready to use now!