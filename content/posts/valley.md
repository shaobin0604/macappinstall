---
title: "Install Valley Benchmark on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Software to test performance and stability for PC hardware"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Valley Benchmark on MacOS using homebrew

- App Name: Valley Benchmark
- App description: Software to test performance and stability for PC hardware
- App Version: 1.0
- App Website: https://benchmark.unigine.com/valley

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Valley Benchmark with the following command
   ```
   brew install --cask valley
   ```
4. Valley Benchmark is ready to use now!