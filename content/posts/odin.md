---
title: "Install odin on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Programming language with focus on simplicity, performance and modern systems"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install odin on MacOS using homebrew

- App Name: odin
- App description: Programming language with focus on simplicity, performance and modern systems
- App Version: 0.13.0
- App Website: https://odin-lang.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install odin with the following command
   ```
   brew install odin
   ```
4. odin is ready to use now!