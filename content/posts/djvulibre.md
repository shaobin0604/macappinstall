---
title: "Install djvulibre on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "DjVu viewer"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install djvulibre on MacOS using homebrew

- App Name: djvulibre
- App description: DjVu viewer
- App Version: 3.5.28
- App Website: https://djvu.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install djvulibre with the following command
   ```
   brew install djvulibre
   ```
4. djvulibre is ready to use now!