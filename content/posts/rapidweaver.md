---
title: "Install RapidWeaver on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Web design software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install RapidWeaver on MacOS using homebrew

- App Name: RapidWeaver
- App description: Web design software
- App Version: 8.9.3,20888
- App Website: https://www.realmacsoftware.com/rapidweaver/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install RapidWeaver with the following command
   ```
   brew install --cask rapidweaver
   ```
4. RapidWeaver is ready to use now!