---
title: "Install IntelliJ IDEA Community Edition on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "IDE for Java development - community edition"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install IntelliJ IDEA Community Edition on MacOS using homebrew

- App Name: IntelliJ IDEA Community Edition
- App description: IDE for Java development - community edition
- App Version: 2021.3.2,213.6777.52
- App Website: https://www.jetbrains.com/idea/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install IntelliJ IDEA Community Edition with the following command
   ```
   brew install --cask intellij-idea-ce
   ```
4. IntelliJ IDEA Community Edition is ready to use now!