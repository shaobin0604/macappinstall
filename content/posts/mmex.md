---
title: "Install Money Manager Ex on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Money management application"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Money Manager Ex on MacOS using homebrew

- App Name: Money Manager Ex
- App description: Money management application
- App Version: 1.5.13
- App Website: https://www.moneymanagerex.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Money Manager Ex with the following command
   ```
   brew install --cask mmex
   ```
4. Money Manager Ex is ready to use now!