---
title: "Install Lightform Creator on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "AR projection and audio reactivity software for Lightform devices"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Lightform Creator on MacOS using homebrew

- App Name: Lightform Creator
- App description: AR projection and audio reactivity software for Lightform devices
- App Version: 2.0.6.920
- App Website: https://lightform.com/creator

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Lightform Creator with the following command
   ```
   brew install --cask lightform
   ```
4. Lightform Creator is ready to use now!