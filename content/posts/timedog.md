---
title: "Install timedog on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Lists files that were saved by a backup of the macOS Time Machine"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install timedog on MacOS using homebrew

- App Name: timedog
- App description: Lists files that were saved by a backup of the macOS Time Machine
- App Version: 1.4
- App Website: https://github.com/nlfiedler/timedog

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install timedog with the following command
   ```
   brew install timedog
   ```
4. timedog is ready to use now!