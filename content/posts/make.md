---
title: "Install make on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utility for directing compilation"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install make on MacOS using homebrew

- App Name: make
- App description: Utility for directing compilation
- App Version: 4.3
- App Website: https://www.gnu.org/software/make/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install make with the following command
   ```
   brew install make
   ```
4. make is ready to use now!