---
title: "Install bzr-upload on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Bazaar plugin to incrementally upload changes to a dumb server"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bzr-upload on MacOS using homebrew

- App Name: bzr-upload
- App description: Bazaar plugin to incrementally upload changes to a dumb server
- App Version: 1.1.0
- App Website: https://launchpad.net/bzr-upload

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bzr-upload with the following command
   ```
   brew install bzr-upload
   ```
4. bzr-upload is ready to use now!