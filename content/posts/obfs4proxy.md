---
title: "Install obfs4proxy on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Pluggable transport proxy for Tor, implementing obfs4"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install obfs4proxy on MacOS using homebrew

- App Name: obfs4proxy
- App description: Pluggable transport proxy for Tor, implementing obfs4
- App Version: 0.0.13
- App Website: https://gitlab.com/yawning/obfs4

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install obfs4proxy with the following command
   ```
   brew install obfs4proxy
   ```
4. obfs4proxy is ready to use now!