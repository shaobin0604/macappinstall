---
title: "Install iprint on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Provides a print_one function"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install iprint on MacOS using homebrew

- App Name: iprint
- App description: Provides a print_one function
- App Version: 1.3-9
- App Website: https://www.samba.org/ftp/unpacked/junkcode/i.c

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install iprint with the following command
   ```
   brew install iprint
   ```
4. iprint is ready to use now!