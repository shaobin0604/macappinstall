---
title: "Install DevilutionX on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Diablo build for modern operating systems"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install DevilutionX on MacOS using homebrew

- App Name: DevilutionX
- App description: Diablo build for modern operating systems
- App Version: 1.3.0
- App Website: https://github.com/diasurgical/devilutionX/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install DevilutionX with the following command
   ```
   brew install --cask devilutionx
   ```
4. DevilutionX is ready to use now!