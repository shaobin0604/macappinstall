---
title: "Install dnsdist on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Highly DNS-, DoS- and abuse-aware loadbalancer"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dnsdist on MacOS using homebrew

- App Name: dnsdist
- App description: Highly DNS-, DoS- and abuse-aware loadbalancer
- App Version: 1.7.0
- App Website: https://www.dnsdist.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dnsdist with the following command
   ```
   brew install dnsdist
   ```
4. dnsdist is ready to use now!