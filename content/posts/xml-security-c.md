---
title: "Install xml-security-c on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Implementation of primary security standards for XML"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install xml-security-c on MacOS using homebrew

- App Name: xml-security-c
- App description: Implementation of primary security standards for XML
- App Version: 2.0.2
- App Website: https://santuario.apache.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install xml-security-c with the following command
   ```
   brew install xml-security-c
   ```
4. xml-security-c is ready to use now!