---
title: "Install clash on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Rule-based tunnel in Go"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install clash on MacOS using homebrew

- App Name: clash
- App description: Rule-based tunnel in Go
- App Version: 1.9.0
- App Website: https://github.com/Dreamacro/clash

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install clash with the following command
   ```
   brew install clash
   ```
4. clash is ready to use now!