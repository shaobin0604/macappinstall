---
title: "Install bioawk on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "AWK modified for biological data"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bioawk on MacOS using homebrew

- App Name: bioawk
- App description: AWK modified for biological data
- App Version: 1.0
- App Website: https://github.com/lh3/bioawk

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bioawk with the following command
   ```
   brew install bioawk
   ```
4. bioawk is ready to use now!