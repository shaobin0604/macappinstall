---
title: "Install nu-smv on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Reimplementation and extension of SMV symbolic model checker"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install nu-smv on MacOS using homebrew

- App Name: nu-smv
- App description: Reimplementation and extension of SMV symbolic model checker
- App Version: 2.6.0
- App Website: https://nusmv.fbk.eu

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install nu-smv with the following command
   ```
   brew install nu-smv
   ```
4. nu-smv is ready to use now!