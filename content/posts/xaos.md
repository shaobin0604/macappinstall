---
title: "Install GNU XaoS on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install GNU XaoS on MacOS using homebrew

- App Name: GNU XaoS
- App description: null
- App Version: 4.2.1
- App Website: https://xaos-project.github.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install GNU XaoS with the following command
   ```
   brew install --cask xaos
   ```
4. GNU XaoS is ready to use now!