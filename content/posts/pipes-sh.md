---
title: "Install pipes-sh on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Animated pipes terminal screensaver"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pipes-sh on MacOS using homebrew

- App Name: pipes-sh
- App description: Animated pipes terminal screensaver
- App Version: 1.3.0
- App Website: https://github.com/pipeseroni/pipes.sh

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pipes-sh with the following command
   ```
   brew install pipes-sh
   ```
4. pipes-sh is ready to use now!