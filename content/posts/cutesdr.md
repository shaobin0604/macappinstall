---
title: "Install CuteSDR on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Demodulation and spectrum display program"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install CuteSDR on MacOS using homebrew

- App Name: CuteSDR
- App description: Demodulation and spectrum display program
- App Version: 1.20
- App Website: https://sourceforge.net/projects/cutesdr/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install CuteSDR with the following command
   ```
   brew install --cask cutesdr
   ```
4. CuteSDR is ready to use now!