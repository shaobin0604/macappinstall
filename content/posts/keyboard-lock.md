---
title: "Install Keyboard Lock on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Simple keyboard locker"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Keyboard Lock on MacOS using homebrew

- App Name: Keyboard Lock
- App description: Simple keyboard locker
- App Version: 1.0
- App Website: https://keylock.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Keyboard Lock with the following command
   ```
   brew install --cask keyboard-lock
   ```
4. Keyboard Lock is ready to use now!