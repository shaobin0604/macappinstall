---
title: "Install jless on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line pager for JSON data"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install jless on MacOS using homebrew

- App Name: jless
- App description: Command-line pager for JSON data
- App Version: 0.7.1
- App Website: https://pauljuliusmartinez.github.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install jless with the following command
   ```
   brew install jless
   ```
4. jless is ready to use now!