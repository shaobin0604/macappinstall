---
title: "Install ats2-postiats on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Programming language with formal specification features"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ats2-postiats on MacOS using homebrew

- App Name: ats2-postiats
- App description: Programming language with formal specification features
- App Version: 0.4.2
- App Website: http://www.ats-lang.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ats2-postiats with the following command
   ```
   brew install ats2-postiats
   ```
4. ats2-postiats is ready to use now!