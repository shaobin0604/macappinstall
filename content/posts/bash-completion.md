---
title: "Install bash-completion on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Programmable completion for Bash 3.2"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bash-completion on MacOS using homebrew

- App Name: bash-completion
- App description: Programmable completion for Bash 3.2
- App Version: 1.3
- App Website: https://salsa.debian.org/debian/bash-completion

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bash-completion with the following command
   ```
   brew install bash-completion
   ```
4. bash-completion is ready to use now!