---
title: "Install mplayershell on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Improved visual experience for MPlayer on macOS"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mplayershell on MacOS using homebrew

- App Name: mplayershell
- App description: Improved visual experience for MPlayer on macOS
- App Version: 0.9.3
- App Website: https://github.com/donmelton/MPlayerShell

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mplayershell with the following command
   ```
   brew install mplayershell
   ```
4. mplayershell is ready to use now!