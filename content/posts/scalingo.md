---
title: "Install scalingo on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CLI for working with Scalingo's PaaS"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install scalingo on MacOS using homebrew

- App Name: scalingo
- App description: CLI for working with Scalingo's PaaS
- App Version: 1.22.1
- App Website: https://doc.scalingo.com/cli

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install scalingo with the following command
   ```
   brew install scalingo
   ```
4. scalingo is ready to use now!