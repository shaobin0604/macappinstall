---
title: "Install Cyberbotics Webots Robot Simulator on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Open source desktop application used to simulate robots"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Cyberbotics Webots Robot Simulator on MacOS using homebrew

- App Name: Cyberbotics Webots Robot Simulator
- App description: Open source desktop application used to simulate robots
- App Version: R2022a
- App Website: https://www.cyberbotics.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Cyberbotics Webots Robot Simulator with the following command
   ```
   brew install --cask webots
   ```
4. Cyberbotics Webots Robot Simulator is ready to use now!