---
title: "Install dopewars on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Free rewrite of a game originally based on \"Drug Wars\""
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dopewars on MacOS using homebrew

- App Name: dopewars
- App description: Free rewrite of a game originally based on "Drug Wars"
- App Version: 1.6.1
- App Website: https://dopewars.sourceforge.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dopewars with the following command
   ```
   brew install dopewars
   ```
4. dopewars is ready to use now!