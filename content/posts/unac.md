---
title: "Install unac on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C library and command that removes accents from a string"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install unac on MacOS using homebrew

- App Name: unac
- App description: C library and command that removes accents from a string
- App Version: 1.8.0
- App Website: https://savannah.nongnu.org/projects/unac

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install unac with the following command
   ```
   brew install unac
   ```
4. unac is ready to use now!