---
title: "Install Micro Snitch on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Monitors and reports any microphone and camera activity"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Micro Snitch on MacOS using homebrew

- App Name: Micro Snitch
- App description: Monitors and reports any microphone and camera activity
- App Version: 1.5
- App Website: https://www.obdev.at/products/microsnitch/index.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Micro Snitch with the following command
   ```
   brew install --cask micro-snitch
   ```
4. Micro Snitch is ready to use now!