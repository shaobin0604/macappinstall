---
title: "Install redpen on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Proofreading tool to help writers of technical documentation"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install redpen on MacOS using homebrew

- App Name: redpen
- App description: Proofreading tool to help writers of technical documentation
- App Version: 1.10.4
- App Website: https://redpen.cc/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install redpen with the following command
   ```
   brew install redpen
   ```
4. redpen is ready to use now!