---
title: "Install cpptest on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Unit testing framework handling automated tests in C++"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cpptest on MacOS using homebrew

- App Name: cpptest
- App description: Unit testing framework handling automated tests in C++
- App Version: 2.0.0
- App Website: https://cpptest.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cpptest with the following command
   ```
   brew install cpptest
   ```
4. cpptest is ready to use now!