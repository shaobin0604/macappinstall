---
title: "Install Screaming Frog SEO Spider on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "SEO site audit tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Screaming Frog SEO Spider on MacOS using homebrew

- App Name: Screaming Frog SEO Spider
- App description: SEO site audit tool
- App Version: 16.6
- App Website: https://www.screamingfrog.co.uk/seo-spider/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Screaming Frog SEO Spider with the following command
   ```
   brew install --cask screaming-frog-seo-spider
   ```
4. Screaming Frog SEO Spider is ready to use now!