---
title: "Install snag on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Automatic build tool for all your needs"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install snag on MacOS using homebrew

- App Name: snag
- App description: Automatic build tool for all your needs
- App Version: 1.2.0
- App Website: https://github.com/Tonkpils/snag

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install snag with the following command
   ```
   brew install snag
   ```
4. snag is ready to use now!