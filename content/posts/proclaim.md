---
title: "Install Faithlife Proclaim on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Church presentation software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Faithlife Proclaim on MacOS using homebrew

- App Name: Faithlife Proclaim
- App description: Church presentation software
- App Version: 3.1.0.0092,311.0.92
- App Website: https://faithlife.com/products/proclaim

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Faithlife Proclaim with the following command
   ```
   brew install --cask proclaim
   ```
4. Faithlife Proclaim is ready to use now!