---
title: "Install jailkit on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utilities to create limited user accounts in a chroot jail"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install jailkit on MacOS using homebrew

- App Name: jailkit
- App description: Utilities to create limited user accounts in a chroot jail
- App Version: 2.23
- App Website: https://olivier.sessink.nl/jailkit/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install jailkit with the following command
   ```
   brew install jailkit
   ```
4. jailkit is ready to use now!