---
title: "Install Athens on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Self-hosted knowledge graph"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Athens on MacOS using homebrew

- App Name: Athens
- App description: Self-hosted knowledge graph
- App Version: 1.0.0
- App Website: https://www.athensresearch.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Athens with the following command
   ```
   brew install --cask athens
   ```
4. Athens is ready to use now!