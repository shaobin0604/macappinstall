---
title: "Install NagBar on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Status bar monitor for Nagios, Icinga/2 and Thruk"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install NagBar on MacOS using homebrew

- App Name: NagBar
- App description: Status bar monitor for Nagios, Icinga/2 and Thruk
- App Version: 1.3.7
- App Website: https://sites.google.com/site/nagbarapp/home

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install NagBar with the following command
   ```
   brew install --cask nagbar
   ```
4. NagBar is ready to use now!