---
title: "Install startup-notification on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Reference implementation of startup notification protocol"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install startup-notification on MacOS using homebrew

- App Name: startup-notification
- App description: Reference implementation of startup notification protocol
- App Version: 0.12
- App Website: https://www.freedesktop.org/wiki/Software/startup-notification/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install startup-notification with the following command
   ```
   brew install startup-notification
   ```
4. startup-notification is ready to use now!