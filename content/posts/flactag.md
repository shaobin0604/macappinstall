---
title: "Install flactag on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tag single album FLAC files with MusicBrainz CUE sheets"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install flactag on MacOS using homebrew

- App Name: flactag
- App description: Tag single album FLAC files with MusicBrainz CUE sheets
- App Version: 2.0.4
- App Website: https://flactag.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install flactag with the following command
   ```
   brew install flactag
   ```
4. flactag is ready to use now!