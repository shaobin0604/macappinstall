---
title: "Install corkscrew on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tunnel SSH through HTTP proxies"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install corkscrew on MacOS using homebrew

- App Name: corkscrew
- App description: Tunnel SSH through HTTP proxies
- App Version: 2.0
- App Website: https://packages.debian.org/sid/corkscrew

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install corkscrew with the following command
   ```
   brew install corkscrew
   ```
4. corkscrew is ready to use now!