---
title: "Install Usage on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tracks application usage"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Usage on MacOS using homebrew

- App Name: Usage
- App description: Tracks application usage
- App Version: 1.4.5.1
- App Website: https://www.mediaatelier.com/Usage/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Usage with the following command
   ```
   brew install --cask usage
   ```
4. Usage is ready to use now!