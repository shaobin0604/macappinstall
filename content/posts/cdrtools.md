---
title: "Install cdrtools on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CD/DVD/Blu-ray premastering and recording software"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cdrtools on MacOS using homebrew

- App Name: cdrtools
- App description: CD/DVD/Blu-ray premastering and recording software
- App Version: 3.02a09
- App Website: https://cdrtools.sourceforge.io/private/cdrecord.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cdrtools with the following command
   ```
   brew install cdrtools
   ```
4. cdrtools is ready to use now!