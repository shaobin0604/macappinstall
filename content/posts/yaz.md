---
title: "Install yaz on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Toolkit for Z39.50/SRW/SRU clients/servers"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install yaz on MacOS using homebrew

- App Name: yaz
- App description: Toolkit for Z39.50/SRW/SRU clients/servers
- App Version: 5.31.1
- App Website: https://www.indexdata.com/resources/software/yaz/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install yaz with the following command
   ```
   brew install yaz
   ```
4. yaz is ready to use now!