---
title: "Install libogg on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Ogg Bitstream Library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libogg on MacOS using homebrew

- App Name: libogg
- App description: Ogg Bitstream Library
- App Version: 1.3.5
- App Website: https://www.xiph.org/ogg/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libogg with the following command
   ```
   brew install libogg
   ```
4. libogg is ready to use now!