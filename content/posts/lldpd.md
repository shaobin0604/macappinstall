---
title: "Install lldpd on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Implementation of IEEE 802.1ab (LLDP)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install lldpd on MacOS using homebrew

- App Name: lldpd
- App description: Implementation of IEEE 802.1ab (LLDP)
- App Version: 1.0.13
- App Website: https://lldpd.github.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install lldpd with the following command
   ```
   brew install lldpd
   ```
4. lldpd is ready to use now!