---
title: "Install OmniGraffle on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Visual communication software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install OmniGraffle on MacOS using homebrew

- App Name: OmniGraffle
- App description: Visual communication software
- App Version: 7.19.4
- App Website: https://www.omnigroup.com/omnigraffle/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install OmniGraffle with the following command
   ```
   brew install --cask omnigraffle
   ```
4. OmniGraffle is ready to use now!