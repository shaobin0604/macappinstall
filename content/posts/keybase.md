---
title: "Install Keybase on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "End-to-end encryption software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Keybase on MacOS using homebrew

- App Name: Keybase
- App description: End-to-end encryption software
- App Version: 5.9.2,20220131221715,a25f15e42b
- App Website: https://keybase.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Keybase with the following command
   ```
   brew install --cask keybase
   ```
4. Keybase is ready to use now!