---
title: "Install Arctype on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "SQL client and database management tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Arctype on MacOS using homebrew

- App Name: Arctype
- App description: SQL client and database management tool
- App Version: 0.9.48
- App Website: https://arctype.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Arctype with the following command
   ```
   brew install --cask arctype
   ```
4. Arctype is ready to use now!