---
title: "Install fizmo on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Z-Machine interpreter"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fizmo on MacOS using homebrew

- App Name: fizmo
- App description: Z-Machine interpreter
- App Version: 0.8.5
- App Website: https://fizmo.spellbreaker.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fizmo with the following command
   ```
   brew install fizmo
   ```
4. fizmo is ready to use now!