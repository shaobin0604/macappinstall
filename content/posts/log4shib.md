---
title: "Install log4shib on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Forked version of log4cpp for the Shibboleth project"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install log4shib on MacOS using homebrew

- App Name: log4shib
- App description: Forked version of log4cpp for the Shibboleth project
- App Version: 2.0.1
- App Website: https://wiki.shibboleth.net/confluence/display/OpenSAML/log4shib

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install log4shib with the following command
   ```
   brew install log4shib
   ```
4. log4shib is ready to use now!