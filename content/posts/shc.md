---
title: "Install shc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Shell Script Compiler"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install shc on MacOS using homebrew

- App Name: shc
- App description: Shell Script Compiler
- App Version: 4.0.3
- App Website: https://neurobin.github.io/shc

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install shc with the following command
   ```
   brew install shc
   ```
4. shc is ready to use now!