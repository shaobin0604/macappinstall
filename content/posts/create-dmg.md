---
title: "Install create-dmg on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Shell script to build fancy DMGs"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install create-dmg on MacOS using homebrew

- App Name: create-dmg
- App description: Shell script to build fancy DMGs
- App Version: 1.0.9
- App Website: https://github.com/create-dmg/create-dmg

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install create-dmg with the following command
   ```
   brew install create-dmg
   ```
4. create-dmg is ready to use now!