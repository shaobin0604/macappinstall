---
title: "Install megacmd on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line client for mega.co.nz storage service"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install megacmd on MacOS using homebrew

- App Name: megacmd
- App description: Command-line client for mega.co.nz storage service
- App Version: 0.016
- App Website: https://github.com/t3rm1n4l/megacmd

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install megacmd with the following command
   ```
   brew install megacmd
   ```
4. megacmd is ready to use now!