---
title: "Install VOX Preferences on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "VOX Add-on for Apple Remote, EarPods and System Buttons"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install VOX Preferences on MacOS using homebrew

- App Name: VOX Preferences
- App description: VOX Add-on for Apple Remote, EarPods and System Buttons
- App Version: 1.5.14
- App Website: https://vox.rocks/mac-music-player/control-extension-download

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install VOX Preferences with the following command
   ```
   brew install --cask vox-preferences-pane
   ```
4. VOX Preferences is ready to use now!