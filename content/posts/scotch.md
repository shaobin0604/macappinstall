---
title: "Install scotch on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Package for graph partitioning, graph clustering, and sparse matrix ordering"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install scotch on MacOS using homebrew

- App Name: scotch
- App description: Package for graph partitioning, graph clustering, and sparse matrix ordering
- App Version: 7.0.1
- App Website: https://gitlab.inria.fr/scotch/scotch

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install scotch with the following command
   ```
   brew install scotch
   ```
4. scotch is ready to use now!