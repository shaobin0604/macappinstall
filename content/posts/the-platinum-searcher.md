---
title: "Install the_platinum_searcher on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Multi-platform code-search similar to ack and ag"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install the_platinum_searcher on MacOS using homebrew

- App Name: the_platinum_searcher
- App description: Multi-platform code-search similar to ack and ag
- App Version: 2.2.0
- App Website: https://github.com/monochromegane/the_platinum_searcher

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install the_platinum_searcher with the following command
   ```
   brew install the_platinum_searcher
   ```
4. the_platinum_searcher is ready to use now!