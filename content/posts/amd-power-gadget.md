---
title: "Install AMD Power Gadget on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Power management, monitoring and VirtualSMC plugin for AMD processors"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install AMD Power Gadget on MacOS using homebrew

- App Name: AMD Power Gadget
- App description: Power management, monitoring and VirtualSMC plugin for AMD processors
- App Version: 0.7
- App Website: https://github.com/trulyspinach/SMCAMDProcessor

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install AMD Power Gadget with the following command
   ```
   brew install --cask amd-power-gadget
   ```
4. AMD Power Gadget is ready to use now!