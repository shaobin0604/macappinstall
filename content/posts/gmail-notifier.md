---
title: "Install Gmail Notifier on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Minimalist Gmail inbox notifications app"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Gmail Notifier on MacOS using homebrew

- App Name: Gmail Notifier
- App description: Minimalist Gmail inbox notifications app
- App Version: 2.1.0
- App Website: https://github.com/jashephe/Gmail-Notifier

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Gmail Notifier with the following command
   ```
   brew install --cask gmail-notifier
   ```
4. Gmail Notifier is ready to use now!