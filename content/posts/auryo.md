---
title: "Install Auryo on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Unofficial desktop app for Soundcloud"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Auryo on MacOS using homebrew

- App Name: Auryo
- App description: Unofficial desktop app for Soundcloud
- App Version: 2.5.4
- App Website: https://auryo.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Auryo with the following command
   ```
   brew install --cask auryo
   ```
4. Auryo is ready to use now!