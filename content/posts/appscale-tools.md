---
title: "Install appscale-tools on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line tools for working with AppScale"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install appscale-tools on MacOS using homebrew

- App Name: appscale-tools
- App description: Command-line tools for working with AppScale
- App Version: 3.5.3
- App Website: https://github.com/AppScale/appscale-tools

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install appscale-tools with the following command
   ```
   brew install appscale-tools
   ```
4. appscale-tools is ready to use now!