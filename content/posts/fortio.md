---
title: "Install fortio on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "HTTP and gRPC load testing and visualization tool and server"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fortio on MacOS using homebrew

- App Name: fortio
- App description: HTTP and gRPC load testing and visualization tool and server
- App Version: 1.20.0
- App Website: https://fortio.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fortio with the following command
   ```
   brew install fortio
   ```
4. fortio is ready to use now!