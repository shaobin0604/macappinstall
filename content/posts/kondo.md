---
title: "Install kondo on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Save disk space by cleaning non-essential files from software projects"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install kondo on MacOS using homebrew

- App Name: kondo
- App description: Save disk space by cleaning non-essential files from software projects
- App Version: 0.5
- App Website: https://github.com/tbillington/kondo

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install kondo with the following command
   ```
   brew install kondo
   ```
4. kondo is ready to use now!