---
title: "Install Tandem on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Virtual office for remote teams"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Tandem on MacOS using homebrew

- App Name: Tandem
- App description: Virtual office for remote teams
- App Version: 2.1.1207
- App Website: https://tandem.chat/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Tandem with the following command
   ```
   brew install --cask tandem
   ```
4. Tandem is ready to use now!