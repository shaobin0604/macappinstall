---
title: "Install mecab-jumandic on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "See mecab"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mecab-jumandic on MacOS using homebrew

- App Name: mecab-jumandic
- App description: See mecab
- App Version: 7.0-20130310
- App Website: https://taku910.github.io/mecab/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mecab-jumandic with the following command
   ```
   brew install mecab-jumandic
   ```
4. mecab-jumandic is ready to use now!