---
title: "Install Lynxlet on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Launch Lynx in a Terminal window"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Lynxlet on MacOS using homebrew

- App Name: Lynxlet
- App description: Launch Lynx in a Terminal window
- App Version: 0.8.1
- App Website: https://habilis.net/lynxlet/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Lynxlet with the following command
   ```
   brew install --cask lynxlet
   ```
4. Lynxlet is ready to use now!