---
title: "Install TermHere on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Finder extension for opening a terminal from the current directory"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install TermHere on MacOS using homebrew

- App Name: TermHere
- App description: Finder extension for opening a terminal from the current directory
- App Version: 1.2.1
- App Website: https://hbang.ws/apps/termhere/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install TermHere with the following command
   ```
   brew install --cask termhere
   ```
4. TermHere is ready to use now!