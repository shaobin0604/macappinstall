---
title: "Install kalker on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Full-featured calculator with math syntax"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install kalker on MacOS using homebrew

- App Name: kalker
- App description: Full-featured calculator with math syntax
- App Version: 1.1.0
- App Website: https://kalker.strct.net

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install kalker with the following command
   ```
   brew install kalker
   ```
4. kalker is ready to use now!