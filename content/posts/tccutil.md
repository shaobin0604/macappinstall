---
title: "Install tccutil on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utility to modify the macOS Accessibility Database (TCC.db)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tccutil on MacOS using homebrew

- App Name: tccutil
- App description: Utility to modify the macOS Accessibility Database (TCC.db)
- App Version: 1.2.11
- App Website: https://github.com/jacobsalmela/tccutil

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tccutil with the following command
   ```
   brew install tccutil
   ```
4. tccutil is ready to use now!