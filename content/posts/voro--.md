---
title: "Install voro++ on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "3D Voronoi cell software library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install voro++ on MacOS using homebrew

- App Name: voro++
- App description: 3D Voronoi cell software library
- App Version: 0.4.6
- App Website: http://math.lbl.gov/voro++

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install voro++ with the following command
   ```
   brew install voro++
   ```
4. voro++ is ready to use now!