---
title: "Install aptly on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Swiss army knife for Debian repository management"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install aptly on MacOS using homebrew

- App Name: aptly
- App description: Swiss army knife for Debian repository management
- App Version: 1.4.0
- App Website: https://www.aptly.info/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install aptly with the following command
   ```
   brew install aptly
   ```
4. aptly is ready to use now!