---
title: "Install MPLab XC8 Compiler on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Compiler for 8-bit PIC and SAM MCUs and MPUs"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install MPLab XC8 Compiler on MacOS using homebrew

- App Name: MPLab XC8 Compiler
- App description: Compiler for 8-bit PIC and SAM MCUs and MPUs
- App Version: 2.32
- App Website: https://www.microchip.com/mplab/compilers

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install MPLab XC8 Compiler with the following command
   ```
   brew install --cask mplab-xc8
   ```
4. MPLab XC8 Compiler is ready to use now!