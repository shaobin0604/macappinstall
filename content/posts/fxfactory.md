---
title: "Install FxFactory on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Browse, install and purchase effects and plugins from a huge catalog"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install FxFactory on MacOS using homebrew

- App Name: FxFactory
- App description: Browse, install and purchase effects and plugins from a huge catalog
- App Version: 7.2.7,6974
- App Website: https://fxfactory.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install FxFactory with the following command
   ```
   brew install --cask fxfactory
   ```
4. FxFactory is ready to use now!