---
title: "Install ghdl on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "VHDL 2008/93/87 simulator"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ghdl on MacOS using homebrew

- App Name: ghdl
- App description: VHDL 2008/93/87 simulator
- App Version: latest
- App Website: https://github.com/ghdl/ghdl/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ghdl with the following command
   ```
   brew install --cask ghdl
   ```
4. ghdl is ready to use now!