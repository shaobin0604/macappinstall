---
title: "Install Tower on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Git client focusing on power and productivity"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Tower on MacOS using homebrew

- App Name: Tower
- App description: Git client focusing on power and productivity
- App Version: 8.0,308,199add6a
- App Website: https://www.git-tower.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Tower with the following command
   ```
   brew install --cask tower
   ```
4. Tower is ready to use now!