---
title: "Install gstreamer on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Development framework for multimedia applications"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gstreamer on MacOS using homebrew

- App Name: gstreamer
- App description: Development framework for multimedia applications
- App Version: 1.18.5
- App Website: https://gstreamer.freedesktop.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gstreamer with the following command
   ```
   brew install gstreamer
   ```
4. gstreamer is ready to use now!