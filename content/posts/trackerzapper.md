---
title: "Install trackerzapper on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Menubar app to remove link tracking parameters automatically"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install trackerzapper on MacOS using homebrew

- App Name: trackerzapper
- App description: Menubar app to remove link tracking parameters automatically
- App Version: 1.2.1
- App Website: https://rknight.me/apps/tracker-zapper

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install trackerzapper with the following command
   ```
   brew install --cask trackerzapper
   ```
4. trackerzapper is ready to use now!