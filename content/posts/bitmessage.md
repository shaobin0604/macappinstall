---
title: "Install Bitmessage on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "P2P communications protocol"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Bitmessage on MacOS using homebrew

- App Name: Bitmessage
- App description: P2P communications protocol
- App Version: 0.6.3.2
- App Website: https://bitmessage.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Bitmessage with the following command
   ```
   brew install --cask bitmessage
   ```
4. Bitmessage is ready to use now!