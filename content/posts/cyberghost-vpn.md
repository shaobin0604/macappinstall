---
title: "Install CyberGhost on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "VPN client"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install CyberGhost on MacOS using homebrew

- App Name: CyberGhost
- App description: VPN client
- App Version: 8.3.3,149
- App Website: https://www.cyberghostvpn.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install CyberGhost with the following command
   ```
   brew install --cask cyberghost-vpn
   ```
4. CyberGhost is ready to use now!