---
title: "Install cljstyle on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool for formatting Clojure code"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cljstyle on MacOS using homebrew

- App Name: cljstyle
- App description: Tool for formatting Clojure code
- App Version: 0.15.0
- App Website: https://github.com/greglook/cljstyle

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cljstyle with the following command
   ```
   brew install --cask cljstyle
   ```
4. cljstyle is ready to use now!