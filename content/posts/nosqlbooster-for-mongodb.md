---
title: "Install NoSQLBooster for MongoDB on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GUI tool and IDE for MongoDB"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install NoSQLBooster for MongoDB on MacOS using homebrew

- App Name: NoSQLBooster for MongoDB
- App description: GUI tool and IDE for MongoDB
- App Version: 7.0.11
- App Website: https://nosqlbooster.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install NoSQLBooster for MongoDB with the following command
   ```
   brew install --cask nosqlbooster-for-mongodb
   ```
4. NoSQLBooster for MongoDB is ready to use now!