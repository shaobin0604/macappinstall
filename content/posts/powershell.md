---
title: "Install PowerShell on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line shell and scripting language"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install PowerShell on MacOS using homebrew

- App Name: PowerShell
- App description: Command-line shell and scripting language
- App Version: 7.2.1
- App Website: https://github.com/PowerShell/PowerShell

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install PowerShell with the following command
   ```
   brew install --cask powershell
   ```
4. PowerShell is ready to use now!