---
title: "Install NBTExplorer on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Named Binary Tag (NBT) data editor for Minecraft"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install NBTExplorer on MacOS using homebrew

- App Name: NBTExplorer
- App description: Named Binary Tag (NBT) data editor for Minecraft
- App Version: 2.0.3
- App Website: https://www.minecraftforum.net/forums/mapping-and-modding-java-edition/minecraft-tools/1262665-nbtexplorer-nbt-editor-for-windows-and-mac

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install NBTExplorer with the following command
   ```
   brew install --cask nbtexplorer
   ```
4. NBTExplorer is ready to use now!