---
title: "Install wiremock-standalone on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Simulator for HTTP-based APIs"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install wiremock-standalone on MacOS using homebrew

- App Name: wiremock-standalone
- App description: Simulator for HTTP-based APIs
- App Version: 2.32.0
- App Website: http://wiremock.org/docs/running-standalone/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install wiremock-standalone with the following command
   ```
   brew install wiremock-standalone
   ```
4. wiremock-standalone is ready to use now!