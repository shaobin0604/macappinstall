---
title: "Install hypre on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library featuring parallel multigrid methods for grid problems"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install hypre on MacOS using homebrew

- App Name: hypre
- App description: Library featuring parallel multigrid methods for grid problems
- App Version: 2.24.0
- App Website: https://computing.llnl.gov/projects/hypre-scalable-linear-solvers-multigrid-methods

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install hypre with the following command
   ```
   brew install hypre
   ```
4. hypre is ready to use now!