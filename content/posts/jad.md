---
title: "Install Jad on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Java decompiler"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Jad on MacOS using homebrew

- App Name: Jad
- App description: Java decompiler
- App Version: 1.5.8g
- App Website: https://varaneckas.com/jad/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Jad with the following command
   ```
   brew install --cask jad
   ```
4. Jad is ready to use now!