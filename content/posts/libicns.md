---
title: "Install libicns on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library for manipulation of the macOS .icns resource format"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libicns on MacOS using homebrew

- App Name: libicns
- App description: Library for manipulation of the macOS .icns resource format
- App Version: 0.8.1
- App Website: https://icns.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libicns with the following command
   ```
   brew install libicns
   ```
4. libicns is ready to use now!