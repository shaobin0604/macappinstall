---
title: "Install Alfaview on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Audio video conferencing"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Alfaview on MacOS using homebrew

- App Name: Alfaview
- App description: Audio video conferencing
- App Version: 8.37.0
- App Website: https://alfaview.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Alfaview with the following command
   ```
   brew install --cask alfaview
   ```
4. Alfaview is ready to use now!