---
title: "Install gaul on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Genetic Algorithm Utility Library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gaul on MacOS using homebrew

- App Name: gaul
- App description: Genetic Algorithm Utility Library
- App Version: 0.1850-0
- App Website: https://gaul.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gaul with the following command
   ```
   brew install gaul
   ```
4. gaul is ready to use now!