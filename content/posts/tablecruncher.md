---
title: "Install Tablecruncher on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Lightweight CSV editor"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Tablecruncher on MacOS using homebrew

- App Name: Tablecruncher
- App description: Lightweight CSV editor
- App Version: 1.6.0.1
- App Website: https://tablecruncher.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Tablecruncher with the following command
   ```
   brew install --cask tablecruncher
   ```
4. Tablecruncher is ready to use now!