---
title: "Install wakeonlan on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Sends magic packets to wake up network-devices"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install wakeonlan on MacOS using homebrew

- App Name: wakeonlan
- App description: Sends magic packets to wake up network-devices
- App Version: 0.42
- App Website: https://github.com/jpoliv/wakeonlan

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install wakeonlan with the following command
   ```
   brew install wakeonlan
   ```
4. wakeonlan is ready to use now!