---
title: "Install lzip on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "LZMA-based compression program similar to gzip or bzip2"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install lzip on MacOS using homebrew

- App Name: lzip
- App description: LZMA-based compression program similar to gzip or bzip2
- App Version: 1.23
- App Website: https://www.nongnu.org/lzip/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install lzip with the following command
   ```
   brew install lzip
   ```
4. lzip is ready to use now!