---
title: "Install camellia on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Image Processing & Computer Vision library written in C"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install camellia on MacOS using homebrew

- App Name: camellia
- App description: Image Processing & Computer Vision library written in C
- App Version: 2.7.0
- App Website: https://camellia.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install camellia with the following command
   ```
   brew install camellia
   ```
4. camellia is ready to use now!