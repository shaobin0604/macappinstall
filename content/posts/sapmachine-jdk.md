---
title: "Install SapMachine OpenJDK Development Kit on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "OpenJDK distribution from SAP"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install SapMachine OpenJDK Development Kit on MacOS using homebrew

- App Name: SapMachine OpenJDK Development Kit
- App description: OpenJDK distribution from SAP
- App Version: 17.0.2
- App Website: https://sapmachine.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install SapMachine OpenJDK Development Kit with the following command
   ```
   brew install --cask sapmachine-jdk
   ```
4. SapMachine OpenJDK Development Kit is ready to use now!