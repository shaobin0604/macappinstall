---
title: "Install ghr on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Upload multiple artifacts to GitHub Release in parallel"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ghr on MacOS using homebrew

- App Name: ghr
- App description: Upload multiple artifacts to GitHub Release in parallel
- App Version: 0.14.0
- App Website: https://tcnksm.github.io/ghr

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ghr with the following command
   ```
   brew install ghr
   ```
4. ghr is ready to use now!