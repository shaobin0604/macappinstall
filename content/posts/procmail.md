---
title: "Install procmail on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Autonomous mail processor"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install procmail on MacOS using homebrew

- App Name: procmail
- App description: Autonomous mail processor
- App Version: 14
- App Website: https://web.archive.org/web/20151013184044/procmail.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install procmail with the following command
   ```
   brew install procmail
   ```
4. procmail is ready to use now!