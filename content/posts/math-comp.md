---
title: "Install math-comp on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Mathematical Components for the Coq proof assistant"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install math-comp on MacOS using homebrew

- App Name: math-comp
- App description: Mathematical Components for the Coq proof assistant
- App Version: 1.14.0
- App Website: https://math-comp.github.io/math-comp/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install math-comp with the following command
   ```
   brew install math-comp
   ```
4. math-comp is ready to use now!