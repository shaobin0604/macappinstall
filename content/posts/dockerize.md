---
title: "Install dockerize on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utility to simplify running applications in docker containers"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dockerize on MacOS using homebrew

- App Name: dockerize
- App description: Utility to simplify running applications in docker containers
- App Version: 0.6.1
- App Website: https://github.com/jwilder/dockerize

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dockerize with the following command
   ```
   brew install dockerize
   ```
4. dockerize is ready to use now!