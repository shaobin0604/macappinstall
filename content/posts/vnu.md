---
title: "Install vnu on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Nu Markup Checker: command-line and server HTML validator"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install vnu on MacOS using homebrew

- App Name: vnu
- App description: Nu Markup Checker: command-line and server HTML validator
- App Version: 20.6.30
- App Website: https://validator.github.io/validator/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install vnu with the following command
   ```
   brew install vnu
   ```
4. vnu is ready to use now!