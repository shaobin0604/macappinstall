---
title: "Install mpw on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Stateless/deterministic password and identity manager"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mpw on MacOS using homebrew

- App Name: mpw
- App description: Stateless/deterministic password and identity manager
- App Version: 2.7-cli-1
- App Website: https://masterpassword.app/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mpw with the following command
   ```
   brew install mpw
   ```
4. mpw is ready to use now!