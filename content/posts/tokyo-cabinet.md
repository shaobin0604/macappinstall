---
title: "Install tokyo-cabinet on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Lightweight database library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tokyo-cabinet on MacOS using homebrew

- App Name: tokyo-cabinet
- App description: Lightweight database library
- App Version: 1.4.48
- App Website: https://dbmx.net/tokyocabinet/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tokyo-cabinet with the following command
   ```
   brew install tokyo-cabinet
   ```
4. tokyo-cabinet is ready to use now!