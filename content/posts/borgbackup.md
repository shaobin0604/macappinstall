---
title: "Install borgbackup on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Deduplicating archiver with compression and authenticated encryption"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install borgbackup on MacOS using homebrew

- App Name: borgbackup
- App description: Deduplicating archiver with compression and authenticated encryption
- App Version: 1.1.17
- App Website: https://borgbackup.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install borgbackup with the following command
   ```
   brew install borgbackup
   ```
4. borgbackup is ready to use now!