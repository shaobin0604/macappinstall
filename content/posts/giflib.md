---
title: "Install giflib on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library and utilities for processing GIFs"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install giflib on MacOS using homebrew

- App Name: giflib
- App description: Library and utilities for processing GIFs
- App Version: 5.2.1
- App Website: https://giflib.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install giflib with the following command
   ```
   brew install giflib
   ```
4. giflib is ready to use now!