---
title: "Install autopsy on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Graphical interface to Sleuth Kit investigation tools"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install autopsy on MacOS using homebrew

- App Name: autopsy
- App description: Graphical interface to Sleuth Kit investigation tools
- App Version: 2.24
- App Website: https://www.sleuthkit.org/autopsy/index.php

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install autopsy with the following command
   ```
   brew install autopsy
   ```
4. autopsy is ready to use now!