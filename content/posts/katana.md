---
title: "Install Katana on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Open-source screenshot utility"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Katana on MacOS using homebrew

- App Name: Katana
- App description: Open-source screenshot utility
- App Version: 1.4.4
- App Website: https://github.com/bluegill/katana/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Katana with the following command
   ```
   brew install --cask katana
   ```
4. Katana is ready to use now!