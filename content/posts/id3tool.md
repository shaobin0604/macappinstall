---
title: "Install id3tool on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "ID3 editing tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install id3tool on MacOS using homebrew

- App Name: id3tool
- App description: ID3 editing tool
- App Version: 1.2a
- App Website: http://nekohako.xware.cx/id3tool/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install id3tool with the following command
   ```
   brew install id3tool
   ```
4. id3tool is ready to use now!