---
title: "Install nodeenv on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Node.js virtual environment builder"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install nodeenv on MacOS using homebrew

- App Name: nodeenv
- App description: Node.js virtual environment builder
- App Version: 1.6.0
- App Website: https://github.com/ekalinin/nodeenv

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install nodeenv with the following command
   ```
   brew install nodeenv
   ```
4. nodeenv is ready to use now!