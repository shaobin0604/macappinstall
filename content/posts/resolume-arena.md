---
title: "Install Resolume Arena on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Video mapping software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Resolume Arena on MacOS using homebrew

- App Name: Resolume Arena
- App description: Video mapping software
- App Version: 7.9.0,11617
- App Website: https://resolume.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Resolume Arena with the following command
   ```
   brew install --cask resolume-arena
   ```
4. Resolume Arena is ready to use now!