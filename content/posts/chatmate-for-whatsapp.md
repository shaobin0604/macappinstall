---
title: "Install ChatMate for WhatsApp on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Extension app WhatsApp"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ChatMate for WhatsApp on MacOS using homebrew

- App Name: ChatMate for WhatsApp
- App description: Extension app WhatsApp
- App Version: 4.3.1,482,1537891987
- App Website: https://chatmate.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ChatMate for WhatsApp with the following command
   ```
   brew install --cask chatmate-for-whatsapp
   ```
4. ChatMate for WhatsApp is ready to use now!