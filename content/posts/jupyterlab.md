---
title: "Install jupyterlab on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Interactive environments for writing and running code"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install jupyterlab on MacOS using homebrew

- App Name: jupyterlab
- App description: Interactive environments for writing and running code
- App Version: 3.2.9
- App Website: https://jupyter.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install jupyterlab with the following command
   ```
   brew install jupyterlab
   ```
4. jupyterlab is ready to use now!