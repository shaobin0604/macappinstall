---
title: "Install ximalaya on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Platform for podcasting and audio-sharing"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ximalaya on MacOS using homebrew

- App Name: ximalaya
- App description: Platform for podcasting and audio-sharing
- App Version: 2.4.0,1642348063
- App Website: https://www.ximalaya.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ximalaya with the following command
   ```
   brew install --cask ximalaya
   ```
4. ximalaya is ready to use now!