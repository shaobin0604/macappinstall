---
title: "Install apache-archiva on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Build Artifact Repository Manager"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install apache-archiva on MacOS using homebrew

- App Name: apache-archiva
- App description: Build Artifact Repository Manager
- App Version: 2.2.7
- App Website: https://archiva.apache.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install apache-archiva with the following command
   ```
   brew install apache-archiva
   ```
4. apache-archiva is ready to use now!