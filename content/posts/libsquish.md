---
title: "Install libsquish on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library for compressing images with the DXT standard"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libsquish on MacOS using homebrew

- App Name: libsquish
- App description: Library for compressing images with the DXT standard
- App Version: 1.15
- App Website: https://sourceforge.net/projects/libsquish/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libsquish with the following command
   ```
   brew install libsquish
   ```
4. libsquish is ready to use now!