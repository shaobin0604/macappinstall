---
title: "Install pdf2json on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "PDF to JSON and XML converter"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pdf2json on MacOS using homebrew

- App Name: pdf2json
- App description: PDF to JSON and XML converter
- App Version: 0.71
- App Website: https://github.com/flexpaper/pdf2json

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pdf2json with the following command
   ```
   brew install pdf2json
   ```
4. pdf2json is ready to use now!