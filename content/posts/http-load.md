---
title: "Install http_load on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Test throughput of a web server by running parallel fetches"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install http_load on MacOS using homebrew

- App Name: http_load
- App description: Test throughput of a web server by running parallel fetches
- App Version: 20160309
- App Website: https://www.acme.com/software/http_load/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install http_load with the following command
   ```
   brew install http_load
   ```
4. http_load is ready to use now!