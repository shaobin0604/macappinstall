---
title: "Install CLIPS IDE on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool for building expert systems"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install CLIPS IDE on MacOS using homebrew

- App Name: CLIPS IDE
- App description: Tool for building expert systems
- App Version: 6.40
- App Website: http://www.clipsrules.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install CLIPS IDE with the following command
   ```
   brew install --cask clips-ide
   ```
4. CLIPS IDE is ready to use now!