---
title: "Install odrive on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to make any cloud storage unified, synchronized, shareable, and encrypted"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install odrive on MacOS using homebrew

- App Name: odrive
- App description: Tool to make any cloud storage unified, synchronized, shareable, and encrypted
- App Version: 7201
- App Website: https://www.odrive.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install odrive with the following command
   ```
   brew install --cask odrive
   ```
4. odrive is ready to use now!