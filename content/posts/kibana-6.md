---
title: "Install kibana@6 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Analytics and search dashboard for Elasticsearch"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install kibana@6 on MacOS using homebrew

- App Name: kibana@6
- App description: Analytics and search dashboard for Elasticsearch
- App Version: 6.8.20
- App Website: https://www.elastic.co/products/kibana

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install kibana@6 with the following command
   ```
   brew install kibana@6
   ```
4. kibana@6 is ready to use now!