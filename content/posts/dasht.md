---
title: "Install dasht on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Search API docs offline, in your terminal or browser"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dasht on MacOS using homebrew

- App Name: dasht
- App description: Search API docs offline, in your terminal or browser
- App Version: 2.4.0
- App Website: https://sunaku.github.io/dasht

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dasht with the following command
   ```
   brew install dasht
   ```
4. dasht is ready to use now!