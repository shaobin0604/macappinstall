---
title: "Install freeciv on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Free and Open Source empire-building strategy game"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install freeciv on MacOS using homebrew

- App Name: freeciv
- App description: Free and Open Source empire-building strategy game
- App Version: 2.6.6
- App Website: http://freeciv.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install freeciv with the following command
   ```
   brew install freeciv
   ```
4. freeciv is ready to use now!