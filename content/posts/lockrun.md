---
title: "Install lockrun on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Run cron jobs with overrun protection"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install lockrun on MacOS using homebrew

- App Name: lockrun
- App description: Run cron jobs with overrun protection
- App Version: 1.1.3
- App Website: http://unixwiz.net/tools/lockrun.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install lockrun with the following command
   ```
   brew install lockrun
   ```
4. lockrun is ready to use now!