---
title: "Install genext2fs on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Generates an ext2 filesystem as a normal (non-root) user"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install genext2fs on MacOS using homebrew

- App Name: genext2fs
- App description: Generates an ext2 filesystem as a normal (non-root) user
- App Version: 1.5.0
- App Website: https://genext2fs.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install genext2fs with the following command
   ```
   brew install genext2fs
   ```
4. genext2fs is ready to use now!