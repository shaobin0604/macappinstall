---
title: "Install abcm2ps on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "ABC music notation software"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install abcm2ps on MacOS using homebrew

- App Name: abcm2ps
- App description: ABC music notation software
- App Version: 8.14.13
- App Website: http://moinejf.free.fr

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install abcm2ps with the following command
   ```
   brew install abcm2ps
   ```
4. abcm2ps is ready to use now!