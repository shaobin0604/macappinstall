---
title: "Install threadweaver on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Helper for multithreaded programming"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install threadweaver on MacOS using homebrew

- App Name: threadweaver
- App description: Helper for multithreaded programming
- App Version: 5.91.0
- App Website: https://api.kde.org/frameworks/threadweaver/html/index.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install threadweaver with the following command
   ```
   brew install threadweaver
   ```
4. threadweaver is ready to use now!