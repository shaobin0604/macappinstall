---
title: "Install Mellow on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Rule-based global transparent proxy client"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Mellow on MacOS using homebrew

- App Name: Mellow
- App description: Rule-based global transparent proxy client
- App Version: 0.1.22
- App Website: https://github.com/mellow-io/mellow

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Mellow with the following command
   ```
   brew install --cask mellow
   ```
4. Mellow is ready to use now!