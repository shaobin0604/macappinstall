---
title: "Install haruhi-dl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Fork of youtube-dl, focused on bringing a fast, steady stream of updates"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install haruhi-dl on MacOS using homebrew

- App Name: haruhi-dl
- App description: Fork of youtube-dl, focused on bringing a fast, steady stream of updates
- App Version: 2021.8.1
- App Website: https://git.sakamoto.pl/laudom/haruhi-dl

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install haruhi-dl with the following command
   ```
   brew install haruhi-dl
   ```
4. haruhi-dl is ready to use now!