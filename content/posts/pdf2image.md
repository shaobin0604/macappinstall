---
title: "Install pdf2image on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Convert PDFs to images"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pdf2image on MacOS using homebrew

- App Name: pdf2image
- App description: Convert PDFs to images
- App Version: 0.53
- App Website: https://code.google.com/p/pdf2image/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pdf2image with the following command
   ```
   brew install pdf2image
   ```
4. pdf2image is ready to use now!