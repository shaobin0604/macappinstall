---
title: "Install Google Web Designer on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Create interactive HTML5-based designs and motion graphics"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Google Web Designer on MacOS using homebrew

- App Name: Google Web Designer
- App description: Create interactive HTML5-based designs and motion graphics
- App Version: 9.0.8.0
- App Website: https://www.google.com/webdesigner/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Google Web Designer with the following command
   ```
   brew install --cask google-web-designer
   ```
4. Google Web Designer is ready to use now!