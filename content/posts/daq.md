---
title: "Install daq on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Network intrusion prevention and detection system"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install daq on MacOS using homebrew

- App Name: daq
- App description: Network intrusion prevention and detection system
- App Version: 3.0.6
- App Website: https://www.snort.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install daq with the following command
   ```
   brew install daq
   ```
4. daq is ready to use now!