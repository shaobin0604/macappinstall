---
title: "Install cd-discid on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Read CD and get CDDB discid information"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cd-discid on MacOS using homebrew

- App Name: cd-discid
- App description: Read CD and get CDDB discid information
- App Version: 1.4
- App Website: https://linukz.org/cd-discid.shtml

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cd-discid with the following command
   ```
   brew install cd-discid
   ```
4. cd-discid is ready to use now!