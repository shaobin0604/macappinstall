---
title: "Install libdrawtext on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library for anti-aliased text rendering in OpenGL"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libdrawtext on MacOS using homebrew

- App Name: libdrawtext
- App description: Library for anti-aliased text rendering in OpenGL
- App Version: 0.5
- App Website: http://nuclear.mutantstargoat.com/sw/libdrawtext/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libdrawtext with the following command
   ```
   brew install libdrawtext
   ```
4. libdrawtext is ready to use now!