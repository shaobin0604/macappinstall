---
title: "Install bitrise on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line automation tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bitrise on MacOS using homebrew

- App Name: bitrise
- App description: Command-line automation tool
- App Version: 1.48.0
- App Website: https://github.com/bitrise-io/bitrise

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bitrise with the following command
   ```
   brew install bitrise
   ```
4. bitrise is ready to use now!