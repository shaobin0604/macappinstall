---
title: "Install cliclick on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool for emulating mouse and keyboard events"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cliclick on MacOS using homebrew

- App Name: cliclick
- App description: Tool for emulating mouse and keyboard events
- App Version: 5.0.1
- App Website: https://www.bluem.net/jump/cliclick/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cliclick with the following command
   ```
   brew install cliclick
   ```
4. cliclick is ready to use now!