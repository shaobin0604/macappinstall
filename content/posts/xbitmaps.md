---
title: "Install xbitmaps on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Bitmap images used by multiple X11 applications"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install xbitmaps on MacOS using homebrew

- App Name: xbitmaps
- App description: Bitmap images used by multiple X11 applications
- App Version: 1.1.2
- App Website: https://xcb.freedesktop.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install xbitmaps with the following command
   ```
   brew install xbitmaps
   ```
4. xbitmaps is ready to use now!