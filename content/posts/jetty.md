---
title: "Install jetty on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Java servlet engine and webserver"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install jetty on MacOS using homebrew

- App Name: jetty
- App description: Java servlet engine and webserver
- App Version: 9.4.45.v20220203
- App Website: https://www.eclipse.org/jetty/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install jetty with the following command
   ```
   brew install jetty
   ```
4. jetty is ready to use now!