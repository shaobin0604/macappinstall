---
title: "Install 4K Video to MP3 on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Convert any video to MP3"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install 4K Video to MP3 on MacOS using homebrew

- App Name: 4K Video to MP3
- App description: Convert any video to MP3
- App Version: 3.0.1
- App Website: https://www.4kdownload.com/products/product-videotomp3

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install 4K Video to MP3 with the following command
   ```
   brew install --cask 4k-video-to-mp3
   ```
4. 4K Video to MP3 is ready to use now!