---
title: "Install Sensei on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Monitors the computer system and optimizes its performance"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Sensei on MacOS using homebrew

- App Name: Sensei
- App description: Monitors the computer system and optimizes its performance
- App Version: 1.5.2,100
- App Website: https://sensei.app/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Sensei with the following command
   ```
   brew install --cask sensei
   ```
4. Sensei is ready to use now!