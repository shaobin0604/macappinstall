---
title: "Install Black Ink on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Download, solve, and print crossword puzzles"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Black Ink on MacOS using homebrew

- App Name: Black Ink
- App description: Download, solve, and print crossword puzzles
- App Version: 2.1.9,2788
- App Website: https://redsweater.com/blackink/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Black Ink with the following command
   ```
   brew install --cask black-ink
   ```
4. Black Ink is ready to use now!