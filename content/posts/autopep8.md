---
title: "Install autopep8 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Automatically formats Python code to conform to the PEP 8 style guide"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install autopep8 on MacOS using homebrew

- App Name: autopep8
- App description: Automatically formats Python code to conform to the PEP 8 style guide
- App Version: 1.6.0
- App Website: https://github.com/hhatto/autopep8

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install autopep8 with the following command
   ```
   brew install autopep8
   ```
4. autopep8 is ready to use now!