---
title: "Install ksnip on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Screenshot and annotation tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ksnip on MacOS using homebrew

- App Name: ksnip
- App description: Screenshot and annotation tool
- App Version: 1.9.2
- App Website: https://github.com/ksnip/ksnip

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ksnip with the following command
   ```
   brew install --cask ksnip
   ```
4. ksnip is ready to use now!