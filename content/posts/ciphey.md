---
title: "Install ciphey on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Automatic decryption and decoding tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ciphey on MacOS using homebrew

- App Name: ciphey
- App description: Automatic decryption and decoding tool
- App Version: 5.14.0
- App Website: https://github.com/Ciphey/Ciphey

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ciphey with the following command
   ```
   brew install ciphey
   ```
4. ciphey is ready to use now!