---
title: "Install SlidePilot on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "PDF presentation tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install SlidePilot on MacOS using homebrew

- App Name: SlidePilot
- App description: PDF presentation tool
- App Version: 1.7.0
- App Website: https://slidepilotapp.com/en

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install SlidePilot with the following command
   ```
   brew install --cask slidepilot
   ```
4. SlidePilot is ready to use now!