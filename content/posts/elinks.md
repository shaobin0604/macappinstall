---
title: "Install elinks on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Text mode web browser"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install elinks on MacOS using homebrew

- App Name: elinks
- App description: Text mode web browser
- App Version: 0.11.7
- App Website: http://elinks.or.cz/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install elinks with the following command
   ```
   brew install elinks
   ```
4. elinks is ready to use now!