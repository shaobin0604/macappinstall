---
title: "Install apache-spark on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Engine for large-scale data processing"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install apache-spark on MacOS using homebrew

- App Name: apache-spark
- App description: Engine for large-scale data processing
- App Version: 3.2.1
- App Website: https://spark.apache.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install apache-spark with the following command
   ```
   brew install apache-spark
   ```
4. apache-spark is ready to use now!