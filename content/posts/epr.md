---
title: "Install epr on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line EPUB reader"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install epr on MacOS using homebrew

- App Name: epr
- App description: Command-line EPUB reader
- App Version: 2.4.13
- App Website: https://github.com/wustho/epr

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install epr with the following command
   ```
   brew install epr
   ```
4. epr is ready to use now!