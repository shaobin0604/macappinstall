---
title: "Install python-yq on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line YAML and XML processor that wraps jq"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install python-yq on MacOS using homebrew

- App Name: python-yq
- App description: Command-line YAML and XML processor that wraps jq
- App Version: 2.13.0
- App Website: https://kislyuk.github.io/yq/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install python-yq with the following command
   ```
   brew install python-yq
   ```
4. python-yq is ready to use now!