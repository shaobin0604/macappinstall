---
title: "Install idris on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Pure functional programming language with dependent types"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install idris on MacOS using homebrew

- App Name: idris
- App description: Pure functional programming language with dependent types
- App Version: 1.3.4
- App Website: https://www.idris-lang.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install idris with the following command
   ```
   brew install idris
   ```
4. idris is ready to use now!