---
title: "Install wapm on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "WebAssembly Package Manager (CLI)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install wapm on MacOS using homebrew

- App Name: wapm
- App description: WebAssembly Package Manager (CLI)
- App Version: 0.5.1
- App Website: https://wapm.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install wapm with the following command
   ```
   brew install wapm
   ```
4. wapm is ready to use now!