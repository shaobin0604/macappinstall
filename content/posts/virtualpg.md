---
title: "Install virtualpg on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Loadable dynamic extension for SQLite and SpatiaLite"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install virtualpg on MacOS using homebrew

- App Name: virtualpg
- App description: Loadable dynamic extension for SQLite and SpatiaLite
- App Version: 2.0.1
- App Website: https://www.gaia-gis.it/fossil/virtualpg/index

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install virtualpg with the following command
   ```
   brew install virtualpg
   ```
4. virtualpg is ready to use now!