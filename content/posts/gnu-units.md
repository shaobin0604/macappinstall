---
title: "Install gnu-units on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GNU unit conversion tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gnu-units on MacOS using homebrew

- App Name: gnu-units
- App description: GNU unit conversion tool
- App Version: 2.21
- App Website: https://www.gnu.org/software/units/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gnu-units with the following command
   ```
   brew install gnu-units
   ```
4. gnu-units is ready to use now!