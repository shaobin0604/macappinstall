---
title: "Install ipmiutil on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "IPMI server management utility"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ipmiutil on MacOS using homebrew

- App Name: ipmiutil
- App description: IPMI server management utility
- App Version: 3.1.8
- App Website: https://ipmiutil.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ipmiutil with the following command
   ```
   brew install ipmiutil
   ```
4. ipmiutil is ready to use now!