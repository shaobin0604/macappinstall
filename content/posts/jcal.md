---
title: "Install jcal on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "UNIX-cal-like tool to display Jalali calendar"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install jcal on MacOS using homebrew

- App Name: jcal
- App description: UNIX-cal-like tool to display Jalali calendar
- App Version: 0.4.1
- App Website: https://savannah.nongnu.org/projects/jcal/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install jcal with the following command
   ```
   brew install jcal
   ```
4. jcal is ready to use now!