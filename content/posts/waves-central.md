---
title: "Install Waves Central on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Client to install and activate Waves products"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Waves Central on MacOS using homebrew

- App Name: Waves Central
- App description: Client to install and activate Waves products
- App Version: 13.0.12
- App Website: https://www.waves.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Waves Central with the following command
   ```
   brew install --cask waves-central
   ```
4. Waves Central is ready to use now!