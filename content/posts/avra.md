---
title: "Install avra on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Assembler for the Atmel AVR microcontroller family"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install avra on MacOS using homebrew

- App Name: avra
- App description: Assembler for the Atmel AVR microcontroller family
- App Version: 1.4.2
- App Website: https://github.com/hsoft/avra

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install avra with the following command
   ```
   brew install avra
   ```
4. avra is ready to use now!