---
title: "Install 硕鼠MAC on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install 硕鼠MAC on MacOS using homebrew

- App Name: 硕鼠MAC
- App description: null
- App Version: 0.5.2.7
- App Website: https://www.flvcd.com/index.htm

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install 硕鼠MAC with the following command
   ```
   brew install --cask flvcd-bigrats
   ```
4. 硕鼠MAC is ready to use now!