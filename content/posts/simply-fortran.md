---
title: "Install Simply Fortran on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Fortran development environment"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Simply Fortran on MacOS using homebrew

- App Name: Simply Fortran
- App description: Fortran development environment
- App Version: 3.15.3384
- App Website: https://simplyfortran.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Simply Fortran with the following command
   ```
   brew install --cask simply-fortran
   ```
4. Simply Fortran is ready to use now!