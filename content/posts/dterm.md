---
title: "Install dterm on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Terminal emulator for use with xterm and friends"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dterm on MacOS using homebrew

- App Name: dterm
- App description: Terminal emulator for use with xterm and friends
- App Version: 0.5
- App Website: http://www.knossos.net.nz/resources/free-software/dterm/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dterm with the following command
   ```
   brew install dterm
   ```
4. dterm is ready to use now!