---
title: "Install kahip on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Karlsruhe High Quality Partitioning"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install kahip on MacOS using homebrew

- App Name: kahip
- App description: Karlsruhe High Quality Partitioning
- App Version: 3.14
- App Website: https://algo2.iti.kit.edu/documents/kahip/index.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install kahip with the following command
   ```
   brew install kahip
   ```
4. kahip is ready to use now!