---
title: "Install MPEG Streamclip on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Video converter, player, and editor"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install MPEG Streamclip on MacOS using homebrew

- App Name: MPEG Streamclip
- App description: Video converter, player, and editor
- App Version: 1.9.2
- App Website: http://www.squared5.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install MPEG Streamclip with the following command
   ```
   brew install --cask mpeg-streamclip
   ```
4. MPEG Streamclip is ready to use now!