---
title: "Install TortoiseHg on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tools for the Mercurial distributed revision control system"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install TortoiseHg on MacOS using homebrew

- App Name: TortoiseHg
- App description: Tools for the Mercurial distributed revision control system
- App Version: 6.0
- App Website: https://tortoisehg.bitbucket.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install TortoiseHg with the following command
   ```
   brew install --cask tortoisehg
   ```
4. TortoiseHg is ready to use now!