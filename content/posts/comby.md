---
title: "Install comby on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool for changing code across many languages"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install comby on MacOS using homebrew

- App Name: comby
- App description: Tool for changing code across many languages
- App Version: 1.7.0
- App Website: https://comby.dev

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install comby with the following command
   ```
   brew install comby
   ```
4. comby is ready to use now!