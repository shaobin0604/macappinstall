---
title: "Install Invoker on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utility for managing Laravel applications"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Invoker on MacOS using homebrew

- App Name: Invoker
- App description: Utility for managing Laravel applications
- App Version: 2.8.0
- App Website: https://invoker.dev/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Invoker with the following command
   ```
   brew install --cask invoker
   ```
4. Invoker is ready to use now!