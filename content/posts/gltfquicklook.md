---
title: "Install GLTFQuickLook on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "QuickLook plugin for glTF files"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install GLTFQuickLook on MacOS using homebrew

- App Name: GLTFQuickLook
- App description: QuickLook plugin for glTF files
- App Version: 0.3.0
- App Website: https://github.com/magicien/GLTFQuickLook

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install GLTFQuickLook with the following command
   ```
   brew install --cask gltfquicklook
   ```
4. GLTFQuickLook is ready to use now!