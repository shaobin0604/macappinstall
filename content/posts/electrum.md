---
title: "Install Electrum on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Bitcoin thin client"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Electrum on MacOS using homebrew

- App Name: Electrum
- App description: Bitcoin thin client
- App Version: 4.1.5
- App Website: https://electrum.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Electrum with the following command
   ```
   brew install --cask electrum
   ```
4. Electrum is ready to use now!