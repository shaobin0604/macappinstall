---
title: "Install emmetapp on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tiling and stacking window manager and window resizing tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install emmetapp on MacOS using homebrew

- App Name: emmetapp
- App description: Tiling and stacking window manager and window resizing tool
- App Version: 1.0.0
- App Website: https://emmetapp.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install emmetapp with the following command
   ```
   brew install --cask emmetapp
   ```
4. emmetapp is ready to use now!