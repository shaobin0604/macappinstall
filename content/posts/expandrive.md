---
title: "Install ExpanDrive on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Network drive and browser for cloud storage"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ExpanDrive on MacOS using homebrew

- App Name: ExpanDrive
- App description: Network drive and browser for cloud storage
- App Version: 7,2021.8.3
- App Website: https://www.expandrive.com/apps/expandrive/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ExpanDrive with the following command
   ```
   brew install --cask expandrive
   ```
4. ExpanDrive is ready to use now!