---
title: "Install git-flow-avh on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "AVH edition of git-flow"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install git-flow-avh on MacOS using homebrew

- App Name: git-flow-avh
- App description: AVH edition of git-flow
- App Version: 1.12.3
- App Website: https://github.com/petervanderdoes/gitflow-avh

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install git-flow-avh with the following command
   ```
   brew install git-flow-avh
   ```
4. git-flow-avh is ready to use now!