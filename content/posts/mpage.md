---
title: "Install mpage on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Many to one page printing utility"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mpage on MacOS using homebrew

- App Name: mpage
- App description: Many to one page printing utility
- App Version: 2.5.7
- App Website: https://mesa.nl/pub/mpage/README

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mpage with the following command
   ```
   brew install mpage
   ```
4. mpage is ready to use now!