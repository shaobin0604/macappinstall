---
title: "Install mBlock on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Coding tool designed for teaching STEAM"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mBlock on MacOS using homebrew

- App Name: mBlock
- App description: Coding tool designed for teaching STEAM
- App Version: 5.4.0
- App Website: https://www.mblock.cc/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mBlock with the following command
   ```
   brew install --cask mblock
   ```
4. mBlock is ready to use now!