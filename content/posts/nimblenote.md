---
title: "Install nimblenote on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Keyboard-driven note taking"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install nimblenote on MacOS using homebrew

- App Name: nimblenote
- App description: Keyboard-driven note taking
- App Version: 3.2.1
- App Website: https://nimblenote.app/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install nimblenote with the following command
   ```
   brew install --cask nimblenote
   ```
4. nimblenote is ready to use now!