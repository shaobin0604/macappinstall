---
title: "Install sslh on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Forward connections based on first data packet sent by client"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sslh on MacOS using homebrew

- App Name: sslh
- App description: Forward connections based on first data packet sent by client
- App Version: 1.22c
- App Website: https://www.rutschle.net/tech/sslh.shtml

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sslh with the following command
   ```
   brew install sslh
   ```
4. sslh is ready to use now!