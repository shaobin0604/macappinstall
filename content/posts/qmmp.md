---
title: "Install qmmp on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Qt-based Multimedia Player"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install qmmp on MacOS using homebrew

- App Name: qmmp
- App description: Qt-based Multimedia Player
- App Version: 2.0.3
- App Website: https://qmmp.ylsoftware.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install qmmp with the following command
   ```
   brew install qmmp
   ```
4. qmmp is ready to use now!