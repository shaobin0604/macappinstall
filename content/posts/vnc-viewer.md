---
title: "Install Real VNC Viewer on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Remote desktop application focusing on security"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Real VNC Viewer on MacOS using homebrew

- App Name: Real VNC Viewer
- App description: Remote desktop application focusing on security
- App Version: 6.21.1109
- App Website: https://www.realvnc.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Real VNC Viewer with the following command
   ```
   brew install --cask vnc-viewer
   ```
4. Real VNC Viewer is ready to use now!