---
title: "Install vbindiff on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Visual Binary Diff"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install vbindiff on MacOS using homebrew

- App Name: vbindiff
- App description: Visual Binary Diff
- App Version: 3.0_beta5
- App Website: https://www.cjmweb.net/vbindiff/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install vbindiff with the following command
   ```
   brew install vbindiff
   ```
4. vbindiff is ready to use now!