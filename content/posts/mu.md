---
title: "Install mu on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool for searching e-mail messages stored in the maildir-format"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mu on MacOS using homebrew

- App Name: mu
- App description: Tool for searching e-mail messages stored in the maildir-format
- App Version: 1.6.10
- App Website: https://www.djcbsoftware.nl/code/mu/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mu with the following command
   ```
   brew install mu
   ```
4. mu is ready to use now!