---
title: "Install jsonlint on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "JSON parser and validator with a CLI"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install jsonlint on MacOS using homebrew

- App Name: jsonlint
- App description: JSON parser and validator with a CLI
- App Version: 1.6.0
- App Website: https://github.com/zaach/jsonlint

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install jsonlint with the following command
   ```
   brew install jsonlint
   ```
4. jsonlint is ready to use now!