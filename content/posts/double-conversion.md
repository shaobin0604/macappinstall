---
title: "Install double-conversion on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Binary-decimal and decimal-binary routines for IEEE doubles"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install double-conversion on MacOS using homebrew

- App Name: double-conversion
- App description: Binary-decimal and decimal-binary routines for IEEE doubles
- App Version: 3.2.0
- App Website: https://github.com/google/double-conversion

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install double-conversion with the following command
   ```
   brew install double-conversion
   ```
4. double-conversion is ready to use now!