---
title: "Install NetEase cloud music on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Music streaming platform"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install NetEase cloud music on MacOS using homebrew

- App Name: NetEase cloud music
- App description: Music streaming platform
- App Version: 2.3.8.872
- App Website: https://music.163.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install NetEase cloud music with the following command
   ```
   brew install --cask neteasemusic
   ```
4. NetEase cloud music is ready to use now!