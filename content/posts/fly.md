---
title: "Install fly on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Official CLI tool for Concourse CI"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fly on MacOS using homebrew

- App Name: fly
- App description: Official CLI tool for Concourse CI
- App Version: 7.6.0
- App Website: https://github.com/concourse/concourse

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fly with the following command
   ```
   brew install --cask fly
   ```
4. fly is ready to use now!