---
title: "Install fwknop on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Single Packet Authorization and Port Knocking"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fwknop on MacOS using homebrew

- App Name: fwknop
- App description: Single Packet Authorization and Port Knocking
- App Version: 2.6.10
- App Website: https://www.cipherdyne.org/fwknop/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fwknop with the following command
   ```
   brew install fwknop
   ```
4. fwknop is ready to use now!