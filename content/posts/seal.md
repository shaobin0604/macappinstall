---
title: "Install seal on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Easy-to-use homomorphic encryption library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install seal on MacOS using homebrew

- App Name: seal
- App description: Easy-to-use homomorphic encryption library
- App Version: 3.7.2
- App Website: https://github.com/microsoft/SEAL

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install seal with the following command
   ```
   brew install seal
   ```
4. seal is ready to use now!