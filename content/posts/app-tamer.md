---
title: "Install AppTamer on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CPU management applicataion"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install AppTamer on MacOS using homebrew

- App Name: AppTamer
- App description: CPU management applicataion
- App Version: 2.6.5,10755
- App Website: https://www.stclairsoft.com/AppTamer/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install AppTamer with the following command
   ```
   brew install --cask app-tamer
   ```
4. AppTamer is ready to use now!