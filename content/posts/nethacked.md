---
title: "Install nethacked on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Bugfixed and interface-patched Nethack"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install nethacked on MacOS using homebrew

- App Name: nethacked
- App description: Bugfixed and interface-patched Nethack
- App Version: 1.0
- App Website: https://nethacked.github.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install nethacked with the following command
   ```
   brew install nethacked
   ```
4. nethacked is ready to use now!