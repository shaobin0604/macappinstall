---
title: "Install BetterZip on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utility to create and modify archives"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install BetterZip on MacOS using homebrew

- App Name: BetterZip
- App description: Utility to create and modify archives
- App Version: 5.2
- App Website: https://macitbetter.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install BetterZip with the following command
   ```
   brew install --cask betterzip
   ```
4. BetterZip is ready to use now!