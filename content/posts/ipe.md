---
title: "Install Ipe on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Drawing editor for creating figures in PDF format"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Ipe on MacOS using homebrew

- App Name: Ipe
- App description: Drawing editor for creating figures in PDF format
- App Version: 7.2.24
- App Website: https://ipe.otfried.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Ipe with the following command
   ```
   brew install --cask ipe
   ```
4. Ipe is ready to use now!