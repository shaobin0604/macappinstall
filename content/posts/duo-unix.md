---
title: "Install duo_unix on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Two-factor authentication for SSH"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install duo_unix on MacOS using homebrew

- App Name: duo_unix
- App description: Two-factor authentication for SSH
- App Version: 1.11.5
- App Website: https://www.duosecurity.com/docs/duounix

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install duo_unix with the following command
   ```
   brew install duo_unix
   ```
4. duo_unix is ready to use now!