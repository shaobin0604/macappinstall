---
title: "Install JetBrains Projector on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Common and client-related code for running Swing applications remotely"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install JetBrains Projector on MacOS using homebrew

- App Name: JetBrains Projector
- App description: Common and client-related code for running Swing applications remotely
- App Version: 1.1.0
- App Website: https://lp.jetbrains.com/projector/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install JetBrains Projector with the following command
   ```
   brew install --cask projector
   ```
4. JetBrains Projector is ready to use now!