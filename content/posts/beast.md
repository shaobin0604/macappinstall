---
title: "Install beast on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Bayesian Evolutionary Analysis Sampling Trees"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install beast on MacOS using homebrew

- App Name: beast
- App description: Bayesian Evolutionary Analysis Sampling Trees
- App Version: 1.10.4
- App Website: https://beast.community/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install beast with the following command
   ```
   brew install beast
   ```
4. beast is ready to use now!