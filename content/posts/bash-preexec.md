---
title: "Install bash-preexec on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Preexec and precmd functions for Bash (like Zsh)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bash-preexec on MacOS using homebrew

- App Name: bash-preexec
- App description: Preexec and precmd functions for Bash (like Zsh)
- App Version: 0.4.1
- App Website: https://github.com/rcaloras/bash-preexec

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bash-preexec with the following command
   ```
   brew install bash-preexec
   ```
4. bash-preexec is ready to use now!