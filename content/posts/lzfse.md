---
title: "Install lzfse on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Apple LZFSE compression library and command-line tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install lzfse on MacOS using homebrew

- App Name: lzfse
- App description: Apple LZFSE compression library and command-line tool
- App Version: 1.0
- App Website: https://github.com/lzfse/lzfse

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install lzfse with the following command
   ```
   brew install lzfse
   ```
4. lzfse is ready to use now!