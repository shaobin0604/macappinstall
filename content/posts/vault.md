---
title: "Install vault on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Secures, stores, and tightly controls access to secrets"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install vault on MacOS using homebrew

- App Name: vault
- App description: Secures, stores, and tightly controls access to secrets
- App Version: 1.9.3
- App Website: https://vaultproject.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install vault with the following command
   ```
   brew install vault
   ```
4. vault is ready to use now!