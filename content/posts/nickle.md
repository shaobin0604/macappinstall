---
title: "Install nickle on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Desk calculator language"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install nickle on MacOS using homebrew

- App Name: nickle
- App description: Desk calculator language
- App Version: 2.90
- App Website: https://www.nickle.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install nickle with the following command
   ```
   brew install nickle
   ```
4. nickle is ready to use now!