---
title: "Install bats on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "TAP-compliant test framework for Bash scripts"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bats on MacOS using homebrew

- App Name: bats
- App description: TAP-compliant test framework for Bash scripts
- App Version: 0.4.0
- App Website: https://github.com/sstephenson/bats

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bats with the following command
   ```
   brew install bats
   ```
4. bats is ready to use now!