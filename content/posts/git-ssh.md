---
title: "Install git-ssh on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Proxy for serving git repositories over SSH"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install git-ssh on MacOS using homebrew

- App Name: git-ssh
- App description: Proxy for serving git repositories over SSH
- App Version: 0.2.0
- App Website: https://github.com/lemarsu/git-ssh

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install git-ssh with the following command
   ```
   brew install git-ssh
   ```
4. git-ssh is ready to use now!