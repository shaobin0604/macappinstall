---
title: "Install StarUML on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Software modeler"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install StarUML on MacOS using homebrew

- App Name: StarUML
- App description: Software modeler
- App Version: 4.1.6
- App Website: https://staruml.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install StarUML with the following command
   ```
   brew install --cask staruml
   ```
4. StarUML is ready to use now!