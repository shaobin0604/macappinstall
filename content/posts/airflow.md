---
title: "Install Airflow on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Watch local content on Apple TV and Chromecast"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Airflow on MacOS using homebrew

- App Name: Airflow
- App description: Watch local content on Apple TV and Chromecast
- App Version: 3.3.1
- App Website: https://airflowapp.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Airflow with the following command
   ```
   brew install --cask airflow
   ```
4. Airflow is ready to use now!