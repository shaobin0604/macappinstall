---
title: "Install Parsify on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Extensible calculator with unit and currency conversions"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Parsify on MacOS using homebrew

- App Name: Parsify
- App description: Extensible calculator with unit and currency conversions
- App Version: 1.8.0
- App Website: https://parsify.app/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Parsify with the following command
   ```
   brew install --cask parsify
   ```
4. Parsify is ready to use now!