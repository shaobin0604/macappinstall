---
title: "Install urdfdom on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Unified Robot Description Format (URDF) parser"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install urdfdom on MacOS using homebrew

- App Name: urdfdom
- App description: Unified Robot Description Format (URDF) parser
- App Version: 3.0.0
- App Website: https://wiki.ros.org/urdf/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install urdfdom with the following command
   ```
   brew install urdfdom
   ```
4. urdfdom is ready to use now!