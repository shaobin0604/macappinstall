---
title: "Install liquid-dsp on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Digital signal processing library for software-defined radios"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install liquid-dsp on MacOS using homebrew

- App Name: liquid-dsp
- App description: Digital signal processing library for software-defined radios
- App Version: 1.4.0
- App Website: https://liquidsdr.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install liquid-dsp with the following command
   ```
   brew install liquid-dsp
   ```
4. liquid-dsp is ready to use now!