---
title: "Install Forecast on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Podcast MP3 encoder with chapters"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Forecast on MacOS using homebrew

- App Name: Forecast
- App description: Podcast MP3 encoder with chapters
- App Version: 0.9.4,137
- App Website: https://overcast.fm/forecast

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Forecast with the following command
   ```
   brew install --cask forecast
   ```
4. Forecast is ready to use now!