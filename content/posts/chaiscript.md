---
title: "Install chaiscript on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Easy to use embedded scripting language for C++"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install chaiscript on MacOS using homebrew

- App Name: chaiscript
- App description: Easy to use embedded scripting language for C++
- App Version: 6.1.0
- App Website: https://chaiscript.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install chaiscript with the following command
   ```
   brew install chaiscript
   ```
4. chaiscript is ready to use now!