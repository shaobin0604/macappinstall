---
title: "Install Image Tool on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Scale images and convert image file formats"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Image Tool on MacOS using homebrew

- App Name: Image Tool
- App description: Scale images and convert image file formats
- App Version: 1.4.1
- App Website: http://www.jimmcgowan.net/Site/ImageTool.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Image Tool with the following command
   ```
   brew install --cask image-tool
   ```
4. Image Tool is ready to use now!