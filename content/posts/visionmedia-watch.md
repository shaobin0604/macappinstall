---
title: "Install visionmedia-watch on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Periodically executes the given command"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install visionmedia-watch on MacOS using homebrew

- App Name: visionmedia-watch
- App description: Periodically executes the given command
- App Version: 0.4.0
- App Website: https://github.com/visionmedia/watch

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install visionmedia-watch with the following command
   ```
   brew install visionmedia-watch
   ```
4. visionmedia-watch is ready to use now!