---
title: "Install Harmony on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Music player"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Harmony on MacOS using homebrew

- App Name: Harmony
- App description: Music player
- App Version: 0.9.1
- App Website: https://getharmony.xyz/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Harmony with the following command
   ```
   brew install --cask harmony
   ```
4. Harmony is ready to use now!