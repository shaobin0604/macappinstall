---
title: "Install fheroes2 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Free Heroes of Might and Magic II is a recreation of HoMM2 game engine"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fheroes2 on MacOS using homebrew

- App Name: fheroes2
- App description: Free Heroes of Might and Magic II is a recreation of HoMM2 game engine
- App Version: 0.9.12
- App Website: https://ihhub.github.io/fheroes2/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fheroes2 with the following command
   ```
   brew install fheroes2
   ```
4. fheroes2 is ready to use now!