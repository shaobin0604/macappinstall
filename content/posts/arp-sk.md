---
title: "Install arp-sk on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "ARP traffic generation tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install arp-sk on MacOS using homebrew

- App Name: arp-sk
- App description: ARP traffic generation tool
- App Version: 0.0.16
- App Website: https://web.archive.org/web/20180223202629/sid.rstack.org/arp-sk/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install arp-sk with the following command
   ```
   brew install arp-sk
   ```
4. arp-sk is ready to use now!