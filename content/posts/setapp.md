---
title: "Install Setapp on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Collection of apps available by subscription"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Setapp on MacOS using homebrew

- App Name: Setapp
- App description: Collection of apps available by subscription
- App Version: 2.13.2,1638793697
- App Website: https://setapp.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Setapp with the following command
   ```
   brew install --cask setapp
   ```
4. Setapp is ready to use now!