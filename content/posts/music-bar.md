---
title: "Install Music Bar on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Control Apple Music right from your menu bar"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Music Bar on MacOS using homebrew

- App Name: Music Bar
- App description: Control Apple Music right from your menu bar
- App Version: 1.6
- App Website: https://musa11971.github.io/Music-Bar/index.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Music Bar with the following command
   ```
   brew install --cask music-bar
   ```
4. Music Bar is ready to use now!