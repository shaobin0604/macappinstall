---
title: "Install Notion on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "App to write, plan, collaborate, and get organized"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Notion on MacOS using homebrew

- App Name: Notion
- App description: App to write, plan, collaborate, and get organized
- App Version: 2.0.21
- App Website: https://www.notion.so/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Notion with the following command
   ```
   brew install --cask notion
   ```
4. Notion is ready to use now!