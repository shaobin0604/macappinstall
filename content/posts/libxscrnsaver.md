---
title: "Install libxscrnsaver on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "X.Org: X11 Screen Saver extension client library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libxscrnsaver on MacOS using homebrew

- App Name: libxscrnsaver
- App description: X.Org: X11 Screen Saver extension client library
- App Version: 1.2.3
- App Website: https://www.x.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libxscrnsaver with the following command
   ```
   brew install libxscrnsaver
   ```
4. libxscrnsaver is ready to use now!