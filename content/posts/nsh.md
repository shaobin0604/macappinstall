---
title: "Install nsh on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Fish-like, POSIX-compatible shell"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install nsh on MacOS using homebrew

- App Name: nsh
- App description: Fish-like, POSIX-compatible shell
- App Version: 0.4.2
- App Website: https://github.com/nuta/nsh

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install nsh with the following command
   ```
   brew install nsh
   ```
4. nsh is ready to use now!