---
title: "Install Electron Fiddle on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Create and play with small Electron experiments"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Electron Fiddle on MacOS using homebrew

- App Name: Electron Fiddle
- App description: Create and play with small Electron experiments
- App Version: 0.27.3
- App Website: https://www.electronjs.org/fiddle

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Electron Fiddle with the following command
   ```
   brew install --cask electron-fiddle
   ```
4. Electron Fiddle is ready to use now!