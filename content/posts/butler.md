---
title: "Install Butler on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Arrange your tasks in a customizable configuration"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Butler on MacOS using homebrew

- App Name: Butler
- App description: Arrange your tasks in a customizable configuration
- App Version: 4.4.4,5115
- App Website: https://manytricks.com/butler/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Butler with the following command
   ```
   brew install --cask butler
   ```
4. Butler is ready to use now!