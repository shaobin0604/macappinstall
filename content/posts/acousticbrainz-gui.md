---
title: "Install AcousticBrainz on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Graphical client to submit audio features to the AcousticBrainz database"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install AcousticBrainz on MacOS using homebrew

- App Name: AcousticBrainz
- App description: Graphical client to submit audio features to the AcousticBrainz database
- App Version: 0.1
- App Website: https://acousticbrainz.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install AcousticBrainz with the following command
   ```
   brew install --cask acousticbrainz-gui
   ```
4. AcousticBrainz is ready to use now!