---
title: "Install exempi on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library to parse XMP metadata"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install exempi on MacOS using homebrew

- App Name: exempi
- App description: Library to parse XMP metadata
- App Version: 2.5.2
- App Website: https://wiki.freedesktop.org/libopenraw/Exempi/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install exempi with the following command
   ```
   brew install exempi
   ```
4. exempi is ready to use now!