---
title: "Install TETR.IO on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Free-to-play Tetris clone"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install TETR.IO on MacOS using homebrew

- App Name: TETR.IO
- App description: Free-to-play Tetris clone
- App Version: 8.0.0
- App Website: https://tetr.io/about

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install TETR.IO with the following command
   ```
   brew install --cask tetrio
   ```
4. TETR.IO is ready to use now!