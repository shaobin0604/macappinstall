---
title: "Install freeswitch on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Telephony platform to route various communication protocols"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install freeswitch on MacOS using homebrew

- App Name: freeswitch
- App description: Telephony platform to route various communication protocols
- App Version: 1.10.7
- App Website: https://freeswitch.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install freeswitch with the following command
   ```
   brew install freeswitch
   ```
4. freeswitch is ready to use now!