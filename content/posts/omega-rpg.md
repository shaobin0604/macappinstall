---
title: "Install omega-rpg on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Classic Roguelike game"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install omega-rpg on MacOS using homebrew

- App Name: omega-rpg
- App description: Classic Roguelike game
- App Version: 0.80.2
- App Website: http://www.alcyone.com/max/projects/omega/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install omega-rpg with the following command
   ```
   brew install omega-rpg
   ```
4. omega-rpg is ready to use now!