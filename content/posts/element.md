---
title: "Install Element on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Matrix collaboration client"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Element on MacOS using homebrew

- App Name: Element
- App description: Matrix collaboration client
- App Version: 1.10.4
- App Website: https://element.io/get-started

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Element with the following command
   ```
   brew install --cask element
   ```
4. Element is ready to use now!