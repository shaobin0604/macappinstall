---
title: "Install baobab on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Gnome disk usage analyzer"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install baobab on MacOS using homebrew

- App Name: baobab
- App description: Gnome disk usage analyzer
- App Version: 41.0
- App Website: https://wiki.gnome.org/Apps/Baobab

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install baobab with the following command
   ```
   brew install baobab
   ```
4. baobab is ready to use now!