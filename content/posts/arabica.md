---
title: "Install arabica on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "XML toolkit written in C++"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install arabica on MacOS using homebrew

- App Name: arabica
- App description: XML toolkit written in C++
- App Version: 20200425
- App Website: https://www.jezuk.co.uk/tags/arabica.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install arabica with the following command
   ```
   brew install arabica
   ```
4. arabica is ready to use now!