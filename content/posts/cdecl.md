---
title: "Install cdecl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Turn English phrases to C or C++ declarations"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cdecl on MacOS using homebrew

- App Name: cdecl
- App description: Turn English phrases to C or C++ declarations
- App Version: 2.5
- App Website: https://cdecl.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cdecl with the following command
   ```
   brew install cdecl
   ```
4. cdecl is ready to use now!