---
title: "Install povray on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Persistence Of Vision RAYtracer (POVRAY)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install povray on MacOS using homebrew

- App Name: povray
- App description: Persistence Of Vision RAYtracer (POVRAY)
- App Version: 3.7.0.10
- App Website: https://www.povray.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install povray with the following command
   ```
   brew install povray
   ```
4. povray is ready to use now!