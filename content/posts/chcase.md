---
title: "Install chcase on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Perl file-renaming script"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install chcase on MacOS using homebrew

- App Name: chcase
- App description: Perl file-renaming script
- App Version: 2.0
- App Website: http://www.primaledge.ca/chcase.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install chcase with the following command
   ```
   brew install chcase
   ```
4. chcase is ready to use now!