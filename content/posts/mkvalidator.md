---
title: "Install mkvalidator on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to verify Matroska and WebM files for spec conformance"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mkvalidator on MacOS using homebrew

- App Name: mkvalidator
- App description: Tool to verify Matroska and WebM files for spec conformance
- App Version: 0.6.0
- App Website: https://www.matroska.org/downloads/mkvalidator.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mkvalidator with the following command
   ```
   brew install mkvalidator
   ```
4. mkvalidator is ready to use now!