---
title: "Install ManageEngine MibBrowser on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ManageEngine MibBrowser on MacOS using homebrew

- App Name: ManageEngine MibBrowser
- App description: null
- App Version: 5.2,9229779
- App Website: https://www.manageengine.com/products/mibbrowser-free-tool/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ManageEngine MibBrowser with the following command
   ```
   brew install --cask manageengine-mibbrowser
   ```
4. ManageEngine MibBrowser is ready to use now!