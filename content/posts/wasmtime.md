---
title: "Install wasmtime on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Standalone JIT-style runtime for WebAssembly, using Cranelift"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install wasmtime on MacOS using homebrew

- App Name: wasmtime
- App description: Standalone JIT-style runtime for WebAssembly, using Cranelift
- App Version: 0.34.1
- App Website: https://wasmtime.dev/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install wasmtime with the following command
   ```
   brew install wasmtime
   ```
4. wasmtime is ready to use now!