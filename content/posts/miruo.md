---
title: "Install miruo on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Pretty-print TCP session monitor/analyzer"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install miruo on MacOS using homebrew

- App Name: miruo
- App description: Pretty-print TCP session monitor/analyzer
- App Version: 0.9.6b
- App Website: https://github.com/KLab/miruo/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install miruo with the following command
   ```
   brew install miruo
   ```
4. miruo is ready to use now!