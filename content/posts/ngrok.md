---
title: "Install ngrok on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Reverse proxy, secure introspectable tunnels to localhost"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ngrok on MacOS using homebrew

- App Name: ngrok
- App description: Reverse proxy, secure introspectable tunnels to localhost
- App Version: 2.3.40
- App Website: https://ngrok.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ngrok with the following command
   ```
   brew install --cask ngrok
   ```
4. ngrok is ready to use now!