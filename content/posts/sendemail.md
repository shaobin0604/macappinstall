---
title: "Install sendemail on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Email program for sending SMTP mail"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sendemail on MacOS using homebrew

- App Name: sendemail
- App description: Email program for sending SMTP mail
- App Version: 1.56
- App Website: https://web.archive.org/web/20191013154932/caspian.dotconf.net/menu/Software/SendEmail/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sendemail with the following command
   ```
   brew install sendemail
   ```
4. sendemail is ready to use now!