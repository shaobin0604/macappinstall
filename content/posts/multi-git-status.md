---
title: "Install multi-git-status on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Show uncommitted, untracked and unpushed changes for multiple Git repos"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install multi-git-status on MacOS using homebrew

- App Name: multi-git-status
- App description: Show uncommitted, untracked and unpushed changes for multiple Git repos
- App Version: 2.0
- App Website: https://github.com/fboender/multi-git-status

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install multi-git-status with the following command
   ```
   brew install multi-git-status
   ```
4. multi-git-status is ready to use now!