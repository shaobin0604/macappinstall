---
title: "Install OSCAR on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CPAP Analysis Reporter"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install OSCAR on MacOS using homebrew

- App Name: OSCAR
- App description: CPAP Analysis Reporter
- App Version: 1.3.1
- App Website: https://www.sleepfiles.com/OSCAR/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install OSCAR with the following command
   ```
   brew install --cask oscar
   ```
4. OSCAR is ready to use now!