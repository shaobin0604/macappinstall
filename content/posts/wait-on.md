---
title: "Install wait_on on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Provides shell scripts with access to kqueue(3)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install wait_on on MacOS using homebrew

- App Name: wait_on
- App description: Provides shell scripts with access to kqueue(3)
- App Version: 1.1
- App Website: https://www.freshports.org/sysutils/wait_on/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install wait_on with the following command
   ```
   brew install wait_on
   ```
4. wait_on is ready to use now!