---
title: "Install plplot on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cross-platform software package for creating scientific plots"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install plplot on MacOS using homebrew

- App Name: plplot
- App description: Cross-platform software package for creating scientific plots
- App Version: 5.15.0
- App Website: https://plplot.sourceforge.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install plplot with the following command
   ```
   brew install plplot
   ```
4. plplot is ready to use now!