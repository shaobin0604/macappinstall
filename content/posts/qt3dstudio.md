---
title: "Install Qt 3D Studio on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Compositing tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Qt 3D Studio on MacOS using homebrew

- App Name: Qt 3D Studio
- App description: Compositing tool
- App Version: 2.8.0
- App Website: https://www.qt.io/developers/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Qt 3D Studio with the following command
   ```
   brew install --cask qt3dstudio
   ```
4. Qt 3D Studio is ready to use now!