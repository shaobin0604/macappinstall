---
title: "Install cloudformation-cli on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CloudFormation Provider Development Toolkit"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cloudformation-cli on MacOS using homebrew

- App Name: cloudformation-cli
- App description: CloudFormation Provider Development Toolkit
- App Version: 0.2.23
- App Website: https://github.com/aws-cloudformation/cloudformation-cli/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cloudformation-cli with the following command
   ```
   brew install cloudformation-cli
   ```
4. cloudformation-cli is ready to use now!