---
title: "Install iMazing on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "iPhone management application"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install iMazing on MacOS using homebrew

- App Name: iMazing
- App description: iPhone management application
- App Version: 2.14.6,15535
- App Website: https://imazing.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install iMazing with the following command
   ```
   brew install --cask imazing
   ```
4. iMazing is ready to use now!