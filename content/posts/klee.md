---
title: "Install klee on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Symbolic Execution Engine"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install klee on MacOS using homebrew

- App Name: klee
- App description: Symbolic Execution Engine
- App Version: 2.2
- App Website: https://klee.github.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install klee with the following command
   ```
   brew install klee
   ```
4. klee is ready to use now!