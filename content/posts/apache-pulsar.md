---
title: "Install apache-pulsar on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cloud-native distributed messaging and streaming platform"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install apache-pulsar on MacOS using homebrew

- App Name: apache-pulsar
- App description: Cloud-native distributed messaging and streaming platform
- App Version: 2.9.1
- App Website: https://pulsar.apache.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install apache-pulsar with the following command
   ```
   brew install apache-pulsar
   ```
4. apache-pulsar is ready to use now!