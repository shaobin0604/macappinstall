---
title: "Install wasm-pack on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Your favorite rust -> wasm workflow tool!"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install wasm-pack on MacOS using homebrew

- App Name: wasm-pack
- App description: Your favorite rust -> wasm workflow tool!
- App Version: 0.10.2
- App Website: https://rustwasm.github.io/wasm-pack/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install wasm-pack with the following command
   ```
   brew install wasm-pack
   ```
4. wasm-pack is ready to use now!