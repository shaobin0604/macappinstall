---
title: "Install nesc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Programming language for deeply networked systems"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install nesc on MacOS using homebrew

- App Name: nesc
- App description: Programming language for deeply networked systems
- App Version: 1.4.0
- App Website: https://github.com/tinyos/nesc

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install nesc with the following command
   ```
   brew install nesc
   ```
4. nesc is ready to use now!