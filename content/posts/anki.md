---
title: "Install Anki on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Memory training application"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Anki on MacOS using homebrew

- App Name: Anki
- App description: Memory training application
- App Version: 2.1.49
- App Website: https://apps.ankiweb.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Anki with the following command
   ```
   brew install --cask anki
   ```
4. Anki is ready to use now!