---
title: "Install Artpip on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Curated photographic desktop backgrounds"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Artpip on MacOS using homebrew

- App Name: Artpip
- App description: Curated photographic desktop backgrounds
- App Version: 2.7.1
- App Website: https://www.artpip.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Artpip with the following command
   ```
   brew install --cask artpip
   ```
4. Artpip is ready to use now!