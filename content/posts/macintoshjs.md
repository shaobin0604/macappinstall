---
title: "Install macintosh.js on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Virtual Apple Macintosh with System 8, running in Electron"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install macintosh.js on MacOS using homebrew

- App Name: macintosh.js
- App description: Virtual Apple Macintosh with System 8, running in Electron
- App Version: 1.1.0
- App Website: https://github.com/felixrieseberg/macintosh.js

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install macintosh.js with the following command
   ```
   brew install --cask macintoshjs
   ```
4. macintosh.js is ready to use now!