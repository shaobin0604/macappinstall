---
title: "Install i2c-tools on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Heterogeneous set of I2C tools for Linux"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install i2c-tools on MacOS using homebrew

- App Name: i2c-tools
- App description: Heterogeneous set of I2C tools for Linux
- App Version: 4.3
- App Website: https://i2c.wiki.kernel.org/index.php/I2C_Tools

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install i2c-tools with the following command
   ```
   brew install i2c-tools
   ```
4. i2c-tools is ready to use now!