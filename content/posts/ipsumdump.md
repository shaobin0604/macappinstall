---
title: "Install ipsumdump on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Summarizes TCP/IP dump files into a self-describing ASCII format"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ipsumdump on MacOS using homebrew

- App Name: ipsumdump
- App description: Summarizes TCP/IP dump files into a self-describing ASCII format
- App Version: 1.86
- App Website: https://read.seas.harvard.edu/~kohler/ipsumdump/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ipsumdump with the following command
   ```
   brew install ipsumdump
   ```
4. ipsumdump is ready to use now!