---
title: "Install mda-lv2 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "LV2 port of the MDA plugins"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mda-lv2 on MacOS using homebrew

- App Name: mda-lv2
- App description: LV2 port of the MDA plugins
- App Version: 1.2.6
- App Website: https://drobilla.net/software/mda-lv2.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mda-lv2 with the following command
   ```
   brew install mda-lv2
   ```
4. mda-lv2 is ready to use now!