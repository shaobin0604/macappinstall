---
title: "Install Defold on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Game engine for development of desktop, mobile and web games"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Defold on MacOS using homebrew

- App Name: Defold
- App description: Game engine for development of desktop, mobile and web games
- App Version: 1.2.190
- App Website: https://defold.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Defold with the following command
   ```
   brew install --cask defold
   ```
4. Defold is ready to use now!