---
title: "Install scipy on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Software for mathematics, science, and engineering"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install scipy on MacOS using homebrew

- App Name: scipy
- App description: Software for mathematics, science, and engineering
- App Version: 1.8.0
- App Website: https://www.scipy.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install scipy with the following command
   ```
   brew install scipy
   ```
4. scipy is ready to use now!