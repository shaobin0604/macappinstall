---
title: "Install ssed on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Super sed stream editor"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ssed on MacOS using homebrew

- App Name: ssed
- App description: Super sed stream editor
- App Version: 3.62
- App Website: https://sed.sourceforge.io/grabbag/ssed/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ssed with the following command
   ```
   brew install ssed
   ```
4. ssed is ready to use now!