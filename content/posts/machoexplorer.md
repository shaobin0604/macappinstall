---
title: "Install MachOExplorer on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Mach-O Executable File Explorer"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install MachOExplorer on MacOS using homebrew

- App Name: MachOExplorer
- App description: Mach-O Executable File Explorer
- App Version: 1.0
- App Website: https://github.com/everettjf/MachOExplorer

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install MachOExplorer with the following command
   ```
   brew install --cask machoexplorer
   ```
4. MachOExplorer is ready to use now!