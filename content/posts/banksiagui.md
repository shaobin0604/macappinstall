---
title: "Install BanksiaGui on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Chess GUI"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install BanksiaGui on MacOS using homebrew

- App Name: BanksiaGui
- App description: Chess GUI
- App Version: 0.53
- App Website: https://banksiagui.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install BanksiaGui with the following command
   ```
   brew install --cask banksiagui
   ```
4. BanksiaGui is ready to use now!