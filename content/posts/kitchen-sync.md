---
title: "Install kitchen-sync on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Fast efficiently sync database without dumping & reloading"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install kitchen-sync on MacOS using homebrew

- App Name: kitchen-sync
- App description: Fast efficiently sync database without dumping & reloading
- App Version: 2.14
- App Website: https://github.com/willbryant/kitchen_sync

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install kitchen-sync with the following command
   ```
   brew install kitchen-sync
   ```
4. kitchen-sync is ready to use now!