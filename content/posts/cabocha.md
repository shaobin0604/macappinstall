---
title: "Install cabocha on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Yet Another Japanese Dependency Structure Analyzer"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cabocha on MacOS using homebrew

- App Name: cabocha
- App description: Yet Another Japanese Dependency Structure Analyzer
- App Version: 0.69
- App Website: https://taku910.github.io/cabocha/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cabocha with the following command
   ```
   brew install cabocha
   ```
4. cabocha is ready to use now!