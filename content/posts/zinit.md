---
title: "Install zinit on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Flexible and fast Zsh plugin manager"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install zinit on MacOS using homebrew

- App Name: zinit
- App description: Flexible and fast Zsh plugin manager
- App Version: 3.7
- App Website: https://zdharma-continuum.github.io/zinit/wiki/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install zinit with the following command
   ```
   brew install zinit
   ```
4. zinit is ready to use now!