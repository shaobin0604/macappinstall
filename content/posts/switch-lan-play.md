---
title: "Install switch-lan-play on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Make you and your friends play games like in a LAN"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install switch-lan-play on MacOS using homebrew

- App Name: switch-lan-play
- App description: Make you and your friends play games like in a LAN
- App Version: 0.2.3
- App Website: https://github.com/spacemeowx2/switch-lan-play

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install switch-lan-play with the following command
   ```
   brew install switch-lan-play
   ```
4. switch-lan-play is ready to use now!