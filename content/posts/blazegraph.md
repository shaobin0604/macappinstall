---
title: "Install blazegraph on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Graph database supporting RDF data model, Sesame, and Blueprint APIs"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install blazegraph on MacOS using homebrew

- App Name: blazegraph
- App description: Graph database supporting RDF data model, Sesame, and Blueprint APIs
- App Version: 2.1.5
- App Website: https://www.blazegraph.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install blazegraph with the following command
   ```
   brew install blazegraph
   ```
4. blazegraph is ready to use now!