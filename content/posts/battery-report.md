---
title: "Install Battery Report on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Creates technical summaries of power supplies"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Battery Report on MacOS using homebrew

- App Name: Battery Report
- App description: Creates technical summaries of power supplies
- App Version: 1.2.0
- App Website: https://www.dssw.co.uk/batteryreport/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Battery Report with the following command
   ```
   brew install --cask battery-report
   ```
4. Battery Report is ready to use now!