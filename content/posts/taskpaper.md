---
title: "Install TaskPaper on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "App to make lists and help with organization"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install TaskPaper on MacOS using homebrew

- App Name: TaskPaper
- App description: App to make lists and help with organization
- App Version: 3.8.16,475
- App Website: https://www.taskpaper.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install TaskPaper with the following command
   ```
   brew install --cask taskpaper
   ```
4. TaskPaper is ready to use now!