---
title: "Install sdcv on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "StarDict Console Version"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sdcv on MacOS using homebrew

- App Name: sdcv
- App description: StarDict Console Version
- App Version: 0.5.3
- App Website: https://dushistov.github.io/sdcv/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sdcv with the following command
   ```
   brew install sdcv
   ```
4. sdcv is ready to use now!