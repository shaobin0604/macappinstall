---
title: "Install IINA+ on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Extra danmaku support for iina (iina 弹幕支持)"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install IINA+ on MacOS using homebrew

- App Name: IINA+
- App description: Extra danmaku support for iina (iina 弹幕支持)
- App Version: 0.6.0,21122622
- App Website: https://github.com/xjbeta/iina-plus

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install IINA+ with the following command
   ```
   brew install --cask iina-plus
   ```
4. IINA+ is ready to use now!