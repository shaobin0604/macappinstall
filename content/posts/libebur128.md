---
title: "Install libebur128 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library implementing the EBU R128 loudness standard"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libebur128 on MacOS using homebrew

- App Name: libebur128
- App description: Library implementing the EBU R128 loudness standard
- App Version: 1.2.6
- App Website: https://github.com/jiixyj/libebur128

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libebur128 with the following command
   ```
   brew install libebur128
   ```
4. libebur128 is ready to use now!