---
title: "Install tesseract on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "OCR (Optical Character Recognition) engine"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tesseract on MacOS using homebrew

- App Name: tesseract
- App description: OCR (Optical Character Recognition) engine
- App Version: 5.0.1
- App Website: https://github.com/tesseract-ocr/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tesseract with the following command
   ```
   brew install tesseract
   ```
4. tesseract is ready to use now!