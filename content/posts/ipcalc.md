---
title: "Install ipcalc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Calculate various network masks, etc. from a given IP address"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ipcalc on MacOS using homebrew

- App Name: ipcalc
- App description: Calculate various network masks, etc. from a given IP address
- App Version: 0.51
- App Website: http://jodies.de/ipcalc

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ipcalc with the following command
   ```
   brew install ipcalc
   ```
4. ipcalc is ready to use now!