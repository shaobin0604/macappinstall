---
title: "Install qBittorrent on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Peer to peer Bitorrent client"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install qBittorrent on MacOS using homebrew

- App Name: qBittorrent
- App description: Peer to peer Bitorrent client
- App Version: 4.3.9
- App Website: https://www.qbittorrent.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install qBittorrent with the following command
   ```
   brew install --cask qbittorrent
   ```
4. qBittorrent is ready to use now!