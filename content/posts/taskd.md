---
title: "Install taskd on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Client-server synchronization for todo lists"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install taskd on MacOS using homebrew

- App Name: taskd
- App description: Client-server synchronization for todo lists
- App Version: 1.1.0
- App Website: https://taskwarrior.org/docs/taskserver/setup.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install taskd with the following command
   ```
   brew install taskd
   ```
4. taskd is ready to use now!