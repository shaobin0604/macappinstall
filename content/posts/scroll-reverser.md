---
title: "Install Scroll Reverser on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool to reverse the direction of scrolling"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Scroll Reverser on MacOS using homebrew

- App Name: Scroll Reverser
- App description: Tool to reverse the direction of scrolling
- App Version: 1.8.1,10490
- App Website: https://pilotmoon.com/scrollreverser/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Scroll Reverser with the following command
   ```
   brew install --cask scroll-reverser
   ```
4. Scroll Reverser is ready to use now!