---
title: "Install Vagrant on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Development environment"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Vagrant on MacOS using homebrew

- App Name: Vagrant
- App description: Development environment
- App Version: 2.2.19
- App Website: https://www.vagrantup.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Vagrant with the following command
   ```
   brew install --cask vagrant
   ```
4. Vagrant is ready to use now!