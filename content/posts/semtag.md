---
title: "Install semtag on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Semantic tagging script for git"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install semtag on MacOS using homebrew

- App Name: semtag
- App description: Semantic tagging script for git
- App Version: 0.1.1
- App Website: https://github.com/pnikosis/semtag

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install semtag with the following command
   ```
   brew install semtag
   ```
4. semtag is ready to use now!