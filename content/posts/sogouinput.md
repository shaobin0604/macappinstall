---
title: "Install Sogou Input Method on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Input method supporting full and double spelling"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Sogou Input Method on MacOS using homebrew

- App Name: Sogou Input Method
- App description: Input method supporting full and double spelling
- App Version: 630a,1642071509
- App Website: https://pinyin.sogou.com/mac/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Sogou Input Method with the following command
   ```
   brew install --cask sogouinput
   ```
4. Sogou Input Method is ready to use now!