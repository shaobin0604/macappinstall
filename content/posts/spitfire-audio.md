---
title: "Install Spitfire Audio on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Download manager for Spitfire audio libraries"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Spitfire Audio on MacOS using homebrew

- App Name: Spitfire Audio
- App description: Download manager for Spitfire audio libraries
- App Version: 3.3.23,1642640400
- App Website: https://www.spitfireaudio.com/info/library-manager/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Spitfire Audio with the following command
   ```
   brew install --cask spitfire-audio
   ```
4. Spitfire Audio is ready to use now!