---
title: "Install MegaZeux on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "ASCII-based game creation system"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install MegaZeux on MacOS using homebrew

- App Name: MegaZeux
- App description: ASCII-based game creation system
- App Version: 2.92f
- App Website: https://www.digitalmzx.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install MegaZeux with the following command
   ```
   brew install --cask megazeux
   ```
4. MegaZeux is ready to use now!