---
title: "Install Flycut on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Clipboard manager for developers"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Flycut on MacOS using homebrew

- App Name: Flycut
- App description: Clipboard manager for developers
- App Version: 1.9.6
- App Website: https://github.com/TermiT/Flycut

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Flycut with the following command
   ```
   brew install --cask flycut
   ```
4. Flycut is ready to use now!