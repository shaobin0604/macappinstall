---
title: "Install dyld-headers on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Header files for the dynamic linker"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dyld-headers on MacOS using homebrew

- App Name: dyld-headers
- App description: Header files for the dynamic linker
- App Version: 852.2
- App Website: https://opensource.apple.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dyld-headers with the following command
   ```
   brew install dyld-headers
   ```
4. dyld-headers is ready to use now!