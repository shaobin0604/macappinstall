---
title: "Install Suspicious Package on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Application for inspecting installer packages"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Suspicious Package on MacOS using homebrew

- App Name: Suspicious Package
- App description: Application for inspecting installer packages
- App Version: 4.1,880
- App Website: https://www.mothersruin.com/software/SuspiciousPackage/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Suspicious Package with the following command
   ```
   brew install --cask suspicious-package
   ```
4. Suspicious Package is ready to use now!