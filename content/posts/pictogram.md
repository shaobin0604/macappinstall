---
title: "Install Pictogram on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Customize and maintain app icons"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Pictogram on MacOS using homebrew

- App Name: Pictogram
- App description: Customize and maintain app icons
- App Version: 0.1,13
- App Website: https://pictogramapp.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Pictogram with the following command
   ```
   brew install --cask pictogram
   ```
4. Pictogram is ready to use now!