---
title: "Install Duplicate File Finder on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Find and remove unwanted duplicate files and folders"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Duplicate File Finder on MacOS using homebrew

- App Name: Duplicate File Finder
- App description: Find and remove unwanted duplicate files and folders
- App Version: 6.15.1,557
- App Website: https://nektony.com/duplicate-finder-free

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Duplicate File Finder with the following command
   ```
   brew install --cask duplicate-file-finder
   ```
4. Duplicate File Finder is ready to use now!