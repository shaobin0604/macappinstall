---
title: "Install rawgl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Rewritten engine for Another World"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install rawgl on MacOS using homebrew

- App Name: rawgl
- App description: Rewritten engine for Another World
- App Version: 0.2.1
- App Website: https://github.com/cyxx/rawgl

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install rawgl with the following command
   ```
   brew install rawgl
   ```
4. rawgl is ready to use now!