---
title: "Install Dropshare on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "File sharing solution"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Dropshare on MacOS using homebrew

- App Name: Dropshare
- App description: File sharing solution
- App Version: 5.19,5258
- App Website: https://dropshare.app/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Dropshare with the following command
   ```
   brew install --cask dropshare
   ```
4. Dropshare is ready to use now!