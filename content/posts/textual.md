---
title: "Install Textual on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Application for interacting with Internet Relay Chat (IRC) chatrooms"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Textual on MacOS using homebrew

- App Name: Textual
- App description: Application for interacting with Internet Relay Chat (IRC) chatrooms
- App Version: 7.2.1,210711.11,9d231fdb0
- App Website: https://www.codeux.com/textual/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Textual with the following command
   ```
   brew install --cask textual
   ```
4. Textual is ready to use now!