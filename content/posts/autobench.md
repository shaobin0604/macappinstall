---
title: "Install autobench on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Automatic webserver benchmark tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install autobench on MacOS using homebrew

- App Name: autobench
- App description: Automatic webserver benchmark tool
- App Version: 2.1.2
- App Website: http://www.xenoclast.org/autobench/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install autobench with the following command
   ```
   brew install autobench
   ```
4. autobench is ready to use now!