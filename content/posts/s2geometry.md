---
title: "Install s2geometry on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Computational geometry and spatial indexing on the sphere"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install s2geometry on MacOS using homebrew

- App Name: s2geometry
- App description: Computational geometry and spatial indexing on the sphere
- App Version: 0.9.0
- App Website: https://github.com/google/s2geometry

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install s2geometry with the following command
   ```
   brew install s2geometry
   ```
4. s2geometry is ready to use now!