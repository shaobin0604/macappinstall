---
title: "Install Bartender on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Menu bar icon organizer"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Bartender on MacOS using homebrew

- App Name: Bartender
- App description: Menu bar icon organizer
- App Version: 4.1.31,41031
- App Website: https://www.macbartender.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Bartender with the following command
   ```
   brew install --cask bartender
   ```
4. Bartender is ready to use now!