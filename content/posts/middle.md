---
title: "Install Middle on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Add middle click for Trackpad and Magic Mouse"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Middle on MacOS using homebrew

- App Name: Middle
- App description: Add middle click for Trackpad and Magic Mouse
- App Version: 1.6.6,69
- App Website: https://middleclick.app/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Middle with the following command
   ```
   brew install --cask middle
   ```
4. Middle is ready to use now!