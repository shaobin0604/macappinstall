---
title: "Install Downie on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Downloads videos from different websites"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Downie on MacOS using homebrew

- App Name: Downie
- App description: Downloads videos from different websites
- App Version: 4.4.7,4353
- App Website: https://software.charliemonroe.net/downie.php

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Downie with the following command
   ```
   brew install --cask downie
   ```
4. Downie is ready to use now!