---
title: "Install julius on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Two-pass large vocabulary continuous speech recognition engine"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install julius on MacOS using homebrew

- App Name: julius
- App description: Two-pass large vocabulary continuous speech recognition engine
- App Version: 4.6
- App Website: https://github.com/julius-speech/julius

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install julius with the following command
   ```
   brew install julius
   ```
4. julius is ready to use now!