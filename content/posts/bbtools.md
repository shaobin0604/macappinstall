---
title: "Install bbtools on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Brian Bushnell's tools for manipulating reads"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bbtools on MacOS using homebrew

- App Name: bbtools
- App description: Brian Bushnell's tools for manipulating reads
- App Version: 38.95
- App Website: https://jgi.doe.gov/data-and-tools/bbtools/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bbtools with the following command
   ```
   brew install bbtools
   ```
4. bbtools is ready to use now!