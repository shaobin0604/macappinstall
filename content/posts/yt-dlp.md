---
title: "Install yt-dlp on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Fork of youtube-dl with additional features and fixes"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install yt-dlp on MacOS using homebrew

- App Name: yt-dlp
- App description: Fork of youtube-dl with additional features and fixes
- App Version: 2022.2.4
- App Website: https://github.com/yt-dlp/yt-dlp

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install yt-dlp with the following command
   ```
   brew install yt-dlp
   ```
4. yt-dlp is ready to use now!