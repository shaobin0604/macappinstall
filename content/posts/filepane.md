---
title: "Install FilePane on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "File management multi-tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install FilePane on MacOS using homebrew

- App Name: FilePane
- App description: File management multi-tool
- App Version: 1.10.7,1576186002
- App Website: https://mymixapps.com/filepane

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install FilePane with the following command
   ```
   brew install --cask filepane
   ```
4. FilePane is ready to use now!