---
title: "Install gtop on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "System monitoring dashboard for terminal"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gtop on MacOS using homebrew

- App Name: gtop
- App description: System monitoring dashboard for terminal
- App Version: 1.1.3
- App Website: https://github.com/aksakalli/gtop

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gtop with the following command
   ```
   brew install gtop
   ```
4. gtop is ready to use now!