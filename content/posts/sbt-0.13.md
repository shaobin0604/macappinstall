---
title: "Install sbt@0.13 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Build tool for Scala projects"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sbt@0.13 on MacOS using homebrew

- App Name: sbt@0.13
- App description: Build tool for Scala projects
- App Version: 0.13.18
- App Website: https://www.scala-sbt.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sbt@0.13 with the following command
   ```
   brew install sbt@0.13
   ```
4. sbt@0.13 is ready to use now!