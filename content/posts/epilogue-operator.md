---
title: "Install Epilogue Operator on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Play and manage Game Boy cartridges on your computer"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Epilogue Operator on MacOS using homebrew

- App Name: Epilogue Operator
- App description: Play and manage Game Boy cartridges on your computer
- App Version: 0.7.1
- App Website: https://www.epilogue.co/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Epilogue Operator with the following command
   ```
   brew install --cask epilogue-operator
   ```
4. Epilogue Operator is ready to use now!