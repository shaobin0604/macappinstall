---
title: "Install man-db on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Unix documentation system"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install man-db on MacOS using homebrew

- App Name: man-db
- App description: Unix documentation system
- App Version: 2.10.1
- App Website: https://www.nongnu.org/man-db/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install man-db with the following command
   ```
   brew install man-db
   ```
4. man-db is ready to use now!