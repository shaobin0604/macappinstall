---
title: "Install dtm on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cross-language distributed transaction manager"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dtm on MacOS using homebrew

- App Name: dtm
- App description: Cross-language distributed transaction manager
- App Version: 1.12.1
- App Website: http://d.dtm.pub

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dtm with the following command
   ```
   brew install dtm
   ```
4. dtm is ready to use now!