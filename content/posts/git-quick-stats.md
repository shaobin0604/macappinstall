---
title: "Install git-quick-stats on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Simple and efficient way to access statistics in git"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install git-quick-stats on MacOS using homebrew

- App Name: git-quick-stats
- App description: Simple and efficient way to access statistics in git
- App Version: 2.3.0
- App Website: https://github.com/arzzen/git-quick-stats

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install git-quick-stats with the following command
   ```
   brew install git-quick-stats
   ```
4. git-quick-stats is ready to use now!