---
title: "Install zsh-you-should-use on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "ZSH plugin that reminds you to use existing aliases for commands you just typed"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install zsh-you-should-use on MacOS using homebrew

- App Name: zsh-you-should-use
- App description: ZSH plugin that reminds you to use existing aliases for commands you just typed
- App Version: 1.7.3
- App Website: https://github.com/MichaelAquilina/zsh-you-should-use

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install zsh-you-should-use with the following command
   ```
   brew install zsh-you-should-use
   ```
4. zsh-you-should-use is ready to use now!