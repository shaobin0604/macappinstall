---
title: "Install Abscissa on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Plotting software"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Abscissa on MacOS using homebrew

- App Name: Abscissa
- App description: Plotting software
- App Version: 4.0.5
- App Website: http://rbruehl.macbay.de/Abscissa/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Abscissa with the following command
   ```
   brew install --cask abscissa
   ```
4. Abscissa is ready to use now!