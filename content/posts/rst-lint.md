---
title: "Install rst-lint on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "ReStructuredText linter"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install rst-lint on MacOS using homebrew

- App Name: rst-lint
- App description: ReStructuredText linter
- App Version: 1.3.2
- App Website: https://github.com/twolfson/restructuredtext-lint

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install rst-lint with the following command
   ```
   brew install rst-lint
   ```
4. rst-lint is ready to use now!