---
title: "Install vapoursynth-imwri on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "VapourSynth filters - ImageMagick HDRI writer/reader"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install vapoursynth-imwri on MacOS using homebrew

- App Name: vapoursynth-imwri
- App description: VapourSynth filters - ImageMagick HDRI writer/reader
- App Version: 1
- App Website: https://github.com/vapoursynth/vs-imwri

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install vapoursynth-imwri with the following command
   ```
   brew install vapoursynth-imwri
   ```
4. vapoursynth-imwri is ready to use now!