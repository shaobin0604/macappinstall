---
title: "Install heatshrink on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Data compression library for embedded/real-time systems"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install heatshrink on MacOS using homebrew

- App Name: heatshrink
- App description: Data compression library for embedded/real-time systems
- App Version: 0.4.1
- App Website: https://github.com/atomicobject/heatshrink

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install heatshrink with the following command
   ```
   brew install heatshrink
   ```
4. heatshrink is ready to use now!