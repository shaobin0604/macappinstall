---
title: "Install picoc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C interpreter for scripting"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install picoc on MacOS using homebrew

- App Name: picoc
- App description: C interpreter for scripting
- App Version: 2.1
- App Website: https://gitlab.com/zsaleeba/picoc

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install picoc with the following command
   ```
   brew install picoc
   ```
4. picoc is ready to use now!