---
title: "Install imapsync on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Migrate or backup IMAP mail accounts"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install imapsync on MacOS using homebrew

- App Name: imapsync
- App description: Migrate or backup IMAP mail accounts
- App Version: 2.178
- App Website: https://imapsync.lamiral.info/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install imapsync with the following command
   ```
   brew install imapsync
   ```
4. imapsync is ready to use now!