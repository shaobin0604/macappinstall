---
title: "Install mbelib on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "P25 Phase 1 and ProVoice vocoder"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mbelib on MacOS using homebrew

- App Name: mbelib
- App description: P25 Phase 1 and ProVoice vocoder
- App Version: 1.3.0
- App Website: https://github.com/szechyjs/mbelib

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mbelib with the following command
   ```
   brew install mbelib
   ```
4. mbelib is ready to use now!