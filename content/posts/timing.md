---
title: "Install Timing on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Automatic time and productivity tracking app"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Timing on MacOS using homebrew

- App Name: Timing
- App description: Automatic time and productivity tracking app
- App Version: 2021.7,315
- App Website: https://timingapp.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Timing with the following command
   ```
   brew install --cask timing
   ```
4. Timing is ready to use now!