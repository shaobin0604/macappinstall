---
title: "Install Nuage on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Free and open-source SoundCloud client"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Nuage on MacOS using homebrew

- App Name: Nuage
- App description: Free and open-source SoundCloud client
- App Version: 0.0.2
- App Website: https://github.com/lbrndnr/nuage-macos

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Nuage with the following command
   ```
   brew install --cask nuage
   ```
4. Nuage is ready to use now!