---
title: "Install flickcurl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library for the Flickr API"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install flickcurl on MacOS using homebrew

- App Name: flickcurl
- App description: Library for the Flickr API
- App Version: 1.26
- App Website: https://librdf.org/flickcurl/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install flickcurl with the following command
   ```
   brew install flickcurl
   ```
4. flickcurl is ready to use now!