---
title: "Install YouTube Music Desktop App on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "YouTube music client"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install YouTube Music Desktop App on MacOS using homebrew

- App Name: YouTube Music Desktop App
- App description: YouTube music client
- App Version: 1.13.0
- App Website: https://ytmdesktop.app/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install YouTube Music Desktop App with the following command
   ```
   brew install --cask ytmdesktop-youtube-music
   ```
4. YouTube Music Desktop App is ready to use now!