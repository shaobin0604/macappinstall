---
title: "Install gwyddion on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Scanning Probe Microscopy visualization and analysis tool"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gwyddion on MacOS using homebrew

- App Name: gwyddion
- App description: Scanning Probe Microscopy visualization and analysis tool
- App Version: 2.59
- App Website: http://gwyddion.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gwyddion with the following command
   ```
   brew install gwyddion
   ```
4. gwyddion is ready to use now!