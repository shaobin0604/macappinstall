---
title: "Install Grisbi on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Personal financial management program"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Grisbi on MacOS using homebrew

- App Name: Grisbi
- App description: Personal financial management program
- App Version: 2.0.5
- App Website: https://www.grisbi.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Grisbi with the following command
   ```
   brew install --cask grisbi
   ```
4. Grisbi is ready to use now!