---
title: "Install Yep on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Document manager"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Yep on MacOS using homebrew

- App Name: Yep
- App description: Document manager
- App Version: 4.0.4,404.101
- App Website: https://www.ironicsoftware.com/yep/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Yep with the following command
   ```
   brew install --cask yep
   ```
4. Yep is ready to use now!