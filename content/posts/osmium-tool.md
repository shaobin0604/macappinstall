---
title: "Install osmium-tool on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Libosmium-based command-line tool for processing OpenStreetMap data"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install osmium-tool on MacOS using homebrew

- App Name: osmium-tool
- App description: Libosmium-based command-line tool for processing OpenStreetMap data
- App Version: 1.13.2
- App Website: https://osmcode.org/osmium-tool/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install osmium-tool with the following command
   ```
   brew install osmium-tool
   ```
4. osmium-tool is ready to use now!