---
title: "Install Spotter on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Productivity tool to launch everything"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Spotter on MacOS using homebrew

- App Name: Spotter
- App description: Productivity tool to launch everything
- App Version: 1.0.7
- App Website: https://github.com/spotter-application/spotter

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Spotter with the following command
   ```
   brew install --cask spotter
   ```
4. Spotter is ready to use now!