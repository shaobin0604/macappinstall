---
title: "Install spin on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Efficient verification tool of multi-threaded software"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install spin on MacOS using homebrew

- App Name: spin
- App description: Efficient verification tool of multi-threaded software
- App Version: 6.5.2
- App Website: https://spinroot.com/spin/whatispin.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install spin with the following command
   ```
   brew install spin
   ```
4. spin is ready to use now!