---
title: "Install SSH Config Editor on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool for managing the OpenSSH ssh client configuration file"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install SSH Config Editor on MacOS using homebrew

- App Name: SSH Config Editor
- App description: Tool for managing the OpenSSH ssh client configuration file
- App Version: 2.3.2,91
- App Website: https://www.hejki.org/ssheditor/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install SSH Config Editor with the following command
   ```
   brew install --cask ssh-config-editor
   ```
4. SSH Config Editor is ready to use now!