---
title: "Install F-Secure Freedome on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "VPN client"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install F-Secure Freedome on MacOS using homebrew

- App Name: F-Secure Freedome
- App description: VPN client
- App Version: 2.43.809.0
- App Website: https://www.f-secure.com/en_US/web/home_us/freedome

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install F-Secure Freedome with the following command
   ```
   brew install --cask freedome
   ```
4. F-Secure Freedome is ready to use now!