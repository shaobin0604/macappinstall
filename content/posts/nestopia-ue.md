---
title: "Install nestopia-ue on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "NES emulator"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install nestopia-ue on MacOS using homebrew

- App Name: nestopia-ue
- App description: NES emulator
- App Version: 1.51.1
- App Website: http://0ldsk00l.ca/nestopia/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install nestopia-ue with the following command
   ```
   brew install nestopia-ue
   ```
4. nestopia-ue is ready to use now!