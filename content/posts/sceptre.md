---
title: "Install sceptre on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Build better AWS infrastructure"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sceptre on MacOS using homebrew

- App Name: sceptre
- App description: Build better AWS infrastructure
- App Version: 2.7.1
- App Website: https://sceptre.cloudreach.com

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sceptre with the following command
   ```
   brew install sceptre
   ```
4. sceptre is ready to use now!