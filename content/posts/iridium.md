---
title: "Install Iridium Browser on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Web browser focusing on security and privacy"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Iridium Browser on MacOS using homebrew

- App Name: Iridium Browser
- App description: Web browser focusing on security and privacy
- App Version: 2021.12.96
- App Website: https://iridiumbrowser.de/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Iridium Browser with the following command
   ```
   brew install --cask iridium
   ```
4. Iridium Browser is ready to use now!