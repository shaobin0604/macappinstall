---
title: "Install colortail on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Like tail(1), but with various colors for specified output"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install colortail on MacOS using homebrew

- App Name: colortail
- App description: Like tail(1), but with various colors for specified output
- App Version: 0.3.4
- App Website: https://github.com/joakim666/colortail

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install colortail with the following command
   ```
   brew install colortail
   ```
4. colortail is ready to use now!