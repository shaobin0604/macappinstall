---
title: "Install acl2 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Logic and programming language in which you can model computer systems"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install acl2 on MacOS using homebrew

- App Name: acl2
- App description: Logic and programming language in which you can model computer systems
- App Version: 8.3
- App Website: https://www.cs.utexas.edu/users/moore/acl2/index.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install acl2 with the following command
   ```
   brew install acl2
   ```
4. acl2 is ready to use now!