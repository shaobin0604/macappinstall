---
title: "Install authoscope on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Scriptable network authentication cracker"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install authoscope on MacOS using homebrew

- App Name: authoscope
- App description: Scriptable network authentication cracker
- App Version: 0.8.0
- App Website: https://github.com/kpcyrd/authoscope

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install authoscope with the following command
   ```
   brew install authoscope
   ```
4. authoscope is ready to use now!