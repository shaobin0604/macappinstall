---
title: "Install Remix IDE desktop on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Desktop version of Remix web IDE used for Ethereum smart contract development"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Remix IDE desktop on MacOS using homebrew

- App Name: Remix IDE desktop
- App description: Desktop version of Remix web IDE used for Ethereum smart contract development
- App Version: 1.3.3
- App Website: https://remix-project.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Remix IDE desktop with the following command
   ```
   brew install --cask remix-ide
   ```
4. Remix IDE desktop is ready to use now!