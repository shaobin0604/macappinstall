---
title: "Install launchdns on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Mini DNS server designed solely to route queries to localhost"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install launchdns on MacOS using homebrew

- App Name: launchdns
- App description: Mini DNS server designed solely to route queries to localhost
- App Version: 1.0.4
- App Website: https://github.com/josh/launchdns

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install launchdns with the following command
   ```
   brew install launchdns
   ```
4. launchdns is ready to use now!