---
title: "Install Jumpshare on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "File sharing, screen recording, and screenshot capture app"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Jumpshare on MacOS using homebrew

- App Name: Jumpshare
- App description: File sharing, screen recording, and screenshot capture app
- App Version: 3.1.3,121
- App Website: https://jumpshare.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Jumpshare with the following command
   ```
   brew install --cask jumpshare
   ```
4. Jumpshare is ready to use now!