---
title: "Install osmcoastline on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Extracts coastline data from OpenStreetMap planet file"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install osmcoastline on MacOS using homebrew

- App Name: osmcoastline
- App description: Extracts coastline data from OpenStreetMap planet file
- App Version: 2.3.1
- App Website: https://osmcode.org/osmcoastline/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install osmcoastline with the following command
   ```
   brew install osmcoastline
   ```
4. osmcoastline is ready to use now!