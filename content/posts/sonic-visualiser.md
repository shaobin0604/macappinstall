---
title: "Install Sonic Visualiser on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Visualisation, analysis, and annotation of music audio recordings"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Sonic Visualiser on MacOS using homebrew

- App Name: Sonic Visualiser
- App description: Visualisation, analysis, and annotation of music audio recordings
- App Version: 4.4,2813
- App Website: https://www.sonicvisualiser.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Sonic Visualiser with the following command
   ```
   brew install --cask sonic-visualiser
   ```
4. Sonic Visualiser is ready to use now!