---
title: "Install samtools on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tools for manipulating next-generation sequencing data"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install samtools on MacOS using homebrew

- App Name: samtools
- App description: Tools for manipulating next-generation sequencing data
- App Version: 1.14
- App Website: https://www.htslib.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install samtools with the following command
   ```
   brew install samtools
   ```
4. samtools is ready to use now!