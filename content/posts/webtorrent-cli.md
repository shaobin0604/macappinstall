---
title: "Install webtorrent-cli on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line streaming torrent client"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install webtorrent-cli on MacOS using homebrew

- App Name: webtorrent-cli
- App description: Command-line streaming torrent client
- App Version: 4.0.3
- App Website: https://webtorrent.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install webtorrent-cli with the following command
   ```
   brew install webtorrent-cli
   ```
4. webtorrent-cli is ready to use now!