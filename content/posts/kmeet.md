---
title: "Install kMeet on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Client for the kMeet videoconferencing solution"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install kMeet on MacOS using homebrew

- App Name: kMeet
- App description: Client for the kMeet videoconferencing solution
- App Version: 1.1.37
- App Website: https://kmeet.infomaniak.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install kMeet with the following command
   ```
   brew install --cask kmeet
   ```
4. kMeet is ready to use now!