---
title: "Install Ricochet on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Anonymous peer-to-peer instant messaging"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Ricochet on MacOS using homebrew

- App Name: Ricochet
- App description: Anonymous peer-to-peer instant messaging
- App Version: 1.1.4.1
- App Website: https://ricochet.im/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Ricochet with the following command
   ```
   brew install --cask ricochet
   ```
4. Ricochet is ready to use now!