---
title: "Install Cleartext on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Text editor"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Cleartext on MacOS using homebrew

- App Name: Cleartext
- App description: Text editor
- App Version: 2.45
- App Website: https://github.com/mortenjust/cleartext-mac

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Cleartext with the following command
   ```
   brew install --cask cleartext
   ```
4. Cleartext is ready to use now!