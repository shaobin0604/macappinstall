---
title: "Install Restream Chat on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Keep your streaming chats in one place"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Restream Chat on MacOS using homebrew

- App Name: Restream Chat
- App description: Keep your streaming chats in one place
- App Version: 2.5.4-beta
- App Website: https://restream.io/chat/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Restream Chat with the following command
   ```
   brew install --cask restream-chat
   ```
4. Restream Chat is ready to use now!