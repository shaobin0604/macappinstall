---
title: "Install git-credential-manager on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Stores Git credentials for Visual Studio Team Services"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install git-credential-manager on MacOS using homebrew

- App Name: git-credential-manager
- App description: Stores Git credentials for Visual Studio Team Services
- App Version: 2.0.4
- App Website: https://docs.microsoft.com/vsts/git/set-up-credential-managers

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install git-credential-manager with the following command
   ```
   brew install git-credential-manager
   ```
4. git-credential-manager is ready to use now!