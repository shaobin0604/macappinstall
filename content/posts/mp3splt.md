---
title: "Install mp3splt on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Command-line interface to split MP3 and Ogg Vorbis files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mp3splt on MacOS using homebrew

- App Name: mp3splt
- App description: Command-line interface to split MP3 and Ogg Vorbis files
- App Version: 2.6.2
- App Website: https://mp3splt.sourceforge.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mp3splt with the following command
   ```
   brew install mp3splt
   ```
4. mp3splt is ready to use now!