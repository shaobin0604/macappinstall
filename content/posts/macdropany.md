---
title: "Install MacDropAny on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Syncs any local folder with the cloud"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install MacDropAny on MacOS using homebrew

- App Name: MacDropAny
- App description: Syncs any local folder with the cloud
- App Version: 3.0.2
- App Website: https://github.com/sebthedev/MacDropAny

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install MacDropAny with the following command
   ```
   brew install --cask macdropany
   ```
4. MacDropAny is ready to use now!