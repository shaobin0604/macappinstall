---
title: "Install OBS on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Open-source software for live streaming and screen recording"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install OBS on MacOS using homebrew

- App Name: OBS
- App description: Open-source software for live streaming and screen recording
- App Version: 27.2
- App Website: https://obsproject.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install OBS with the following command
   ```
   brew install --cask obs
   ```
4. OBS is ready to use now!