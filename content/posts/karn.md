---
title: "Install karn on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Manage multiple Git identities"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install karn on MacOS using homebrew

- App Name: karn
- App description: Manage multiple Git identities
- App Version: 0.0.5
- App Website: https://github.com/prydonius/karn

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install karn with the following command
   ```
   brew install karn
   ```
4. karn is ready to use now!