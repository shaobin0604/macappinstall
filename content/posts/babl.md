---
title: "Install babl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Dynamic, any-to-any, pixel format translation library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install babl on MacOS using homebrew

- App Name: babl
- App description: Dynamic, any-to-any, pixel format translation library
- App Version: 0.1.88
- App Website: https://www.gegl.org/babl/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install babl with the following command
   ```
   brew install babl
   ```
4. babl is ready to use now!