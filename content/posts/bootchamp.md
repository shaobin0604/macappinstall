---
title: "Install BootChamp on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Menu bar app to boot into your Boot Camp Windows partition"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install BootChamp on MacOS using homebrew

- App Name: BootChamp
- App description: Menu bar app to boot into your Boot Camp Windows partition
- App Version: 1.7
- App Website: https://kainjow.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install BootChamp with the following command
   ```
   brew install --cask bootchamp
   ```
4. BootChamp is ready to use now!