---
title: "Install ocrad on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Optical character recognition (OCR) program"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ocrad on MacOS using homebrew

- App Name: ocrad
- App description: Optical character recognition (OCR) program
- App Version: 0.28
- App Website: https://www.gnu.org/software/ocrad/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ocrad with the following command
   ```
   brew install ocrad
   ```
4. ocrad is ready to use now!