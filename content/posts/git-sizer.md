---
title: "Install git-sizer on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Compute various size metrics for a Git repository"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install git-sizer on MacOS using homebrew

- App Name: git-sizer
- App description: Compute various size metrics for a Git repository
- App Version: 1.5.0
- App Website: https://github.com/github/git-sizer

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install git-sizer with the following command
   ```
   brew install git-sizer
   ```
4. git-sizer is ready to use now!