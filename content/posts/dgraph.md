---
title: "Install dgraph on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Fast, Distributed Graph DB"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dgraph on MacOS using homebrew

- App Name: dgraph
- App description: Fast, Distributed Graph DB
- App Version: 20.11.3
- App Website: https://dgraph.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dgraph with the following command
   ```
   brew install dgraph
   ```
4. dgraph is ready to use now!