---
title: "Install infra on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Kubernetes desktop client"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install infra on MacOS using homebrew

- App Name: infra
- App description: Kubernetes desktop client
- App Version: 0.46.0
- App Website: https://infra.app/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install infra with the following command
   ```
   brew install --cask infra
   ```
4. infra is ready to use now!