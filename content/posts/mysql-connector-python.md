---
title: "Install MySQL Connector for Python on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Self-contained Python driver for communicating with MySQL servers"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install MySQL Connector for Python on MacOS using homebrew

- App Name: MySQL Connector for Python
- App description: Self-contained Python driver for communicating with MySQL servers
- App Version: 8.0.23
- App Website: https://dev.mysql.com/downloads/connector/python/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install MySQL Connector for Python with the following command
   ```
   brew install --cask mysql-connector-python
   ```
4. MySQL Connector for Python is ready to use now!