---
title: "Install chmlib on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library for dealing with Microsoft ITSS/CHM files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install chmlib on MacOS using homebrew

- App Name: chmlib
- App description: Library for dealing with Microsoft ITSS/CHM files
- App Version: 0.40
- App Website: http://www.jedrea.com/chmlib/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install chmlib with the following command
   ```
   brew install chmlib
   ```
4. chmlib is ready to use now!