---
title: "Install screen on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Terminal multiplexer with VT100/ANSI terminal emulation"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install screen on MacOS using homebrew

- App Name: screen
- App description: Terminal multiplexer with VT100/ANSI terminal emulation
- App Version: 4.9.0
- App Website: https://www.gnu.org/software/screen

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install screen with the following command
   ```
   brew install screen
   ```
4. screen is ready to use now!