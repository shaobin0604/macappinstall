---
title: "Install inspectrum on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Offline radio signal analyser"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install inspectrum on MacOS using homebrew

- App Name: inspectrum
- App description: Offline radio signal analyser
- App Version: 0.2.3
- App Website: https://github.com/miek/inspectrum

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install inspectrum with the following command
   ```
   brew install inspectrum
   ```
4. inspectrum is ready to use now!