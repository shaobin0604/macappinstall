---
title: "Install atkmm@2.28 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Official C++ interface for the ATK accessibility toolkit library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install atkmm@2.28 on MacOS using homebrew

- App Name: atkmm@2.28
- App description: Official C++ interface for the ATK accessibility toolkit library
- App Version: 2.28.2
- App Website: https://www.gtkmm.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install atkmm@2.28 with the following command
   ```
   brew install atkmm@2.28
   ```
4. atkmm@2.28 is ready to use now!