---
title: "Install crc32c on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Implementation of CRC32C with CPU-specific acceleration"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install crc32c on MacOS using homebrew

- App Name: crc32c
- App description: Implementation of CRC32C with CPU-specific acceleration
- App Version: 1.1.2
- App Website: https://github.com/google/crc32c

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install crc32c with the following command
   ```
   brew install crc32c
   ```
4. crc32c is ready to use now!