---
title: "Install zeromq on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "High-performance, asynchronous messaging library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install zeromq on MacOS using homebrew

- App Name: zeromq
- App description: High-performance, asynchronous messaging library
- App Version: 4.3.4
- App Website: https://zeromq.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install zeromq with the following command
   ```
   brew install zeromq
   ```
4. zeromq is ready to use now!