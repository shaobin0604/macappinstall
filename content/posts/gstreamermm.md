---
title: "Install gstreamermm on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GStreamer C++ bindings"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gstreamermm on MacOS using homebrew

- App Name: gstreamermm
- App description: GStreamer C++ bindings
- App Version: 1.10.0
- App Website: https://gstreamer.freedesktop.org/bindings/cplusplus.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gstreamermm with the following command
   ```
   brew install gstreamermm
   ```
4. gstreamermm is ready to use now!