---
title: "Install WordPress.com on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "WordPress client"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install WordPress.com on MacOS using homebrew

- App Name: WordPress.com
- App description: WordPress client
- App Version: 7.2.0
- App Website: https://apps.wordpress.com/desktop/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install WordPress.com with the following command
   ```
   brew install --cask wordpresscom
   ```
4. WordPress.com is ready to use now!