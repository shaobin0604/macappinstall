---
title: "Install BlueJ on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Java Development Environment designed for begginers"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install BlueJ on MacOS using homebrew

- App Name: BlueJ
- App description: Java Development Environment designed for begginers
- App Version: 5.0.2a
- App Website: https://www.bluej.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install BlueJ with the following command
   ```
   brew install --cask bluej
   ```
4. BlueJ is ready to use now!