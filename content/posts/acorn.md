---
title: "Install Acorn on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Image editor focused on simplicity"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Acorn on MacOS using homebrew

- App Name: Acorn
- App description: Image editor focused on simplicity
- App Version: 7.1.2,15994
- App Website: https://flyingmeat.com/acorn/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Acorn with the following command
   ```
   brew install --cask acorn
   ```
4. Acorn is ready to use now!