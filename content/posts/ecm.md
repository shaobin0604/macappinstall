---
title: "Install ecm on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Prepare CD image files so they compress better"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ecm on MacOS using homebrew

- App Name: ecm
- App description: Prepare CD image files so they compress better
- App Version: 1.0
- App Website: https://web.archive.org/web/20140227165748/www.neillcorlett.com/ecm/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ecm with the following command
   ```
   brew install ecm
   ```
4. ecm is ready to use now!