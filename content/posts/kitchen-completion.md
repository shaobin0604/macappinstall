---
title: "Install kitchen-completion on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Bash completion for Kitchen"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install kitchen-completion on MacOS using homebrew

- App Name: kitchen-completion
- App description: Bash completion for Kitchen
- App Version: 1.0.0
- App Website: https://github.com/MarkBorcherding/test-kitchen-bash-completion

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install kitchen-completion with the following command
   ```
   brew install kitchen-completion
   ```
4. kitchen-completion is ready to use now!