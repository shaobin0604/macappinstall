---
title: "Install java-service-wrapper on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Simplify the deployment, launch and monitoring of Java applications"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install java-service-wrapper on MacOS using homebrew

- App Name: java-service-wrapper
- App description: Simplify the deployment, launch and monitoring of Java applications
- App Version: 3.5.48
- App Website: https://wrapper.tanukisoftware.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install java-service-wrapper with the following command
   ```
   brew install java-service-wrapper
   ```
4. java-service-wrapper is ready to use now!