---
title: "Install VLC media player on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Multimedia player"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install VLC media player on MacOS using homebrew

- App Name: VLC media player
- App description: Multimedia player
- App Version: 3.0.16
- App Website: https://www.videolan.org/vlc/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install VLC media player with the following command
   ```
   brew install --cask vlc
   ```
4. VLC media player is ready to use now!