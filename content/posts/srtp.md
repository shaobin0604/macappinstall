---
title: "Install srtp on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Implementation of the Secure Real-time Transport Protocol"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install srtp on MacOS using homebrew

- App Name: srtp
- App description: Implementation of the Secure Real-time Transport Protocol
- App Version: 2.4.2
- App Website: https://github.com/cisco/libsrtp

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install srtp with the following command
   ```
   brew install srtp
   ```
4. srtp is ready to use now!