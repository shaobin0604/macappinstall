---
title: "Install watchexec on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Execute commands when watched files change"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install watchexec on MacOS using homebrew

- App Name: watchexec
- App description: Execute commands when watched files change
- App Version: 1.18.6
- App Website: https://github.com/watchexec/watchexec

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install watchexec with the following command
   ```
   brew install watchexec
   ```
4. watchexec is ready to use now!