---
title: "Install Disk Expert on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Disk space analyzer"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Disk Expert on MacOS using homebrew

- App Name: Disk Expert
- App description: Disk space analyzer
- App Version: 3.6.3,361
- App Website: https://nektony.com/disk-expert

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Disk Expert with the following command
   ```
   brew install --cask disk-expert
   ```
4. Disk Expert is ready to use now!