---
title: "Install RemNote on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Spaced-repetition powered note-taking tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install RemNote on MacOS using homebrew

- App Name: RemNote
- App description: Spaced-repetition powered note-taking tool
- App Version: 1.5.3
- App Website: https://www.remnote.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install RemNote with the following command
   ```
   brew install --cask remnote
   ```
4. RemNote is ready to use now!