---
title: "Install mad on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "MPEG audio decoder"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mad on MacOS using homebrew

- App Name: mad
- App description: MPEG audio decoder
- App Version: 0.15.1b
- App Website: https://www.underbit.com/products/mad/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mad with the following command
   ```
   brew install mad
   ```
4. mad is ready to use now!