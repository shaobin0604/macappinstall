---
title: "Install mps-youtube on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Terminal based YouTube player and downloader"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mps-youtube on MacOS using homebrew

- App Name: mps-youtube
- App description: Terminal based YouTube player and downloader
- App Version: 0.2.8
- App Website: https://github.com/mps-youtube/mps-youtube

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mps-youtube with the following command
   ```
   brew install mps-youtube
   ```
4. mps-youtube is ready to use now!