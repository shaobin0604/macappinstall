---
title: "Install katago on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Neural Network Go engine with no human-provided knowledge"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install katago on MacOS using homebrew

- App Name: katago
- App description: Neural Network Go engine with no human-provided knowledge
- App Version: 1.10.0
- App Website: https://github.com/lightvector/KataGo

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install katago with the following command
   ```
   brew install katago
   ```
4. katago is ready to use now!