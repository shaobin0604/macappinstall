---
title: "Install Facebook Messenger on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Native desktop app for Messenger (formerly Facebook Messenger)"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Facebook Messenger on MacOS using homebrew

- App Name: Facebook Messenger
- App description: Native desktop app for Messenger (formerly Facebook Messenger)
- App Version: 140.4.112,351062954
- App Website: https://www.messenger.com/desktop

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Facebook Messenger with the following command
   ```
   brew install --cask messenger
   ```
4. Facebook Messenger is ready to use now!