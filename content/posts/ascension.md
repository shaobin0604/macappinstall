---
title: "Install Ascension on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "ANSI/ASCII art viewer"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Ascension on MacOS using homebrew

- App Name: Ascension
- App description: ANSI/ASCII art viewer
- App Version: 3.0.0
- App Website: https://github.com/ansilove/Ascension

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Ascension with the following command
   ```
   brew install --cask ascension
   ```
4. Ascension is ready to use now!