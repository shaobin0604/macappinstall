---
title: "Install payload-dumper-go on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Android OTA payload dumper written in Go"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install payload-dumper-go on MacOS using homebrew

- App Name: payload-dumper-go
- App description: Android OTA payload dumper written in Go
- App Version: 1.2.0
- App Website: https://github.com/ssut/payload-dumper-go

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install payload-dumper-go with the following command
   ```
   brew install payload-dumper-go
   ```
4. payload-dumper-go is ready to use now!