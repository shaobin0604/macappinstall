---
title: "Install libbladerf on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "USB 3.0 Superspeed Software Defined Radio Source"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libbladerf on MacOS using homebrew

- App Name: libbladerf
- App description: USB 3.0 Superspeed Software Defined Radio Source
- App Version: 2021.10
- App Website: https://nuand.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libbladerf with the following command
   ```
   brew install libbladerf
   ```
4. libbladerf is ready to use now!