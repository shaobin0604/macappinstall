---
title: "Install fork-cleaner on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cleans up old and inactive forks on your GitHub account"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fork-cleaner on MacOS using homebrew

- App Name: fork-cleaner
- App description: Cleans up old and inactive forks on your GitHub account
- App Version: 2.2.1
- App Website: https://github.com/caarlos0/fork-cleaner

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fork-cleaner with the following command
   ```
   brew install fork-cleaner
   ```
4. fork-cleaner is ready to use now!