---
title: "Install git-ftp on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Git-powered FTP client"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install git-ftp on MacOS using homebrew

- App Name: git-ftp
- App description: Git-powered FTP client
- App Version: 1.6.0
- App Website: https://git-ftp.github.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install git-ftp with the following command
   ```
   brew install git-ftp
   ```
4. git-ftp is ready to use now!