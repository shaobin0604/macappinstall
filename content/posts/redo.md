---
title: "Install redo on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Implements djb's redo: an alternative to make"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install redo on MacOS using homebrew

- App Name: redo
- App description: Implements djb's redo: an alternative to make
- App Version: 0.42d
- App Website: https://redo.rtfd.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install redo with the following command
   ```
   brew install redo
   ```
4. redo is ready to use now!