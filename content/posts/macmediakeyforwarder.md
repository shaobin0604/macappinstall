---
title: "Install Mac Media Key Forwarder on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Media key forwarder for iTunes and Spotify"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Mac Media Key Forwarder on MacOS using homebrew

- App Name: Mac Media Key Forwarder
- App description: Media key forwarder for iTunes and Spotify
- App Version: 3.1
- App Website: http://milgra.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Mac Media Key Forwarder with the following command
   ```
   brew install --cask macmediakeyforwarder
   ```
4. Mac Media Key Forwarder is ready to use now!