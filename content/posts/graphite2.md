---
title: "Install graphite2 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Smart font renderer for non-Roman scripts"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install graphite2 on MacOS using homebrew

- App Name: graphite2
- App description: Smart font renderer for non-Roman scripts
- App Version: 1.3.14
- App Website: https://graphite.sil.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install graphite2 with the following command
   ```
   brew install graphite2
   ```
4. graphite2 is ready to use now!