---
title: "Install fanyi on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Mandarin and english translate tool in your command-line"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fanyi on MacOS using homebrew

- App Name: fanyi
- App description: Mandarin and english translate tool in your command-line
- App Version: 5.1.1
- App Website: https://github.com/afc163/fanyi

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fanyi with the following command
   ```
   brew install fanyi
   ```
4. fanyi is ready to use now!