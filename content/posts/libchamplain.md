---
title: "Install libchamplain on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "ClutterActor for displaying maps"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libchamplain on MacOS using homebrew

- App Name: libchamplain
- App description: ClutterActor for displaying maps
- App Version: 0.12.20
- App Website: https://wiki.gnome.org/Projects/libchamplain

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libchamplain with the following command
   ```
   brew install libchamplain
   ```
4. libchamplain is ready to use now!