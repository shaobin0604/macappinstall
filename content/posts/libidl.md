---
title: "Install libidl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library for creating CORBA IDL files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libidl on MacOS using homebrew

- App Name: libidl
- App description: Library for creating CORBA IDL files
- App Version: 0.8.14
- App Website: https://ftp.acc.umu.se/pub/gnome/sources/libIDL/0.8/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libidl with the following command
   ```
   brew install libidl
   ```
4. libidl is ready to use now!