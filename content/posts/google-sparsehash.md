---
title: "Install google-sparsehash on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Extremely memory-efficient hash_map implementation"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install google-sparsehash on MacOS using homebrew

- App Name: google-sparsehash
- App description: Extremely memory-efficient hash_map implementation
- App Version: 2.0.4
- App Website: https://github.com/sparsehash/sparsehash

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install google-sparsehash with the following command
   ```
   brew install google-sparsehash
   ```
4. google-sparsehash is ready to use now!