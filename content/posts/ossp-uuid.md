---
title: "Install ossp-uuid on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "ISO-C API and CLI for generating UUIDs"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ossp-uuid on MacOS using homebrew

- App Name: ossp-uuid
- App description: ISO-C API and CLI for generating UUIDs
- App Version: 1.6.2
- App Website: https://web.archive.org/web/www.ossp.org/pkg/lib/uuid/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ossp-uuid with the following command
   ```
   brew install ossp-uuid
   ```
4. ossp-uuid is ready to use now!