---
title: "Install taglib on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Audio metadata library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install taglib on MacOS using homebrew

- App Name: taglib
- App description: Audio metadata library
- App Version: 1.12
- App Website: https://taglib.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install taglib with the following command
   ```
   brew install taglib
   ```
4. taglib is ready to use now!