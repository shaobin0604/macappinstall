---
title: "Install libimagequant on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Palette quantization library extracted from pnquant2"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libimagequant on MacOS using homebrew

- App Name: libimagequant
- App description: Palette quantization library extracted from pnquant2
- App Version: 4.0.0
- App Website: https://pngquant.org/lib/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libimagequant with the following command
   ```
   brew install libimagequant
   ```
4. libimagequant is ready to use now!