---
title: "Install LibrePCB on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "EDA software to develop printed circuit boards"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install LibrePCB on MacOS using homebrew

- App Name: LibrePCB
- App description: EDA software to develop printed circuit boards
- App Version: 0.1.6
- App Website: https://librepcb.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install LibrePCB with the following command
   ```
   brew install --cask librepcb
   ```
4. LibrePCB is ready to use now!