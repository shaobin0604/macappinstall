---
title: "Install UltraStar Deluxe on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Karaoke game"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install UltraStar Deluxe on MacOS using homebrew

- App Name: UltraStar Deluxe
- App description: Karaoke game
- App Version: 2020.4.0
- App Website: https://usdx.eu/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install UltraStar Deluxe with the following command
   ```
   brew install --cask ultrastardeluxe
   ```
4. UltraStar Deluxe is ready to use now!