---
title: "Install juju-wait on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Juju plugin for waiting for deployments to settle"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install juju-wait on MacOS using homebrew

- App Name: juju-wait
- App description: Juju plugin for waiting for deployments to settle
- App Version: 2.8.4
- App Website: https://launchpad.net/juju-wait

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install juju-wait with the following command
   ```
   brew install juju-wait
   ```
4. juju-wait is ready to use now!