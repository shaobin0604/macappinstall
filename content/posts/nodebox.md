---
title: "Install NodeBox on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Node-based data application for visualization and generative design"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install NodeBox on MacOS using homebrew

- App Name: NodeBox
- App description: Node-based data application for visualization and generative design
- App Version: 3.0.52
- App Website: https://www.nodebox.net/node/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install NodeBox with the following command
   ```
   brew install --cask nodebox
   ```
4. NodeBox is ready to use now!