---
title: "Install scap-workbench on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "SCAP Scanner And Tailoring Graphical User Interface"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install scap-workbench on MacOS using homebrew

- App Name: scap-workbench
- App description: SCAP Scanner And Tailoring Graphical User Interface
- App Version: 1.2.1
- App Website: https://www.open-scap.org/tools/scap-workbench/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install scap-workbench with the following command
   ```
   brew install --cask scap-workbench
   ```
4. scap-workbench is ready to use now!