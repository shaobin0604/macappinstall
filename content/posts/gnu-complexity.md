---
title: "Install gnu-complexity on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Measures complexity of C source"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gnu-complexity on MacOS using homebrew

- App Name: gnu-complexity
- App description: Measures complexity of C source
- App Version: 1.10
- App Website: https://www.gnu.org/software/complexity

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gnu-complexity with the following command
   ```
   brew install gnu-complexity
   ```
4. gnu-complexity is ready to use now!