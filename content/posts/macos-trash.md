---
title: "Install macos-trash on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Move files and folders to the trash"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install macos-trash on MacOS using homebrew

- App Name: macos-trash
- App description: Move files and folders to the trash
- App Version: 1.2.0
- App Website: https://github.com/sindresorhus/macos-trash

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install macos-trash with the following command
   ```
   brew install macos-trash
   ```
4. macos-trash is ready to use now!