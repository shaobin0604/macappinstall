---
title: "Install murex on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Bash-like shell designed for greater command-line productivity and safer scripts"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install murex on MacOS using homebrew

- App Name: murex
- App description: Bash-like shell designed for greater command-line productivity and safer scripts
- App Version: 2.5.2020
- App Website: https://murex.rocks

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install murex with the following command
   ```
   brew install murex
   ```
4. murex is ready to use now!