---
title: "Install jvmtop on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Console application for monitoring all running JVMs on a machine"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install jvmtop on MacOS using homebrew

- App Name: jvmtop
- App description: Console application for monitoring all running JVMs on a machine
- App Version: 0.8.0
- App Website: https://github.com/patric-r/jvmtop

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install jvmtop with the following command
   ```
   brew install jvmtop
   ```
4. jvmtop is ready to use now!