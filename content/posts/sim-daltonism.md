---
title: "Install Sim Daltonism on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Color blindness simulator for videos and images"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Sim Daltonism on MacOS using homebrew

- App Name: Sim Daltonism
- App description: Color blindness simulator for videos and images
- App Version: 2.0.5
- App Website: https://michelf.ca/projects/mac/sim-daltonism/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Sim Daltonism with the following command
   ```
   brew install --cask sim-daltonism
   ```
4. Sim Daltonism is ready to use now!