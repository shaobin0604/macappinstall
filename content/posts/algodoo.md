---
title: "Install Algodoo on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Draw and interact with physical systems"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Algodoo on MacOS using homebrew

- App Name: Algodoo
- App description: Draw and interact with physical systems
- App Version: 2.1.3
- App Website: https://www.algodoo.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Algodoo with the following command
   ```
   brew install --cask algodoo
   ```
4. Algodoo is ready to use now!