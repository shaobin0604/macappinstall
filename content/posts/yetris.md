---
title: "Install yetris on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Customizable Tetris for the terminal"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install yetris on MacOS using homebrew

- App Name: yetris
- App description: Customizable Tetris for the terminal
- App Version: 2.3.0
- App Website: https://github.com/alexdantas/yetris

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install yetris with the following command
   ```
   brew install yetris
   ```
4. yetris is ready to use now!