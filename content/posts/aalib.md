---
title: "Install aalib on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Portable ASCII art graphics library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install aalib on MacOS using homebrew

- App Name: aalib
- App description: Portable ASCII art graphics library
- App Version: 1.4rc5
- App Website: https://aa-project.sourceforge.io/aalib/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install aalib with the following command
   ```
   brew install aalib
   ```
4. aalib is ready to use now!