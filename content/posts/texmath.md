---
title: "Install texmath on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Haskell library for converting LaTeX math to MathML"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install texmath on MacOS using homebrew

- App Name: texmath
- App description: Haskell library for converting LaTeX math to MathML
- App Version: 0.12.4
- App Website: https://johnmacfarlane.net/texmath.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install texmath with the following command
   ```
   brew install texmath
   ```
4. texmath is ready to use now!