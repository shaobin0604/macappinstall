---
title: "Install mdbook on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Create modern online books from Markdown files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mdbook on MacOS using homebrew

- App Name: mdbook
- App description: Create modern online books from Markdown files
- App Version: 0.4.15
- App Website: https://rust-lang.github.io/mdBook/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mdbook with the following command
   ```
   brew install mdbook
   ```
4. mdbook is ready to use now!