---
title: "Install gitlab-runner on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Official GitLab CI runner"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gitlab-runner on MacOS using homebrew

- App Name: gitlab-runner
- App description: Official GitLab CI runner
- App Version: 14.7.0
- App Website: https://gitlab.com/gitlab-org/gitlab-runner

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gitlab-runner with the following command
   ```
   brew install gitlab-runner
   ```
4. gitlab-runner is ready to use now!