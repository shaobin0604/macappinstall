---
title: "Install funcoeszz on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Dozens of command-line mini-applications (Portuguese)"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install funcoeszz on MacOS using homebrew

- App Name: funcoeszz
- App description: Dozens of command-line mini-applications (Portuguese)
- App Version: 21.1
- App Website: https://funcoeszz.net/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install funcoeszz with the following command
   ```
   brew install funcoeszz
   ```
4. funcoeszz is ready to use now!