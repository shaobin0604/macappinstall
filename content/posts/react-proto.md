---
title: "Install React Proto on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "React application prototyping tool for developers and designers"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install React Proto on MacOS using homebrew

- App Name: React Proto
- App description: React application prototyping tool for developers and designers
- App Version: 1.0.0
- App Website: https://react-proto.github.io/react-proto

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install React Proto with the following command
   ```
   brew install --cask react-proto
   ```
4. React Proto is ready to use now!