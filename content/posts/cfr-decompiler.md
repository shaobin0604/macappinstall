---
title: "Install cfr-decompiler on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Yet Another Java Decompiler"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cfr-decompiler on MacOS using homebrew

- App Name: cfr-decompiler
- App description: Yet Another Java Decompiler
- App Version: 0.152
- App Website: https://www.benf.org/other/cfr/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cfr-decompiler with the following command
   ```
   brew install cfr-decompiler
   ```
4. cfr-decompiler is ready to use now!