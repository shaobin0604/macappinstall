---
title: "Install ipinfo on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tool for calculation of IP networks"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ipinfo on MacOS using homebrew

- App Name: ipinfo
- App description: Tool for calculation of IP networks
- App Version: 1.2
- App Website: https://kyberdigi.cz/projects/ipinfo/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ipinfo with the following command
   ```
   brew install ipinfo
   ```
4. ipinfo is ready to use now!