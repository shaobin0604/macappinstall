---
title: "Install innotop on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Top clone for MySQL"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install innotop on MacOS using homebrew

- App Name: innotop
- App description: Top clone for MySQL
- App Version: 1.13.0
- App Website: https://github.com/innotop/innotop/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install innotop with the following command
   ```
   brew install innotop
   ```
4. innotop is ready to use now!