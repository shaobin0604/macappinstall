---
title: "Install pdfgrep on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Search PDFs for strings matching a regular expression"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pdfgrep on MacOS using homebrew

- App Name: pdfgrep
- App description: Search PDFs for strings matching a regular expression
- App Version: 2.1.2
- App Website: https://pdfgrep.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pdfgrep with the following command
   ```
   brew install pdfgrep
   ```
4. pdfgrep is ready to use now!