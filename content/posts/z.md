---
title: "Install z on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Tracks most-used directories to make cd smarter"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install z on MacOS using homebrew

- App Name: z
- App description: Tracks most-used directories to make cd smarter
- App Version: 1.9
- App Website: https://github.com/rupa/z

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install z with the following command
   ```
   brew install z
   ```
4. z is ready to use now!