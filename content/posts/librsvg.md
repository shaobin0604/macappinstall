---
title: "Install librsvg on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library to render SVG files using Cairo"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install librsvg on MacOS using homebrew

- App Name: librsvg
- App description: Library to render SVG files using Cairo
- App Version: 2.52.6
- App Website: https://wiki.gnome.org/Projects/LibRsvg

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install librsvg with the following command
   ```
   brew install librsvg
   ```
4. librsvg is ready to use now!