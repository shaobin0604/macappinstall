---
title: "Install meilisearch on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Ultra relevant, instant and typo-tolerant full-text search API"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install meilisearch on MacOS using homebrew

- App Name: meilisearch
- App description: Ultra relevant, instant and typo-tolerant full-text search API
- App Version: 0.25.2
- App Website: https://docs.meilisearch.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install meilisearch with the following command
   ```
   brew install meilisearch
   ```
4. meilisearch is ready to use now!