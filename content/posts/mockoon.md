---
title: "Install Mockoon on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Create mock APIs locally"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Mockoon on MacOS using homebrew

- App Name: Mockoon
- App description: Create mock APIs locally
- App Version: 1.17.0
- App Website: https://mockoon.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Mockoon with the following command
   ```
   brew install --cask mockoon
   ```
4. Mockoon is ready to use now!