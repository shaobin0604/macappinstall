---
title: "Install tmpreaper on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Clean up files in directories based on their age"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install tmpreaper on MacOS using homebrew

- App Name: tmpreaper
- App description: Clean up files in directories based on their age
- App Version: 1.6.14
- App Website: https://packages.debian.org/sid/tmpreaper

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install tmpreaper with the following command
   ```
   brew install tmpreaper
   ```
4. tmpreaper is ready to use now!