---
title: "Install Replacicon on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "App icon replacement utility"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Replacicon on MacOS using homebrew

- App Name: Replacicon
- App description: App icon replacement utility
- App Version: 1.3,20
- App Website: https://replacicon.app/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Replacicon with the following command
   ```
   brew install --cask replacicon
   ```
4. Replacicon is ready to use now!