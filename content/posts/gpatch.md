---
title: "Install gpatch on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Apply a diff file to an original"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gpatch on MacOS using homebrew

- App Name: gpatch
- App description: Apply a diff file to an original
- App Version: 2.7.6
- App Website: https://savannah.gnu.org/projects/patch/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gpatch with the following command
   ```
   brew install gpatch
   ```
4. gpatch is ready to use now!