---
title: "Install cppman on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C++ 98/11/14/17/20 manual pages from cplusplus.com and cppreference.com"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cppman on MacOS using homebrew

- App Name: cppman
- App description: C++ 98/11/14/17/20 manual pages from cplusplus.com and cppreference.com
- App Version: 0.5.3
- App Website: https://github.com/aitjcize/cppman

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cppman with the following command
   ```
   brew install cppman
   ```
4. cppman is ready to use now!