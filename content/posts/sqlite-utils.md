---
title: "Install sqlite-utils on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CLI utility for manipulating SQLite databases"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sqlite-utils on MacOS using homebrew

- App Name: sqlite-utils
- App description: CLI utility for manipulating SQLite databases
- App Version: 3.24
- App Website: https://sqlite-utils.datasette.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sqlite-utils with the following command
   ```
   brew install sqlite-utils
   ```
4. sqlite-utils is ready to use now!