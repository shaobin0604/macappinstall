---
title: "Install No-IP DUC on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Keeps current IP address in sync"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install No-IP DUC on MacOS using homebrew

- App Name: No-IP DUC
- App description: Keeps current IP address in sync
- App Version: 4.0.12,55.0
- App Website: https://www.noip.com/download?page=mac

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install No-IP DUC with the following command
   ```
   brew install --cask no-ip-duc
   ```
4. No-IP DUC is ready to use now!