---
title: "Install libspng on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C library for reading and writing PNG format files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libspng on MacOS using homebrew

- App Name: libspng
- App description: C library for reading and writing PNG format files
- App Version: 0.7.2
- App Website: https://libspng.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libspng with the following command
   ```
   brew install libspng
   ```
4. libspng is ready to use now!