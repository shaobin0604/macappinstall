---
title: "Install ssh-audit on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "SSH server & client auditing"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ssh-audit on MacOS using homebrew

- App Name: ssh-audit
- App description: SSH server & client auditing
- App Version: 2.5.0
- App Website: https://github.com/jtesta/ssh-audit

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ssh-audit with the following command
   ```
   brew install ssh-audit
   ```
4. ssh-audit is ready to use now!