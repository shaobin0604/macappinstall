---
title: "Install dos2unix on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Convert text between DOS, UNIX, and Mac formats"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install dos2unix on MacOS using homebrew

- App Name: dos2unix
- App description: Convert text between DOS, UNIX, and Mac formats
- App Version: 7.4.2
- App Website: https://waterlan.home.xs4all.nl/dos2unix.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install dos2unix with the following command
   ```
   brew install dos2unix
   ```
4. dos2unix is ready to use now!