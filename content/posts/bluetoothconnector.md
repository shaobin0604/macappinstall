---
title: "Install bluetoothconnector on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Connect and disconnect Bluetooth devices"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bluetoothconnector on MacOS using homebrew

- App Name: bluetoothconnector
- App description: Connect and disconnect Bluetooth devices
- App Version: 2.0.0
- App Website: https://github.com/lapfelix/BluetoothConnector

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bluetoothconnector with the following command
   ```
   brew install bluetoothconnector
   ```
4. bluetoothconnector is ready to use now!