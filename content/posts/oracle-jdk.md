---
title: "Install Oracle Java Standard Edition Development Kit on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "JDK from Oracle"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Oracle Java Standard Edition Development Kit on MacOS using homebrew

- App Name: Oracle Java Standard Edition Development Kit
- App description: JDK from Oracle
- App Version: 17.0.2
- App Website: https://www.oracle.com/java/technologies/downloads/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Oracle Java Standard Edition Development Kit with the following command
   ```
   brew install --cask oracle-jdk
   ```
4. Oracle Java Standard Edition Development Kit is ready to use now!