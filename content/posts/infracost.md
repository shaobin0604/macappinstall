---
title: "Install infracost on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cost estimates for Terraform"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install infracost on MacOS using homebrew

- App Name: infracost
- App description: Cost estimates for Terraform
- App Version: 0.9.18
- App Website: https://www.infracost.io/docs/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install infracost with the following command
   ```
   brew install infracost
   ```
4. infracost is ready to use now!