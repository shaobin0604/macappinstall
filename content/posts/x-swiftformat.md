---
title: "Install X-SwiftFormat on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Xcode extension to format Swift code"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install X-SwiftFormat on MacOS using homebrew

- App Name: X-SwiftFormat
- App description: Xcode extension to format Swift code
- App Version: 1.1.2
- App Website: https://github.com/ruiaureliano/X-SwiftFormat

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install X-SwiftFormat with the following command
   ```
   brew install --cask x-swiftformat
   ```
4. X-SwiftFormat is ready to use now!