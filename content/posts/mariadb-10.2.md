---
title: "Install mariadb@10.2 on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Drop-in replacement for MySQL"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mariadb@10.2 on MacOS using homebrew

- App Name: mariadb@10.2
- App description: Drop-in replacement for MySQL
- App Version: 10.2.41
- App Website: https://mariadb.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mariadb@10.2 with the following command
   ```
   brew install mariadb@10.2
   ```
4. mariadb@10.2 is ready to use now!