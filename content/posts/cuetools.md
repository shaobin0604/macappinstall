---
title: "Install cuetools on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utilities for .cue and .toc files"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cuetools on MacOS using homebrew

- App Name: cuetools
- App description: Utilities for .cue and .toc files
- App Version: 1.4.1
- App Website: https://github.com/svend/cuetools

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cuetools with the following command
   ```
   brew install cuetools
   ```
4. cuetools is ready to use now!