---
title: "Install TimeCamp on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Client application for TimeCamp software - track time and change tasks"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install TimeCamp on MacOS using homebrew

- App Name: TimeCamp
- App description: Client application for TimeCamp software - track time and change tasks
- App Version: 1.7.2.0
- App Website: https://www.timecamp.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install TimeCamp with the following command
   ```
   brew install --cask timecamp
   ```
4. TimeCamp is ready to use now!