---
title: "Install LANDrop on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Drop any files to any devices on your LAN"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install LANDrop on MacOS using homebrew

- App Name: LANDrop
- App description: Drop any files to any devices on your LAN
- App Version: 0.4.0
- App Website: https://landrop.app/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install LANDrop with the following command
   ```
   brew install --cask landrop
   ```
4. LANDrop is ready to use now!