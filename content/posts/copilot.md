---
title: "Install copilot on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CLI tool for Amazon ECS and AWS Fargate"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install copilot on MacOS using homebrew

- App Name: copilot
- App description: CLI tool for Amazon ECS and AWS Fargate
- App Version: 1.15.0
- App Website: https://aws.github.io/copilot-cli/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install copilot with the following command
   ```
   brew install copilot
   ```
4. copilot is ready to use now!