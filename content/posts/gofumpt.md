---
title: "Install gofumpt on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Stricter gofmt"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gofumpt on MacOS using homebrew

- App Name: gofumpt
- App description: Stricter gofmt
- App Version: 0.2.1
- App Website: https://github.com/mvdan/gofumpt

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gofumpt with the following command
   ```
   brew install gofumpt
   ```
4. gofumpt is ready to use now!