---
title: "Install Raspberry Pi Imager on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Imaging utility to install operating systems to a microSD card"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Raspberry Pi Imager on MacOS using homebrew

- App Name: Raspberry Pi Imager
- App description: Imaging utility to install operating systems to a microSD card
- App Version: 1.7.1
- App Website: https://www.raspberrypi.org/downloads/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Raspberry Pi Imager with the following command
   ```
   brew install --cask raspberry-pi-imager
   ```
4. Raspberry Pi Imager is ready to use now!