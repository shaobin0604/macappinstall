---
title: "Install gettext on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GNU internationalization (i18n) and localization (l10n) library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gettext on MacOS using homebrew

- App Name: gettext
- App description: GNU internationalization (i18n) and localization (l10n) library
- App Version: 0.21
- App Website: https://www.gnu.org/software/gettext/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gettext with the following command
   ```
   brew install gettext
   ```
4. gettext is ready to use now!