---
title: "Install gotests on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Automatically generate Go test boilerplate from your source code"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gotests on MacOS using homebrew

- App Name: gotests
- App description: Automatically generate Go test boilerplate from your source code
- App Version: 1.6.0
- App Website: https://github.com/cweill/gotests

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gotests with the following command
   ```
   brew install gotests
   ```
4. gotests is ready to use now!