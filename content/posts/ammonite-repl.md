---
title: "Install ammonite-repl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Ammonite is a cleanroom re-implementation of the Scala REPL"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ammonite-repl on MacOS using homebrew

- App Name: ammonite-repl
- App description: Ammonite is a cleanroom re-implementation of the Scala REPL
- App Version: 2.5.2
- App Website: https://ammonite.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ammonite-repl with the following command
   ```
   brew install ammonite-repl
   ```
4. ammonite-repl is ready to use now!