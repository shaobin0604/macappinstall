---
title: "Install kubebuilder on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "SDK for building Kubernetes APIs using CRDs"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install kubebuilder on MacOS using homebrew

- App Name: kubebuilder
- App description: SDK for building Kubernetes APIs using CRDs
- App Version: 3.3.0
- App Website: https://github.com/kubernetes-sigs/kubebuilder

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install kubebuilder with the following command
   ```
   brew install kubebuilder
   ```
4. kubebuilder is ready to use now!