---
title: "Install brew-php-switcher on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Switch Apache / Valet / CLI configs between PHP versions"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install brew-php-switcher on MacOS using homebrew

- App Name: brew-php-switcher
- App description: Switch Apache / Valet / CLI configs between PHP versions
- App Version: 2.4
- App Website: https://github.com/philcook/php-switcher

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install brew-php-switcher with the following command
   ```
   brew install brew-php-switcher
   ```
4. brew-php-switcher is ready to use now!