---
title: "Install crunch on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Wordlist generator"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install crunch on MacOS using homebrew

- App Name: crunch
- App description: Wordlist generator
- App Version: 3.6
- App Website: https://sourceforge.net/projects/crunch-wordlist/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install crunch with the following command
   ```
   brew install crunch
   ```
4. crunch is ready to use now!