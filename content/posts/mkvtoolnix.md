---
title: "Install mkvtoolnix on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Matroska media files manipulation tools"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install mkvtoolnix on MacOS using homebrew

- App Name: mkvtoolnix
- App description: Matroska media files manipulation tools
- App Version: 65.0.0
- App Website: https://mkvtoolnix.download/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install mkvtoolnix with the following command
   ```
   brew install mkvtoolnix
   ```
4. mkvtoolnix is ready to use now!