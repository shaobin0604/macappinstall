---
title: "Install ATOK on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Japanese input method editor (IME) produced by JustSystems"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ATOK on MacOS using homebrew

- App Name: ATOK
- App description: Japanese input method editor (IME) produced by JustSystems
- App Version: 2021,32.1.0,try3
- App Website: https://www.justsystems.com/jp/products/atokmac/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ATOK with the following command
   ```
   brew install --cask atok
   ```
4. ATOK is ready to use now!