---
title: "Install brew-pip on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Install pip packages as homebrew formulae"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install brew-pip on MacOS using homebrew

- App Name: brew-pip
- App description: Install pip packages as homebrew formulae
- App Version: 0.4.1
- App Website: https://github.com/hanxue/brew-pip

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install brew-pip with the following command
   ```
   brew install brew-pip
   ```
4. brew-pip is ready to use now!