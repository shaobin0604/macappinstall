---
title: "Install google-chat-electron on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Standalone app for Google Chat"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install google-chat-electron on MacOS using homebrew

- App Name: google-chat-electron
- App description: Standalone app for Google Chat
- App Version: 2.14.2
- App Website: https://github.com/ankurk91/google-chat-electron

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install google-chat-electron with the following command
   ```
   brew install --cask google-chat-electron
   ```
4. google-chat-electron is ready to use now!