---
title: "Install sgrep on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Search SGML, XML, and HTML"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sgrep on MacOS using homebrew

- App Name: sgrep
- App description: Search SGML, XML, and HTML
- App Version: 1.94a
- App Website: https://www.cs.helsinki.fi/u/jjaakkol/sgrep.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sgrep with the following command
   ```
   brew install sgrep
   ```
4. sgrep is ready to use now!