---
title: "Install Adobe AIR on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Framework used in the development of applications and games"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Adobe AIR on MacOS using homebrew

- App Name: Adobe AIR
- App description: Framework used in the development of applications and games
- App Version: 33.1.1.743
- App Website: https://airsdk.harman.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Adobe AIR with the following command
   ```
   brew install --cask adobe-air
   ```
4. Adobe AIR is ready to use now!