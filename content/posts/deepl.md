---
title: "Install DeepL on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Trains AIs to understand and translate texts"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install DeepL on MacOS using homebrew

- App Name: DeepL
- App description: Trains AIs to understand and translate texts
- App Version: 3.1.133440
- App Website: https://www.deepl.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install DeepL with the following command
   ```
   brew install --cask deepl
   ```
4. DeepL is ready to use now!