---
title: "Install libmatroska on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Extensible, open standard container format for audio/video"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libmatroska on MacOS using homebrew

- App Name: libmatroska
- App description: Extensible, open standard container format for audio/video
- App Version: 1.6.3
- App Website: https://www.matroska.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libmatroska with the following command
   ```
   brew install libmatroska
   ```
4. libmatroska is ready to use now!