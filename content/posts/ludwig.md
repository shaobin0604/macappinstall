---
title: "Install ludwig on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Sentence search engine app that helps you write better English"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ludwig on MacOS using homebrew

- App Name: ludwig
- App description: Sentence search engine app that helps you write better English
- App Version: 1.1.7
- App Website: https://ludwig.guru/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ludwig with the following command
   ```
   brew install --cask ludwig
   ```
4. ludwig is ready to use now!