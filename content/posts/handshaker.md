---
title: "Install HandShaker on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "App for managing Android devices"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install HandShaker on MacOS using homebrew

- App Name: HandShaker
- App description: App for managing Android devices
- App Version: 2.5.6,408
- App Website: https://www.smartisan.com/apps/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install HandShaker with the following command
   ```
   brew install --cask handshaker
   ```
4. HandShaker is ready to use now!