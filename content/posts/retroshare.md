---
title: "Install RetroShare on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Friend-2-Friend and secure decentralised communication platform"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install RetroShare on MacOS using homebrew

- App Name: RetroShare
- App description: Friend-2-Friend and secure decentralised communication platform
- App Version: 0.6.6
- App Website: https://retroshare.cc/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install RetroShare with the following command
   ```
   brew install --cask retroshare
   ```
4. RetroShare is ready to use now!