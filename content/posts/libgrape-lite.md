---
title: "Install libgrape-lite on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "C++ library for parallel graph processing"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libgrape-lite on MacOS using homebrew

- App Name: libgrape-lite
- App description: C++ library for parallel graph processing
- App Version: 0.1.0
- App Website: https://github.com/alibaba/libgrape-lite

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libgrape-lite with the following command
   ```
   brew install libgrape-lite
   ```
4. libgrape-lite is ready to use now!