---
title: "Install The Cheat on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Game trainer"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install The Cheat on MacOS using homebrew

- App Name: The Cheat
- App description: Game trainer
- App Version: 1.2.5
- App Website: https://github.com/chazmcgarvey/thecheat

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install The Cheat with the following command
   ```
   brew install --cask the-cheat
   ```
4. The Cheat is ready to use now!