---
title: "Install Remote Wake Up on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Wake up devices with a click of a button"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Remote Wake Up on MacOS using homebrew

- App Name: Remote Wake Up
- App description: Wake up devices with a click of a button
- App Version: 1.4.1,34b
- App Website: https://www.witt-software.com/remotewakeup/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Remote Wake Up with the following command
   ```
   brew install --cask remote-wake-up
   ```
4. Remote Wake Up is ready to use now!