---
title: "Install libbpg on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Image format meant to improve on JPEG quality and file size"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libbpg on MacOS using homebrew

- App Name: libbpg
- App description: Image format meant to improve on JPEG quality and file size
- App Version: 0.9.8
- App Website: https://bellard.org/bpg/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libbpg with the following command
   ```
   brew install libbpg
   ```
4. libbpg is ready to use now!