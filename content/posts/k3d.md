---
title: "Install k3d on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Little helper to run Rancher Lab's k3s in Docker"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install k3d on MacOS using homebrew

- App Name: k3d
- App description: Little helper to run Rancher Lab's k3s in Docker
- App Version: 5.3.0
- App Website: https://k3d.io

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install k3d with the following command
   ```
   brew install k3d
   ```
4. k3d is ready to use now!