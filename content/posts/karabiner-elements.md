---
title: "Install Karabiner Elements on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Keyboard customizer"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Karabiner Elements on MacOS using homebrew

- App Name: Karabiner Elements
- App description: Keyboard customizer
- App Version: 14.3.0
- App Website: https://pqrs.org/osx/karabiner/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Karabiner Elements with the following command
   ```
   brew install --cask karabiner-elements
   ```
4. Karabiner Elements is ready to use now!