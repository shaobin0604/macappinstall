---
title: "Install Softube Central on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Installer for installation and license activation of Softube products"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Softube Central on MacOS using homebrew

- App Name: Softube Central
- App description: Installer for installation and license activation of Softube products
- App Version: 1.5.9
- App Website: https://www.softube.com/softube-central/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Softube Central with the following command
   ```
   brew install --cask softube-central
   ```
4. Softube Central is ready to use now!