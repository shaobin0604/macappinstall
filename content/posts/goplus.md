---
title: "Install goplus on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Programming language for engineering, STEM education, and data science"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install goplus on MacOS using homebrew

- App Name: goplus
- App description: Programming language for engineering, STEM education, and data science
- App Version: 1.0.39
- App Website: https://goplus.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install goplus with the following command
   ```
   brew install goplus
   ```
4. goplus is ready to use now!