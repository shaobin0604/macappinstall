---
title: "Install KeyCue on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Finds, learns and remembers keyboard shortcuts"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install KeyCue on MacOS using homebrew

- App Name: KeyCue
- App description: Finds, learns and remembers keyboard shortcuts
- App Version: 9.10
- App Website: https://www.ergonis.com/products/keycue/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install KeyCue with the following command
   ```
   brew install --cask keycue
   ```
4. KeyCue is ready to use now!