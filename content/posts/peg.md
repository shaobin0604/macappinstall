---
title: "Install peg on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Program to perform pattern matching on text"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install peg on MacOS using homebrew

- App Name: peg
- App description: Program to perform pattern matching on text
- App Version: 0.1.18
- App Website: https://www.piumarta.com/software/peg/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install peg with the following command
   ```
   brew install peg
   ```
4. peg is ready to use now!