---
title: "Install Clash for Windows on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GUI based on Clash"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Clash for Windows on MacOS using homebrew

- App Name: Clash for Windows
- App description: GUI based on Clash
- App Version: 0.19.8
- App Website: https://github.com/Fndroid/clash_for_windows_pkg

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Clash for Windows with the following command
   ```
   brew install --cask clash-for-windows
   ```
4. Clash for Windows is ready to use now!