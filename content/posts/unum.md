---
title: "Install unum on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Interconvert numbers, Unicode, and HTML/XHTML entities"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install unum on MacOS using homebrew

- App Name: unum
- App description: Interconvert numbers, Unicode, and HTML/XHTML entities
- App Version: 3.4-14.0.0
- App Website: https://www.fourmilab.ch/webtools/unum/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install unum with the following command
   ```
   brew install unum
   ```
4. unum is ready to use now!