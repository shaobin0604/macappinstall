---
title: "Install Webex Meetings on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Video communication and virtual meeting platform"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Webex Meetings on MacOS using homebrew

- App Name: Webex Meetings
- App description: Video communication and virtual meeting platform
- App Version: 2201.1322.4201.4
- App Website: https://www.webex.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Webex Meetings with the following command
   ```
   brew install --cask webex-meetings
   ```
4. Webex Meetings is ready to use now!