---
title: "Install bzr-builder on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Bazaar plugin to construct a branch based on a recipe"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install bzr-builder on MacOS using homebrew

- App Name: bzr-builder
- App description: Bazaar plugin to construct a branch based on a recipe
- App Version: 0.7.3
- App Website: https://launchpad.net/bzr-builder

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install bzr-builder with the following command
   ```
   brew install bzr-builder
   ```
4. bzr-builder is ready to use now!