---
title: "Install repl on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Wrap non-interactive programs with a REPL"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install repl on MacOS using homebrew

- App Name: repl
- App description: Wrap non-interactive programs with a REPL
- App Version: 1.0.0
- App Website: https://github.com/defunkt/repl

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install repl with the following command
   ```
   brew install repl
   ```
4. repl is ready to use now!