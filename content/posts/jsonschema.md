---
title: "Install jsonschema on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Implementation of JSON Schema for Python"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install jsonschema on MacOS using homebrew

- App Name: jsonschema
- App description: Implementation of JSON Schema for Python
- App Version: 4.4.0
- App Website: https://github.com/Julian/jsonschema

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install jsonschema with the following command
   ```
   brew install jsonschema
   ```
4. jsonschema is ready to use now!