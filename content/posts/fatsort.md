---
title: "Install fatsort on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Sorts FAT16 and FAT32 partitions"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install fatsort on MacOS using homebrew

- App Name: fatsort
- App description: Sorts FAT16 and FAT32 partitions
- App Version: 1.6.4
- App Website: https://fatsort.sourceforge.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install fatsort with the following command
   ```
   brew install fatsort
   ```
4. fatsort is ready to use now!