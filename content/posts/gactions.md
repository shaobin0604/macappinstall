---
title: "Install gactions on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "CLI tool for Google Actions SDK"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gactions on MacOS using homebrew

- App Name: gactions
- App description: CLI tool for Google Actions SDK
- App Version: 3
- App Website: https://developers.google.com/assistant/conversational

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gactions with the following command
   ```
   brew install --cask gactions
   ```
4. gactions is ready to use now!