---
title: "Install Wing Personal on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Free Python IDE designed for students and hobbyists"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Wing Personal on MacOS using homebrew

- App Name: Wing Personal
- App description: Free Python IDE designed for students and hobbyists
- App Version: 8.1.2.0
- App Website: https://www.wingware.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Wing Personal with the following command
   ```
   brew install --cask wing-personal
   ```
4. Wing Personal is ready to use now!