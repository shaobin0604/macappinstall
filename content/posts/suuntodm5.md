---
title: "Install Suunto DM5 on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Create dive plans and analyze your dives"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Suunto DM5 on MacOS using homebrew

- App Name: Suunto DM5
- App description: Create dive plans and analyze your dives
- App Version: 1.5.3
- App Website: https://www.suunto.com/Support/software-support/dm5/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Suunto DM5 with the following command
   ```
   brew install --cask suuntodm5
   ```
4. Suunto DM5 is ready to use now!