---
title: "Install Jump Desktop Connect on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Remote desktop app"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Jump Desktop Connect on MacOS using homebrew

- App Name: Jump Desktop Connect
- App description: Remote desktop app
- App Version: 6.7.69,60769
- App Website: https://jumpdesktop.com/connect/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Jump Desktop Connect with the following command
   ```
   brew install --cask jump-desktop-connect
   ```
4. Jump Desktop Connect is ready to use now!