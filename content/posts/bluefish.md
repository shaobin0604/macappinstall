---
title: "Install Bluefish on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Open source code editor"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Bluefish on MacOS using homebrew

- App Name: Bluefish
- App description: Open source code editor
- App Version: 2.2.12
- App Website: http://bluefish.openoffice.nl/index.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Bluefish with the following command
   ```
   brew install --cask bluefish
   ```
4. Bluefish is ready to use now!