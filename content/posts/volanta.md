---
title: "Install Volanta on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Personal flight tracker"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Volanta on MacOS using homebrew

- App Name: Volanta
- App description: Personal flight tracker
- App Version: 1.1.11,58470a63
- App Website: https://volanta.app/roadmap/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Volanta with the following command
   ```
   brew install --cask volanta
   ```
4. Volanta is ready to use now!