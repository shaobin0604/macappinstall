---
title: "Install cargo-instruments on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Easily generate Instruments traces for your rust crate"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cargo-instruments on MacOS using homebrew

- App Name: cargo-instruments
- App description: Easily generate Instruments traces for your rust crate
- App Version: 0.4.4
- App Website: https://github.com/cmyr/cargo-instruments

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cargo-instruments with the following command
   ```
   brew install cargo-instruments
   ```
4. cargo-instruments is ready to use now!