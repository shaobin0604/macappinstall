---
title: "Install atop on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Advanced system and process monitor for Linux using process events"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install atop on MacOS using homebrew

- App Name: atop
- App description: Advanced system and process monitor for Linux using process events
- App Version: 2.7.1
- App Website: https://www.atoptool.nl

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install atop with the following command
   ```
   brew install atop
   ```
4. atop is ready to use now!