---
title: "Install systemc on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Core SystemC language and examples"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install systemc on MacOS using homebrew

- App Name: systemc
- App description: Core SystemC language and examples
- App Version: 2.3.2
- App Website: https://accellera.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install systemc with the following command
   ```
   brew install systemc
   ```
4. systemc is ready to use now!