---
title: "Install Duplicacy on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Cloud backup tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Duplicacy on MacOS using homebrew

- App Name: Duplicacy
- App description: Cloud backup tool
- App Version: 2.1.2
- App Website: https://duplicacy.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Duplicacy with the following command
   ```
   brew install --cask duplicacy
   ```
4. Duplicacy is ready to use now!