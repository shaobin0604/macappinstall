---
title: "Install Docker Toolbox on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Docker Toolbox on MacOS using homebrew

- App Name: Docker Toolbox
- App description: null
- App Version: 19.03.1
- App Website: https://www.docker.com/products/docker-toolbox

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Docker Toolbox with the following command
   ```
   brew install --cask docker-toolbox
   ```
4. Docker Toolbox is ready to use now!