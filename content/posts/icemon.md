---
title: "Install icemon on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Icecream GUI Monitor"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install icemon on MacOS using homebrew

- App Name: icemon
- App description: Icecream GUI Monitor
- App Version: 3.3
- App Website: https://github.com/icecc/icemon

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install icemon with the following command
   ```
   brew install icemon
   ```
4. icemon is ready to use now!