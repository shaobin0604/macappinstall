---
title: "Install cargo-llvm-lines on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Count lines of LLVM IR per generic function"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install cargo-llvm-lines on MacOS using homebrew

- App Name: cargo-llvm-lines
- App description: Count lines of LLVM IR per generic function
- App Version: 0.4.13
- App Website: https://github.com/dtolnay/cargo-llvm-lines

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install cargo-llvm-lines with the following command
   ```
   brew install cargo-llvm-lines
   ```
4. cargo-llvm-lines is ready to use now!