---
title: "Install Parallels Client on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "RDP client"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Parallels Client on MacOS using homebrew

- App Name: Parallels Client
- App description: RDP client
- App Version: 18.3.0,22893
- App Website: https://www.parallels.com/products/ras/features/rdp-client/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Parallels Client with the following command
   ```
   brew install --cask parallels-client
   ```
4. Parallels Client is ready to use now!