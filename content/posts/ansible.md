---
title: "Install ansible on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Automate deployment, configuration, and upgrading"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install ansible on MacOS using homebrew

- App Name: ansible
- App description: Automate deployment, configuration, and upgrading
- App Version: 5.2.0
- App Website: https://www.ansible.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install ansible with the following command
   ```
   brew install ansible
   ```
4. ansible is ready to use now!