---
title: "Install Lens on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Kubernetes IDE"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Lens on MacOS using homebrew

- App Name: Lens
- App description: Kubernetes IDE
- App Version: 5.3.4,20220120.1
- App Website: https://k8slens.dev/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Lens with the following command
   ```
   brew install --cask lens
   ```
4. Lens is ready to use now!