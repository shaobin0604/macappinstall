---
title: "Install gdk-pixbuf on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Toolkit for image loading and pixel buffer manipulation"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gdk-pixbuf on MacOS using homebrew

- App Name: gdk-pixbuf
- App description: Toolkit for image loading and pixel buffer manipulation
- App Version: 2.42.6
- App Website: https://gtk.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gdk-pixbuf with the following command
   ```
   brew install gdk-pixbuf
   ```
4. gdk-pixbuf is ready to use now!