---
title: "Install GlueMotion on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Create and correct time lapse movies"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install GlueMotion on MacOS using homebrew

- App Name: GlueMotion
- App description: Create and correct time lapse movies
- App Version: 2.0.3,89956f77-5445-463f-8baf-f151b507af40
- App Website: https://neededapps.com/gluemotion/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install GlueMotion with the following command
   ```
   brew install --cask gluemotion
   ```
4. GlueMotion is ready to use now!