---
title: "Install libprotoident on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Performs application layer protocol identification for flows"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libprotoident on MacOS using homebrew

- App Name: libprotoident
- App description: Performs application layer protocol identification for flows
- App Version: 2.0.13
- App Website: https://research.wand.net.nz/software/libprotoident.php

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libprotoident with the following command
   ```
   brew install libprotoident
   ```
4. libprotoident is ready to use now!