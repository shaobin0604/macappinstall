---
title: "Install john on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Featureful UNIX password cracker"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install john on MacOS using homebrew

- App Name: john
- App description: Featureful UNIX password cracker
- App Version: 1.9.0
- App Website: https://www.openwall.com/john/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install john with the following command
   ```
   brew install john
   ```
4. john is ready to use now!