---
title: "Install gst-editing-services on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "GStreamer Editing Services"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install gst-editing-services on MacOS using homebrew

- App Name: gst-editing-services
- App description: GStreamer Editing Services
- App Version: 1.18.5
- App Website: https://gstreamer.freedesktop.org/modules/gst-editing-services.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install gst-editing-services with the following command
   ```
   brew install gst-editing-services
   ```
4. gst-editing-services is ready to use now!