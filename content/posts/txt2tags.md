---
title: "Install txt2tags on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Conversion tool to generating several file formats"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install txt2tags on MacOS using homebrew

- App Name: txt2tags
- App description: Conversion tool to generating several file formats
- App Version: 3.7
- App Website: https://txt2tags.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install txt2tags with the following command
   ```
   brew install txt2tags
   ```
4. txt2tags is ready to use now!