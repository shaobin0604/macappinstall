---
title: "Install TimeMachineEditor on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Utility to change the default backup interval of Time Machine"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install TimeMachineEditor on MacOS using homebrew

- App Name: TimeMachineEditor
- App description: Utility to change the default backup interval of Time Machine
- App Version: 5.1.9,213
- App Website: https://tclementdev.com/timemachineeditor/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install TimeMachineEditor with the following command
   ```
   brew install --cask timemachineeditor
   ```
4. TimeMachineEditor is ready to use now!