---
title: "Install exiftool on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Perl lib for reading and writing EXIF metadata"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install exiftool on MacOS using homebrew

- App Name: exiftool
- App description: Perl lib for reading and writing EXIF metadata
- App Version: 12.30
- App Website: https://exiftool.org

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install exiftool with the following command
   ```
   brew install exiftool
   ```
4. exiftool is ready to use now!