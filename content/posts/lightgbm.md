---
title: "Install lightgbm on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Fast, distributed, high performance gradient boosting framework"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install lightgbm on MacOS using homebrew

- App Name: lightgbm
- App description: Fast, distributed, high performance gradient boosting framework
- App Version: 3.3.2
- App Website: https://github.com/microsoft/LightGBM

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install lightgbm with the following command
   ```
   brew install lightgbm
   ```
4. lightgbm is ready to use now!