---
title: "Install PreForm on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "3D printing setup, management, and monitoring"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install PreForm on MacOS using homebrew

- App Name: PreForm
- App description: 3D printing setup, management, and monitoring
- App Version: 3.22.0,87_14649
- App Website: https://formlabs.com/tools/preform/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install PreForm with the following command
   ```
   brew install --cask preform
   ```
4. PreForm is ready to use now!