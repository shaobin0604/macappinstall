---
title: "Install leaf on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "General purpose reloader for all projects"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install leaf on MacOS using homebrew

- App Name: leaf
- App description: General purpose reloader for all projects
- App Version: 1.3.0
- App Website: https://pkg.go.dev/github.com/vrongmeal/leaf

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install leaf with the following command
   ```
   brew install leaf
   ```
4. leaf is ready to use now!