---
title: "Install benthos on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Stream processor for mundane tasks written in Go"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install benthos on MacOS using homebrew

- App Name: benthos
- App description: Stream processor for mundane tasks written in Go
- App Version: 3.63.0
- App Website: https://www.benthos.dev

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install benthos with the following command
   ```
   brew install benthos
   ```
4. benthos is ready to use now!