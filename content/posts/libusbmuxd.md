---
title: "Install libusbmuxd on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "USB multiplexor library for iOS devices"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libusbmuxd on MacOS using homebrew

- App Name: libusbmuxd
- App description: USB multiplexor library for iOS devices
- App Version: 2.0.2
- App Website: https://www.libimobiledevice.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libusbmuxd with the following command
   ```
   brew install libusbmuxd
   ```
4. libusbmuxd is ready to use now!