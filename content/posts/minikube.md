---
title: "Install minikube on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Run a Kubernetes cluster locally"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install minikube on MacOS using homebrew

- App Name: minikube
- App description: Run a Kubernetes cluster locally
- App Version: 1.25.1
- App Website: https://minikube.sigs.k8s.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install minikube with the following command
   ```
   brew install minikube
   ```
4. minikube is ready to use now!