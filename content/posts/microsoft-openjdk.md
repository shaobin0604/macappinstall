---
title: "Install Microsoft Build of OpenJDK on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "OpenJDK distribution from Microsoft"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Microsoft Build of OpenJDK on MacOS using homebrew

- App Name: Microsoft Build of OpenJDK
- App description: OpenJDK distribution from Microsoft
- App Version: 17.0.2.8.1
- App Website: https://microsoft.com/openjdk

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Microsoft Build of OpenJDK with the following command
   ```
   brew install --cask microsoft-openjdk
   ```
4. Microsoft Build of OpenJDK is ready to use now!