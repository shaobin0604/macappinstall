---
title: "Install Celestia on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Space simulation for exploring the universe in three dimensions"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Celestia on MacOS using homebrew

- App Name: Celestia
- App description: Space simulation for exploring the universe in three dimensions
- App Version: 1.6.2
- App Website: https://celestia.space/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Celestia with the following command
   ```
   brew install --cask celestia
   ```
4. Celestia is ready to use now!