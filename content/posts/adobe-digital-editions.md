---
title: "Install Adobe Digital Editions on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "E-book reader"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Adobe Digital Editions on MacOS using homebrew

- App Name: Adobe Digital Editions
- App description: E-book reader
- App Version: 4.5.11
- App Website: https://www.adobe.com/solutions/ebook/digital-editions.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Adobe Digital Editions with the following command
   ```
   brew install --cask adobe-digital-editions
   ```
4. Adobe Digital Editions is ready to use now!