---
title: "Install pnetcdf on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Parallel netCDF library for scientific data using the OpenMPI library"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install pnetcdf on MacOS using homebrew

- App Name: pnetcdf
- App description: Parallel netCDF library for scientific data using the OpenMPI library
- App Version: 1.12.2
- App Website: https://parallel-netcdf.github.io/index.html

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install pnetcdf with the following command
   ```
   brew install pnetcdf
   ```
4. pnetcdf is ready to use now!