---
title: "Install Aural Player on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Audio player inspired by Winamp"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Aural Player on MacOS using homebrew

- App Name: Aural Player
- App description: Audio player inspired by Winamp
- App Version: 3.5.2
- App Website: https://github.com/maculateConception/aural-player

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Aural Player with the following command
   ```
   brew install --cask aural
   ```
4. Aural Player is ready to use now!