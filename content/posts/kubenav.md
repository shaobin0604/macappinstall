---
title: "Install kubenav on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Navigator for your Kubernetes clusters right in your pocket"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install kubenav on MacOS using homebrew

- App Name: kubenav
- App description: Navigator for your Kubernetes clusters right in your pocket
- App Version: 3.9.0
- App Website: https://kubenav.io/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install kubenav with the following command
   ```
   brew install --cask kubenav
   ```
4. kubenav is ready to use now!