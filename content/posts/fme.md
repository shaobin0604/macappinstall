---
title: "Install FME Desktop on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Platform for integrating spatial data"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install FME Desktop on MacOS using homebrew

- App Name: FME Desktop
- App description: Platform for integrating spatial data
- App Version: 2021.2.3,21812
- App Website: https://www.safe.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install FME Desktop with the following command
   ```
   brew install --cask fme
   ```
4. FME Desktop is ready to use now!