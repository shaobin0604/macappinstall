---
title: "Install wgcf on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Generate WireGuard profile from Cloudflare Warp account"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install wgcf on MacOS using homebrew

- App Name: wgcf
- App description: Generate WireGuard profile from Cloudflare Warp account
- App Version: 2.2.11
- App Website: https://github.com/ViRb3/wgcf

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install wgcf with the following command
   ```
   brew install wgcf
   ```
4. wgcf is ready to use now!