---
title: "Install marked on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Markdown parser and compiler built for speed"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install marked on MacOS using homebrew

- App Name: marked
- App description: Markdown parser and compiler built for speed
- App Version: 4.0.12
- App Website: https://marked.js.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install marked with the following command
   ```
   brew install marked
   ```
4. marked is ready to use now!