---
title: "Install libimobiledevice on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Library to communicate with iOS devices natively"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install libimobiledevice on MacOS using homebrew

- App Name: libimobiledevice
- App description: Library to communicate with iOS devices natively
- App Version: 1.3.0
- App Website: https://www.libimobiledevice.org/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install libimobiledevice with the following command
   ```
   brew install libimobiledevice
   ```
4. libimobiledevice is ready to use now!