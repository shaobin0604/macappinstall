---
title: "Install sslsplit on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Man-in-the-middle attacks against SSL encrypted network connections"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install sslsplit on MacOS using homebrew

- App Name: sslsplit
- App description: Man-in-the-middle attacks against SSL encrypted network connections
- App Version: 0.5.5
- App Website: https://www.roe.ch/SSLsplit

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install sslsplit with the following command
   ```
   brew install sslsplit
   ```
4. sslsplit is ready to use now!