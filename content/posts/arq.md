---
title: "Install Arq on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Multi-cloud backup application"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Arq on MacOS using homebrew

- App Name: Arq
- App description: Multi-cloud backup application
- App Version: 7.14.1
- App Website: https://www.arqbackup.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Arq with the following command
   ```
   brew install --cask arq
   ```
4. Arq is ready to use now!