---
title: "Install DiffMerge on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Visually compare and merge files"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install DiffMerge on MacOS using homebrew

- App Name: DiffMerge
- App description: Visually compare and merge files
- App Version: 4.2.1.1013
- App Website: https://www.sourcegear.com/diffmerge/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install DiffMerge with the following command
   ```
   brew install --cask diffmerge
   ```
4. DiffMerge is ready to use now!