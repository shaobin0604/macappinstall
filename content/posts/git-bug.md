---
title: "Install git-bug on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Distributed, offline-first bug tracker embedded in git, with bridges"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install git-bug on MacOS using homebrew

- App Name: git-bug
- App description: Distributed, offline-first bug tracker embedded in git, with bridges
- App Version: 0.7.2
- App Website: https://github.com/MichaelMure/git-bug

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install git-bug with the following command
   ```
   brew install git-bug
   ```
4. git-bug is ready to use now!