---
title: "Install Manta on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Invoicing desktop app with customizable templates"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Manta on MacOS using homebrew

- App Name: Manta
- App description: Invoicing desktop app with customizable templates
- App Version: 1.1.4
- App Website: https://getmanta.app/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Manta with the following command
   ```
   brew install --cask manta
   ```
4. Manta is ready to use now!