---
title: "Install freedink on MacOS with Brew  – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Portable version of the Dink Smallwood game engine"
categories:
- Core
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install freedink on MacOS using homebrew

- App Name: freedink
- App description: Portable version of the Dink Smallwood game engine
- App Version: 109.6
- App Website: https://www.gnu.org/software/freedink/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install freedink with the following command
   ```
   brew install freedink
   ```
4. freedink is ready to use now!