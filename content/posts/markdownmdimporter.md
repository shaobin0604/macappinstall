---
title: "Install Markdown Spotlight Plugin on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "null"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install Markdown Spotlight Plugin on MacOS using homebrew

- App Name: Markdown Spotlight Plugin
- App description: null
- App Version: latest
- App Website: https://brettterpstra.com/2011/10/18/fixing-spotlight-indexing-of-markdown-content/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install Markdown Spotlight Plugin with the following command
   ```
   brew install --cask markdownmdimporter
   ```
4. Markdown Spotlight Plugin is ready to use now!