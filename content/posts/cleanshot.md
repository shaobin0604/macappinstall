---
title: "Install CleanShot on MacOS with Brew – Monterey, Big Sur, Mojave, Catalina, High Sierra, Capitan"
description: "Screen capturing tool"
categories:
- Cask
date: 2022-02-18T00:16:06+08:00
draft: false
---

This article explains the steps to install CleanShot on MacOS using homebrew

- App Name: CleanShot
- App description: Screen capturing tool
- App Version: 4.1
- App Website: https://getcleanshot.com/

# Install steps

1. Open Spotlight search using "**command + space**” button and type "**Terminal**". Then press "**return/enter**" key. This will open terminal.
2. Run the following command in terminal to install **Homebrew**
   ```
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```
3. Now install CleanShot with the following command
   ```
   brew install --cask cleanshot
   ```
4. CleanShot is ready to use now!